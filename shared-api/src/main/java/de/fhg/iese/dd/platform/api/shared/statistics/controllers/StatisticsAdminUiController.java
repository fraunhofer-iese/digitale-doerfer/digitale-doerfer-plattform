/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.statistics.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.statistics.clientmodel.ClientStatisticsReportReference;
import de.fhg.iese.dd.platform.api.shared.statistics.clientmodel.StatisticsClientModelMapper;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.exceptions.StatisticsReportNotFoundException;
import de.fhg.iese.dd.platform.business.shared.statistics.security.GetStatisticsReportAction;
import de.fhg.iese.dd.platform.business.shared.statistics.services.IStatisticsService;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/adminui/statistics")
@Api(tags = {"shared.adminui.statistics"})
public class StatisticsAdminUiController extends BaseController {

    @Autowired
    private StatisticsClientModelMapper statisticsClientModelMapper;

    @Autowired
    private IStatisticsService statisticsService;

    @ApiOperation(value = "Most recent statistics reports",
            notes = "Returns most recent daily and monthly statistics reports")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = GetStatisticsReportAction.class)
    @GetMapping("/report")
    public List<ClientStatisticsReportReference> getStatisticsReportReferences() {

        getRoleService().decidePermissionAndThrowNotAuthorized(GetStatisticsReportAction.class,
                getCurrentPersonNotNull());

        return statisticsService.getConfiguredReportDefinitions(-2).stream()
                .map(statisticsClientModelMapper::toClientStatisticsReportReference)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Download statistics report",
            notes = "Downloads a specific statistics report")
    @ApiExceptions({
            ClientExceptionType.STATISTICS_REPORT_NOT_FOUND,
            ClientExceptionType.FILE_STORAGE_FAILED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = GetStatisticsReportAction.class)
    @GetMapping(value = "/report/content",
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public
    @IgnoreArchitectureViolation(
            value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
            reason = "byte array is needed for file download"
    )
    @ResponseBody ResponseEntity<byte[]> getStatisticsReportContent(
            @RequestParam
            @ApiParam(value = "file name of the statistics file", required = true)
            String reportFileName
    ) {

        getRoleService().decidePermissionAndThrowNotAuthorized(GetStatisticsReportAction.class,
                getCurrentPersonNotNull());

        statisticsService.getConfiguredReportDefinitions(-2).stream()
                .filter((StatisticsReportDefinition r) -> StringUtils.equalsAnyIgnoreCase(reportFileName,
                        r.getFileNameText(),
                        r.getFileNameCsv(),
                        r.getFileNameCsvMetadata()))
                .findFirst()
                .orElseThrow(() -> new StatisticsReportNotFoundException("No statistics report '{}' available",
                        reportFileName));
        String contentType;
        if (StringUtils.endsWithIgnoreCase(reportFileName, "csv")) {
            contentType = "text/csv;charset=UTF-8";
        } else {
            contentType = "text/plain;charset=UTF-8";
        }

        byte[] reportBytes = statisticsService.getStatisticsReport(reportFileName);

        String headerValue = "attachment; filename=\"" + StringUtils.substringAfterLast(reportFileName, "/") + "\"";
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, contentType)
                .header(HttpHeaders.CONTENT_DISPOSITION, headerValue)
                .body(reportBytes);
    }

}

