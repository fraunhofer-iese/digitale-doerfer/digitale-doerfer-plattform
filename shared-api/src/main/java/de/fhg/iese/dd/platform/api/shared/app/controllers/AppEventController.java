/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Balthasar Weitzel, Johannes Schneider, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.app.clientevent.ClientAppVariantChangeSelectedGeoAreasRequest;
import de.fhg.iese.dd.platform.api.shared.app.clientevent.ClientAppVariantChangeSelectedGeoAreasResponse;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.app.events.AppVariantChangeSelectedGeoAreasConfirmation;
import de.fhg.iese.dd.platform.business.shared.app.events.AppVariantChangeSelectedGeoAreasRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/app/event")
@Api(tags = {"shared.app.events"})
class AppEventController extends BaseController {

    @Autowired
    private IGeoAreaService geoAreaService;

    @ApiOperation(value = "Changes the selected geo areas for an app variant",
            notes = "Change the geo areas a person has selected for an app variant.")
    @ApiExceptions({
            ClientExceptionType.APP_VARIANT_USAGE_INVALID,
            ClientExceptionType.GEO_AREA_NOT_FOUND,
            ClientExceptionType.APP_VARIANT_NOT_FOUND,
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY,
            notes = "The appVariantIdentifier is part of the request entity")
    @PostMapping("appVariantChangeSelectedGeoAreasRequest")
    public ClientAppVariantChangeSelectedGeoAreasResponse onAppVariantChangeSelectedGeoAreasRequest(
            @Valid @RequestBody
            @ApiParam(value = "The new geo areas the person is interested in for this app variant.", required = true)
            final ClientAppVariantChangeSelectedGeoAreasRequest clientRequest
    ) {

        final Person person = getCurrentPersonNotNull();

        final AppVariant appVariant = getAppVariantNotNull();

        final Set<GeoArea> newSelectedGeoAreas =
                new HashSet<>(geoAreaService.findAllById(clientRequest.getNewSelectedGeoAreaIds()));

        addAdditionalLogValue("countSelectedGeoAreas", newSelectedGeoAreas.size());

        final AppVariantChangeSelectedGeoAreasRequest request = AppVariantChangeSelectedGeoAreasRequest.builder()
                .person(person)
                .appVariant(appVariant)
                .newSelectedGeoAreas(newSelectedGeoAreas)
                .build();

        final AppVariantChangeSelectedGeoAreasConfirmation confirmation =
                notifyAndWaitForReply(AppVariantChangeSelectedGeoAreasConfirmation.class, request);
        return ClientAppVariantChangeSelectedGeoAreasResponse.builder()
                .personId(person.getId())
                .newSelectedGeoAreaIds(toSortedIds(confirmation.getNewSelectedGeoAreas()))
                .build();
    }

    private static <T extends BaseEntity> List<String> toSortedIds(final Collection<T> entities) {
        return entities.stream()
                .map(BaseEntity::getId)
                .sorted()
                .collect(Collectors.toList());
    }

}
