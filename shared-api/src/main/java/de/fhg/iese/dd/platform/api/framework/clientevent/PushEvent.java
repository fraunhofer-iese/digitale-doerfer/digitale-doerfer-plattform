/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Danielle Korth, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.clientevent;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;

/**
 * Documentation annotation for a method that sends push events. Typically used on a @{@link EventProcessing} method in
 * a PushEventProcessor.
 */
@Documented
@Repeatable(PushEvents.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PushEvent {

    /**
     * Client event that is sent.
     */
    Class<? extends ClientBaseEvent> clientEvent();

    /**
     * Internal event that triggers the sending of the client event. Only required if it differs from the event that is
     * processed by the annotated method.
     */
    Class<? extends BaseEvent> event() default BaseEvent.class;

    /**
     * Simple name of the push category id that is used when sending the event. It is looked up at the datamanagement
     * layer of the module where the annotation is used.
     */
    String categoryIdName() default "";

    /**
     * Subject of the push categories that are used there. All categories with {@link PushCategory#getSubject()} equal
     * to this subject are used.
     */
    String categorySubject() default "";

    /**
     * If the event is sent loud
     */
    boolean loud() default true;

    /**
     * Documentation to whom the event is sent and what the typical trigger is.
     */
    String notes() default "";

}
