/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.shop.clientmodel;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.api.shared.address.clientmodel.AddressClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHours;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHoursEntry;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;

@Component
public class ShopClientModelMapper {

    @Autowired
    private FileClientModelMapper fileClientModelMapper;
    @Autowired
    private AddressClientModelMapper addressClientModelMapper;

    public ClientShop createClientShopReduced(Shop shop) {
        if (shop == null) {
            return null;
        }
        ClientShop cs = new ClientShop();
        cs.setId(shop.getId());
        if (!CollectionUtils.isEmpty(shop.getAddresses())) {
            cs.setAddress(addressClientModelMapper.toClientAddress(
                    shop.getAddresses()
                            .iterator().next()
                            .getAddress()));
        }
        cs.setEmail(shop.getEmail());
        cs.setName(shop.getName());
        cs.setPhone(shop.getPhone());
        cs.setProfilePicture(fileClientModelMapper.toClientMediaItem(shop.getProfilePicture()));
        cs.setWebShopUrl(shop.getWebShopURL());
        return cs;
    }

    public ClientShop createClientShop(Shop shop) {
        if (shop == null) {
            return null;
        }
        ClientShop cs = createClientShopReduced(shop);
        cs.setOpeningHours(shop.getOpeningHours());
        return cs;
    }
    
    public OpeningHours fromClientOpeningHoursEntries(Set<ClientOpeningHoursEntry> clientOpeningHoursEntries) {
        if (CollectionUtils.isEmpty(clientOpeningHoursEntries)) {
            return null;
        }
        OpeningHours openingHours = new OpeningHours();
        openingHours.setEntries(
                clientOpeningHoursEntries.stream()
                        .map(entry -> new OpeningHoursEntry(entry.getWeekday())
                                .withFromMinutes(entry.getFromTime())
                                .withToMinutes(entry.getToTime()))
                        .collect(Collectors.toSet())
        );
        return openingHours;
    }

}
