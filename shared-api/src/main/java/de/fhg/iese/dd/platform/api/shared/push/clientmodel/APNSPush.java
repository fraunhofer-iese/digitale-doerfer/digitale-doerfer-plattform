/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.clientmodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import lombok.NoArgsConstructor;

@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
@IgnoreArchitectureViolation(
        value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
        reason = "Only used for technical communication with push services, not with clients directly")
public class APNSPush {

    @JsonProperty("aps")
    private APNSPushContent aps;

    public APNSPush(boolean isSilent, String message, ClientBaseEvent event, Integer badge) {
        super();
        this.aps = new APNSPushContent(isSilent, message, event, badge);
    }

    @JsonProperty("aps")
    public APNSPushContent getAps() {
        return aps;
    }

    @JsonInclude(Include.NON_NULL)
    @IgnoreArchitectureViolation(
            value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
            reason = "Only used for technical communication with push services, not with clients directly")
    public static class APNSPushContent {

        @JsonIgnore
        private final boolean isSilent;
        @JsonIgnore
        private final String message;
        @JsonIgnore
        private final ClientBaseEvent event;
        @JsonIgnore
        private final String eventName;
        @JsonIgnore
        private final Integer badge;

        APNSPushContent(boolean isSilent, String message, ClientBaseEvent event, Integer badge) {
            super();
            this.isSilent = isSilent;
            this.message = message;
            this.event = event;
            this.badge = badge;
            if (event == null) {
                this.eventName = "Notification";
            } else {
                this.eventName = event.getClass().getSimpleName();
            }
        }

        @JsonProperty("event")
        @JsonInclude(Include.NON_NULL)
        public ClientBaseEvent getEvent() {
            return event;
        }

        @JsonProperty("category")
        public String getEventName() {
            return eventName;
        }

        @JsonProperty("badge")
        public Integer getBadge() {
            return badge;
        }

        @JsonProperty("content-available")
        public int getContentAvailable() {
            if (event != null) {
                return 1;
            } else if (isSilent) {
                return 1;
            } else {
                return 0;
            }
        }

        @JsonProperty("alert")
        @JsonInclude(Include.NON_NULL)
        public String getAlert(){
            if(isSilent){
                return null;
            }else{
                return message;
            }
        }

        @JsonProperty("sound")
        @JsonInclude(Include.NON_NULL)
        public String getSound(){
            if(isSilent){
                return null;
            }else{
                return "default";
            }
        }
        
    }

}
