/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.feature.controllers;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiException;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.feature.clientmodel.ClientFeatureExtended;
import de.fhg.iese.dd.platform.api.shared.feature.clientmodel.FeatureClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureMetaModel;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.business.shared.security.SuperAdminAction;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/administration/feature")
@Api(tags = {"admin", "shared.admin.feature"})
public class FeatureAdminController extends BaseController {

    @Autowired
    private IFeatureService featureService;
    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private IAppService appService;
    @Autowired
    private FeatureClientModelMapper featureClientModelMapper;

    @ApiOperation(value = "All available feature meta models",
            notes = "All available features and their meta models")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @GetMapping(value = "/metamodel")
    @IgnoreArchitectureViolation(
            value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
            reason = "Admin endpoint, non critical data")
    public List<FeatureMetaModel> getAllFeatureMetaModels() {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        return featureService.getAllFeatureMetaModels();
    }

    @ApiOperation(value = "Gets all features for a specific target",
            notes = "Gets all features in the configuration for the given target. All parameters are optional. " +
                    "Feature target can not have both app and app variant.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND,
            ClientExceptionType.APP_NOT_FOUND,
            ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiException(value = ClientExceptionType.UNSPECIFIED_BAD_REQUEST,
            reason = "If both app and app variant are provided")
    @GetMapping(value = "/target")
    public List<ClientFeatureExtended> getAllFeatures(
            @RequestParam(required = false) String tenantId,
            @RequestParam(required = false) String geoAreaId,
            @RequestParam(required = false) String appIdentifier,
            @RequestParam(required = false) String appVariantIdentifier) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        GeoArea geoArea = null;
        if (StringUtils.isNotEmpty(geoAreaId)) {
            geoArea = geoAreaService.findGeoAreaById(geoAreaId);
        }
        Tenant tenant = null;
        if (!StringUtils.isEmpty(tenantId)) {
            tenant = tenantService.findTenantById(tenantId);
        }
        App app = null;
        if (!StringUtils.isEmpty(appIdentifier)) {
            app = appService.findByAppIdentifier(appIdentifier);
        }
        AppVariant appVariant = null;
        if (!StringUtils.isEmpty(appVariantIdentifier)) {
            appVariant = appService.findAppVariantByAppVariantIdentifier(appVariantIdentifier);
        }
        if (app != null && appVariant != null) {
            throw new BadRequestException("Feature target can not have both app and app variant.");
        }
        FeatureTarget featureTarget;
        if (app == null) {
            featureTarget = FeatureTarget.of(geoArea, tenant, appVariant);
        } else {
            featureTarget = FeatureTarget.of(geoArea, tenant, app);
        }

        return featureClientModelMapper.toClientFeaturesExtended(
                featureService.getAllFeaturesForTarget(featureTarget, false));
    }

}
