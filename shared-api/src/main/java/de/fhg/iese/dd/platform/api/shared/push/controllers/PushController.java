/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2021 Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.push.clientevent.ClientPushCategoryUserSettingChangeResponse;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.ClientPushCategoryUserSetting;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.PushClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PlatformNotSupportedException;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushCategoryNotConfigurableException;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushRegistrationService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IParallelismService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.enums.PushPlatformType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/push")
@Api(tags = {"shared.push"})
class PushController extends BaseController {

    @Autowired
    private IPushRegistrationService pushRegistrationService;
    @Autowired
    private IPushCategoryService pushCategoryService;
    @Autowired
    private IPersonService personService;
    @Autowired
    private IParallelismService parallelismService;
    @Autowired
    private PushClientModelMapper pushClientModelMapper;

    @ApiOperation(value = "Registers the endpoint for a person",
            notes = "Connects an endpoint with a person to be able to receive person-specific notifications. " +
                    "Does not throw exceptions if the actual registration at the push provider failed.")
    @ApiExceptions({ClientExceptionType.PLATFORM_NOT_SUPPORTED,
            ClientExceptionType.APP_VARIANT_NOT_FOUND,
            ClientExceptionType.CONCURRENT_REQUEST_FAILED})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping(value = "/person")
    public void registerEndpointPerson(
            @ApiParam(
                    value = "device and app-specific token provided by the device-specific notification service platform",
                    required = true)
            @RequestParam("token") String token,

            @ApiParam(value = "identifies the notification service platform the endpoint is using", required = true)
            @RequestParam("platform") PushPlatformType platformType) {

        checkTokenAndPlatform(token, platformType);

        final Person currentUser = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        //waiting for this response is not useful, since the clients can not do anything if there is an exception
        parallelismService.getBlockableExecutor().submit(() -> {
            try {
                pushRegistrationService.registerEndpoint(token, platformType, appVariant, currentUser);
            } catch (RuntimeException e) {
                log.error("Failed to register push endpoint for app variant " + appVariant.getAppVariantIdentifier() +
                        " and platform " + platformType, e);
            }
        });
    }

    @ApiOperation(value = "Unregisters the endpoint for a person",
            notes = "Disconnects an endpoint from a person and deletes it. Authorization is not required, " +
                    "token, personId, platform and app variant are sufficient to identify the user.")
    @ApiExceptions({ClientExceptionType.PLATFORM_NOT_SUPPORTED,
            ClientExceptionType.APP_VARIANT_NOT_FOUND,
            ClientExceptionType.CONCURRENT_REQUEST_FAILED})
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @DeleteMapping
    public void unregisterEndpoint(
            @ApiParam(
                    value = "device and app-specific token provided by the device-specific notification service platform",
                    required = true)
            @RequestParam("token") String token,

            @ApiParam(value = "ID of the person", required = true)
            @RequestParam("personId") String personId,

            @ApiParam(value = "identifies the notification service platform the endpoint is using", required = true)
            @RequestParam("platform") PushPlatformType platformType) {

        checkTokenAndPlatform(token, platformType);

        final Person person = personService.findPersonById(personId);

        pushRegistrationService.unregisterEndpoint(token, platformType, getAppVariantNotNull(), person);
    }

    private void checkTokenAndPlatform(String token, PushPlatformType platformType) {
        if (platformType == null) {
            throw new PlatformNotSupportedException("No platform provided").withDetail("platform");
        }
        if (StringUtils.isEmpty(token)) {
            throw new BadRequestException("Token can not be empty").withDetail("token");
        }
    }

    @ApiOperation(value = "Gets all push category settings for the current user in the current app variant",
            notes = "Returns only settings that can be configured by the user and that are visible. " +
                    "If there is no person provided the default settings are returned.")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/categorySetting")
    public List<ClientPushCategoryUserSetting> getPushCategoryUserSettings() {

        Person currentUser = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();

        return pushCategoryService.getConfigurableAndVisiblePushCategoryUserSettings(currentUser, appVariant).stream()
                .map(pushClientModelMapper::toClientPushCategoryUserSetting)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Changes a push category setting for the current user",
            notes = "Changes the settings for this user for the given push category.\n" +
                    "If there are child categories for this category they are also changed. These changed child " +
                    "categories are returned as `changedDependentPushCategoryUserSettings`.")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND,
            ClientExceptionType.PUSH_CATEGORY_NOT_FOUND,
            ClientExceptionType.PUSH_CATEGORY_NOT_CONFIGURABLE})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping(value = "/categorySetting/{categoryId}")
    public ClientPushCategoryUserSettingChangeResponse changePushCategoryUserSetting(

            @ApiParam(value = "id of the push category to be changed", required = true)
            @PathVariable("categoryId") String categoryId,

            @ApiParam(value = "flag if loud push should be enabled for that category", required = true)
            @RequestParam boolean loudPushEnabled) {

        Person currentUser = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();

        PushCategory pushCategory = pushCategoryService.findPushCategoryById(categoryId);

        if (!pushCategory.getApp().equals(appVariant.getApp())) {
            throw new PushCategoryNotConfigurableException(
                    "The category '{}' is not defined for app variant '{}'.", pushCategory.getId(),
                    appVariant.getAppVariantIdentifier());
        }

        addAdditionalLogValue("loud", loudPushEnabled);

        Pair<PushCategoryUserSetting, List<PushCategoryUserSetting>> pushCategoryUserSetting =
                pushCategoryService.changePushCategoryUserSetting(pushCategory, currentUser, appVariant,
                        loudPushEnabled);

        return ClientPushCategoryUserSettingChangeResponse.builder()
                .changedPushCategoryUserSetting(
                        pushClientModelMapper.toClientPushCategoryUserSetting(pushCategoryUserSetting.getLeft()))
                .changedDependentPushCategoryUserSettings(
                        pushClientModelMapper.toClientPushCategoryUserSetting(pushCategoryUserSetting.getRight()))
                .build();
    }

}
