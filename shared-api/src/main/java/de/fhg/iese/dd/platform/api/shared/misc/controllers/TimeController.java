/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.misc.controllers;

import java.time.DateTimeException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/time")
@Api(tags={"shared.time"})
class TimeController extends BaseController {

    @ApiOperation(value = "Gets the current platform time as timestamp (ms since 1970)")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping(produces = { MediaType.TEXT_PLAIN_VALUE })
    public String getTime(
            @RequestParam
            @ApiParam( value = "if the timestamp should be utc or in the specific timezone", required = true)
            boolean inUtc) {

        return Long.toString(getTimeService().toTimeMillis(getZonedDateTime(inUtc)));
    }

    @ApiOperation(value = "Gets the current platform time formatted")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping(value="/formatted", produces = { MediaType.TEXT_PLAIN_VALUE })
    public String getStringTime(
            @RequestParam
            @ApiParam(value = "if the time should be utc or in the specific timezone", required = true)
            boolean inUtc,

            @RequestParam(required = false)
            @ApiParam("format string according to ISO 8601")
            String format) {

        ZonedDateTime dateTime = getZonedDateTime(inUtc);

        if (StringUtils.isNotEmpty(format)) {
            try {
                return getZonedFormatter(format, inUtc).format(dateTime);
            } catch (DateTimeException e) {
                log.error("Illegal format for time: {}", format);
                return dateTime.toString();
            }
        } else {
            return dateTime.toString();
        }

    }

    private ZonedDateTime getZonedDateTime(boolean inUtc) {
        return inUtc ? getTimeService().nowUTC() : getTimeService().nowLocal();
    }

    private DateTimeFormatter getZonedFormatter(String format, boolean inUtc) {
        return DateTimeFormatter.ofPattern(format).withZone(inUtc ? ZoneOffset.UTC : getTimeService().getLocalTimeZone());
    }

}
