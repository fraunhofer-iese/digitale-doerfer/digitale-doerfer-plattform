/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Tahmid Ekram
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.ClientActionDocumentation;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.ClientRoleActionDocumentation;
import de.fhg.iese.dd.platform.api.shared.security.services.IClientAuthorizationDocumentationService;
import de.fhg.iese.dd.platform.business.shared.security.ListActionConfigurationAction;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/adminui")
@Api(tags = {"shared.adminui.authorization"})
public class AuthorizationAdminUiController extends BaseController {

    @Autowired
    private IClientAuthorizationDocumentationService clientAuthorizationDocumentationService;

    @ApiOperation(value = "Lists all action documentation",
            notes = "Lists documentation about all actions that are used to secure the endpoints.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = ListActionConfigurationAction.class)
    @GetMapping("/authorization/action/documentation")
    public List<ClientActionDocumentation> getActionDocumentation() {

        Person currentPerson = getCurrentPersonNotNull();
        getRoleService().decidePermissionAndThrowNotAuthorized(ListActionConfigurationAction.class, currentPerson);

        return clientAuthorizationDocumentationService.getAllClientActionDocumentations();
    }

    @ApiOperation(value = "Lists action documentation for individual roles",
            notes = "Lists documentation about unrestricted and entity restricted actions for each role")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = ListActionConfigurationAction.class)
    @GetMapping("/authorization/role/action/documentation")
    public List<ClientRoleActionDocumentation> getRoleActionDocumentation() {

        Person currentPerson = getCurrentPersonNotNull();
        getRoleService().decidePermissionAndThrowNotAuthorized(ListActionConfigurationAction.class, currentPerson);

        return clientAuthorizationDocumentationService.getAllClientRoleActionDocumentation();
    }

}
