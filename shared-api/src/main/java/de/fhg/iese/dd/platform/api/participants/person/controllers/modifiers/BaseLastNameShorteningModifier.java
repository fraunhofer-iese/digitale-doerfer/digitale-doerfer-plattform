/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import de.fhg.iese.dd.platform.api.framework.modifiers.IContentFilterSettings;
import de.fhg.iese.dd.platform.api.framework.modifiers.IOutgoingClientEntityModifier;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.BaseClientPerson;

public abstract class BaseLastNameShorteningModifier implements IOutgoingClientEntityModifier<BaseClientPerson> {

    @Override
    public Class<BaseClientPerson> getType() {
        return BaseClientPerson.class;
    }

    @Override
    public boolean modify(BaseClientPerson clientEntity, IContentFilterSettings contentFilterSettings) {
        clientEntity.setLastName(shorten(clientEntity.getLastName()));
        return true;
    }

    public static String shorten(String name) {
        if (name == null) {
            return null;
        }
        String trimmed = name.trim();
        if (!StringUtils.isEmpty(trimmed)) {
            return trimmed.substring(0, 1).toUpperCase(Locale.ROOT) + ".";
        }
        return name;
    }

}
