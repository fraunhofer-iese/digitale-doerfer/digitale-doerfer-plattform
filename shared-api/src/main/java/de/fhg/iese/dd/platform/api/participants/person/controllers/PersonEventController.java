/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Balthasar Weitzel, Johannes Schneider, Dominik Schnier, Benjamin Hassenfratz,
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers;

import de.fhg.iese.dd.platform.api.framework.*;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.*;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonOwn;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItemPlaceHolder;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.services.IClientMediaItemService;
import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.participants.person.events.*;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.EMailAlreadyUsedException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonAlreadyExistsException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonHasNoPendingNewEmailException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonInformationInvalidException;
import de.fhg.iese.dd.platform.business.participants.person.security.CreatePersonAction;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.PersonResetLastLoggedInRequest;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailAddressVerificationService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.business.shared.security.services.IOauthManagementService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.participants.feature.PersonEmailVerificationFeature;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Duration;

import static de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService.TokenAuthorizationStatus;

@RestController
@RequestMapping("/person/event")
@Api(tags = {"participants.person.events"})
class PersonEventController extends BaseController {

    @Getter
    @Setter
    @SuperBuilder
    @NoArgsConstructor
    //it only extends base event to match the signature of authorizationService.generateAuthToken
    private static class PersonRegistrationRequest extends BaseEvent {

        private String oauthId;
        private String email;

    }

    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;
    @Autowired
    private FileClientModelMapper fileClientModelMapper;
    @Autowired
    private IPersonService personService;
    @Autowired
    private IAuthorizationService authorizationService;
    @Autowired
    private IEmailAddressVerificationService emailAddressVerificationService;
    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private IOauthManagementService oauthManagementService;
    @Autowired
    private IClientMediaItemService clientMediaItemService;

    @ApiOperation(value = "Creates a new person",
            notes = "Creates a new Person. Refer to documentation of request for required or optional fields.")
    @ApiExceptions({
            ClientExceptionType.PERSON_INFORMATION_INVALID,
            ClientExceptionType.TENANT_NOT_FOUND,
            ClientExceptionType.EMAIL_ALREADY_REGISTERED,
            ClientExceptionType.ADDRESS_INVALID,
            ClientExceptionType.PERSON_ALREADY_EXISTS
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("personCreateRequest")
    public ClientPersonCreateResponse onPersonCreateRequest(
            @Valid @RequestBody
            @ApiParam(value = """
                    The details of the new person to be created.

                    Refer to documentation of request for required or optional fields.""",
                    required = true)
            ClientPersonCreateRequest request
    ) {
        final String oauthId = getOAuthUserIdOrNull();
        checkOauthId(oauthId);

        PersonCreateRequest personCreateRequest = toPersonCreateRequestWithoutOauthId(request);
        personCreateRequest.setOauthId(oauthId);
        Person createdPerson = notifyAndWaitForReply(PersonCreateConfirmation.class, personCreateRequest).getPerson();
        return ClientPersonCreateResponse.builder()
                .createdPerson(personClientModelMapper.createClientPersonOwn(createdPerson))
                .build();
    }

    @ApiOperation(value = "Creates a new person, authorized via registrationToken",
            notes = "Creates a new Person. Refer to documentation of request for required or optional fields.")
    @ApiExceptions({
            ClientExceptionType.PERSON_INFORMATION_INVALID,
            ClientExceptionType.TENANT_NOT_FOUND,
            ClientExceptionType.EMAIL_ALREADY_REGISTERED,
            ClientExceptionType.ADDRESS_INVALID,
            ClientExceptionType.PERSON_ALREADY_EXISTS
    })
    @ApiAuthentication(value = ApiAuthenticationType.ONE_TIME_TOKEN, notes = "valid registration token is required")
    @PostMapping("personCreateWithRegistrationTokenRequest")
    public ClientPersonCreateResponse onPersonCreateWithRegistrationTokenRequest(
            @Valid @RequestBody
            @ApiParam(value = """
                    The details of the new person to be created.

                    Refer to documentation of request for required or optional fields.""",
                    required = true)
            ClientPersonCreateWithRegistrationTokenRequest request,
            @RequestParam
            @ApiParam(value = "Alternative way of authentication without oauth, used by auth0 rules", required = true)
            String registrationToken
    ) {

        final String email = trimAndCheck(request.getEmail(), "email");
        final String oauthId = trimAndCheck(request.getOauthId(), "oauthId");
        authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(registrationToken,
                PersonRegistrationRequest.builder()
                        .email(email)
                        .oauthId(oauthId)
                        .build());
        checkOauthId(oauthId);

        PersonCreateRequest personCreateRequest = toPersonCreateRequestWithoutOauthId(request);
        personCreateRequest.setOauthId(oauthId);
        Person createdPerson = notifyAndWaitForReply(PersonCreateConfirmation.class, personCreateRequest).getPerson();
        return ClientPersonCreateResponse.builder()
                .createdPerson(personClientModelMapper.createClientPersonOwn(createdPerson))
                .build();
    }

    @ApiOperation(value = "Creates a new person by admin request",
            notes = "Creates a new Person. Refer to documentation of request for required or optional fields.\n" +
                    "Also registers an oauth account with the persons email and triggers a password reset mail.")
    @ApiExceptions({
            ClientExceptionType.PERSON_INFORMATION_INVALID,
            ClientExceptionType.TENANT_NOT_FOUND,
            ClientExceptionType.EMAIL_ALREADY_REGISTERED,
            ClientExceptionType.ADDRESS_INVALID,
            ClientExceptionType.PERSON_ALREADY_EXISTS
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {CreatePersonAction.class},
            notes = "Super admins and global user admins can create persons in every tenant and for every home area. " +
                    "Regular user admins may only create persons in tenants they are admins for (or with home areas of " +
                    "these tenants).")
    @PostMapping("personCreateByAdminRequest")
    public ClientPersonCreateByAdminResponse onPersonCreateByAdminRequest(
            @Valid @RequestBody
            @ApiParam(value = """
                    The details of the new person to be created.

                    Refer to documentation of request for required or optional fields.""",
                    required = true)
            ClientPersonCreateRequest request
    ) {
        PermissionTenantRestricted createPersonPermission =
                getRoleService().decidePermissionAndThrowNotAuthorized(CreatePersonAction.class,
                        getCurrentPersonNotNull());

        Pair<GeoArea, Tenant> homeAreaAndTenant =
                getHomeAreaAndTenant(request.getHomeAreaId(), request.getHomeCommunityId());

        if (createPersonPermission.isTenantDenied(homeAreaAndTenant.getRight())) {
            throw new NotAuthorizedException("Insufficient privileges to create person in tenant {}",
                    homeAreaAndTenant.getRight().getId());
        }

        PersonCreateByAdminRequest personCreateByAdminRequest =
                new PersonCreateByAdminRequest(toPersonCreateRequestWithoutOauthId(request));
        Person createdPerson =
                notifyAndWaitForReply(PersonCreateByAdminConfirmation.class, personCreateByAdminRequest).getPerson();
        return ClientPersonCreateByAdminResponse.builder()
                .createdPerson(personClientModelMapper.createClientPersonExtended(
                        personService.fetchPersonExtendedAttributes(createdPerson)))
                .build();
    }

    private PersonCreateRequest toPersonCreateRequestWithoutOauthId(ClientPersonCreateRequest request) {

        final String email = checkAndNormalizeEmail(check(request.getEmail(), "email"));

        String firstName = beautifyAndCheck(request.getFirstName(), "firstName");
        String lastName = beautifyAndCheck(request.getLastName(), "lastName");

        Address address = null;

        if (request.getAddress() != null) {
            String name = beautifyAndCheck(request.getAddress().getName(), "address.name");
            String city = beautifyAndCheck(request.getAddress().getCity(), "address.city");
            String street = beautifyAndCheck(request.getAddress().getStreet(), "address.street");
            String zip = trimAndCheck(request.getAddress().getZip(), "address.zip");
            address = Address.builder()
                    .name(name)
                    .street(street)
                    .zip(zip)
                    .city(city)
                    .gpsLocation(request.getAddress().getGpsLocation() == null ? null :
                            new GPSLocation(request.getAddress().getGpsLocation().getLatitude(),
                                    request.getAddress().getGpsLocation().getLongitude()))
                    .build();
        }

        Pair<GeoArea, Tenant> homeAreaAndTenant =
                getHomeAreaAndTenant(request.getHomeAreaId(), request.getHomeCommunityId());

        return PersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(trim(request.getNickName()))
                .phoneNumber(trim(request.getPhoneNumber()))
                .email(email)
                .address(address)
                .homeArea(homeAreaAndTenant.getLeft())
                .homeTenant(homeAreaAndTenant.getRight())
                .build();
    }

    private String checkAndNormalizeEmail(String email) {
        String normalizedEmail = emailAddressVerificationService.ensureCorrectEmailSyntaxAndNormalize(email);
        //check early if the email address is already used, this is less expensive than checking it in the event processor
        if (personService.existsPersonByEmail(normalizedEmail)) {
            throw new EMailAlreadyUsedException(email);
        }
        return normalizedEmail;
    }

    private void checkOauthId(String oauthId) {
        if (StringUtils.isEmpty(oauthId)) {
            throw PersonInformationInvalidException.forMissingField("oauthId");
        }
        if (personService.existsPersonByOauthIdNotDeleted(oauthId)) {
            throw new PersonAlreadyExistsException("The person with oauthId '{}' already exists.", oauthId);
        }
    }

    private Pair<GeoArea, Tenant> getHomeAreaAndTenant(String homeAreaId, String homeTenantId) {
        Tenant homeTenant = null;
        GeoArea homeArea = null;
        if (StringUtils.isNotEmpty(homeAreaId)) {
            homeArea = geoAreaService.findGeoAreaById(homeAreaId);
        }
        if (StringUtils.isNotEmpty(homeTenantId)) {
            homeTenant = tenantService.findTenantById(homeTenantId);
        }
        if (homeArea != null) {
            personService.checkIsValidHomeArea(homeArea);
            homeTenant = homeArea.getTenant();
        }
        if (homeArea == null && homeTenant == null) {
            throw PersonInformationInvalidException.forMissingBothHomeAreaAndHomeTenant();
        }
        return Pair.of(homeArea, homeTenant);
    }

    @ApiOperation(value = "Changes a users password",
            notes = "Internally the user is tried to log in to Auth0 with the old password. If the login was successful, " +
                    "the current password of the user is changed to the new password. " +
                    "This only works for username-password accounts. Use /person/loginHint to determine if the change is possible.")
    @ApiException(value = ClientExceptionType.PASSWORD_CHANGE_NOT_POSSIBLE,
            reason = "The password could not be changed because there is no username-password authentication for this user.")
    @ApiException(value = ClientExceptionType.PASSWORD_WRONG,
            reason = "The password could not be changed because the old password was wrong.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("personChangePasswordRequest")
    public void onPersonChangePasswordRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
            ClientPersonChangePasswordRequest request
    ) {

        final Person person = getCurrentPersonNotNull();

        oauthManagementService.changePasswordAndVerifyOldPassword(
                person.getOauthId(),
                person.getEmail(),
                request.getOldPassword(),
                request.getNewPassword());
    }

    @ApiOperation(value = "Update own person",
            notes = "Updates the own person. To manipulate an address use the address list endpoints. " +
                    "To change the email address use the separate change email request.")
    @ApiExceptions({
            ClientExceptionType.PERSON_INFORMATION_INVALID})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("personUpdateRequest")
    @SuppressFBWarnings(value = "RV_RETURN_VALUE_IGNORED_NO_SIDE_EFFECT",
            justification = "There is this hack to query for the size of the addresses to avoid lazy loading")
    public ClientPersonUpdateResponse onPersonUpdateRequest(
            @Valid @RequestBody
            @ApiParam(value = "The updated details of the own person.",
                    required = true)
            ClientPersonUpdateRequest request
    ) {

        Person oldPerson = getCurrentPersonNotNull();

        String firstName = beautifyAndCheck(request.getFirstName(), "firstName");
        String lastName = beautifyAndCheck(request.getLastName(), "lastName");
        String nickName = trim(request.getNickName());
        String phoneNumber = trim(request.getPhoneNumber());

        PersonUpdateRequest personUpdateRequest = PersonUpdateRequest.builder()
                .person(oldPerson)
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .build();

        PersonUpdateConfirmation personUpdateConfirmation =
                notifyAndWaitForReply(PersonUpdateConfirmation.class, personUpdateRequest);
        ClientPersonOwn clientPerson =
                personClientModelMapper.createClientPersonOwn(personUpdateConfirmation.getPerson());
        return ClientPersonUpdateResponse.builder()
                .updatedPerson(clientPerson)
                .build();
    }

    @ApiOperation(value = "Update own person extended",
            notes = "Updates all fields of the own person, including home area and profile picture." +
                    "To manipulate an address use the address list endpoints. " +
                    "To change the email address use the separate change email request.")
    @ApiExceptions({
            ClientExceptionType.PERSON_INFORMATION_INVALID})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("personExtendedUpdateRequest")
    @SuppressFBWarnings(value = "RV_RETURN_VALUE_IGNORED_NO_SIDE_EFFECT",
            justification = "There is this hack to query for the size of the addresses to avoid lazy loading")
    public ClientPersonExtendedUpdateResponse onPersonExtendedUpdateRequest(
            @Valid @RequestBody
            @ApiParam(value = "The updated details of the own person.",
                    required = true)
            ClientPersonExtendedUpdateRequest request
    ) {

        final Person oldPerson = getCurrentPersonNotNull();

        final String firstName = beautifyAndCheck(request.getFirstName(), "firstName");
        final String lastName = beautifyAndCheck(request.getLastName(), "lastName");
        final String nickName = trim(request.getNickName());
        final String phoneNumber = trim(request.getPhoneNumber());
        final GeoArea homeArea = geoAreaService.findGeoAreaById(request.getHomeAreaId());
        final ClientMediaItemPlaceHolder profilePicture = request.getProfilePicture();
        final PersonExtendedUpdateRequest personExtendedUpdateRequest = PersonExtendedUpdateRequest.builder()
                .person(oldPerson)
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .profilePicture(clientMediaItemService.toItemPlaceHolder(profilePicture, oldPerson.getProfilePicture(),
                        oldPerson))
                .homeArea(homeArea)
                .build();

        final PersonExtendedUpdateConfirmation personExtendedUpdateConfirmation =
                notifyAndWaitForReply(PersonExtendedUpdateConfirmation.class, personExtendedUpdateRequest);
        final ClientPersonOwn clientPerson =
                personClientModelMapper.createClientPersonOwn(personExtendedUpdateConfirmation.getPerson());
        return ClientPersonExtendedUpdateResponse.builder()
                .updatedPerson(clientPerson)
                .build();
    }

    @ApiOperation(value = "Change own email address",
            notes = """
                    Changes the own email address and resets the verification status. Currently only possible for username-password accounts, not for social media accounts.
                    There is a time restriction on how often the email address can be changed. It is currently set to 4h.
                    For technical reasons this time restriction is coupled to the sending of email verification emails. This means that if no email verification is enabled (for the tenant of the person) there is no time restriction for email changes. Additionally it means that a re-send of the email verification email resets the timer for email changes, too.""")
    @ApiExceptions({
            ClientExceptionType.EMAIL_ALREADY_REGISTERED,
            ClientExceptionType.EMAIL_ADDRESS_INVALID,
            ClientExceptionType.EMAIL_CHANGE_NOT_POSSIBLE})
    @ApiException(value = ClientExceptionType.EMAIL_CHANGE_NOT_POSSIBLE,
            reason = "The account was no username-password account")
    @ApiException(value = ClientExceptionType.EMAIL_CHANGE_NOT_POSSIBLE,
            reason = "The time limit for changing the email is not elapsed")
    @ApiException(value = ClientExceptionType.EMAIL_CHANGE_NOT_POSSIBLE,
            reason = "There was an unexpected error at the authentication provider")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("personChangeEmailAddressRequest")
    @SuppressFBWarnings(value = "RV_RETURN_VALUE_IGNORED_NO_SIDE_EFFECT",
            justification = "There is this hack to query for the size of the addresses to avoid lazy loading")
    public ClientPersonChangeEmailAddressConfirmation onPersonChangeEmailAddressRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
            ClientPersonChangeEmailAddressRequest request
    ) {

        Person person = getCurrentPersonNotNull();
        String newEmailAddress =
                checkAttributeNotNullOrEmpty(request.getNewEmailAddress(), "newEmailAddress");
        String normalizedEmail = checkAndNormalizeEmail(newEmailAddress);

        // check early if the email address is already used, this is less expensive than checking it in the event processor
        if (StringUtils.equalsIgnoreCase(person.getEmail(), newEmailAddress) ||
                personService.existsPersonByEmail(normalizedEmail)) {
            throw new EMailAlreadyUsedException(newEmailAddress);
        }

        PersonChangeEmailAddressConfirmation personChangeEmailAddressConfirmation =
                notifyAndWaitForReply(PersonChangeEmailAddressConfirmation.class,
                        PersonChangeEmailAddressRequest.builder()
                                .person(person)
                                .newEmailAddress(normalizedEmail)
                                .build());
        ClientPersonOwn clientPerson =
                personClientModelMapper.createClientPersonOwn(personChangeEmailAddressConfirmation.getPerson());
        return ClientPersonChangeEmailAddressConfirmation.builder()
                .person(clientPerson)
                .build();
    }

    @ApiOperation(value = "Cancel the change to pending new email address",
            notes = "Cancel the change to the pending new email address. The current mail address is continued to be used and the pending new email address is deleted.")
    @ApiException(value = ClientExceptionType.PERSON_HAS_NO_PENDING_NEW_EMAIL,
            reason = "Thrown if the person is currently not changing the email address and thus has no pending new email.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("personCancelChangeEmailAddressRequest")
    public void onPersonCancelChangeEmailAddressRequest() {

        final Person person = getCurrentPersonNotNull();
        if (StringUtils.isEmpty(person.getPendingNewEmail())) {
            throw new PersonHasNoPendingNewEmailException(
                    "Person '{}' is currently not changing the email address and has no pending new email address",
                    person.getId());
        }

        notifyAndWaitForReply(PersonCancelChangeEmailAddressConfirmation.class,
                PersonCancelChangeEmailAddressRequest.builder()
                        .person(person)
                        .build());
    }

    @Deprecated
    @ApiOperation(value = "Changes the tenant a person belongs to",
            notes = "Change the home tenant a person belongs to by specifying the new tenant. " +
                    "This should not be used if the home area is known!")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("personChangeCommunityRequest")
    public ClientPersonChangeCommunityResponse onPersonPersonChangeCommunityRequest(
            @Valid @RequestBody
            @ApiParam(
                    value = "The new tenant the person should be part of. This is the tenant the person lives in.",
                    required = true)
            ClientPersonChangeCommunityRequest request
    ) {

        Person person = getCurrentPersonNotNull();

        Tenant newHomeTenant = tenantService.findTenantById(request.getNewHomeCommunityId());

        PersonChangeHomeTenantRequest personChangeCommunityRequest = PersonChangeHomeTenantRequest.builder()
                .person(person)
                .oldHomeTenant(person.getTenant())
                .newHomeTenant(newHomeTenant)
                .build();

        PersonChangeHomeTenantConfirmation personChangeHomeTenantConfirmation =
                notifyAndWaitForReply(PersonChangeHomeTenantConfirmation.class, personChangeCommunityRequest);
        return ClientPersonChangeCommunityResponse.builder()
                .personId(person.getId())
                .newHomeCommunityId(personChangeHomeTenantConfirmation.getNewHomeTenant().getId())
                .build();
    }

    @ApiOperation(value = "Changes the home area of a person",
            notes = "Change the home area of a person by specifying the new home area.")
    @ApiExceptions({
            ClientExceptionType.GEO_AREA_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("personChangeHomeAreaRequest")
    public ClientPersonChangeHomeAreaResponse onPersonPersonChangeHomeAreaRequest(
            @Valid @RequestBody
            @ApiParam(value = "The new home area of the person. This is the area the person lives in.", required = true)
            ClientPersonChangeHomeAreaRequest request
    ) {

        final Person person = getCurrentPersonNotNull();

        final GeoArea newHomeArea = geoAreaService.findGeoAreaById(request.getNewHomeAreaId());

        final PersonChangeHomeAreaRequest personChangeHomeAreaRequest = PersonChangeHomeAreaRequest.builder()
                .person(person)
                .newHomeArea(newHomeArea)
                .build();

        final PersonChangeHomeAreaConfirmation changeHomeAreaConfirmation =
                notifyAndWaitForReply(PersonChangeHomeAreaConfirmation.class, personChangeHomeAreaRequest);
        return ClientPersonChangeHomeAreaResponse.builder()
                .personId(person.getId())
                .homeAreaId(changeHomeAreaConfirmation.getNewHomeArea().getId())
                .build();
    }

    @ApiOperation(
            value = "Checks via oauthId if a person exists in backend and, if not, returns a registration token for this oauthId.",
            notes = "The registration token can be used to register a new person in the backend")
    @ApiExceptions({
            ClientExceptionType.UNSPECIFIED_BAD_REQUEST,
            ClientExceptionType.UNSPECIFIED_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            notes = "API key is configured in oauth.api-key-for-registration-token")
    @PostMapping("personRegistrationTokenRequest")
    public ClientPersonRegistrationTokenResponse onPersonRegistrationTokenRequest(
            @Valid @RequestBody
            @ApiParam(value = "a request with the person's oauthId and email", required = true)
            ClientPersonRegistrationTokenRequest clientPersonRegistrationTokenRequest
    ) {
        final String oauthId = trimAndCheck(clientPersonRegistrationTokenRequest.getOauthId(), "oauthId");
        final String email = trimAndCheck(clientPersonRegistrationTokenRequest.getEmail(), "email");

        //call must be executed by authorized client only
        validateApiKey(getConfig().getOauth()::getApiKeyForRegistrationToken, getApiKeyNotNull(),
                "oauth.api-key-for-registration-token");

        if (personService.existsPersonByOauthIdNotDeleted(oauthId) || personService.existsPersonByEmail(email)) {
            return ClientPersonRegistrationTokenResponse.builder()
                    .personExists(true)
                    .token(null)
                    .build();
        }

        final String token = authorizationService.generateAuthToken(PersonRegistrationRequest.builder()
                .email(email)
                .oauthId(oauthId)
                .build(), Duration.ofMinutes(5));
        return ClientPersonRegistrationTokenResponse.builder()
                .personExists(false)
                .token(token)
                .build();
    }

    @ApiOperation(value = "Signals that a user in status PENDING_DELETION is still active",
            notes = "Sets the value of 'lastLoggedIn' to now and changes the status PENDING_DELETION " +
                    "so that the user does not get deleted. This is called from the webapp that is " +
                    "linked in the inactivity warning email.")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.ONE_TIME_TOKEN, notes = "Authorization is done via mail token")
    @PostMapping(value = "personResetLastLoggedInRequest/mail")
    public void resetLastLoggedInRequestMail(
            @RequestParam String mailToken,
            @Valid @RequestBody ClientPersonResetLastLoggedInRequest clientRequest) {

        final Person person = getPersonService().findPersonById(clientRequest.getPersonId());
        addAdditionalLogValue("personId", person.getId());
        final PersonResetLastLoggedInRequest request = new PersonResetLastLoggedInRequest(person);

        authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, request);

        notify(request);
    }

    @ApiOperation(value = "Request a new email verification mail",
            notes = "Resends the verification mail in order that a user can verify its email address")
    @ApiExceptions({
            ClientExceptionType.RESEND_VERIFICATION_EMAIL_NOT_POSSIBLE})
    @ApiException(value = ClientExceptionType.FEATURE_NOT_ENABLED,
            reason = "Only enabled with PersonEmailVerificationFeature")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping(value = "personResendVerificationEmailRequest")
    public ClientPersonResendVerificationEmailResponse onPersonResendVerificationEmailRequest() {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        getFeatureService().checkFeatureEnabled(PersonEmailVerificationFeature.class,
                FeatureTarget.of(person, appVariant));

        final PersonResendVerificationEmailRequest personResendVerificationEmailRequest =
                PersonResendVerificationEmailRequest.builder()
                        .person(person)
                        .build();

        final PersonResendVerificationEmailConfirmation personResendVerificationEmailConfirmation =
                notifyAndWaitForReply(PersonResendVerificationEmailConfirmation.class,
                        personResendVerificationEmailRequest);

        return ClientPersonResendVerificationEmailResponse.builder()
                .person(personClientModelMapper.createClientPersonReferenceWithEmail(
                        personResendVerificationEmailConfirmation.getPerson()))
                .build();
    }

    @ApiOperation(value = "Signals that a user wants to verify its email address",
            notes = "Sets the verification status EMAIL_VERIFIED of the user")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.ONE_TIME_TOKEN, notes = "Authorization is done via mail token")
    @PostMapping(value = "personVerifyEmailRequest/mail")
    public ResponseEntity<ClientPersonVerifyEmailAddressResponse> onPersonVerifyEmailRequest(
            @RequestParam String mailToken,
            @Valid @RequestBody ClientPersonVerifyEmailAddressRequest clientRequest) {

        final Person person = getPersonService().findPersonById(clientRequest.getPersonId());
        addAdditionalLogValue("personId", person.getId());
        final String emailAddress =
                StringUtils.isEmpty(person.getPendingNewEmail()) ? person.getEmail() : person.getPendingNewEmail();

        final PersonChangeEmailAddressVerificationStatusRequest personChangeEmailAddressVerificationStatusRequest =
                PersonChangeEmailAddressVerificationStatusRequest.builder()
                        .person(person)
                        .emailAddress(emailAddress)
                        .build();

        final TokenAuthorizationStatus tokenAuthorizationStatus =
                authorizationService.checkAuthTokenAuthorizedForEvent(mailToken,
                        personChangeEmailAddressVerificationStatusRequest);

        // send email verify request only when the token authorization status is valid
        if (tokenAuthorizationStatus.equals(TokenAuthorizationStatus.TOKEN_VALID)) {

            final PersonChangeEmailAddressVerificationStatusConfirmation
                    personChangeEmailAddressVerificationStatusConfirmation =
                    notifyAndWaitForReply(PersonChangeEmailAddressVerificationStatusConfirmation.class,
                            personChangeEmailAddressVerificationStatusRequest);

            return new ResponseEntity<>(ClientPersonVerifyEmailAddressResponse.builder()
                    .personWithVerifiedEmail(personClientModelMapper.createClientPersonReferenceWithEmail(
                            personChangeEmailAddressVerificationStatusConfirmation.getPerson()))
                    .verificationStatusChanged(
                            personChangeEmailAddressVerificationStatusConfirmation.isVerificationStatusChanged())
                    .success(true)
                    .message("E-Mail-Adresse '" + emailAddress + "' wurde erfolgreich verifiziert.")
                    .build(), HttpStatus.OK);
        }

        String message;
        switch (tokenAuthorizationStatus) {
            case TOKEN_EXPIRED:
                message =
                        "Die E-Mail ist leider schon zu alt, bitte fordere in der App eine neue E-Mail zur Bestätigung deiner Adresse an.";
                break;
            case TOKEN_DOES_NOT_MATCH_ENDPOINT:
                message =
                        "Hier stimmt etwas nicht, bitte fordere in der App eine neue E-Mail zur Bestätigung deiner Adresse an.";
                break;
            case TOKEN_VALUES_DO_NOT_MATCH:
                message =
                        "Die E-Mail ist leider schon zu alt und war für die Bestätigung einer anderen Adresse gedacht. Bitte fordere in der App eine neue E-Mail zur Bestätigung deiner Adresse an.";
                break;
            case TOKEN_INVALID:
                message =
                        "Hier stimmt etwas nicht. Probiere den Link aus der E-Mail manuell zu kopieren (Rechtsklick, Link kopieren) und in den Browser einzufügen. Wenn auch das nicht klappt, fordere bitte in der App eine neue E-Mail zur Bestätigung deiner Adresse an.";
                break;
            default:
                log.warn("Unexpected token authorization status was found '{}'", tokenAuthorizationStatus);
                message =
                        "Hier stimmt etwas nicht, bitte fordere in der App eine neue E-Mail zur Bestätigung deiner Adresse an.";
        }

        return new ResponseEntity<>(ClientPersonVerifyEmailAddressResponse.builder()
                .verificationStatusChanged(false)
                .success(false)
                .message(message)
                .build(), HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "Changes the profile picture of the current user",
            notes = "Updates the profile picture of the current user by replacing it with the new picture")
    @ApiExceptions({
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("personProfilePictureChangeRequest")
    ClientPersonProfilePictureChangeConfirmation onPersonProfilePictureChangeRequest(
            @Valid @RequestBody
            ClientPersonProfilePictureChangeRequest request
    ) {

        final Person person = getCurrentPersonNotNull();
        final TemporaryMediaItem newProfilePicture =
                mediaItemService.findTemporaryItemById(person, request.getTemporaryMediaItemId());

        final PersonProfilePictureChangeConfirmation personProfilePictureChangeConfirmation =
                notifyAndWaitForReply(PersonProfilePictureChangeConfirmation.class,
                        PersonProfilePictureChangeRequest.builder()
                                .person(person)
                                .newProfilePicture(newProfilePicture)
                                .build());

        return ClientPersonProfilePictureChangeConfirmation.builder()
                .newProfilePicture(fileClientModelMapper.toClientMediaItem(
                        personProfilePictureChangeConfirmation.getPerson().getProfilePicture()))
                .build();
    }

    @ApiOperation(value = "Deletes the profile picture of the current user",
            notes = "The request is idempotent and will not fail if there is no profile picture set.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("personProfilePictureDeleteRequest")
    ClientPersonProfilePictureDeleteConfirmation onPersonProfilePictureDeleteRequest() {

        final Person person = getCurrentPersonNotNull();

        final PersonProfilePictureDeleteConfirmation personProfilePictureDeleteConfirmation =
                notifyAndWaitForReply(PersonProfilePictureDeleteConfirmation.class,
                        PersonProfilePictureDeleteRequest.builder()
                                .person(person)
                                .build());

        return ClientPersonProfilePictureDeleteConfirmation.builder()
                .personId(personProfilePictureDeleteConfirmation.getPerson().getId())
                .build();
    }

    private static String trim(String ugly) {
        return StringUtils.trim(ugly);
    }

    private static String nice(String ugly) {
        if (ugly == null) {
            return null;
        }
        return StringUtils.capitalize(ugly.trim());
    }

    private static String beautifyAndCheck(String input, String inputName) {
        final String result = nice(input);
        return check(result, inputName);
    }

    private static String trimAndCheck(String input, String inputName) {
        final String result = trim(input);
        return check(result, inputName);
    }

    private static String check(String result, String inputName) {
        if (StringUtils.isEmpty(result)) {
            throw PersonInformationInvalidException.forMissingField(inputName);
        }
        return result;
    }

}
