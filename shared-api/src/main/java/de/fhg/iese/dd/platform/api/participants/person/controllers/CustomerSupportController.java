/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Adeline Silva Schäfer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientContactPerson;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.ContactPersonNotFoundException;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.CommonHelpDesk;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.CommunityHelpDesk;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/support")
@Api(tags = {"participants.support"})
public class CustomerSupportController extends BaseController {

    @Autowired
    private IRoleService roleService;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;

    @ApiOperation(value = "Returns the default contact person",
            notes = "Returns the default contact person for the home geo area of the person or an error if none is defined.")
    @ApiExceptions({
            ClientExceptionType.CONTACT_PERSON_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/contacts/default")
    public ClientContactPerson getDefaultContactPerson() {

        return getAllContactPersons(getCurrentPersonNotNull()).stream()
                //if we have multiple, we take the newest created
                .max(Comparator.comparingLong(BaseEntity::getCreated))
                .map(personClientModelMapper::createClientContactPerson)
                .orElse(null); //can never be null, since the returned list is never empty
    }

    @ApiOperation(value = "Returns all contact persons",
            notes = "Returns all contact persons for the home geo area of the person, might be none or multiple.")
    @ApiExceptions({
            ClientExceptionType.CONTACT_PERSON_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/contacts")
    public List<ClientContactPerson> getContactPersons() {

        return getAllContactPersons(getCurrentPersonNotNull()).stream()
                .map(personClientModelMapper::createClientContactPerson)
                .collect(Collectors.toList());
    }

    private List<Person> getAllContactPersons(Person currentPerson) {

        Tenant tenant = currentPerson.getTenant();

        List<Person> contactPersons = roleService.getAllPersonsWithRoleForEntity(CommunityHelpDesk.class, tenant);
        if (CollectionUtils.isEmpty(contactPersons)) {
            contactPersons = roleService.getAllPersonsWithRole(CommonHelpDesk.class);
            if (CollectionUtils.isEmpty(contactPersons)) {
                throw new ContactPersonNotFoundException("No contact person defined");
            }
        }
        return contactPersons;
    }

}
