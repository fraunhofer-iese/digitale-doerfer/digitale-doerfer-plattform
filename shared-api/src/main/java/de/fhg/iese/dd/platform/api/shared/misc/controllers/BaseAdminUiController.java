/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Johannes Schneider, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.misc.controllers;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonExtended;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthAccountNotFoundException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthManagementException;
import de.fhg.iese.dd.platform.business.shared.security.services.IOauthManagementService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;

public abstract class BaseAdminUiController extends BaseController {

    @Autowired
    protected IPersonService personService;
    @Autowired
    protected IOauthManagementService oauthManagementService;
    @Autowired
    protected PersonClientModelMapper personClientModelMapper;

    protected ClientPersonExtended retrieveOauthAccountAndConvertToClientPerson(Person person) {

        OauthAccount oauthAccount = null;
        String failedReason = null;
        try {
            oauthAccount = oauthManagementService.queryUserByOauthId(person.getOauthId());
        } catch (OauthAccountNotFoundException ex) {
            failedReason = String.format("No OAuth account found for user with oauth id %s", person.getOauthId());
        } catch (OauthManagementException ex) {
            failedReason = ex.getMessage();
        }
        return personClientModelMapper.createClientPersonExtended(
                personService.fetchPersonExtendedAttributes(person), oauthAccount, failedReason);
    }

}
