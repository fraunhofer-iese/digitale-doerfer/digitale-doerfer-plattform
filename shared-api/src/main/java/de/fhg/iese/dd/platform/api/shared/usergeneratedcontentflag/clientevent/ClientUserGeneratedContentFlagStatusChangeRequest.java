/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientUserGeneratedContentFlagStatusChangeRequest extends ClientBaseEvent {

    @NotBlank
    @ApiModelProperty(value = "ID of the user generated content flag", required = true)
    private String flagId;

    @NotBlank
    @ApiModelProperty(value = "comment on why this status change has happened", required = true)
    private String comment;

    @NotNull
    @ApiModelProperty(value = "new status of the user generated content flag", required = true)
    private UserGeneratedContentFlagStatus newStatus;

}
