/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagDeleteFlagEntityByAdminResponse;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagStatusChangeRequest;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagStatusChangeResponse;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.UserGeneratedContentFlagClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagDeleteFlagEntityByAdminConfirmation;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagDeleteFlagEntityByAdminRequest;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagStatusChangeConfirmation;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagStatusChangeRequest;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.security.ChangeUserGeneratedContentFlagAction;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.security.DeleteUserGeneratedContentFlagEntityAction;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("adminui/flag/event")
@Api(tags = {"shared.adminui.flag.events"})
public class UserGeneratedContentFlagAdminUiEventController extends BaseController {

    @Autowired
    private IUserGeneratedContentFlagService userGeneratedContentFlagService;
    @Autowired
    private UserGeneratedContentFlagClientModelMapper userGeneratedContentFlagClientModelMapper;

    @ApiOperation("Change the status of a user generated content flag")
    @ApiExceptions({
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ChangeUserGeneratedContentFlagAction.class})
    @PostMapping("flagStatusChangeRequest")
    public ClientUserGeneratedContentFlagStatusChangeResponse onFlagStatusChangeRequest(
            @Valid @RequestBody final ClientUserGeneratedContentFlagStatusChangeRequest clientRequest) {

        final Person currentPerson = getCurrentPersonNotNull();

        final UserGeneratedContentFlag flag = userGeneratedContentFlagService.findById(clientRequest.getFlagId());

        PermissionTenantRestricted changeFlagPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ChangeUserGeneratedContentFlagAction.class, currentPerson);

        if (changeFlagPermission.isTenantDenied(flag.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to change flag {}", flag.getId());
        }

        final UserGeneratedContentFlagStatusChangeConfirmation response =
                notifyAndWaitForReply(UserGeneratedContentFlagStatusChangeConfirmation.class,
                        UserGeneratedContentFlagStatusChangeRequest.builder()
                                .flag(flag)
                                .initiator(currentPerson)
                                .comment(clientRequest.getComment())
                                .newStatus(clientRequest.getNewStatus())
                                .build());

        return new ClientUserGeneratedContentFlagStatusChangeResponse(
                userGeneratedContentFlagClientModelMapper.toClientUserGeneratedContentFlagDetail(
                        response.getChangedFlag()));
    }

    @ApiOperation(value = "Delete the entity of user generated content flag",
            notes = "Deletes the entity of a user generated content flag and sets the status to ACCEPTED.")
    @ApiExceptions({
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_NOT_FOUND,
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_INVALID,
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_ENTITY_CAN_NOT_BE_DELETED,
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_ENTITY_DELETION_FAILED})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {DeleteUserGeneratedContentFlagEntityAction.class})
    @PostMapping("deleteFlagEntityByAdminRequest")
    public ClientUserGeneratedContentFlagDeleteFlagEntityByAdminResponse onDeleteFlagEntityRequest(
            @Valid @RequestBody final ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest clientRequest) {

        final Person currentPerson = getCurrentPersonNotNull();

        final UserGeneratedContentFlag flag = userGeneratedContentFlagService.findById(clientRequest.getFlagId());

        PermissionTenantRestricted deleteFlagEntityPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(DeleteUserGeneratedContentFlagEntityAction.class, currentPerson);

        if (deleteFlagEntityPermission.isTenantDenied(flag.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to delete the entity of flag {}", flag.getId());
        }

        final UserGeneratedContentFlagDeleteFlagEntityByAdminConfirmation response =
                notifyAndWaitForReply(UserGeneratedContentFlagDeleteFlagEntityByAdminConfirmation.class,
                        UserGeneratedContentFlagDeleteFlagEntityByAdminRequest.builder()
                                .flag(flag)
                                .initiator(currentPerson)
                                .comment(clientRequest.getComment())
                                .build());

        return new ClientUserGeneratedContentFlagDeleteFlagEntityByAdminResponse(
                userGeneratedContentFlagClientModelMapper.toClientUserGeneratedContentFlagDetail(
                        response.getChangedFlag()));
    }

}
