/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.clientmodel;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientAbsoluteImageCropDefinition {

    @ApiModelProperty("x coordinate of the top left corner of the cropped image, must be positive or zero")
    @PositiveOrZero(message = "absolute x offset must not be negative!")
    private int absoluteOffsetX;
    @ApiModelProperty("y coordinate of the top left corner of the cropped image, must be positive or zero")
    @PositiveOrZero(message = "absolute y offset must not be negative!")
    private int absoluteOffsetY;
    @ApiModelProperty("width of the cropped image, must be positive")
    @Positive(message = "absolute width must not be negative or zero!")
    private int absoluteWidth;
    @ApiModelProperty("height of the cropped image, must be positive")
    @Positive(message = "absolute height must not be negative or zero!")
    private int absoluteHeight;

}
