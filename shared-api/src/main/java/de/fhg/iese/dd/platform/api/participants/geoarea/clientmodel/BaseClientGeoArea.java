/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel;

import java.util.List;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRawValue;

import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItem;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.enums.GeoAreaType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@SuperBuilder
@NoArgsConstructor
//custom ordering for better readability at debugging
@JsonPropertyOrder(value = {"id", "name", "geoAreaType", "depth", "leaf", "boundaryPoints", "zip", "center",
        "customAttributesJson", "logo", "children"})
public abstract class BaseClientGeoArea<C extends BaseClientGeoArea<C>> extends ClientBaseEntity {

    private String name;

    private GeoAreaType geoAreaType;

    private ClientMediaItem logo;

    @JsonRawValue
    @ApiModelProperty(dataType = "java.util.Map")
    private String geoJson;

    @Nullable
    @ApiModelProperty(value = "List of matching zip codes. Might be null. If more than one, they are separated by ','.",
            allowEmptyValue = true)
    private String zip;

    private ClientGPSLocation center;

    @Nullable
    @ApiModelProperty(value = "Indicates if a geo area is a leaf. Might be null", allowEmptyValue = true)
    private Boolean leaf;

    @Nullable
    @ApiModelProperty(value = "Depth in the global tree. This is *not* the depth in a returned subtree. " +
            "Root areas (e.g. Germany) are 0. Might be null", allowEmptyValue = true)
    private Integer depth;

    private List<C> childGeoAreas;

    @JsonRawValue
    @ApiModelProperty(dataType = "java.util.Map")
    private String customAttributesJson;

}
