/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.misc.clientevent;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientQrCodeLabelCreateRequest extends ClientBaseEvent {

    private String receiverName;
    private String receiverAddressName;
    private String receiverStreet;
    private String receiverCity;
    private String senderName;
    private String senderStreet;
    private String senderCity;
    private String transportNotes;
    private String contentNotes;
    private String trackingCode;
    private String qrCodeText;
    private int parcelCount;

}
