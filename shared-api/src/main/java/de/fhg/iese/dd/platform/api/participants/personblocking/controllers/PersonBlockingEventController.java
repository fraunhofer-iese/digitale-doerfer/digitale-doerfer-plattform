/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Stefan Schweitzer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.personblocking.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.participants.personblocking.clientevent.ClientPersonBlockRequest;
import de.fhg.iese.dd.platform.api.participants.personblocking.clientevent.ClientPersonUnBlockRequest;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.personblocking.events.PersonBlockConfirmation;
import de.fhg.iese.dd.platform.business.participants.personblocking.events.PersonBlockRequest;
import de.fhg.iese.dd.platform.business.participants.personblocking.events.PersonUnBlockConfirmation;
import de.fhg.iese.dd.platform.business.participants.personblocking.events.PersonUnBlockRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("person/event")
@Api(tags={"participants.person.block.events"})
public class PersonBlockingEventController extends BaseController {

    @Autowired
    private IPersonService personService;

    @ApiOperation("Block a person")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.BLOCKED_PERSONS_LIMIT_EXCEEDED})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("personBlockRequest")
    public void personBlockRequest(
            @Valid @RequestBody
            @ApiParam(value = "The person to block", required = true) final ClientPersonBlockRequest clientRequest) {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();
        final App app = appVariant.getApp();

        final Person blockedPerson = personService.findPersonById(clientRequest.getPersonId());

        final PersonBlockRequest request = PersonBlockRequest.builder()
                .app(app)
                .blockingPerson(person)
                .blockedPerson(blockedPerson)
                .build();
        notifyAndWaitForReply(PersonBlockConfirmation.class, request);
    }

    @ApiOperation("Unblock a person")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("personUnBlockRequest")
    public void personUnBlockRequest(
            @Valid @RequestBody
            @ApiParam(value = "The person to unblock", required = true)
            final ClientPersonUnBlockRequest clientRequest) {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();
        final App app = appVariant.getApp();

        final Person blockedPerson = personService.findPersonById(clientRequest.getPersonId());

        final PersonUnBlockRequest request = PersonUnBlockRequest.builder()
                .app(app)
                .blockingPerson(person)
                .blockedPerson(blockedPerson)
                .build();
        notifyAndWaitForReply(PersonUnBlockConfirmation.class, request);
    }

}
