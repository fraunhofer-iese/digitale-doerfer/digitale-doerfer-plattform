/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.clientmodel;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientDocumentItemPlaceHolder implements IClientFileItemPlaceHolder {

    private String documentItemId;

    private String temporaryDocumentItemId;

    @Override
    @JsonIgnore
    public String getItemId() {
        return documentItemId;
    }

    @Override
    @JsonIgnore
    public String getTemporaryItemId() {
        return temporaryDocumentItemId;
    }
    
    @Override
    public String toString() {
        return "ClientDocumentItemPlaceHolder ["
                + (documentItemId != null ? "documentItemId=" + documentItemId + ", " : "")
                + (temporaryDocumentItemId != null ? "temporaryDocumentItemId=" + temporaryDocumentItemId : "")
                + "]";
    }

}
