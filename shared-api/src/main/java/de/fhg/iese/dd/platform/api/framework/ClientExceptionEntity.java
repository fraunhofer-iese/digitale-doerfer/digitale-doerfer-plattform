/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"type", "exception", "message", "detail", "path"})
public class ClientExceptionEntity {

    @ApiModelProperty(notes = "UTC timestamp when the error occurred")
    private final long timestamp;
    @JsonIgnore
    private HttpStatus status;
    @ApiModelProperty(notes = "Class name of the internal exception (Do not rely on them, they change without notice)")
    private final String exception;
    @ApiModelProperty(notes = "Developer readable message why the error occurred")
    private final String message;
    @ApiModelProperty(notes = "Request path that was called")
    private final String path;
    @ApiModelProperty(notes = "Type of error that was thrown (The types are only extended, never changed)")
    private ClientExceptionType type;
    @ApiModelProperty(notes = "Machine readable specification about the cause of the error " +
            "(Id of requested entity, name of the field, name of the configuration entry)")
    private final String detail;

    @JsonProperty("error")
    @ApiModelProperty(notes = "Developer readable reason phrase of the http status")
    public String getErrorString() {
        return status.getReasonPhrase();
    }

    @JsonProperty("status")
    @ApiModelProperty(notes = "Name of the http status")
    public String getStatusString() {
        return status.toString();
    }

    public void setType(ClientExceptionType type) {
        this.type = type;
        this.status = type.getHttpStatus();
    }

    public ClientExceptionEntity withType(ClientExceptionType type) {
        setType(type);
        return this;
    }

    @JsonIgnore
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ClientException [");
        if (path != null) {
            builder.append("path=\"");
            builder.append(path);
            builder.append("\", ");
        }
        if (status != null) {
            builder.append("status=\"");
            builder.append(status);
            builder.append("\", ");
        }
        if (exception != null) {
            builder.append("exception=\"");
            builder.append(exception);
            builder.append("\", ");
        }
        if (message != null) {
            builder.append("message=\"");
            builder.append(message);
            builder.append("\", ");
        }
        if (type != null) {
            builder.append("type=\"");
            builder.append(type);
            builder.append("\"");
        }
        if (detail != null) {
            builder.append(", detail=\"");
            builder.append(detail);
            builder.append("\"");
        }
        builder.append("]");
        return builder.toString();
    }

}
