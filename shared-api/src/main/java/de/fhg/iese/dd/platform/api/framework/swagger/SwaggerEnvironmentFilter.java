/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.swagger;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.util.ContentCachingResponseWrapper;

import de.fhg.iese.dd.platform.datamanagement.framework.services.IEnvironmentService;

public class SwaggerEnvironmentFilter implements Filter {

    private final IEnvironmentService environmentService;

    private String cssExtension;

    public SwaggerEnvironmentFilter(IEnvironmentService environmentService) {
        super();
        this.environmentService = environmentService;
    }

    @Override
    public void init(FilterConfig filterConfig) {
        //coloring
        String backgroundColor = environmentService.getEnvironmentInfoColor();

        //"watermarking"
        String watermarkSVG = "<svg width='400' height='600' xmlns='http://www.w3.org/2000/svg'>"
                + "<text "
                + "id='svg_1' "
                + "font-weight='bold' "
                + "transform='rotate(-85 250,250)' "
                + "xml:space='preserve' "
                + "text-anchor='middle' "
                + "font-family='Courier New, Courier, monospace' "
                + "font-size='90' "
                + "y='80' "
                + "x='200' "
                + "fill='lightgrey'>"
                + "<tspan>"
                + environmentService.getEnvironmentFullName()
                + "</tspan>"
                + "</text>"
                + "</svg>";
        watermarkSVG = watermarkSVG.replace("'", "\"").replace("<", "%3C").replace(">", "%3E");

        //dynamic css extension base on values of the environment
        cssExtension = "\n" +
                //color the top bar with the color of the environment and adjust its size
                ".topbar {\n" +
                "  background-color: " + backgroundColor + " !important;\n" +
                "  padding: 15px !important;\n" +
                "}\n" +
                "\n" +
                //insert the environment name in the top bar
                ".topbar-wrapper .link::after {\n" +
                "  content: '" + environmentService.getEnvironmentFullName() + "';\n" +
                "  width: 400px;\n" +
                "}\n" +
                "\n" +
                //add the watermark of the current environment
                "div.swagger-ui {\n" +
                "  background-image: url('data:image/svg+xml;utf8," + watermarkSVG + "');\n" +
                "  background-repeat: repeat-y;\n" +
                "  background-position-y: 50px;\n" +
                "}\n";
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (request instanceof HttpServletRequest) {
            ContentCachingResponseWrapper responseWrapper =
                    new ContentCachingResponseWrapper((HttpServletResponse) response);

            chain.doFilter(request, responseWrapper);

            String uri = ((HttpServletRequest) request).getRequestURI();

            if (uri.contains("swagger-ui.css")) {
                String originalCss = getOriginalContent(responseWrapper);
                responseWrapper.resetBuffer();

                //replace monospace font
                String replacedCss = StringUtils.replace(
                        originalCss,
                        "Source Code Pro",
                        "JetBrains Mono");
                responseWrapper.getWriter().write(replacedCss);
                //append dynamic css extensions
                responseWrapper.getWriter().write(cssExtension);
                responseWrapper.copyBodyToResponse();

            } else if (uri.contains("swagger-ui.html")) {
                String originalHtml = getOriginalContent(responseWrapper);

                //title
                String replacedHtml = StringUtils.replace(
                        originalHtml,
                        "<title>Swagger UI</title>",
                        "<title>" + environmentService.getEnvironmentFullName() + " DD API</title>\n" +
                                "<link rel=\"stylesheet\" type=\"text/css\" href=\"webjars/springfox-swagger-ui/swagger-custom.css\">");

                responseWrapper.resetBuffer();
                responseWrapper.getWriter().write(replacedHtml);
                responseWrapper.copyBodyToResponse();
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    private String getOriginalContent(ContentCachingResponseWrapper responseWrapper) throws UnsupportedEncodingException{
        return new String(responseWrapper.getContentAsByteArray(), responseWrapper.getCharacterEncoding());
    }

    @Override
    public void destroy() {}

}
