/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanExpressionContext;
import org.springframework.beans.factory.config.BeanExpressionResolver;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.expression.StandardBeanExpressionResolver;

// custom constraint annotations used in custom validators additionally require the following parameters:
// message, groups, payload (https://reflectoring.io/bean-validation-with-spring-boot/)
public abstract class BaseValidator {

    @Autowired
    private ConfigurableBeanFactory beanfactory;

    protected Object getConfigValue(String expression) {
        BeanExpressionResolver resolver = beanfactory.getBeanExpressionResolver();
        if (resolver == null) {
            resolver = new StandardBeanExpressionResolver();
        }
        return resolver.evaluate(expression, new BeanExpressionContext(beanfactory, null));
    }

}
