/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.clientmodel;

import java.util.List;
import java.util.Set;

import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientOauthAccount {

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @Builder
    @ToString
    public static class ClientOauthAccountDevice {

        private String clientId;
        private String clientName;
        private String deviceName;

    }

    private String oauthId;
    private String name;
    private Set<OauthAccount.AuthenticationMethod> authenticationMethods;
    private List<ClientOauthAccountDevice> devices;
    @ApiModelProperty("can be null")
    private Integer loginCount;
    @ApiModelProperty("not relevant anymore, use verificationStatuses of the person instead")
    private boolean emailVerified;
    private boolean blocked;
    private OauthAccount.BlockReason blockReason;
    private boolean retrievalFailed;
    private String failedReason;

}
