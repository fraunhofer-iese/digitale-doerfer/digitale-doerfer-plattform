/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.clientmodel;

import java.util.Comparator;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.shared.security.services.IOauthClientService;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;

@Component
public class SecurityClientModelMapper {

    @Autowired
    private IOauthClientService oauthClientService;

    public ClientOauthAccount toClientOauthAccount(OauthAccount oauthAccount) {

        if (oauthAccount == null) {
            return null;
        }
        final ClientOauthAccount clientOauthAccount = ClientOauthAccount.builder()
                .oauthId(oauthAccount.getOauthId())
                .name(oauthAccount.getName())
                .authenticationMethods(oauthAccount.getAuthenticationMethods())
                .loginCount(oauthAccount.getLoginCount())
                .emailVerified(oauthAccount.isEmailVerified())
                .blocked(oauthAccount.isBlocked())
                .blockReason(oauthAccount.getBlockReason())
                .retrievalFailed(false)
                .failedReason(null)
                .build();
        if (!CollectionUtils.isEmpty(oauthAccount.getDevices())) {
            clientOauthAccount.setDevices(oauthAccount.getDevices().stream()
                    .map(d -> ClientOauthAccount.ClientOauthAccountDevice.builder()
                            .clientId(d.getClientIdentifier())
                            //this service call is okay, since it only uses cached data
                            .clientName(oauthClientService.findByOauthClientIdentifierOptional(d.getClientIdentifier())
                                    .map(OauthClient::getName)
                                    .orElse("-"))
                            .deviceName(d.getDeviceName())
                            .build())
                    .sorted(Comparator.comparing(ClientOauthAccount.ClientOauthAccountDevice::getClientName)
                            .thenComparing(ClientOauthAccount.ClientOauthAccountDevice::getDeviceName))
                    .collect(Collectors.toList()));
        }
        return clientOauthAccount;
    }

}
