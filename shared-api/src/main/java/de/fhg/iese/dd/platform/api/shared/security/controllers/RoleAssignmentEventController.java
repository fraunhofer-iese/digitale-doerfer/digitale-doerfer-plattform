/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.security.clientevent.ClientCreateRoleAssignmentRequest;
import de.fhg.iese.dd.platform.api.shared.security.clientevent.ClientCreateRoleAssignmentResponse;
import de.fhg.iese.dd.platform.api.shared.security.clientevent.ClientRemoveRoleAssignmentRequest;
import de.fhg.iese.dd.platform.api.shared.security.clientevent.ClientRemoveRoleAssignmentResponse;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.RoleClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.security.events.CreateRoleAssignmentConfirmation;
import de.fhg.iese.dd.platform.business.shared.security.events.CreateRoleAssignmentRequest;
import de.fhg.iese.dd.platform.business.shared.security.events.RemoveRoleAssignmentConfirmation;
import de.fhg.iese.dd.platform.business.shared.security.events.RemoveRoleAssignmentRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/roleAssignment/event")
@Api(tags = {"participants.roleAssignment.events"})
public class RoleAssignmentEventController extends BaseController {

    @Autowired
    private IPersonService personService;
    @Autowired
    private RoleClientModelMapper roleClientModelMapper;
    
    @ApiOperation(value = "Assign a role to a person")
    @ApiExceptions({
            ClientExceptionType.ROLE_NOT_FOUND,
            ClientExceptionType.RELATED_ENTITY_NOT_FOUND,
            ClientExceptionType.RELATED_ENTITY_MUST_NOT_BE_NULL,
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.ROLE_MANAGEMENT_NOT_ALLOWED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuperAdmin.class, RoleManager.class, UserAdmin.class, GlobalUserAdmin.class},
            notes = """
                    If the person is super admin, it is allowed to manage the super admin role and all other roles for all entities.
                    If the person is global user admin, it is allowed to manage the global user admin role.
                    Moreover, any other person is allowed to manage a role for an entity if the person is role manager or user admin for the tenant that is affected by the related entity.\s
                    E.g. a person that is role manager for tenant x is allowed to manage the VG admin role of related entity x or the show owner role of a shop that belongs to tenant x.
                    """)
    @PostMapping("/createRoleAssignmentRequest")
    public ClientCreateRoleAssignmentResponse createRoleAssignment(
            @Valid @RequestBody ClientCreateRoleAssignmentRequest request) {

        final Person currentPerson = getCurrentPersonNotNull();
        final BaseRole<? extends NamedEntity> role = getRoleService().getRoleForKey(request.getRoleKey());
        final String relatedEntityId = request.getRelatedEntityId();
        getRoleService().checkIfRelatedEntityIdCanBeNull(role, relatedEntityId);
        getRoleService().checkIfPersonIsAllowedToCreateAssignmentOfRoleForEntity(currentPerson, role, relatedEntityId);

        final Person person = personService.findPersonById(request.getPersonId());

        CreateRoleAssignmentConfirmation confirmation = notifyAndWaitForReply(CreateRoleAssignmentConfirmation.class,
                new CreateRoleAssignmentRequest(role, person, relatedEntityId));

        return ClientCreateRoleAssignmentResponse.builder()
                .roleAssignment(roleClientModelMapper.toClientRoleAssignment(confirmation.getRoleAssignment()))
                .build();
    }

    @ApiOperation(value = "Remove a role from a person")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.ROLE_MANAGEMENT_NOT_ALLOWED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuperAdmin.class, RoleManager.class, UserAdmin.class, GlobalUserAdmin.class},
            notes = """
                    If the person is super admin, it is allowed to manage the super admin role and all other roles for all entities.
                    If the person is global user admin, it is allowed to manage the global user admin role.
                    Moreover, any other person is allowed to manage a role for an entity if the person is role manager or user admin for the tenant that is affected by the related entity.\s
                    E.g. a person that is role manager for tenant x is allowed to manage the VG admin role of related entity x or the show owner role of a shop that belongs to tenant x.
                    """)
    @PostMapping("/removeRoleAssignmentRequest")
    public ClientRemoveRoleAssignmentResponse removeRoleAssignment(
            @Valid @RequestBody ClientRemoveRoleAssignmentRequest request) {

        final Person currentPerson = getCurrentPersonNotNull();
        final RoleAssignment roleAssignment = getRoleService().getRoleAssignment(request.getRoleAssignmentId());
        getRoleService().checkIfPersonIsAllowedToDeleteRoleAssignment(currentPerson, roleAssignment);

        RemoveRoleAssignmentConfirmation response = notifyAndWaitForReply(RemoveRoleAssignmentConfirmation.class,
                new RemoveRoleAssignmentRequest(roleAssignment));
        return ClientRemoveRoleAssignmentResponse.builder()
                .roleAssignmentId(response.getRoleAssignment().getId())
                .build();
    }

}
