/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientTemporaryDocumentItem;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.DocumentConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemUploadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IDocumentItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/document")
@Api(tags = {"shared.document"})
public class DocumentItemController extends BaseController {

    @Autowired
    private IDocumentItemService documentItemService;
    @Autowired
    private FileClientModelMapper fileClientModelMapper;
    @Autowired
    private DocumentConfig documentConfig;

    @ApiOperation(value = "Uploads a temporary document item",
            notes = """
                    The uploaded document can be used by this user to create entities with document items. Use the returned id (NOT the id of the document item) in create requests.

                    The temporary files have an expiration date and are removed automatically afterwards""")
    @ApiExceptions({
            ClientExceptionType.FILE_ITEM_UPLOAD_FAILED,
            ClientExceptionType.TEMPORARY_FILE_ITEM_LIMIT_EXCEEDED})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/temp")
    public ClientTemporaryDocumentItem uploadTemporaryDocumentItem(
            @RequestParam MultipartFile file,

            @RequestParam(required = false)
            @ApiParam("Optional title of the document, if none is provided the filename is taken")
            String title,

            @RequestParam(required = false)
            @ApiParam("Optional description of the document")
            String description
    ) {

        if (file.getSize() >= documentConfig.getMaxUploadFileSize().toBytes()) {
            throw new FileItemUploadException("File size limit of {}MB for upload exceeded",
                    documentConfig.getMaxUploadFileSize().toMegabytes());
        }
        final Person owner = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        try {
            final TemporaryDocumentItem temporaryDocumentItem =
                    documentItemService.createTemporaryDocumentItem(file.getBytes(), file.getOriginalFilename(),
                            title, description,
                            FileOwnership.of(owner, appVariant));
            return fileClientModelMapper.toClientTemporaryDocumentItem(temporaryDocumentItem);
        } catch (IOException e) {
            log.error("Could not read file", e);
            throw new FileItemUploadException(e);
        }
    }

    @ApiOperation(value = "Deletes a temporary document file",
            notes = "Deletes a temporary file that was uploaded by the user before.")
    @ApiExceptions({
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @DeleteMapping("/temp/{temporaryDocumentItemId}")
    public void deleteTemporaryDocumentItem(@PathVariable String temporaryDocumentItemId) {

        Person owner = getCurrentPersonNotNull();
        documentItemService.deleteTemporaryItemById(owner, temporaryDocumentItemId);
    }

    @ApiOperation(value = "Gets a temporary document file",
            notes = "Gets a temporary file that was uploaded by the user before.")
    @ApiExceptions({
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/temp/{temporaryDocumentItemId}")
    public ClientTemporaryDocumentItem getTemporaryDocumentItem(@PathVariable String temporaryDocumentItemId) {

        Person owner = getCurrentPersonNotNull();
        TemporaryDocumentItem temporaryDocumentItem =
                documentItemService.findTemporaryItemById(owner, temporaryDocumentItemId);

        return fileClientModelMapper.toClientTemporaryDocumentItem(temporaryDocumentItem);
    }

    @ApiOperation(value = "Gets all temporary document files",
            notes = "Gets all temporary files that were uploaded by the user before.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/temp")
    public List<ClientTemporaryDocumentItem> getAllTemporaryDocumentItems() {

        Person owner = getCurrentPersonNotNull();

        return documentItemService.findAllTemporaryItemsByOwner(owner).stream()
                .map(fileClientModelMapper::toClientTemporaryDocumentItem)
                .collect(Collectors.toList());
    }

}
