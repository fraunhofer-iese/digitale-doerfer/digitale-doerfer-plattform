/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.services;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.ClientPushEventDocumentation;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushCategoryNotFoundException;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushException;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushSendService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory.PushCategoryId;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.springframework.lang.Nullable;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Wrapper for {@link IPushSendService} that maps {@link ClientBaseEvent}s to Json messages and forwards them. It is
 * only required since client entities can not be referenced from the business layer.
 */
public interface IClientPushService {

    @Getter
    @SuperBuilder(toBuilder = true)
    @Setter(AccessLevel.PACKAGE)
    @ToString
    class BasePushSendRequest {

        private ClientBaseEvent event;
        private String message;

        private Integer badgeCount;

    }

    @Getter
    @SuperBuilder(toBuilder = true)
    @Setter(AccessLevel.PACKAGE)
    @ToString
    class PushSendRequest extends BasePushSendRequest {

        private PushCategory category;
        private PushCategoryId categoryId;

    }

    @Getter
    @SuperBuilder(toBuilder = true)
    @Setter(AccessLevel.PACKAGE)
    class PushSendRequestMultipleCategories extends BasePushSendRequest {

        private Collection<PushCategory> categories;

    }

    void pushToPersonLoud(Person receiver, ClientPushService.PushSendRequest pushSendRequest) throws PushException;

    void pushToPersonLoud(Person receiver, ClientBaseEvent event, String message, PushCategory category)
            throws PushException;

    void pushToPersonLoud(Person author, Person receiver, ClientPushService.PushSendRequest pushSendRequest)
            throws PushException;

    void pushToPersonLoud(Person author, Person receiver, ClientBaseEvent event, String message, PushCategory category)
            throws PushException;

    void pushToPersonInLastUsedAppVariantLoud(Person receiver, PushSendRequestMultipleCategories pushSendRequest)
            throws PushException;

    void pushToPersonSilent(Person receiver, ClientPushService.PushSendRequest pushSendRequest) throws PushException;

    void pushToPersonSilent(Person receiver, ClientBaseEvent event, PushCategory category) throws PushException;

    void pushToPersonSilent(Person receiver, PushSendRequestMultipleCategories pushSendRequest)
            throws PushException;

    void pushToPersonSilent(Person receiver, ClientBaseEvent event, Collection<PushCategory> categories)
            throws PushException;

    void pushToGeoAreasLoud(@Nullable Person author, Set<GeoArea> geoAreas, boolean onlyForSameHomeArea,
                            PushSendRequest pushSendRequest) throws PushException;
    void pushToGeoAreaLoud(Person author, GeoArea geoArea, boolean onlyForSameHomeArea,
            PushSendRequest pushSendRequest) throws PushException;

    void pushToGeoAreaLoud(GeoArea geoArea, PushSendRequest pushSendRequest) throws PushException;

    void pushToGeoAreaLoud(GeoArea geoArea, ClientBaseEvent event, String message, PushCategory category)
            throws PushException;

    void pushToGeoAreasSilent(Set<GeoArea> geoAreas, boolean onlyForSameHomeArea, PushSendRequest pushSendRequest)
            throws PushException;

    void pushToGeoAreasSilent(Set<GeoArea> geoAreas, boolean onlyForSameHomeArea, ClientBaseEvent event, PushCategory category);

    void pushToGeoAreaSilent(GeoArea geoArea, PushSendRequest pushSendRequest)
            throws PushException;

    void pushToGeoAreaSilent(GeoArea geoArea, ClientBaseEvent event, PushCategory category) throws PushException;

    void pushToGeoAreaSilent(GeoArea geoArea, boolean onlyForSameHomeArea, PushSendRequest pushSendRequest)
            throws PushException;

    PushCategory findCategory(PushCategoryId pushCategoryId) throws PushCategoryNotFoundException;

    String getPushEventDocumentationPlain();

    List<ClientPushEventDocumentation> getPushEventDocumentation();

}
