/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.swagger;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.method.HandlerMethod;

import springfox.documentation.RequestHandler;
import springfox.documentation.builders.OperationBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.spi.service.contexts.RequestMappingContext;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

public abstract class BaseSwaggerPlugin implements OperationBuilderPlugin {

    protected final Logger log = LogManager.getLogger(this.getClass());

    @Override
    public boolean supports(DocumentationType delimiter) {
        return SwaggerPluginSupport.pluginDoesApply(delimiter);
    }

    @Override
    public final void apply(OperationContext context) {
        try {
            extendDocumentation(context);
        } catch (Exception e) {
            log.error("Swagger documentation plugin " + this.getClass().getName() + " failed: " + e.getMessage(), e);
        }
    }

    public abstract void extendDocumentation(OperationContext context);

    protected void appendToOperationField(OperationContext context, String fieldName, String value) {
        if (StringUtils.isEmpty(value)) {
            return;
        }
        manipulateOperationField(context, fieldName, v -> v + value);
    }

    protected void prependToOperationField(OperationContext context, String fieldName, String value) {
        if (StringUtils.isEmpty(value)) {
            return;
        }
        manipulateOperationField(context, fieldName, v -> value + v);
    }

    private void manipulateOperationField(OperationContext context, String fieldName, Function<String, String> operation) {
        Field field = ReflectionUtils.findField(OperationBuilder.class, fieldName, String.class);
        if (field != null) {
            ReflectionUtils.makeAccessible(field);
            Object fieldContent = ReflectionUtils.getField(field, context.operationBuilder());
            if (fieldContent instanceof String) {
                String newFieldContent = operation.apply((String) fieldContent);
                ReflectionUtils.setField(field, context.operationBuilder(), newFieldContent);
            } else {
                if (fieldContent == null) {
                    String newFieldContent = operation.apply("");
                    ReflectionUtils.setField(field, context.operationBuilder(), newFieldContent);
                } else {
                    log.error("Field {} is not of type String", fieldName);
                }
            }
        } else {
            log.error("Could not get field {}", fieldName);
        }
    }

    protected boolean isNotDDClass(OperationContext context){
        RequestMappingContext requestContext = getValue(context, "requestContext", RequestMappingContext.class);
        if(requestContext!=null){
            RequestHandler handler = getValue(requestContext, "handler", RequestHandler.class);
            if(handler != null){
                @SuppressWarnings("deprecation")
                HandlerMethod handlerMethod = handler.getHandlerMethod();
                if(handlerMethod != null) {
                    return !StringUtils.startsWith(handlerMethod.getBeanType().getName(), "de.fhg.iese.dd");
                }
            }
        }
        return false;
    }

    private <T> T getValue(Object object, String fieldName, Class<T> expectedType){
        Field field = ReflectionUtils.findField(object.getClass(), fieldName, expectedType);
        if (field != null) {
            ReflectionUtils.makeAccessible(field);
            Object fieldContent = ReflectionUtils.getField(field, object);
            if(fieldContent == null) {
                return null;
            }
            if (expectedType.isAssignableFrom(fieldContent.getClass())) {
                @SuppressWarnings("unchecked")
                T result = (T) fieldContent;
                return result;
            } else {
                log.error("Field {} is of wrong type", fieldName);
            }
        } else {
            log.error("Could not get field {}", fieldName);
        }
        return null;
    }

    protected List<Parameter> getParameters(final OperationContext context) {
        final OperationBuilder operationBuilder = context.operationBuilder();
        @SuppressWarnings("unchecked")
        List<Parameter> parameters = getValue(operationBuilder, "parameters", List.class);
        return parameters;
    }

    protected void setParameterRequired(OperationContext context, String parameterName, boolean required,
            String description) {

        Object fieldContent = getValue(context.operationBuilder(), "parameters", List.class);
        if (fieldContent != null) {
            @SuppressWarnings("unchecked")
            List<Parameter> parameterList = (List) fieldContent;
            //get the parameter to be modified
            Optional<Parameter> parameterOpt = parameterList.stream()
                    .filter(p -> StringUtils.equals(p.getName(), parameterName))
                    .findFirst();

            if (parameterOpt.isPresent()) {
                Parameter oldParameter = parameterOpt.get();
                //the method for cloning a parameter is not visible, so we need to call it via reflection
                Method fromMethod = ReflectionUtils.findMethod(ParameterBuilder.class, "from", Parameter.class);
                if (fromMethod != null) {
                    ReflectionUtils.makeAccessible(fromMethod);
                    ParameterBuilder parameterBuilder = new ParameterBuilder();
                    ReflectionUtils.invokeMethod(fromMethod, parameterBuilder, parameterOpt.get());

                    parameterBuilder.required(required);
                    parameterBuilder.description(description);
                    Parameter newParameter = parameterBuilder.build();
                    int oldIndex = parameterList.indexOf(oldParameter);
                    parameterList.remove(oldIndex);
                    parameterList.add(oldIndex, newParameter);
                } else {
                    log.error("Could not get method");
                }
            } else {
                log.error("Parameter {} not found", parameterName);
            }
        } else {
            log.error("No parameters found");
        }
    }

    protected void removeParameter(OperationContext context, String parameterName) {
        Object fieldContent = getValue(context.operationBuilder(), "parameters", List.class);
        if (fieldContent != null) {
            @SuppressWarnings("unchecked")
            List<Parameter> parameterList = (List) fieldContent;
            //get the parameter to be modified
            parameterList.removeIf(p -> StringUtils.equals(p.getName(), parameterName));
        } else {
            log.error("No parameters found");
        }
    }

}
