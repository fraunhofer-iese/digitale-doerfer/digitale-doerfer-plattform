/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.controllers;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.push.clientevent.ClientInformationMessageEvent;
import de.fhg.iese.dd.platform.api.shared.push.services.IClientPushService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.push.providers.IExternalPushProvider;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushRegistrationService;
import de.fhg.iese.dd.platform.business.shared.security.SuperAdminAction;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/administration/shared/push")
@Api(tags = {"admin", "shared.push.admin"})
class PushAdminController extends BaseController {

    @Autowired
    private IPersonService personService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private IClientPushService clientPushService;
    @Autowired
    private IPushRegistrationService pushRegistrationService;
    @Autowired
    private IPushCategoryService pushCategoryService;

    @ApiOperation(value = "Send Push Notification to GeoArea")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping(value = "/send/geoArea")
    public void sendPushToGeoArea(
            @RequestParam("geoAreaId") String geoAreaId,
            @RequestParam("message") String message,
            @RequestParam("categoryId") String categoryId
    ) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        PushCategory category = pushCategoryService.findPushCategoryById(categoryId);
        GeoArea geoArea = geoAreaService.findGeoAreaById(geoAreaId);

        clientPushService.pushToGeoAreaLoud(geoArea, new ClientInformationMessageEvent(message), message, category);
    }

    @ApiOperation(value = "Send Push Notification to Persons")
    @ApiExceptions({ClientExceptionType.PERSON_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping(value = "/send/persons")
    public String sendPushToPerson(
            @RequestParam("personIds") List<String> personIds,
            @RequestParam("message") String message,
            @RequestParam("categoryId") String categoryId
    ) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        List<Person> persons = personService.findAllPersonsById(personIds);
        PushCategory category = pushCategoryService.findPushCategoryById(categoryId);
        for (Person person : persons) {
            clientPushService.pushToPersonLoud(person, new ClientInformationMessageEvent(message), message, category);
        }

        return "Message sent to " + persons.size() + " persons";
    }

    @ApiOperation(value = "Get all registrations of a person")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @GetMapping(value = "/registrations")
    public String getAllPushRegistrations(
            @RequestParam("personId") String personId) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        StringBuilder summary = new StringBuilder();
        Person person = personService.findPersonById(personId);
        summary.append("Person '").append(person.getFullName()).append("' (").append(person.getId()).append(")\n");
        summary.append("has these endpoints registered:\n");
        final Collection<IExternalPushProvider.ExternalPushEndpointDescription> endpoints =
                pushRegistrationService.getPushEndpoints(person);
        for (IExternalPushProvider.ExternalPushEndpointDescription endpoint : endpoints) {
            summary.append(endpoint.toString()).append("\n");
        }
        return summary.toString();
    }

    @ApiOperation(value = "Delete existing push category",
            notes = "Deletes a non longer used push category. "
                    + "There is no check if the category is really unused, this needs to be done manually before.")
    @ApiExceptions({ClientExceptionType.PUSH_CATEGORY_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @DeleteMapping(value = "pushCategory")
    public String deletePushCategory(
            @ApiParam(value = "id of the push category", required = true)
            @RequestParam(value = "pushCategoryId") String pushCategoryId
    ) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        PushCategory pushCategory = pushCategoryService.findPushCategoryById(pushCategoryId);

        LogSummary logSummary = pushCategoryService.deletePushCategory(pushCategory);

        return logSummary.toString();
    }

}
