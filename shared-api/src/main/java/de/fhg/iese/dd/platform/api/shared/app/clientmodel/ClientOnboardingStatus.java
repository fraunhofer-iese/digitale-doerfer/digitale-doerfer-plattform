/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2022 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.clientmodel;

import java.util.List;
import java.util.Map;

import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoArea;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonOwn;
import de.fhg.iese.dd.platform.api.shared.feature.clientmodel.ClientFeature;
import de.fhg.iese.dd.platform.api.shared.legal.clientmodel.ClientLegalTextStatus;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.ClientPushCategoryUserSetting;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientOnboardingStatus extends ClientBaseEntity {

    private ClientPersonOwn ownPerson;
    private ClientAppVariantVersionCheckResult versionCheckResult;
    private List<ClientFeature> features;
    private ClientLegalTextStatus legalTextStatus;
    private List<ClientPushCategoryUserSetting> pushCategorySettings;
    private ClientGeoArea homeGeoArea;
    private ClientGeoAreaSelectionStatus homeGeoAreaSelectionStatus;
    private List<ClientGeoArea> selectedGeoAreas;
    private Map<String, ClientGeoAreaSelectionStatus> selectedGeoAreaIdsInvalid;
    private long availableLeafGeoAreasCount;
    private boolean onboardingRequired;

}
