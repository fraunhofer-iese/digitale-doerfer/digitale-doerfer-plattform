/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers;

import java.util.Collections;
import java.util.Set;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientFilterStatus;
import de.fhg.iese.dd.platform.api.framework.modifiers.IContentFilterSettings;
import de.fhg.iese.dd.platform.api.framework.modifiers.IOutgoingClientEntityModifier;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.BaseClientPerson;

@Component
@Order(1)
class ClientPersonFlagAndBlockModifier implements IOutgoingClientEntityModifier<BaseClientPerson> {

    private static final Set<String> ANY_PATHS = Collections.singleton("/**");

    @Override
    public Class<BaseClientPerson> getType() {
        return BaseClientPerson.class;
    }

    @Override
    public Set<String> getPathPatterns() {
        return ANY_PATHS;
    }

    @Override
    public boolean modify(BaseClientPerson baseClientPerson, IContentFilterSettings contentFilterSettings) {
        if (contentFilterSettings.getBlockedPersonIds().contains(baseClientPerson.getId())) {
            baseClientPerson.setFilterStatus(ClientFilterStatus.BLOCKED);
            baseClientPerson.setProfilePicture(null);
            return true; //we want to have further modifications, e.g. last name shortened
        }
        if (contentFilterSettings.getFlaggedEntityIds().contains(baseClientPerson.getId())) {
            baseClientPerson.setFilterStatus(ClientFilterStatus.FLAGGED);
        }
        return true;
    }

}
