/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2022 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonReferenceWithEmail;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.api.participants.tenant.clientmodel.TenantClientModelMapper;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlagStatusRecord;

@Component
public class UserGeneratedContentFlagClientModelMapper {

    @Autowired
    private PersonClientModelMapper personClientModelMapper;
    @Autowired
    private TenantClientModelMapper tenantClientModelMapper;

    public ClientUserGeneratedContentFlagForFlagCreator toClientUserGeneratedContentFlagForFlagCreator(
            UserGeneratedContentFlag flag) {

        return ClientUserGeneratedContentFlagForFlagCreator.builder()
                .id(flag.getId())
                .flagEntityId(flag.getEntityId())
                .flagEntityType(flag.getEntityType())
                .status(flag.getStatus())
                .build();
    }

    public ClientUserGeneratedContentFlag toClientUserGeneratedContentFlag(UserGeneratedContentFlag flag) {

        return setClientUserGeneratedContentFlagAttributes(flag, new ClientUserGeneratedContentFlag());
    }

    public ClientUserGeneratedContentFlagDetail toClientUserGeneratedContentFlagDetail(UserGeneratedContentFlag flag) {

        final ClientUserGeneratedContentFlagDetail clientFlag = new ClientUserGeneratedContentFlagDetail();
        return setClientUserGeneratedContentFlagDetailAttributes(flag, clientFlag);
    }

    public ClientUserGeneratedContentFlag setClientUserGeneratedContentFlagAttributes(UserGeneratedContentFlag flag,
            ClientUserGeneratedContentFlag clientFlag) {
        clientFlag.setId(flag.getId());
        clientFlag.setEntityId(flag.getEntityId());
        clientFlag.setEntityType(flag.getEntityType());
        clientFlag.setStatus(flag.getStatus());
        clientFlag.setLastStatusUpdate(flag.getLastStatusUpdate());
        clientFlag.setTenant(
                tenantClientModelMapper.createClientCommunity(flag.getTenant()));
        clientFlag.setFlagCreator(
                personClientModelMapper.createClientPersonReferenceWithEmail(flag.getFlagCreator()));
        clientFlag.setEntityAuthor(
                personClientModelMapper.createClientPersonReferenceWithEmail(flag.getEntityAuthor()));
        if (!CollectionUtils.isEmpty(flag.getUserGeneratedContentFlagStatusRecords())) {
            // get first status record as the list is ordered by created desc
            clientFlag.setLastStatusRecord(toClientUserGeneratedContentFlagStatusRecord(
                    flag.getUserGeneratedContentFlagStatusRecords().get(0)));
        }
        return clientFlag;
    }

    public <T extends ClientUserGeneratedContentFlagDetail> T setClientUserGeneratedContentFlagDetailAttributes(
            UserGeneratedContentFlag flag, T clientFlag) {
        setClientUserGeneratedContentFlagAttributes(flag, clientFlag);
        if (!CollectionUtils.isEmpty(flag.getUserGeneratedContentFlagStatusRecords())) {
            clientFlag.setStatusRecords(flag.getUserGeneratedContentFlagStatusRecords().stream()
                    .map(this::toClientUserGeneratedContentFlagStatusRecord)
                    .collect(Collectors.toList()));
        }
        return clientFlag;
    }

    public ClientUserGeneratedContentFlagStatusRecord toClientUserGeneratedContentFlagStatusRecord(
            UserGeneratedContentFlagStatusRecord statusRecord) {
        if (statusRecord == null) {
            return null;
        }
        final ClientPersonReferenceWithEmail initiator =
                personClientModelMapper.createClientPersonReferenceWithEmail(statusRecord.getInitiator());
        return ClientUserGeneratedContentFlagStatusRecord.builder()
                .id(statusRecord.getId())
                .initiator(initiator)
                .status(statusRecord.getStatus())
                .created(statusRecord.getCreated())
                .comment(statusRecord.getComment())
                .build();
    }

}
