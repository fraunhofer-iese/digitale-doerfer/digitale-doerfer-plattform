/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.clientmodel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ClientPushEventDocumentation {

    @Builder.Default
    @ApiModelProperty(required = true)
    private String appId = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String appIdentifier = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String appName = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String eventName = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String clientEventName = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String categoryId = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String categoryName = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String categoryDescription = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String categoryInternalName = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String categorySubject = "";

    private boolean loud;

    @Builder.Default
    @ApiModelProperty(required = true)
    private String notes = "";

}
