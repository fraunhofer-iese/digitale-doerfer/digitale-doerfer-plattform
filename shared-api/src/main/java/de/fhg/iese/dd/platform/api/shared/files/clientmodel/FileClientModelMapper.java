/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.clientmodel;

import de.fhg.iese.dd.platform.datamanagement.shared.files.model.*;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class FileClientModelMapper {

    public ClientTemporaryMediaItem toClientTemporaryMediaItem(TemporaryMediaItem temporaryMediaItem) {
        if (temporaryMediaItem == null) {
            return null;
        }
        return ClientTemporaryMediaItem.builder()
                .id(temporaryMediaItem.getId())
                .expirationTime(temporaryMediaItem.getExpirationTime())
                .mediaItem(toClientMediaItem(temporaryMediaItem.getMediaItem()))
                .build();
    }

    public ClientMediaItem toClientMediaItem(MediaItem mediaItem) {
        if (mediaItem == null) {
            return null;
        }
        final RelativeImageCropDefinition imageCropDefinition = mediaItem.getRelativeImageCropDefinition();
        return ClientMediaItem.builder()
                .id(mediaItem.getId())
                .urls(mediaItem.getUrlsJson())
                .relativeImageCropDefinition(imageCropDefinition != null ? ClientRelativeImageCropDefinition.builder()
                        .relativeOffsetX(imageCropDefinition.getRelativeOffsetX())
                        .relativeOffsetY(imageCropDefinition.getRelativeOffsetY())
                        .relativeWidth(imageCropDefinition.getRelativeWidth())
                        .relativeHeight(imageCropDefinition.getRelativeHeight())
                        .build() : null)
                .build();
    }

    public List<ClientMediaItem> toClientMediaItems(List<MediaItem> mediaItems) {
        if (CollectionUtils.isEmpty(mediaItems)) {
            return Collections.emptyList();
        } else {
            return mediaItems.stream()
                    .map(this::toClientMediaItem)
                    .collect(Collectors.toList());
        }
    }

    public ClientMediaItemSize toClientMediaItemSize(MediaItemSize mediaItemSize, int currentSizeVersion) {
        if (mediaItemSize == null) {
            return null;
        }
        return ClientMediaItemSize.builder()
                .name(mediaItemSize.getName())
                .version(currentSizeVersion)
                .maxSize(mediaItemSize.getMaxSize())
                .squared(mediaItemSize.isSquared())
                .build();
    }

    public ClientTemporaryDocumentItem toClientTemporaryDocumentItem(TemporaryDocumentItem temporaryDocumentItem) {
        if (temporaryDocumentItem == null) {
            return null;
        }
        return ClientTemporaryDocumentItem.builder()
                .id(temporaryDocumentItem.getId())
                .expirationTime(temporaryDocumentItem.getExpirationTime())
                .documentItem(toClientDocumentItem(temporaryDocumentItem.getDocumentItem()))
                .build();
    }

    public ClientDocumentItem toClientDocumentItem(DocumentItem documentItem) {
        if (documentItem == null) {
            return null;
        }
        return ClientDocumentItem.builder()
                .id(documentItem.getId())
                .url(documentItem.getUrl())
                .mediaType(documentItem.getMediaType())
                .title(documentItem.getTitle())
                .description(documentItem.getDescription())
                .build();
    }

    public List<ClientDocumentItem> toClientDocumentItems(Set<DocumentItem> documentItems) {
        if (CollectionUtils.isEmpty(documentItems)) {
            return Collections.emptyList();
        } else {
            return documentItems.stream()
                    .map(this::toClientDocumentItem)
                    .filter(Objects::nonNull)
                    .sorted(Comparator.comparing(ClientDocumentItem::getTitle))
                    .collect(Collectors.toList());
        }
    }

}
