/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Stefan Schweitzer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientGeoAreaExtended extends BaseClientGeoArea<ClientGeoAreaExtended> {

    @JsonInclude
    private String parentId;
    @Nullable
    @ApiModelProperty(value = "Number of all children under this area, including children of children. Might be null",
            allowEmptyValue = true)
    private Integer countChildren;
    @Nullable
    @ApiModelProperty(value = "Number of direct children under this area. Might be null", allowEmptyValue = true)
    private Integer countDirectChildren;

    @Deprecated
    private String tenantId;

}
