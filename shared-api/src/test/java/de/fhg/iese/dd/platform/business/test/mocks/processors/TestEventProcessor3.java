/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Alberto Lara, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.test.mocks.processors;

import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.test.mocks.events.TestEventC;
import de.fhg.iese.dd.platform.business.test.mocks.events.TestEventD;

@EventProcessor
public class TestEventProcessor3 extends BaseEventProcessor {

    private String getName(){
        return this.getClass().getSimpleName();
    }

    @EventProcessing
    private TestEventD handleTestEventC(TestEventC event) {
        return new TestEventD(event.getData() + "|" + getName());
    }

}
