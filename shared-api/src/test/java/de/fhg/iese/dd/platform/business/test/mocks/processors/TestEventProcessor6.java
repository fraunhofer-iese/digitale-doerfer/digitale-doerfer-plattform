/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Alberto Lara, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.test.mocks.processors;

import java.util.function.Consumer;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.test.mocks.events.TestEventH;
import de.fhg.iese.dd.platform.business.test.mocks.events.TestEventI;
import lombok.Setter;

@EventProcessor
public class TestEventProcessor6 extends BaseEventProcessor {

    @Setter
    private Consumer<BaseEvent> testEventConsumer;

    @EventProcessing(relevantEvents = {
            TestEventH.class,
            TestEventI.class})
    private void handleTestEvent(BaseEvent event) {
        if(testEventConsumer != null){
            this.testEventConsumer.accept(event);
        }
    }

}
