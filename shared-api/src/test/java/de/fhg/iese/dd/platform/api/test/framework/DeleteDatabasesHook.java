/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Alberto Lara
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.test.framework;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.context.WebApplicationContext;

import lombok.extern.log4j.Log4j2;

/**
 * Creates a Hook that is executed when the VM is shutdown, which will delete the database. Only relevant for tests on
 * MySQL
 */
@Configuration
@Log4j2
@Profile({"testmysql"})
public class DeleteDatabasesHook {

    private static final String QUERY_DROP_DATABASE_MYSQL = "drop database if exists ebdb";

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @PostConstruct
    void createShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            long start = System.currentTimeMillis();
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.execute(new TransactionCallbackWithoutResult() {
                @Override
                public void doInTransactionWithoutResult(TransactionStatus status) {
                    String forkNumber = System.getProperty("forkNumber");
                    String dropDatabaseQuery = QUERY_DROP_DATABASE_MYSQL + forkNumber + ";";

                    entityManager.createNativeQuery(dropDatabaseQuery)
                            .executeUpdate();
                }
            });
            entityManager.getEntityManagerFactory().getCache().evictAll();
            log.debug("Drop test databases in " + (System.currentTimeMillis() - start) + " ms");
        }));
    }

}
