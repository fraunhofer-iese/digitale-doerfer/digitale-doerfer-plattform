/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2018 Alberto Lara, Tahmid Ekram
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.test.framework;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.context.WebApplicationContext;

import com.zaxxer.hikari.HikariDataSource;

/**
 * For testing with MySql and creating automatically schemas
 * according to the surefire fork number (although this can be done easily for
 * mysql in the YML file, for better understanding and for running the test
 * without Maven, which is the only case when the forkNumber is actually
 * specified, the test datasource for MySQL is defined here.
 *
 */
@Configuration
@Profile({ "testmysql" })
public class TestDatabaseInitializationMySQL {

    private static final String MYSQL_URL_1 = "jdbc:mysql://localhost:3306/ebdb";
    private static final String MYSQL_URL_2 = "?createDatabaseIfNotExist=true&verifyServerCertificate=false&allowMultiQueries=true&useSSL=false";
    private static final String MYSQL_DRIVER_CLASS_NAME = "com.mysql.cj.jdbc.Driver";

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Bean
    public DataSource dataSource() {

        //First of all, the number of the fork from surefire is retrieved, since it is the base for naming the test database schemas
        String forkNumber = System.getProperty("forkNumber");

        //If the forkNumber is null that means that the tests are run without maven, therefore the property is not initialized
        if(forkNumber == null){
            forkNumber = "1";
            System.setProperty("forkNumber", forkNumber);
        }

        //Now the URL for the datasource is built based on the forkNumber
        String URL = MYSQL_URL_1 + forkNumber + MYSQL_URL_2;

        //Same for the driver
        String driverClass = MYSQL_DRIVER_CLASS_NAME;

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName(driverClass);
        dataSource.setJdbcUrl(URL);
        dataSource.setUsername(webApplicationContext.getEnvironment().getProperty("spring.datasource.username"));
        dataSource.setPassword(webApplicationContext.getEnvironment().getProperty("spring.datasource.password"));
        dataSource.setConnectionInitSql("SET NAMES utf8mb4;");

        return dataSource;
    }

}
