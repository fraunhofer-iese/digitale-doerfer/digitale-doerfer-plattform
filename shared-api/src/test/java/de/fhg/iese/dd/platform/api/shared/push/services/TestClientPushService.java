/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.framework.clientevent.ClientMinimizableEvent;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushException;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonWriter;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Profile({"test"})
@Service
public class TestClientPushService extends ClientPushService {

    @Autowired
    private PushCategoryRepository pushCategoryRepository;

    private IClientPushService mock;

    @PostConstruct
    private void initialize() {
        mock = Mockito.mock(IClientPushService.class);
    }

    @Override
    public void pushToPersonLoud(Person receiver, PushSendRequest pushSendRequest) throws PushException {
        super.pushToPersonLoud(receiver, pushSendRequest);
        mock.pushToPersonLoud(receiver, pushSendRequest);
    }

    @Override
    public void pushToPersonLoud(Person author, Person receiver, PushSendRequest pushSendRequest)
            throws PushException {
        super.pushToPersonLoud(author, receiver, pushSendRequest);
        mock.pushToPersonLoud(author, receiver, pushSendRequest);
    }

    @Override
    public void pushToPersonInLastUsedAppVariantLoud(Person receiver,
            PushSendRequestMultipleCategories pushSendRequest)
            throws PushException {
        super.pushToPersonInLastUsedAppVariantLoud(receiver, pushSendRequest);
        mock.pushToPersonInLastUsedAppVariantLoud(receiver, pushSendRequest);
    }

    @Override
    public void pushToPersonSilent(Person receiver, PushSendRequest pushSendRequest) throws PushException {
        super.pushToPersonSilent(receiver, pushSendRequest);
        mock.pushToPersonSilent(receiver, pushSendRequest);
    }

    @Override
    public void pushToPersonSilent(Person receiver, PushSendRequestMultipleCategories pushSendRequest)
            throws PushException {
        super.pushToPersonSilent(receiver, pushSendRequest);
        mock.pushToPersonSilent(receiver, pushSendRequest);
    }

    @Override
    public void pushToGeoAreasLoud(Person author, Set<GeoArea> geoAreas, boolean onlyForSameHomeArea, PushSendRequest pushSendRequest) throws PushException {

        super.pushToGeoAreasLoud(author, geoAreas, onlyForSameHomeArea, pushSendRequest);
        mock.pushToGeoAreasLoud(author, geoAreas, onlyForSameHomeArea, pushSendRequest);
    }

    @Override
    public void pushToGeoAreasSilent(Set<GeoArea> geoAreas, boolean onlyForSameHomeArea, PushSendRequest pushSendRequest) throws PushException {

        super.pushToGeoAreasSilent(geoAreas, onlyForSameHomeArea, pushSendRequest);
        mock.pushToGeoAreasSilent(geoAreas, onlyForSameHomeArea, pushSendRequest);
    }

    public <T extends ClientBaseEvent> Pair<String, T> getPushedEventAndMessageToPersonLoud(Person receiver,
            Class<T> expectedEvent, PushCategory.PushCategoryId pushCategoryId) {

        ArgumentCaptor<PushSendRequest> requestCaptor = ArgumentCaptor.forClass(PushSendRequest.class);

        Mockito.verify(mock, Mockito.atLeast(1))
                .pushToPersonLoud(
                        Mockito.eq(receiver),
                        requestCaptor.capture());

        PushSendRequest request = getRequestForCategory(requestCaptor, pushCategoryId, expectedEvent);
        @SuppressWarnings("unchecked")
        T event = (T) request.getEvent();
        return Pair.of(request.getMessage(), event);
    }

    public <T extends ClientBaseEvent> T getPushedEventToPersonLoud(Person receiver, Class<T> expectedEvent,
            PushCategory.PushCategoryId pushCategoryId) {
        return getPushedEventAndMessageToPersonLoud(receiver, expectedEvent, pushCategoryId).getRight();
    }

    public <T extends ClientBaseEvent> Pair<String, T> getPushedEventAndMessageToPersonLoud(Person author,
            Person receiver,
            Class<T> expectedEvent, PushCategory.PushCategoryId pushCategoryId) {

        ArgumentCaptor<PushSendRequest> requestCaptor = ArgumentCaptor.forClass(PushSendRequest.class);

        Mockito.verify(mock, Mockito.atLeast(1))
                .pushToPersonLoud(
                        Mockito.eq(author),
                        Mockito.eq(receiver),
                        requestCaptor.capture());

        PushSendRequest request = getRequestForCategory(requestCaptor, pushCategoryId, expectedEvent);
        @SuppressWarnings("unchecked")
        T event = (T) request.getEvent();
        return Pair.of(request.getMessage(), event);
    }

    public <T extends ClientBaseEvent> T getPushedEventToPersonLoud(Person author, Person receiver,
            Class<T> expectedEvent, PushCategory.PushCategoryId pushCategoryId) {
        return getPushedEventAndMessageToPersonLoud(author, receiver, expectedEvent, pushCategoryId).getRight();
    }

    public <T extends ClientBaseEvent> Pair<String, T> getPushedEventToPersonInLastUsedAppVariantLoud(Person receiver,
            Class<T> expectedEvent, Collection<PushCategory> pushCategories) {

        ArgumentCaptor<PushSendRequestMultipleCategories> requestCaptor =
                ArgumentCaptor.forClass(PushSendRequestMultipleCategories.class);

        Mockito.verify(mock, Mockito.atLeast(1)).pushToPersonInLastUsedAppVariantLoud(
                Mockito.eq(receiver),
                requestCaptor.capture());

        PushSendRequestMultipleCategories request = getRequestForCategory(requestCaptor, pushCategories, expectedEvent);
        @SuppressWarnings("unchecked")
        T event = (T) request.getEvent();
        return Pair.of(request.getMessage(), event);
    }

    public <T extends ClientBaseEvent> T getPushedEventToPersonSilent(Person receiver, Class<T> expectedEvent,
            PushCategory.PushCategoryId pushCategoryId) {

        ArgumentCaptor<PushSendRequest> requestCaptor = ArgumentCaptor.forClass(PushSendRequest.class);

        Mockito.verify(mock, Mockito.atLeast(1))
                .pushToPersonSilent(
                        Mockito.eq(receiver),
                        requestCaptor.capture());
        T event = getPushedEventForCategory(requestCaptor, pushCategoryId, expectedEvent);

        log.info("Pushed silent notification for {} to {}", event.getClass().getSimpleName(),
                receiver.getFullName());
        return event;
    }

    public <T extends ClientBaseEvent> T getPushedEventToPersonSilent(Person receiver, Class<T> expectedEvent,
            Collection<PushCategory> pushCategories) {

        ArgumentCaptor<PushSendRequestMultipleCategories> requestCaptor =
                ArgumentCaptor.forClass(PushSendRequestMultipleCategories.class);

        Mockito.verify(mock, Mockito.atLeast(1)).pushToPersonSilent(
                Mockito.eq(receiver),
                requestCaptor.capture());

        PushSendRequestMultipleCategories request = getRequestForCategory(requestCaptor, pushCategories, expectedEvent);
        @SuppressWarnings("unchecked")
        T event = (T) request.getEvent();

        log.info("Pushed silent notification for {} to {}", event.getClass().getSimpleName(),
                receiver.getFullName());
        return event;
    }

    public <T extends ClientBaseEvent> Pair<String, T> getPushedEventAndMessageToGeoAreasLoud(Person author,
                                                                                              Set<GeoArea> geoAreas,
                                                                                              boolean silentForOtherHomeAreas,
                                                                                              Class<T> expectedEvent,
                                                                                              PushCategory.PushCategoryId pushCategoryId) {

        ArgumentCaptor<PushSendRequest> requestCaptor = ArgumentCaptor.forClass(PushSendRequest.class);

        Mockito.verify(mock, Mockito.atLeast(1))
                .pushToGeoAreasLoud(
                        Mockito.eq(author),
                        Mockito.eq(geoAreas),
                        Mockito.eq(silentForOtherHomeAreas),
                        requestCaptor.capture());
        PushSendRequest request = getRequestForCategory(requestCaptor, pushCategoryId, expectedEvent);
        @SuppressWarnings("unchecked")
        T event = (T) request.getEvent();
        return Pair.of(request.getMessage(), event);
    }

    public <T extends ClientBaseEvent> Pair<String, T> getPushedEventAndMessageToGeoAreaLoud(Person author,
                                                                                             GeoArea geoArea,
                                                                                             boolean silentForOtherHomeAreas,
                                                                                             Class<T> expectedEvent,
                                                                                             PushCategory.PushCategoryId pushCategoryId) {

        return getPushedEventAndMessageToGeoAreasLoud(author, Set.of(geoArea), silentForOtherHomeAreas, expectedEvent,
                pushCategoryId);
    }

    public <T extends ClientBaseEvent> T getPushedEventToGeoAreasLoud(Person author,
                                                                      Set<GeoArea> geoAreas,
                                                                      boolean silentForOtherHomeAreas,
                                                                      Class<T> expectedEvent,
                                                                      PushCategory.PushCategoryId pushCategoryId) {

        return getPushedEventAndMessageToGeoAreasLoud(author, geoAreas, silentForOtherHomeAreas, expectedEvent,
                pushCategoryId).getRight();
    }

    public <T extends ClientBaseEvent> T getPushedEventToGeoAreaLoud(Person author,
                                                                     GeoArea geoArea,
                                                                     boolean silentForOtherHomeAreas,
                                                                     Class<T> expectedEvent,
                                                                     PushCategory.PushCategoryId pushCategoryId) {

        return getPushedEventAndMessageToGeoAreaLoud(author, geoArea, silentForOtherHomeAreas, expectedEvent,
                pushCategoryId).getRight();
    }

    public <T extends ClientBaseEvent> Pair<String, T> getPushedEventAndMessageToGeoAreaLoud(GeoArea geoArea,
            Class<T> expectedEvent,
            PushCategory.PushCategoryId pushCategoryId) {

        return getPushedEventAndMessageToGeoAreasLoud(null, Set.of(geoArea), false, expectedEvent, pushCategoryId);
    }

    public <T extends ClientBaseEvent> T getPushedEventToGeoAreasLoud(Set<GeoArea> geoAreas,
                                                                      Class<T> expectedEvent,
                                                                      PushCategory.PushCategoryId pushCategoryId) {

        return getPushedEventAndMessageToGeoAreasLoud(null, geoAreas, false, expectedEvent, pushCategoryId).getRight();
    }

    public <T extends ClientBaseEvent> T getPushedEventToGeoAreaLoud(GeoArea geoArea,
                                                                     Class<T> expectedEvent,
                                                                     PushCategory.PushCategoryId pushCategoryId) {

        return getPushedEventAndMessageToGeoAreaLoud(geoArea, expectedEvent, pushCategoryId).getRight();
    }

    public <T extends ClientBaseEvent> T getPushedEventToGeoAreasSilent(Set<GeoArea> geoAreas,
                                                                        boolean silentForOtherHomeAreas,
                                                                        Class<T> expectedEvent,
                                                                        PushCategory.PushCategoryId pushCategoryId) {

        ArgumentCaptor<PushSendRequest> requestCaptor = ArgumentCaptor.forClass(PushSendRequest.class);

        Mockito.verify(mock, Mockito.atLeast(1))
                .pushToGeoAreasSilent(
                        Mockito.eq(geoAreas),
                        Mockito.eq(silentForOtherHomeAreas),
                        requestCaptor.capture()
                );
        return getPushedEventForCategory(requestCaptor, pushCategoryId, expectedEvent);
    }

    public <T extends ClientBaseEvent> T getPushedEventToGeoAreasSilent(Set<GeoArea> geoAreas,
                                                                        Class<T> expectedEvent,
                                                                        PushCategory.PushCategoryId pushCategoryId) {

        return getPushedEventToGeoAreasSilent(geoAreas, false, expectedEvent, pushCategoryId);
    }

    public <T extends ClientBaseEvent> T getPushedEventToGeoAreaSilent(GeoArea geoArea, Class<T> expectedEvent,
                                                                       PushCategory.PushCategoryId pushCategoryId) {

        return getPushedEventToGeoAreasSilent(Set.of(geoArea), false, expectedEvent, pushCategoryId);
    }


    public <T extends ClientBaseEvent> T getPushedEventToGeoAreaSilent(GeoArea geoArea, boolean silentForOtherHomeAreas,
                                                                       Class<T> expectedEvent,
                                                                       PushCategory.PushCategoryId pushCategoryId) {

        return getPushedEventToGeoAreasSilent(Set.of(geoArea), silentForOtherHomeAreas, expectedEvent, pushCategoryId);
    }

    private <T> T getPushedEventForCategory(ArgumentCaptor<PushSendRequest> requestCaptor,
                                            PushCategory.PushCategoryId pushCategoryId, Class<T> expectedEvent) {

        PushSendRequest request = getRequestForCategory(requestCaptor, pushCategoryId, expectedEvent);
        @SuppressWarnings("unchecked")
        T event = (T) request.getEvent();
        return event;
    }

    private PushSendRequest getRequestForCategory(ArgumentCaptor<PushSendRequest> requestCaptor,
            PushCategory.PushCategoryId pushCategoryId, Class<?> expectedEvent) {
        PushCategory pushCategory = findPushCategory(pushCategoryId);

        List<PushSendRequest> requestsForCategory = requestCaptor.getAllValues().stream()
                .filter(r -> pushCategory.equals(r.getCategory()) || pushCategoryId.equals(r.getCategoryId()))
                .collect(Collectors.toList());

        assertThat(requestsForCategory).as("Exactly one event should be pushed for this category").hasSize(1);
        PushSendRequest request = requestsForCategory.get(0);
        ClientBaseEvent event = request.getEvent();
        assertThat(event).as("Different event type pushed").isOfAnyClassIn(expectedEvent);
        assertMinimizedPushEventIsValid(event);

        if (request.getMessage() != null) {
            log.info("Pushed message for {}:\n'{}'", event.getClass().getSimpleName(),
                    request.getMessage());
        }
        return request;
    }

    private PushSendRequestMultipleCategories getRequestForCategory(
            ArgumentCaptor<PushSendRequestMultipleCategories> requestCaptor,
            Collection<PushCategory> pushCategories, Class<?> expectedEvent) {

        List<PushSendRequestMultipleCategories> requestsForCategory = requestCaptor.getAllValues().stream()
                .filter(r -> CollectionUtils.isEqualCollection(pushCategories, r.getCategories()))
                .collect(Collectors.toList());

        assertThat(requestsForCategory).as("Exactly one event should be pushed for these categories").hasSize(1);
        PushSendRequestMultipleCategories request = requestsForCategory.get(0);
        ClientBaseEvent event = request.getEvent();
        assertThat(event).as("Different event type pushed").isOfAnyClassIn(expectedEvent);
        assertMinimizedPushEventIsValid(event);

        if (request.getMessage() != null) {
            log.info("Pushed message for {}:\n'{}'", event.getClass().getSimpleName(),
                    request.getMessage());
        }
        return request;
    }

    public void assertNoMorePushedEvents() {
        Mockito.verifyNoMoreInteractions(mock);
        Mockito.reset(mock);
    }

    private PushCategory findPushCategory(PushCategory.PushCategoryId pushCategoryId) {
        PushCategory category = pushCategoryRepository.findById(pushCategoryId.getId()).orElse(null);
        assertNotNull(category);
        return category;
    }

    public void reset() {
        Mockito.reset(mock);
    }

    /**
     * Assumption: The original event contains all fields of the minimized one
     */
    private static void assertMinimizedPushEventIsValid(ClientBaseEvent event) {
        if (!(event instanceof ClientMinimizableEvent)) {
            return;
        }
        ClientMinimizableEvent minimizableEvent = (ClientMinimizableEvent) event;
        ClientBaseEvent minimizedEvent = minimizableEvent.toMinimizedEvent();

        try {
            String eventJson = defaultJsonWriter().writeValueAsString(minimizableEvent);
            String minimizedEventJson = defaultJsonWriter().writeValueAsString(minimizedEvent);
            try {
                //The comparison mode LENIENT means that even if the actual JSON contains extended fields, the test will still pass
                JSONAssert.assertEquals("Minimized event has different field values then the original event",
                        minimizedEventJson, eventJson, JSONCompareMode.LENIENT);
            } catch (AssertionError e) {
                throw new AssertionError(
                        e.getMessage() + "\nminimized:\n" + minimizedEventJson + "\noriginal:\n" + eventJson + "\n");
            }
        } catch (JsonProcessingException | JSONException e) {
            throw new AssertionError("Could not check minimized event", e);
        }
    }

}
