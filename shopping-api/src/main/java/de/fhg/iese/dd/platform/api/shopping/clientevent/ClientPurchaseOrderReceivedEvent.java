/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping.clientevent;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderItem;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor //due to legacy in test
public class ClientPurchaseOrderReceivedEvent extends ClientBaseEvent {

    @NotBlank
    @ApiModelProperty(required = true)
    private String shopOrderId;
    @NotBlank
    @ApiModelProperty(required = true)
    private String communityId;
    @NotBlank
    @ApiModelProperty(required = true)
    private String senderShopId;
    private String purchaseOrderNotes;
    @NotNull
    @ApiModelProperty(required = true)
    private ClientAddressDefinition pickupAddress;
    @NotBlank
    @ApiModelProperty(required = true)
    private String receiverPersonId;
    @NotNull
    @ApiModelProperty(required = true)
    private ClientAddressDefinition deliveryAddress;
    private String transportNotes;
    private List<ClientPurchaseOrderItem> orderedItems;
    @IgnoreArchitectureViolation(
            value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
            reason = "Legacy implementation")
    private TimeSpan desiredDeliveryTime;
    private String deliveryPoolingStationId;
    private int credits;

}
