/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping.clientevent;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.participants.shop.clientmodel.ClientOpeningHoursEntry;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import java.util.Set;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientShopCreateRequest extends ClientBaseEvent {

    @NotBlank
    @ApiModelProperty(required = true)
    private String tenantId;
    @NotBlank
    @ApiModelProperty(required = true)
    private String name;
    @NotBlank
    @ApiModelProperty(required = true)
    private String email;
    private String phone;
    private String webShopURL;
    private ClientAddressDefinition address;
    private String profilePictureURL;
    private Set<ClientOpeningHoursEntry> openingHours;

}
