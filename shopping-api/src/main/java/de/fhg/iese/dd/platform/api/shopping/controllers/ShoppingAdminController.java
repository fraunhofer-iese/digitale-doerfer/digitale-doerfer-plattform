/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.AddressClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForTransportConfirmation;
import de.fhg.iese.dd.platform.business.logistics.services.IPoolingStationService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.shop.services.IShopService;
import de.fhg.iese.dd.platform.business.participants.tenant.exceptions.TenantNotFoundException;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.address.exceptions.AddressInvalidException;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.business.shared.misc.services.IRandomService;
import de.fhg.iese.dd.platform.business.shared.security.SuperAdminAction;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderCreatedEvent;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderReadyForTransportRequest;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderReceivedEvent;
import de.fhg.iese.dd.platform.business.shopping.services.IDemoShoppingService;
import de.fhg.iese.dd.platform.business.shopping.services.IPurchaseOrderService;
import de.fhg.iese.dd.platform.business.shopping.services.IShoppingScoreService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Dimension;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PackagingType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

import static de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrderItem.builder;

@RestController
@RequestMapping("/administration/shopping")
@Api(tags = {"admin","shopping.admin"})
class ShoppingAdminController extends BaseController {

    @Autowired
    private IDemoShoppingService demoShoppingService;

    @Autowired
    private IShoppingScoreService shoppingScoreService;

    @Autowired
    private IPersonService personService;
    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IShopService shopService;
    @Autowired
    private IPoolingStationService poolingStationService;
    @Autowired
    private IAddressService addressService;
    @Autowired
    private IPurchaseOrderService purchaseOrderService;
    @Autowired
    private IRandomService randomUtil;
    @Autowired
    private AddressClientModelMapper addressClientModelMapper;

    @ApiOperation(value = "Delete shopping data in demo tenant",
            notes = "Deletes all shopping data in the given demo tenant")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @DeleteMapping("deleteDemoShoppingData")
    public void deleteDemoLogisticsData(@RequestParam String communityId) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        demoShoppingService.deleteDataForDemoTenant(communityId);
    }

    @ApiOperation(value = "Rebook wrong sales commissions",
            notes = "Due to an error sales commissions were deducted from receivers accounts instead of shop accounts. " +
                    "This fixes it for existing entries.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping("score/rebookSalesCommission")
    public void rebookSalesCommission() {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        shoppingScoreService.rebookWrongSalesCommissions();
    }

    @ApiOperation(value = "Create random purchase order and delivery for testing purpose",
            notes = """
                    Creates a random order and marks it as ready for delivery, so that a delivery is created.

                    Provide the personId of the receiver.

                    If no address is provided the first address of the person is taken.

                    If a pooling station id is provided the delivery is shipped to that station.

                    Returns the delivery ID and the tracking code URL.""")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping("randomDelivery")
    public String randomDelivery(
            @RequestParam String receiverPersonId,
            @RequestParam(required = false) String tenantId,
            @RequestParam(required = false) String shopId,
            @RequestParam(required = false) String poolingStationId,
            @RequestBody(required = false) ClientAddressDefinition deliveryAddressDefinition
    ) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        Person receiver = personService.findPersonById(receiverPersonId);

        Tenant tenant;
        if (StringUtils.isEmpty(tenantId)) {
            tenant = receiver.getTenant();
            if (tenant == null) {
                throw new TenantNotFoundException("User is not any tenant");
            }
        } else {
            tenant = tenantService.findTenantById(tenantId);
        }

        Address deliveryAddress;
        if (deliveryAddressDefinition != null) {
            deliveryAddress = addressService.findOrCreateAddress(
                    addressClientModelMapper.toAddressDefinition(deliveryAddressDefinition),
                    IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                    IAddressService.GPSResolutionStrategy.LOOKEDUP_NONE,
                    false);
        } else {
            final List<AddressListEntry> addressListEntries = personService.getAddressListEntries(receiver);
            if (!CollectionUtils.isEmpty(addressListEntries)) {
                deliveryAddress = addressListEntries.get(0).getAddress();
            } else {
                throw new AddressInvalidException("{} has no addresses", receiver.toString());
            }
        }

        Shop shop;
        if (StringUtils.isEmpty(shopId)) {
            shop = shopService.findAnyByTenant(tenant);
        } else {
            shop = shopService.findShopById(shopId);
        }

        PoolingStation poolingStation = null;
        if (!StringUtils.isEmpty(poolingStationId)) {
            poolingStation = poolingStationService.findPoolingStationById(poolingStationId);
        }

        PurchaseOrderReceivedEvent purchaseOrderReceivedEvent = new PurchaseOrderReceivedEvent();
        purchaseOrderReceivedEvent.setShopOrderId(randomUtil.randomNumberString(5));
        purchaseOrderReceivedEvent.setTenant(tenant);
        purchaseOrderReceivedEvent.setSender(shop);
        purchaseOrderReceivedEvent.setPurchaseOrderNotes("This is a purchase order note.");
        purchaseOrderReceivedEvent.setPickupAddress(shop.getAddresses().iterator().next().getAddress());
        purchaseOrderReceivedEvent.setReceiver(receiver);
        purchaseOrderReceivedEvent.setTransportNotes("This is a transport note.");
        purchaseOrderReceivedEvent.setDeliveryPoolingStation(poolingStation);
        purchaseOrderReceivedEvent.setDeliveryAddress(deliveryAddress);
        purchaseOrderReceivedEvent.setDesiredDeliveryTime(randomUtil.randomTimeSpan());
        purchaseOrderReceivedEvent.setCredits(10);

        purchaseOrderReceivedEvent.setOrderedItems(Collections.singletonList(builder()
                .amount(randomUtil.randomInt(1, 42))
                .itemName("RandomProduct" + randomUtil.randomNumberString(1))
                .unit("Stück")
                .build()));

        PurchaseOrderCreatedEvent poce =
                notifyAndWaitForReply(PurchaseOrderCreatedEvent.class, purchaseOrderReceivedEvent);

        Delivery delivery = poce.getPurchaseOrder().getDelivery();

        PurchaseOrder purchaseOrder = purchaseOrderService.findByDelivery(delivery);

        PurchaseOrderReadyForTransportRequest readyForTransportRequest = new PurchaseOrderReadyForTransportRequest(
                purchaseOrder,
                null,
                Dimension.builder()
                        .length(40.0)
                        .width(30.0)
                        .height(20.0)
                        .weight(0.7)
                        .build(),
                "This is a content note.",
                PackagingType.PARCEL,
                1
        );

        DeliveryReadyForTransportConfirmation deliveryReadyForTransportConfirmation =
                notifyAndWaitForReply(DeliveryReadyForTransportConfirmation.class, readyForTransportRequest);

        delivery = deliveryReadyForTransportConfirmation.getDelivery();

        return "DeliveryId: " + delivery.getId() + ",\n"
                + "Tracking Code: " + delivery.getTrackingCode() + ",\n"
                + "Label: " + delivery.getTrackingCodeLabelURL() + ",\n"
                + "TenantId: " + tenant.getId() + ",\n"
                + "Delivery: " + delivery + ",\n"
                + "PurchaseOrder: " + purchaseOrder.toString() + ",\n"
                ;
    }

}
