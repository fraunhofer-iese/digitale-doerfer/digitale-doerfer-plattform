/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Axel Wickenkamp, Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping.clientevent;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientDimension;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderParcel;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PackagingType;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor //due to legacy in test
public class ClientPurchaseOrderReadyForTransportRequest extends ClientBaseEvent {

    @NotBlank
    @ApiModelProperty(required = true)
    private String shopOrderId;
    private ClientAddressDefinition pickupAddress;
    @NotNull
    @ApiModelProperty(required = true)
    private ClientDimension size;
    private String contentNotes;
    @NotNull
    @ApiModelProperty(required = true)
    private PackagingType packagingType;
    @Positive(message = "Num parcels must not be negative or zero!")
    @Builder.Default
    private int numParcels = 1;
    private List<ClientPurchaseOrderParcel> additionalParcels;

}
