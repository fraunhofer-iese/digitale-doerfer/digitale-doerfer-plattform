/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Steffen Hupp, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.LogisticsClientModelMapper;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderParcel;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.datamanagement.shopping.ShoppingConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/shopping")
@Api(tags={"shopping"})
public class ShoppingController extends BaseController{

    @Autowired
    private IDeliveryService deliveryService;
    @Autowired
    private LogisticsClientModelMapper logisticsClientModelMapper;

    @ApiOperation(value = "Get all parcels for a specific reference number.")
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION,
            notes = "API key is configured in BestellBar app variant")
    @GetMapping("/apiKey/parcels")
    public List<ClientPurchaseOrderParcel> getParcelsByShopReferenceId(
            @RequestParam String referenceNumber)  {

        validateAuthenticatedAppVariantAgainstApp(getAppService().findById(ShoppingConstants.BESTELLBAR_APP_ID));

        return deliveryService.findAllDeliveriesByCustomReferenceNumber(referenceNumber)
                .stream()
                .map(d -> ClientPurchaseOrderParcel.builder()
                        .trackingCodeLabelURL(d.getTrackingCodeLabelURL())
                        .contentNotes(d.getContentNotes())
                        .packagingType(d.getPackagingType())
                        .size(logisticsClientModelMapper.toClientDimension(d.getSize()))
                        .deliveryId(d.getId())
                        .build())
                .collect(Collectors.toList());
    }

}
