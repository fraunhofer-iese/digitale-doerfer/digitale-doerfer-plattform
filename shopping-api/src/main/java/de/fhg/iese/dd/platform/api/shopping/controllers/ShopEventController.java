/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel, Axel Wickenkamp, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.api.participants.shop.clientmodel.ShopClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.AddressClientModelMapper;
import de.fhg.iese.dd.platform.api.shopping.clientevent.*;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.shop.services.IShopService;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailAddressVerificationService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.business.shared.security.services.PermissionEntityRestricted;
import de.fhg.iese.dd.platform.business.shopping.events.*;
import de.fhg.iese.dd.platform.business.shopping.security.ShopCreateUpdateDeleteAction;
import de.fhg.iese.dd.platform.business.shopping.security.ShopReadUpdateOwnShopAction;
import de.fhg.iese.dd.platform.business.shopping.security.ShopReadUpdateOwnerAction;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHours;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.repos.OpeningHoursEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.repos.OpeningHoursRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.MalformedURLException;
import java.net.URL;

@RestController
@RequestMapping("/shop/event")
@Api(tags={"participants.shop.events"})
class ShopEventController extends BaseController {

    @Autowired
    private IPersonService personService;
    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IShopService shopService;
    @Autowired
    private IAddressService addressService;
    @Autowired
    public OpeningHoursRepository openingHoursRepository;
    @Autowired
    public OpeningHoursEntryRepository openingHoursEntryRepository;
    @Autowired
    public IEmailAddressVerificationService emailAddressVerificationService;
    @Autowired
    public IRoleService roleService;
    @Autowired
    private AddressClientModelMapper addressClientModelMapper;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;
    @Autowired
    private ShopClientModelMapper shopClientModelMapper;

    @ApiOperation(value = "Creates a new shop.",
            notes = "All parameters except tenantId, name and email are required")
    @ApiExceptions({
            ClientExceptionType.PERSON_WITH_OAUTH_ID_NOT_FOUND,
            ClientExceptionType.EMAIL_ADDRESS_INVALID,
            ClientExceptionType.TENANT_NOT_FOUND,
            ClientExceptionType.ADDRESS_INVALID,
            ClientExceptionType.FILE_ITEM_UPLOAD_FAILED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = ShopCreateUpdateDeleteAction.class)
    @PostMapping("shopCreateRequest")
    public ClientShopCreateConfirmation onShopCreateRequest(
            @Valid @RequestBody ClientShopCreateRequest request) {

        Person currentUser = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantOrNull();
        PermissionTenantRestricted permission =
                getRoleService().decidePermissionAndThrowNotAuthorized(ShopCreateUpdateDeleteAction.class, currentUser);
        Tenant tenant = tenantService.findTenantById(request.getTenantId());

        if (permission.isTenantDenied(tenant)) {
            throw new NotAuthorizedException("No permission to create shop in tenant '{}'", tenant.getId());
        }

        String shopEmail = emailAddressVerificationService.ensureCorrectEmailSyntaxAndNormalize(request.getEmail());

        Address address = request.getAddress() == null ? null :
                addressService.findOrCreateAddress(
                        addressClientModelMapper.toAddressDefinition(request.getAddress()),
                        IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                        IAddressService.GPSResolutionStrategy.LOOKEDUP_NONE,
                        true);

        OpeningHours openingHours = shopClientModelMapper.fromClientOpeningHoursEntries(request.getOpeningHours());

        ShopCreateRequest shopCreateRequest = ShopCreateRequest.builder()
                .tenant(tenant)
                .creator(currentUser)
                .appVariant(appVariant)
                .name(request.getName())
                .email(shopEmail)
                .phone(request.getPhone())
                .webShopURL(request.getWebShopURL())
                .address(address)
                .profilePictureURL(createProfilePictureURL(request.getProfilePictureURL()))
                .openingHours(openingHours)
                .build();

        ShopCreateConfirmation shopCreateConfirmation =
                notifyAndWaitForReply(ShopCreateConfirmation.class, shopCreateRequest);

        return new ClientShopCreateConfirmation(
                shopClientModelMapper.createClientShop(shopCreateConfirmation.getShop()));
    }

    @ApiOperation(value = "Updates data of a specific shop.",
            notes = "Can be triggered by a shop manager of the tenant the shop is in or the shop owner of the shop.")
    @ApiExceptions({
            ClientExceptionType.EMAIL_ADDRESS_INVALID,
            ClientExceptionType.TENANT_NOT_FOUND,
            ClientExceptionType.ADDRESS_INVALID,
            ClientExceptionType.FILE_ITEM_UPLOAD_FAILED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ShopCreateUpdateDeleteAction.class, ShopReadUpdateOwnShopAction.class})
    @PostMapping("shopUpdateRequest")
    public ClientShopUpdateConfirmation onShopUpdateRequest(
            @Valid @RequestBody ClientShopUpdateRequest request) {

        Person currentUser = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantOrNull();
        PermissionTenantRestricted permissionTenantRestricted =
                getRoleService().decidePermission(ShopCreateUpdateDeleteAction.class, currentUser);
        PermissionEntityRestricted<Shop> permissionShopRestricted =
                getRoleService().decidePermission(ShopReadUpdateOwnShopAction.class, currentUser);
        Shop shop = shopService.findShopById(request.getShopId());

        if (permissionTenantRestricted.isTenantDenied(shop.getTenant()) &&
                permissionShopRestricted.isEntityDenied(shop)) {
            throw new NotAuthorizedException("No permission to update shop  '{}'", shop.getId());
        }

        String shopEmail;
        if (StringUtils.isNotEmpty(request.getEmail())) {
            shopEmail = emailAddressVerificationService.ensureCorrectEmailSyntaxAndNormalize(request.getEmail());
        } else {
            shopEmail = null;
        }

        Address address = request.getAddress() == null ? null :
                addressService.findOrCreateAddress(
                        addressClientModelMapper.toAddressDefinition(request.getAddress()),
                        IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                        IAddressService.GPSResolutionStrategy.LOOKEDUP_NONE,
                        true);

        OpeningHours openingHours =
                shopClientModelMapper.fromClientOpeningHoursEntries(request.getOpeningHours());

        ShopUpdateRequest shopUpdateRequest = ShopUpdateRequest.builder()
                .shop(shop)
                .appVariant(appVariant)
                .name(request.getName())
                .email(shopEmail)
                .phone(request.getPhone())
                .webShopURL(request.getWebShopURL())
                .address(address)
                .profilePictureURL(createProfilePictureURL(request.getProfilePictureURL()))
                .openingHours(openingHours)
                .build();

        ShopUpdateConfirmation shopUpdateConfirmation =
                notifyAndWaitForReply(ShopUpdateConfirmation.class, shopUpdateRequest);

        return new ClientShopUpdateConfirmation(
                shopClientModelMapper.createClientShop(shopUpdateConfirmation.getShop()));
    }

    @ApiOperation(value = "Set a new shop owner.",
            notes = "The new shop owner can either be found by personId or by email. If both are set, the personId is preferred.")
    @ApiExceptions({
            ClientExceptionType.EMAIL_ADDRESS_INVALID,
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.PERSON_WITH_EMAIL_NOT_FOUND,
            ClientExceptionType.FILE_ITEM_UPLOAD_FAILED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = ShopReadUpdateOwnerAction.class)
    @PostMapping(value = "shopSetOwnerRequest")
    public ClientShopSetOwnerConfirmation onShopSetOwnerRequest(
            @Valid @RequestBody ClientShopSetOwnerRequest clientShopSetOwnerRequest) {

        Person currentUser = getCurrentPersonNotNull();

        PermissionTenantRestricted permission =
                getRoleService().decidePermissionAndThrowNotAuthorized(ShopReadUpdateOwnerAction.class, currentUser);

        Shop shop = shopService.findShopById(clientShopSetOwnerRequest.getShopId());
        if (permission.isTenantDenied(shop.getTenant())) {
            throw new NotAuthorizedException("No permission to set owner of shop '{}'", shop.getId());
        }

        Person newOwner;
        String personId = clientShopSetOwnerRequest.getPersonId();
        String email = clientShopSetOwnerRequest.getEmail();
        if (StringUtils.isNotEmpty(personId)) {
            newOwner = personService.findPersonById(personId);
        } else if (StringUtils.isNotEmpty(email)) {
            newOwner = personService.findPersonByEmail(email);
        } else {
            throw new BadRequestException("personId or email is required");
        }

        ShopSetOwnerRequest shopSetOwnerRequest = ShopSetOwnerRequest.builder()
                .shop(shop)
                .newOwner(newOwner)
                .build();

        ShopSetOwnerConfirmation shopSetOwnerConfirmation =
                notifyAndWaitForReply(ShopSetOwnerConfirmation.class, shopSetOwnerRequest);

        return new ClientShopSetOwnerConfirmation(
                personClientModelMapper.createClientPersonReference(shopSetOwnerConfirmation.getOwner()));
    }

    @ApiOperation(value = "Deletes a specific shop.",
            notes = "Shops with existing orders can not be deleted.")
    @ApiExceptions({
            ClientExceptionType.SHOP_NOT_FOUND,
            ClientExceptionType.SHOP_NOT_DELETABLE})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = ShopCreateUpdateDeleteAction.class)
    @PostMapping(value = "shopDeleteRequest")
    public ClientShopDeleteConfirmation onShopDeleteRequest(
            @Valid @RequestBody ClientShopDeleteRequest clientShopDeleteRequest) {

        PermissionTenantRestricted permission =
                getRoleService().decidePermissionAndThrowNotAuthorized(ShopCreateUpdateDeleteAction.class,
                        getCurrentPersonNotNull());
        Shop shop = shopService.findShopById(clientShopDeleteRequest.getShopId());
        if (permission.isTenantDenied(shop.getTenant())) {
            throw new NotAuthorizedException("No permission to delete shop '{}'", shop.getId());
        }
        ShopDeleteConfirmation shopDeleteConfirmation = notifyAndWaitForReply(ShopDeleteConfirmation.class,
                new ShopDeleteRequest(shop));
        return new ClientShopDeleteConfirmation(shopDeleteConfirmation.getShopId());
    }

    private URL createProfilePictureURL(String clientProfilePictureURL) {

        if (StringUtils.isEmpty(clientProfilePictureURL)) {
            return null;
        }
        try {
            return new URL(clientProfilePictureURL);
        } catch (MalformedURLException e) {
            throw new BadRequestException("Invalid profile picture URL: " + e);
        }
    }

}
