/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Balthasar Weitzel, Axel Wickenkamp, Dominik Schnier, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping.controllers;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidEventAttributeException;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientDeliveryCreatedEvent;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.LogisticsClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.AddressClientModelMapper;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReadyForTransportConfirmation;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReadyForTransportRequest;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReceivedEvent;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderItem;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderParcel;
import de.fhg.iese.dd.platform.api.shopping.exceptions.PurchaseOrderInvalidException;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForTransportConfirmation;
import de.fhg.iese.dd.platform.business.logistics.services.IPoolingStationService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.shop.services.IShopService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.address.exceptions.AddressInvalidException;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailAddressVerificationService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderCreatedEvent;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderReadyForTransportRequest;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderReceivedEvent;
import de.fhg.iese.dd.platform.business.shopping.exceptions.PurchaseOrderNotFoundException;
import de.fhg.iese.dd.platform.business.shopping.services.IPurchaseOrderService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.repos.OpeningHoursEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.repos.OpeningHoursRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shopping.ShoppingConstants;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrderItem;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrderParcel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/shopping/event")
@Api(tags={"shopping.events"})
class ShoppingEventController extends BaseController {

    @Autowired
    private IPersonService personService;
    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IPoolingStationService poolingStationService;
    @Autowired
    private IShopService shopService;
    @Autowired
    private IPurchaseOrderService purchaseOrderService;
    @Autowired
    private IAddressService addressService;
    @Autowired
    public OpeningHoursRepository openingHoursRepository;
    @Autowired
    public OpeningHoursEntryRepository openingHoursEntryRepository;
    @Autowired
    public IEmailAddressVerificationService emailAddressVerificationService;
    @Autowired
    public IRoleService roleService;
    @Autowired
    private LogisticsClientModelMapper logisticsClientModelMapper;
    @Autowired
    private AddressClientModelMapper addressClientModelMapper;

    @ApiOperation(value = "Notifies about a new purchase order at the web shop.")
    @ApiExceptions({
            ClientExceptionType.NOT_AUTHENTICATED,
            ClientExceptionType.PURCHASE_ORDER_INVALID,
            ClientExceptionType.ADDRESS_INVALID
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION,
            notes = "API key is configured in BestellBar AppVariant")
    @PostMapping("purchaseorderreceived")
    public ClientDeliveryCreatedEvent purchaseOrderReceived(
            @Valid @RequestBody ClientPurchaseOrderReceivedEvent clientPurchaseOrderReceivedEvent) {

        Tenant tenant = tenantService.findTenantById(clientPurchaseOrderReceivedEvent.getCommunityId());
        validateAuthenticatedAppVariantAgainstAppAndTenant(
                getAppService().findById(ShoppingConstants.BESTELLBAR_APP_ID),
                tenant);

        if (purchaseOrderService.findByShopOrderId(clientPurchaseOrderReceivedEvent.getShopOrderId()) != null) {
            throw new PurchaseOrderInvalidException(
                    "shopOrderId " + clientPurchaseOrderReceivedEvent.getShopOrderId() + " already exists!").withDetail(
                    "shopOrderId");
        }

        // check for valid addresses
        Address pickupAddress = checkAndFindOrCreateAddress(
                addressClientModelMapper.toAddressDefinition(clientPurchaseOrderReceivedEvent.getPickupAddress()),
                "pickup");
        Address deliveryAddress = checkAndFindOrCreateAddress(
                addressClientModelMapper.toAddressDefinition(clientPurchaseOrderReceivedEvent.getDeliveryAddress()),
                "delivery");

        // check for valid desired delivery time (end after start and start not longer than 15 minutes ago)
        if (clientPurchaseOrderReceivedEvent.getDesiredDeliveryTime().getStartTime()
                > clientPurchaseOrderReceivedEvent.getDesiredDeliveryTime().getEndTime()) {
            throw new PurchaseOrderInvalidException("The desired delivery time cannot end before its start").withDetail(
                    "desiredDeliveryTime");
        }
        if (clientPurchaseOrderReceivedEvent.getDesiredDeliveryTime().getStartTime() <
                getTimeService().currentTimeMillisUTC() - TimeUnit.MINUTES.toMillis(15)) {
            throw new PurchaseOrderInvalidException("The desired delivery time cannot start in the past").withDetail(
                    "desiredDeliveryTime");
        }

        // check for valid entities
        Person receiver = personService.findPersonById(clientPurchaseOrderReceivedEvent.getReceiverPersonId());
        Shop shop = shopService.findShopById(clientPurchaseOrderReceivedEvent.getSenderShopId());

        PoolingStation poolingStation = null;
        String poolingStationId = clientPurchaseOrderReceivedEvent.getDeliveryPoolingStationId();
        if (StringUtils.isNotEmpty(poolingStationId)) {
            poolingStation = poolingStationService.findPoolingStationById(poolingStationId);
        }

        List<PurchaseOrderItem> purchaseOrderItems =
                createPurchaseOrderItems(clientPurchaseOrderReceivedEvent.getOrderedItems());

        PurchaseOrderReceivedEvent purchaseOrderReceivedEvent = new PurchaseOrderReceivedEvent();
        purchaseOrderReceivedEvent.setShopOrderId(clientPurchaseOrderReceivedEvent.getShopOrderId());
        purchaseOrderReceivedEvent.setTenant(tenant);
        purchaseOrderReceivedEvent.setSender(shop);
        purchaseOrderReceivedEvent.setPurchaseOrderNotes(clientPurchaseOrderReceivedEvent.getPurchaseOrderNotes());
        purchaseOrderReceivedEvent.setPickupAddress(pickupAddress);
        purchaseOrderReceivedEvent.setReceiver(receiver);
        purchaseOrderReceivedEvent.setTransportNotes(clientPurchaseOrderReceivedEvent.getTransportNotes());
        purchaseOrderReceivedEvent.setDeliveryPoolingStation(poolingStation);
        purchaseOrderReceivedEvent.setDeliveryAddress(deliveryAddress);
        purchaseOrderReceivedEvent.setDesiredDeliveryTime(clientPurchaseOrderReceivedEvent.getDesiredDeliveryTime());
        purchaseOrderReceivedEvent.setOrderedItems(purchaseOrderItems);
        purchaseOrderReceivedEvent.setCredits(clientPurchaseOrderReceivedEvent.getCredits());

        PurchaseOrderCreatedEvent poce =
                notifyAndWaitForReply(PurchaseOrderCreatedEvent.class, purchaseOrderReceivedEvent);

        Delivery delivery = poce.getPurchaseOrder().getDelivery();

        return new ClientDeliveryCreatedEvent(delivery.getId(), delivery.getTrackingCode(),
                poce.getPurchaseOrder().getId());
    }

    @ApiOperation(value = "Notifies that the parcel for a purchase order is ready to be shipped.")
    @ApiExceptions({
            ClientExceptionType.PURCHASE_ORDER_NOT_FOUND,
            ClientExceptionType.ADDRESS_INVALID
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION,
            notes = "API key is configured in BestellBar AppVariant")
    @PostMapping("/purchaseorderreadyfortransport")
    public ClientPurchaseOrderReadyForTransportConfirmation purchaseOrderReadyForTransport(
            @Valid @RequestBody ClientPurchaseOrderReadyForTransportRequest event) {

        // check order exists
        PurchaseOrder purchaseOrder = purchaseOrderService.findByShopOrderId(event.getShopOrderId());
        if (purchaseOrder == null) {
            throw new PurchaseOrderNotFoundException("for ShopOrder " + event.getShopOrderId());
        }

        validateAuthenticatedAppVariantAgainstAppAndTenant(
                getAppService().findById(ShoppingConstants.BESTELLBAR_APP_ID),
                purchaseOrder.getTenant());

        if (event.getAdditionalParcels() != null) {
            for (ClientPurchaseOrderParcel parcel : event.getAdditionalParcels()) {
                if (parcel.getPackagingType() == null) {
                    throw new InvalidEventAttributeException("packagingType is not defined").withDetail(
                            "packagingType");
                }

                if (parcel.getSize() == null)
                    throw new InvalidEventAttributeException("size is not defined").withDetail("size");
            }
        }

        if (event.getNumParcels() <= 0)
            throw new InvalidEventAttributeException("num parcels out of range").withDetail("numParcels");

        Address pickupAddress = null;
        if (event.getPickupAddress() != null) {
            pickupAddress =
                    checkAndFindOrCreateAddress(addressClientModelMapper.toAddressDefinition(event.getPickupAddress()),
                            "pickup");
        }

        PurchaseOrderReadyForTransportRequest readyForTransportRequest = new PurchaseOrderReadyForTransportRequest(
                purchaseOrder,
                pickupAddress,
                logisticsClientModelMapper.fromClientDimension(event.getSize()).toNormalizedDimension(),
                event.getContentNotes(),
                event.getPackagingType(),
                event.getNumParcels(),
                event.getAdditionalParcels() != null ?
                        event.getAdditionalParcels().stream()
                                .map(p -> PurchaseOrderParcel.builder()
                                        .contentNotes(p.getContentNotes())
                                        .packagingType(p.getPackagingType())
                                        .size(logisticsClientModelMapper.fromClientDimension(p.getSize())
                                                .toNormalizedDimension())
                                        .build())
                                .collect(Collectors.toList())
                        : null
        );

        DeliveryReadyForTransportConfirmation deliveryReadyForTransportConfirmation =
                notifyAndWaitForReply(DeliveryReadyForTransportConfirmation.class, readyForTransportRequest);

        return new ClientPurchaseOrderReadyForTransportConfirmation(
                deliveryReadyForTransportConfirmation.getDelivery().getTrackingCodeLabelURL(),
                deliveryReadyForTransportConfirmation.getDelivery().getTrackingCodeLabelA4URL());
    }

    private List<PurchaseOrderItem> createPurchaseOrderItems(List<ClientPurchaseOrderItem> clientPurchaseOrderItems) {

        if (CollectionUtils.isEmpty(clientPurchaseOrderItems)) {
            return Collections.emptyList();
        }

        return clientPurchaseOrderItems.stream()
                .map(clientPurchaseOrderItem -> PurchaseOrderItem.builder()
                        .amount(clientPurchaseOrderItem.getAmount())
                        .itemName(clientPurchaseOrderItem.getItemName())
                        .unit(clientPurchaseOrderItem.getUnit())
                        .build())
                .collect(Collectors.toList());
    }

    private Address checkAndFindOrCreateAddress(IAddressService.AddressDefinition address, String message) throws AddressInvalidException {
        addressService.checkAddress(address, message);
        return addressService.findOrCreateAddress(address,
                IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                IAddressService.GPSResolutionStrategy.LOOKEDUP_NONE,
                true);
    }

}
