/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2022 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonReference;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.api.participants.shop.clientmodel.ClientShop;
import de.fhg.iese.dd.platform.api.participants.shop.clientmodel.ShopClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.shop.services.IShopService;
import de.fhg.iese.dd.platform.business.participants.tenant.exceptions.TenantNotFoundException;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.security.services.PermissionEntityRestricted;
import de.fhg.iese.dd.platform.business.shopping.security.ShopCreateUpdateDeleteAction;
import de.fhg.iese.dd.platform.business.shopping.security.ShopReadUpdateOwnShopAction;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.repos.ShopRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/shop")
@Api(tags={"participants.shop"})
class ShopController extends BaseController {

    @Autowired
    private ShopRepository shopRepository;
    @Autowired
    private IShopService shopService;
    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IAppService appService;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;
    @Autowired
    private ShopClientModelMapper shopClientModelMapper;

    @ApiOperation(value = "Returns all shops of the given tenant")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2_OPTIONAL)
    @GetMapping
    public List<ClientShop> getAllShops(
            @ApiParam(
                    value = "When not set, the IDs of the tenants/communities of all subscribed geo areas are used. If unauthorized, this parameter must be set")
            @RequestParam(required = false) String communityId) {

        final Person requester = getCurrentPersonOrNull();

        Set<Tenant> tenants = null;

        if(StringUtils.isNotBlank(communityId)){
            tenants = Collections.singleton(tenantService.findTenantById(communityId));
        } else {
            if(requester == null) {
                throw new TenantNotFoundException("Could not find tenant '{}' (tenant id is empty and requesting person could not be determined).", communityId);
            }
            final AppVariant appVariant = getAppVariantNotNull();
            tenants = appService.getTenantsOfSelectedGeoAreas(appVariant, requester);
        }

        if(tenants.isEmpty()) {
            throw new TenantNotFoundException("Could not find tenants for requesting person or for id '{}'", communityId);
        }

        List<Shop> shops = shopRepository.findAllByCommunityInOrderByCreatedAsc(tenants);
        return shops.stream()
                .map(shop -> shopClientModelMapper.createClientShop(shop))
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Returns the shop for the given shop id")
    @ApiExceptions({
            ClientExceptionType.SHOP_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping("/{shopId}")
    public ClientShop getShop(
            @PathVariable String shopId) {

        return shopClientModelMapper.createClientShop(shopService.findShopById(shopId));
    }

    @ApiOperation(value = "Get all owners of a specific shop")
    @ApiExceptions({
            ClientExceptionType.SHOP_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ShopCreateUpdateDeleteAction.class, ShopReadUpdateOwnShopAction.class})
    @GetMapping("/{shopId}/owner")
    public List<ClientPersonReference> getShopOwnersByShopId(
            @PathVariable String shopId
    ) {

        Person currentUser = getCurrentPersonNotNull();
        PermissionTenantRestricted permissionTenantRestricted =
                getRoleService().decidePermission(ShopCreateUpdateDeleteAction.class, currentUser);
        PermissionEntityRestricted<Shop> permissionShopRestricted =
                getRoleService().decidePermission(ShopReadUpdateOwnShopAction.class, currentUser);
        Shop shop = shopService.findShopById(shopId);

        if (permissionTenantRestricted.isTenantDenied(shop.getTenant()) &&
                permissionShopRestricted.isEntityDenied(shop)) {
            throw new NotAuthorizedException("No permission to get owners of shop '{}'", shop.getId());
        }

        return shopService.findAllShopOwners(shop).stream()
                .map(person -> personClientModelMapper.createClientPersonReference(person))
                .collect(Collectors.toList());
    }

}
