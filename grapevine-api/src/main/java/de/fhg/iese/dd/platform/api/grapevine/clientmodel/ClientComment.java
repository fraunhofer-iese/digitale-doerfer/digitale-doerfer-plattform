/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Adeline Silva Schäfer, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientmodel;

import de.fhg.iese.dd.platform.api.framework.clientmodel.FilteredClientBaseEntity;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonReference;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientDocumentItem;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItem;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientComment extends FilteredClientBaseEntity {

    private String postId;
    private ClientPersonReference creator;
    private ClientPersonReference replyTo;
    private List<ClientMediaItem> images;
    private List<ClientDocumentItem> attachments;
    private String text;
    private long created;
    private long lastModified;
    private boolean deleted;
    private long likeCount;
    @ApiModelProperty("""
            Can be "true", "false" or "null".

            Currently "null" is only used when pushing a CommentChangeEvent to a GeoArea.
            In this case, we can not reliably have an individual liked state for each person.
            Do not overwrite any cached liked state on this occasion.""")
    private Boolean liked;

}
