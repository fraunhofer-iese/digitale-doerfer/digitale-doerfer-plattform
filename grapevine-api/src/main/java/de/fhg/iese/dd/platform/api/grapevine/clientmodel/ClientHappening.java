/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2018 Adeline Silva Schäfer, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddress;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientHappening extends ClientExternalPost {

    private String organizer;
    private long startTime;
    private long endTime;
    private boolean allDayEvent;
    private long participantCount;
    private Boolean participated;

    @Deprecated
    @JsonProperty("venue")
    @ApiModelProperty(hidden = true)
    public String getVenue() {
        if (getCustomAddress() == null) {
            return null;
        }
        return getCustomAddress().getName();
    }

    @Deprecated
    @JsonProperty("venueAddress")
    @ApiModelProperty(hidden = true)
    public ClientAddress getVenueAddress() {
        return getCustomAddress();
    }

}
