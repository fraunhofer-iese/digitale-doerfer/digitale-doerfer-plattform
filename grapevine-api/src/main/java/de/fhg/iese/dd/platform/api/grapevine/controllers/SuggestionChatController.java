/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChatExtended;
import de.fhg.iese.dd.platform.api.communication.clientmodel.CommunicationClientModelMapper;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineChatService;
import de.fhg.iese.dd.platform.business.grapevine.services.ISuggestionService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionFirstResponder;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionWorker;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("grapevine/suggestion/chat")
@Api(tags = { "grapevine.suggestion.chat" })
class SuggestionChatController extends BaseController {

    @Autowired
    private IGrapevineChatService grapevineChatService;

    @Autowired
    private CommunicationClientModelMapper communicationClientModelMapper;

    @Autowired
    private ISuggestionService suggestionService;

    @ApiOperation(value = "Return all private and group chats the person is in.",
            notes = "returned chats include extended information (list of chat subjects)")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuggestionFirstResponder.class, SuggestionWorker.class})
    @GetMapping("own")
    public List<ClientChatExtended> getOwnChatsAndSuggestionInternalWorkerChats() {

        Person person = getCurrentPersonNotNull();

        suggestionService.checkSuggestionRoles(person);

        List<Chat> ownChatsAndSuggestionInternalWorkerChats =
                grapevineChatService.findOwnChatsAndSuggestionInternalWorkerChats(person);

        return communicationClientModelMapper.createClientChatsExtended(ownChatsAndSuggestionInternalWorkerChats,
                person);
    }

}
