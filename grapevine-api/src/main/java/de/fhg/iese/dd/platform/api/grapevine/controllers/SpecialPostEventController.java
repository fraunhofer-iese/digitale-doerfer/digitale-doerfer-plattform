/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2024 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.*;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.specialpost.ClientSpecialPostChangeRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.specialpost.ClientSpecialPostCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.exceptions.PostActionNotAllowedException;
import de.fhg.iese.dd.platform.business.grapevine.events.PostChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.specialpost.SpecialPostChangeRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.specialpost.SpecialPostCreateRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentCreationRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.SpecialPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SpecialPost;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/grapevine/event")
@Api(tags = {"grapevine.specialPost.events"})
class SpecialPostEventController extends BasePostEventController {

    @ApiOperation(value = "Creates a new SpecialPost",
            notes = "Creates a new SpecialPost, if the persons is allowed to. See exceptions for details.")
    @ApiExceptions({
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.POST_COULD_NOT_BE_CREATED})
    @ApiException(value = ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT,
            reason = "If the feature 'de.fhg.iese.dd.dorffunk.content.creation-restriction' is enabled " +
                    "for the app variant, users have to have at least one of the allowed verification " +
                    "status, as configured in the feature.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping(value = "specialPostCreateRequest")
    public ClientPostCreateConfirmation onSpecialPostCreateRequest(
            @Valid @RequestBody ClientSpecialPostCreateRequest request) {

        final Person currentPerson = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        checkMaxTextLength(SpecialPostTypeFeature.class, currentPerson, appVariant, request::getText);
        checkVerificationStatusRestriction(ContentCreationRestrictionFeature.class, currentPerson, appVariant);

        SpecialPostCreateRequest.SpecialPostCreateRequestBuilder<?, ?> requestBuilder =
                SpecialPostCreateRequest.builder()
                        .customAttributes(request.getCustomAttributes());
        setPostCreateByPersonRequestValues(requestBuilder, currentPerson, appVariant, request);

        final PostCreateConfirmation<?> createConfirmation =
                notifyAndWaitForReply(PostCreateConfirmation.class, requestBuilder.build());

        Post post = createConfirmation.getPost();
        return ClientPostCreateConfirmation.builder()
                .post(grapevineModelMapper.toClientPost(post, false))
                .organizationTags(grapevineModelMapper.getClientOrganizationTagsOrNull(post))
                .build();
    }

    @ApiOperation(value = "Updates an existing SpecialPost",
            notes = "Updates an existing SpecialPost. Only text, images and custom attributes can be updated.")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.FILE_ITEM_DUPLICATE_USAGE})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("specialPostChangeRequest")
    public ClientPostChangeConfirmation onSpecialPostChangeRequest(
            @Valid @RequestBody ClientSpecialPostChangeRequest request) {

        if (request.isUseEmptyFields()) {
            checkAttributeNotNullOrEmpty(request.getText(), "text");
        }

        final Person currentPerson = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        checkMaxTextLength(SpecialPostTypeFeature.class, currentPerson, appVariant, request::getText);

        final SpecialPost specialPost = postService.findSpecialPostById(request.getPostId());
        if (!currentPerson.equals(specialPost.getCreator())) {
            throw new PostActionNotAllowedException("Only the creator can edit the post");
        }

        SpecialPostChangeRequest.SpecialPostChangeRequestBuilder<?, ?> requestBuilder =
                SpecialPostChangeRequest.builder()
                        .customAttributes(request.getCustomAttributes());
        setPostChangeRequestByPersonValues(requestBuilder, specialPost, currentPerson, appVariant, request);
        final PostChangeConfirmation<?> changeConfirmation =
                notifyAndWaitForReply(PostChangeConfirmation.class, requestBuilder.build());

        final Post changedPost = changeConfirmation.getPost();
        return new ClientPostChangeConfirmation(
                grapevineModelMapper.toClientPost(changedPost,
                        postInteractionService.isPostLiked(currentPerson, changedPost)));
    }

}
