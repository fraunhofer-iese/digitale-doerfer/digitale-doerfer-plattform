/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Balthasar Weitzel, Benjamin Hassenfratz, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidEventAttributeException;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientBaseExternalPostCreateOrUpdateByNewsSourceRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientExternalPostCreateOrUpdateByNewsSourceConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientExternalPostDeleteConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientExternalPostDeleteRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.happening.ClientHappeningCreateOrUpdateByNewsSourceRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.newsitem.ClientNewsItemCreateOrUpdateByNewsSourceRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientCroppedImageReference;
import de.fhg.iese.dd.platform.api.grapevine.exceptions.PostInvalidException;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientLocationDefinition;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientAbsoluteImageCropDefinition;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientRelativeImageCropDefinition;
import de.fhg.iese.dd.platform.business.grapevine.events.PostChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostDeleteRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningChangeByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningCreateByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.newsitem.NewsItemChangeByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.newsitem.NewsItemCreateByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostTypeCannotBeChangedException;
import de.fhg.iese.dd.platform.business.grapevine.services.INewsSourceService;
import de.fhg.iese.dd.platform.business.grapevine.services.IOrganizationService;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.HappeningPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.NewsItemPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.NewsSourceFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.PostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileDownloadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemUploadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.InvalidImageCropDefinitionException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.*;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileDownloadService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import io.swagger.annotations.*;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.Validator;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/grapevine/event")
@Api(tags = {"grapevine.newsSource.events"})
public class NewsSourceEventController extends BasePostEventController {

    private static final String TEAM_NOTIFICATION_TOPIC_NEWS_CONFIG = "grapevine-news-config";

    @Autowired
    private INewsSourceService newsSourceService;
    @Autowired
    private IOrganizationService organizationService;
    @Autowired
    private IAddressService addressService;
    @Autowired
    private IPostService postService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private ITeamNotificationService teamNotificationService;
    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private IFileDownloadService fileDownloadService;
    @Autowired
    private Validator validator;

    @Component
    public static class StringToClientNewsItemCreateRequestConverter implements Converter<String, ClientNewsItemCreateOrUpdateByNewsSourceRequest> {

        @Autowired
        private ObjectMapper objectMapper;

        @Override
        @SneakyThrows
        public ClientNewsItemCreateOrUpdateByNewsSourceRequest convert(@NotNull String source) {
            return objectMapper.readValue(source, ClientNewsItemCreateOrUpdateByNewsSourceRequest.class);
        }

    }

    @ApiOperation(value = "Publishes a NewsItem into DorfFunk",
            notes = "The geo area has to be in the available geo areas of the app variant of the news source.")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.GEO_AREA_NOT_FOUND,
            ClientExceptionType.POST_TYPE_CANNOT_BE_CHANGED,
            ClientExceptionType.FILE_DOWNLOAD_FAILED,
            ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION})
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            notes = "Every news source has its own API Key, which is configured via data init",
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    @PostMapping(value = "newsItemPublishRequest")
    public ClientExternalPostCreateOrUpdateByNewsSourceConfirmation onNewsItemCreateRequest(
            @Valid @RequestBody ClientNewsItemCreateOrUpdateByNewsSourceRequest request) {

        return onNewsItemCreateOrUpdateRequestWithImageUpload(request, null);
    }

    @ApiOperation(value = "Publishes a NewsItem into DorfFunk (image upload)",
            notes = "The geo area has to be in the available geo areas of the app variant of the news source.\n" +
                    "One image can be uploaded with the request, it is used as first image.")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.GEO_AREA_NOT_FOUND,
            ClientExceptionType.POST_TYPE_CANNOT_BE_CHANGED,
            ClientExceptionType.FILE_ITEM_UPLOAD_FAILED,
            ClientExceptionType.FILE_DOWNLOAD_FAILED,
            ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            notes = "Every news source has its own API Key, which is configured via data init",
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    @PostMapping(value = "newsItemPublishRequestImageUpload")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "request",
                    value = """
                            Actual publishing request. It is of type ClientNewsItemPublishRequest, see endpoint /newsItemPublishRequest for details.\s
                            Unfortunately swagger-ui can not render form data parameters.
                            ```
                            {
                              "authorName": "string",
                              "categories": "string",
                              "externalId": "string",
                              "geoAreaIds": [
                                "string"
                              ],
                              "imageURLs": [
                                "string"
                              ],
                              "newsURL": "string",
                              "text": "string",
                              "croppedImageReferences": [
                                {
                                  "url": "string",
                                  "x": "int",
                                  "y": "int",
                                  "width": "int",
                                  "height": "int"
                                }
                              ]
                            }
                            ```""",
                    required = true,
                    dataTypeClass = ClientNewsItemCreateOrUpdateByNewsSourceRequest.class,
                    paramType = "form")
    )
    public ClientExternalPostCreateOrUpdateByNewsSourceConfirmation onNewsItemCreateOrUpdateRequestWithImageUpload(
            @Valid @RequestParam("request") @ApiParam(hidden = true)
            ClientNewsItemCreateOrUpdateByNewsSourceRequest request,
            @IgnoreArchitectureViolation(value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
                    reason = "Special endpoint for convenient news publishing when the image isn't downloadable")
            @RequestParam(value = "image", required = false) MultipartFile image) {

        //automatic validation fails here
        final Set<ConstraintViolation<ClientNewsItemCreateOrUpdateByNewsSourceRequest>> violations =
                validator.validate(request);
        if (!CollectionUtils.isEmpty(violations)) {
            throw new ConstraintViolationException(violations);
        }

        logNewsRequest(request);

        AppVariant appVariant = getAuthenticatedAppVariantNotNull();
        NewsSource newsSource = newsSourceService.findNewsSourceByAppVariant(appVariant);

        final Optional<NewsItemPostTypeFeature> featureOptional =
                featureService.getFeatureOptional(NewsItemPostTypeFeature.class, FeatureTarget.of(appVariant));
        //get the feature for the app variant, if not configured, get it for DorfFunk
        PostTypeFeature<?> feature =
                featureOptional.orElseGet(() -> featureService.getFeature(NewsItemPostTypeFeature.class,
                        FeatureTarget.of(getAppService().findById(DorfFunkConstants.APP_ID))));
        if (StringUtils.length(request.getText()) > feature.getMaxNumberCharsPerPost()) {
            throw new InvalidEventAttributeException("NewsItem text has more than {} characters",
                    feature.getMaxNumberCharsPerPost()).withDetail("text");
        }

        Set<GeoArea> geoAreas = getGeoAreas(request);
        validateCreateOrUpdateRequest(request);
        checkCreateOrUpdateRequest(request, newsSource, geoAreas);

        final MediaItem imageUploaded;
        if (image != null && !image.isEmpty()) {
            byte[] imageData;
            try {
                imageData = image.getBytes();
            } catch (IOException e) {
                throw new FileItemUploadException(e);
            }
            imageUploaded = mediaItemService.createMediaItem(imageData, FileOwnership.of(appVariant));
        } else {
            imageUploaded = null;
        }

        String externalId = request.getExternalId();
        Optional<ExternalPost> existingPostOpt =
                postService.findExternalPostByNewsSourceAndExternalIgnoringDeleted(newsSource, externalId);
        String postId;
        if (existingPostOpt.isPresent()) {
            ExternalPost existingPost = existingPostOpt.get();
            if (!(existingPost instanceof NewsItem)) {
                throw new PostTypeCannotBeChangedException(existingPost);
            }
            postId = existingPost.getId();
            NewsItemChangeByNewsSourceRequest changeRequest =
                    NewsItemChangeByNewsSourceRequest.builder()
                            .post((NewsItem) existingPost)
                            .appVariant(appVariant)
                            .imageUploaded(imageUploaded)
                            .imageURLs(getImageURLs(request.getImageURLs()))
                            .croppedImageReferences(getCroppedImageReferences(request))
                            .text(request.getText())
                            .organization(getOrganization(request, newsSource))
                            .authorName(request.getAuthorName())
                            .postURL(request.getNewsURL())
                            .categories(request.getCategories())
                            .geoAreas(geoAreas)
                            .publishImmediately(request.isPublishImmediately())
                            .desiredPublishTime(request.getDesiredPublishTime())
                            .desiredUnpublishTime(request.getDesiredUnpublishTime())
                            .commentsDisallowed(request.isCommentsDisallowed())
                            .build();
            notifyAndWaitForReply(PostChangeConfirmation.class, changeRequest);
        } else {
            NewsItemCreateByNewsSourceRequest createRequest =
                    NewsItemCreateByNewsSourceRequest.builder()
                            .appVariant(appVariant)
                            .imageUploaded(imageUploaded)
                            .imageURLs(getImageURLs(request.getImageURLs()))
                            .croppedImageReferences(getCroppedImageReferences(request))
                            .text(request.getText())
                            .organization(getOrganization(request, newsSource))
                            .authorName(request.getAuthorName())
                            .externalId(externalId)
                            .postURL(request.getNewsURL())
                            .newsSource(newsSource)
                            .categories(request.getCategories())
                            .geoAreas(geoAreas)
                            .publishImmediately(request.isPublishImmediately())
                            .desiredPublishTime(request.getDesiredPublishTime())
                            .desiredUnpublishTime(request.getDesiredUnpublishTime())
                            .commentsDisallowed(request.isCommentsDisallowed())
                            .build();
            PostCreateConfirmation<?> postCreateConfirmation =
                    notifyAndWaitForReply(PostCreateConfirmation.class, createRequest);
            postId = postCreateConfirmation.getPost().getId();
        }

        addAdditionalLogValue("postId", postId);
        return ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.builder()
                .postId(postId)
                .externalId(externalId)
                .build();
    }

    @ApiOperation(value = "Publishes a Happening into DorfFunk",
            notes = "The geo area has to be in the available geo areas of the app variant of the news source.")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.GEO_AREA_NOT_FOUND,
            ClientExceptionType.POST_TYPE_CANNOT_BE_CHANGED,
            ClientExceptionType.FILE_DOWNLOAD_FAILED,
            ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION})
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            notes = "Every news source has its own API Key, which is configured via data init",
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    @PostMapping(value = "happeningPublishRequest")
    public ClientExternalPostCreateOrUpdateByNewsSourceConfirmation onHappeningPublishRequest(
            @Valid @RequestBody ClientHappeningCreateOrUpdateByNewsSourceRequest request) {

        logNewsRequest(request);

        AppVariant appVariant = getAuthenticatedAppVariantNotNull();
        NewsSource newsSource = newsSourceService.findNewsSourceByAppVariant(appVariant);

        final Optional<HappeningPostTypeFeature> featureOptional =
                featureService.getFeatureOptional(HappeningPostTypeFeature.class, FeatureTarget.of(appVariant));
        //get the feature for the app variant, if not configured, get it for DorfFunk
        PostTypeFeature<?> feature =
                featureOptional.orElseGet(() -> featureService.getFeature(HappeningPostTypeFeature.class,
                        FeatureTarget.of(getAppService().findById(DorfFunkConstants.APP_ID))));
        if (StringUtils.length(request.getText()) > feature.getMaxNumberCharsPerPost()) {
            throw new InvalidEventAttributeException("Happening text has more than {} characters",
                    feature.getMaxNumberCharsPerPost()).withDetail("text");
        }

        long startTime = request.getStartTime();
        long endTime = request.getEndTime();
        boolean allDayEvent = request.isAllDayEvent();
        checkHappeningTimes(startTime, endTime, allDayEvent);

        Set<GeoArea> geoAreas = getGeoAreas(request);
        validateCreateOrUpdateRequest(request);
        checkCreateOrUpdateRequest(request, newsSource, geoAreas);

        String externalId = request.getExternalId();
        Optional<ExternalPost> existingPostOpt =
                postService.findExternalPostByNewsSourceAndExternalIgnoringDeleted(newsSource, externalId);
        String postId;
        if (existingPostOpt.isPresent()) {
            ExternalPost existingPost = existingPostOpt.get();
            if (!(existingPost instanceof Happening)) {
                throw new PostTypeCannotBeChangedException(existingPost);
            }
            postId = existingPost.getId();
            HappeningChangeByNewsSourceRequest changeRequest =
                    HappeningChangeByNewsSourceRequest.builder()
                            .post((Happening) existingPost)
                            .appVariant(appVariant)
                            .imageURLs(getImageURLs(request.getImageURLs()))
                            .croppedImageReferences(getCroppedImageReferences(request))
                            .text(request.getText())
                            .organization(getOrganization(request, newsSource))
                            .authorName(request.getAuthorName())
                            .postURL(request.getNewsURL())
                            .categories(request.getCategories())
                            .organizer(request.getOrganizer())
                            .startTime(startTime)
                            .endTime(endTime)
                            .allDayEvent(allDayEvent)
                            .geoAreas(geoAreas)
                            .publishImmediately(request.isPublishImmediately())
                            .desiredPublishTime(request.getDesiredPublishTime())
                            .desiredUnpublishTime(request.getDesiredUnpublishTime())
                            .commentsDisallowed(request.isCommentsDisallowed())
                            .customAddress(getAddress(request.getCustomLocationDefinition(), request.getVenue()))
                            .build();
            notifyAndWaitForReply(PostChangeConfirmation.class, changeRequest);
        } else {
            HappeningCreateByNewsSourceRequest createRequest =
                    HappeningCreateByNewsSourceRequest.builder()
                            .appVariant(appVariant)
                            .imageURLs(getImageURLs(request.getImageURLs()))
                            .croppedImageReferences(getCroppedImageReferences(request))
                            .text(request.getText())
                            .organization(getOrganization(request, newsSource))
                            .authorName(request.getAuthorName())
                            .externalId(request.getExternalId())
                            .postURL(request.getNewsURL())
                            .newsSource(newsSource)
                            .categories(request.getCategories())
                            .organizer(request.getOrganizer())
                            .startTime(startTime)
                            .endTime(endTime)
                            .allDayEvent(allDayEvent)
                            .geoAreas(geoAreas)
                            .publishImmediately(request.isPublishImmediately())
                            .desiredPublishTime(request.getDesiredPublishTime())
                            .desiredUnpublishTime(request.getDesiredUnpublishTime())
                            .commentsDisallowed(request.isCommentsDisallowed())
                            .customAddress(getAddress(request.getCustomLocationDefinition(), request.getVenue()))
                            .build();

            PostCreateConfirmation<?> postCreateConfirmation =
                    notifyAndWaitForReply(PostCreateConfirmation.class, createRequest);
            postId = postCreateConfirmation.getPost().getId();
        }

        addAdditionalLogValue("postId", postId);
        return ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.builder()
                .postId(postId)
                .externalId(externalId)
                .build();
    }

    private List<CroppedImageReference> getCroppedImageReferences(ClientBaseExternalPostCreateOrUpdateByNewsSourceRequest request) {

        List<ClientCroppedImageReference> clientCroppedImageReferences = request.getCroppedImages();
        if (CollectionUtils.isEmpty(clientCroppedImageReferences)) {
            return Collections.emptyList();
        } else {
            List<CroppedImageReference> imageReferences = new ArrayList<>(clientCroppedImageReferences.size());
            for (ClientCroppedImageReference clientImageReference : clientCroppedImageReferences) {
                CroppedImageReference imageReference = CroppedImageReference.builder()
                        .url(getImageURL(clientImageReference.getUrl()))
                        .build();
                imageReferences.add(imageReference);
                ClientRelativeImageCropDefinition relativeImageCropDefinition = clientImageReference.getRelativeCropDefinition();
                ClientAbsoluteImageCropDefinition absoluteImageCropDefinition = clientImageReference.getAbsoluteCropDefinition();
                if (relativeImageCropDefinition != null && absoluteImageCropDefinition != null) {
                    throw new InvalidImageCropDefinitionException(
                            "ClientCroppedImageReference for url {} contains both relative and absolute crop definitions",
                            clientImageReference.getUrl());
                }
                if (relativeImageCropDefinition != null) {
                    imageReference.setImageCropDefinition(RelativeImageCropDefinition.builder()
                            .relativeOffsetX(relativeImageCropDefinition.getRelativeOffsetX())
                            .relativeOffsetY(relativeImageCropDefinition.getRelativeOffsetY())
                            .relativeWidth(relativeImageCropDefinition.getRelativeWidth())
                            .relativeHeight(relativeImageCropDefinition.getRelativeHeight())
                            .build());
                    continue;
                }
                if (absoluteImageCropDefinition != null) {
                    imageReference.setImageCropDefinition(AbsoluteImageCropDefinition.builder()
                            .absoluteOffsetX(absoluteImageCropDefinition.getAbsoluteOffsetX())
                            .absoluteOffsetY(absoluteImageCropDefinition.getAbsoluteOffsetY())
                            .absoluteWidth(absoluteImageCropDefinition.getAbsoluteWidth())
                            .absoluteHeight(absoluteImageCropDefinition.getAbsoluteHeight())
                            .build());
                    continue;
                }
                // legacy attributes
                ClientAbsoluteImageCropDefinition legacyAbsoluteImageCropDefinition = clientImageReference.getLegacyAbsoluteImageCropDefinition();
                if (legacyAbsoluteImageCropDefinition == null) {
                    throw new InvalidImageCropDefinitionException(
                            "ClientCroppedImageReference for url {} does not contain any crop definition",
                            clientImageReference.getUrl());
                }
                imageReference.setImageCropDefinition(AbsoluteImageCropDefinition.builder()
                        .absoluteOffsetX(legacyAbsoluteImageCropDefinition.getAbsoluteOffsetX())
                        .absoluteOffsetY(legacyAbsoluteImageCropDefinition.getAbsoluteOffsetY())
                        .absoluteWidth(legacyAbsoluteImageCropDefinition.getAbsoluteWidth())
                        .absoluteHeight(legacyAbsoluteImageCropDefinition.getAbsoluteHeight())
                        .build());
            }
            return imageReferences;
        }
    }

    @Nullable
    private Organization getOrganization(ClientBaseExternalPostCreateOrUpdateByNewsSourceRequest request, NewsSource newsSource) {

        String organizationId = request.getOrganizationId();
        if (StringUtils.isNotEmpty(organizationId)) {
            Organization organization = organizationService.findOrganizationById(organizationId);
            newsSourceService.checkOrganizationAvailable(newsSource, organization);
            return organization;
        } else {
            return null;
        }
    }

    @Nullable
    private Address getAddress(ClientLocationDefinition customLocationDefinition, String venue) {

        if (customLocationDefinition != null) {
            return addressService.findOrCreateAddress(
                    addressClientModelMapper.toLocationDefinition(customLocationDefinition), false);
        } else {
            if (StringUtils.isNotEmpty(venue)) {
                return addressService.findOrCreateAddress(
                        IAddressService.AddressDefinition.builder()
                                .name(venue)
                                .build(),
                        IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                        IAddressService.GPSResolutionStrategy.LOOKEDUP_NONE,
                        false);
            } else {
                return null;
            }
        }
    }

    @ApiOperation(value = "Deletes a NewsItem, Happening",
            notes = "Deletes an existing NewsItem or Happening. Only items created by the same news source can be deleted.")
    @ApiExceptions({
            ClientExceptionType.POST_ACTION_NOT_ALLOWED,
            ClientExceptionType.POST_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            notes = "Every news source has its own API Key, which is configured via data init",
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    @PostMapping(value = "externalPostDeleteRequest")
    public ClientExternalPostDeleteConfirmation externalPostDeleteRequest(
            @Valid @RequestBody ClientExternalPostDeleteRequest request) {

        final String postId = request.getPostId();
        final String externalId = request.getExternalId();
        addAdditionalLogValue("postId", postId);
        addAdditionalLogValue("externalId", externalId);

        AppVariant appVariant = getAuthenticatedAppVariantNotNull();
        NewsSource newsSource = newsSourceService.findNewsSourceByAppVariant(appVariant);

        final ExternalPost externalPost;
        if (StringUtils.isNotEmpty(postId)) {
            externalPost = postService.findExternalPostById(postId);
        } else {
            if (StringUtils.isNotEmpty(externalId)) {
                externalPost = postService.findExternalPostByNewsSourceAndExternalIdNotDeleted(newsSource, externalId);
            } else {
                throw new InvalidEventAttributeException(
                        "Both externalId and postId are empty, one of them needs to be provided");
            }
        }
        if (!newsSource.equals(externalPost.getNewsSource())) {
            throw new NotAuthorizedException(
                    "Post '{}' was created by news source '{}' and can not be deleted by news source '{}'",
                    externalPost.getId(),
                    externalPost.getNewsSource().getId(),
                    newsSource.getId());
        }
        addAdditionalLogValue("newsURL", externalPost.getUrl());

        PostDeleteRequest deleteRequest = new PostDeleteRequest(externalPost);

        PostDeleteConfirmation deleteConfirmation = notifyAndWaitForReply(PostDeleteConfirmation.class, deleteRequest);
        return ClientExternalPostDeleteConfirmation.builder()
                .postId(deleteConfirmation.getPost().getId())
                .externalId(externalPost.getExternalId())
                .build();
    }

    private void validateCreateOrUpdateRequest(ClientBaseExternalPostCreateOrUpdateByNewsSourceRequest request)
            throws InvalidEventAttributeException {
        Long desiredPublishTime = request.getDesiredPublishTime();
        Long desiredUnpublishTime = request.getDesiredUnpublishTime();
        if (desiredPublishTime != null && desiredUnpublishTime != null) {
            if (desiredPublishTime >= desiredUnpublishTime) {
                throw new InvalidEventAttributeException("Desired unpublish time {} is before desired publish time {}",
                        desiredUnpublishTime, desiredPublishTime)
                        .withDetail("desiredPublishTime");
            }
        }
    }

    private void checkCreateOrUpdateRequest(ClientBaseExternalPostCreateOrUpdateByNewsSourceRequest request,
                                            NewsSource newsSource, Set<GeoArea> geoAreas) {

        Optional<NewsSourceFeature> newsSourceFeature = featureService.getFeatureOptional(NewsSourceFeature.class,
                FeatureTarget.of(newsSource.getAppVariant()));
        StringBuilder warningMessage = new StringBuilder();
        StringBuilder errorMessage = new StringBuilder();
        boolean warning = false;
        boolean error = false;
        AppVariant appVariant = newsSource.getAppVariant();

        warningMessage.append("News Source `")
                .append(newsSource.getId())
                .append("` ");
        errorMessage.append("Invalid request using News Source '")
                .append(newsSource.getId())
                .append("': ");
        if (appVariant != null) {
            warningMessage.append("with app variant identifier `")
                    .append(appVariant.getAppVariantIdentifier())
                    .append("` ");
        } else {
            warningMessage.append("without app variant ");
        }
        warningMessage.append("and site url ")
                .append(newsSource.getSiteUrl())
                .append(" has configuration errors:");
        if (appVariant != null) {
            Set<GeoArea> availableGeoAreas = getAppService().getAvailableGeoAreasStrict(appVariant);
            if (!availableGeoAreas.containsAll(geoAreas)) {
                Set<GeoArea> notContainedGeoAreas = geoAreas.stream()
                        .filter(geoArea -> !availableGeoAreas.contains(geoArea))
                        .collect(Collectors.toSet());
                warningMessage.append("\n`")
                        .append(notContainedGeoAreas)
                        .append("` are not contained in the available geo areas. ");
                Set<Tenant> availableTenants = getAppService().getAvailableTenants(appVariant);
                warningMessage.append("Fix this by adding the ids `")
                        .append(notContainedGeoAreas.stream()
                                .map(GeoArea::getId)
                                .collect(Collectors.toSet()))
                        .append("` of the geo areas (or one of it's parent areas) to `geoAreaIdsIncluded` of " +
                                "the AppVariantGeoAreaInitMapping for the app variant of the news source.\n");
                if (availableTenants.size() == 1) {
                    warningMessage.append("File is ");
                } else {
                    warningMessage.append("Files are ");
                }
                warningMessage.append("located at \n")
                        .append(availableTenants.stream()
                                .map(t -> "`data-init/init-grapevine/news/<env>/AppVariantGeoAreaInitMapping_" +
                                        t.getId() + ".json`" + " (" + t.getName() + ")")
                                .collect(Collectors.joining("\n")));
                if (newsSourceFeature.map(NewsSourceFeature::isEnforceGeoAreaCheck).orElse(false)) {
                    errorMessage.append("GeoAreas ")
                            .append(notContainedGeoAreas)
                            .append(" are not contained in the available geo areas. ");
                    error = true;
                }
                warning = true;
            }
        }
        if (!StringUtils.startsWith(request.getNewsURL(), newsSource.getSiteUrl())) {
            warningMessage.append("\n")
                    .append("Published post url does not start with the site url `")
                    .append(newsSource.getSiteUrl())
                    .append("`. Check if the site has the right base url. " +
                            "Check if the API Key is the correct one for this news source, maybe it is the one of another news source.");
            if (newsSourceFeature.map(NewsSourceFeature::isEnforcePostURLCheck).orElse(false)) {
                errorMessage.append("\n")
                        .append("News url '")
                        .append(request.getNewsURL())
                        .append("' does not start with the site url '")
                        .append(newsSource.getSiteUrl())
                        .append("'. ");
                error = true;
            }
            warning = true;
        }
        warningMessage.append("\n")
                .append("Published post url `")
                .append(request.getNewsURL())
                .append("`");
        if (warning) {
            teamNotificationService.sendTeamNotification(TEAM_NOTIFICATION_TOPIC_NEWS_CONFIG,
                    TeamNotificationPriority.INFO_APPLICATION_SYSTEM,
                    warningMessage.toString());
        }
        if (error) {
            throw new InvalidEventAttributeException(errorMessage.toString());
        }
    }

    private Set<GeoArea> getGeoAreas(ClientBaseExternalPostCreateOrUpdateByNewsSourceRequest request) {

        List<String> requestedGeoAreaIds = request.getGeoAreaIds();
        String requestedGeoAreaId = request.getGeoAreaId();
        if (CollectionUtils.isEmpty(requestedGeoAreaIds)) {
            if (StringUtils.isBlank(requestedGeoAreaId)) {
                throw new InvalidEventAttributeException("The request needs at least one geo area id")
                        .withDetail("geoAreaId");
            }
            return Set.of(geoAreaService.findGeoAreaById(requestedGeoAreaId));
        } else {
            if (StringUtils.isNotEmpty(requestedGeoAreaId)) {
                throw new InvalidEventAttributeException(
                        "Request has single geo area id and list of geo area ids set. Use the list only.");
            }
            List<String> duplicateIds = BaseEntity.findDuplicateIds(requestedGeoAreaIds);
            if (!duplicateIds.isEmpty()) {
                throw new InvalidEventAttributeException("The request contains duplicate geo area ids: {}",
                        duplicateIds);
            }
            return geoAreaService.findAllById(requestedGeoAreaIds);
        }
    }

    private void logNewsRequest(ClientBaseExternalPostCreateOrUpdateByNewsSourceRequest request) {
        addAdditionalLogValue("newsURL", request.getNewsURL());
    }

    private List<URL> getImageURLs(List<String> imageURLs) {
        if (!CollectionUtils.isEmpty(imageURLs)) {
            return imageURLs.stream()
                    .map(this::getImageURL)
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    @NotNull
    private URL getImageURL(String rawURL) {

        try {
            return fileDownloadService.createUrlAndCheck(rawURL);
        } catch (FileDownloadException e) {
            throw new PostInvalidException("Image URL invalid: {}", e.toString()).withDetail(rawURL);
        }
    }

}
