/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Adeline Silva Schäfer, Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientevent;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;

import de.fhg.iese.dd.platform.api.framework.validation.MaxElements;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientDocumentItemPlaceHolder;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItemPlaceHolder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public abstract class ClientBasePostChangeByPersonRequest extends ClientBasePostChangeRequest {

    @MaxElements(max = @Value("#{grapevineConfig.maxNumberImagesPerPost}"))
    private List<ClientMediaItemPlaceHolder> mediaItemPlaceHolders;
    @MaxElements(max = @Value("#{grapevineConfig.maxNumberAttachmentsPerPost}"))
    private List<ClientDocumentItemPlaceHolder> documentItemPlaceHolders;

}
