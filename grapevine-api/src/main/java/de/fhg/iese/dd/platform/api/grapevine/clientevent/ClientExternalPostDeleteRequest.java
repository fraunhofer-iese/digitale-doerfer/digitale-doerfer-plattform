/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientevent;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientExternalPostDeleteRequest extends ClientBaseEvent {

    @ApiModelProperty(notes = "either use your own external id (that was used to create the post) " +
            "or the post id that was returned as result of the create request")
    private String postId;

    @ApiModelProperty(notes = "either use your own external id (that was used to create the post) " +
            "or the post id that was returned as result of the create request")
    private String externalId;

}
