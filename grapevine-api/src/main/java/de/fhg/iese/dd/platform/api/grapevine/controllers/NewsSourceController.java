/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Balthasar Weitzel, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientOrganization;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientOrganizationTag;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostStatistics;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoArea;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.GeoAreaClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.services.INewsSourceService;
import de.fhg.iese.dd.platform.business.grapevine.services.IOrganizationService;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostService;
import de.fhg.iese.dd.platform.business.grapevine.statistics.IPostStatisticsService;
import de.fhg.iese.dd.platform.business.grapevine.statistics.PostStatistics;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationTag;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/grapevine")
@Api(tags = {"grapevine.newsSource"})
public class NewsSourceController extends BaseController {

    @Autowired
    private INewsSourceService newsSourceService;
    @Autowired
    private IPostService postService;
    @Autowired
    private IPostStatisticsService postStatisticsService;
    @Autowired
    private IAppService appService;
    @Autowired
    private IOrganizationService organizationService;
    @Autowired
    private GrapevineClientModelMapper grapevineClientModelMapper;
    @Autowired
    private GeoAreaClientModelMapper geoAreaClientModelMapper;

    @ApiOperation(value = "Checks configuration of DorfNews / DorfPages",
            notes = "Checks the configuration of a DorfNews or DorfPages instance. " +
                    "Validates the API Key based on the site url and returns the names of the geo area and tenant")
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            notes = "Every news source has its own API Key, which is configured via data init",
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    //the url is not really correct, but hard to change since there are many non-updatable clients out there
    @GetMapping(value = "/event/checkNewsConfiguration")
    public String checkNewsConfiguration(
            @RequestHeader(name = HEADER_NAME_API_KEY)
            //intentionally added here instead of using getApiKeyNotNull, so that the check is more robust
            String apiKey,
            @ApiParam(value = "also known as baseURL in other endpoints", required = true)
            @RequestParam
            String siteURL,
            @RequestParam
            String geoAreaId,
            @ApiParam(value = "deprecated, use tenantId instead", hidden = true)
            @RequestParam(required = false)
            String communityId,
            @RequestParam(required = false)
            String tenantId) {

        String realTenantId = StringUtils.isBlank(tenantId) ? communityId : tenantId;
        return newsSourceService.checkConfiguration(geoAreaId, realTenantId, siteURL, apiKey);
    }

    @ApiOperation(value = "Returns statistics of a post",
            notes = "The post has to be created by the news source")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            notes = "Every news source has its own API Key, which is configured via data init",
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    @GetMapping("/statistics/post/{postId}")
    public ClientPostStatistics getPostStatistics(
            @PathVariable String postId) {

        AppVariant appVariant = getAuthenticatedAppVariantNotNull();
        NewsSource newsSource = newsSourceService.findNewsSourceByAppVariant(appVariant);
        ExternalPost externalPost = postService.findExternalPostById(postId);
        if (!newsSource.equals(externalPost.getNewsSource())) {
            throw new NotAuthorizedException(
                    "Post '{}' was created by news source '{}' and can not be accessed by news source '{}'",
                    externalPost.getId(),
                    externalPost.getNewsSource().getId(),
                    newsSource.getId());
        }

        PostStatistics postStatistics = postStatisticsService.calculatePostStatistics(externalPost);
        return grapevineClientModelMapper.toClientPostStatistics(postStatistics);
    }

    @ApiOperation(value = "Returns all available geo areas of a news source",
            notes = "Only the geo areas directly assigned to the app variant of the news source are returned, not the parent geo areas.")
    @ApiExceptions({
            ClientExceptionType.NEWS_SOURCE_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            notes = "Every news source has its own API Key, which is configured via data init",
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    @GetMapping("/newssource/availableGeoAreas")
    public List<ClientGeoArea> getAvailableGeoAreas() {

        AppVariant appVariant = getAuthenticatedAppVariantNotNull();
        // check if the AppVariant has a NewsSource
        newsSourceService.findNewsSourceByAppVariant(appVariant);
        Set<GeoArea> availableGeoAreas = appService.getAvailableGeoAreasStrict(appVariant);

        return geoAreaClientModelMapper.toClientGeoAreas(availableGeoAreas, false);
    }

    @ApiOperation(value = "Returns all available organizations of a news source",
            notes = "All organizations that are assigned to the news source are returned")
    @ApiExceptions({
            ClientExceptionType.NEWS_SOURCE_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            notes = "Every news source has its own API Key, which is configured via data init",
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    @GetMapping("/newssource/availableOrganizations")
    public List<ClientOrganization> getAvailableOrganizations(
            @ApiParam(value = "Organizations need to have at least one of the tags. If empty, all are returned.")
            @RequestParam(required = false)
            Set<ClientOrganizationTag> requiredTags
    ) {

        AppVariant appVariant = getAuthenticatedAppVariantNotNull();
        NewsSource newsSource = newsSourceService.findNewsSourceByAppVariant(appVariant);
        Set<ClientOrganizationTag> filteredRequiredTags;
        if (requiredTags != null) {
            filteredRequiredTags = requiredTags.stream()
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
        } else {
            filteredRequiredTags = Collections.emptySet();
        }
        Set<OrganizationTag> tags;
        if (CollectionUtils.isEmpty(filteredRequiredTags)) {
            tags = EnumSet.allOf(OrganizationTag.class);
        } else {
            tags = grapevineClientModelMapper.toOrganizationTags(filteredRequiredTags);
        }

        return newsSourceService.findAllAvailableOrganizationsByNewsSource(newsSource, tags).stream()
                .map(organization -> grapevineClientModelMapper.toClientOrganization(organization,
                        organizationService.getRelatedGeoAreaIds(organization)))
                .collect(Collectors.toList());
    }

}
