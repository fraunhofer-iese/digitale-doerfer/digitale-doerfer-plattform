/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Adeline Silva Schäfer, Ala Harirchi, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.GrapevineStatisticsProcessingException;
import de.fhg.iese.dd.platform.business.grapevine.statistics.legacy.GrapevineStats;
import de.fhg.iese.dd.platform.business.grapevine.statistics.legacy.IGrapevineStatisticsService;
import de.fhg.iese.dd.platform.business.grapevine.statistics.legacy.TenantGroupStats;
import de.fhg.iese.dd.platform.business.grapevine.statistics.legacy.TenantStats;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.security.SuperAdminAction;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/administration/grapevine/stats")
@Api(tags = {"admin", "grapevine.admin.stats"})
public class LegacyGrapevineStatisticsAdminController extends BaseController {

    @Autowired
    private IGrapevineStatisticsService grapevineStatisticsService;

    @Autowired
    private ITenantService tenantService;

    @ApiOperation(value = "Gets global statistics and all tenant specific",
            notes = "Gets the TenantStats, UserStats, and PostStats for all tenants in the specified period as a human readable report")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @GetMapping(value = "/globalReport")
    public String getGrapevineStatsReport(

            @RequestParam(defaultValue = "0")
            @ApiParam(value = "start time of the report") long start,

            @RequestParam(required = false)
            @ApiParam(value = "end time to which the stats should be calculated, if 0 the current time is taken",
                    defaultValue = "0") long end) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        if (end == 0) {
            end = getTimeService().currentTimeMillisUTC();
        }

        GrapevineStats grapevineStats = grapevineStatisticsService.getGrapevineStats(start, end);
        try {
            return grapevineStatisticsService.getGrapevineStatsReport(start, end, grapevineStats);
        } catch (Exception ex) {
            throw new GrapevineStatisticsProcessingException("Error processing Statics Report", ex);
        }
    }

    @ApiOperation(value = "Gets specific statistics about all tenants sharing the same tag",
            notes = "Gets the TenantGroupStats in the specified period as a human readable report")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @GetMapping(value = "/tenantGroupReport")
    public String getTenantGroupStatsReport(

            @RequestParam
            @ApiParam(value = "tag of the tenant") String tenantGroupTag,

            @RequestParam
            @ApiParam(value = "start time of the report") long start,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "end time to which the stats should be calculated, if 0 the current time is taken",
                    defaultValue = "0") long end) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        if (end == 0) {
            end = getTimeService().currentTimeMillisUTC();
        }

        TenantGroupStats tenantGroupStats = grapevineStatisticsService.getTenantGroupStats(tenantGroupTag, start, end);

        try {
            return grapevineStatisticsService.getTenantGroupStatsReport(start, end, tenantGroupStats);
        } catch (Exception ex) {
            throw new GrapevineStatisticsProcessingException("Error processing Statics Report", ex);
        }
    }

    @ApiOperation(value = "Gets tenant specific statistics",
            notes = "Gets the TenantStats in the specified period as a human readable report")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @GetMapping(value = "/tenantReport")
    public String getTenantStatsReport(

            @RequestParam
            @ApiParam(value = "tenant") String tenantId,

            @RequestParam
            @ApiParam(value = "start time of the report") long start,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "end time to which the stats should be calculated, if 0 the current time is taken",
                    defaultValue = "0") long end) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        if (end == 0) {
            end = getTimeService().currentTimeMillisUTC();
        }

        Tenant tenant = tenantService.findTenantById(tenantId);
        TenantStats tenantStats = grapevineStatisticsService.getTenantStats(tenant, start, end);

        try {
            return grapevineStatisticsService.getTenantStatsReport(start, end, tenantStats);
        }
        catch (Exception ex) {
            throw new GrapevineStatisticsProcessingException("Error processing Statics Report", ex);
        }
    }

}
