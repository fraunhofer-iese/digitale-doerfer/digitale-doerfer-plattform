/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel, Johannes Schneider, Adeline Schäfer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.communication.clientmodel.CommunicationClientModelMapper;
import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiException;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidEventAttributeException;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnCommentConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnCommentRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnPostConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnPostRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.suggestion.ClientChatStartWithSuggestionWorkerConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.suggestion.ClientChatStartWithSuggestionWorkerRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.chat.BaseChatStartRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.chat.ChatStartOnCommentConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.chat.ChatStartOnCommentRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.chat.ChatStartOnPostConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.chat.ChatStartOnPostRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.suggestion.ChatStartWithSuggestionWorkerConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.suggestion.ChatStartWithSuggestionWorkerRequest;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostHasNoCreatorException;
import de.fhg.iese.dd.platform.business.grapevine.services.ICommentService;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostService;
import de.fhg.iese.dd.platform.business.grapevine.services.ISuggestionService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentCreationRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionFirstResponder;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionWorker;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/grapevine/chat/event")
@Api(tags = {"grapevine.chat.events"})
class GrapevineChatEventController extends BaseController {

    private static final List<Class<? extends BaseRole<?>>> ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER =
            List.of(SuggestionFirstResponder.class, SuggestionWorker.class);

    @Autowired
    private IPersonService personService;
    @Autowired
    private IPostService postService;
    @Autowired
    private ICommentService commentService;
    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private ISuggestionService suggestionService;
    @Autowired
    private CommunicationClientModelMapper communicationClientModelMapper;

    @ApiOperation(value = "Start a new chat on a post",
            notes = "If there is no chat it is created, if there is one already existing, the message is added to it. "
                    + "It creates a message that references that post and a text message.")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.POST_HAS_NO_CREATOR,
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND})
    @ApiException(value = ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT,
            reason = "If the feature 'de.fhg.iese.dd.dorffunk.content.creation-restriction' is enabled " +
                    "for the app variant, users have to have at least one of the allowed verification " +
                    "status, as configured in the feature.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("chatStartOnPostRequest")
    public ClientChatStartOnPostConfirmation onClientChatStartOnPostRequest(
            @Valid @RequestBody ClientChatStartOnPostRequest clientRequest) {

        final Person sender = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        final String message = clientRequest.getMessage();
        final String postId = clientRequest.getPostId();
        checkVerificationStatusRestriction(ContentCreationRestrictionFeature.class, sender, appVariant);
        final Post post = postService.findPostByIdNotDeleted(postId);
        if (post.getCreator() == null) {
            throw new PostHasNoCreatorException("The post {} has no creator, so no chat can be started", post.getId());
        }

        final String temporaryMediaItemId = clientRequest.getTemporaryMediaItemId();

        final ChatStartOnPostRequest request = ChatStartOnPostRequest.builder()
                .sender(sender)
                .appVariant(appVariant)
                .post(post)
                .sentTime(clientRequest.getSentTime())
                .build();

        checkAndSetMessageAndMediaItem(sender, appVariant, message, temporaryMediaItemId, request);

        final ChatStartOnPostConfirmation confirmation =
                notifyAndWaitForReply(ChatStartOnPostConfirmation.class, request);

        return new ClientChatStartOnPostConfirmation(
                confirmation.getPost().getId(),
                communicationClientModelMapper.toClientChat(confirmation.getChat(), sender));
    }

    @ApiOperation(value = "Start a new chat on a comment",
            notes = "If there is no chat it is created, if there is one already existing, the message is added to it. "
                    + "It creates a message that references that comment and a text message.")
    @ApiExceptions({
            ClientExceptionType.COMMENT_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND})
    @ApiException(value = ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT,
            reason = "If the feature 'de.fhg.iese.dd.dorffunk.content.creation-restriction' is enabled " +
                    "for the app variant, users have to have at least one of the allowed verification " +
                    "status, as configured in the feature.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("chatStartOnCommentRequest")
    public ClientChatStartOnCommentConfirmation onClientChatStartOnCommentRequest(
            @Valid @RequestBody ClientChatStartOnCommentRequest clientRequest) {

        final Person sender = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        final String message = clientRequest.getMessage();
        final String commentId = clientRequest.getCommentId();
        checkVerificationStatusRestriction(ContentCreationRestrictionFeature.class, sender, appVariant);
        final Comment comment = commentService.findCommentByIdIncludingDeleted(commentId);
        final String temporaryMediaItemId = clientRequest.getTemporaryMediaItemId();

        final ChatStartOnCommentRequest request = ChatStartOnCommentRequest.builder()
                .sender(sender)
                .appVariant(appVariant)
                .comment(comment)
                .sentTime(clientRequest.getSentTime())
                .build();

        checkAndSetMessageAndMediaItem(sender, appVariant, message, temporaryMediaItemId, request);

        final ChatStartOnCommentConfirmation confirmation =
                notifyAndWaitForReply(ChatStartOnCommentConfirmation.class, request);

        return new ClientChatStartOnCommentConfirmation(
                confirmation.getComment().getId(),
                communicationClientModelMapper.toClientChat(confirmation.getChat(), sender));
    }

    @ApiOperation(value = "Start private chat with suggestion worker",
            notes = "Starts a private chat with a suggestion worker. The worker has to have the role SuggestionWorker for any tenant.")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND,
            ClientExceptionType.POST_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuggestionFirstResponder.class, SuggestionWorker.class})
    @PostMapping(value = "chatStartWithSuggestionWorkerRequest")
    public ClientChatStartWithSuggestionWorkerConfirmation onChatStartWithSuggestionWorkerRequest(
            @Valid @RequestBody ClientChatStartWithSuggestionWorkerRequest clientRequest) {

        final Person person = getCurrentPersonNotNull();

        if (!getRoleService().hasRoleForAnyEntity(person, ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER)) {
            throw new NotAuthorizedException(
                    "Chatting to workers is only allowed for SuggestionFirstResponder and SuggestionWorker.");
        }

        final Person chatReceiver = personService.findPersonById(clientRequest.getWorkerPersonId());
        if (!getRoleService().hasRoleForAnyEntity(chatReceiver, ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER)) {
            throw new NotAuthorizedException("Only SuggestionWorker and SuggestionFirstResponder can be chatted with.");
        }
        final Person sender = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantOrNull();

        final String message = clientRequest.getMessage();
        final String temporaryMediaItemId = clientRequest.getTemporaryMediaItemId();

        ChatStartWithSuggestionWorkerRequest request;
        if(StringUtils.isNotEmpty(clientRequest.getSuggestionId())) {
            Suggestion suggestion = suggestionService.findSuggestionByIdIncludingOverrideDeleted(clientRequest.getSuggestionId(), false);
            request = ChatStartWithSuggestionWorkerRequest.builder()
                    .sender(sender)
                    .appVariant(appVariant)
                    .sentTime(clientRequest.getSentTime())
                    .receiver(chatReceiver)
                    .suggestion(suggestion)
                    .build();
        } else {
            request = ChatStartWithSuggestionWorkerRequest.builder()
                    .sender(sender)
                    .appVariant(appVariant)
                    .sentTime(clientRequest.getSentTime())
                    .receiver(chatReceiver)
                    .build();
        }

        checkAndSetMessageAndMediaItem(sender, appVariant, message, temporaryMediaItemId, request);

        ChatStartWithSuggestionWorkerConfirmation confirmation =
                notifyAndWaitForReply(ChatStartWithSuggestionWorkerConfirmation.class, request);

        return new ClientChatStartWithSuggestionWorkerConfirmation(
                communicationClientModelMapper.toClientChat(confirmation.getChat(), sender));
    }

    private void checkAndSetMessageAndMediaItem(Person sender, AppVariant appVariant, String message,
            String temporaryMediaItemId, BaseChatStartRequest request) {

        boolean validMessage = false;

        if (StringUtils.isNotEmpty(message)) {
            request.setMessage(message);
            validMessage = true;
        }
        if (StringUtils.isNotEmpty(temporaryMediaItemId)) {
            TemporaryMediaItem temporaryMediaItem =
                    mediaItemService.findTemporaryItemById(sender, temporaryMediaItemId);
            request.setTemporaryMediaItem(temporaryMediaItem);
            request.setAppVariant(appVariant);
            validMessage = true;
        }
        if (!validMessage) {
            throw new InvalidEventAttributeException("The attributes {} and {} may not be both be empty/null!",
                    "message", "temporaryMediaItemId");
        }
    }

}
