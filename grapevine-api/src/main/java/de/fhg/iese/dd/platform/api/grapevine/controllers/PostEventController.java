/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Balthasar Weitzel, Dominik Schnier, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.*;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.*;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.gossip.ClientGossipChangeRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.gossip.ClientGossipCreateInGroupRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.gossip.ClientGossipCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.happening.ClientHappeningChangeRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.happening.ClientHappeningCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientLikePostConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientLikePostRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientUnlikePostConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientUnlikePostRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.offer.ClientOfferChangeRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.offer.ClientOfferCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.seeking.ClientSeekingChangeRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.seeking.ClientSeekingCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostView;
import de.fhg.iese.dd.platform.api.grapevine.exceptions.PostActionNotAllowedException;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagResponse;
import de.fhg.iese.dd.platform.business.grapevine.events.*;
import de.fhg.iese.dd.platform.business.grapevine.events.gossip.GossipChangeRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.gossip.GossipCreateInGroupRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.gossip.GossipCreateRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningChangeByPersonRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningCreateByPersonRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.like.LikePostConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.like.LikePostRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.like.UnlikePostConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.like.UnlikePostRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.offer.OfferChangeRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.offer.OfferCreateRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.seeking.SeekingChangeRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.seeking.SeekingCreateRequest;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.grapevine.services.ITradingCategoryService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagConfirmation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/grapevine/event")
@Api(tags = {"grapevine.post.events"})
class PostEventController extends BasePostEventController {

    @Autowired
    private IGroupService groupService;
    @Autowired
    private ITradingCategoryService tradingCategoryService;

    @ApiOperation(value = "Creates a new Gossip",
            notes = "Creates a new Gossip, if the persons is allowed to. See exceptions for details.")
    @ApiExceptions({
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.POST_COULD_NOT_BE_CREATED})
    @ApiException(value = ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT,
            reason = "If the feature 'de.fhg.iese.dd.dorffunk.content.creation-restriction' is enabled " +
                    "for the app variant, users have to have at least one of the allowed verification " +
                    "status, as configured in the feature.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping(value = "gossipCreateRequest")
    public ClientPostCreateConfirmation onGossipCreateRequest(
            @Valid @RequestBody ClientGossipCreateRequest request) {

        Person currentPerson = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();

        checkMaxTextLength(GossipPostTypeFeature.class, currentPerson, appVariant, request::getText);

        checkVerificationStatusRestriction(ContentCreationRestrictionFeature.class, currentPerson, appVariant);
        GossipCreateRequest.GossipCreateRequestBuilder<?, ?> requestBuilder = GossipCreateRequest.builder();
        setPostCreateByPersonRequestValues(requestBuilder, currentPerson, appVariant, request);

        return handlePostCreateRequest(requestBuilder.build());
    }

    @ApiOperation(value = "Creates a new Gossip in Group",
            notes = "Creates a new Gossip in a group.\n" +
                    "The user needs to be member of the group to be allowed to post content.")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND,
            ClientExceptionType.NOT_MEMBER_OF_GROUP,
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE})
    @ApiException(value = ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT,
            reason = "If the feature 'de.fhg.iese.dd.dorffunk.content.creation-restriction' is enabled " +
                    "for the app variant, users have to have at least one of the allowed verification " +
                    "status, as configured in the feature.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping(value = "gossipCreateInGroupRequest")
    public ClientPostCreateConfirmation onGossipCreateInGroupRequest(
            @Valid @RequestBody ClientGossipCreateInGroupRequest request) {

        Person currentPerson = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();

        checkMaxTextLength(GossipPostTypeFeature.class, currentPerson, appVariant, request::getText);
        Group group =
                groupService.findGroupByIdFilteredByGroupSettings(request.getGroupId(), currentPerson, appVariant);
        groupService.checkIsGroupMember(group, currentPerson);
        checkVerificationStatusRestriction(ContentCreationRestrictionFeature.class, currentPerson, appVariant);
        GossipCreateInGroupRequest.GossipCreateInGroupRequestBuilder<?, ?> requestBuilder =
                GossipCreateInGroupRequest.builder()
                        .group(group);

        setPostCreateByPersonRequestValues(requestBuilder, currentPerson, appVariant, request);
        return handlePostCreateRequest(requestBuilder.build());
    }

    @ApiOperation(value = "Creates a new Seeking",
            notes = "Creates a new Seeking.")
    @ApiExceptions({
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.POST_COULD_NOT_BE_CREATED})
    @ApiException(value = ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT,
            reason = "If the feature 'de.fhg.iese.dd.dorffunk.content.creation-restriction' is enabled " +
                    "for the app variant, users have to have at least one of the allowed verification " +
                    "status, as configured in the feature.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping(value = "seekingCreateRequest")
    public ClientPostCreateConfirmation onSeekingCreateRequest(
            @Valid @RequestBody ClientSeekingCreateRequest request) {

        Person currentPerson = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();

        checkMaxTextLength(SeekingPostTypeFeature.class, currentPerson, appVariant, request::getText);

        checkVerificationStatusRestriction(ContentCreationRestrictionFeature.class, currentPerson, appVariant);
        SeekingCreateRequest.SeekingCreateRequestBuilder<?, ?> requestBuilder =
                SeekingCreateRequest.builder()
                        .tradingCategory(getTradingCategoryOrDefault(request))
                        .date(request.getDate());

        setPostCreateByPersonRequestValues(requestBuilder, currentPerson, appVariant, request);

        return handlePostCreateRequest(requestBuilder.build());
    }

    @ApiOperation(value = "Creates a new Offer",
            notes = "Creates a new Offer.")
    @ApiExceptions({
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.POST_COULD_NOT_BE_CREATED})
    @ApiException(value = ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT,
            reason = "If the feature 'de.fhg.iese.dd.dorffunk.content.creation-restriction' is enabled " +
                    "for the app variant, users have to have at least one of the allowed verification " +
                    "status, as configured in the feature.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("offerCreateRequest")
    public ClientPostCreateConfirmation onOfferCreateRequest(
            @Valid @RequestBody ClientOfferCreateRequest request) {

        Person currentPerson = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();

        checkMaxTextLength(OfferPostTypeFeature.class, currentPerson, appVariant, request::getText);

        checkVerificationStatusRestriction(ContentCreationRestrictionFeature.class, currentPerson, appVariant);
        OfferCreateRequest.OfferCreateRequestBuilder<?, ?> requestBuilder =
                OfferCreateRequest.builder()
                        .tradingCategory(getTradingCategoryOrDefault(request))
                        .date(request.getDate());

        setPostCreateByPersonRequestValues(requestBuilder, currentPerson, appVariant, request);

        return handlePostCreateRequest(requestBuilder.build());
    }

    @ApiOperation(value = "Creates a new Happening",
            notes = "Creates a new Happening. " +
                    "Only possible if the feature `de.fhg.iese.dd.dorffunk.happening.creation` is enabled for " +
                    "the requesting app variant and user.")
    @ApiExceptions({
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.POST_COULD_NOT_BE_CREATED})
    @ApiException(value = ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT,
            reason = "If the feature 'de.fhg.iese.dd.dorffunk.content.creation-restriction' is enabled " +
                    "for the app variant, users have to have at least one of the allowed verification " +
                    "status, as configured in the feature.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("happeningCreateRequest")
    public ClientPostCreateConfirmation onHappeningCreateRequest(
            @Valid @RequestBody ClientHappeningCreateRequest request) {

        Person currentPerson = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();
        
        //if the feature is not enabled an exception is thrown
        featureService.checkFeatureEnabled(HappeningCreationByPersonFeature.class,
                FeatureTarget.of(currentPerson, appVariant));
        checkMaxTextLength(HappeningPostTypeFeature.class, currentPerson, appVariant, request::getText);

        long startTime = request.getStartTime();
        long endTime = request.getEndTime();
        boolean allDayEvent = request.isAllDayEvent();
        checkHappeningTimes(startTime, endTime, allDayEvent);

        checkVerificationStatusRestriction(ContentCreationRestrictionFeature.class, currentPerson, appVariant);
        HappeningCreateByPersonRequest.HappeningCreateByPersonRequestBuilder<?, ?> requestBuilder =
                HappeningCreateByPersonRequest.builder()
                        .startTime(startTime)
                        .endTime(endTime)
                        .allDayEvent(allDayEvent);
        setPostCreateByPersonRequestValues(requestBuilder, currentPerson, appVariant, request);

        return handlePostCreateRequest(requestBuilder.build());
    }

    @ApiOperation(value = "Updates an existing Gossip",
            notes = "Updates an existing Gossip. Only text and images can be updated.")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.FILE_ITEM_DUPLICATE_USAGE})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("gossipChangeRequest")
    public ClientPostChangeConfirmation onGossipChangeRequest(
            @Valid @RequestBody ClientGossipChangeRequest request) {

        if (request.isUseEmptyFields()) {
            checkAttributeNotNullOrEmpty(request.getText(), "text");
        }

        Person currentPerson = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();

        checkMaxTextLength(GossipPostTypeFeature.class, currentPerson, appVariant, request::getText);

        Gossip gossip = postService.findGossipById(request.getPostId());
        if (!currentPerson.equals(gossip.getCreator())) {
            throw new PostActionNotAllowedException("Only the creator can edit the post");
        }

        GossipChangeRequest.GossipChangeRequestBuilder<?, ?> requestBuilder = GossipChangeRequest.builder();

        setPostChangeRequestByPersonValues(requestBuilder, gossip, currentPerson, appVariant, request);

        return handlePostChangeRequest(currentPerson, requestBuilder.build());
    }

    @ApiOperation(value = "Updates an existing Seeking",
            notes = "Updates an existing Seeking. Text, images, category, status and date can be updated.")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.FILE_ITEM_DUPLICATE_USAGE})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("seekingChangeRequest")
    public ClientPostChangeConfirmation onSeekingChangeRequest(
            @Valid @RequestBody ClientSeekingChangeRequest request) {

        if (request.isUseEmptyFields()) {
            checkAttributeNotNullOrEmpty(request.getText(), "text");
        }

        Person currentPerson = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();

        checkMaxTextLength(SeekingPostTypeFeature.class, currentPerson, appVariant, request::getText);

        Seeking seeking = postService.findSeekingById(request.getPostId());
        if (!currentPerson.equals(seeking.getCreator())) {
            throw new PostActionNotAllowedException("Only the creator can edit the post");
        }

        SeekingChangeRequest.SeekingChangeRequestBuilder<?, ?> requestBuilder = SeekingChangeRequest.builder()
                .tradingCategory(getTradingCategoryNull(request))
                .tradingStatus(request.getTradingStatus())
                .date(request.getDate());
        setPostChangeRequestByPersonValues(requestBuilder, seeking, currentPerson, appVariant, request);

        return handlePostChangeRequest(currentPerson, requestBuilder.build());
    }

    @ApiOperation(value = "Updates an existing Offer",
            notes = "Updates an existing Offer. Text, images, category, status and date can be updated.")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.FILE_ITEM_DUPLICATE_USAGE})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("offerChangeRequest")
    public ClientPostChangeConfirmation onOfferChangeRequest(
            @Valid @RequestBody ClientOfferChangeRequest request) {

        if (request.isUseEmptyFields()) {
            checkAttributeNotNullOrEmpty(request.getText(), "text");
        }

        Person currentPerson = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();

        checkMaxTextLength(OfferPostTypeFeature.class, currentPerson, appVariant, request::getText);

        Offer offer = postService.findOfferById(request.getPostId());
        if (!currentPerson.equals(offer.getCreator())) {
            throw new PostActionNotAllowedException("Only the creator can edit the post");
        }

        OfferChangeRequest.OfferChangeRequestBuilder<?, ?> requestBuilder = OfferChangeRequest.builder()
                .tradingCategory(getTradingCategoryNull(request))
                .tradingStatus(request.getTradingStatus())
                .date(request.getDate());
        setPostChangeRequestByPersonValues(requestBuilder, offer, currentPerson, appVariant, request);

        return handlePostChangeRequest(currentPerson, requestBuilder.build());
    }

    @ApiOperation(value = "Updates an existing Happening",
            notes = "Updates an existing Happening. Text, images, start and end time can be updated.")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.FILE_ITEM_DUPLICATE_USAGE})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("happeningChangeRequest")
    public ClientPostChangeConfirmation onHappeningChangeRequest(
            @Valid @RequestBody ClientHappeningChangeRequest request) {

        if (request.isUseEmptyFields()) {
            checkAttributeNotNullOrEmpty(request.getText(), "text");
        }

        Person currentPerson = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();

        checkMaxTextLength(HappeningPostTypeFeature.class, currentPerson, appVariant, request::getText);

        Happening happening = postService.findHappeningById(request.getPostId());
        if (!currentPerson.equals(happening.getCreator())) {
            throw new PostActionNotAllowedException("Only the creator can edit the post");
        }
        //if there are no values provided the existing ones are used to check
        Long startTime = request.getStartTime();
        Long endTime = request.getEndTime();
        Boolean allDayEvent = request.getAllDayEvent();
        if (startTime == null) {
            startTime = happening.getStartTime();
        }
        if (endTime == null) {
            endTime = happening.getEndTime();
        }
        if (allDayEvent == null) {
            allDayEvent = happening.isAllDayEvent();
        }
        checkHappeningTimes(startTime, endTime, allDayEvent);

        HappeningChangeByPersonRequest.HappeningChangeByPersonRequestBuilder<?, ?> requestBuilder =
                HappeningChangeByPersonRequest.builder()
                        .startTime(request.getStartTime())
                        .endTime(request.getEndTime())
                        .allDayEvent(request.getAllDayEvent());
        setPostChangeRequestByPersonValues(requestBuilder, happening, currentPerson, appVariant, request);

        return handlePostChangeRequest(currentPerson, requestBuilder.build());
    }

    @ApiOperation(value = "Deletes a post created by a person (except Suggestions)",
            notes = "Deletes a post (except Suggestions). Only the creator can delete the post.\n" +
                    "Suggestions can not be deleted that way, they need to be withdrawn")
    @ApiExceptions({
            ClientExceptionType.POST_ACTION_NOT_ALLOWED,
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.SUGGESTION_CANNOT_BE_DELETED})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("postDeleteRequest")
    public ClientPostDeleteConfirmation onPostDeleteRequest(
            @Valid @RequestBody ClientPostDeleteRequest request) {

        Person person = getCurrentPersonNotNull();
        Post post = postService.findPostByIdNotDeleted(request.getPostId());

        if (!person.equals(post.getCreator())) {
            throw new PostActionNotAllowedException("Only the creator can delete the post");
        }

        PostDeleteConfirmation deleteConfirmation = notifyAndWaitForReply(PostDeleteConfirmation.class,
                new PostDeleteRequest(post));
        return new ClientPostDeleteConfirmation(deleteConfirmation.getPost().getId());
    }

    @ApiOperation(value = "Likes a post",
            notes = "Likes an existing post. It is possible to like own posts.")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND})
    @ApiException(value = ClientExceptionType.POST_ALREADY_LIKED,
            reason = "The post is already liked and can not be liked again.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("likePostRequest")
    public ClientLikePostConfirmation onLikePostRequest(
            @Valid @RequestBody ClientLikePostRequest request) {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();
        final Post post =
                postService.findByIdFilteredByHiddenForOtherHomeAreasAndGroupContentVisibility(request.getPostId(),
                        person, appVariant);

        final LikePostRequest likePostRequest = LikePostRequest.builder()
                .post(post)
                .liker(person)
                .build();

        LikePostConfirmation likePostConfirmation = notifyAndWaitForReply(LikePostConfirmation.class, likePostRequest);

        return new ClientLikePostConfirmation(grapevineModelMapper.toClientPost(likePostConfirmation.getPost(), true));
    }

    @ApiOperation(value = "Unlikes a post",
            notes = "Unlikes an existing post.")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.POST_ALREADY_UNLIKED,
            ClientExceptionType.POST_NOT_LIKED})
    @ApiException(value = ClientExceptionType.POST_ALREADY_UNLIKED,
            reason = "The post is already unliked and can not be unliked again.")
    @ApiException(value = ClientExceptionType.POST_NOT_LIKED,
            reason = "The post is not liked and thus can not be unliked.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("unlikePostRequest")
    public ClientUnlikePostConfirmation onUnlikePostRequest(
            @Valid @RequestBody ClientUnlikePostRequest request) {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();
        final Post post =
                postService.findByIdFilteredByHiddenForOtherHomeAreasAndGroupContentVisibility(request.getPostId(),
                        person, appVariant);

        final UnlikePostRequest unlikePostRequest = UnlikePostRequest.builder()
                .post(post)
                .unliker(person)
                .build();

        UnlikePostConfirmation unlikePostConfirmation =
                notifyAndWaitForReply(UnlikePostConfirmation.class, unlikePostRequest);

        return new ClientUnlikePostConfirmation(
                grapevineModelMapper.toClientPost(unlikePostConfirmation.getPost(), false));
    }

    @ApiOperation(value = "Flags a Post as inappropriate",
            notes = "optional 'comment' is the reason for flagging this entity")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_ALREADY_EXISTS
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("postFlagRequest")
    public ClientUserGeneratedContentFlagResponse postFlagRequest(
            @Valid @RequestBody ClientPostFlagRequest request) {

        final Person flagCreator = getCurrentPersonNotNull();
        final Post post = postService.findPostByIdNotDeleted(request.getPostId());

        final PostFlagRequest flagRequest = PostFlagRequest.builder()
                .entity(post)
                .entityAuthor(post.getCreator())
                .entityDescription(
                        String.format("%s in %s: '%s'", post.getPostType(), post.getGeoAreas(), post.getText()))
                .flagCreator(flagCreator)
                .tenant(post.getTenant())
                .comment(request.getComment())
                .allowMultipleFlagsOfSameEntity(false)
                .build();

        final UserGeneratedContentFlagConfirmation confirmation =
                notifyAndWaitForReply(UserGeneratedContentFlagConfirmation.class, flagRequest);
        return new ClientUserGeneratedContentFlagResponse(confirmation.getCreatedFlag());
    }

    @ApiOperation(value = "Reports about the timestamps of viewed posts",
            notes = "Reports with the timestamp (given as long using Unix timestamp in UTC) what posts were viewed" +
                    " and what posts were viewed in more detail")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("postsViewEvent")
    public void onPostsViewEvent(
            @Valid @RequestBody ClientPostsViewEvent request) {

        final Person person = getCurrentPersonNotNull();
        if (CollectionUtils.isEmpty(request.getPostViews())) {
            return;
        }
        final long now = getTimeService().currentTimeMillisUTC();
        final long latestViewTime = now - TimeUnit.DAYS.toMillis(7);
        final List<PostView> postViews = new ArrayList<>(request.getPostViews().size());
        for (ClientPostView clientPostView : request.getPostViews()) {
            Post post;
            try {
                post = postService.findPostByIdIncludingDeleted(clientPostView.getPostId());
            } catch (PostNotFoundException e) {
                //not found posts are ignored in order to get as much data as possible
                continue;
            }
            PostView postView = PostView.builder()
                    .post(post)
                    .build();
            if (clientPostView.getViewedOverviewTime() != null) {
                if (clientPostView.getViewedOverviewTime() < latestViewTime) {
                    //view time too much in the past
                    postView.setViewedOverviewTime(null);
                } else if (clientPostView.getViewedOverviewTime() > now) {
                    // view time in the future
                    postView.setViewedOverviewTime(now);
                } else {
                    postView.setViewedOverviewTime(clientPostView.getViewedOverviewTime());
                }
            }
            if (clientPostView.getViewedDetailTime() != null) {
                if (clientPostView.getViewedDetailTime() < latestViewTime) {
                    //view time too much in the past
                    postView.setViewedDetailTime(null);
                } else if (clientPostView.getViewedDetailTime() > now) {
                    // view time in the future
                    postView.setViewedDetailTime(now);
                } else {
                    postView.setViewedDetailTime(clientPostView.getViewedDetailTime());
                }
            }
            if (postView.getViewedOverviewTime() != null || postView.getViewedDetailTime() != null) {
                postViews.add(postView);
            }
        }

        notify(PostsViewEvent.builder()
                .viewer(person)
                .postViews(postViews)
                .build());
    }

    private ClientPostChangeConfirmation handlePostChangeRequest(Person person,
            BasePostChangeRequest<?> changeRequest) {
        PostChangeConfirmation<?> changeConfirmation =
                notifyAndWaitForReply(PostChangeConfirmation.class, changeRequest);

        final Post changedPost = changeConfirmation.getPost();
        return new ClientPostChangeConfirmation(
                grapevineModelMapper.toClientPost(changedPost,
                        postInteractionService.isPostLiked(person, changedPost)));
    }

    private ClientPostCreateConfirmation handlePostCreateRequest(BasePostCreateRequest createRequest) {
        PostCreateConfirmation<?> createConfirmation =
                notifyAndWaitForReply(PostCreateConfirmation.class, createRequest);

        final Post createdPost = createConfirmation.getPost();
        return ClientPostCreateConfirmation.builder()
                .post(grapevineModelMapper.toClientPost(createdPost, false))
                .organizationTags(grapevineModelMapper.getClientOrganizationTagsOrNull(createdPost))
                .build();
    }

    private TradingCategory getTradingCategoryOrDefault(ClientTradeCreateRequest request) {
        if (request.getTradingCategoryId() != null) {
            return tradingCategoryService.findTradingCategoryById(request.getTradingCategoryId());
        } else {
            return tradingCategoryService.getDefaultTradingCategory();
        }
    }

    private TradingCategory getTradingCategoryNull(ClientTradeChangeRequest request) {
        if (request.getTradingCategoryId() != null) {
            return tradingCategoryService.findTradingCategoryById(request.getTradingCategoryId());
        } else {
            return null;
        }
    }

}
