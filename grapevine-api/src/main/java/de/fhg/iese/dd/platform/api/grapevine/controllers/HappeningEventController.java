/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.happening.ClientHappeningParticipationConfirmConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.happening.ClientHappeningParticipationConfirmRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.happening.ClientHappeningParticipationRevokeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.happening.ClientHappeningParticipationRevokeRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningParticipationConfirmConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningParticipationConfirmRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningParticipationRevokeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningParticipationRevokeRequest;
import de.fhg.iese.dd.platform.business.grapevine.services.IHappeningService;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostInteractionService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/grapevine/event")
@Api(tags = {"grapevine.happening.events"})
public class HappeningEventController extends BasePostEventController {

    @Autowired
    private IHappeningService happeningService;
    @Autowired
    private IPostInteractionService postInteractionService;

    @ApiOperation(value = "Announce participation in happening")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping(value = "happeningParticipationConfirmRequest")
    ClientHappeningParticipationConfirmConfirmation onHappeningParticipationConfirmRequest(
            @Valid @RequestBody ClientHappeningParticipationConfirmRequest request) {

        final Person currentPerson = getCurrentPersonNotNull();

        final Happening happening = happeningService.findById(request.getHappeningId());
        final HappeningParticipationConfirmRequest happeningParticipationConfirmRequest =
                HappeningParticipationConfirmRequest.builder()
                        .happening(happening)
                        .personToParticipate(currentPerson)
                        .build();

        final HappeningParticipationConfirmConfirmation happeningParticipationConfirmConfirmation =
                notifyAndWaitForReply(HappeningParticipationConfirmConfirmation.class,
                        happeningParticipationConfirmRequest);
        final Happening updatedHappening = happeningParticipationConfirmConfirmation.getUpdatedHappening();

        return ClientHappeningParticipationConfirmConfirmation.builder()
                .updatedHappening(grapevineModelMapper.toClientPost(updatedHappening,
                        postInteractionService.isPostLiked(currentPerson, updatedHappening)))
                .build();
    }

    @ApiOperation(value = "Revoke participation in happening")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping(value = "happeningParticipationRevokeRequest")
    ClientHappeningParticipationRevokeConfirmation onHappeningParticipationRevokeRequest(
            @Valid @RequestBody ClientHappeningParticipationRevokeRequest request) {

        final Person currentPerson = getCurrentPersonNotNull();

        final Happening happening = happeningService.findById(request.getHappeningId());
        final HappeningParticipationRevokeRequest happeningParticipationRevokeRequest =
                HappeningParticipationRevokeRequest.builder()
                        .happening(happening)
                        .personToCancel(currentPerson)
                        .build();

        final HappeningParticipationRevokeConfirmation happeningParticipationRevokeConfirmation =
                notifyAndWaitForReply(HappeningParticipationRevokeConfirmation.class,
                        happeningParticipationRevokeRequest);
        final Happening updatedHappening = happeningParticipationRevokeConfirmation.getUpdatedHappening();

        return ClientHappeningParticipationRevokeConfirmation.builder()
                .updatedHappening(grapevineModelMapper.toClientPost(updatedHappening,
                        postInteractionService.isPostLiked(currentPerson, updatedHappening)))
                .build();
    }

}
