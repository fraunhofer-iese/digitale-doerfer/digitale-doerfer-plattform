/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientOrganization;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientOrganizationTag;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.services.IOrganizationService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationTag;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/grapevine/organization")
@Api(tags = {"grapevine.organization"})
public class OrganizationController extends BaseController {

    @Autowired
    private IOrganizationService organizationService;
    @Autowired
    private GrapevineClientModelMapper grapevineClientModelMapper;

    @ApiOperation(value = "Return all available organizations",
            notes = "All organizations where one of selected geo areas of the user is in the geo areas of the organization." +
                    "Filtered by tags, the organizations need to have at least one of the tags.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping
    public List<ClientOrganization> getAllAvailableOrganizations(
            @ApiParam(value = "Organizations need to have at least one of the tags. If empty, all are returned.")
            @RequestParam(required = false)
            Set<ClientOrganizationTag> requiredTags
    ) {

        Person person = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();
        Set<OrganizationTag> tags;
        if (CollectionUtils.isEmpty(requiredTags)) {
            tags = EnumSet.allOf(OrganizationTag.class);
        } else {
            tags = grapevineClientModelMapper.toOrganizationTags(requiredTags);
        }

        return organizationService.findAllAvailableOrganizations(person, appVariant, tags).stream()
                .map(organization -> grapevineClientModelMapper.toClientOrganization(organization,
                        organizationService.getRelatedGeoAreaIds(organization)))
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Return a organization by id",
            notes = "No access restriction takes place, all organizations can be queried.")
    @ApiExceptions({ClientExceptionType.ORGANIZATION_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/{organizationId}")
    public ClientOrganization getOrganizationById(@PathVariable String organizationId) {

        //used to enforce authentication
        getCurrentPersonNotNull();
        getAppVariantNotNull();

        Organization organization = organizationService.findOrganizationById(organizationId);
        return grapevineClientModelMapper.toClientOrganization(organization,
                organizationService.getRelatedGeoAreaIds(organization));
    }

}
