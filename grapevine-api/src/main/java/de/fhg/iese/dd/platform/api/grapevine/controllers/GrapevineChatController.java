/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChat;
import de.fhg.iese.dd.platform.api.communication.clientmodel.CommunicationClientModelMapper;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineChatService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/grapevine/chat")
@Api(tags = { "grapevine.chat" })
class GrapevineChatController extends BaseController {

    @Autowired
    private IGrapevineChatService grapevineChatService;
    @Autowired
    private CommunicationClientModelMapper communicationClientModelMapper;

    @ApiOperation(value = "Return all private chats of the person",
            notes = "Return all private chats the person is in.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("own")
    public List<ClientChat> getOwnChats() {

        Person person = getCurrentPersonNotNull();

        List<Chat> ownChats = grapevineChatService.findOwnChats(person);

        return communicationClientModelMapper.toClientChats(ownChats, person);
    }

}
