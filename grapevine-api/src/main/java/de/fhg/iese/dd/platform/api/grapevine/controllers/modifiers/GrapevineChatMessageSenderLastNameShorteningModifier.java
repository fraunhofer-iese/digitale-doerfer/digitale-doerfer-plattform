/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers.modifiers;

import java.util.Set;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.communication.controllers.modifiers.ChatMessageSenderLastNameShorteningModifier;

@Component
public class GrapevineChatMessageSenderLastNameShorteningModifier extends ChatMessageSenderLastNameShorteningModifier {

    //Currently the push messages are not sent, but we want to be sure that they do not accidentally include the full name
    @Override
    public Set<String> getPushEventPackagePrefixes() {
        return GrapevineLastNameShorteningModifier.PUSH_EVENT_PACKAGE_PREFIX;
    }

    @Override
    public Set<String> getPathPatterns() {
        return ClientGrapevineChatFlagAndBlockModifier.GRAPEVINE_CHAT_CONTROLLER_PATHS;
    }

}
