/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers.modifiers;

import java.util.Set;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.framework.modifiers.IContentFilterSettings;
import de.fhg.iese.dd.platform.api.framework.modifiers.IOutgoingClientEntityModifier;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientGroup;

@Component
class ClientGroupFlagModifier implements IOutgoingClientEntityModifier<ClientGroup> {

    @Override
    public Class<ClientGroup> getType() {
        return ClientGroup.class;
    }

    @Override
    public Set<String> getPushEventPackagePrefixes() {
        return GrapevineLastNameShorteningModifier.PUSH_EVENT_PACKAGE_PREFIX;
    }

    @Override
    public Set<String> getPathPatterns() {
        return GrapevineLastNameShorteningModifier.GRAPEVINE_PATHS;
    }

    @Override
    public boolean modify(ClientGroup clientGroup, IContentFilterSettings contentFilterSettings) {
        IOutgoingClientEntityModifier.handleFlaggingAndBlocking(clientGroup,
                null,
                contentFilterSettings);
        return true;
    }

}
