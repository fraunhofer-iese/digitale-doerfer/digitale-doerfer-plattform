/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Adeline Silva Schäfer, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientmodel;

import java.util.function.Function;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsItem;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Offer;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Seeking;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SpecialPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;

public enum ClientPostType {

    //Arrow "->" be used here ("ClientPostTypeVisitor::whenGossip" causes raw type compiler warning)
    GOSSIP(ClientPostTypeVisitor::whenGossip),
    NEWS_ITEM(ClientPostTypeVisitor::whenNewsItem),
    SUGGESTION(ClientPostTypeVisitor::whenSuggestion),
    SEEKING(ClientPostTypeVisitor::whenSeeking),
    OFFER(ClientPostTypeVisitor::whenOffer),
    HAPPENING(ClientPostTypeVisitor::whenHappening),
    SPECIAL_POST(ClientPostTypeVisitor::whenSpecialPost);

    private static final ClientPostTypeVisitor<Class<? extends Post>> TO_POST_CLASS =
            new ClientPostTypeVisitor<>() {

                @Override
                public Class<? extends Post> whenGossip() {
                    return Gossip.class;
                }

                @Override
                public Class<? extends Post> whenNewsItem() {
                    return NewsItem.class;
                }

                @Override
                public Class<? extends Post> whenSuggestion() {
                    return Suggestion.class;
                }

                @Override
                public Class<? extends Post> whenSeeking() {
                    return Seeking.class;
                }

                @Override
                public Class<? extends Post> whenOffer() {
                    return Offer.class;
                }

                @Override
                public Class<? extends Post> whenHappening() {
                    return Happening.class;
                }

                @Override
                public Class<? extends Post> whenSpecialPost() {
                    return SpecialPost.class;
                }
            };

    private static final ClientPostTypeVisitor<PostType> TO_POST_TYPE =
            new ClientPostTypeVisitor<>() {

                @Override
                public PostType whenGossip() {
                    return PostType.Gossip;
                }

                @Override
                public PostType whenNewsItem() {
                    return PostType.NewsItem;
                }

                @Override
                public PostType whenSuggestion() {
                    return PostType.Suggestion;
                }

                @Override
                public PostType whenSeeking() {
                    return PostType.Seeking;
                }

                @Override
                public PostType whenOffer() {
                    return PostType.Offer;
                }

                @Override
                public PostType whenHappening() {
                    return PostType.Happening;
                }

                @Override
                public PostType whenSpecialPost() {
                    return PostType.SpecialPost;
                }
            };

    //safe to declare it transient since an enum is serialized by it constant name (String) only
    // cf. section 1.12 in https://docs.oracle.com/javase/1.5.0/docs/guide/serialization/spec/serial-arch.html#enum
    private final transient Function<ClientPostTypeVisitor<?>, Object> visitor;

    ClientPostType(final Function<ClientPostTypeVisitor<?>, Object> visitor) {
        this.visitor = visitor;
    }

    @SuppressWarnings("unchecked") //the visitor returns type T, there is just no option to pass <T> to the Function
    public <T> T accept(final ClientPostTypeVisitor<T> v) {
        return (T) visitor.apply(v);
    }

    public Class<? extends Post> toPostClass() {
        return accept(TO_POST_CLASS);
    }

    public PostType toPostType() {
        return accept(TO_POST_TYPE);
    }

}
