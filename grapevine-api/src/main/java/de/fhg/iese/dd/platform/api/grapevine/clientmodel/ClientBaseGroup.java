/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientmodel;

import de.fhg.iese.dd.platform.api.framework.clientmodel.FilteredClientBaseEntity;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItem;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupAccessibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupContentVisibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupVisibility;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public abstract class ClientBaseGroup extends FilteredClientBaseEntity {

    @ApiModelProperty(required = true)
    private String name;
    @ApiModelProperty(required = true)
    private String shortName;
    private String description;
    private ClientMediaItem logo;
    @ApiModelProperty(required = true)
    private GroupVisibility visibility;
    @ApiModelProperty(required = true)
    private GroupContentVisibility contentVisibility;
    @ApiModelProperty(required = true)
    private GroupAccessibility accessibility;
    @ApiModelProperty(required = true)
    private long memberCount;
    @ApiModelProperty(required = true)
    private boolean deleted;
    @ApiModelProperty(value = "Main geo area, might be null for existing groups")
    private String mainGeoAreaId;

}
