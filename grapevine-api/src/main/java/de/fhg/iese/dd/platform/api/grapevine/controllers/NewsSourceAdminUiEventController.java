/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Benjamin Hassenfratz, Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientNewsSourceCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientNewsSourceCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientNewsSourceDeleteConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientNewsSourceDeleteRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.business.grapevine.events.NewsSourceCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.NewsSourceCreateRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.NewsSourceDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.NewsSourceDeleteRequest;
import de.fhg.iese.dd.platform.business.grapevine.security.CreateNewsSourceAction;
import de.fhg.iese.dd.platform.business.grapevine.security.DeleteNewsSourceAction;
import de.fhg.iese.dd.platform.business.grapevine.services.INewsSourceService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.security.services.IOauthClientService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/adminui/newssource/event")
@Api(tags = {"grapevine.adminui.newssource.events"})
public class NewsSourceAdminUiEventController extends BaseAdminUiController {

    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private IAppService appService;
    @Autowired
    private INewsSourceService newsSourceService;
    @Autowired
    private IOauthClientService oauthClientService;

    @Autowired
    private GrapevineClientModelMapper grapevineModelMapper;

    @ApiOperation(value = "Creates a news source",
            notes = "Creates a new news source. Since the news source requires an app variant, a new app variant is also created.")
    @ApiExceptions({
            ClientExceptionType.APP_NOT_FOUND,
            ClientExceptionType.OAUTH_CLIENT_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND,
            ClientExceptionType.APP_VARIANT_ALREADY_EXISTS,
            ClientExceptionType.NEWS_SOURCE_ALREADY_EXISTS
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {CreateNewsSourceAction.class}
    )
    @PostMapping("newsSourceCreateRequest")
    public ClientNewsSourceCreateConfirmation onNewsSourceCreateRequest(
            @Valid @RequestBody ClientNewsSourceCreateRequest request
    ) {

        Person currentPerson = getCurrentPersonNotNull();

        getRoleService().decidePermissionAndThrowNotAuthorized(CreateNewsSourceAction.class, currentPerson);
        OauthClient oauthClient;
        if (StringUtils.isEmpty(request.getOauthClientIdentifier())) {
            oauthClient = null;
        } else {
            oauthClient = oauthClientService.findByOauthClientIdentifier(request.getOauthClientIdentifier());
        }
        NewsSourceCreateRequest createRequest = NewsSourceCreateRequest.builder()
                .creator(currentPerson)
                .siteUrl(request.getSiteUrl())
                .siteName(request.getSiteName())
                .logo(mediaItemService.findTemporaryItemById(currentPerson, request.getTemporaryMediaItemId()))
                .callbackUrl(request.getCallBackUrl())
                .app(appService.findById(request.getAppId()))
                .externalBasicAuth(request.getExternalBasicAuth())
                .externalApiKey(request.getExternalApiKey())
                .oauthClient(oauthClient)
                .changeNotes(request.getChangeNotes())
                .build();

        NewsSourceCreateConfirmation
                createConfirmation = notifyAndWaitForReply(NewsSourceCreateConfirmation.class, createRequest);

        return ClientNewsSourceCreateConfirmation.builder()
                .newsSource(grapevineModelMapper.toClientNewsSource(createConfirmation.getNewsSource()))
                .build();
    }

    @ApiOperation(value = "Deletes a news source",
            notes = "Deletes a news source. Since the news source requires an app variant, the app variant is soft deleted too.")
    @ApiExceptions({
            ClientExceptionType.NEWS_SOURCE_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {DeleteNewsSourceAction.class}
    )
    @PostMapping("newsSourceDeleteRequest")
    public ClientNewsSourceDeleteConfirmation onNewsSourceDeleteRequest(
            @Valid @RequestBody ClientNewsSourceDeleteRequest request
    ) {

        Person currentPerson = getCurrentPersonNotNull();

        getRoleService().decidePermissionAndThrowNotAuthorized(DeleteNewsSourceAction.class, currentPerson);

        NewsSourceDeleteRequest deleteRequest = NewsSourceDeleteRequest.builder()
                .deleter(currentPerson)
                .newsSource(newsSourceService.findNewsSourceById(request.getNewsSourceId()))
                .changeNotes(request.getChangeNotes())
                .build();

        NewsSourceDeleteConfirmation deleteConfirmation =
                notifyAndWaitForReply(NewsSourceDeleteConfirmation.class, deleteRequest);

        return ClientNewsSourceDeleteConfirmation.builder()
                .newsSourceId(deleteConfirmation.getDeletedNewsSource().getId())
                .message(deleteConfirmation.getMessage())
                .build();
    }

}
