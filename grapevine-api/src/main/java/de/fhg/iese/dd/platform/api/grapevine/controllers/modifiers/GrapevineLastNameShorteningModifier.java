/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers.modifiers;

import java.util.Collections;
import java.util.Set;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier;

@Component
class GrapevineLastNameShorteningModifier extends BaseLastNameShorteningModifier {

    static final Set<String> PUSH_EVENT_PACKAGE_PREFIX =
            Collections.singleton("de.fhg.iese.dd.platform.api.grapevine.clientevent");

    static final Set<String> GRAPEVINE_PATHS = Collections.singleton("/grapevine/**");

    private static final Set<String> GRAPEVINE_EXCLUDED_PATHS =
            Set.of("/grapevine/suggestion/*/potentialWorker",
                    "/grapevine/suggestion/*",
                    //these are handled by ExtendedSuggestionLastNameShorteningModifier
                    "/grapevine/event/suggestionStatusChangeRequest",
                    "/grapevine/event/suggestionCategoryChangeRequest",
                    "/grapevine/event/suggestionWorkerAddRequest",
                    "/grapevine/event/suggestionWorkerRemoveRequest");

    @Override
    public Set<String> getPushEventPackagePrefixes() {
        return PUSH_EVENT_PACKAGE_PREFIX;
    }

    @Override
    public Set<String> getPathPatterns() {
        return GRAPEVINE_PATHS;
    }

    @Override
    public Set<String> getExcludedPathPatterns() {
        return GRAPEVINE_EXCLUDED_PATHS;
    }

}
