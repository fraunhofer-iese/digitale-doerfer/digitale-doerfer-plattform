/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidEventAttributeException;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientExternalPostDeleteByIdRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientExternalPostDeleteConfirmation;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.business.grapevine.events.PostDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostDeleteRequest;
import de.fhg.iese.dd.platform.business.grapevine.security.DeleteExternalPostAction;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostService;
import de.fhg.iese.dd.platform.business.shared.security.services.PermissionEntityRestricted;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/adminui/post/event")
@Api(tags = {"grapevine.adminui.post.event"})
public class ExternalPostAdminUiEventController extends BaseAdminUiController {

    @Autowired
    private IPostService postService;

    @ApiOperation(value = "Deletes a NewsItem, Happening",
            notes = "Deletes an existing NewsItem or Happening. Only items created by the same news source can be deleted.")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {DeleteExternalPostAction.class})
    @PostMapping(value = "externalPostDeleteRequest")
    public ClientExternalPostDeleteConfirmation externalPostDeleteRequest(
            @Valid @RequestBody ClientExternalPostDeleteByIdRequest request) {

        final Person currentPerson = getCurrentPersonNotNull();

        final String postId = request.getPostId();
        addAdditionalLogValue("postId", postId);

        final ExternalPost externalPost;
        if (StringUtils.isNotEmpty(postId)) {
            externalPost = postService.findExternalPostById(postId);
        } else {
            throw new InvalidEventAttributeException(
                    "postId is empty");
        }
        addAdditionalLogValue("newsURL", externalPost.getUrl());

        PermissionEntityRestricted<NewsSource> permissionNewsSourceRestricted = getRoleService()
                .decidePermission(DeleteExternalPostAction.class, currentPerson);

        if (permissionNewsSourceRestricted.isEntityDenied(externalPost.getNewsSource())) {
            throw new NotAuthorizedException("No permission to delete external post for news source '{}'",
                    externalPost.getNewsSource().getId());
        }

        PostDeleteRequest deleteRequest = new PostDeleteRequest(externalPost);

        PostDeleteConfirmation deleteConfirmation = notifyAndWaitForReply(PostDeleteConfirmation.class, deleteRequest);
        return ClientExternalPostDeleteConfirmation.builder()
                .postId(deleteConfirmation.getPost().getId())
                .externalId(externalPost.getExternalId())
                .build();
    }

}
