/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonReference;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.services.IHappeningService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.HappeningListParticipantsFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/grapevine/happening")
@Api(tags = {"grapevine.happening"})
public class HappeningController extends BaseController {

    private static final Sort DEFAULT_MEMBER_SORT = Sort.by("person.firstName");

    @Autowired
    private IHappeningService happeningService;
    @Autowired
    private IFeatureService featureService;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;

    @ApiOperation(value = "Return the list of participants for a happening",
            notes = "Return all participants for a happening, excluding participants having status 'PARALLEL_WORLD_INHABITANT'." +
                    "Only enabled if the 'HappeningListParticipantsFeature' is enabled for the requesting app variant and user."
    )
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID,
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.FEATURE_NOT_ENABLED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping("/{happeningId}/participants")
    public Page<ClientPersonReference> getAllParticipantsById(
            @PathVariable String happeningId,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of participants",
                    defaultValue = "0") int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of participants returned in a page",
                    defaultValue = "10") int count) {

        final Person currentPerson = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        // if the feature is not enabled an exception is thrown
        featureService.checkFeatureEnabled(HappeningListParticipantsFeature.class,
                FeatureTarget.of(currentPerson, appVariant));

        checkPageAndCountValues(page, count);

        final PageRequest pageRequest = PageRequest.of(page, count, DEFAULT_MEMBER_SORT);
        final Happening happening = happeningService.findById(happeningId);
        final Page<Person> participants =
                happeningService.findAllParticipantsByHappening(happening, currentPerson, pageRequest);

        return participants.map(personClientModelMapper::createClientPersonReference);
    }

}
