/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientmodel;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import de.fhg.iese.dd.platform.api.framework.clientmodel.FilteredClientBaseEntity;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonReference;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddress;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientDocumentItem;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItem;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonWriter;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public abstract class ClientPostBaseEntity extends FilteredClientBaseEntity {

    private ClientPersonReference creator;
    private List<String> geoAreaIds;
    private boolean hiddenForOtherHomeAreas;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String groupId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String organizationId;
    private List<ClientMediaItem> images;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<ClientDocumentItem> attachments;
    private String text;
    private long created;
    private long lastModified;
    private long commentCount;
    @ApiModelProperty("If true comments are allowed for everyone. If false, only the creator can comment")
    private boolean commentsAllowed;
    private long likeCount;
    @ApiModelProperty("""
            Can be "true", "false" or "null".

            Currently "null" is only used when pushing a PostChangeEvent to a GeoArea.
            In this case, we can not reliably have an individual liked state for each person.
            Do not overwrite any cached liked state on this case.""")
    private Boolean liked;
    private long lastActivity;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ClientAddress customAddress;
    @JsonRawValue
    @ApiModelProperty(dataType = "java.util.Map")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String customAttributes;

    /**
     * Only required for deserialization in tests
     *
     * @param customAttributesMap
     */
    @JsonSetter("customAttributes")
    private void setCustomAttributesMap(Map<String, Object> customAttributesMap) {

        try {
            customAttributes = defaultJsonWriter().writeValueAsString(customAttributesMap);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @JsonIgnore
    public void setCustomAttributes(String customAttributes) {

        this.customAttributes = customAttributes;
    }

    @Deprecated
    @JsonProperty("customLocation")
    @ApiModelProperty(value = "Deprecated, use customAddress instead", hidden = true)
    public ClientGPSLocation getGPSLocation() {
        if (customAddress == null) {
            return null;
        }
        return customAddress.getGpsLocation();
    }

    @Deprecated
    @JsonProperty("lastComment")
    @ApiModelProperty(hidden = true)
    public long getLastComment() {
        return lastActivity;
    }

    @Deprecated
    @JsonProperty("geoAreaId")
    public String getGeoAreaId() {

        if (CollectionUtils.isEmpty(geoAreaIds)) {
            return null;
        } else {
            //we take the first geo area id (they are sorted by id), so that it doesn't change with every call
            return geoAreaIds.get(0);
        }
    }

}
