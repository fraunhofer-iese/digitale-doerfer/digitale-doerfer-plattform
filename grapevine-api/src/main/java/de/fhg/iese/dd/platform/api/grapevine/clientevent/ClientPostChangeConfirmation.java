/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2019 Adeline Silva Schäfer, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientevent;

import java.util.Collections;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.framework.clientevent.ClientMinimizableEvent;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPost;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientPostChangeConfirmation extends ClientBaseEvent implements ClientMinimizableEvent {

    @Getter
    @AllArgsConstructor
    private static class Minimized extends ClientBaseEvent {

        private final String postId;
        @JsonProperty("post")
        private final Map<String, ClientPostType> type;

    }

    public String getPostId() {
        return post.getId();
    }

    @NotNull
    private ClientPost post;

    @Override
    public Minimized toMinimizedEvent() {
        return new ClientPostChangeConfirmation.Minimized(getPostId(),
                Collections.singletonMap("type", post.getType()));
    }

}
