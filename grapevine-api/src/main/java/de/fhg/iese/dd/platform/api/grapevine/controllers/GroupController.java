/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientGroup;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPost;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostType;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonReference;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.InvalidSortingCriterionException;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostInteractionService;
import de.fhg.iese.dd.platform.business.grapevine.services.PagedQuery;
import de.fhg.iese.dd.platform.business.grapevine.services.PostSortCriteria;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.GroupListMembersFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.GroupMembershipAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/grapevine/group")
@Api(tags = {"grapevine.group"})
public class GroupController extends BaseController {

    @Autowired
    private IGroupService groupService;
    @Autowired
    private IPostInteractionService postInteractionService;
    @Autowired
    private GrapevineClientModelMapper grapevineModelMapper;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;

    @ApiOperation(value = "Return all available groups",
            notes = """
                    All Groups the person is allowed to see, depending on the group visibility setting:
                    - None of the excluded geo areas of the group is in available geo areas of app variant
                    - Depending on the setting of the group visibility:
                      - ANYONE:
                        - Home area of the caller is in geo areas of group
                        - One of selected geo areas of the caller in geo areas of group
                      - SAME_HOME_AREA:
                        - Home area of the caller is in geo areas of the group


                    Additionally these groups are returned:
                    + Already joined
                    + Requested ones""")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping
    public List<ClientGroup> getAllAvailableGroups() {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        List<Group> groups =
                groupService.findAllAvailableGroupsFilteredByGroupSettingsAndFeatureConfig(person, appVariant);
        Map<Group, GroupMembershipStatus> groupMemberships =
                groupService.findGroupMembershipStatusByGroup(groups, person);
        return grapevineModelMapper.toClientGroups(groups, groupMemberships,
                groupService.findGeoAreaIdsByGroups(groups));
    }

    @ApiOperation(value = "Return a group by id",
            notes = """
                    Only groups the user is allowed to see are returned, depending on the group visibility setting:
                    - None of the excluded geo areas of the group is in available geo areas of app variant
                    - Depending on the setting of the group visibility:
                      - ANYONE:
                        - Home area of the caller is in geo areas of group
                        - One of selected geo areas of the caller in geo areas of group
                      - SAME_HOME_AREA:
                        - Home area of the caller is in geo areas of the group


                    Additionally these groups are returned:
                    + Already joined
                    + Requested ones""")
    @ApiExceptions({ClientExceptionType.GROUP_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/{groupId}")
    public ClientGroup getGroupById(@PathVariable String groupId) {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        Group group = groupService.findGroupByIdFilteredByGroupSettings(groupId, person, appVariant);
        GroupMembershipStatus groupMembership = groupService.findGroupMembershipStatusForGroup(group, person);
        return grapevineModelMapper.toClientGroup(group, groupMembership, groupService.findGeoAreaIdsForGroup(group));
    }

    @ApiOperation(value = "Return all posts for a group",
            notes = """
                    Return all posts for the given group. The response is paged.

                    | sorting criteria | attribute used for filtering and sorting | returned posts are subtype of | direction | default start if null | default end if null |
                    | ---- | ---- | ---- | ---- | ---- | ---- |
                    | CREATED | created | Post | DESC | MIN | MAX |
                    | LAST_ACTIVITY | lastActivity | Post | DESC | MIN | MAX |
                    """
    )
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND,
            ClientExceptionType.GROUP_CONTENT_NOT_ACCESSIBLE,
            ClientExceptionType.PAGE_PARAMETER_INVALID,
            ClientExceptionType.INVALID_SORTING_CRITERION
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping("/{groupId}/post")
    public Page<ClientPost> getAllPostsByAppVariantAndSelectedGeoAreas(

            @PathVariable
            @ApiParam(value = "Id of the group to return the posts for")
                    String groupId,

            @RequestParam(required = false)
            @ApiParam(value = "Only return posts of this type")
                    ClientPostType type,

            @RequestParam(required = false)
            @ApiParam(value = "Start time from which the posts should be returned")
                    Long startTime,

            @RequestParam(required = false)
            @ApiParam(value = "End time from which the posts should be returned")
                    Long endTime,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of posts",
                    defaultValue = "0") int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of posts returned in a page",
                    defaultValue = "10") int count,

            @RequestParam(required = false, defaultValue = "CREATED")
            @ApiParam(value = "sorting criteria, see table above for clarification",
                    defaultValue = "CREATED") PostSortCriteria sortBy) {

        checkPageAndCountValues(page, count);

        checkSortCriteria(sortBy, type);

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        Group group = groupService.findGroupByIdFilteredByGroupSettings(groupId, person, appVariant);

        groupService.checkIsGroupContentVisible(group, person, appVariant);

        final PagedQuery pagedQuery = PagedQuery.builder()
                .pageNumber(page)
                .elementsPerPage(count)
                .sortCriteria(sortBy)
                .build();

        final Page<Post> posts = groupService.findAllPostsInGroup(group, grapevineModelMapper.toPostType(type),
                startTime, endTime, pagedQuery);

        Set<Post> likedPosts = postInteractionService.filterLikedPosts(person, posts.getContent());

        return posts.map(post -> grapevineModelMapper.toClientPost(post, likedPosts.contains(post)));
    }

    @ApiOperation(value = "Return the list of members for a group",
            notes = "Return all members of a group. Only enabled if the 'GroupListMembersFeature' is enabled for the " +
                    "requesting app variant and user.")
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID,
            ClientExceptionType.GROUP_NOT_FOUND,
            ClientExceptionType.FEATURE_NOT_ENABLED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping("/{groupId}/members")
    public Page<ClientPersonReference> getAllMembersByGroupId(
            @PathVariable String groupId,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of participants",
                    defaultValue = "0") int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of participants returned in a page",
                    defaultValue = "10") int count) {

        final Person currentPerson = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        // if the feature is not enabled an exception is thrown
        getFeatureService().checkFeatureEnabled(GroupListMembersFeature.class,
                FeatureTarget.of(currentPerson, appVariant));

        checkPageAndCountValues(page, count);

        final PageRequest pageRequest = PageRequest.of(page, count);
        final Group group = groupService.findGroupByIdFilteredByGroupSettings(groupId, currentPerson, appVariant);

        groupService.checkIsGroupContentVisible(group, currentPerson, appVariant);

        final Page<Person> members =
                groupService.findAllApprovedMembers(group, pageRequest);

        return members.map(personClientModelMapper::createClientPersonReference);
    }

    @ApiOperation(value = "Return the list of group membership admins for a group",
            notes = "Return all group membership admins of a group. Only enabled if the 'GroupListMembersFeature' is " +
                    "enabled for the requesting app variant and user.")
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID,
            ClientExceptionType.GROUP_NOT_FOUND,
            ClientExceptionType.FEATURE_NOT_ENABLED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping("/{groupId}/admins")
    public List<ClientPersonReference> getAllGroupAdminsByGroupId(
            @PathVariable String groupId) {

        final Person currentPerson = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        // if the feature is not enabled an exception is thrown
        getFeatureService().checkFeatureEnabled(GroupListMembersFeature.class,
                FeatureTarget.of(currentPerson, appVariant));

        final Group group = groupService.findGroupByIdFilteredByGroupSettings(groupId, currentPerson, appVariant);

        groupService.checkIsGroupContentVisible(group, currentPerson, appVariant);

        final List<Person> admins =
                getRoleService().getAllPersonsWithRoleForEntity(GroupMembershipAdmin.class, group);

        return admins.stream()
                .map(personClientModelMapper::createClientPersonReference)
                .sorted(Comparator.comparing(ClientPersonReference::getFirstName)
                        .thenComparing(ClientPersonReference::getLastName))
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Return all posts for all member groups",
            notes = """
                    Return all posts. The response is paged.

                    | sorting criteria | attribute used for filtering and sorting | returned posts are subtype of | direction | default start if null | default end if null |
                    | ---- | ---- | ---- | ---- | ---- | ---- |
                    | CREATED | created | Post | DESC | MIN | MAX |
                    | LAST_ACTIVITY | lastActivity | Post | DESC | MIN | MAX |
                    """
    )
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID,
            ClientExceptionType.INVALID_SORTING_CRITERION
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/post")
    public Page<ClientPost> getAllPostsByMemberGroups(

            @RequestParam(required = false)
            @ApiParam(value = "Only return posts of this type")
            ClientPostType type,

            @RequestParam(required = false)
            @ApiParam(value = "Start time from which the posts should be returned")
            Long startTime,

            @RequestParam(required = false)
            @ApiParam(value = "End time from which the posts should be returned")
            Long endTime,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of posts",
                    defaultValue = "0") int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of posts returned in a page",
                    defaultValue = "10") int count,

            @RequestParam(required = false, defaultValue = "CREATED")
            @ApiParam(value = "sorting criteria, see table above for clarification",
                    defaultValue = "CREATED") PostSortCriteria sortBy) {

        checkPageAndCountValues(page, count);

        checkSortCriteria(sortBy, type);

        final Person person = getCurrentPersonNotNull();

        final PagedQuery pagedQuery = PagedQuery.builder()
                .pageNumber(page)
                .elementsPerPage(count)
                .sortCriteria(sortBy)
                .build();

        final Page<Post> posts =
                groupService.findAllPostsInMemberGroups(person, grapevineModelMapper.toPostType(type), startTime,
                        endTime, pagedQuery);

        Set<Post> likedPosts = postInteractionService.filterLikedPosts(person, posts.getContent());

        return posts.map(post -> grapevineModelMapper.toClientPost(post, likedPosts.contains(post)));
    }

    private void checkSortCriteria(final PostSortCriteria sortBy, final ClientPostType postType) {
        final Class<? extends Post> typeClass = (postType == null)
                ? Post.class
                : postType.toPostClass();
        if (!sortBy.isAvailableFor(typeClass)) {
            throw new InvalidSortingCriterionException("sortBy {} is not available for type {}", sortBy, postType);
        }
    }

}
