/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientNewsSource;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.business.grapevine.security.ListNewsSourcesAction;
import de.fhg.iese.dd.platform.business.grapevine.services.INewsSourceService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/adminui/newssource")
@Api(tags = {"grapevine.adminui.newssource"})
public class NewsSourceAdminUiController extends BaseAdminUiController {

    @Autowired
    private INewsSourceService newsSourceService;

    @Autowired
    private GrapevineClientModelMapper grapevineModelMapper;

    @ApiOperation(value = "Returns all news sources that the caller has access to")
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListNewsSourcesAction.class}
    )
    @GetMapping
    public Page<ClientNewsSource> getNewsSources(
            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of news sources", defaultValue = "0") int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of news sources returned in a page", defaultValue = "10") int count
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        getRoleService().decidePermissionAndThrowNotAuthorized(ListNewsSourcesAction.class, currentPerson);

        checkPageAndCountValues(page, count);

        return newsSourceService.findAllNotDeletedNewsSources(PageRequest.of(page, count))
                .map(grapevineModelMapper::toClientNewsSource);
    }

    @ApiOperation(value = "Returns a news source by id")
    @ApiExceptions({
            ClientExceptionType.NEWS_SOURCE_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListNewsSourcesAction.class}
    )
    @GetMapping(value = "/{newsSourceId}")
    public ClientNewsSource getNewsSourceById(
            @PathVariable String newsSourceId
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        getRoleService().decidePermissionAndThrowNotAuthorized(ListNewsSourcesAction.class, currentPerson);

        final NewsSource newsSource = newsSourceService.findNewsSourceById(newsSourceId);
        return grapevineModelMapper.toClientNewsSource(newsSource);
    }

}
