/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.exceptions.BadPageParameterException;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.*;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.UserGeneratedContentFlagClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.CommentNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.services.ICommentService;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostService;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.security.ExportUserGeneratedContentFlagsAction;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.security.ListUserGeneratedContentFlagsAction;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.results.UserGeneratedContentFlagSummary;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RestController
@RequestMapping("grapevine/adminui/flag")
@Api(tags = {"grapevine.adminui.flag"})
public class GrapevineUserGeneratedContentFlagAdminUiController extends BaseAdminUiController {

    @Autowired
    private ICommentService commentService;
    @Autowired
    private IPostService postService;
    @Autowired
    private IGroupService groupService;
    @Autowired
    private IUserGeneratedContentFlagService userGeneratedContentFlagService;
    @Autowired
    private GrapevineClientModelMapper grapevineClientModelMapper;
    @Autowired
    protected UserGeneratedContentFlagClientModelMapper userGeneratedContentFlagClientModelMapper;


    @ApiOperation(value = "Exports flagged posts and comments",
            notes = "Exports flagged posts and comments as CSV. If ignoreEmpty is true the pages might not be full.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ExportUserGeneratedContentFlagsAction.class})
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID
    })
    @GetMapping(value = "exportFlags",
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public
    @IgnoreArchitectureViolation(
            value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
            reason = "byte array is needed for file download"
    )
    @ResponseBody ResponseEntity<byte[]> exportFlaggedPostsAndComments(
            @RequestParam
            @ApiParam(value = "start time of the flags", required = true)
            long start,
            @RequestParam
            @ApiParam(value = "end time of the flags", required = true)
            long end,
            @RequestParam(required = false, defaultValue = "50")
            @ApiParam(value = "Number of entities on a page")
            int pageSize,
            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of posts", defaultValue = "0")
            int pageNumber,
            @RequestParam(required = false, defaultValue = "true")
            @ApiParam(value = "if true empty or blank text is ignored", defaultValue = "true")
            boolean ignoreEmpty
    ) {

        checkPageAndCountValues(pageNumber, pageSize);
        checkStartAndEndTime(start, end);

        final Person currentPerson = getCurrentPersonNotNull();
        getRoleService()
                .decidePermissionAndThrowNotAuthorized(ExportUserGeneratedContentFlagsAction.class, currentPerson);

        Set<String> expectedFlaggedPosts = Set.of(
                Gossip.class.getName(),
                Seeking.class.getName(),
                Offer.class.getName(),
                Suggestion.class.getName(),
                NewsItem.class.getName(),
                Happening.class.getName());
        Set<String> expectedFlaggedComments = Set.of(
                Comment.class.getName());
        HashSet<String> expectedFlaggedEntityTypes = new HashSet<>(expectedFlaggedPosts);
        expectedFlaggedEntityTypes.addAll(expectedFlaggedComments);

        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        Page<UserGeneratedContentFlagSummary> flagPage = userGeneratedContentFlagService
                .getFlagSummaryPerFlaggedEntity(expectedFlaggedEntityTypes, start, end, pageRequest);
        if (pageNumber > flagPage.getTotalPages()) {
            throw new BadPageParameterException("'pageNumber' {} out of range of {} pages", pageNumber,
                    flagPage.getTotalPages());
        }

        List<ClientFlagSummaryExportLine> exportLines = new ArrayList<>(flagPage.getNumberOfElements());

        for (UserGeneratedContentFlagSummary flagSummary : flagPage) {
            String entityType = flagSummary.getEntityType();
            String entityId = flagSummary.getEntityId();
            Post post = null;
            List<String> postAndCommentsText = new ArrayList<>();
            String entityText = "";
            if (expectedFlaggedPosts.contains(entityType)) {
                try {
                    post = postService.findPostByIdIncludingDeleted(entityId);
                    entityText = StringUtils.trim(post.getText());
                    postAndCommentsText.add(entityText);
                } catch (PostNotFoundException e) {
                    entityText = "";
                }
            }
            if (expectedFlaggedComments.contains(entityType)) {
                try {
                    Comment comment = commentService.findCommentByIdIncludingDeleted(entityId);
                    entityText = StringUtils.trim(comment.getText());
                    post = comment.getPost();
                    postAndCommentsText.add(post.getText());
                } catch (CommentNotFoundException e) {
                    entityText = "";
                }
            }
            if (ignoreEmpty) {
                if (StringUtils.isBlank(entityText)) {
                    continue;
                }
            }
            if (post == null) {
                continue;
            }

            int flaggedTextIndex = 0;
            List<Comment> comments = commentService.findAllByPostOrderByCreatedAsc(post);
            for (int i = 0; i < comments.size(); i++) {
                Comment comment = comments.get(i);
                postAndCommentsText.add(comment.getText());
                if (Objects.equals(comment.getId(), entityId)) {
                    flaggedTextIndex = i + 1;   // + 1 for post index
                }
            }

            String postAndComments;
            try {
                postAndComments = JsonMapping.defaultJsonWriter().forType(List.class)
                        .writeValueAsString(postAndCommentsText);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }

            exportLines.add(ClientFlagSummaryExportLine.builder()
                    .entityId(entityId)
                    .entityType(StringUtils.removeStart(entityType, "de.fhg.iese.dd.platform.datamanagement."))
                    .postId(post.getId())
                    .text(entityText)
                    .flaggedTextIndex(flaggedTextIndex)
                    .postAndCommentsText(postAndComments)
                    .numFlagsAccepted((int) flagSummary.getNumAccepted())
                    .numFlagsOpen((int) flagSummary.getNumOpen())
                    .numFlagsInProgress((int) flagSummary.getNumInProgress())
                    .numFlagsRejected((int) flagSummary.getNumRejected())
                    .build());
        }

        byte[] csv;
        try (StringWriter stringWriter = new StringWriter()) {
            //this should help excel to interpret UTF8 correctly by adding a UTF8 BOM
            stringWriter.write("\ufeff");
            HeaderColumnNameMappingStrategy<ClientFlagSummaryExportLine> mappingStrategy = new HeaderColumnNameMappingStrategy<>();
            mappingStrategy.setType(ClientFlagSummaryExportLine.class);
            StatefulBeanToCsv<ClientFlagSummaryExportLine> csvWriter =
                    new StatefulBeanToCsvBuilder<ClientFlagSummaryExportLine>(stringWriter)
                    .withApplyQuotesToAll(false)
                    .withSeparator(';')
                    .withQuotechar('"')
                            .withMappingStrategy(mappingStrategy)
                    .build();
            csvWriter.write(exportLines);

            csv = stringWriter.toString().getBytes(StandardCharsets.UTF_8);
        } catch (IOException | CsvDataTypeMismatchException | CsvRequiredFieldEmptyException e) {
            throw new RuntimeException(e);
        }

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd--HH-mm-ss");
        String startTime = getTimeService().toLocalTime(start).format(dateTimeFormatter);
        String endTime = getTimeService().toLocalTime(end).format(dateTimeFormatter);
        String fileName = String.format("flags__%s__%s__%d__%d-%d.csv", startTime, endTime, pageSize, pageNumber,
                flagPage.getTotalPages() - 1);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, "text/csv;charset=UTF-8")
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .body(csv);
    }

    @ApiOperation(value = "Returns details of a flagged post", notes = "The result can contain deleted entities")
    @ApiExceptions({
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_NOT_FOUND,
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE,
            ClientExceptionType.POST_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListUserGeneratedContentFlagsAction.class})
    @GetMapping("post/{flagId}")
    public ClientUserGeneratedContentFlagPost getPostFlagById(@PathVariable String flagId) {

        final Person currentPerson = getCurrentPersonNotNull();
        PermissionTenantRestricted getFlagsPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ListUserGeneratedContentFlagsAction.class, currentPerson);

        UserGeneratedContentFlag userGeneratedContentFlag =
                userGeneratedContentFlagService.findByIdWithStatusRecords(flagId);

        if (getFlagsPermission.isTenantDenied(userGeneratedContentFlag.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to list flag {}", flagId);
        }

        //this ensures that the type of the flag is actually a post (or subtype of it)
        userGeneratedContentFlagService.checkEntityTypeOrSubtype(userGeneratedContentFlag, Post.class);

        Post flaggedPost = postService.findPostByIdIncludingDeleted(userGeneratedContentFlag.getEntityId());

        //this also includes deleted
        List<Comment> comments = commentService.findAllByPostOrderByCreatedAsc(flaggedPost);

        return grapevineClientModelMapper.toClientUserGeneratedContentFlagPost(userGeneratedContentFlag,
                flaggedPost, comments);
    }

    @ApiOperation(value = "Returns details of a flagged comment", notes = "The result can contain deleted entities")
    @ApiExceptions({
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_NOT_FOUND,
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE,
            ClientExceptionType.COMMENT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListUserGeneratedContentFlagsAction.class})
    @GetMapping("comment/{flagId}")
    public ClientUserGeneratedContentFlagComment getCommentFlagById(@PathVariable String flagId) {

        final Person currentPerson = getCurrentPersonNotNull();
        PermissionTenantRestricted getFlagsPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ListUserGeneratedContentFlagsAction.class, currentPerson);

        UserGeneratedContentFlag userGeneratedContentFlag =
                userGeneratedContentFlagService.findByIdWithStatusRecords(flagId);

        if (getFlagsPermission.isTenantDenied(userGeneratedContentFlag.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to list flag {}", flagId);
        }

        //this ensures that the type of the flag is actually a comment (or subtype of it)
        userGeneratedContentFlagService.checkEntityTypeOrSubtype(userGeneratedContentFlag, Comment.class);

        //this also includes deleted
        Comment flaggedComment = commentService.findCommentByIdIncludingDeleted(userGeneratedContentFlag.getEntityId());

        //this also includes deleted
        List<Comment> comments = commentService.findAllByPostOrderByCreatedAsc(flaggedComment.getPost());

        return grapevineClientModelMapper.toClientUserGeneratedContentFlagComment(userGeneratedContentFlag,
                flaggedComment, comments);
    }

    @ApiOperation(value = "Returns details of a flagged group", notes = "The result can contain deleted entities")
    @ApiExceptions({
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_NOT_FOUND,
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE,
            ClientExceptionType.GROUP_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListUserGeneratedContentFlagsAction.class})
    @GetMapping("group/{flagId}")
    public ClientUserGeneratedContentFlagGroup getGroupFlagById(@PathVariable String flagId) {

        final Person currentPerson = getCurrentPersonNotNull();
        PermissionTenantRestricted getFlagsPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ListUserGeneratedContentFlagsAction.class, currentPerson);

        UserGeneratedContentFlag userGeneratedContentFlag =
                userGeneratedContentFlagService.findByIdWithStatusRecords(flagId);

        if (getFlagsPermission.isTenantDenied(userGeneratedContentFlag.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to list flag {}", flagId);
        }

        //this ensures that the type of the flag is actually a group (or subtype of it)
        userGeneratedContentFlagService.checkEntityTypeOrSubtype(userGeneratedContentFlag, Group.class);

        Group flaggedGroup = groupService.findGroupByIdIncludingDeleted(userGeneratedContentFlag.getEntityId());
        GroupMembershipStatus groupMembership =
                groupService.findGroupMembershipStatusForGroup(flaggedGroup, currentPerson);

        return grapevineClientModelMapper.toClientUserGeneratedContentFlagGroup(userGeneratedContentFlag,
                flaggedGroup, groupMembership, groupService.findGeoAreaIdsForGroup(flaggedGroup));
    }

    private void checkStartAndEndTime(long start, long end) {
        if (start < 0 || end < 0) {
            throw new BadRequestException("start and end must be greater or equal to 0");
        }
        if (start > end) {
            throw new BadRequestException("End time must be after start time");
        }
    }

}
