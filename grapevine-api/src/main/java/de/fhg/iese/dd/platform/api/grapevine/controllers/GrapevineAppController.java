/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChat;
import de.fhg.iese.dd.platform.api.communication.clientmodel.CommunicationClientModelMapper;
import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientConvenience;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientGrapevineOnboardingStatus;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientSuggestionCategory;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.services.IConvenienceService;
import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineChatService;
import de.fhg.iese.dd.platform.business.grapevine.services.ISuggestionCategoryService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BaseRuntimeException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.InternalServerErrorException;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IParallelismService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/grapevine")
@Api(tags = {"grapevine.app"})
class GrapevineAppController extends BaseController {

    @Autowired
    private IGrapevineChatService grapevineChatService;
    @Autowired
    private ISuggestionCategoryService suggestionCategoryService;
    @Autowired
    private IConvenienceService convenienceService;
    @Autowired
    private IParallelismService parallelismService;
    @Autowired
    private GrapevineClientModelMapper grapevineClientModelMapper;
    @Autowired
    private CommunicationClientModelMapper communicationClientModelMapper;

    @ApiOperation(value = "Returns the grapevine specific onboarding status of the current user",
            notes = "Combines fetching chats, suggestion categories and conveniences")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "onboardingStatus")
    public ClientGrapevineOnboardingStatus getOnboardingStatus() {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        final CompletableFuture<List<ClientChat>> ownChatsFuture =
                CompletableFuture.supplyAsync(() -> communicationClientModelMapper.toClientChats(
                                grapevineChatService.findOwnChats(person), person),
                        parallelismService.getBlockableExecutor());

        final CompletableFuture<List<ClientConvenience>> conveniencesFuture =
                CompletableFuture.supplyAsync(() ->
                                convenienceService.findAllAvailableConveniences(person, appVariant).stream()
                                        .map(grapevineClientModelMapper::toClientConvenience)
                                        .collect(Collectors.toList()),
                        parallelismService.getBlockableExecutor());

        final CompletableFuture<List<ClientSuggestionCategory>> suggestionCategoriesFuture =
                CompletableFuture.supplyAsync(() ->
                                suggestionCategoryService.findAllSuggestionCategoriesInDataInitOrder()
                                        .stream()
                                        .map(grapevineClientModelMapper::toClientSuggestionCategory)
                                        .collect(Collectors.toList()),
                        parallelismService.getBlockableExecutor());
        try {
            //since it is three completely independent sub-requests they can be processed in parallel
            return ClientGrapevineOnboardingStatus.builder()
                    .id(person.getId())
                    .ownChats(ownChatsFuture.get())
                    .conveniences(conveniencesFuture.get())
                    .suggestionCategories(suggestionCategoriesFuture.get())
                    .build();
        } catch (InterruptedException e) {
            throw new InternalServerErrorException(e);
        } catch (ExecutionException e) {
            final Throwable cause = e.getCause();
            if (cause instanceof BaseRuntimeException) {
                throw (BaseRuntimeException) cause;
            } else {
                throw new InternalServerErrorException(cause);
            }
        }
    }

}
