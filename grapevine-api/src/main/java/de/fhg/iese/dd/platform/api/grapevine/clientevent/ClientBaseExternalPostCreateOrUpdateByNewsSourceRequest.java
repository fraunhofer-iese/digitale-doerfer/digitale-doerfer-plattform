/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientevent;

import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientCroppedImageReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public abstract class ClientBaseExternalPostCreateOrUpdateByNewsSourceRequest extends ClientBasePostCreateRequest {

    @ApiModelProperty(hidden = true, value = "use geoAreaIds instead")
    private String geoAreaId;
    @ApiModelProperty
    private List<String> geoAreaIds;
    @NotBlank
    @ApiModelProperty(required = true)
    private String externalId;
    @NotBlank
    @ApiModelProperty(required = true)
    private String newsURL;
    @NotBlank
    @ApiModelProperty(required = true)
    private String authorName;
    private List<String> imageURLs;
    @Valid
    private List<ClientCroppedImageReference> croppedImages;
    private String organizationId;
    private String categories;
    @Builder.Default
    private boolean publishImmediately = true;
    @Builder.Default
    @ApiModelProperty(example = "false")
    private boolean commentsDisallowed = false;
    @ApiModelProperty(notes = "Can be null, can not be after desired unpublish time")
    private Long desiredPublishTime;
    @ApiModelProperty(notes = "Can be null, can not be before desired publish time")
    private Long desiredUnpublishTime;

}
