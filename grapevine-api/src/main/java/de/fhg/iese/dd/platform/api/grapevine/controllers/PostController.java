/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Adeline Silva Schäfer, Balthasar Weitzel, Johannes Schneider, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.*;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.SearchParameterTooLongException;
import de.fhg.iese.dd.platform.api.framework.exceptions.SearchParameterTooShortException;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientOrganizationTag;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPost;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostType;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.InvalidSortingCriterionException;
import de.fhg.iese.dd.platform.business.grapevine.services.*;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IPersistenceSupportService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationTag;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/grapevine")
@Api(tags = {"grapevine.post"})
class PostController extends BaseController {

    private static final Collection<PostType> ALL_EXTERNAL_POST_TYPES =
            List.of(PostType.NewsItem, PostType.Happening);

    @Autowired
    private IAppService appService;
    @Autowired
    private IPostService postService;
    @Autowired
    private IPostInteractionService postInteractionService;
    @Autowired
    private IHappeningService happeningService;
    @Autowired
    private IPersistenceSupportService persistenceSupportService;
    @Autowired
    private GrapevineClientModelMapper grapevineModelMapper;

    @ApiOperation(value = "Return all posts for the geo areas selected by the user",
            notes = """
                    Return all posts for the geo areas selected by the user, excluding posts from users with status 'PARALLEL_WORLD_INHABITANT'. The response is *fake paged*. The total number of elements can only be used to determine if there is a next page or not.

                    | sorting criteria | attribute used for filtering and sorting | returned posts are subtype of | direction | default start if null | default end if null |
                    | ---- | ---- | ---- | ---- | ---- | ---- |
                    | CREATED | created | Post | DESC | MIN | MAX |
                    | LAST_ACTIVITY | lastActivity | Post | DESC | MIN | MAX |
                    | HAPPENING_START | startTime | Happening | ASC | MIN | MAX |
                    """
    )
    @ApiExceptions({
            ClientExceptionType.APP_VARIANT_NOT_FOUND,
            ClientExceptionType.PAGE_PARAMETER_INVALID,
            ClientExceptionType.INVALID_SORTING_CRITERION
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping("/postBySelectedGeoAreas")
    public Page<ClientPost> getAllPostsByAppVariantAndSelectedGeoAreas(

            @RequestParam(required = false)
            @ApiParam(value = "Only return posts of this type")
                    ClientPostType type,

            @RequestParam(required = false)
            @ApiParam(value = "Start time from which the posts should be returned")
                    Long startTime,

            @RequestParam(required = false)
            @ApiParam(value = "End time from which the posts should be returned")
                    Long endTime,

            @RequestParam(required = false)
            @ApiParam(value = "If not empty only posts that have an organization and the organization has any of the required tags are returned")
            List<ClientOrganizationTag> requiredOrganizationTags,

            @RequestParam(required = false)
            @ApiParam(value = "If not empty only posts that have no organization or the organization has none of the forbidden tags are returned")
            List<ClientOrganizationTag> forbiddenOrganizationTags,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of posts",
                    defaultValue = "0") int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of posts returned in a page",
                    defaultValue = "10") int count,

            @RequestParam(required = false, defaultValue = "CREATED")
            @ApiParam(value = "sorting criteria, see table above for clarification",
                    defaultValue = "CREATED") PostSortCriteria sortBy) {

        checkPageAndCountValues(page, count);

        checkSortCriteria(sortBy, type);

        if (CollectionUtils.containsAny(requiredOrganizationTags, forbiddenOrganizationTags)) {
            throw new BadRequestException("requiredOrganizationTags may not contain forbiddenOrganizationTags");
        }

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        final Set<String> selectedGeoAreaIds = appService.getSelectedGeoAreaIds(appVariant, person);

        final PagedQuery pagedQuery = PagedQuery.builder()
                .pageNumber(page)
                .elementsPerPage(count)
                .sortCriteria(sortBy)
                .build();

        Set<OrganizationTag> requiredTags = grapevineModelMapper.toOrganizationTags(requiredOrganizationTags);
        Set<OrganizationTag> forbiddenTags = grapevineModelMapper.toOrganizationTags(forbiddenOrganizationTags);

        final Page<Post> posts = persistenceSupportService.sliceToFakePage(
                postService.findAllPosts(person, selectedGeoAreaIds,
                        grapevineModelMapper.toPostType(type), startTime, endTime,
                        requiredTags, forbiddenTags, pagedQuery));

        //these log details are used to understand why request are slow
        addAdditionalLogValue("countSelectedGeoAreas", selectedGeoAreaIds.size());
        addAdditionalLogValue("countPosts", posts.getNumberOfElements());
        addAdditionalLogValue("postType", type);
        addAdditionalLogValue("sortBy", sortBy.name());

        Set<Post> likedPosts = postInteractionService.filterLikedPosts(person, posts.getContent());
        // only required for happenings
        happeningService.setParticipated(person, posts.getContent());
        return new PageImpl<>(posts.stream()
                .map(post -> grapevineModelMapper.toClientPost(post, likedPosts.contains(post)))
                .collect(Collectors.toList()),
                posts.getPageable(),
                posts.getTotalElements());
    }

    @ApiOperation(value = "Return all posts of current user",
            notes = "Return all posts created by the current user. The response is paged.")
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID,
            ClientExceptionType.INVALID_SORTING_CRITERION,
            ClientExceptionType.UNSPECIFIED_BAD_REQUEST
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/post/own")
    public Page<ClientPost> getAllOwnPosts(
            @RequestParam(required = false)
            @ApiParam(value = "Only return posts of this type")
                    ClientPostType type,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of posts", defaultValue = "0")
                    int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of posts returned in a page", defaultValue = "10")
                    int count,

            @RequestParam(required = false, defaultValue = "CREATED")
            @ApiParam(value = "CREATED: newest posts are listed first \n" +
                    "LAST_ACTIVITY: posts with the most recent last activity are listed first.",
                    defaultValue = "CREATED")
                    PostSortCriteria sortBy) {

        checkPageAndCountValues(page, count);
        checkSortCriteria(sortBy, type);

        addAdditionalLogValue("postType", type);
        addAdditionalLogValue("sortBy", sortBy.name());

        final Person currentUser = getCurrentPersonNotNull();

        final PagedQuery pagedQuery = PagedQuery.builder()
                .pageNumber(page)
                .elementsPerPage(count)
                .sortCriteria(sortBy)
                .build();

        final Page<Post> posts;
        if (type == null) {
            posts = postService.findAllByCreator(currentUser, pagedQuery);
        } else {
            posts = postService.findAllByCreatorAndPostType(currentUser, grapevineModelMapper.toPostType(type),
                    pagedQuery);
        }
        // happenings get the field "participated" set for the current user
        happeningService.setParticipated(currentUser, posts.getContent());
        Set<Post> likedPosts = postInteractionService.filterLikedPosts(currentUser, posts.getContent());
        return posts.map(post -> grapevineModelMapper.toClientPost(post, likedPosts.contains(post)));
    }

    @ApiOperation(value = "Return all external posts for geo areas",
            notes = """
                    Return all external posts for the given geo areas. Be aware that the geo area ids are not checked, if they are invalid no post will match.The response is paged.

                    | sorting criterion | attribute used for filtering and sorting | returned posts are subtype of | direction |
                    | ---- | ---- | ---- | ---- |
                    | CREATED | created | ExternalPost | DESC |
                    | LAST_MODIFIED | lastModified | ExternalPost | DESC |
                    | HAPPENING_START | startTime | Happening | ASC |
                    """
    )
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID,
            ClientExceptionType.INVALID_SORTING_CRITERION,
    })
    @ApiException(value = ClientExceptionType.UNSPECIFIED_BAD_REQUEST,
            reason = "Not only external post types are requested")
    @ApiException(value = ClientExceptionType.UNSPECIFIED_BAD_REQUEST, reason = "No geo area ids are given")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping("/post/externalByGeoAreas")
    public Page<ClientPost> getAllExternalPostsByGeoAreas(

            @RequestParam
            @ApiParam(value = "Only return posts of these geo areas, " +
                    "empty = exception")
                    Set<String> geoAreaIds,

            @RequestParam(required = false)
            @ApiParam(value = "Only return posts of these types, allowed are NEWS_ITEM and HAPPENING, " +
                    "empty = all allowed types")
                    Set<ClientPostType> postTypes,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of posts",
                    defaultValue = "0") int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of posts returned in a page",
                    defaultValue = "10") int count,

            @RequestParam(required = false, defaultValue = "CREATED")
            @ApiParam(value = "sorting criterion, see table above for clarification",
                    defaultValue = "CREATED") PostSortCriteria sortBy) {

        checkPageAndCountValues(page, count);
        Collection<PostType> types;
        if (CollectionUtils.isEmpty(postTypes)) {
            types = ALL_EXTERNAL_POST_TYPES;
        } else {
            types = new ArrayList<>(postTypes.size());
            for (ClientPostType postType : postTypes) {
                checkIsExternal(postType);
                checkSortCriteria(sortBy, postType);
                types.add(postType.toPostType());
            }
        }

        if (CollectionUtils.isEmpty(geoAreaIds)) {
            throw new BadRequestException("No geo areas selected");
        }

        final PagedQuery pagedQuery = PagedQuery.builder()
                .pageNumber(page)
                .elementsPerPage(count)
                .sortCriteria(sortBy)
                .build();

        final Page<? extends ExternalPost> posts = postService.findAllExternalPosts(geoAreaIds, types, pagedQuery);

        //these log details are used to understand why request are slow
        addAdditionalLogValue("countSelectedGeoAreas", geoAreaIds.size());
        addAdditionalLogValue("countPosts", posts.getNumberOfElements());
        addAdditionalLogValue("postTypes", postTypes);
        addAdditionalLogValue("sortBy", sortBy.name());

        Stream<? extends ExternalPost> postStream;
        if (posts.getNumberOfElements() > 12) {
            //if there are less elements the overhead for parallel is too much
            postStream = posts.stream().parallel();
        } else {
            postStream = posts.stream();
        }
        return new PageImpl<>(postStream
                .map(post -> grapevineModelMapper.toClientPost(post, null))
                .collect(Collectors.toList()),
                posts.getPageable(),
                posts.getTotalElements());
    }

    @ApiOperation(value = "Return all external posts for a given news source",
            notes = """
                    Return all external posts for a given news source.The response is paged.

                    | sorting criterion | attribute used for filtering and sorting | returned posts are subtype of | direction |
                    | ---- | ---- | ---- | ---- |
                    | CREATED | created | ExternalPost | DESC |
                    | LAST_MODIFIED | lastModified | ExternalPost | DESC |
                    | HAPPENING_START | startTime | Happening | ASC |
                    """
    )
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID,
            ClientExceptionType.INVALID_SORTING_CRITERION,
    })
    @ApiException(value = ClientExceptionType.UNSPECIFIED_BAD_REQUEST,
            reason = "Not only external post types are requested")
    @ApiException(value = ClientExceptionType.UNSPECIFIED_BAD_REQUEST, reason = "No news source id is given")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping("/post/externalByNewsSource")
    public Page<ClientPost> getAllExternalPostsByNewsSource(

            @RequestParam
            @ApiParam(value = "Only return external posts of this news source, " +
                    "empty = exception")
                    String newsSourceId,

            @RequestParam(required = false)
            @ApiParam(value = "Only return posts of these types, allowed are NEWS_ITEM and HAPPENING, " +
                    "empty = all allowed types")
                    Set<ClientPostType> postTypes,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of posts",
                    defaultValue = "0") int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of posts returned in a page",
                    defaultValue = "10") int count,

            @RequestParam(required = false, defaultValue = "CREATED")
            @ApiParam(value = "sorting criterion, see table above for clarification",
                    defaultValue = "CREATED") PostSortCriteria sortBy) {

        checkPageAndCountValues(page, count);
        Collection<PostType> types;
        if (CollectionUtils.isEmpty(postTypes)) {
            types = ALL_EXTERNAL_POST_TYPES;
        } else {
            types = new ArrayList<>(postTypes.size());
            for (ClientPostType postType : postTypes) {
                checkIsExternal(postType);
                checkSortCriteria(sortBy, postType);
                types.add(postType.toPostType());
            }
        }

        if (StringUtils.isEmpty(newsSourceId)) {
            throw new BadRequestException("No news source selected");
        }

        final PagedQuery pagedQuery = PagedQuery.builder()
                .pageNumber(page)
                .elementsPerPage(count)
                .sortCriteria(sortBy)
                .build();

        final Page<? extends ExternalPost> posts = postService.findAllExternalPostsByNewsSource(newsSourceId, types, pagedQuery);

        //these log details are used to understand why request are slow
        addAdditionalLogValue("countPosts", posts.getNumberOfElements());
        addAdditionalLogValue("postTypes", postTypes);
        addAdditionalLogValue("sortBy", sortBy.name());

        Stream<? extends ExternalPost> postStream;
        if (posts.getNumberOfElements() > 12) {
            //if there are less elements the overhead for parallel is too much
            postStream = posts.stream().parallel();
        } else {
            postStream = posts.stream();
        }
        return new PageImpl<>(postStream
                .map(post -> grapevineModelMapper.toClientPost(post, null))
                .collect(Collectors.toList()),
                posts.getPageable(),
                posts.getTotalElements());
    }

    @ApiOperation(value = "Returns a specific post",
            notes = "If the post is inside a group, the visibility settings of the group are considered.")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping("/post/{postId}")
    public ClientPost getPostById(
            @PathVariable String postId) {

        final Person currentUser = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        Post post = postService.findByIdFilteredByHiddenForOtherHomeAreasAndGroupContentVisibility(
                postId, currentUser, appVariant);
        // only required when post is a happening
        happeningService.setParticipated(currentUser, Collections.singleton(post));
        return grapevineModelMapper.toClientPost(post, postInteractionService.isPostLiked(currentUser, post));
    }

    @ApiOperation(value = "Return all posts in which the current user participated.",
            notes = "Returns all posts commented but not created by the current user. The response is paged.")
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/activity")
    public Page<ClientPost> getAllPostsWithOwnActivities(
            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of posts",
                    defaultValue = "0") int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of posts returned in a page",
                    defaultValue = "10") int count) {

        checkPageAndCountValues(page, count);

        Person currentUser = getCurrentPersonNotNull();
        final PagedQuery pagedQuery = PagedQuery.builder()
                .pageNumber(page)
                .elementsPerPage(count)
                .sortCriteria(PostSortCriteria.LAST_ACTIVITY)
                .build();
        Page<Post> posts = postService.findAllByActivity(currentUser, pagedQuery);
        Set<Post> likedPosts = postInteractionService.filterLikedPosts(currentUser, posts.getContent());
        // happenings get the field "participated" set for the current user
        happeningService.setParticipated(currentUser, posts.getContent());
        addAdditionalLogValue("pageSize", count);
        addAdditionalLogValue("countPosts", posts.getNumberOfElements());
        return posts.map(post -> grapevineModelMapper.toClientPost(post, likedPosts.contains(post)));
    }

    @ApiOperation(value = "Search posts with specific text.",
            notes = "Returns all posts with a specific text. " +
                    "This text can be in the post text, in the address or in one of the comments. " +
                    "The response is paged.")
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID,
            ClientExceptionType.SEARCH_PARAMETER_TOO_SHORT,
            ClientExceptionType.SEARCH_NOT_AVAILABLE
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping("/search")
    public Page<ClientPost> searchPosts(
            @RequestParam
            @ApiParam(value = "Search term, can consist of multiple words, minimum length 3, maximum 100",
                    required = true)
                    String search,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of posts",
                    defaultValue = "0") int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of posts returned in a page",
                    defaultValue = "10") int count) {

        String trimmedSearch = StringUtils.trim(search);
        if (StringUtils.length(trimmedSearch) < 3) {
            throw new SearchParameterTooShortException("search", 3);
        }
        if (StringUtils.length(trimmedSearch) > 100) {
            throw new SearchParameterTooLongException("search", 100);
        }
        checkPageAndCountValues(page, count);
        final AppVariant appVariant = getAppVariantNotNull();

        final Person currentUser = getCurrentPersonNotNull();
        final PageRequest pageRequest = PageRequest.of(page, count);
        final Set<String> selectedGeoAreaIds = appService.getSelectedGeoAreaIds(appVariant, currentUser);
        Page<Post> posts = postService.searchPosts(search, currentUser, selectedGeoAreaIds, pageRequest);
        Set<Post> likedPosts = postInteractionService.filterLikedPosts(currentUser, posts.getContent());
        // happenings get the field "participated" set for the current user
        happeningService.setParticipated(currentUser, posts.getContent());
        addAdditionalLogValue("pageSize", count);
        addAdditionalLogValue("countPosts", posts.getNumberOfElements());
        addAdditionalLogValue("searchLength", StringUtils.length(search));
        addAdditionalLogValue("totalResultSize", posts.getTotalElements());
        return posts.map(post -> grapevineModelMapper.toClientPost(post, likedPosts.contains(post)));
    }

    private void checkIsExternal(final ClientPostType postType) {
        if ((postType != null) && !ExternalPost.class.isAssignableFrom(postType.toPostClass())) {
            throw new BadRequestException("Only external post types are allowed").withDetail(postType.name());
        }
    }

    private void checkSortCriteria(final PostSortCriteria sortBy, final ClientPostType postType) {
        final Class<? extends Post> typeClass = (postType == null)
                ? Post.class
                : postType.toPostClass();
        if (!sortBy.isAvailableFor(typeClass)) {
            throw new InvalidSortingCriterionException("sortBy {} is not available for type {}", sortBy, postType);
        }
    }

}
