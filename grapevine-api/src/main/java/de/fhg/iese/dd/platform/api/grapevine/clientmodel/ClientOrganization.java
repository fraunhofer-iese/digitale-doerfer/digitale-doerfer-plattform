/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientmodel;

import com.fasterxml.jackson.annotation.JsonRawValue;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddress;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItem;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientOrganization extends ClientBaseEntity {

    @ApiModelProperty(required = true)
    private String name;

    @ApiModelProperty(required = true)
    private String locationDescription;

    @ApiModelProperty(required = true)
    private String description;

    private ClientMediaItem overviewImage;

    private ClientMediaItem logoImage;

    private List<ClientMediaItem> detailImages;

    private String url;

    private String phone;

    private String emailAddress;

    private ClientAddress address;

    @JsonRawValue
    @ApiModelProperty(dataType = "[Lde.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientOrganizationFact")
    private String facts;

    private List<ClientOrganizationPerson> persons;

    private List<ClientOrganizationTag> tags;

    @ApiModelProperty(notes = "The geo area ids are only the root areas of all available geo areas. " +
            "To get all available ids add the children of these geo areas.")
    private List<String> geoAreaIds;

}
