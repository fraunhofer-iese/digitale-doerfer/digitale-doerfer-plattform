/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Balthasar Weitzel, Dominik Schnier, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientevent;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.framework.clientevent.ClientMinimizableEvent;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientOrganizationTag;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPost;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientPostCreateConfirmation extends ClientBaseEvent implements ClientMinimizableEvent {

    @Getter
    @AllArgsConstructor
    private static class Minimized extends ClientBaseEvent {

        private final String postId;
        @JsonProperty("post")
        private final Map<String, ClientPostType> type;
        private final List<ClientOrganizationTag> organizationTags;

    }

    public String getPostId() {
        return post.getId();
    }

    @NotNull
    private ClientPost post;

    /**
     * Optional list of organization tags. Is only used for push events. If the post has no relation to an organization it will be null
     */
    private List<ClientOrganizationTag> organizationTags;

    @Override
    public Minimized toMinimizedEvent() {
        return new Minimized(getPostId(), Collections.singletonMap("type", post.getType()), organizationTags);
    }

}
