/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2021 Johannes Schneider, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientmodel;

import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Generic base entity for post as presented to the client. Uses this internal delegation structure because of client db
 * constraints.
 */
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)//for toObject (Jackson) in unit tests
public class ClientPost extends ClientBaseEntity {

    private ClientPostType type;

    private ClientGossip gossip;
    private ClientNewsItem newsItem;
    private ClientSuggestion suggestion;
    private ClientSeeking seeking;
    private ClientOffer offer;
    private ClientHappening happening;
    private ClientSpecialPost specialPost;

    ClientPost(ClientGossip gossip) {
        this.gossip = gossip;
        initId(gossip);
        this.type = ClientPostType.GOSSIP;
    }

    ClientPost(ClientNewsItem newsItem) {
        this.newsItem = newsItem;
        initId(newsItem);
        this.type = ClientPostType.NEWS_ITEM;
    }

    ClientPost(ClientSuggestion suggestion) {
        this.suggestion = suggestion;
        initId(suggestion);
        this.type = ClientPostType.SUGGESTION;
    }

    ClientPost(ClientSeeking seeking) {
        this.seeking = seeking;
        initId(seeking);
        this.type = ClientPostType.SEEKING;
    }

    ClientPost(ClientOffer offer) {
        this.offer = offer;
        initId(offer);
        this.type = ClientPostType.OFFER;
    }

    ClientPost(ClientHappening happening) {
        this.happening = happening;
        initId(happening);
        this.type = ClientPostType.HAPPENING;
    }

    ClientPost(ClientSpecialPost specialPost) {
        this.specialPost = specialPost;
        initId(specialPost);
        this.type = ClientPostType.SPECIAL_POST;
    }

    private void initId(ClientBaseEntity entity) {
        if (entity != null) {
            setId(entity.getId());
        }
    }

}
