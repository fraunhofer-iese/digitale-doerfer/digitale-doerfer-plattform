/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientevent.happening;

import javax.validation.constraints.Positive;

import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientBasePostChangeByPersonRequest;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientLocationDefinition;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientHappeningChangeRequest extends ClientBasePostChangeByPersonRequest {

    @Positive(message = "Time must not be negative or zero!")
    private Long startTime;
    private Long endTime;
    private Boolean allDayEvent;
    private ClientLocationDefinition venueLocationDefinition;

}
