/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientmodel;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;

/**
 * Customization of the client mapping, used to adjust fields of client entities that can not be derived directly from the data entities.
 */
public interface IClientPostMappingCustomization<C extends ClientPostBaseEntity, P extends Post> {

    Class<C> getRelevantClientPostClass();

    /**
     * Applied after all other mappings are done. The order of customizations can be changed by applying @Order.
     *
     * @param clientEntity the client entity to adjust
     * @param post         the original entity
     * @return the modified client entity (usually just the same entity)
     */
    C customizeMapping(C clientEntity, P post);

}
