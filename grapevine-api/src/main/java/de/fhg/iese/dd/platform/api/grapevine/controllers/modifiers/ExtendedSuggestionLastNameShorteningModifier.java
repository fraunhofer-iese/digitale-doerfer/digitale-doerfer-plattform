/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers.modifiers;

import static de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier.shorten;

import java.util.Set;

import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableSet;

import de.fhg.iese.dd.platform.api.framework.modifiers.IContentFilterSettings;
import de.fhg.iese.dd.platform.api.framework.modifiers.IOutgoingClientEntityModifier;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientSuggestionExtended;

@Component
class ExtendedSuggestionLastNameShorteningModifier implements IOutgoingClientEntityModifier<ClientSuggestionExtended> {

    @Override
    public Class<ClientSuggestionExtended> getType() {
        return ClientSuggestionExtended.class;
    }

    //Currently the push messages are not sent, but we want to be sure that they do not accidentally include the full name
    @Override
    public Set<String> getPushEventPackagePrefixes() {
        return GrapevineLastNameShorteningModifier.PUSH_EVENT_PACKAGE_PREFIX;
    }

    @Override
    public Set<String> getPathPatterns() {
        return ImmutableSet.of("/grapevine/suggestion/*",
                "/grapevine/event/suggestionStatusChangeRequest",
                "/grapevine/event/suggestionCategoryChangeRequest",
                "/grapevine/event/suggestionWorkerAddRequest",
                "/grapevine/event/suggestionWorkerRemoveRequest");
    }

    @Override
    public boolean modify(ClientSuggestionExtended clientSuggestionExtended,
            IContentFilterSettings contentFilterSettings) {

        if (clientSuggestionExtended.getCreator() != null) {
            clientSuggestionExtended.getCreator().setLastName(
                    shorten(clientSuggestionExtended.getCreator().getLastName()));
        }
        return false; //do not modify attributes, particularly not the person references in the worker mappings
    }

}
