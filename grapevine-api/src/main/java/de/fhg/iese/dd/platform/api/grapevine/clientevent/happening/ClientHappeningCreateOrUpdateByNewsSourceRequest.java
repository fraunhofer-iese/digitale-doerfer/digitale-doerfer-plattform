/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Adeline Silva Schäfer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientevent.happening;

import javax.validation.constraints.Positive;

import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientBaseExternalPostCreateOrUpdateByNewsSourceRequest;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientLocationDefinition;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientHappeningCreateOrUpdateByNewsSourceRequest extends ClientBaseExternalPostCreateOrUpdateByNewsSourceRequest {

    private String organizer;
    private String venue;
    @Positive(message = "Time must not be negative or zero!")
    private long startTime;
    private long endTime;
    private boolean allDayEvent;

    @Deprecated
    @ApiModelProperty(notes = "use customLocationDefinition instead", hidden = true)
    public ClientLocationDefinition getVenueLocationDefinition() {
        return getCustomLocationDefinition();
    }

    @Deprecated
    @ApiModelProperty(notes = "use customLocationDefinition instead", hidden = true)
    public void setVenueLocationDefinition(
            ClientLocationDefinition venueLocationDefinition) {
        setCustomLocationDefinition(venueLocationDefinition);
    }

}
