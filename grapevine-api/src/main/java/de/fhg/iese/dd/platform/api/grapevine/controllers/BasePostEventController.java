/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidEventAttributeException;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientBasePostChangeByPersonRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientBasePostCreateByPersonRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.AddressClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.api.shared.services.IClientDocumentItemService;
import de.fhg.iese.dd.platform.api.shared.services.IClientMediaItemService;
import de.fhg.iese.dd.platform.business.grapevine.events.BasePostChangeRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.BasePostCreateRequest;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostInteractionService;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.PostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IDocumentItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

public abstract class BasePostEventController extends BaseController {

    @Autowired
    protected GrapevineConfig grapevineConfig;
    @Autowired
    protected IPostService postService;
    @Autowired
    protected IPostInteractionService postInteractionService;
    @Autowired
    protected IAddressService addressService;
    @Autowired
    protected IMediaItemService mediaItemService;
    @Autowired
    protected IDocumentItemService documentItemService;
    @Autowired
    protected IClientMediaItemService clientMediaItemService;
    @Autowired
    protected IClientDocumentItemService clientDocumentItemService;
    @Autowired
    protected IFeatureService featureService;
    @Autowired
    protected GrapevineClientModelMapper grapevineModelMapper;
    @Autowired
    protected AddressClientModelMapper addressClientModelMapper;

    protected void checkMaxTextLength(Class<? extends PostTypeFeature<?>> featureClass, Person currentUser,
            AppVariant appVariant, Supplier<String> text) throws InvalidEventAttributeException {

        @SuppressWarnings({"rawtypes", "RedundantSuppression"})
        //rawtypes is required to prevent the compiler at jenkins to generate warnings
        int maxTextLength = featureService.getFeatureOptional(featureClass, FeatureTarget.of(currentUser, appVariant))
                .map(PostTypeFeature::getMaxNumberCharsPerPost)
                .orElse(100);
        final int length = StringUtils.length(text.get());
        if (length > maxTextLength) {
            throw new InvalidEventAttributeException("Text is too long: {} of {} characters", length, maxTextLength)
                    .withDetail("text");
        }
    }

    protected GPSLocation customLocationOrHomeAreaCenter(ClientGPSLocation customLocation, Person creator) {
        if (customLocation != null) {
            GPSLocation resultLocation = new GPSLocation(customLocation.getLatitude(), customLocation.getLongitude());
            if (resultLocation.isValid()) {
                return resultLocation;
            }
        }
        if (creator != null && creator.getHomeArea() != null) {
            return creator.getHomeArea().getCenter();
        }
        return new GPSLocation(0.0, 0.0);
    }

    protected void checkHappeningTimes(long startTime, long endTime, boolean allDayEvent) {
        if (!allDayEvent) {
            if (endTime < startTime) {
                throw new InvalidEventAttributeException("Attribute 'endTime' must be greater than 'startTime'");
            }
            if (endTime == startTime) {
                throw new InvalidEventAttributeException("Attribute 'endTime' cannot be equal to 'startTime'");
            }
        }
        if (startTime == 0) {
            throw new InvalidEventAttributeException("Attribute 'startTime' is not set").withDetail("startTime");
        }
    }

    protected void setPostCreateByPersonRequestValues(
            BasePostCreateRequest.BasePostCreateRequestBuilder<?, ?> requestBuilder,
            Person creator, AppVariant appVariant, ClientBasePostCreateByPersonRequest clientRequest) {

        GPSLocation createdLocation = customLocationOrHomeAreaCenter(clientRequest.getCreatedLocation(), creator);
        Address customAddress = addressService.findOrCreateAddress(
                addressClientModelMapper.toLocationDefinition(clientRequest.getCustomLocationDefinition()),
                false);
        requestBuilder
                .creator(creator)
                .appVariant(appVariant)
                .text(clientRequest.getText())
                .hiddenForOtherHomeAreas(clientRequest.isHiddenForOtherHomeAreas())
                .createdLocation(createdLocation)
                .customAddress(customAddress)
                .temporaryMediaItemsToAdd(mediaItemService
                        .findTemporaryItemsById(creator, clientRequest.getTemporaryMediaItemIds()))
                .temporaryDocumentItemsToAdd(documentItemService.findTemporaryItemsByIdUnordered(creator,
                        clientRequest.getTemporaryDocumentItemIds()));
    }

    protected <P extends Post> void setPostChangeRequestByPersonValues(
            BasePostChangeRequest.BasePostChangeRequestBuilder<P, ?, ?> requestBuilder,
            P post, Person creator, AppVariant appVariant, ClientBasePostChangeByPersonRequest clientRequest) {

        Address customAddress;
        if (clientRequest.getCustomLocationDefinition() != null && !clientRequest.isRemoveCustomAddress()) {
            customAddress = addressService.findOrCreateAddress(
                    addressClientModelMapper.toLocationDefinition(clientRequest.getCustomLocationDefinition()), false);
        } else {
            customAddress = post.getCustomAddress();
        }
        requestBuilder
                .post(post)
                .creator(creator)
                .appVariant(appVariant)
                .text(clientRequest.getText())
                .customAddress(customAddress)
                .removeCustomAddress(clientRequest.isRemoveCustomAddress())
                .useEmptyFields(clientRequest.isUseEmptyFields())
                .imageHolders(clientMediaItemService.toItemPlaceHolders(
                        clientRequest.getMediaItemPlaceHolders(),
                        post.getImages(), creator))
                .documentHolders(clientDocumentItemService.toItemPlaceHolders(
                        clientRequest.getDocumentItemPlaceHolders(),
                        post.getAttachments(), creator));
    }

}
