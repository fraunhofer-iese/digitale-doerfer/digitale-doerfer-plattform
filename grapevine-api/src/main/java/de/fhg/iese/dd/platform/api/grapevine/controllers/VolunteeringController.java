/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientVolunteering;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.OrganizationNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.services.IOrganizationService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationTag;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/grapevine/volunteering")
@Api(tags = {"grapevine.volunteering"})
public class VolunteeringController extends BaseController {

    @Autowired
    private IOrganizationService organizationService;
    @Autowired
    private GrapevineClientModelMapper grapevineClientModelMapper;

    @ApiOperation(value = "Return all available volunteerings",
            notes = "All volunteerings where one of selected geo areas of the user is in the geo areas of the volunteering.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping
    public List<ClientVolunteering> getAllAvailableVolunteerings() {

        Person person = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();

        return organizationService.findAllAvailableOrganizations(person, appVariant,
                        EnumSet.of(OrganizationTag.VOLUNTEERING)).stream()
                .map(organization -> grapevineClientModelMapper.toClientVolunteering(organization,
                        organizationService.getRelatedGeoAreaIds(organization)))
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Return a volunteering by id",
            notes = "No access restriction takes place, all volunteerings can be queried.")
    @ApiExceptions({ClientExceptionType.ORGANIZATION_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/{volunteeringId}")
    public ClientVolunteering getVolunteeringById(@PathVariable String volunteeringId) {

        //used to enforce authentication
        getCurrentPersonNotNull();
        getAppVariantNotNull();

        Organization organization = organizationService.findOrganizationById(volunteeringId);
        if (!organization.getTags().hasValue(OrganizationTag.VOLUNTEERING)) {
            throw new OrganizationNotFoundException(volunteeringId);
        }
        return grapevineClientModelMapper.toClientVolunteering(organization,
                organizationService.getRelatedGeoAreaIds(organization));
    }

}
