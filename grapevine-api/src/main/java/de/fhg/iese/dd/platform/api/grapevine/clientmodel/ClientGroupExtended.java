/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2021 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientmodel;

import java.util.List;

import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoArea;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
public class ClientGroupExtended extends ClientBaseGroup {

    @ApiModelProperty(notes = "included is relative to the selected geo areas of the user")
    private List<ClientGeoArea> includedGeoAreas;
    @ApiModelProperty(notes = "excluded is relative to the available geo areas of the app variant")
    private List<ClientGeoArea> excludedGeoAreas;
    private String tenantId;

    @Deprecated
    @ApiModelProperty(value = "use includedGeoAreas instead", hidden = true)
    public List<ClientGeoArea> getGeoAreas() {
        return includedGeoAreas;
    }

    @Deprecated
    @ApiModelProperty(value = "use includedGeoAreas instead", hidden = true)
    public void setGeoAreas(List<ClientGeoArea> geoAreas) {
        this.includedGeoAreas = geoAreas;
    }

}
