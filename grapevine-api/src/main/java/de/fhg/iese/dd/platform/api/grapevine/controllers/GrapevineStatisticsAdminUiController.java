/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientGrapevineGeoAreaInfo;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.business.grapevine.security.ListGrapevineGeoAreaInfoAction;
import de.fhg.iese.dd.platform.business.grapevine.statistics.GrapevineGeoAreaInfo;
import de.fhg.iese.dd.platform.business.grapevine.statistics.legacy.IGrapevineStatisticsService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/grapevine/adminui/statistics")
@Api(tags = {"grapevine.adminui.statistics"})
class GrapevineStatisticsAdminUiController extends BaseAdminUiController {

    @Autowired
    private IGeoAreaService geoAreaService;

    @Autowired
    private IGrapevineStatisticsService grapevineStatisticsService;

    @ApiOperation(value = "Gets geo area specific statistics",
            notes = """
                    Returns a brief statistic of the geo area:
                    - number of non deleted persons whose home geo area is equal to the given geo area
                    - number of non deleted posts created in the given geo area
                    - number of non deleted comments created on these posts""")
    @ApiExceptions({
            ClientExceptionType.GEO_AREA_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = ListGrapevineGeoAreaInfoAction.class)
    @GetMapping(value = "/geoArea/{geoAreaId}")
    public ClientGrapevineGeoAreaInfo getGrapevineStatsForGeoArea(
            @PathVariable String geoAreaId
    ) {

        PermissionTenantRestricted listGrapevineGeoAreaInfoPermission =
                getRoleService().decidePermissionAndThrowNotAuthorized(ListGrapevineGeoAreaInfoAction.class,
                        getCurrentPersonNotNull());

        final GeoArea geoArea = geoAreaService.findGeoAreaById(geoAreaId);

        if (listGrapevineGeoAreaInfoPermission.isTenantDenied(geoArea.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to list grapevine info for geo area '{}'",
                    geoAreaId);
        }

        final GrapevineGeoAreaInfo grapevineGeoAreaInfo = grapevineStatisticsService.getGrapevineGeoAreaInfo(geoArea);
        return ClientGrapevineGeoAreaInfo.builder()
                .id(geoAreaId)
                .geoAreaId(grapevineGeoAreaInfo.getGeoArea().getId())
                .personCount(grapevineGeoAreaInfo.getPersonCount())
                .postCount(grapevineGeoAreaInfo.getPostCount())
                .commentCount(grapevineGeoAreaInfo.getCommentCount())
                .build();
    }

}
