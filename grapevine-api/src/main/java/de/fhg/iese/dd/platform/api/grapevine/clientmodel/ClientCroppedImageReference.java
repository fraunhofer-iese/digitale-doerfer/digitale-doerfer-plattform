/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Johannes Eveslage, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientmodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientAbsoluteImageCropDefinition;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientRelativeImageCropDefinition;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.annotation.Nullable;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientCroppedImageReference {

    @NotBlank
    private String url;
    @Valid
    @Nullable
    private ClientRelativeImageCropDefinition relativeCropDefinition;
    @Valid
    @Nullable
    private ClientAbsoluteImageCropDefinition absoluteCropDefinition;

    // legacy attributes
    @ApiModelProperty(hidden = true,
            value = "x coordinate of the top left corner of the cropped image, must be positive or zero")
    private int x;
    @ApiModelProperty(hidden = true,
            value = "y coordinate of the top left corner of the cropped image, must be positive or zero")
    private int y;
    @ApiModelProperty(hidden = true, value = "width of the cropped image, must be positive")
    private int width;
    @ApiModelProperty(hidden = true, value = "height of the cropped image, must be positive")
    private int height;

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    @Nullable
    @Valid
    //this is validated to ensure the legacy attributes are validated
    public ClientAbsoluteImageCropDefinition getLegacyAbsoluteImageCropDefinition() {
        if (x == 0 && y == 0 && width == 0 && height == 0) {
            return null;
        }
        return ClientAbsoluteImageCropDefinition.builder()
                .absoluteOffsetX(x)
                .absoluteOffsetY(y)
                .absoluteHeight(height)
                .absoluteWidth(width)
                .build();
    }

}
