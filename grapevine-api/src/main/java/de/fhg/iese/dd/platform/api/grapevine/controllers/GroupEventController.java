/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import java.util.Set;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiException;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupChangeRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupDeleteConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupDeleteRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupFlagRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupJoinConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupJoinRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupLeaveConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupLeaveRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupPendingJoinAcceptConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupPendingJoinAcceptRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupPendingJoinDenyConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupPendingJoinDenyRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.services.IClientMediaItemService;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagResponse;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupChangeRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupCreateRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupDeleteRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupFlagRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupJoinConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupJoinRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupLeaveConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupLeaveRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinAcceptConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinAcceptRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinDenyConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinDenyRequest;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagConfirmation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.GroupCreationFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembership;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.GroupMembershipAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/grapevine/group/event")
@Api(tags = {"grapevine.group.events"})
public class GroupEventController extends BaseController {

    @Autowired
    private IGroupService groupService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private IClientMediaItemService clientMediaItemService;
    @Autowired
    private GrapevineClientModelMapper grapevineModelMapper;
    @Autowired
    private IAuthorizationService authorizationService;

    @ApiOperation(value = "Create group",
            notes = "Every person can create a group. The creator is the first admin of the group.\n" +
                    "Only possible if the feature `de.fhg.iese.dd.dorffunk.group.creation` is enabled for " +
                    "the requesting app variant and user.")
    @ApiExceptions({
            ClientExceptionType.GEO_AREA_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND,
    })
    @ApiException(value = ClientExceptionType.GROUP_GEO_AREAS_INVALID,
            reason = "Excluded geo areas can not contain any of the included geo areas, " +
                    "main geo area has to be in the included geo areas")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("/groupCreateRequest")
    public ClientGroupCreateConfirmation onGroupCreateRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
            ClientGroupCreateRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();
        getFeatureService().checkFeatureEnabled(GroupCreationFeature.class,
                FeatureTarget.of(currentPerson, appVariant));

        final Tenant tenant = currentPerson.getTenant();

        final GeoArea mainGeoArea = geoAreaService.findGeoAreaById(request.getMainGeoAreaId());
        final Set<GeoArea> includedGeoAreas = geoAreaService.findAllById(request.getIncludedGeoAreaIds());
        final Set<GeoArea> excludedGeoAreas = geoAreaService.findAllById(request.getExcludedGeoAreaIds());
        groupService.checkGeoAreaConsistency(mainGeoArea, includedGeoAreas, excludedGeoAreas);

        final GroupCreateRequest groupCreateRequest = GroupCreateRequest.builder()
                .name(StringUtils.trim(request.getName()))
                .shortName(StringUtils.trim(request.getShortName()))
                .description(StringUtils.defaultIfBlank(StringUtils.trim(request.getDescription()), null))
                .tenant(tenant)
                .creator(currentPerson)
                .groupVisibility(request.getGroupVisibility())
                .groupAccessibility(request.getGroupAccessibility())
                .groupContentVisibility(request.getGroupContentVisibility())
                .mainGeoArea(mainGeoArea)
                .includedGeoAreas(includedGeoAreas)
                .excludedGeoAreas(excludedGeoAreas)
                .build();
        if (request.getLogoTemporaryMediaItemId() != null) {
            groupCreateRequest.setLogo(mediaItemService
                    .findTemporaryItemById(currentPerson, request.getLogoTemporaryMediaItemId()));
        }

        final GroupCreateConfirmation groupCreateConfirmation =
                notifyAndWaitForReply(GroupCreateConfirmation.class, groupCreateRequest);

        Group createdGroup = groupCreateConfirmation.getCreatedGroup();

        return new ClientGroupCreateConfirmation(
                grapevineModelMapper.toClientGroup(createdGroup, groupCreateConfirmation.getGroupMembershipStatus(),
                        groupService.findGeoAreaIdsForGroup(createdGroup)));
    }

    @ApiOperation(value = "Delete group",
            notes = "Every group admin can delete a group. The group does not need to be empty.\n" +
                    "Only possible if the feature `de.fhg.iese.dd.dorffunk.group.creation` is enabled for " +
                    "the requesting app variant and user.")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("/groupDeleteRequest")
    public ClientGroupDeleteConfirmation onGroupDeleteRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
            ClientGroupDeleteRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();
        getFeatureService().checkFeatureEnabled(GroupCreationFeature.class,
                FeatureTarget.of(currentPerson, appVariant));

        final Group group = groupService.findGroupById(request.getGroupId());
        groupService.checkGroupMembershipAdmin(currentPerson, group);

        final GroupDeleteConfirmation groupDeleteConfirmation =
                notifyAndWaitForReply(GroupDeleteConfirmation.class, GroupDeleteRequest.builder()
                        .group(group)
                        .deletingPerson(currentPerson)
                        .build());

        return ClientGroupDeleteConfirmation.builder()
                .groupId(groupDeleteConfirmation.getDeletedGroupId())
                .build();
    }

    @ApiOperation(value = "Update group",
            notes = "Every group admin can change the group.\n" +
                    "Only possible if the feature `de.fhg.iese.dd.dorffunk.group.creation` is enabled for " +
                    "the requesting app variant and user.")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND,
            ClientExceptionType.GEO_AREA_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND,
            ClientExceptionType.FILE_ITEM_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("/groupChangeRequest")
    public ClientGroupChangeConfirmation onGroupChangeRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
            ClientGroupChangeRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();
        getFeatureService().checkFeatureEnabled(GroupCreationFeature.class,
                FeatureTarget.of(currentPerson, appVariant));

        final Group groupToBeChanged = groupService.findGroupById(request.getGroupId());

        groupService.checkGroupMembershipAdmin(currentPerson, groupToBeChanged);

        final GeoArea mainGeoArea = geoAreaService.findGeoAreaById(request.getMainGeoAreaId());
        final Set<GeoArea> includedGeoAreas = geoAreaService.findAllById(request.getIncludedGeoAreaIds());
        final Set<GeoArea> excludedGeoAreas = geoAreaService.findAllById(request.getExcludedGeoAreaIds());
        groupService.checkGeoAreaConsistency(mainGeoArea, includedGeoAreas, excludedGeoAreas);

        final GroupChangeConfirmation groupChangeConfirmation =
                notifyAndWaitForReply(GroupChangeConfirmation.class, GroupChangeRequest.builder()
                        .group(groupToBeChanged)
                        .changingPerson(currentPerson)
                        .newName(StringUtils.trim(request.getName()))
                        .newShortName(StringUtils.trim(request.getShortName()))
                        .newDescription(StringUtils.defaultIfBlank(StringUtils.trim(request.getDescription()), null))
                        .newGroupVisibility(request.getGroupVisibility())
                        .newGroupAccessibility(request.getGroupAccessibility())
                        .newGroupContentVisibility(request.getGroupContentVisibility())
                        .newMainGeoArea(mainGeoArea)
                        .newIncludedGeoAreas(includedGeoAreas)
                        .newExcludedGeoAreas(excludedGeoAreas)
                        .logoPlaceHolder(clientMediaItemService.toItemPlaceHolder(request.getLogo(),
                                groupToBeChanged.getLogo(),
                                currentPerson))
                        .build());
        final Group updatedGroup = groupChangeConfirmation.getUpdatedGroup();

        return new ClientGroupChangeConfirmation(
                grapevineModelMapper.toClientGroup(updatedGroup,
                        groupService.findGroupMembershipStatusForGroup(updatedGroup, currentPerson),
                        groupService.findGeoAreaIdsForGroup(updatedGroup)));
    }

    @ApiOperation(value = "Request to join a group",
            notes = "Depending on the group accessibility settings the user is member immediately or requires approval." +
                    "If the membership status is APPROVED or REJECTED the existing membership is returned. " +
                    "If the status is PENDING a GROUP_MEMBERSHIP_ALREADY_PENDING error is returned.")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND,
            ClientExceptionType.GROUP_MEMBERSHIP_ALREADY_PENDING})
    @ApiException(value = ClientExceptionType.GROUP_MEMBERSHIP_ALREADY_PENDING,
            reason = "The person already requested to join the group and was not yet approved.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping(value = "groupJoinRequest")
    public ClientGroupJoinConfirmation joinGroup(
            @Valid @RequestBody ClientGroupJoinRequest clientRequest) {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();
        final Group group =
                groupService.findGroupByIdFilteredByGroupSettings(clientRequest.getGroupId(), person, appVariant);

        final GroupJoinRequest request = GroupJoinRequest.builder()
                .group(group)
                .personToJoin(person)
                .memberIntroductionText(clientRequest.getMemberIntroductionText())
                .build();

        final GroupJoinConfirmation joinConfirmation = notifyAndWaitForReply(GroupJoinConfirmation.class, request);
        GroupMembership membership = joinConfirmation.getMembership();

        return new ClientGroupJoinConfirmation(grapevineModelMapper.toClientGroup(
                membership.getGroup(),
                membership.getStatus(),
                groupService.findGeoAreaIdsForGroup(group)));
    }

    @ApiOperation(value = "Request to leave a group",
            notes = "A user can leave a group any time. " +
                    "Depending on the group accessibility settings the user might require approval on re-joining the group.")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND,
            ClientExceptionType.NOT_MEMBER_OF_GROUP
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping(value = "groupLeaveRequest")
    public ClientGroupLeaveConfirmation leaveGroup(
            @Valid @RequestBody ClientGroupLeaveRequest clientRequest) {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();
        final Group group =
                groupService.findGroupByIdFilteredByGroupSettings(clientRequest.getGroupId(), person, appVariant);

        final GroupLeaveRequest request = GroupLeaveRequest.builder()
                .group(group)
                .personToLeave(person)
                .build();

        final GroupLeaveConfirmation leaveConfirmation = notifyAndWaitForReply(GroupLeaveConfirmation.class, request);

        return new ClientGroupLeaveConfirmation(grapevineModelMapper.toClientGroup(
                leaveConfirmation.getGroup(),
                leaveConfirmation.getMembershipStatus(),
                groupService.findGeoAreaIdsForGroup(group)));
    }

    @ApiOperation(value = "Accept a pending group join request",
            notes = "This action can only be done by membership admins of the group.")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredRoles = {GroupMembershipAdmin.class})
    @PostMapping(value = "groupPendingJoinAcceptRequest")
    public ClientGroupPendingJoinAcceptConfirmation acceptPendingGroupJoinRequest(
            @Valid @RequestBody ClientGroupPendingJoinAcceptRequest clientRequest) {

        final Person acceptingPerson = getCurrentPersonNotNull();
        final Group group = groupService.findGroupById(clientRequest.getGroupId());
        groupService.checkGroupMembershipAdmin(acceptingPerson, group);
        final Person personToAccept = getPersonService().findPersonById(clientRequest.getPersonIdToAccept());

        final GroupPendingJoinAcceptRequest request = GroupPendingJoinAcceptRequest.builder()
                .group(group)
                .acceptingPerson(acceptingPerson)
                .personToAccept(personToAccept)
                .build();

        final GroupPendingJoinAcceptConfirmation pendingJoinAcceptConfirmation =
                notifyAndWaitForReply(GroupPendingJoinAcceptConfirmation.class, request);

        return ClientGroupPendingJoinAcceptConfirmation.builder()
                .groupId(pendingJoinAcceptConfirmation.getGroup().getId())
                .groupMembershipStatus(pendingJoinAcceptConfirmation.getGroupMembershipStatus())
                .groupMembershipStatusChanged(pendingJoinAcceptConfirmation.isGroupMembershipStatusChanged())
                .build();
    }

    @ApiOperation(value = "Accept a pending group join request that was requested via mail",
            notes = "This action can only be done by membership admins of the group.")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.ONE_TIME_TOKEN, notes = "Authorization is done via mail token")
    @PostMapping(value = "groupPendingJoinAcceptRequest/mail")
    public ClientGroupPendingJoinAcceptConfirmation acceptPendingGroupJoinRequestMail(
            @RequestParam String mailToken,
            @RequestParam String acceptingPersonId,
            @Valid @RequestBody ClientGroupPendingJoinAcceptRequest clientRequest) {

        final Group group = groupService.findGroupById(clientRequest.getGroupId());
        final Person acceptingPerson = getPersonService().findPersonById(acceptingPersonId);
        addAdditionalLogValue("personId", acceptingPerson.getId());
        final Person personToAccept = getPersonService().findPersonById(clientRequest.getPersonIdToAccept());

        groupService.checkGroupMembershipAdmin(acceptingPerson, group);

        final GroupPendingJoinAcceptRequest request = GroupPendingJoinAcceptRequest.builder()
                .group(group)
                .acceptingPerson(acceptingPerson)
                .personToAccept(personToAccept)
                .build();

        authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, request);

        final GroupPendingJoinAcceptConfirmation
                pendingJoinAcceptConfirmation =
                notifyAndWaitForReply(GroupPendingJoinAcceptConfirmation.class, request);

        return ClientGroupPendingJoinAcceptConfirmation.builder()
                .groupId(pendingJoinAcceptConfirmation.getGroup().getId())
                .groupMembershipStatus(pendingJoinAcceptConfirmation.getGroupMembershipStatus())
                .groupMembershipStatusChanged(pendingJoinAcceptConfirmation.isGroupMembershipStatusChanged())
                .build();
    }

    @ApiOperation(value = "Deny a pending group join request",
            notes = "This action can only be done by membership admins of the group.")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredRoles = {GroupMembershipAdmin.class})
    @PostMapping(value = "groupPendingJoinDenyRequest")
    public ClientGroupPendingJoinDenyConfirmation denyPendingGroupJoinRequest(
            @Valid @RequestBody ClientGroupPendingJoinDenyRequest clientRequest) {

        final Person denyingPerson = getCurrentPersonNotNull();
        final Group group = groupService.findGroupById(clientRequest.getGroupId());
        final Person personToDeny = getPersonService().findPersonById(clientRequest.getPersonIdToDeny());

        groupService.checkGroupMembershipAdmin(denyingPerson, group);

        final GroupPendingJoinDenyRequest request = GroupPendingJoinDenyRequest.builder()
                .group(group)
                .denyingPerson(denyingPerson)
                .personToDeny(personToDeny)
                .build();

        final GroupPendingJoinDenyConfirmation pendingJoinDenyConfirmation =
                notifyAndWaitForReply(GroupPendingJoinDenyConfirmation.class, request);

        return ClientGroupPendingJoinDenyConfirmation.builder()
                .groupId(pendingJoinDenyConfirmation.getGroup().getId())
                .groupMembershipStatus(pendingJoinDenyConfirmation.getGroupMembershipStatus())
                .groupMembershipStatusChanged(pendingJoinDenyConfirmation.isGroupMembershipStatusChanged())
                .build();
    }

    @ApiOperation(value = "Deny a pending group join request that was requested via mail",
            notes = "This action can only be done by membership admins of the group.")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.ONE_TIME_TOKEN, notes = "Authorization is done via mail token")
    @PostMapping(value = "groupPendingJoinDenyRequest/mail")
    public ClientGroupPendingJoinDenyConfirmation denyPendingGroupJoinRequestMail(
            @RequestParam String mailToken,
            @RequestParam String denyingPersonId,
            @Valid @RequestBody ClientGroupPendingJoinDenyRequest clientRequest) {

        final Group group = groupService.findGroupById(clientRequest.getGroupId());
        final Person denyingPerson = getPersonService().findPersonById(denyingPersonId);
        addAdditionalLogValue("personId", denyingPerson.getId());
        final Person personToDeny = getPersonService().findPersonById(clientRequest.getPersonIdToDeny());

        groupService.checkGroupMembershipAdmin(denyingPerson, group);

        final GroupPendingJoinDenyRequest request = GroupPendingJoinDenyRequest.builder()
                .group(group)
                .denyingPerson(denyingPerson)
                .personToDeny(personToDeny)
                .build();

        authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, request);

        final GroupPendingJoinDenyConfirmation pendingJoinDenyConfirmation =
                notifyAndWaitForReply(GroupPendingJoinDenyConfirmation.class, request);

        return ClientGroupPendingJoinDenyConfirmation.builder()
                .groupId(pendingJoinDenyConfirmation.getGroup().getId())
                .groupMembershipStatus(pendingJoinDenyConfirmation.getGroupMembershipStatus())
                .groupMembershipStatusChanged(pendingJoinDenyConfirmation.isGroupMembershipStatusChanged())
                .build();
    }

    @ApiOperation(value = "Flags a group as inappropriate",
            notes = "Caller does not need to be member of the group, but the group visibility rules are applied")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping(value = "groupFlagRequest")
    public ClientUserGeneratedContentFlagResponse onChatFlagRequest(
            @Valid @RequestBody ClientGroupFlagRequest request) {

        final Person flagCreator = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();
        final Group group =
                groupService.findGroupByIdFilteredByGroupSettings(request.getGroupId(), flagCreator, appVariant);
        Person firstAdmin = getRoleService().getAllPersonsWithRoleForEntity(GroupMembershipAdmin.class, group).stream()
                .findFirst()
                .orElse(null);

        final GroupFlagRequest flagRequest = GroupFlagRequest.builder()
                .entity(group)
                .entityAuthor(firstAdmin)
                .entityDescription(String.format("Group %s '%s'", group.getId(), group.getName()))
                .flagCreator(flagCreator)
                .tenant(flagCreator.getTenant())
                .comment(request.getComment())
                .allowMultipleFlagsOfSameEntity(false)
                .build();

        final UserGeneratedContentFlagConfirmation confirmation =
                notifyAndWaitForReply(UserGeneratedContentFlagConfirmation.class, flagRequest);
        return new ClientUserGeneratedContentFlagResponse(confirmation.getCreatedFlag());
    }

}
