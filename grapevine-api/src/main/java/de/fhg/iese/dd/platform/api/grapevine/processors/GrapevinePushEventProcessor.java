/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz, Ben Burkhard, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.processors;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.framework.clientevent.PushEvent;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostDeleteConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentOnPostEvent;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientReplyToCommentEvent;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupPendingJoinAcceptConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupPendingJoinDenyConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.push.services.IClientPushService;
import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.*;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinAcceptConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinDenyConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.like.LikeCommentConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.like.LikePostConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.like.UnlikeCommentConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.like.UnlikePostConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.suggestion.SuggestionCategoryChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.suggestion.SuggestionStatusChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.suggestion.SuggestionWithdrawConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.grapevine.services.INewsSourceService;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostInteractionService;
import de.fhg.iese.dd.platform.business.participants.personblocking.services.IPersonBlockingService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.feature.exceptions.FeatureNotFoundException;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
public class GrapevinePushEventProcessor extends BaseEventProcessor {

    @Autowired
    private IClientPushService clientPushService;
    @Autowired
    private IGroupService groupService;
    @Autowired
    private IPersonBlockingService personBlockingService;
    @Autowired
    private IPostInteractionService postInteractionService;
    @Autowired
    private IFeatureService featureService;
    @Autowired
    private IAppService appService;
    @Autowired
    private INewsSourceService newsSourceService;

    @Autowired
    private GrapevineClientModelMapper grapevineClientModelMapper;

    @EventProcessing
    private void handlePostCreateConfirmation(PostCreateConfirmation<?> createConfirmation) {

        if (!createConfirmation.isPublished()) {
            return;
        }
        try {
            handlePublishingOfPost(createConfirmation.getPost(), createCustomPushSendRequest(createConfirmation),
                    createConfirmation.getPushMessageLoud());
        } catch (Exception ex) {
            handlePushException(ex, createConfirmation);
        }
    }

    @EventProcessing
    private void handleExternalPostPublishConfirmation(ExternalPostPublishConfirmation publishConfirmation) {

        try {
            handlePublishingOfPost(publishConfirmation.getExternalPost(),
                    createCustomPushSendRequest(publishConfirmation), publishConfirmation.getPushMessageLoud());
        } catch (Exception ex) {
            handlePushException(ex, publishConfirmation);
        }
    }

    @PushEvent(clientEvent = ClientPostCreateConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED",
            notes = """
                    Sent to all persons that selected the geo area (except the author) in which the post was created.
                    If the post is marked hiddenForOtherHomeAreas, it will only be sent to persons that have the same home area.
                    If it is an internal suggestion no push is sent.""")
    @PushEvent(clientEvent = ClientPostCreateConfirmation.class,
            loud = false,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED",
            notes = "Sent to the author of the gossip.")
    @PushEvent(clientEvent = ClientPostCreateConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_POST_IN_GROUP_CREATED",
            notes = "Sent to all group members if a new post was created in the group.")
    private void handlePublishingOfPost(Post post, @Nullable IClientPushService.PushSendRequest customPushSendRequest,
                                        @Nullable Boolean customLoudPush) {
        //do not push content created by parallel world inhabitants
        if (post.isParallelWorld()) {
            return;
        }
        if (post instanceof Suggestion && ((Suggestion) post).isInternal()) {
            return;
        }
        Pair<PushCategory.PushCategoryId, String> pushCategoryIdAndMessage =
                post.accept(new Post.Visitor<>() {
                    @Override
                    public Pair<PushCategory.PushCategoryId, String> whenGossip(Gossip gossip) {

                        return Pair.of(determinePushCategoryId(DorfFunkConstants.PUSH_CATEGORY_ID_GOSSIP_CREATED),
                                String.format("%s funkt in %s: %n%s",
                                        getAbbreviatedCreatorName(gossip),
                                        getChannelName(GossipPostTypeFeature.class, gossip, "Plausch"),
                                        abbreviateText(gossip.getText())));
                    }

                    @Override
                    public Pair<PushCategory.PushCategoryId, String> whenNewsItem(NewsItem newsItem) {

                        return Pair.of(determinePushCategoryId(DorfFunkConstants.PUSH_CATEGORY_ID_NEWSITEM_CREATED),
                                String.format("%s funkt in %s: %n%s",
                                        newsItem.getNewsSource() == null
                                                ? getAbbreviatedCreatorName(newsItem)
                                                : newsSourceService.getExpandedNewsSourceSiteName(newsItem),
                                        getChannelName(NewsItemPostTypeFeature.class, newsItem, "News"),
                                        abbreviateText(newsItem.getText())));
                    }

                    @Override
                    public Pair<PushCategory.PushCategoryId, String> whenSuggestion(Suggestion suggestion) {

                        return Pair.of(determinePushCategoryId(DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_CREATED),
                                String.format("%s funkt in %s: %n%s",
                                        getAbbreviatedCreatorName(suggestion),
                                        getChannelName(SuggestionPostTypeFeature.class, suggestion, "\"Sag's uns\""),
                                        abbreviateText(suggestion.getText())));
                    }

                    @Override
                    public Pair<PushCategory.PushCategoryId, String> whenSeeking(Seeking seeking) {

                        return Pair.of(determinePushCategoryId(DorfFunkConstants.PUSH_CATEGORY_ID_SEEKING_CREATED),
                                String.format("%s funkt in %s: %n%s",
                                        getAbbreviatedCreatorName(seeking),
                                        getChannelName(SeekingPostTypeFeature.class, seeking, "Suche"),
                                        abbreviateText(seeking.getText())));
                    }

                    @Override
                    public Pair<PushCategory.PushCategoryId, String> whenOffer(Offer offer) {

                        return Pair.of(determinePushCategoryId(DorfFunkConstants.PUSH_CATEGORY_ID_OFFER_CREATED),
                                String.format("%s funkt in %s: %n%s",
                                        getAbbreviatedCreatorName(offer),
                                        getChannelName(OfferPostTypeFeature.class, offer, "Biete"),
                                        abbreviateText(offer.getText())));
                    }

                    @Override
                    public Pair<PushCategory.PushCategoryId, String> whenHappening(Happening happening) {

                        return Pair.of(determinePushCategoryId(DorfFunkConstants.PUSH_CATEGORY_ID_HAPPENING_CREATED),
                                String.format("%s funkt in %s: %n%s",
                                        happening.getNewsSource() == null
                                                ? getAbbreviatedCreatorName(happening)
                                                : newsSourceService.getExpandedNewsSourceSiteName(happening),
                                        getChannelName(HappeningPostTypeFeature.class, happening, "Events"),
                                        abbreviateText(happening.getText())));
                    }

                    @Override
                    public Pair<PushCategory.PushCategoryId, String> whenSpecialPost(SpecialPost specialPost) {

                        return Pair.of(determinePushCategoryId(DorfFunkConstants.PUSH_CATEGORY_ID_SPECIALPOST_CREATED),
                                String.format("%s funkt in %s: %n%s",
                                        getAbbreviatedCreatorName(specialPost),
                                        getChannelName(SpecialPostTypeFeature.class, specialPost, "Dorfhelfer"),
                                        abbreviateText(specialPost.getText())));
                    }

                    private PushCategory.PushCategoryId determinePushCategoryId(PushCategory.PushCategoryId defaultCategoryId) {

                        if (post.getGroup() == null) {
                            if (post.getOrganization() != null && post.getOrganization().getTags()
                                    .hasValue(OrganizationTag.EMERGENCY_AID)) {
                                return DorfFunkConstants.PUSH_CATEGORY_ID_EMERGENCY_AID_POST_CREATED;
                            }
                            return defaultCategoryId;
                        }
                        return DorfFunkConstants.PUSH_CATEGORY_ID_POST_IN_GROUP_CREATED;
                    }
                });

        PushCategory.PushCategoryId pushCategoryId = pushCategoryIdAndMessage.getLeft();
        String message = pushCategoryIdAndMessage.getRight();

        ClientPostCreateConfirmation clientEvent = ClientPostCreateConfirmation.builder()
                .post(grapevineClientModelMapper.toClientPost(post, false))
                .organizationTags(grapevineClientModelMapper.getClientOrganizationTagsOrNull(post))
                .build();

        PushCategory category = clientPushService.findCategory(pushCategoryId);

        Person creator = post.getCreator();
        IClientPushService.PushSendRequest pushSendRequest = IClientPushService.PushSendRequest.builder()
                .event(clientEvent)
                .message(message)
                .category(category)
                .badgeCount(1)
                .build();

        if (post.getGroup() != null) {
            //post for a group
            final List<Person> groupMembers = groupService.findAllApprovedMembers(post.getGroup());
            for (Person groupMember : groupMembers) {
                if (groupMember.equals(creator)
                        || personBlockingService.existsBlocking(category.getApp(), groupMember, creator)) {
                    //the author should not get a badge
                    clientPushService.pushToPersonSilent(groupMember, pushSendRequest.toBuilder()
                            .badgeCount(null)
                            .build());
                } else {
                    clientPushService.pushToPersonLoud(creator, groupMember, pushSendRequest);
                }
            }
        } else {
            if (customPushSendRequest != null && customLoudPush != null) {
                //explicit push message defined by event sender
                if (customLoudPush) {
                    clientPushService.pushToGeoAreasLoud(creator, post.getGeoAreas(), post.isHiddenForOtherHomeAreas(),
                            IClientPushService.PushSendRequest.builder()
                                    .event(clientEvent)
                                    .message(customPushSendRequest.getMessage())
                                    .category(customPushSendRequest.getCategory())
                                    .badgeCount(customPushSendRequest.getBadgeCount())
                                    .build());
                } else {
                    clientPushService.pushToGeoAreasSilent(post.getGeoAreas(), post.isHiddenForOtherHomeAreas(),
                            IClientPushService.PushSendRequest.builder()
                                    .event(clientEvent)
                                    .message(customPushSendRequest.getMessage())
                                    .category(customPushSendRequest.getCategory())
                                    .badgeCount(customPushSendRequest.getBadgeCount())
                                    .build());
                }
            } else {
                //post for everyone in the geo areas
                clientPushService.pushToGeoAreasLoud(creator, post.getGeoAreas(),
                        post.isHiddenForOtherHomeAreas(),
                        pushSendRequest);
            }
        }
    }

    @PushEvent(clientEvent = ClientPostChangeConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED",
            loud = false,
            notes = """
                    Sent to all persons that selected the geo area in which the changed post was created.
                    If the post was marked hiddenForOtherHomeAreas, it will only be sent to persons that have the same home area.
                    If the post was created in a group, it will only be sent to group members.
                    If it is an internal suggestion no push is sent.""")
    @EventProcessing
    private void handlePostChangeConfirmation(PostChangeConfirmation<?> changeConfirmation) {

        if (!changeConfirmation.isPublished()) {
            return;
        }
        final Post post = changeConfirmation.getPost();
        //do not push content created by parallel world inhabitants
        if (post.isParallelWorld()) {
            return;
        }
        if (post instanceof Suggestion && ((Suggestion) post).isInternal()) {
            return;
        }
        try {
            final ClientPostChangeConfirmation event =
                    new ClientPostChangeConfirmation(grapevineClientModelMapper.toClientPost(post, null));
            final PushCategory category =
                    clientPushService.findCategory(DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);
            if (post.getGroup() != null) {
                pushToGroupMembersSilent(post.getGroup(), event, category);
            } else {
                if (changeConfirmation.getPushMessageLoud() != null &&
                        changeConfirmation.getPushCategoryId() != null &&
                        StringUtils.isNotBlank(changeConfirmation.getPushMessage())) {
                    //explicit push message defined by event sender
                    IClientPushService.PushSendRequest pushSendRequest = IClientPushService.PushSendRequest.builder()
                            .event(event)
                            .message(changeConfirmation.getPushMessage())
                            .category(clientPushService.findCategory(changeConfirmation.getPushCategoryId()))
                            .badgeCount(1)
                            .build();
                    if (changeConfirmation.getPushMessageLoud()) {
                        clientPushService.pushToGeoAreasLoud(null, post.getGeoAreas(), post.isHiddenForOtherHomeAreas(),
                                pushSendRequest);
                    } else {
                        clientPushService.pushToGeoAreasSilent(post.getGeoAreas(), post.isHiddenForOtherHomeAreas(),
                                pushSendRequest);
                    }
                } else {
                    clientPushService.pushToGeoAreasSilent(post.getGeoAreas(), post.isHiddenForOtherHomeAreas(), event,
                            category);
                }
            }
        } catch (Exception ex) {
            handlePushException(ex, changeConfirmation);
        }
    }

    @PushEvent(clientEvent = ClientPostChangeConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_STATUS_CHANGED",
            loud = false,
            notes = """
                    Sent to all persons that selected the geo area in which the changed suggestion was created.
                    If the suggestion was marked hiddenForOtherHomeAreas, it will only be sent to persons that have the same home area.
                    If the suggestion was created in a group, it will only be sent to group members.""")
    @PushEvent(clientEvent = ClientPostChangeConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_STATUS_CHANGED",
            notes = "Sent to the author of the suggestion.")
    @EventProcessing
    private void handleStatusChangeConfirmation(SuggestionStatusChangeConfirmation statusChangeConfirmation) {
        final Suggestion suggestion = statusChangeConfirmation.getChangedSuggestion();
        // if deleted do not push to anyone
        if (suggestion.isDeleted() || suggestion.isInternal()) {
            return;
        }
        try {
            Person creator = suggestion.getCreator();
            final ClientPostChangeConfirmation event = new ClientPostChangeConfirmation(
                    grapevineClientModelMapper.toClientPost(suggestion,
                            postInteractionService.isPostLiked(creator, suggestion)));
            clientPushService.pushToPersonLoud(creator, IClientPushService.PushSendRequest.builder()
                    .event(event)
                    .message("Der Status deiner Meldung hat sich geändert.")
                    .categoryId(DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_STATUS_CHANGED)
                    .build());
            clientPushService.pushToGeoAreasSilent(suggestion.getGeoAreas(), suggestion.isHiddenForOtherHomeAreas(),
                    event, clientPushService.findCategory(
                            DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_STATUS_CHANGED));
        } catch (Exception ex) {
            handlePushException(ex, statusChangeConfirmation);
        }
    }

    @EventProcessing
    private void handlePostDeleteConfirmation(PostDeleteConfirmation deleteConfirmation) {
        try {
            Post post = deleteConfirmation.getPost();
            handleUnpublishingOfPost(post, post.getGeoAreas());
        } catch (Exception ex) {
            handlePushException(ex, deleteConfirmation);
        }
    }

    @EventProcessing
    private void handlePostUnpublishConfirmation(ExternalPostUnpublishConfirmation unpublishConfirmation) {
        try {
            handleUnpublishingOfPost(unpublishConfirmation.getExternalPost(), unpublishConfirmation.getGeoAreas());
        } catch (Exception ex) {
            handlePushException(ex, unpublishConfirmation);
        }
    }

    @PushEvent(clientEvent = ClientPostDeleteConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED",
            loud = false,
            notes = """
                    Sent to all persons that selected the geo area in which the deleted post was created.
                    If the post was marked hiddenForOtherHomeAreas, it will only be sent to persons that have the same home area.
                    If the post was created in a group, it will only be sent to group members.
                    If it is an internal suggestion no push is sent.""")
    private void handleUnpublishingOfPost(Post post, Set<GeoArea> geoAreas) {

        //do not push content created by parallel world inhabitants
        if (post.isParallelWorld()) {
            return;
        }
        if (post instanceof Suggestion && ((Suggestion) post).isInternal()) {
            return;
        }

        final ClientPostDeleteConfirmation event = new ClientPostDeleteConfirmation(post.getId());
        final PushCategory category =
                clientPushService.findCategory(DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED);
        if (post.getGroup() != null) {
            pushToGroupMembersSilent(post.getGroup(), event, category);
        } else {
            clientPushService.pushToGeoAreasSilent(geoAreas, post.isHiddenForOtherHomeAreas(), event, category);
        }
    }

    @PushEvent(clientEvent = ClientPostDeleteConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED",
            loud = false,
            notes = """
                    Sent to all persons that selected the geo area in which the withdrawn suggestion was created.
                    If the post was marked hiddenForOtherHomeAreas, it will only be sent to persons that have the same home area.
                    If it is an internal suggestion no push is sent.""")
    @EventProcessing
    private void handleSuggestionWithdrawConfirmation(SuggestionWithdrawConfirmation withdrawConfirmation) {
        Suggestion withdrawnSuggestion = withdrawConfirmation.getSuggestion();
        //do not push content created by parallel world inhabitants
        if (withdrawnSuggestion.isParallelWorld()) {
            return;
        }
        if (withdrawnSuggestion.isInternal()) {
            return;
        }
        try {
            clientPushService.pushToGeoAreasSilent(
                    withdrawnSuggestion.getGeoAreas(),
                    withdrawnSuggestion.isHiddenForOtherHomeAreas(),
                    new ClientPostDeleteConfirmation(withdrawnSuggestion.getId()),
                    clientPushService.findCategory(DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED));
        } catch (Exception ex) {
            handlePushException(ex, withdrawConfirmation);
        }
    }

    @PushEvent(clientEvent = ClientPostChangeConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED",
            loud = false,
            notes = """
                    Sent to all persons that selected the geo area in which the changed suggestion was created.
                    If the post was marked hiddenForOtherHomeAreas, it will only be sent to persons that have the same home area.
                    If it is an internal suggestion no push is sent.""")
    @EventProcessing
    private void handleSuggestionCategoryChangeConfirmation(SuggestionCategoryChangeConfirmation
            suggestionCategoryChangeConfirmation) {
        Suggestion changedSuggestion = suggestionCategoryChangeConfirmation.getChangedSuggestion();
        //do not push content created by parallel world inhabitants
        if (changedSuggestion.isParallelWorld()) {
            return;
        }
        if (changedSuggestion.isInternal()) {
            return;
        }
        try {
            clientPushService.pushToGeoAreasSilent(
                    changedSuggestion.getGeoAreas(),
                    changedSuggestion.isHiddenForOtherHomeAreas(),
                    new ClientPostChangeConfirmation(
                            grapevineClientModelMapper.toClientPost(changedSuggestion, null)),
                    clientPushService.findCategory(DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED));
        } catch (Exception ex) {
            handlePushException(ex, suggestionCategoryChangeConfirmation);
        }
    }

    @PushEvent(clientEvent = ClientPostChangeConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED",
            loud = false,
            notes = "Sent to the person that liked the post.")
    @EventProcessing
    private void handleLikePostConfirmation(LikePostConfirmation likePostConfirmation) {
        final Post post = likePostConfirmation.getPost();
        final Person liker = likePostConfirmation.getLiker();
        try {
            clientPushService.pushToPersonSilent(
                    liker,
                    new ClientPostChangeConfirmation(grapevineClientModelMapper.toClientPost(post, true)),
                    clientPushService.findCategory(DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED));
        } catch (Exception ex) {
            handlePushException(ex, likePostConfirmation);
        }
    }

    @PushEvent(clientEvent = ClientPostChangeConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED",
            loud = false,
            notes = "Sent to the person that unliked the post.")
    @EventProcessing
    private void handleUnlikePostConfirmation(UnlikePostConfirmation unlikePostConfirmation) {
        final Post post = unlikePostConfirmation.getPost();
        final Person unliker = unlikePostConfirmation.getUnliker();
        try {
            clientPushService.pushToPersonSilent(
                    unliker,
                    new ClientPostChangeConfirmation(grapevineClientModelMapper.toClientPost(post, false)),
                    clientPushService.findCategory(DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED));
        } catch (Exception ex) {
            handlePushException(ex, unlikePostConfirmation);
        }
    }

    @PushEvent(clientEvent = ClientCommentOnPostEvent.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_ON_POST",
            notes = "Sent to the creator the post the comment is created on.\n" +
                    "If the post is an internal suggestion no push is sent.")
    @PushEvent(clientEvent = ClientReplyToCommentEvent.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_REPLY",
            notes = "Sent to the creator of the comment the comment is a reply to.\n" +
                    "If the post is an internal suggestion no push is sent.")
    @EventProcessing
    private void handleCommentCreateConfirmation(CommentCreateConfirmation createConfirmation) {
        final Comment comment = createConfirmation.getComment();
        final Comment repliedComment = createConfirmation.getComment().getReplyTo();
        final Person repliedCommentCreator = repliedComment != null ? repliedComment.getCreator() : null;
        final Post post = comment.getPost();
        final Person postCreator = post.getCreator();
        final Person commentCreator = comment.getCreator();
        if (postCreator == null) {
            return;
        }
        //do not push content created by parallel world inhabitants
        if (commentCreator.getStatuses().hasValue(PersonStatus.PARALLEL_WORLD_INHABITANT)) {
            return;
        }
        if (post instanceof Suggestion && ((Suggestion) post).isInternal()) {
            return;
        }
        if (post.getGroup() == null || groupService.isGroupMember(post.getGroup(), postCreator)) {
            try {
                // push loud to post creator, if
                // this is not the comment creator and
                // the replied comment is not created by the post creator -> will get a reply to comment anyway
                if (!commentCreator.equals(postCreator) && !postCreator.equals(repliedCommentCreator)) {
                    String message = String.format("%s antwortet auf deinen Funk: %n%s",
                            commentCreator.getFirstNameFirstCharLastDot(),
                            abbreviateText(comment.getText()));
                    clientPushService.pushToPersonLoud(
                            commentCreator,
                            postCreator,
                            ClientCommentOnPostEvent.builder()
                                    .post(grapevineClientModelMapper.toClientPost(post,
                                            postInteractionService.isPostLiked(postCreator, post)))
                                    .comment(grapevineClientModelMapper.toClientComment(comment, false))
                                    .build(),
                            message,
                            clientPushService.findCategory(
                                    DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_ON_POST));
                }
            } catch (Exception ex) {
                handlePushException(ex, createConfirmation);
            }
        }

        //push to comment author that got a reply
        if (repliedComment != null) {
            if (repliedComment.getPost().getGroup() == null ||
                    groupService.isGroupMember(repliedComment.getPost().getGroup(), repliedCommentCreator)) {
                try {
                    String message = String.format("%s antwortet auf deinen Kommentar: %n%s",
                            commentCreator.getFirstNameFirstCharLastDot(),
                            abbreviateText(comment.getText()));
                    clientPushService.pushToPersonLoud(
                            commentCreator,
                            repliedCommentCreator,
                            ClientReplyToCommentEvent.builder()
                                    .postId(post.getId())
                                    .newComment(grapevineClientModelMapper.toClientComment(comment, false))
                                    .referencedCommentId(repliedComment.getId())
                                    .build(),
                            message,
                            clientPushService.findCategory(
                                    DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_REPLY));
                } catch (Exception ex) {
                    handlePushException(ex, createConfirmation);
                }
            }
        }
    }

    @PushEvent(clientEvent = ClientCommentChangeConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_CHANGED",
            loud = false,
            notes = "Sent to the person that liked the comment.")
    @EventProcessing
    private void handleLikeCommentConfirmation(LikeCommentConfirmation likeCommentConfirmation) {
        final Comment comment = likeCommentConfirmation.getComment();
        final Person liker = likeCommentConfirmation.getLiker();
        try {
            clientPushService.pushToPersonSilent(
                    liker,
                    new ClientCommentChangeConfirmation(grapevineClientModelMapper.toClientComment(comment, true)),
                    clientPushService.findCategory(DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_CHANGED));
        } catch (Exception ex) {
            handlePushException(ex, likeCommentConfirmation);
        }
    }

    @PushEvent(clientEvent = ClientCommentChangeConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_CHANGED",
            loud = false,
            notes = "Sent to the person that unliked the comment.")
    @EventProcessing
    private void handleUnlikeCommentConfirmation(UnlikeCommentConfirmation unlikeCommentConfirmation) {
        final Comment comment = unlikeCommentConfirmation.getComment();
        final Person unliker = unlikeCommentConfirmation.getUnliker();
        try {
            clientPushService.pushToPersonSilent(
                    unliker,
                    new ClientCommentChangeConfirmation(grapevineClientModelMapper.toClientComment(comment, false)),
                    clientPushService.findCategory(DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_CHANGED));
        } catch (Exception ex) {
            handlePushException(ex, unlikeCommentConfirmation);
        }
    }

    @PushEvent(clientEvent = ClientGroupPendingJoinAcceptConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_GROUP_MEMBER_STATUS_CHANGED",
            loud = false,
            notes = "Sent to the person that requested to join the group and was accepted.")
    @EventProcessing
    private void handleGroupPendingJoinAcceptConfirmation(
            GroupPendingJoinAcceptConfirmation groupPendingJoinAcceptConfirmation) {
        if (!groupPendingJoinAcceptConfirmation.isGroupMembershipStatusChanged()) {
            return;
        }
        Person personToAccept = groupPendingJoinAcceptConfirmation.getPersonToAccept();
        String message = String.format("Du wurdest zur Gruppe %s hinzugefügt.",
                groupPendingJoinAcceptConfirmation.getGroup().getName());
        try {
            clientPushService.pushToPersonLoud(
                    personToAccept,
                    ClientGroupPendingJoinAcceptConfirmation.builder()
                            .groupId(groupPendingJoinAcceptConfirmation.getGroup().getId())
                            .build(),
                    message,
                    clientPushService.findCategory(
                            DorfFunkConstants.PUSH_CATEGORY_ID_GROUP_MEMBER_STATUS_CHANGED));
        } catch (Exception ex) {
            handlePushException(ex, groupPendingJoinAcceptConfirmation);
        }
    }

    @PushEvent(clientEvent = ClientGroupPendingJoinDenyConfirmation.class,
            categoryIdName = "DorfFunkConstants.PUSH_CATEGORY_ID_GROUP_MEMBER_STATUS_CHANGED",
            loud = false,
            notes = "Sent to the person that requested to join the group and was denied.")
    @EventProcessing
    private void handleGroupPendingJoinDenyConfirmation(
            GroupPendingJoinDenyConfirmation groupPendingJoinDenyConfirmation) {
        if (!groupPendingJoinDenyConfirmation.isGroupMembershipStatusChanged()) {
            return;
        }
        Person personToDeny = groupPendingJoinDenyConfirmation.getPersonToDeny();
        String message = String.format("Deine Anfrage für die Gruppe %s wurde abgelehnt.",
                groupPendingJoinDenyConfirmation.getGroup().getName());
        try {
            clientPushService.pushToPersonLoud(personToDeny,
                    ClientGroupPendingJoinDenyConfirmation.builder()
                            .groupId(groupPendingJoinDenyConfirmation.getGroup().getId())
                            .build(),
                    message,
                    clientPushService.findCategory(DorfFunkConstants.PUSH_CATEGORY_ID_GROUP_MEMBER_STATUS_CHANGED));
        } catch (Exception ex) {
            handlePushException(ex, groupPendingJoinDenyConfirmation);
        }
    }

    private String getAbbreviatedCreatorName(Post post) {

        Person creator = post.getCreator();
        if (creator == null) {
            return "-";
        } else {
            return creator.getFirstNameFirstCharLastDot();
        }
    }

    private <P extends Post> String getChannelName(Class<? extends PostTypeFeature<P>> feature, P post,
            String defaultName) {

        try {
            //post.tenant can be null, so the app config for the feature is taken
            PostTypeFeature<?> featureValue = featureService
                    .getFeature(feature,
                            FeatureTarget.of(post.getTenant(), appService.findById(DorfFunkConstants.APP_ID)));
            if (StringUtils.isNotEmpty(featureValue.getChannelName())) {
                return featureValue.getChannelName();
            }
        } catch (FeatureNotFoundException e) {
            //nothing to do, the feature has not been configured properly, so we return the default name
        }
        return defaultName;
    }

    private void pushToGroupMembersSilent(Group group, ClientBaseEvent event, PushCategory category) {
        final List<Person> groupMembers = groupService.findAllApprovedMembers(group);
        for (Person groupMember : groupMembers) {
            clientPushService.pushToPersonSilent(groupMember, event, category);
        }
    }

    private String abbreviateText(String text) {
        return StringUtils.abbreviate(text, "...", 50);
    }

    private void handlePushException(Exception ex, BaseEvent consumedEvent) {
        log.error("Failed to push " + consumedEvent.getLogMessage(), ex);
    }

    private IClientPushService.PushSendRequest createCustomPushSendRequest(BasePostCustomPushEvent event) {

        IClientPushService.PushSendRequest pushSendRequest;
        if (event.getPushMessageLoud() != null &&
                event.getPushCategoryId() != null &&
                StringUtils.isNotBlank(event.getPushMessage())) {
            pushSendRequest = IClientPushService.PushSendRequest.builder()
                    .message(event.getPushMessage())
                    .category(clientPushService.findCategory(event.getPushCategoryId()))
                    .badgeCount(1)
                    .build();
        } else {
            pushSendRequest = null;
        }
        return pushSendRequest;
    }

}
