/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientConvenience;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.services.IConvenienceService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Convenience;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/grapevine/convenience")
@Api(tags = {"grapevine.convenience"})
public class ConvenienceController extends BaseController {

    @Autowired
    private IConvenienceService convenienceService;
    @Autowired
    private GrapevineClientModelMapper grapevineClientModelMapper;

    @ApiOperation(value = "Return all available conveniences",
            notes = "All conveniences where one of selected geo areas of the user is in the geo areas of the convenience.\n" +
                    "The convenience is configured for the user, so the `url` is user specific.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping
    public List<ClientConvenience> getAllAvailableConveniences() {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        return convenienceService.findAllAvailableConveniences(person, appVariant).stream()
                .map(grapevineClientModelMapper::toClientConvenience)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Return a convenience by id",
            notes = "No access restriction takes place, all conveniences can be queried.\n" +
                    "The convenience is configured for the user, so the `url` is user specific.")
    @ApiExceptions({ClientExceptionType.CONVENIENCE_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/{convenienceId}")
    public ClientConvenience getConvenienceById(@PathVariable String convenienceId) {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        final Convenience convenience = convenienceService.findConvenienceById(convenienceId, person, appVariant);
        return grapevineClientModelMapper.toClientConvenience(convenience);
    }

}
