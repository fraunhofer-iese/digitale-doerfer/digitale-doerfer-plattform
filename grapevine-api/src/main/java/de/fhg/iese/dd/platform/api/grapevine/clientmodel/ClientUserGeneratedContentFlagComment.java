/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientmodel;

import java.util.List;

import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlagDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientUserGeneratedContentFlagComment extends ClientUserGeneratedContentFlagDetail {

    @ApiModelProperty(required = true, notes = "comment that was flagged, can be deleted")
    private ClientComment flagComment;

    @ApiModelProperty(required = true, notes = "post where the flagged comment is related to, can be deleted")
    private ClientPost postOfComment;

    @ApiModelProperty(required = true, notes = "indicates if the post the flagged comment is related to was deleted")
    private boolean postOfCommentDeleted;

    @ApiModelProperty(required = true,
            notes = "comments of the post where the flagged comment is related to, including deleted")
    private List<ClientComment> commentsOfPost;

}
