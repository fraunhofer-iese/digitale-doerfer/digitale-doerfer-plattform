/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupAddGroupMembershipAdminConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupAddGroupMembershipAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupAddPersonConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupAddPersonRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupChangeNameByAdminConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupChangeNameByAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupChangeVisibilityAndAccessibilityByAdminConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupChangeVisibilityAndAccessibilityByAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupCreateByAdminConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupCreateByAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupDeleteByAdminConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupDeleteByAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupModifyGeoAreasByAdminConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupModifyGeoAreasByAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupRemoveGroupMembershipAdminConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupRemoveGroupMembershipAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupAddGroupMembershipAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupAddGroupMembershipAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupChangeNameByAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupChangeNameByAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupChangeVisibilityAndAccessibilityByAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupChangeVisibilityAndAccessibilityByAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupCreateByAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupCreateByAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupDeleteByAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupDeleteByAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupJoinByAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupJoinConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupModifyGeoAreasByAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupModifyGeoAreasByAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupRemoveGroupMembershipAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupRemoveGroupMembershipAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.security.AddPersonToGroupAction;
import de.fhg.iese.dd.platform.business.grapevine.security.CreateGroupAction;
import de.fhg.iese.dd.platform.business.grapevine.security.ModifyGroupAction;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/adminui/group/event")
@Api(tags = {"grapevine.adminui.group.events"})
public class GroupAdminUiEventController extends BaseAdminUiController {

    @Autowired
    private IGroupService groupService;

    @Autowired
    private ITenantService tenantService;

    @Autowired
    private IGeoAreaService geoAreaService;

    @Autowired
    private IPersonService personService;

    @Autowired
    private GrapevineClientModelMapper grapevineClientModelMapper;

    @ApiOperation(value = "Create group for specified tenant")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND,
            ClientExceptionType.GEO_AREA_NOT_FOUND,
            ClientExceptionType.PERSON_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {CreateGroupAction.class})
    @PostMapping("/groupCreateByAdminRequest")
    public ClientGroupCreateByAdminConfirmation onGroupCreateByAdminRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientGroupCreateByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted createGroupPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(CreateGroupAction.class, currentPerson);

        final Tenant tenant = tenantService.findTenantById(request.getTenantId());

        if (createGroupPermission.isTenantDenied(tenant)) {
            throw new NotAuthorizedException("Insufficient privileges to create group '{}'", request.getName());
        }

        final Set<GeoArea> includedGeoAreas = geoAreaService.findAllById(request.getIncludedGeoAreaIds());
        final Set<GeoArea> excludedGeoAreas = geoAreaService.findAllById(request.getExcludedGeoAreaIds());
        final List<Person> groupMembershipAdminsToAdd =
                personService.findAllPersonsById(request.getGroupMembershipAdminIds());

        final GroupCreateByAdminConfirmation groupCreateByAdminConfirmation =
                notifyAndWaitForReply(GroupCreateByAdminConfirmation.class, GroupCreateByAdminRequest.builder()
                        .name(request.getName())
                        .shortName(request.getShortName())
                        .tenant(tenant)
                        .groupVisibility(request.getGroupVisibility())
                        .groupAccessibility(request.getGroupAccessibility())
                        .groupContentVisibility(request.getGroupContentVisibility())
                        .includedGeoAreas(includedGeoAreas)
                        .excludedGeoAreas(excludedGeoAreas)
                        .groupMembershipAdminsToJoin(groupMembershipAdminsToAdd)
                        .build());

        return new ClientGroupCreateByAdminConfirmation(
                grapevineClientModelMapper.toClientGroupExtendedDetail(groupCreateByAdminConfirmation.getCreatedGroup(),
                        groupCreateByAdminConfirmation.getGroupGeoAreas(),
                        groupCreateByAdminConfirmation.getGroupMembershipAdmins()));
    }

    @ApiOperation(value = "Delete group",
            notes = "If the attribute 'forceDeleteNonEmptyGroup' is false, the request fails if the group " +
                    "contains any posts or > 3 members.")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND,
            ClientExceptionType.GROUP_CANNOT_BE_DELETED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ModifyGroupAction.class})
    @PostMapping("/groupDeleteByAdminRequest")
    public ClientGroupDeleteByAdminConfirmation onGroupDeleteByAdminRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientGroupDeleteByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted deleteGroupPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ModifyGroupAction.class, currentPerson);

        final Group group = groupService.findGroupById(request.getGroupId());

        if (deleteGroupPermission.isTenantDenied(group.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to delete group '{}'", request.getGroupId());
        }
        final long memberCount = group.getMemberCount();

        final GroupDeleteByAdminConfirmation groupDeleteByAdminConfirmation =
                notifyAndWaitForReply(GroupDeleteByAdminConfirmation.class, GroupDeleteByAdminRequest.builder()
                        .group(group)
                        .forceDeleteNonEmptyGroup(request.isForceDeleteNonEmptyGroup())
                        .build());

        return ClientGroupDeleteByAdminConfirmation.builder()
                .groupId(groupDeleteByAdminConfirmation.getDeletedGroupId())
                .deletedMemberCount(memberCount)
                .build();
    }

    @ApiOperation(value = "Add a person to group")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND,
            ClientExceptionType.PERSON_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {AddPersonToGroupAction.class})
    @PostMapping("/groupAddPersonRequest")
    public ClientGroupAddPersonConfirmation onGroupAddPersonRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientGroupAddPersonRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted addPersonToGroupPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(AddPersonToGroupAction.class, currentPerson);

        final Group group = groupService.findGroupById(request.getGroupId());
        final Person personToJoin = getPersonService().findPersonById(request.getPersonIdToAdd());

        if (addPersonToGroupPermission.isTenantDenied(group.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to add person '{}' to group '{}'",
                    personToJoin.getId(), group.getId());
        }

        final GroupJoinConfirmation groupJoinConfirmation =
                notifyAndWaitForReply(GroupJoinConfirmation.class, GroupJoinByAdminRequest.builder()
                        .group(group)
                        .personToJoin(personToJoin)
                        .memberIntroductionText(request.getMemberIntroductionText())
                        .build());
        final Group updatedGroup = groupJoinConfirmation.getMembership().getGroup();

        return new ClientGroupAddPersonConfirmation(
                grapevineClientModelMapper.toClientGroupExtended(updatedGroup,
                        groupService.findGeoAreasForGroup(updatedGroup)));
    }

    @ApiOperation(value = "Add a person as group membership admin to group",
            notes = "Fully idempotent: Person is only added to group if it is not a member yet. " +
                    "Initial assigning of GroupMembershipAdmin role is only done if it was not already done")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND,
            ClientExceptionType.PERSON_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ModifyGroupAction.class})
    @PostMapping("/groupAddGroupMembershipAdminRequest")
    public ClientGroupAddGroupMembershipAdminConfirmation onGroupAddGroupMembershipAdminRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientGroupAddGroupMembershipAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted addAdminToGroupPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ModifyGroupAction.class, currentPerson);

        final Group group = groupService.findGroupById(request.getGroupId());
        final Person membershipAdminToAdd = getPersonService().findPersonById(request.getMembershipAdminIdToAdd());

        if (addAdminToGroupPermission.isTenantDenied(group.getTenant())) {
            throw new NotAuthorizedException(
                    "Insufficient privileges to add person '{}' as group membership admin to group '{}'",
                    membershipAdminToAdd.getId(), group.getId());
        }

        final GroupAddGroupMembershipAdminConfirmation groupAddGroupMembershipAdminConfirmation =
                notifyAndWaitForReply(GroupAddGroupMembershipAdminConfirmation.class,
                        GroupAddGroupMembershipAdminRequest.builder()
                                .group(group)
                                .membershipAdminToAdd(membershipAdminToAdd)
                                .build());
        final Group updatedGroup = groupAddGroupMembershipAdminConfirmation.getUpdatedGroup();

        return new ClientGroupAddGroupMembershipAdminConfirmation(
                grapevineClientModelMapper.toClientGroupExtendedDetail(updatedGroup,
                        groupService.findGeoAreasForGroup(updatedGroup),
                        groupAddGroupMembershipAdminConfirmation.getGroupMembershipAdmins()));
    }

    @ApiOperation(value = "Remove group membership admin role from person in a group")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND,
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.GROUP_MEMBERSHIP_ADMIN_REMOVAL_NOT_POSSIBLE
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ModifyGroupAction.class})
    @PostMapping("/groupRemoveGroupMembershipAdminRequest")
    public ClientGroupRemoveGroupMembershipAdminConfirmation onGroupRemoveGroupMembershipAdminRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientGroupRemoveGroupMembershipAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted removeAdminFromGroupPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ModifyGroupAction.class, currentPerson);

        final Group group = groupService.findGroupById(request.getGroupId());
        final Person membershipAdminToRemove =
                getPersonService().findPersonById(request.getMembershipAdminIdToRemove());

        if (removeAdminFromGroupPermission.isTenantDenied(group.getTenant())) {
            throw new NotAuthorizedException(
                    "Insufficient privileges to remove group membership admin from person '{}' in group '{}'",
                    membershipAdminToRemove.getId(), group.getId());
        }

        final GroupRemoveGroupMembershipAdminConfirmation groupRemoveGroupMembershipAdminConfirmation =
                notifyAndWaitForReply(GroupRemoveGroupMembershipAdminConfirmation.class,
                        GroupRemoveGroupMembershipAdminRequest.builder()
                                .group(group)
                                .membershipAdminToRemove(membershipAdminToRemove)
                                .build());
        final Group updatedGroup = groupRemoveGroupMembershipAdminConfirmation.getUpdatedGroup();
        final List<Person> groupMembershipAdmins =
                groupRemoveGroupMembershipAdminConfirmation.getGroupMembershipAdmins();

        return new ClientGroupRemoveGroupMembershipAdminConfirmation(
                grapevineClientModelMapper.toClientGroupExtendedDetail(updatedGroup,
                        groupService.findGeoAreasForGroup(updatedGroup),
                        groupMembershipAdmins));
    }

    @ApiOperation(value = "Modify geo areas of group")
    @ApiExceptions({
            ClientExceptionType.EVENT_ATTRIBUTE_INVALID,
            ClientExceptionType.GROUP_NOT_FOUND,
            ClientExceptionType.GEO_AREA_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ModifyGroupAction.class})
    @PostMapping("/groupModifyGeoAreasByAdminRequest")
    public ClientGroupModifyGeoAreasByAdminConfirmation onGroupModifyGeoAreasByAdminRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientGroupModifyGeoAreasByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted modifyGroupPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ModifyGroupAction.class, currentPerson);

        final Group group = groupService.findGroupById(request.getGroupId());

        if (modifyGroupPermission.isTenantDenied(group.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to modify geo areas of group '{}'",
                    request.getGroupId());
        }

        final Set<GeoArea> includedGeoAreas = geoAreaService.findAllById(request.getIncludedGeoAreaIds());
        final Set<GeoArea> excludedGeoAreas = geoAreaService.findAllById(request.getExcludedGeoAreaIds());

        final GroupModifyGeoAreasByAdminConfirmation groupModifyGeoAreasByAdminConfirmation =
                notifyAndWaitForReply(GroupModifyGeoAreasByAdminConfirmation.class,
                        GroupModifyGeoAreasByAdminRequest.builder()
                                .group(group)
                                .includedGeoAreas(includedGeoAreas)
                                .excludedGeoAreas(excludedGeoAreas)
                                .build());

        return new ClientGroupModifyGeoAreasByAdminConfirmation(
                grapevineClientModelMapper.toClientGroupExtended(
                        groupModifyGeoAreasByAdminConfirmation.getGroup(),
                        groupModifyGeoAreasByAdminConfirmation.getGroupGeoAreas()));
    }

    @ApiOperation(value = "Change name of group")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ModifyGroupAction.class})
    @PostMapping("/groupChangeNameByAdminRequest")
    public ClientGroupChangeNameByAdminConfirmation onGroupChangeNameByAdminRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientGroupChangeNameByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted modifyGroupPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ModifyGroupAction.class, currentPerson);

        final Group groupToBeChanged = groupService.findGroupById(request.getGroupId());

        if (modifyGroupPermission.isTenantDenied(groupToBeChanged.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to modify name of group '{}'",
                    request.getGroupId());
        }

        final GroupChangeNameByAdminConfirmation groupChangeNameByAdminConfirmation =
                notifyAndWaitForReply(GroupChangeNameByAdminConfirmation.class, GroupChangeNameByAdminRequest.builder()
                        .group(groupToBeChanged)
                        .newName(request.getNewName())
                        .newShortName(request.getNewShortName())
                        .build());
        final Group updatedGroup = groupChangeNameByAdminConfirmation.getUpdatedGroup();

        return new ClientGroupChangeNameByAdminConfirmation(
                grapevineClientModelMapper.toClientGroupExtended(updatedGroup,
                        groupService.findGeoAreasForGroup(updatedGroup)));
    }

    @ApiOperation(value = "Change accessibility and visibility of group")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ModifyGroupAction.class})
    @PostMapping("/groupChangeVisibilityAndAccessibilityByAdminRequest")
    public ClientGroupChangeVisibilityAndAccessibilityByAdminConfirmation onGroupChangeVisibilityAndAccessibilityByAdminRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientGroupChangeVisibilityAndAccessibilityByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted modifyGroupPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ModifyGroupAction.class, currentPerson);

        final Group groupToBeChanged = groupService.findGroupById(request.getGroupId());

        if (modifyGroupPermission.isTenantDenied(groupToBeChanged.getTenant())) {
            throw new NotAuthorizedException(
                    "Insufficient privileges to modify visibility and accessibility of group '{}'",
                    request.getGroupId());
        }

        final GroupChangeVisibilityAndAccessibilityByAdminConfirmation
                groupChangeVisibilityAndAccessibilityByAdminConfirmation =
                notifyAndWaitForReply(GroupChangeVisibilityAndAccessibilityByAdminConfirmation.class,
                        GroupChangeVisibilityAndAccessibilityByAdminRequest.builder()
                                .group(groupToBeChanged)
                                .groupVisibility(request.getGroupVisibility())
                                .groupAccessibility(request.getGroupAccessibility())
                                .groupContentVisibility(request.getGroupContentVisibility())
                                .build());
        final Group updatedGroup = groupChangeVisibilityAndAccessibilityByAdminConfirmation.getUpdatedGroup();

        return new ClientGroupChangeVisibilityAndAccessibilityByAdminConfirmation(
                grapevineClientModelMapper.toClientGroupExtended(updatedGroup,
                        groupService.findGeoAreasForGroup(updatedGroup)));
    }

}
