/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientTradingCategory;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.services.ITradingCategoryService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/grapevine/trade")
@Api(tags = { "grapevine.post" })
class TradeController extends BaseController {

    @Autowired
    private ITradingCategoryService tradingCategoryService;

    @Autowired
    private GrapevineClientModelMapper grapevineModelMapper;

    @ApiOperation(value = "Returns all trading categories")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping(value = "/tradingCategory")
    public List<ClientTradingCategory> getTradingCategories() {

        return tradingCategoryService.findAllTradingCategoriesInDataInitOrder()
                .stream()
                .map(grapevineModelMapper::toClientTradingCategory)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Return the trading category with the given id")
    @ApiExceptions(ClientExceptionType.TRADING_CATEGORY_NOT_FOUND)
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping(value = "/tradingCategory/{id}")
    public ClientTradingCategory getTradingCategoryById(@PathVariable String id) {

        return grapevineModelMapper.toClientTradingCategory(tradingCategoryService.findTradingCategoryById(id));
    }

}
