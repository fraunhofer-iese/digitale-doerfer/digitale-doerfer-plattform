/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2021 Dominik Schnier, Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientGroupExtended;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientGroupExtendedDetail;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientGroupMemberExtended;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.business.grapevine.security.ListGroupMembersAction;
import de.fhg.iese.dd.platform.business.grapevine.security.ListGroupsAction;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembership;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.GroupMembershipAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/adminui/group")
@Api(tags = {"grapevine.adminui.group"})
public class GroupAdminUiController extends BaseAdminUiController {

    @Autowired
    private IGroupService groupService;

    @Autowired
    private ITenantService tenantService;

    @Autowired
    private GrapevineClientModelMapper grapevineModelMapper;

    @ApiOperation(value = "Returns all groups that the caller has access to")
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID,
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListGroupsAction.class})
    @GetMapping
    public Page<ClientGroupExtended> getGroups(

            @RequestParam(required = false)
            @ApiParam("only return groups for a specific tenantId")
                    String tenantId,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of groups",
                    defaultValue = "0")
                    int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of groups returned in a page",
                    defaultValue = "10")
                    int count
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        final PermissionTenantRestricted listGroupsPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ListGroupsAction.class, currentPerson);
        if (StringUtils.isNotEmpty(tenantId)) {
            //looking up the tenant to ensure that the tenant id is valid
            tenantService.checkTenantByIdExists(tenantId);
            // Is only checked when a tenantId is given
            if (listGroupsPermission.isTenantDenied(tenantId)) {
                //the permission to view this tenant is not given
                throw new NotAuthorizedException("Insufficient privileges to get groups of tenant {}", tenantId);
            }
        }

        checkPageAndCountValues(page, count);
        final PageRequest pageRequest = PageRequest.of(page, count);
        final Page<Group> groups;

        if (StringUtils.isNotEmpty(tenantId)) {
            groups = groupService.findAllNotDeletedGroupsBelongingToTenant(
                    Collections.singleton(tenantId),
                    pageRequest);
        } else {
            if (listGroupsPermission.isAllTenantsAllowed()) {
                groups = groupService.findAllNotDeletedGroups(pageRequest);
            } else {
                groups = groupService.findAllNotDeletedGroupsBelongingToTenant(
                        listGroupsPermission.getAllowedTenantIds(), pageRequest);
            }
        }
        final Map<String, IGroupService.GroupGeoAreas> geoAreasByGroups =
                groupService.findGeoAreasByGroups(groups.toList());

        return new PageImpl<>(
                grapevineModelMapper.toClientGroupsExtended(groups.toList(), geoAreasByGroups),
                groups.getPageable(),
                groups.getTotalElements());
    }

    @ApiOperation(value = "Return a group by id")
    @ApiExceptions({
            ClientExceptionType.GROUP_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListGroupsAction.class}
    )
    @GetMapping(value = "/{groupId}")
    public ClientGroupExtendedDetail getGroupById(
            @PathVariable String groupId
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted listGroupsPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ListGroupsAction.class, currentPerson);

        final Group group = groupService.findGroupByIdIncludingDeleted(groupId);

        if (listGroupsPermission.isTenantDenied(group.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to get group {}", groupId);
        }

        final List<Person> groupMembershipAdmins = groupService.findAllApprovedMembers(group).stream()
                .filter(p -> getRoleService().hasRoleForEntity(p, GroupMembershipAdmin.class, group))
                .collect(Collectors.toList());
        final long postCount = groupService.getPostCountOfGroup(group);
        final Long newestPostCreatedTime = groupService.findNewestPostCreatedTime(group);
        final Long newestCommentCreatedTime = groupService.findNewestCommentCreatedTime(group);
        final IGroupService.GroupGeoAreas geoAreasForGroup = groupService.findGeoAreasForGroup(group);

        return grapevineModelMapper.toClientGroupExtendedDetail(group, geoAreasForGroup, groupMembershipAdmins,
                postCount, newestPostCreatedTime, newestCommentCreatedTime);
    }

    @ApiOperation(value = "Returns all members of a group that the caller has access to")
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID,
            ClientExceptionType.GROUP_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListGroupMembersAction.class})
    @GetMapping(value = "/{groupId}/members")
    Page<ClientGroupMemberExtended> getGroupMembers(

            @PathVariable String groupId,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of group members",
                    defaultValue = "0")
                    int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of group members returned in a page",
                    defaultValue = "10")
                    int count
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        final PermissionTenantRestricted listGroupMembersPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ListGroupMembersAction.class, currentPerson);

        final Group group = groupService.findGroupById(groupId);

        if (listGroupMembersPermission.isTenantDenied(group.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to get members of group '{}'", groupId);
        }

        checkPageAndCountValues(page, count);
        final PageRequest pageRequest = PageRequest.of(page, count, Sort.by("memberLastName"));
        final Page<GroupMembership> groupMemberships = groupService.findAllGroupMembershipsByGroup(group, pageRequest);

        return groupMemberships.map(grapevineModelMapper::toClientGroupMemberExtended);
    }

}
