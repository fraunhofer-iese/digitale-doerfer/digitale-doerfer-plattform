/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Johannes Schneider, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientComment;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.services.ICommentService;
import de.fhg.iese.dd.platform.business.grapevine.services.ILikeCommentService;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/grapevine/comment")
@Api(tags = { "grapevine.comment" })
class CommentController extends BaseController {

    @Autowired
    private IPostService postService;

    @Autowired
    private ICommentService commentService;

    @Autowired
    private GrapevineClientModelMapper grapevineModelMapper;

    @Autowired
    private ILikeCommentService likeCommentService;

    @ApiOperation(value = "Return all comments for a post",
            notes = "Returns all comments for a post, excluding comments from users with status 'PARALLEL_WORLD_INHABITANT'. " +
                    "Deleted comments are also listed with deleted=true and no text or images.")
    @ApiExceptions({ClientExceptionType.POST_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping
    public List<ClientComment> getAllCommentsForPost(
            @RequestParam
            @ApiParam(value = "Post for which the comments are to be returned")
                    String postId) {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        final Post post = postService.findByIdFilteredByHiddenForOtherHomeAreasAndGroupContentVisibility(postId, person,
                appVariant);
        final List<Comment> comments = commentService.findAllByPostFilteredOrderByCreatedAsc(person, post);

        Set<Comment> likedComments = likeCommentService.filterLikedComments(person, comments);
        return comments.stream()
                .map(comment -> grapevineModelMapper.toClientComment(comment, likedComments.contains(comment)))
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Returns a specific comment")
    @ApiExceptions({ClientExceptionType.COMMENT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping(value = "/{commentId}")
    public ClientComment readSingleComment(
            @PathVariable String commentId) {

        Person person = getCurrentPersonNotNull(); //checks that this endpoint is called from a registered user

        Comment comment = commentService.findCommentByIdIncludingDeleted(commentId);
        Set<Comment> likedComments = likeCommentService.filterLikedComments(person, Collections.singletonList(comment));
        return grapevineModelMapper.toClientComment(comment, likedComments.contains(comment));
    }

}
