/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientevent.group;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.framework.validation.MaxTextLength;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItemPlaceHolder;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupAccessibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupContentVisibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupVisibility;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientGroupChangeRequest extends ClientBaseEvent {

    @NotBlank
    @ApiModelProperty(required = true)
    private String groupId;

    @MaxTextLength(max = @Value("#{grapevineConfig.maxNumberCharsPerGroupName}"))
    @ApiModelProperty(required = true)
    private String name;

    @MaxTextLength(max = @Value("#{grapevineConfig.maxNumberCharsPerGroupShortName}"))
    @ApiModelProperty(value = "Can be omitted and will be auto generated based on the name")
    private String shortName;

    @MaxTextLength(max = @Value("#{grapevineConfig.maxNumberCharsPerGroupDescription}"))
    private String description;

    private ClientMediaItemPlaceHolder logo;

    @NotNull
    @ApiModelProperty(required = true)
    private GroupVisibility groupVisibility;

    @NotNull
    @ApiModelProperty(required = true)
    private GroupAccessibility groupAccessibility;

    @NotNull
    @ApiModelProperty(required = true)
    private GroupContentVisibility groupContentVisibility;

    @NotBlank
    @ApiModelProperty(required = true)
    private String mainGeoAreaId;

    @NotEmpty
    @ApiModelProperty(required = true)
    @Singular
    private List<String> includedGeoAreaIds;

    @Singular
    private List<String> excludedGeoAreaIds;

}
