/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Tahmid Ekram, Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.business.grapevine.services.ISuggestionCategoryService;
import de.fhg.iese.dd.platform.business.grapevine.services.ITradingCategoryService;
import de.fhg.iese.dd.platform.business.shared.security.SuperAdminAction;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionCategory;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.TradingCategory;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/administration/grapevine/")
@Api(tags = { "admin", "grapevine.admin" })
class GrapevineAdminController extends BaseController {

    @Autowired
    private ITradingCategoryService tradingCategoryService;
    @Autowired
    private ISuggestionCategoryService suggestionCategoryService;

    @ApiOperation(value = "Deletes a trading category",
            notes = "Deletes a trading category and updates all offers and seekings that referenced " +
                    "this category to the MISC category")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @DeleteMapping(value = "tradingCategory/{id}")
    void deleteTradingCategory(@PathVariable String id) {

        Person currentPerson = getCurrentPersonNotNull();
        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, currentPerson);
        final TradingCategory tradingCategory = tradingCategoryService.findTradingCategoryById(id);
        tradingCategoryService.deleteTradingCategory(tradingCategory, currentPerson);
    }

    @ApiOperation(value = "Deletes a suggestion category",
            notes = "Deletes a suggestion category and updates all suggestions that referenced " +
                    "this category to the MISC category")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @DeleteMapping(value = "suggestionCategory/{id}")
    void deleteSuggestionCategory(@PathVariable String id) {

        Person currentPerson = getCurrentPersonNotNull();
        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, currentPerson);
        final SuggestionCategory suggestionCategory = suggestionCategoryService.findSuggestionCategoryById(id);
        suggestionCategoryService.deleteSuggestionCategory(suggestionCategory, currentPerson);
    }

}
