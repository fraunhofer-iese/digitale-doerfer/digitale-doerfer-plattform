/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.*;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidEventAttributeException;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.*;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientLikeCommentConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientLikeCommentRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientUnlikeCommentConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientUnlikeCommentRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.api.grapevine.exceptions.CommentActionNotAllowedException;
import de.fhg.iese.dd.platform.api.shared.services.IClientDocumentItemService;
import de.fhg.iese.dd.platform.api.shared.services.IClientMediaItemService;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagResponse;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.*;
import de.fhg.iese.dd.platform.business.grapevine.events.like.LikeCommentConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.like.LikeCommentRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.like.UnlikeCommentConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.like.UnlikeCommentRequest;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.CommentCreationNotAllowedException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.CommentNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.services.*;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagConfirmation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentCreationRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionFirstResponder;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionWorker;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IDocumentItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

@RestController
@RequestMapping("/grapevine/comment/event")
@Api(tags = {"grapevine.comment.events"})
class CommentEventController extends BaseController {

    @Autowired
    private ICommentService commentService;
    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private IDocumentItemService documentItemService;
    @Autowired
    private IClientMediaItemService clientMediaItemService;
    @Autowired
    private IClientDocumentItemService clientDocumentItemService;
    @Autowired
    private IPostService postService;
    @Autowired
    private IGroupService groupService;
    @Autowired
    private ISuggestionService suggestionService;
    @Autowired
    private GrapevineClientModelMapper grapevineModelMapper;
    @Autowired
    private ILikeCommentService likeCommentService;

    @ApiOperation(value = "Creates a new Comment",
            notes = "Creates a new comment with or without pictures.\n" +
                    "If the field `commentsAllowed` of the post is false only the creator of the post can " +
                    "create comments.")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.NOT_MEMBER_OF_GROUP})
    @ApiException(value = ClientExceptionType.COMMENT_CREATION_NOT_ALLOWED,
            reason = "If the field `commentsAllowed` of the post is false only the creator of the post " +
                    "can create comments.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("commentCreateRequest")
    public ClientCommentCreateConfirmation onCommentCreateRequest(
            @Valid @RequestBody ClientCommentCreateRequest clientRequest) {

        final Person currentUser = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        checkVerificationStatusRestriction(ContentCreationRestrictionFeature.class, currentUser, appVariant);
        final Post post = postService.findByIdFilteredByHiddenForOtherHomeAreasAndGroupContentVisibility(
                clientRequest.getPostId(), currentUser, appVariant);
        if (post.getGroup() != null) {
            groupService.checkIsGroupMember(post.getGroup(), currentUser);
        }
        if (!post.isCommentsAllowed()) {
            if (!Objects.equals(currentUser, post.getCreator())) {
                throw new CommentCreationNotAllowedException();
            }
        }
        return createCommentOnPost(clientRequest, currentUser, appVariant, post);
    }

    @ApiOperation(value = "Creates a new Comment on a Suggestion by a suggestion worker",
            notes = "Creates a new comment on a suggestion by a suggestion worker. " +
                    "If the suggestion is deleted or withdrawn comments can not be created. " +
                    "The field `commentsAllowed` of the post is ignored.")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuggestionFirstResponder.class, SuggestionWorker.class})
    @PostMapping("suggestion/commentCreateRequest")
    public ClientCommentCreateConfirmation onCommentCreateRequestBySuggestionWorker(
            @Valid @RequestBody ClientCommentCreateRequest clientRequest) {

        final Person currentUser = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantOrNull();

        //we do not care about override deleted, since withdrawn suggestions can not be commented on
        final Suggestion suggestion =
                suggestionService.findSuggestionById(clientRequest.getPostId(), false);

        suggestionService.checkSuggestionRoles(currentUser, suggestion.getTenant());

        return createCommentOnPost(clientRequest, currentUser, appVariant, suggestion);
    }

    private ClientCommentCreateConfirmation createCommentOnPost(ClientCommentCreateRequest clientRequest,
            Person person, AppVariant appVariant, Post post) {

        Comment replyTo = null;
        if (StringUtils.isNotEmpty(clientRequest.getReplyToCommentId())) {
            replyTo = commentService.findCommentByIdIncludingDeleted(clientRequest.getReplyToCommentId());
        }

        final CommentCreateRequest request = CommentCreateRequest.builder()
                .creator(person)
                .appVariant(appVariant)
                .post(post)
                .replyTo(replyTo)
                .text(clientRequest.getText())
                .temporaryMediaItemsToAdd(mediaItemService
                        .findTemporaryItemsById(person, clientRequest.getTemporaryMediaItemIds()))
                .temporaryDocumentItemsToAdd(documentItemService.findTemporaryItemsByIdUnordered(person,
                        clientRequest.getTemporaryDocumentItemIds()))
                .build();

        final CommentCreateConfirmation createConfirmation =
                notifyAndWaitForReply(CommentCreateConfirmation.class, request);

        return new ClientCommentCreateConfirmation(
                grapevineModelMapper.toClientComment(createConfirmation.getComment(), false));
    }

    @ApiOperation(value = "Deletes a Comment",
            notes = "Deletes an existing Comment. Only the creator can delete the comment.")
    @ApiExceptions({
            ClientExceptionType.COMMENT_ACTION_NOT_ALLOWED,
            ClientExceptionType.COMMENT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("commentDeleteRequest")
    public ClientCommentDeleteConfirmation onCommentDeleteRequest(
            @Valid @RequestBody ClientCommentDeleteRequest request) {

        Comment comment = commentService.findCommentByIdNotDeleted(request.getCommentId());
        Person person = getCurrentPersonNotNull();

        if (!person.equals(comment.getCreator())) {
            throw new CommentActionNotAllowedException("Only the creator can delete the comment");
        }

        CommentDeleteRequest deleteRequest = new CommentDeleteRequest(comment);

        CommentDeleteConfirmation deleteConfirmation =
                notifyAndWaitForReply(CommentDeleteConfirmation.class, deleteRequest);
        return new ClientCommentDeleteConfirmation(deleteConfirmation.getComment().getId());
    }

    @ApiOperation(value = "Deletes all own comments for a given post")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("commentDeleteAllForPostRequest")
    public void onCommentDeleteAllForPostRequest(
            @Valid @RequestBody ClientCommentDeleteAllForPostRequest request) {

        final Person person = getCurrentPersonNotNull();

        // We need the unfiltered post function, because we are accessing posts which are not available to the user anymore
        final Post post = postService.findPostByIdNotDeleted(request.getPostId());

        notify(new CommentDeleteAllForPostRequest(post, person));
    }

    @ApiOperation(value = "Updates an existing Comment",
            notes = "Updates an existing Comment. Only text and images can be updated.")
    @ApiExceptions({
            ClientExceptionType.COMMENT_ACTION_NOT_ALLOWED,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.COMMENT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("commentChangeRequest")
    public ClientCommentChangeConfirmation onCommentChangeRequest(
            @Valid @RequestBody ClientCommentChangeRequest request) {

        if (request.isUseEmptyFields()) {
            if (StringUtils.isEmpty(request.getText())
                    && CollectionUtils.isEmpty(request.getMediaItemPlaceHolders())) {
                throw new InvalidEventAttributeException("The attributes {}  may not be empty/null!",
                        "text or mediaItemPlaceHolders");
            }
        }

        Comment comment = commentService.findCommentByIdNotDeleted(request.getCommentId());
        Person person = getCurrentPersonNotNull();

        if (!person.equals(comment.getCreator())) {
            throw new CommentActionNotAllowedException("Only the creator can edit the comment");
        }

        CommentChangeRequest changeRequest = CommentChangeRequest.builder()
                .comment(comment)
                .appVariant(getAppVariantNotNull())
                .text(request.getText())
                .useEmptyFields(request.isUseEmptyFields())
                .imageHolders(clientMediaItemService.toItemPlaceHolders(
                        request.getMediaItemPlaceHolders(), comment.getImages(), person))
                .documentHolders(clientDocumentItemService.toItemPlaceHolders(
                        request.getDocumentItemPlaceHolders(), comment.getAttachments(), person))
                .build();
        CommentChangeConfirmation changeConfirmation =
                notifyAndWaitForReply(CommentChangeConfirmation.class, changeRequest);

        Comment changedComment = changeConfirmation.getComment();
        Set<Comment> likedComments = likeCommentService.filterLikedComments(person, Collections.singletonList(changedComment));
        return new ClientCommentChangeConfirmation(
                grapevineModelMapper.toClientComment(changedComment, likedComments.contains(changedComment)));
    }

    @ApiOperation(value = "Likes a comment",
            notes = "Likes an existing comment. It is possible to like your own comments. If a comment is already liked, it returns a CommentAlreadyLikedException.")
    @ApiExceptions({
            ClientExceptionType.COMMENT_NOT_FOUND,
            ClientExceptionType.COMMENT_ALREADY_LIKED})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("likeCommentRequest")
    public ClientLikeCommentConfirmation onLikeCommentRequest(
            @Valid @RequestBody ClientLikeCommentRequest request) {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();
        final Comment comment = commentService.findCommentByIdNotDeleted(request.getCommentId());
        try {
            postService.findByIdFilteredByHiddenForOtherHomeAreasAndGroupContentVisibility(comment.getPost().getId(),
                    person, appVariant);
        } catch (PostNotFoundException e) {
            throw new CommentNotFoundException(comment.getId());
        }

        final LikeCommentRequest likeCommentRequest = LikeCommentRequest.builder()
                .comment(comment)
                .liker(person)
                .build();

        LikeCommentConfirmation likeCommentConfirmation = notifyAndWaitForReply(LikeCommentConfirmation.class, likeCommentRequest);

        return new ClientLikeCommentConfirmation(grapevineModelMapper.toClientComment(likeCommentConfirmation.getComment(), true));
    }

    @ApiOperation(value = "Unlikes a comment",
            notes = "Unlikes an existing comment. If a comment is already unliked, it returns a CommentAlreadyUnlikedException.")
    @ApiExceptions({
            ClientExceptionType.COMMENT_NOT_FOUND,
            ClientExceptionType.COMMENT_ALREADY_UNLIKED,
            ClientExceptionType.COMMENT_NOT_LIKED})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("unlikeCommentRequest")
    public ClientUnlikeCommentConfirmation onUnlikeCommentRequest(
            @Valid @RequestBody ClientUnlikeCommentRequest request) {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();
        final Comment comment = commentService.findCommentByIdNotDeleted(request.getCommentId());
        try {
            postService.findByIdFilteredByHiddenForOtherHomeAreasAndGroupContentVisibility(comment.getPost().getId(),
                    person, appVariant);
        } catch (PostNotFoundException e) {
            throw new CommentNotFoundException(comment.getId());
        }

        final UnlikeCommentRequest unlikeCommentRequest = UnlikeCommentRequest.builder()
                .comment(comment)
                .unliker(person)
                .build();

        UnlikeCommentConfirmation unlikeCommentConfirmation = notifyAndWaitForReply(UnlikeCommentConfirmation.class, unlikeCommentRequest);

        return new ClientUnlikeCommentConfirmation(grapevineModelMapper.toClientComment(unlikeCommentConfirmation.getComment(), false));
    }

    @ApiOperation(value = "Flags a comment as inappropriate",
            notes = "'commentId' is the id of the comment, while optional 'comment' is the reason for flagging this entity")
    @ApiExceptions({
            ClientExceptionType.COMMENT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("commentFlagRequest")
    public ClientUserGeneratedContentFlagResponse commentFlagRequest(
            @Valid @RequestBody ClientCommentFlagRequest request) {

        final Person flagCreator = getCurrentPersonNotNull();
        final Comment comment = commentService.findCommentByIdNotDeleted(request.getCommentId());

        final CommentFlagRequest flagRequest = CommentFlagRequest.builder()
                .entity(comment)
                .entityAuthor(comment.getCreator())
                .entityDescription(String.format("Comment '%s' on %s '%.50s' in '%s': ",
                        comment.getText(),
                        comment.getPost().getPostType(),
                        comment.getPost().getText(),
                        comment.getPost().getGeoAreas()))
                .flagCreator(flagCreator)
                .tenant(comment.getPost().getTenant())
                .comment(request.getComment())
                .allowMultipleFlagsOfSameEntity(false)
                .build();

        final UserGeneratedContentFlagConfirmation response =
                notifyAndWaitForReply(UserGeneratedContentFlagConfirmation.class, flagRequest);
        return new ClientUserGeneratedContentFlagResponse(response.getCreatedFlag());
    }

}
