/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientComment;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientSuggestionCategory;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientSuggestionExtended;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonReference;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.InvalidFilterCriteriaException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.InvalidSortingCriterionException;
import de.fhg.iese.dd.platform.business.grapevine.services.*;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionStatus;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionFirstResponder;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionWorker;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/grapevine/suggestion")
@Api(tags = { "grapevine.suggestion" })
class SuggestionController extends BaseController {

    @Autowired
    private IPersonService personService;
    @Autowired
    private ISuggestionService suggestionService;
    @Autowired
    private ISuggestionCategoryService suggestionCategoryService;
    @Autowired
    private ICommentService commentService;
    @Autowired
    private IPostInteractionService postInteractionService;
    @Autowired
    private ILikeCommentService likeCommentService;
    @Autowired
    private GrapevineClientModelMapper grapevineModelMapper;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;

    @ApiOperation(value = "Returns all suggestion categories")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping(value = "/suggestionCategory")
    public List<ClientSuggestionCategory> getSuggestionCategories() {

        return suggestionCategoryService.findAllSuggestionCategoriesInDataInitOrder()
                .stream()
                .map(grapevineModelMapper::toClientSuggestionCategory)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Return the suggestion category with the given id")
    @ApiExceptions(ClientExceptionType.SUGGESTION_CATEGORY_NOT_FOUND)
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping(value = "/suggestionCategory/{suggestionCategoryId}")
    public ClientSuggestionCategory getSuggestionCategoryById(@PathVariable String suggestionCategoryId) {

        return grapevineModelMapper.toClientSuggestionCategory(
                suggestionCategoryService.findSuggestionCategoryById(suggestionCategoryId));
    }

    @ApiOperation(value = "Returns all potential suggestion workers",
            notes = "Returns all potential suggestion workers for the given suggestion. " +
                    "These are all persons with roles SuggestionFirstResponder or SuggestionWorker for the tenant of the suggestion")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.NOT_AUTHORIZED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuggestionFirstResponder.class, SuggestionWorker.class})
    @GetMapping(value = "/{suggestionId}/potentialWorker")
    public List<ClientPersonReference> getPotentialSuggestionWorkers(@PathVariable String suggestionId) {

        Person person = getCurrentPersonNotNull();
        Suggestion suggestion = suggestionService.findSuggestionByIdIncludingOverrideDeleted(suggestionId, false);
        suggestionService.checkSuggestionRoles(person, suggestion.getTenant());

        return getRoleService().getAllPersonsWithRoleForEntity(
                        Arrays.asList(SuggestionFirstResponder.class, SuggestionWorker.class),
                        suggestion.getTenant()).stream()
                .filter(Person::isNotDeleted)
                .map(personClientModelMapper::createClientPersonReference)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Returns all extended suggestions",
            notes = """
                    Returns all extended suggestions, containing more details than the standard posts. The response is paged.

                    | sorting criteria | attribute used for filtering and sorting | returned posts are subtype of | direction | default start if null | default end if null |
                    | ---- | ---- | ---- | ---- | ---- | ---- |
                    | CREATED | created | Post | DESC | MIN | MAX |
                    | LAST_SUGGESTION_ACTIVITY | lastSuggestionActivity | Suggestion | DESC | MIN | MAX |
                    | LAST_ACTIVITY | lastActivity | Post | DESC | MIN | MAX |
                    """)
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID,
            ClientExceptionType.INVALID_SORTING_CRITERION,
            ClientExceptionType.INVALID_FILTER_CRITERIA,
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuggestionFirstResponder.class, SuggestionWorker.class})
    @GetMapping(value = "/")
    public Page<ClientSuggestionExtended> getExtendedSuggestions(

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "Start time from which the suggestions should be returned, if it is 0 then all since 01.01.1970 will be returned. " +
                    "If the suggestions are sorted by the last activity, the parameter refers to the time of the last comment instead of the time when the suggestion was created.",
                    defaultValue = "0") long startTime,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "End time from which the suggestions should be returned, if 0 all suggestions between startTime and now will be returned. " +
                    "If the suggestions are sorted by the last activity, the parameter refers to the time of the last comment instead of the time when the suggestion was created.",
                    defaultValue = "0") long endTime,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of suggestions",
                    defaultValue = "0") int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of suggestions returned in a page, if it is 0 no pagination is applied",
                    defaultValue = "10") int count,

            @RequestParam(required = false, defaultValue = "CREATED")
            @ApiParam(value = "Sorting criterion. When CREATED, newest suggestions are listed first. When LAST_SUGGESTION_ACTIVITY, suggestions " +
                    "with the most recent suggestion activity are listed first. Considered as an suggestion activity is the creation, update or " +
                    "deletion of a comment or a new chat message. When LAST_ACTIVITY, suggestions with the most recent change in terms of comments " +
                    "are listed first. Difference between this two options is the consideration of new chat messages.",
                    defaultValue = "CREATED",
                    allowableValues = "CREATED, LAST_SUGGESTION_ACTIVITY, LAST_ACTIVITY") PostSortCriteria sortBy,

            @RequestParam(required = false)
            @ApiParam(value = "Filter criterion. When a personId is supplied, only suggestions with the given person as an assigned suggestion worker will be listed.\n" +
                    "Can not be applied with 'unassigned' = true! This will throw an InvalidFilterCriteriaException.")
                    String containedWorkerId,

            @RequestParam(required = false, defaultValue = "false")
            @ApiParam(value = "Filter criterion. When supplied, only suggestions without assigned suggestion workers will be listed.\n" +
                    "Can not be applied with containsWorker! This will throw an InvalidFilterCriteriaException.") boolean unassigned,

            @RequestParam(required = false)
            @ApiParam(value = "Only suggestions with `suggestionStatus` *in* `includedStatus` are returned. If not set, this filter is not applied.")
                    Set<SuggestionStatus> includedStatus,

            @RequestParam(required = false)
            @ApiParam(value = "Only suggestions with `suggestionStatus` *not in* `excludedStatus` are returned. If not set, this filter is not applied.")
                    Set<SuggestionStatus> excludedStatus
            ) {

        if (count == 0) {
            count = Integer.MAX_VALUE;
            page = 0;
        }

        checkPageAndCountValues(page, count);

        if (!sortBy.isAvailableFor(Suggestion.class)) {
            throw new InvalidSortingCriterionException("sortBy {} is not available for type SUGGESTION", sortBy);
        }

        if (StringUtils.isNotEmpty(containedWorkerId) && unassigned) {
            throw new InvalidFilterCriteriaException("Only one filter criterion of 'containsWorker' and 'unassigned' can be applied");
        }

        final Person person = getCurrentPersonNotNull();
        final Set<String> tenantIds = suggestionService.checkSuggestionRolesAndReturnTenantIds(person);

        final String assignedWorkerId;

        if (StringUtils.isNotEmpty(containedWorkerId)) {
            personService.checkPersonExistsById(containedWorkerId);
            assignedWorkerId = containedWorkerId;
        } else {
            assignedWorkerId = null;
        }

        final PagedQuery pagedQuery = PagedQuery.builder()
                .pageNumber(page)
                .elementsPerPage(count)
                .sortCriteria(sortBy)
                .build();
        
        final Page<Suggestion> suggestions =
                suggestionService.findAllSuggestionsInTenantsIncludingOverrideDeleted(tenantIds, startTime,
                        endTime, includedStatus, excludedStatus, assignedWorkerId, unassigned, pagedQuery);

        Set<Post> likedPosts = postInteractionService.filterLikedPosts(person, suggestions.getContent());
        return suggestions.map(suggestion ->
                grapevineModelMapper.toClientSuggestionExtended(suggestion, likedPosts.contains(suggestion)));
    }

    @ApiOperation(value = "Returns one extended suggestion",
            notes = "Returns one extended suggestion, containing more details than the standard posts")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.NOT_AUTHORIZED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuggestionFirstResponder.class, SuggestionWorker.class})
    @GetMapping(value = "/{suggestionId}")
    public ClientSuggestionExtended getExtendedSuggestion(@PathVariable String suggestionId) {

        Person person = getCurrentPersonNotNull();
        Suggestion suggestion = suggestionService.findSuggestionByIdIncludingOverrideDeleted(suggestionId, true);
        suggestionService.checkSuggestionRoles(person, suggestion.getTenant());
        return grapevineModelMapper.toClientSuggestionExtended(suggestion,
                postInteractionService.isPostLiked(person, suggestion));
    }

    @ApiOperation(value = "Returns all comments for the given suggestion",
            notes = "in contrast to /grapevine/comment this method only works for comments, but also returns " +
                    "comments of suggestions that are withdrawn (and, thus as being deleted, " +
                    "not returned by the other endpoint)")
    @ApiExceptions({
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.NOT_AUTHORIZED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuggestionFirstResponder.class, SuggestionWorker.class})
    @GetMapping(value = "/{suggestionId}/comments")
    public List<ClientComment> getSuggestionComments(@PathVariable String suggestionId) {

        Person person = getCurrentPersonNotNull();
        Suggestion suggestion = suggestionService.findSuggestionByIdIncludingOverrideDeleted(suggestionId, false);
        suggestionService.checkSuggestionRoles(person, suggestion.getTenant());

        final List<Comment> comments = commentService.findAllByPostFilteredOrderByCreatedAsc(person, suggestion);

        Set<Comment> likedComments = likeCommentService.filterLikedComments(person, comments);
        return comments.stream()
                .map(comment -> grapevineModelMapper.toClientComment(comment, likedComments.contains(comment)))
                .collect(Collectors.toList());
    }

}
