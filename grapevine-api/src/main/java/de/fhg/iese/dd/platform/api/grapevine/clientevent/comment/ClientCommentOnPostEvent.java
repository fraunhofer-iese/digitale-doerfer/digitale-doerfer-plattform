/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientevent.comment;

import java.util.Collections;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.framework.clientevent.ClientMinimizableEvent;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientComment;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPost;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientCommentOnPostEvent extends ClientBaseEvent implements ClientMinimizableEvent {

    @Getter
    @SuperBuilder
    private static class Minimized extends ClientBaseEvent {

        private final String postId;
        private final String commentId;
        @JsonProperty("post")
        private final Map<String, ClientPostType> type;

    }

    public String getPostId() {
        return post.getId();
    }

    public String getCommentId() {
        return comment.getId();
    }

    private ClientPost post;

    private ClientComment comment;

    @Override
    public Minimized toMinimizedEvent() {
        return Minimized.builder()
                .postId(getPostId())
                .commentId(getCommentId())
                .type(Collections.singletonMap("type", post.getType()))
                .build();
    }

}
