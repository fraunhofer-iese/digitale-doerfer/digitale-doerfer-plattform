/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Johannes Schneider, Balthasar Weitzel, Benjamin Hassenfratz, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.clientmodel;

import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.GeoAreaClientModelMapper;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.AddressClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.UserGeneratedContentFlagClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.grapevine.statistics.PostDayTimeStatistics;
import de.fhg.iese.dd.platform.business.grapevine.statistics.PostGeoAreaStatistics;
import de.fhg.iese.dd.platform.business.grapevine.statistics.PostStatistics;
import de.fhg.iese.dd.platform.business.grapevine.statistics.PostTimeStatistics;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Component
@Log4j2
public class GrapevineClientModelMapper {

    @Autowired
    private PersonClientModelMapper personClientModelMapper;
    @Autowired
    private FileClientModelMapper fileClientModelMapper;
    @Autowired
    private GeoAreaClientModelMapper geoAreaClientModelMapper;
    @Autowired
    private AddressClientModelMapper addressClientModelMapper;
    @Autowired
    private UserGeneratedContentFlagClientModelMapper userGeneratedContentFlagClientModelMapper;
    @Autowired
    private List<IClientPostMappingCustomization<?, ?>> mappingCustomizations;

    public ClientComment toClientComment(Comment comment, Boolean liked) {
        if (comment == null) {
            return null;
        }
        ClientComment clientComment = ClientComment.builder()
                .id(comment.getId())
                .postId(comment.getPost().getId())
                .deleted(comment.isDeleted())
                .creator(personClientModelMapper.createClientPersonReference(comment.getCreator()))
                .attachments(fileClientModelMapper.toClientDocumentItems(comment.getAttachments()))
                .created(comment.getCreated())
                .likeCount(comment.getLikeCount())
                .liked(liked)
                .lastModified(comment.getLastModified())
                .build();

        if (comment.isNotDeleted()) {
            clientComment.setText(comment.getText());
            clientComment.setImages(fileClientModelMapper.toClientMediaItems(comment.getImages()));
        } // else: text and image are "censored"
        if (comment.getReplyTo() != null) {
            clientComment.setReplyTo(
                    personClientModelMapper.createClientPersonReference(comment.getReplyTo().getCreator()));
        }

        return clientComment;
    }

    public ClientComment toClientCommentIncludingDeleted(Comment comment) {
        if (comment == null) {
            return null;
        }
        ClientComment clientComment = toClientComment(comment, null);
        clientComment.setText(comment.getText());
        clientComment.setImages(fileClientModelMapper.toClientMediaItems(comment.getImages()));
        return clientComment;
    }

    public ClientPost toClientPost(Post post, Boolean liked) {

        return post.accept(new Post.Visitor<>() {
            @Override
            public ClientPost whenGossip(Gossip gossip) {

                return new ClientPost(applyCustomizations(toClientGossip(gossip, liked), gossip));
            }

            @Override
            public ClientPost whenNewsItem(NewsItem newsItem) {

                return new ClientPost(applyCustomizations(toClientNewsItem(newsItem, liked), newsItem));
            }

            @Override
            public ClientPost whenSuggestion(Suggestion suggestion) {

                return new ClientPost(applyCustomizations(toClientSuggestion(suggestion, liked), suggestion));
            }

            @Override
            public ClientPost whenSeeking(Seeking seeking) {

                return new ClientPost(applyCustomizations(toClientSeeking(seeking, liked), seeking));
            }

            @Override
            public ClientPost whenOffer(Offer offer) {

                return new ClientPost(applyCustomizations(toClientOffer(offer, liked), offer));
            }

            @Override
            public ClientPost whenHappening(Happening happening) {

                return new ClientPost(applyCustomizations(toClientHappening(happening, liked), happening));
            }

            @Override
            public ClientPost whenSpecialPost(SpecialPost specialPost) {

                return new ClientPost(applyCustomizations(toClientSpecialPost(specialPost, liked), specialPost));
            }

        });
    }

    private <C extends ClientPostBaseEntity, P extends Post> C applyCustomizations(C clientEntity, P post) {

        C result = clientEntity;
        for (IClientPostMappingCustomization<?, ?> rawCustomization : mappingCustomizations) {
            @SuppressWarnings("unchecked")
            IClientPostMappingCustomization<C, P> customization = (IClientPostMappingCustomization<C, P>) rawCustomization;
            if (customization.getRelevantClientPostClass().isAssignableFrom(clientEntity.getClass())) {
                result = customization.customizeMapping(result, post);
            }
        }
        return result;
    }

    public PostType toPostType(ClientPostType clientPostType) {
        if (clientPostType == null) {
            return null;
        }
        return clientPostType.toPostType();
    }

    public ClientTradingCategory toClientTradingCategory(TradingCategory tradingCategory) {
        if (tradingCategory == null) {
            return null;
        }
        return ClientTradingCategory.builder()
                .id(tradingCategory.getId())
                .shortName(tradingCategory.getShortName())
                .displayName(tradingCategory.getDisplayName())
                .build();
    }

    public ClientSuggestionCategory toClientSuggestionCategory(SuggestionCategory suggestionCategory) {
        if (suggestionCategory == null) {
            return null;
        }
        return ClientSuggestionCategory.builder()
                .id(suggestionCategory.getId())
                .shortName(suggestionCategory.getShortName())
                .displayName(suggestionCategory.getDisplayName())
                .orderValue(suggestionCategory.getOrderValue())
                .build();
    }

    public ClientGroup toClientGroup(Group group, GroupMembershipStatus membershipStatus,
            IGroupService.GroupGeoAreaIds groupGeoAreaIds) {
        if (group == null) {
            return null;
        }
        return ClientGroup.builder()
                .id(group.getId())
                .name(group.getName())
                .shortName(group.getShortName())
                .description(group.getDescription())
                .logo(fileClientModelMapper.toClientMediaItem(group.getLogo()))
                .visibility(group.getVisibility())
                .contentVisibility(group.getContentVisibility())
                .accessibility(group.getAccessibility())
                .geoAreaIds(new ArrayList<>(groupGeoAreaIds.getIncludedGeoAreaIds()))
                .deleted(group.isDeleted())
                .memberCount(group.getMemberCount())
                .membershipStatus(membershipStatus == null ? GroupMembershipStatus.NOT_MEMBER : membershipStatus)
                .mainGeoAreaId(groupGeoAreaIds.getMainGeoAreaId())
                .build();
    }

    public List<ClientGroup> toClientGroups(Collection<Group> groups,
            Map<Group, GroupMembershipStatus> groupMembershipStatusByGroup,
            Map<String, IGroupService.GroupGeoAreaIds> groupGeoAreaIdsByGroup) {
        if (CollectionUtils.isEmpty(groups)) {
            return Collections.emptyList();
        }
        return groups.stream()
                .map(group -> toClientGroup(group,
                        groupMembershipStatusByGroup.get(group),
                        groupGeoAreaIdsByGroup.get(group.getId())))
                .collect(Collectors.toList());
    }

    public ClientGroupExtended toClientGroupExtended(Group group, IGroupService.GroupGeoAreas groupGeoAreas) {
        if (group == null) {
            return null;
        }
        final ClientGroupExtended clientGroupExtended = ClientGroupExtended.builder()
                .id(group.getId())
                .name(group.getName())
                .shortName(group.getShortName())
                .logo(fileClientModelMapper.toClientMediaItem(group.getLogo()))
                .visibility(group.getVisibility())
                .contentVisibility(group.getContentVisibility())
                .accessibility(group.getAccessibility())
                .deleted(group.isDeleted())
                .memberCount(group.getMemberCount())
                .tenantId(group.getTenant().getId())
                .build();
        if (groupGeoAreas != null) {
            clientGroupExtended.setIncludedGeoAreas(
                    geoAreaClientModelMapper.toClientGeoAreas(groupGeoAreas.getIncludedGeoAreas(), false));
            clientGroupExtended.setExcludedGeoAreas(
                    geoAreaClientModelMapper.toClientGeoAreas(groupGeoAreas.getExcludedGeoAreas(), false));
        }
        return clientGroupExtended;
    }

    public List<ClientGroupExtended> toClientGroupsExtended(Collection<Group> groups,
            Map<String, IGroupService.GroupGeoAreas> groupGeoAreasByGroupId) {
        if (CollectionUtils.isEmpty(groups)) {
            return Collections.emptyList();
        }
        return groups.stream()
                .map(group -> toClientGroupExtended(group, groupGeoAreasByGroupId.get(group.getId())))
                .collect(Collectors.toList());
    }

    public ClientGroupExtendedDetail toClientGroupExtendedDetail(Group group, IGroupService.GroupGeoAreas groupGeoAreas,
            List<Person> groupMembershipAdmins) {
        return toClientGroupExtendedDetail(group, groupGeoAreas, groupMembershipAdmins, 0, null, null);
    }

    public ClientGroupExtendedDetail toClientGroupExtendedDetail(Group group, IGroupService.GroupGeoAreas groupGeoAreas,
            List<Person> groupMembershipAdmins, long postCount, Long lastPost, Long lastCommentOfPost) {
        if (group == null) {
            return null;
        }
        return ClientGroupExtendedDetail.builder()
                .id(group.getId())
                .name(group.getName())
                .shortName(group.getShortName())
                .logo(fileClientModelMapper.toClientMediaItem(group.getLogo()))
                .visibility(group.getVisibility())
                .contentVisibility(group.getContentVisibility())
                .accessibility(group.getAccessibility())
                .includedGeoAreas(geoAreaClientModelMapper.toClientGeoAreas(groupGeoAreas.getIncludedGeoAreas(), false))
                .excludedGeoAreas(geoAreaClientModelMapper.toClientGeoAreas(groupGeoAreas.getExcludedGeoAreas(), false))
                .deleted(group.isDeleted())
                .memberCount(group.getMemberCount())
                .tenantId(group.getTenant().getId())
                .groupMembershipAdmins(
                        groupMembershipAdmins == null ? Collections.emptyList() : groupMembershipAdmins.stream()
                                .map(personClientModelMapper::createClientPersonReferenceWithEmail)
                                .collect(Collectors.toList()))
                .postCount(postCount)
                .lastPost(lastPost)
                .lastCommentOfPost(lastCommentOfPost)
                .created(group.getCreated())
                .build();
    }

    public ClientGroupMemberExtended toClientGroupMemberExtended(GroupMembership groupMembership) {
        if (groupMembership == null) {
            return null;
        }
        return ClientGroupMemberExtended.builder()
                .member(personClientModelMapper.createClientPersonReferenceWithEmail(groupMembership.getMember()))
                .membershipStatus(groupMembership.getStatus())
                .memberIntroductionText(groupMembership.getMemberIntroductionText())
                .build();
    }

    private ClientGossip toClientGossip(Gossip gossip, Boolean liked) {
        if (gossip == null) {
            return null;
        }
        ClientGossip.ClientGossipBuilder<?, ?> clientGossipBuilder = ClientGossip.builder();
        clientGossipBuilder.liked(liked);
        convertPostAttributes(gossip, clientGossipBuilder);
        return clientGossipBuilder.build();
    }

    private ClientSeeking toClientSeeking(Seeking seeking, Boolean liked) {
        if (seeking == null) {
            return null;
        }
        ClientSeeking.ClientSeekingBuilder<?, ?> clientSeekingBuilder = ClientSeeking.builder();
        clientSeekingBuilder.liked(liked);
        convertTradeAttributes(seeking, clientSeekingBuilder);
        return clientSeekingBuilder.build();
    }

    private ClientOffer toClientOffer(Offer offer, Boolean liked) {
        if (offer == null) {
            return null;
        }
        final ClientOffer.ClientOfferBuilder<?, ?> clientOfferBuilder = ClientOffer.builder();
        clientOfferBuilder.liked(liked);
        convertTradeAttributes(offer, clientOfferBuilder);
        return clientOfferBuilder.build();
    }

    private ClientNewsItem toClientNewsItem(NewsItem newsItem, Boolean liked) {
        if (newsItem == null) {
            return null;
        }
        ClientNewsItem.ClientNewsItemBuilder<?, ?> clientNewsItemBuilder = ClientNewsItem.builder();
        clientNewsItemBuilder.liked(liked);
        convertExternalPostAttributes(newsItem, clientNewsItemBuilder);
        return clientNewsItemBuilder.build();
    }

    private ClientHappening toClientHappening(Happening happening, Boolean liked) {
        if (happening == null) {
            return null;
        }
        ClientHappening.ClientHappeningBuilder<?, ?> clientHappeningBuilder = ClientHappening.builder();
        clientHappeningBuilder.liked(liked);
        convertExternalPostAttributes(happening, clientHappeningBuilder);
        clientHappeningBuilder.organizer(happening.getOrganizer());
        clientHappeningBuilder.startTime(happening.getStartTime());
        clientHappeningBuilder.endTime(happening.getEndTime());
        clientHappeningBuilder.allDayEvent(happening.isAllDayEvent());
        clientHappeningBuilder.participantCount(happening.getParticipantCount());
        clientHappeningBuilder.participated(happening.getParticipated());
        return clientHappeningBuilder.build();
    }

    private ClientSpecialPost toClientSpecialPost(SpecialPost specialPost, Boolean liked) {
        if (specialPost == null) {
            return null;
        }
        ClientSpecialPost.ClientSpecialPostBuilder<?, ?> clientSpecialPostBuilder = ClientSpecialPost.builder();
        clientSpecialPostBuilder.liked(liked);
        convertPostAttributes(specialPost, clientSpecialPostBuilder);
        return clientSpecialPostBuilder.build();
    }

    private ClientSuggestion toClientSuggestion(Suggestion suggestion, Boolean liked) {
        if (suggestion == null) {
            return null;
        }
        ClientSuggestion.ClientSuggestionBuilder<?, ?> clientSuggestionBuilder = ClientSuggestion.builder();
        clientSuggestionBuilder.liked(liked);
        convertPostAttributes(suggestion, clientSuggestionBuilder);
        clientSuggestionBuilder.suggestionStatus(suggestion.getSuggestionStatus());
        clientSuggestionBuilder.suggestionCategory(toClientSuggestionCategory(suggestion.getSuggestionCategory()));
        return clientSuggestionBuilder.build();
    }

    public ClientSuggestionExtended toClientSuggestionExtended(Suggestion suggestion, Boolean liked) {
        if (suggestion == null) {
            return null;
        }
        ClientSuggestionExtended.ClientSuggestionExtendedBuilder<?, ?> clientSuggestionBuilder =
                ClientSuggestionExtended.builder()
                        .liked(liked)
                        .suggestionStatus(suggestion.getSuggestionStatus())
                        .suggestionCategory(toClientSuggestionCategory(suggestion.getSuggestionCategory()))
                        .withdrawn(suggestion.isDeleted())
                        .withdrawReason(suggestion.getWithdrawReason())
                        .withdrawnTimestamp(suggestion.getWithdrawn())
                        .lastSuggestionActivity(suggestion.getLastSuggestionActivity())
                        .internal(suggestion.isInternal());
        convertPostAttributes(suggestion, clientSuggestionBuilder);
        //this null check is only required to deal with old suggestions that do not have an internal chat
        if (suggestion.getInternalWorkerChat() != null) {
            clientSuggestionBuilder.internalWorkerChatId(suggestion.getInternalWorkerChat().getId());
        }
        convertSuggestionWorkers(suggestion, clientSuggestionBuilder);
        convertSuggestionStatusRecords(suggestion, clientSuggestionBuilder);
        return clientSuggestionBuilder.build();
    }

    private void convertSuggestionWorkers(Suggestion from,
            ClientSuggestionExtended.ClientSuggestionExtendedBuilder<?, ?> to) {
        if (CollectionUtils.isEmpty(from.getSuggestionWorkerMappings())) {
            to.suggestionWorkerMappings(Collections.emptyList());
            return;
        }
        List<ClientSuggestionWorkerMapping> clientWorkerMappings = from.getSuggestionWorkerMappings().stream()
                .map(swm -> ClientSuggestionWorkerMapping.builder()
                        .id(swm.getId())
                        .worker(personClientModelMapper.createClientPersonReferenceWithEmail(swm.getWorker()))
                        .created(swm.getCreated())
                        .build())
                .collect(Collectors.toList());
        to.suggestionWorkerMappings(clientWorkerMappings);
    }

    private void convertSuggestionStatusRecords(Suggestion from,
            ClientSuggestionExtended.ClientSuggestionExtendedBuilder<?, ?> to) {
        if (CollectionUtils.isEmpty(from.getSuggestionStatusRecords())) {
            to.suggestionStatusRecords(Collections.emptyList());
            return;
        }
        List<ClientSuggestionStatusRecord> clientStatusRecords = from.getSuggestionStatusRecords().stream()
                .map(ssr -> ClientSuggestionStatusRecord.builder()
                        .id(ssr.getId())
                        .created(ssr.getCreated())
                        .status(ssr.getStatus())
                        .build())
                .collect(Collectors.toList());
        to.suggestionStatusRecords(clientStatusRecords);
    }

    private void convertPostAttributes(Post from, ClientPostBaseEntity.ClientPostBaseEntityBuilder<?, ?> to) {
        to.id(from.getId());
        to.geoAreaIds(from.getGeoAreas().stream()
                .map(GeoArea::getId)
                .sorted()
                .collect(Collectors.toList()));
        to.groupId(BaseEntity.getIdOf(from.getGroup()));
        to.organizationId(BaseEntity.getIdOf(from.getOrganization()));
        to.images(fileClientModelMapper.toClientMediaItems(from.getImages()));
        to.attachments(fileClientModelMapper.toClientDocumentItems(from.getAttachments()));
        to.likeCount(from.getLikeCount());
        to.text(from.getText());
        to.created(from.getCreated());
        to.lastModified(from.getLastModified());
        to.commentCount(from.getCommentCount());
        to.commentsAllowed(from.isCommentsAllowed());
        to.lastActivity(from.getLastActivity());
        to.customAddress(addressClientModelMapper.toClientAddress(from.getCustomAddress()));
        to.creator(personClientModelMapper.createClientPersonReference(from.getCreator()));
        to.hiddenForOtherHomeAreas(from.isHiddenForOtherHomeAreas());
        to.customAttributes(from.getCustomAttributesJson());
    }

    private void convertTradeAttributes(Trade from, ClientTrade.ClientTradeBuilder<?, ?> to) {
        convertPostAttributes(from, to);
        to.tradingCategoryEntity(toClientTradingCategory((from.getTradingCategory())));
        to.tradingStatus(from.getTradingStatus());
        to.date(from.getDate());
    }

    private void convertExternalPostAttributes(ExternalPost externalPost,
            ClientExternalPost.ClientExternalPostBuilder<?, ?> to) {
        convertPostAttributes(externalPost, to);
        to.externalId(externalPost.getExternalId());
        to.url(externalPost.getUrl());
        to.authorName(externalPost.getAuthorName());
        to.categories(externalPost.getCategories());
        if (externalPost.getNewsSource() != null) {
            to.siteName(externalPost.getNewsSource().getSiteName());
            to.sitePicture(fileClientModelMapper.toClientMediaItem(externalPost.getNewsSource().getSiteLogo()));
        }
    }

    public ClientUserGeneratedContentFlagPost toClientUserGeneratedContentFlagPost(
            UserGeneratedContentFlag userGeneratedContentFlag, Post flaggedPost, List<Comment> commentsOfPost) {
        ClientUserGeneratedContentFlagPost clientFlagPost = ClientUserGeneratedContentFlagPost.builder()
                .flagPost(toClientPost(flaggedPost, null))
                .postDeleted(flaggedPost.isDeleted())
                .commentsOfPost(commentsOfPost.stream()
                        .map(this::toClientCommentIncludingDeleted)
                        .collect(Collectors.toList()))
                .build();
        return userGeneratedContentFlagClientModelMapper.setClientUserGeneratedContentFlagDetailAttributes(
                userGeneratedContentFlag,
                clientFlagPost);
    }

    public ClientUserGeneratedContentFlagComment toClientUserGeneratedContentFlagComment(
            UserGeneratedContentFlag userGeneratedContentFlag, Comment flaggedComment, List<Comment> commentsOfPost) {
        ClientUserGeneratedContentFlagComment clientFlagComment = ClientUserGeneratedContentFlagComment.builder()
                .flagComment(toClientCommentIncludingDeleted(flaggedComment))
                .postOfComment(toClientPost(flaggedComment.getPost(), null))
                .postOfCommentDeleted(flaggedComment.getPost().isDeleted())
                .commentsOfPost(commentsOfPost.stream()
                        .map(this::toClientCommentIncludingDeleted)
                        .collect(Collectors.toList()))
                .build();
        return userGeneratedContentFlagClientModelMapper.setClientUserGeneratedContentFlagDetailAttributes(
                userGeneratedContentFlag,
                clientFlagComment);
    }

    public ClientConvenience toClientConvenience(Convenience convenience) {

        return ClientConvenience.builder()
                .id(convenience.getId())
                .name(convenience.getName())
                .description(convenience.getDescription())
                .organizationName(convenience.getOrganizationName())
                .organizationLogo(fileClientModelMapper.toClientMediaItem(convenience.getOrganizationLogo()))
                .url(convenience.getUrl())
                .urlLabel(convenience.getUrlLabel())
                .overviewImage(fileClientModelMapper.toClientMediaItem(convenience.getOverviewImage()))
                .detailImages(fileClientModelMapper.toClientMediaItems(convenience.getDetailImages()))
                .build();
    }

    public ClientNewsSource toClientNewsSource(NewsSource newsSource) {
        if (newsSource == null) {
            return null;
        }

        return ClientNewsSource.builder()
                .id(newsSource.getId())
                .appVariantId(BaseEntity.getIdOf(newsSource.getAppVariant()))
                .siteUrl(newsSource.getSiteUrl())
                .siteName(newsSource.getSiteName())
                .logo(fileClientModelMapper.toClientMediaItem(newsSource.getSiteLogo()))
                .apiManaged(newsSource.isApiManaged())
                .build();
    }

    public ClientVolunteering toClientVolunteering(Organization organization, Collection<String> geoAreaIds) {

        if (organization == null) {
            return null;
        }
        ClientVolunteering clientVolunteering = ClientVolunteering.builder().build();
        BeanUtils.copyProperties(toClientOrganization(organization, geoAreaIds), clientVolunteering);
        return clientVolunteering;
    }

    public ClientOrganization toClientOrganization(Organization organization, Collection<String> geoAreaIds) {

        if (organization == null) {
            return null;
        }
        return ClientOrganization.builder()
                .id(organization.getId())
                .name(organization.getName())
                .locationDescription(organization.getLocationDescription())
                .description(organization.getDescription())
                .overviewImage(fileClientModelMapper.toClientMediaItem(organization.getOverviewImage()))
                .logoImage(fileClientModelMapper.toClientMediaItem(organization.getLogoImage()))
                .detailImages(fileClientModelMapper.toClientMediaItems(organization.getDetailImages()))
                .url(organization.getUrl())
                .phone(organization.getPhone())
                .emailAddress(organization.getEmailAddress())
                .address(addressClientModelMapper.toClientAddress(organization.getAddress()))
                .facts(organization.getFactsJson())
                .persons(organization.getOrganizationPersons().stream()
                        .map(this::toClientOrganizationPerson)
                        .collect(Collectors.toList()))
                .tags(toClientOrganizationTags(organization.getTags().getValues()))
                .geoAreaIds(geoAreaIds.stream()
                        .sorted()
                        .collect(Collectors.toList()))
                .build();
    }

    public ClientOrganizationPerson toClientOrganizationPerson(OrganizationPerson organizationPerson) {
        if (organizationPerson == null) {
            return null;
        }
        return ClientOrganizationPerson.builder()
                .id(organizationPerson.getId())
                .profilePicture(fileClientModelMapper.toClientMediaItem(organizationPerson.getProfilePicture()))
                .name(organizationPerson.getName())
                .position(organizationPerson.getPosition())
                .category(organizationPerson.getCategory())
                .build();
    }

    public List<ClientOrganizationTag> getClientOrganizationTagsOrNull(Post post) {

        if (post != null && post.getOrganization() != null && post.getOrganization().getTags() != null) {
            return toClientOrganizationTags(post.getOrganization().getTags().getValues());
        } else {
            return null;
        }
    }

    public List<ClientOrganizationTag> toClientOrganizationTags(Set<OrganizationTag> organizationTags) {
        if (CollectionUtils.isEmpty(organizationTags)) {
            return Collections.emptyList();
        }
        EnumSet<ClientOrganizationTag> result = EnumSet.noneOf(ClientOrganizationTag.class);
        for (OrganizationTag organizationTag : organizationTags) {
            if (organizationTag == null) {
                log.warn("null organization tag");
                continue;
            }
            switch (organizationTag) {
                case VOLUNTEERING:
                    result.add(ClientOrganizationTag.VOLUNTEERING);
                    break;
                case EMERGENCY_AID:
                    result.add(ClientOrganizationTag.EMERGENCY_AID);
                    break;
                case CIVIL_PROTECTION:
                    result.add(ClientOrganizationTag.CIVIL_PROTECTION);
                    break;
                default:
                    log.warn("unknown organization tag {}", organizationTag);
            }
        }
        return result.stream()
                .sorted()
                .collect(Collectors.toList());
    }

    public Set<OrganizationTag> toOrganizationTags(Collection<ClientOrganizationTag> organizationTags) {
        if (CollectionUtils.isEmpty(organizationTags)) {
            return Collections.emptySet();
        }
        EnumSet<OrganizationTag> result = EnumSet.noneOf(OrganizationTag.class);
        for (ClientOrganizationTag clientOrganizationTag : organizationTags) {
            if (clientOrganizationTag == null) {
                log.warn("null client organization tag");
                continue;
            }
            switch (clientOrganizationTag) {
                case VOLUNTEERING:
                    result.add(OrganizationTag.VOLUNTEERING);
                    break;
                case EMERGENCY_AID:
                    result.add(OrganizationTag.EMERGENCY_AID);
                    break;
                case CIVIL_PROTECTION:
                    result.add(OrganizationTag.CIVIL_PROTECTION);
                    break;
                default:
                    log.warn("unknown client organization tag {}", clientOrganizationTag);
            }
        }
        return result;
    }

    public ClientPostStatistics toClientPostStatistics(PostStatistics postStatistics) {
        if (postStatistics == null) {
            return null;
        }
        return ClientPostStatistics.builder()
                .id(postStatistics.getPost().getId())
                .calculationTime(postStatistics.getCalculationTime())
                .viewedOverviewCount(postStatistics.getViewedOverviewCount())
                .viewedDetailCount(postStatistics.getViewedDetailCount())
                .likeCount(postStatistics.getLikeCount())
                .commentCount(postStatistics.getCommentCount())
                .pastDaysStatistics(postStatistics.getPastDaysStatistics().stream()
                        .map(this::toClientPostTimeStatistics)
                        .collect(Collectors.toList()))
                .dayTimeStatistics(postStatistics.getDayTimeStatistics().stream()
                        .map(this::toClientPostDayTimeStatistics)
                        .collect(Collectors.toList()))
                .geoAreaStatistics(postStatistics.getGeoAreaStatistics().stream()
                        .map(this::toClientPostGeoAreaStatistics)
                        .collect(Collectors.toList()))
                .build();
    }

    private ClientPostGeoAreaStatistics toClientPostGeoAreaStatistics(PostGeoAreaStatistics postGeoAreaStatistics) {
        if (postGeoAreaStatistics == null) {
            return null;
        }
        return ClientPostGeoAreaStatistics.builder()
                .geoArea(geoAreaClientModelMapper.toClientGeoArea(postGeoAreaStatistics.getGeoArea(), false))
                .viewedOverviewCount(postGeoAreaStatistics.getViewedOverviewCount())
                .viewedDetailCount(postGeoAreaStatistics.getViewedDetailCount())
                .build();
    }

    private ClientPostDayTimeStatistics toClientPostDayTimeStatistics(PostDayTimeStatistics postDayTimeStatistics) {
        if (postDayTimeStatistics == null) {
            return null;
        }
        return ClientPostDayTimeStatistics.builder()
                .startHour(postDayTimeStatistics.getStartHour())
                .endHour(postDayTimeStatistics.getEndHour())
                .viewedOverviewCount(postDayTimeStatistics.getViewedOverviewCount())
                .viewedDetailCount(postDayTimeStatistics.getViewedDetailCount())
                .build();
    }

    private ClientPostTimeStatistics toClientPostTimeStatistics(PostTimeStatistics postTimeStatistics) {
        if (postTimeStatistics == null) {
            return null;
        }
        return ClientPostTimeStatistics.builder()
                .startTime(postTimeStatistics.getStartTime())
                .endTime(postTimeStatistics.getEndTime())
                .viewedOverviewCount(postTimeStatistics.getViewedOverviewCount())
                .viewedDetailCount(postTimeStatistics.getViewedDetailCount())
                .build();
    }

    public ClientUserGeneratedContentFlagGroup toClientUserGeneratedContentFlagGroup(UserGeneratedContentFlag userGeneratedContentFlag, Group flaggedGroup, GroupMembershipStatus membershipStatus, IGroupService.GroupGeoAreaIds groupGeoAreas) {
        ClientUserGeneratedContentFlagGroup clientFlagGroup = ClientUserGeneratedContentFlagGroup.builder()
                .flagGroup(toClientGroup(flaggedGroup, membershipStatus, groupGeoAreas))
                .build();
        return userGeneratedContentFlagClientModelMapper.setClientUserGeneratedContentFlagDetailAttributes(
                userGeneratedContentFlag,
                clientFlagGroup);
    }

}
