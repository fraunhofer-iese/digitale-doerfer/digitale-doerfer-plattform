/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.framework.*;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.suggestion.*;
import de.fhg.iese.dd.platform.api.grapevine.exceptions.PostActionNotAllowedException;
import de.fhg.iese.dd.platform.business.grapevine.events.PostChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.suggestion.*;
import de.fhg.iese.dd.platform.business.grapevine.services.ISuggestionCategoryService;
import de.fhg.iese.dd.platform.business.grapevine.services.ISuggestionService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentCreationRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.SuggestionCreationFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.SuggestionPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionCategory;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionFirstResponder;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionWorker;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/grapevine/event")
@Api(tags = {"grapevine.suggestion.events"})
public class SuggestionEventController extends BasePostEventController {

    @Autowired
    private ISuggestionService suggestionService;
    @Autowired
    private ISuggestionCategoryService suggestionCategoryService;
    @Autowired
    private IPersonService personService;

    @ApiOperation(value = "Create a new Suggestion",
            notes = "Creates a new Suggestion. " +
                    "Only possible if the feature `de.fhg.iese.dd.dorffunk.suggestion.creation` is enabled for " +
                    "the requesting app variant and user.\n" +
                    "The field `commentsAllowed` of the created suggestion is determined by the flag " +
                    "`publicCommentCreation` of  feature `de.fhg.iese.dd.dorffunk.suggestion`.")
    @ApiExceptions({
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.POST_COULD_NOT_BE_CREATED,
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.SUGGESTION_CATEGORY_NOT_FOUND,
            ClientExceptionType.FEATURE_NOT_ENABLED})
    @ApiException(value = ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT,
            reason = "If the feature 'de.fhg.iese.dd.dorffunk.content.creation-restriction' is enabled " +
                    "for the app variant, users have to have at least one of the allowed verification " +
                    "status, as configured in the feature.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @PostMapping("suggestionCreateRequest")
    public ClientPostCreateConfirmation onSuggestionCreateRequest(
            @Valid @RequestBody ClientSuggestionCreateRequest request) {

        Person currentPerson = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();

        checkMaxTextLength(SuggestionPostTypeFeature.class, currentPerson, appVariant, request::getText);

        //if the feature is not enabled an exception is thrown
        featureService.checkFeatureEnabled(SuggestionCreationFeature.class,
                FeatureTarget.of(currentPerson, appVariant));
        checkVerificationStatusRestriction(ContentCreationRestrictionFeature.class, currentPerson, appVariant);

        SuggestionCategory suggestionCategory = request.getSuggestionCategoryId() == null ?
                suggestionCategoryService.getDefaultSuggestionCategory()
                :
                        suggestionCategoryService.findSuggestionCategoryById(request.getSuggestionCategoryId());

        boolean internal = Boolean.TRUE.equals(request.getInternal());
        if (internal) {
            try {
                suggestionService.checkSuggestionRoles(currentPerson, currentPerson.getTenant());
            } catch (NotAuthorizedException e) {
                throw new NotAuthorizedException(
                        "Creation of internal suggestions is only allowed for suggestion workers", e);
            }
        }
        SuggestionCreateRequest.SuggestionCreateRequestBuilder<?, ?> requestBuilder = SuggestionCreateRequest.builder()
                .suggestionCategory(suggestionCategory)
                .internal(internal);
        setPostCreateByPersonRequestValues(requestBuilder, currentPerson, appVariant, request);

        PostCreateConfirmation<?> createConfirmation =
                notifyAndWaitForReply(PostCreateConfirmation.class, requestBuilder.build());
        Post post = createConfirmation.getPost();
        return ClientPostCreateConfirmation.builder()
                .post(grapevineModelMapper.toClientPost(post, false))
                .organizationTags(grapevineModelMapper.getClientOrganizationTagsOrNull(post))
                .build();
    }

    @ApiOperation(value = "Update an existing Suggestion",
            notes = "Updates an existing Suggestion. Text, images and category can be updated.")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.FILE_ITEM_DUPLICATE_USAGE,
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.POST_ACTION_NOT_ALLOWED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY,
            notes = "Only the creator can update the suggestion.")
    @PostMapping("suggestionChangeRequest")
    public ClientPostChangeConfirmation onSuggestionChangeRequest(
            @Valid @RequestBody ClientSuggestionChangeRequest request) {

        Person currentPerson = getCurrentPersonNotNull();
        AppVariant appVariant = getAppVariantNotNull();
        if (request.isUseEmptyFields()) {
            checkAttributeNotNullOrEmpty(request.getText(), "text");
        }

        checkMaxTextLength(SuggestionPostTypeFeature.class, currentPerson, appVariant, request::getText);

        Suggestion suggestion = suggestionService.findSuggestionById(request.getPostId(), false);
        if (!currentPerson.equals(suggestion.getCreator())) {
            throw new PostActionNotAllowedException("Only the creator can edit the post");
        }

        SuggestionCategory category;
        if (StringUtils.isNotEmpty(request.getSuggestionCategoryId())) {
            //category id is set, so we should use it
            category = suggestionCategoryService.findSuggestionCategoryById(request.getSuggestionCategoryId());
        } else {
            if (request.isUseEmptyFields()) {
                //use empty fields is set, but the suggestion category is empty, so it should be the set to default
                category = suggestionCategoryService.getDefaultSuggestionCategory();
            } else {
                //use empty fields is not set and the suggestion category is empty, so it should be unchanged
                category = null;
            }
        }

        SuggestionChangeRequest.SuggestionChangeRequestBuilder<?, ?> requestBuilder = SuggestionChangeRequest.builder()
                .suggestionCategory(category);

        setPostChangeRequestByPersonValues(requestBuilder, suggestion, currentPerson, appVariant, request);
        PostChangeConfirmation<?> changeConfirmation =
                notifyAndWaitForReply(PostChangeConfirmation.class, requestBuilder.build());

        final Post changedPost = changeConfirmation.getPost();

        return new ClientPostChangeConfirmation(
                grapevineModelMapper.toClientPost(changedPost,
                        postInteractionService.isPostLiked(currentPerson, changedPost)));
    }

    @ApiOperation(value = "Withdraw an existing Suggestion",
            notes = "Withdraws an existing Suggestion. It can still be read by SuggestionFirstResponder and SuggestionWorker afterwards.")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.POST_ACTION_NOT_ALLOWED})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, notes = "Only the creator can withdraw the suggestion.")
    @PostMapping("suggestionWithdrawRequest")
    public ClientSuggestionWithdrawConfirmation onSuggestionWithdrawRequest(
            @Valid @RequestBody ClientSuggestionWithdrawRequest request) {

        Person currentPerson = getCurrentPersonNotNull();

        Suggestion suggestion = suggestionService.findSuggestionById(request.getSuggestionId(), false);

        if (!currentPerson.equals(suggestion.getCreator())) {
            throw new PostActionNotAllowedException("Only the creator can withdraw the suggestion");
        }
        SuggestionWithdrawConfirmation confirmation = notifyAndWaitForReply(SuggestionWithdrawConfirmation.class,
                new SuggestionWithdrawRequest(suggestion, request.getReason()));
        return new ClientSuggestionWithdrawConfirmation(confirmation.getSuggestion().getId());
    }

    @ApiOperation(value = "Change the status of a Suggestion",
            notes = """
                    Changes the status of a suggestion. Currently these transitions are possible:
                    OPEN -> IN_PROGRESS, ON_HOLD, DONE
                    IN_PROGRESS -> ON_HOLD, DONE
                    ON_HOLD -> IN_PROGRESS, DONE
                    DONE -> IN_PROGRESS""")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.NOT_AUTHORIZED,
            ClientExceptionType.SUGGESTION_STATUS_CHANGE_INVALID})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuggestionFirstResponder.class, SuggestionWorker.class})
    @PostMapping("suggestionStatusChangeRequest")
    public ClientSuggestionStatusChangeConfirmation onSuggestionStatusChangeRequest(
            @Valid @RequestBody ClientSuggestionStatusChangeRequest request) {

        Person currentPerson = getCurrentPersonNotNull();

        Suggestion suggestion =
                suggestionService.findSuggestionByIdIncludingOverrideDeleted(request.getSuggestionId(), true);

        suggestionService.checkSuggestionRoles(currentPerson, suggestion.getTenant());

        SuggestionStatusChangeConfirmation confirmation = notifyAndWaitForReply(
                SuggestionStatusChangeConfirmation.class,
                new SuggestionStatusChangeRequest(suggestion, request.getNewStatus(), currentPerson));
        final Suggestion changedSuggestion = confirmation.getChangedSuggestion();
        return new ClientSuggestionStatusChangeConfirmation(
                grapevineModelMapper.toClientSuggestionExtended(changedSuggestion,
                        postInteractionService.isPostLiked(currentPerson, changedSuggestion)));
    }

    @ApiOperation(value = "Change the category of a Suggestion",
            notes = "Changes the category of a suggestion.")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.SUGGESTION_CATEGORY_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuggestionFirstResponder.class, SuggestionWorker.class})
    @PostMapping("suggestionCategoryChangeRequest")
    public ClientSuggestionCategoryChangeConfirmation onSuggestionCategoryChangeRequest(
            @Valid @RequestBody ClientSuggestionCategoryChangeRequest request) {

        Person currentPerson = getCurrentPersonNotNull();
        SuggestionCategory newSuggestionCategory =
                suggestionCategoryService.findSuggestionCategoryById(request.getNewSuggestionCategoryId());
        Suggestion suggestion =
                suggestionService.findSuggestionByIdIncludingOverrideDeleted(request.getSuggestionId(), true);

        suggestionService.checkSuggestionRoles(currentPerson, suggestion.getTenant());

        final Suggestion suggestionToReturn;
        if (suggestion.getSuggestionCategory().equals(newSuggestionCategory)) {
            //category unchanged
            suggestionToReturn = suggestion;
        } else {
            SuggestionCategoryChangeConfirmation confirmation = notifyAndWaitForReply(
                    SuggestionCategoryChangeConfirmation.class,
                    new SuggestionCategoryChangeRequest(suggestion, newSuggestionCategory, currentPerson));
            suggestionToReturn = confirmation.getChangedSuggestion();
        }
        return new ClientSuggestionCategoryChangeConfirmation(
                grapevineModelMapper.toClientSuggestionExtended(suggestionToReturn,
                        postInteractionService.isPostLiked(currentPerson, suggestionToReturn)));
    }

    @ApiOperation(value = "Add a worker to a Suggestion",
            notes = "Adds a worker to a suggestion. The worker has to have the role SuggestionWorker for the tenant of the suggestion.")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.SUGGESTION_WORKER_ALREADY_WORKING_ON_SUGGESTION})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuggestionFirstResponder.class, SuggestionWorker.class})
    @PostMapping("suggestionWorkerAddRequest")
    public ClientSuggestionWorkerAddConfirmation onSuggestionWorkerAddRequest(
            @Valid @RequestBody ClientSuggestionWorkerAddRequest request) {

        Person currentPerson = getCurrentPersonNotNull();
        Suggestion suggestion =
                suggestionService.findSuggestionByIdIncludingOverrideDeleted(request.getSuggestionId(), true);

        suggestionService.checkSuggestionRoles(currentPerson, suggestion.getTenant());

        Person newWorker = personService.findPersonById(request.getWorkerPersonId());

        suggestionService.checkNewWorker(newWorker, suggestion.getTenant());

        SuggestionWorkerAddConfirmation confirmation = notifyAndWaitForReply(SuggestionWorkerAddConfirmation.class,
                SuggestionWorkerAddRequest.builder()
                        .suggestion(suggestion)
                        .worker(newWorker)
                        .suggestionWorkerAddInitiator(currentPerson)
                        .build());

        final Suggestion changedSuggestion = confirmation.getSuggestion();
        return new ClientSuggestionWorkerAddConfirmation(
                grapevineModelMapper.toClientSuggestionExtended(changedSuggestion,
                        postInteractionService.isPostLiked(currentPerson, changedSuggestion)));
    }

    @ApiOperation(value = "Remove current user as worker from a Suggestion",
            notes = "Removes the current user as worker from a suggestion. The worker has to be an active worker of the suggestion.")
    @ApiExceptions({
            ClientExceptionType.POST_INVALID,
            ClientExceptionType.POST_NOT_FOUND,
            ClientExceptionType.SUGGESTION_WORKER_NOT_WORKING_ON_SUGGESTION})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuggestionFirstResponder.class, SuggestionWorker.class})
    @PostMapping("suggestionWorkerRemoveRequest")
    public ClientSuggestionWorkerRemoveConfirmation onSuggestionWorkerRemoveRequest(
            @Valid @RequestBody ClientSuggestionWorkerRemoveRequest request) {

        Person currentPerson = getCurrentPersonNotNull();
        Suggestion suggestion =
                suggestionService.findSuggestionByIdIncludingOverrideDeleted(request.getSuggestionId(), true);

        suggestionService.checkSuggestionRoles(currentPerson, suggestion.getTenant());

        SuggestionWorkerRemoveConfirmation confirmation =
                notifyAndWaitForReply(SuggestionWorkerRemoveConfirmation.class,
                        SuggestionWorkerRemoveRequest.builder()
                                .suggestion(suggestion)
                                .workerToRemove(currentPerson)
                                .build());
        final Suggestion changedSuggestion = confirmation.getSuggestion();
        return new ClientSuggestionWorkerRemoveConfirmation(
                grapevineModelMapper.toClientSuggestionExtended(changedSuggestion,
                        postInteractionService.isPostLiked(currentPerson, changedSuggestion)));
    }

}
