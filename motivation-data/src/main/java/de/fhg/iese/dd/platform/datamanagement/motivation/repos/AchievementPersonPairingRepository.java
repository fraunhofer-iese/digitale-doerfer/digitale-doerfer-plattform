/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.motivation.repos;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public interface AchievementPersonPairingRepository extends JpaRepository<AchievementPersonPairing, String> {

    //The according auto generated query (countByPersonAndAchievementLevelId) throws an internal exception, replaced with manual query
    @Query(value = "select count(a) "
            + " from AchievementPersonPairing a "
            + " where a.person = :achiever "
            + " and a.achievementLevel.id in :achievementLevelIds ")
    long countAchievedLevels(@Param("achiever") Person person, @Param("achievementLevelIds") Collection<String> achievementLevelIds);

    //The according auto generated query (findAllByPersonAndAchievementLevelIdOrderByTimestampDesc) throws an internal exception, replaced with manual query
    @Query(value = "select a "
            + " from AchievementPersonPairing a "
            + " where a.person = :achiever "
            + " and a.achievementLevel.id in :achievementLevelIds "
            + " order by a.timestamp desc")
    List<AchievementPersonPairing> findAllAchievedLevelsOrderByTimestampDesc(@Param("achiever") Person person, @Param("achievementLevelIds") Collection<String> achievementLevelIds);

    List<AchievementPersonPairing> findAllByPersonOrderByTimestampDesc(Person person);

    AchievementPersonPairing findByPersonAndAchievementLevel(Person person, AchievementLevel achievementLevel);

    @Transactional(readOnly = false)
    void deleteAllByAchievementLevelId(String achievementLevelId);

    @Transactional(readOnly = false)
    void deleteAllByPerson(Person person);

}
