/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.motivation.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AccountEntryType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Table(indexes = {
        @Index(columnList = "subjectName,subjectId,subjectMarker"),
        @Index(columnList = "account_id,subjectName,subjectId,subjectMarker"),
})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountEntry extends BaseEntity {

    @ManyToOne(optional = false)
    private Account account;

    @Enumerated(EnumType.STRING)
    private AccountEntryType accountEntryType;

    @Column
    private int amount;

    @ManyToOne
    private Account partner;

    @Column
    private String postingText;

    /**
     * Used to differentiate different bookings that were done for the same
     * subject
     */
    @Column
    private String subjectMarker;

    @Column
    private String subjectName;

    @Column(length = 36)
    private String subjectId;

    @Column
    private long timestamp;

    public boolean isReservation(){
        return accountEntryType == AccountEntryType.RESERVATION_MINUS ||
               accountEntryType == AccountEntryType.RESERVATION_PLUS;
    }

    public AccountEntry withCreated(long created){
        this.created = created;
        return this;
    }

    @Override
    public String toString() {
        return "AccountEntry ["
                + "id=" + id
                + ", account=" + account
                + ", accountEntryType=" + accountEntryType
                + ", amount=" + amount
                + ", partner=" + partner
                + ", postingText=" + postingText
                + ", timestamp=" + timestamp + "]";
    }

}
