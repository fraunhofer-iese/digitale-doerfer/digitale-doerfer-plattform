/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Matthias Gerbershagen
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.motivation.config;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.Setter;

@Validated
@Configuration
@ConfigurationProperties(prefix="dd-platform.score")
@Getter
@Setter
public class ScoreConfig {

    /**
     * Global credit limit for all score accounts. No reservations or bookings
     * beyond this limit are possible. Has to be be >= 0
     */
    @NotNull
    @Max(value=0, message="Only negative or zero credit limit possible")
    private int globalCreditLimit;

    @NotNull
    @Min(value = 0, message = "Only positive or zero initial amount possible")
    private int initialAmountTenantAccounts;

    @NotNull
    @Min(value=0, message="Only positive or zero initial amount possible")
    private int initialAmountPersonAccounts;

}
