/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.motivation.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Table(
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"person_id", "achievement_level_id"})},
        indexes = {
                @Index(columnList = "person_id, achievement_level_id, timestamp")
        })
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class AchievementPersonPairing extends BaseEntity {

    @ManyToOne(optional = false)
    private Person person;

    @Column
    private long timestamp;

    @OneToOne
    @JoinColumn(nullable = false)
    private AchievementLevel achievementLevel;

    public AchievementPersonPairing withConstantId() {
        super.generateConstantId(person, achievementLevel);
        return this;
    }

    @Override
    public String toString() {
        return "AchievementPersonPairing ["
                + "id=" + id
                + ", person=" + person
                + ", timestamp=" + timestamp
                + ", achievementLevel=" + achievementLevel + "]";
    }

}
