/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.motivation.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AppAccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "account_community")
public class TenantAccount extends Account {

    @ManyToOne(optional = false)
    private Tenant owner;

    @Enumerated(EnumType.STRING)
    private AppAccountType appAccountType;

    @Override
    public String toString() {
        return "TenantAccount ["
                + "id=" + id
                + ", owner=" + owner
                + ", appAccountType=" + appAccountType + "]";
    }

}
