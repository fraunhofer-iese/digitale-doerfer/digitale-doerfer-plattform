/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2018 Balthasar Weitzel, Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.processors;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryCreatedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForTransportConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryStatusChangedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeBundleForSelectionEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeSelectRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCancelRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCreatedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportUndoPickupRequest;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportAlternativeNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.logistics.services.IParcelAddressService;
import de.fhg.iese.dd.platform.business.logistics.services.ITrackingCodeService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAlternativeService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.ConnectDeliveryWithSellingPointRequest;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.DeliveryConnectedWithSellingPointConfirmation;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.UpdateSellingPointForDeliveryConfirmation;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.UpdateSellingPointForDeliveryRequest;
import de.fhg.iese.dd.platform.business.mobilekiosk.exceptions.DeliveryAlreadyConnectedException;
import de.fhg.iese.dd.platform.business.mobilekiosk.exceptions.DeliveryToSellingPointNotFoundException;
import de.fhg.iese.dd.platform.business.mobilekiosk.exceptions.SellingPointNotAvailableForSelectionException;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.IDeliveryToSellingPointService;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.IParcelLabelService;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.ISellingPointService;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonNotFoundException;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportKind;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.DeliveryToSellingPoint;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.roles.SellingVehicleDriver;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_PREFERRED)
public class DeliveryToSellingPointEventProcessor extends BaseEventProcessor {

    @Autowired
    private IDeliveryToSellingPointService deliveryToSellingPointService;

    @Autowired
    private ISellingPointService sellingPointService;

    @Autowired
    private ITransportAssignmentService transportAssignmentService;

    @Autowired
    private ITransportAlternativeService transportAlternativeService;

    @Autowired
    private IDeliveryService deliveryService;

    @Autowired
    private IParcelAddressService parcelAddressService;

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IParcelLabelService parcelLabelService;

    @Autowired
    private ITrackingCodeService trackingCodeService;

    @EventProcessing
    private DeliveryConnectedWithSellingPointConfirmation handleConnectDeliveryWithSellingPointRequest(ConnectDeliveryWithSellingPointRequest request) {

        if (deliveryToSellingPointService.existsForDelivery(request.getDelivery())) {
            throw new DeliveryAlreadyConnectedException();
        }
        if (!sellingPointService.isSellingPointAvailableForSelection(request.getSellingPoint())) {
            throw new SellingPointNotAvailableForSelectionException();
        }

        DeliveryToSellingPoint deliveryToSellingPoint =
                deliveryToSellingPointService.store(DeliveryToSellingPoint.builder()
                    .delivery(request.getDelivery())
                    .sellingPoint(request.getSellingPoint()).build());

        Delivery delivery = createParcelLabel(deliveryToSellingPoint);
        updateDeliveryTimeAndAddressFromSellingPoint(delivery, request.getSellingPoint());

        return DeliveryConnectedWithSellingPointConfirmation.builder()
                .delivery(delivery)
                .sellingPoint(request.getSellingPoint())
                .build();
    }

    @EventProcessing
    private BaseEvent handleDeliveryStatusChangedEvent(DeliveryStatusChangedEvent request) {

        if (!deliveryToSellingPointService.existsForDelivery(request.getDelivery())) {
            return null;
        }

        if (request.getNewStatus() == DeliveryStatus.WAITING_FOR_PICKUP &&
                request.getOldStatus() == DeliveryStatus.IN_DELIVERY) {
            //undo pickup -> cancel assignment
            TransportAssignment oldTransportAssignment =
                    transportAssignmentService.getLatestTransportAssignment(request.getDelivery());
            return new TransportAssignmentCancelRequest(oldTransportAssignment.getCarrier(), oldTransportAssignment,
                    request.getDelivery(), "Assignment to different selling vehicle");
        } else if (request.getNewStatus() == DeliveryStatus.IN_DELIVERY) {
            //updated -> return confirmation
            DeliveryToSellingPoint deliveryToSellingPoint =
                    deliveryToSellingPointService.findByDelivery(request.getDelivery());
            return DeliveryConnectedWithSellingPointConfirmation.builder()
                    .delivery(request.getDelivery())
                    .sellingPoint(deliveryToSellingPoint.getSellingPoint())
                    .build();
        }
        return null;
    }

    @EventProcessing
    private TransportAlternativeSelectRequest handleTransportAlternativeBundleForSelectionEvent(TransportAlternativeBundleForSelectionEvent request) {

        if (!deliveryToSellingPointService.existsForDelivery(request.getTransportAlternativeBundle().getDelivery())) {
            return null;
        }
        //for selection -> assign to driver
        TransportAlternativeBundle transportAlternativeBundle = request.getTransportAlternativeBundle();
        DeliveryToSellingPoint deliveryToSellingPoint =
                deliveryToSellingPointService.findByDelivery(request.getTransportAlternativeBundle().getDelivery());
        TransportAlternative transportAlternativeToAssign = findTransportAlternative(transportAlternativeBundle);
        Person carrier = getDriver(deliveryToSellingPoint.getSellingPoint());

        return new TransportAlternativeSelectRequest(carrier, transportAlternativeToAssign);
    }

    @EventProcessing
    private TransportPickupRequest handleTransportAssignmentCreatedEvent(TransportAssignmentCreatedEvent createdEvent) {

        if(!deliveryToSellingPointService.existsForDelivery(createdEvent.getDelivery())){
            return null;
        }

        return new TransportPickupRequest(createdEvent.getTransportAssignment().getCarrier(),
                createdEvent.getDelivery(),
                createdEvent.getTransportAssignment());
    }

    private TransportAlternative findTransportAlternative(TransportAlternativeBundle transportAlternativeBundle) {
        return transportAlternativeService.findTransportAlternativesByBundle(transportAlternativeBundle)
                .stream().filter(ta -> ta.getKind() == TransportKind.RECEIVER)
                .findFirst()
                .orElseThrow(() -> new TransportAlternativeNotFoundException("for TransportAlternativeBundle "
                        + transportAlternativeBundle.getId()));
    }

    private Person getDriver(SellingPoint sellingPoint) {
        return roleService.getAllPersonsWithRoleForEntity(SellingVehicleDriver.class,
                sellingPoint.getVehicle())
                .stream()
                .findFirst()
                .orElseThrow(
                        () -> new PersonNotFoundException("No driver found for selling vehicle {}", sellingPoint.getVehicle().getId()));
    }

    @EventProcessing
    private List<BaseEvent> handleUpdateSellingPointForDeliveryRequest(UpdateSellingPointForDeliveryRequest request) {

        final SellingPoint sellingPoint = request.getSellingPoint();
        final Delivery delivery = request.getDelivery();

        if (!sellingPointService.isSellingPointAvailableForSelection(sellingPoint)) {
            throw new SellingPointNotAvailableForSelectionException();
        }

        DeliveryToSellingPoint deliveryToSellingPoint = deliveryToSellingPointService.findByDelivery(delivery);
        SellingPoint oldSellingPoint = sellingPointService.refresh(deliveryToSellingPoint.getSellingPoint());
        deliveryToSellingPoint.setSellingPoint(sellingPoint);
        deliveryToSellingPointService.store(deliveryToSellingPoint);

        updateDeliveryTimeAndAddressFromSellingPoint(delivery, sellingPoint);

        DeliveryStatus deliveryStatus = deliveryService.getCurrentStatusRecord(delivery).getStatus();
        if (deliveryStatus == DeliveryStatus.CREATED) {
            //not yet marked as ready for transport -> not assigned -> nothing further to do
            return Arrays.asList(
                    DeliveryConnectedWithSellingPointConfirmation.builder()
                            .delivery(delivery)
                            .sellingPoint(deliveryToSellingPoint.getSellingPoint())
                            .build(),
                    UpdateSellingPointForDeliveryConfirmation.builder()
                            .delivery(delivery)
                            .sellingPoint(sellingPoint)
                            .oldSellingPoint(oldSellingPoint)
                            .admin(request.getAdmin())
                            .build());
        }

        if (!oldSellingPoint.getVehicle().equals(sellingPoint.getVehicle())) {
            //already assigned and picked up and vehicle changed -> cancel "in delivery", cancel assignment, create new assignment
            TransportAssignment oldTransportAssignment =
                    transportAssignmentService.getLatestTransportAssignment(delivery);
            return Arrays.asList(
                    new TransportUndoPickupRequest(delivery, oldTransportAssignment,
                            "Assignment to different selling vehicle"),
                    UpdateSellingPointForDeliveryConfirmation.builder()
                            .delivery(delivery)
                            .sellingPoint(sellingPoint)
                            .oldSellingPoint(oldSellingPoint)
                            .admin(request.getAdmin())
                            .build());
        }
        return Arrays.asList(
                DeliveryConnectedWithSellingPointConfirmation.builder()
                        .delivery(delivery)
                        .sellingPoint(deliveryToSellingPoint.getSellingPoint()).build(),
                UpdateSellingPointForDeliveryConfirmation.builder()
                        .delivery(delivery)
                        .sellingPoint(sellingPoint)
                        .oldSellingPoint(oldSellingPoint)
                        .admin(request.getAdmin())
                        .build());
    }

    private void updateDeliveryTimeAndAddressFromSellingPoint(Delivery delivery, SellingPoint sellingPoint) {
        delivery.setDesiredDeliveryTime(sellingPoint.getPlannedStay());
        delivery.getDeliveryAddress().setAddress(sellingPoint.getLocation());
        parcelAddressService.store(delivery.getDeliveryAddress());
        deliveryService.store(delivery);
    }

    private Delivery createParcelLabel(DeliveryToSellingPoint deliveryToSellingPoint) {
        Delivery delivery = deliveryService.refresh(deliveryToSellingPoint.getDelivery());
        if(!trackingCodeService.isRollingParcelCode(delivery.getTrackingCode())) {
            delivery.setTrackingCode(trackingCodeService.getRollingParcelCode());
            log.info("parcel label store delivery trackingCode");
            delivery = deliveryService.store(delivery);
            deliveryToSellingPoint.setDelivery(delivery);
            delivery.setTrackingCodeLabelURL(
                    parcelLabelService.generateParcelLabelPdfUrl(deliveryToSellingPoint));
            log.info("parcel label store delivery trackingCodeUrl");
        }
        return deliveryService.store(delivery);
    }
    
    @EventProcessing
    private void handleDeliveryCreatedEvent(DeliveryCreatedEvent event) {
        try {
            // Connect the deliveries for the additional packages to the same selling point as the original delivery
            DeliveryToSellingPoint originalDeliveryToSellingPoint =
                    deliveryToSellingPointService.findFirstByCustomReferenceNumber(
                            event.getDelivery().getCustomReferenceNumber());
            deliveryToSellingPointService.store(DeliveryToSellingPoint.builder()
                        .delivery(event.getDelivery())
                        .sellingPoint(originalDeliveryToSellingPoint.getSellingPoint()).build());
        } catch (DeliveryToSellingPointNotFoundException e){
            if(log.isTraceEnabled()) {
                log.trace("Delivery " + event.getDelivery().getId() +
                        " is not an additional parcel delivery to a selling point.");
            }
        }
    }

    @EventProcessing
    private void handleDeliveryReadyForTransportConfirmation(DeliveryReadyForTransportConfirmation event) {

        try {
            // Create the parcel label with the rolling parcel code
            DeliveryToSellingPoint deliveryToSellingPoint =
                    deliveryToSellingPointService.findByDelivery(event.getDelivery());

            createParcelLabel(deliveryToSellingPoint);
        } catch (DeliveryToSellingPointNotFoundException e){
            if(log.isTraceEnabled()) {
                log.trace("Delivery " + event.getDelivery().getId() +
                        " is not an additional parcel delivery to a selling point.");
            }
        }
    }
    
}
