/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.processors;

import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.mobilekiosk.MobileKioskEmailTemplate;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.SellingPointDeletedEvent;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.UpdateSellingPointForDeliveryConfirmation;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.IDeliveryToSellingPointService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderReadyForTransportConfirmation;
import de.fhg.iese.dd.platform.business.shopping.services.IPurchaseOrderService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.feature.LogisticsShopFeature;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.LogisticsParticipantType;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.config.MobileKioskConfig;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.DeliveryToSellingPoint;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
class MobileKioskEmailEventProcessor extends BaseEventProcessor {

    @Autowired
    private IEmailSenderService emailSenderService;
    
    @Autowired
    private IDeliveryToSellingPointService deliveryToSellingPointService;
    
    @Autowired
    private ITimeService timeService;
    
    @Autowired
    private IRoleService roleService;
    
    @Autowired
    private IPurchaseOrderService purchaseOrderService;

    @Autowired
    private IFeatureService featureService;

    @Autowired
    private IAppService appService;
    
    @Autowired
    private MobileKioskConfig mkConfig;

    @EventProcessing
    private void handleUpdateSellingPointForDeliveryResponse(UpdateSellingPointForDeliveryConfirmation event) {
        if(event.getAdmin() != null){
            try{
                sendEmailRegardingReselectedAppointmentByAdminToCustomer(
                        deliveryToSellingPointService.findByDelivery(event.getDelivery()),
                        event.getOldSellingPoint(),
                        event.getAdmin());
            }catch (Exception e){
                log.error("Failed to send email to customer for UpdateSellingPointForDeliveryResponse "+event.getLogMessage(), e);
            }
            try{
                sendEmailRegardingReselectedAppointmentByAdminToVendor(
                        deliveryToSellingPointService.findByDelivery(event.getDelivery()),
                        event.getOldSellingPoint(),
                        event.getAdmin());
            }catch (Exception e){
                log.error("Failed to send email to vendor for UpdateSellingPointForDeliveryResponse "+event.getLogMessage(), e);
            }
        }
        else {
            try{
                sendEmailRegardingReselectedAppointmentToVendor(deliveryToSellingPointService.findByDelivery(event.getDelivery()), event.getOldSellingPoint());
            }catch (Exception e){
                log.error("Failed to send email to vendor for UpdateSellingPointForDeliveryResponse "+event.getLogMessage(), e);
            }
        }
    }

    @EventProcessing
    private void handleSellingPointDeletedEvent(SellingPointDeletedEvent event) {
        Set<DeliveryToSellingPoint> deliveryToSellingPoints = deliveryToSellingPointService.findBySellingPoint(event.getDeletedSellingPoint());
        
        for(DeliveryToSellingPoint deliveryToSellingPoint : deliveryToSellingPoints) {
            try {
                sendEmailRegardingDeletedSellingPointToCustomer(deliveryToSellingPoint);
            } catch (Exception e) {
                log.error("Failed to send Email for SellingPointDeletedEvent "+event.getLogMessage(), e);
            }
        }
    }

    @EventProcessing
    private void handlePurchaseOrderReadyForTransportConfirmation(PurchaseOrderReadyForTransportConfirmation event) {
        if(deliveryToSellingPointService.existsForDelivery(event.getPurchaseOrder().getDelivery())){
            try {
                sendEmailRegardingConfirmedDelivery(
                        deliveryToSellingPointService.findByDelivery(event.getPurchaseOrder().getDelivery()));
            } catch (Exception e){
                log.error("Failed to send delivery confirmation email for PurchaseOrderReadyForTransportConfirmation "
                        +event.getLogMessage(), e);
            }
        }
    }

    private void sendEmailRegardingDeletedSellingPointToCustomer(DeliveryToSellingPoint deliveryToSellingPoint) throws Exception{

        if(deliveryToSellingPoint.getDelivery().getReceiver().getParticipantType() != LogisticsParticipantType.PRIVATE
                || deliveryToSellingPoint.getDelivery().getSender().getParticipantType() != LogisticsParticipantType.SHOP) {
            return;
        }

        PurchaseOrder purchaseOrder = purchaseOrderService.findByDelivery(deliveryToSellingPoint.getDelivery());
        String orderId = extractOrderIdFromShopOrderId(purchaseOrder.getShopOrderId());
        LogisticsShopFeature feature = featureService.getFeature(LogisticsShopFeature.class,
                FeatureTarget.of(deliveryToSellingPoint.getDelivery().getTenant(), appService.findById(
                        LogisticsConstants.LIEFERBAR_APP_ID)));
        String changeSellingPointUrl = feature.getShopUrl() + "/my-account/view-order/" + orderId;

        final Person receiver = deliveryToSellingPoint.getDelivery().getReceiver().getPerson();
        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("sellingPointName", deliveryToSellingPoint.getSellingPoint().getLocation().getName());
        templateModel.put("sellingPointDate",
                toLocalDate(deliveryToSellingPoint.getSellingPoint().getPlannedStay().getStartTime()));
        templateModel.put("receiver", receiver);
        templateModel.put("orderId", orderId);
        templateModel.put("orderDate", toLocalDate(purchaseOrder.getCreated()));
        templateModel.put("shop", deliveryToSellingPoint.getDelivery().getSender().getShop());
        templateModel.put("changeSellingPointUrl", changeSellingPointUrl);

        addCommonData(deliveryToSellingPoint, templateModel);

        String fromEmailAddress = mkConfig.getSenderEmailAddressShop();
        String text = emailSenderService.createTextWithTemplate(
                MobileKioskEmailTemplate.CUSTOMER_NEED_TO_RESELECT_SELLINGPOINT,
                templateModel);
        String subject = "Wichtiger Hinweis zu deiner Bestellung";

        emailSenderService.sendEmail(fromEmailAddress, receiver.getFullName(), receiver.getEmail(), subject, text,
                Collections.emptyList(), true);
    }

    private void sendEmailRegardingReselectedAppointmentToVendor(DeliveryToSellingPoint deliveryToSellingPoint, SellingPoint oldSellingPoint) throws Exception{

        if(deliveryToSellingPoint.getDelivery().getReceiver().getParticipantType() != LogisticsParticipantType.PRIVATE
                || deliveryToSellingPoint.getDelivery().getSender().getParticipantType() != LogisticsParticipantType.SHOP) {
            return;
        }
        PurchaseOrder purchaseOrder = purchaseOrderService.findByDelivery(deliveryToSellingPoint.getDelivery());
        String orderId = extractOrderIdFromShopOrderId(purchaseOrder.getShopOrderId());
        LogisticsShopFeature feature = featureService.getFeature(LogisticsShopFeature.class,
                FeatureTarget.of(deliveryToSellingPoint.getDelivery().getTenant(),
                        appService.findById(LogisticsConstants.LIEFERBAR_APP_ID)));
        String orderUrl = feature.getShopUrl() + "/wp-admin/post.php?post=" + orderId + "&action=edit";
        final Shop sender = purchaseOrder.getSender();

        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("receiverName", deliveryToSellingPoint.getDelivery().getReceiver().getPerson().getFullName());
        templateModel.put("orderId", orderId);
        templateModel.put("orderDate", toLocalDate(purchaseOrder.getCreated()));
        templateModel.put("orderUrl", orderUrl);

        addCommonData(deliveryToSellingPoint, templateModel);
        addNewSellingPointData(deliveryToSellingPoint, templateModel);
        addOldSellingPointData(oldSellingPoint, templateModel);

        String fromEmailAddress = mkConfig.getSenderEmailAddressShop();
        String text =
                emailSenderService.createTextWithTemplate(MobileKioskEmailTemplate.VENDOR_NEW_APPOINTMENT_BY_CUSTOMER,
                        templateModel);
        String subject = "Hinweis zu einer Bestellung";

        emailSenderService.sendEmail(fromEmailAddress, sender.getName(), sender.getEmail(), subject, text,
                Collections.emptyList(), true);
    }

    private void sendEmailRegardingReselectedAppointmentByAdminToCustomer(DeliveryToSellingPoint deliveryToSellingPoint,
            SellingPoint oldSellingPoint, Person admin) throws Exception {

        if(deliveryToSellingPoint.getDelivery().getReceiver().getParticipantType() != LogisticsParticipantType.PRIVATE
                || deliveryToSellingPoint.getDelivery().getSender().getParticipantType() != LogisticsParticipantType.SHOP) {
            return;
        }

        PurchaseOrder purchaseOrder = purchaseOrderService.findByDelivery(deliveryToSellingPoint.getDelivery());
        String orderId = extractOrderIdFromShopOrderId(purchaseOrder.getShopOrderId());
        final Person receiver = deliveryToSellingPoint.getDelivery().getReceiver().getPerson();

        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("receiver", receiver);
        templateModel.put("orderId", orderId);
        templateModel.put("orderDate", toLocalDate(purchaseOrder.getCreated()));

        addCommonData(deliveryToSellingPoint, templateModel);
        addNewSellingPointData(deliveryToSellingPoint, templateModel);
        addOldSellingPointData(oldSellingPoint, templateModel);

        templateModel.put("adminAvailable", admin != null);
        if (admin != null) {
            templateModel.put("adminName", admin.getFullName());
        }

        templateModel.put("shop", deliveryToSellingPoint.getDelivery().getSender().getShop());

        String fromEmailAddress = mkConfig.getSenderEmailAddressShop();
        String text =
                emailSenderService.createTextWithTemplate(MobileKioskEmailTemplate.CUSTOMER_NEW_APPOINTMENT_BY_ADMIN,
                        templateModel);
        String subject = "Wichtiger Hinweis zu deiner Bestellung";

        emailSenderService.sendEmail(fromEmailAddress, receiver.getFullName(), receiver.getEmail(), subject, text,
                Collections.emptyList(), true);
    }

    private void sendEmailRegardingReselectedAppointmentByAdminToVendor(DeliveryToSellingPoint deliveryToSellingPoint,
            SellingPoint oldSellingPoint, Person admin) throws Exception {

        if(deliveryToSellingPoint.getDelivery().getReceiver().getParticipantType() != LogisticsParticipantType.PRIVATE
                || deliveryToSellingPoint.getDelivery().getSender().getParticipantType() != LogisticsParticipantType.SHOP) {
            return;
        }

        PurchaseOrder purchaseOrder = purchaseOrderService.findByDelivery(deliveryToSellingPoint.getDelivery());
        String orderId = extractOrderIdFromShopOrderId(purchaseOrder.getShopOrderId());
        LogisticsShopFeature feature = featureService.getFeature(LogisticsShopFeature.class,
                FeatureTarget.of(deliveryToSellingPoint.getDelivery().getTenant(),
                        appService.findById(LogisticsConstants.LIEFERBAR_APP_ID)));
        String orderUrl = feature.getShopUrl() + "/wp-admin/post.php?post=" + orderId + "&action=edit";

        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("receiverName", deliveryToSellingPoint.getDelivery().getReceiver().getPerson().getFullName());
        templateModel.put("orderId", orderId);
        templateModel.put("orderDate", toLocalDate(purchaseOrder.getCreated()));
        templateModel.put("orderUrl", orderUrl);

        addNewSellingPointData(deliveryToSellingPoint, templateModel);
        addOldSellingPointData(oldSellingPoint, templateModel);

        templateModel.put("adminAvailable", admin != null);
        if (admin != null) {
            templateModel.put("adminName", admin.getFullName());
        }

        String fromEmailAddress = mkConfig.getSenderEmailAddressShop();
        String text =
                emailSenderService.createTextWithTemplate(MobileKioskEmailTemplate.VENDOR_NEW_APPOINTMENT_BY_ADMIN,
                        templateModel);
        String subject = "Hinweis zu einer Bestellung";

        final Shop sender = purchaseOrder.getSender();
        emailSenderService.sendEmail(fromEmailAddress, sender.getName(), sender.getEmail(), subject, text,
                Collections.emptyList(), true);
    }

    private void sendEmailRegardingConfirmedDelivery(DeliveryToSellingPoint deliveryToSellingPoint) throws Exception {

        if (deliveryToSellingPoint.getDelivery().getReceiver().getParticipantType() != LogisticsParticipantType.PRIVATE
                || deliveryToSellingPoint.getDelivery().getSender().getParticipantType() !=
                LogisticsParticipantType.SHOP) {
            return;
        }

        PurchaseOrder purchaseOrder = purchaseOrderService.findByDelivery(deliveryToSellingPoint.getDelivery());
        String orderId = extractOrderIdFromShopOrderId(purchaseOrder.getShopOrderId());

        Map<String, Object> templateModel = new HashMap<>();
        final Person receiver = deliveryToSellingPoint.getDelivery().getReceiver().getPerson();
        templateModel.put("receiver", receiver);
        templateModel.put("orderId", orderId);
        templateModel.put("orderDate", toLocalDate(purchaseOrder.getCreated()));

        templateModel.put("sellingPointDate",
                toLocalDate(deliveryToSellingPoint.getSellingPoint().getPlannedStay().getStartTime()));
        templateModel.put("sellingPointStartTime",
                toLocalTime(deliveryToSellingPoint.getSellingPoint().getPlannedStay().getStartTime()));
        templateModel.put("sellingPointEndTime",
                toLocalTime(deliveryToSellingPoint.getSellingPoint().getPlannedStay().getEndTime()));
        templateModel.put("sellingPointAddress",
                deliveryToSellingPoint.getSellingPoint().getLocation().getStreet()
                        + ", " + deliveryToSellingPoint.getSellingPoint().getLocation().getCity());
        templateModel.put("sellingPointCity", deliveryToSellingPoint.getSellingPoint().getLocation().getCity());

        templateModel.put("shop", deliveryToSellingPoint.getDelivery().getSender().getShop());

        addCommonData(deliveryToSellingPoint, templateModel);

        String fromEmailAddress = mkConfig.getSenderEmailAddressShop();
        String text = emailSenderService.createTextWithTemplate(MobileKioskEmailTemplate.CUSTOMER_DELIVERY_AGREEMENT,
                templateModel);
        String subject = "Hinweis zur Abholung";

        emailSenderService.sendEmail(fromEmailAddress, receiver.getFullName(), receiver.getEmail(), subject, text,
                Collections.emptyList(), true);
    }

    private void addCommonData(DeliveryToSellingPoint deliveryToSellingPoint, Map<String, Object> templateModel){

        Person contactPerson =
                roleService.getDefaultContactPersonForTenant(deliveryToSellingPoint.getDelivery().getTenant());
        templateModel.put("contactPerson", contactPerson);

    }

    private void addNewSellingPointData(DeliveryToSellingPoint deliveryToSellingPoint, Map<String, Object> templateModel) {

        templateModel.put("newSellingPointDate",
                toLocalDate(deliveryToSellingPoint.getSellingPoint().getPlannedStay().getStartTime()));
        templateModel.put("newSellingPointStartTime",
                toLocalTime(deliveryToSellingPoint.getSellingPoint().getPlannedStay().getStartTime()));
        templateModel.put("newSellingPointEndTime",
                toLocalTime(deliveryToSellingPoint.getSellingPoint().getPlannedStay().getEndTime()));
        templateModel.put("newSellingPointAddress",
                deliveryToSellingPoint.getSellingPoint().getLocation().getStreet()
                        + ", " + deliveryToSellingPoint.getSellingPoint().getLocation().getCity());
    }

    private void addOldSellingPointData(SellingPoint oldSellingPoint, Map<String, Object> templateModel) {
        templateModel.put("oldSellingPointAvailable", oldSellingPoint != null);
        if(oldSellingPoint != null) {
            templateModel.put("oldSellingPointDate", toLocalDate(oldSellingPoint.getPlannedStay().getStartTime()));
            templateModel.put("oldSellingPointStartTime", toLocalTime(oldSellingPoint.getPlannedStay().getStartTime()));
            templateModel.put("oldSellingPointEndTime", toLocalTime(oldSellingPoint.getPlannedStay().getEndTime()));
            templateModel.put("oldSellingPointAddress",
                    oldSellingPoint.getLocation().getStreet() + ", " + oldSellingPoint.getLocation().getCity());
        }
    }

    String extractOrderIdFromShopOrderId(String shopOrderId) {
        if(shopOrderId == null) {
            return null;
        }
        Pattern shopOrderIdPattern = mkConfig.getShopOrderIdExtractionRegex(); // "^(?:[^_]*_)?(\\d+)(?:_\\d+)?$"
        Matcher matcher = shopOrderIdPattern.matcher(shopOrderId);
        if(matcher.matches()) {
            return matcher.group(1);
        } else {
            return shopOrderId;
        }
    }

    private String toLocalDate(long timeStampUTC){
        return timeService.toLocalTime(timeStampUTC).format(DateTimeFormatter.ofPattern("dd.MM."));
    }

    private String toLocalTime(long timeStampUTC){
        return timeService.toLocalTime(timeStampUTC).format(DateTimeFormatter.ofPattern("HH:mm"));
    }

}
