/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.services;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.SellingPointDeletedEvent;
import de.fhg.iese.dd.platform.business.mobilekiosk.exceptions.SellingPointNotFoundException;
import de.fhg.iese.dd.platform.business.mobilekiosk.model.SellingPointWithTransportAssignments;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.DeliveryToSellingPoint;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPointStatus;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.repos.SellingPointRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

@Service
class SellingPointService extends BaseEntityService<SellingPoint> implements ISellingPointService  {

    @Autowired
    private SellingPointRepository sellingPointRepository;
    @Autowired
    private IDeliveryToSellingPointService deliveryToSellingPointService;
    @Autowired
    private ITransportAssignmentService transportAssignmentService;

    @Override
    public List<SellingPoint> findAllActiveFutureOrCurrentSellingPointsOrderedByPlannedArrivalAsc(Tenant tenant) {
        return sellingPointRepository
                .findAllByCommunityAndPlannedStayEndTimeGreaterThanAndStatusOrderByPlannedStayStartTimeAsc(tenant,
                        timeService.currentTimeMillisUTC(), SellingPointStatus.ACTIVE);
    }

    @Override
    public SellingPoint findById(String id) {
        if (id == null) {
            throw new SellingPointNotFoundException("null");
        }
        return sellingPointRepository.findById(id).orElseThrow(() -> new SellingPointNotFoundException(id));
    }

    @Override
    public void updateSellingPoints(Tenant tenant, Collection<SellingPoint> sellingPoints) {
        long now = timeService.currentTimeMillisUTC();

        //process SellingPoints in the past -> status = ARCHIVED
        Set<SellingPoint> sellingPointsToBeArchived = sellingPointRepository
                .findAllByCommunityAndPlannedStayEndTimeLessThanAndStatus(tenant, now, SellingPointStatus.ACTIVE);
        sellingPointsToBeArchived.forEach(sp -> sp.setStatus(SellingPointStatus.ARCHIVED));
        sellingPointRepository.saveAll(sellingPointsToBeArchived);

        //TODO set now to a later point in time to prevent deletion of selling points on the next day? See JIRA BAY-93
        Set<SellingPoint> currentOrFutureSellingPointsToBeUpdated = sellingPoints.stream()
                .filter(sp -> sp.getPlannedStay().getEndTime() >= now)
                .collect(Collectors.toSet());
        Set<SellingPoint> currentOrFutureSellingPointsInTheDatabase =
                sellingPointRepository.findAllByCommunityAndPlannedStayEndTimeGreaterThan(
                        tenant, now);

        for (SellingPoint sellingPoint : currentOrFutureSellingPointsInTheDatabase) {
            if (currentOrFutureSellingPointsToBeUpdated
                    .stream().noneMatch(sp -> sp.getLocation().getId().equals(sellingPoint.getLocation().getId()) &&
                            sp.getPlannedStay().getStartTime() == sellingPoint.getPlannedStay().getStartTime() &&
                            sp.getPlannedStay().getEndTime() == sellingPoint.getPlannedStay().getEndTime())) {
                //there is a selling point in the database that doesn't have an corresponding updated selling point -> delete
                deleteSellingPoint(sellingPoint);
            }
        }

        for (SellingPoint updatedSellingPoint : currentOrFutureSellingPointsToBeUpdated) {
            Optional<SellingPoint> sellingPoint = currentOrFutureSellingPointsInTheDatabase
                    .stream()
                    .filter(sp -> sp.getLocation().getId().equals(updatedSellingPoint.getLocation().getId()) &&
                            sp.getPlannedStay().getStartTime() == updatedSellingPoint.getPlannedStay().getStartTime() &&
                            sp.getPlannedStay().getEndTime() == updatedSellingPoint.getPlannedStay().getEndTime())
                    .findFirst();
            if(sellingPoint.isPresent()) {
                //there is a selling point in the database for an updated selling point -> update
                updatedSellingPoint.setId(sellingPoint.get().getId());
                sellingPointRepository.save(updatedSellingPoint);
            } else {
                //there is no selling point in the database for updated selling point -> create
                sellingPointRepository.save(updatedSellingPoint);
            }
        }

        sellingPointRepository.flush();
    }

    private void deleteSellingPoint(SellingPoint sellingPointToBeDeleted) {
        sellingPointToBeDeleted.setStatus(SellingPointStatus.DELETED);
        sellingPointToBeDeleted = sellingPointRepository.save(sellingPointToBeDeleted);
        notify(new SellingPointDeletedEvent(sellingPointToBeDeleted));
    }

    private ZonedDateTime getStartOfDay() {
        return timeService.nowLocal().toLocalDate().atStartOfDay(timeService.getLocalTimeZone());
    }

    @Override
    public boolean isSellingPointAvailableForSelection(SellingPoint sellingPoint) {
        return sellingPoint.getStatus() == SellingPointStatus.ACTIVE &&
                isSellingPointStayDayAfterTomorrowOrLater(sellingPoint);
    }

    private boolean isSellingPointStayDayAfterTomorrowOrLater(SellingPoint sellingPoint) {
        final ZonedDateTime startOfStay = timeService.toLocalTime(sellingPoint.getPlannedStay().getStartTime());
        final ZonedDateTime startOfDayAfterTomorrow = getStartOfDay().plusDays(2);
        return startOfStay.isAfter(startOfDayAfterTomorrow);
    }

    @Override
    public List<SellingPoint> getSellingPointsAvailableForOrder(Tenant tenant){
        final ZonedDateTime start = getStartOfDay().plusDays(2);
        return sellingPointRepository.findAllByCommunityAndPlannedStayStartTimeGreaterThanAndStatusOrderByPlannedStayStartTimeAsc(
                tenant,
                timeService.toTimeMillis(start),
                SellingPointStatus.ACTIVE);
    }

    @Override
    public List<SellingPoint> getTodaysSellingPoints(Tenant tenant) {
        final ZonedDateTime startOfToday = getStartOfDay();
        return sellingPointRepository.findAllByCommunityAndPlannedStayStartTimeGreaterThanAndPlannedStayStartTimeLessThanOrderByPlannedStayStartTimeAsc(
                        tenant,
                        timeService.toTimeMillis(startOfToday),
                        timeService.toTimeMillis(startOfToday.plusDays(1)))
                .stream()
                .filter(sp -> sp.getStatus() != SellingPointStatus.DELETED)
                .collect(Collectors.toList());
    }

    @Override
    public List<SellingPointWithTransportAssignments> getSellingPointsWithTransportAssignments(Tenant tenant){
        List<SellingPoint> sellingPoints = sellingPointRepository
                .findAllByCommunityAndPlannedStayStartTimeGreaterThanOrderByPlannedStayStartTimeAsc(
                        tenant, timeService.currentTimeMillisUTC())
                .stream().filter(sp->sp.getStatus()!=SellingPointStatus.DELETED).collect(Collectors.toList());

        Set<String> sellingPointIds = sellingPoints.stream()
                .map(BaseEntity::getId)
                .collect(Collectors.toSet());

        Set<DeliveryToSellingPoint> deliveryToSellingPoints = deliveryToSellingPointService
                .findAllBySellingPointIds(sellingPointIds);

        Set<String> deliveryIds = deliveryToSellingPoints.stream()
                .map(dtsp -> dtsp.getDelivery().getId())
                .collect(Collectors.toSet());

        Set<TransportAssignment> transportAssignments = transportAssignmentService.findAllByDeliveryIds(deliveryIds);

        return buildSellingPointWithTransportAssignments(sellingPoints, deliveryToSellingPoints, transportAssignments);
    }

    private static List<SellingPointWithTransportAssignments> buildSellingPointWithTransportAssignments(List<SellingPoint> sellingPoints,
                Set<DeliveryToSellingPoint> deliveryToSellingPoints, Set<TransportAssignment> transportAssignments) {
        List<SellingPointWithTransportAssignments> sellingPointWithTransportAssignmentsList = new ArrayList<>();

        for(SellingPoint sellingPoint : sellingPoints) {
            SellingPointWithTransportAssignments sellingPointWithTransportAssignments =
                    SellingPointWithTransportAssignments.builder()
                            .sellingPoint(sellingPoint).transportAssignments(new HashSet<>()).build();

            for (DeliveryToSellingPoint deliveryToSellingPoint : deliveryToSellingPoints.stream()
                    .filter(dtsp -> dtsp.getSellingPoint().getId().equals(sellingPoint.getId()))
                    .collect(Collectors.toSet())) {
                sellingPointWithTransportAssignments.getTransportAssignments().addAll(
                        transportAssignments.stream().filter(
                                ta -> ta.getDelivery().getId().equals(deliveryToSellingPoint.getDelivery().getId()))
                                .collect(Collectors.toSet()));
            }
            sellingPointWithTransportAssignmentsList.add(sellingPointWithTransportAssignments);
        }

        return sellingPointWithTransportAssignmentsList;
    }

    @Override
    public List<SellingPointWithTransportAssignments> getSellingPointsWithTransportAssignments(Tenant tenant, int relativeDate, int days){
        if(days<1) days = 1;
        final ZonedDateTime startDay = getStartOfDay().plusDays(relativeDate);

        List<SellingPoint> sellingPoints = sellingPointRepository
                .findAllByCommunityAndPlannedStayStartTimeGreaterThanAndPlannedStayStartTimeLessThanOrderByPlannedStayStartTimeAsc(
                        tenant, timeService.toTimeMillis(startDay), timeService.toTimeMillis(startDay.plusDays(days)))
                .stream().filter(sp->sp.getStatus()!=SellingPointStatus.DELETED).collect(Collectors.toList());

        Set<String> sellingPointIds = sellingPoints.stream().map(BaseEntity::getId).collect(Collectors.toSet());

        Set<DeliveryToSellingPoint> deliveryToSellingPoints = deliveryToSellingPointService
                .findAllBySellingPointIds(sellingPointIds);

        Set<String> deliveryIds = deliveryToSellingPoints.stream().map(dtsp->dtsp.getDelivery().getId()).collect(Collectors.toSet());

        Set<TransportAssignment> transportAssignments = transportAssignmentService.findAllByDeliveryIds(deliveryIds);

        return buildSellingPointWithTransportAssignments(sellingPoints, deliveryToSellingPoints, transportAssignments);
    }

    @Override
    public SellingPointWithTransportAssignments getSellingPointWithTransportAssignmentsById(String id) {
        SellingPoint sellingPoint = findById(id);

        SellingPointWithTransportAssignments sellingPointWithTransportAssignments =
                SellingPointWithTransportAssignments.builder()
                        .sellingPoint(sellingPoint)
                        .transportAssignments(new HashSet<>()).
                        build();

        Set<DeliveryToSellingPoint> deliveryToSellingPoints = deliveryToSellingPointService.findBySellingPoint(sellingPoint);

        Set<String> deliveryIds = deliveryToSellingPoints.stream()
                .map(dtsp->dtsp.getDelivery().getId())
                .collect(Collectors.toSet());

        Set<TransportAssignment> transportAssignments = transportAssignmentService.findAllByDeliveryIds(deliveryIds);

        for (DeliveryToSellingPoint deliveryToSellingPoint : deliveryToSellingPoints.stream()
                .filter(dtsp -> dtsp.getSellingPoint().getId().equals(sellingPoint.getId()))
                .collect(Collectors.toSet())) {
            sellingPointWithTransportAssignments.getTransportAssignments().addAll(
                    transportAssignments.stream().filter(
                            ta -> ta.getDelivery().getId().equals(deliveryToSellingPoint.getDelivery().getId()))
                            .collect(Collectors.toSet()));
        }

        return sellingPointWithTransportAssignments;
    }

    @Override
    public boolean isAnySellingPointInProximityOfCurrentTime(long proximityMillis, String vehicleId) {
        long now = timeService.currentTimeMillisUTC();
        long pastTime = now - proximityMillis;
        long futureTime = now + proximityMillis;

        return sellingPointRepository.existsByPlannedStayStartTimeGreaterThanEqualAndPlannedStayStartTimeLessThanEqualAndVehicleIdEquals(
                pastTime, futureTime, vehicleId)
                ||
                sellingPointRepository.existsByPlannedStayEndTimeGreaterThanEqualAndPlannedStayEndTimeLessThanEqualAndVehicleIdEquals(
                        pastTime, futureTime, vehicleId)
                ||
                sellingPointRepository.existsByPlannedStayStartTimeLessThanEqualAndPlannedStayEndTimeGreaterThanEqualAndVehicleIdEquals(
                        pastTime, futureTime, vehicleId);
    }

}
