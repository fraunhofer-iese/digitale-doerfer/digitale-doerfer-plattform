/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2018 Balthasar Weitzel, Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.UpdateSellingPointsConfirmation;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.UpdateSellingPointsRequest;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.ISellingPointService;

@EventProcessor
public class SellingPointEventProcessor extends BaseEventProcessor {
    
    @Autowired
    private ISellingPointService sellingPointService;

    @EventProcessing
    private UpdateSellingPointsConfirmation handleUpdateSellingPointsRequest(UpdateSellingPointsRequest request) {
        sellingPointService.updateSellingPoints(request.getTenant(), request.getSellingPoints());
        return new UpdateSellingPointsConfirmation();
    }

}
