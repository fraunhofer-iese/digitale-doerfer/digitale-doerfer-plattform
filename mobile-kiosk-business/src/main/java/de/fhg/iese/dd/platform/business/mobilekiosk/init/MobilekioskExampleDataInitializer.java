/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2018 Adeline Silva Schäfer, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.init;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventBusAccessor;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForTransportConfirmation;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.IDeliveryToSellingPointService;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.ISellingPointService;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.ISellingVehicleService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.shop.services.IShopService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderCreatedEvent;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderReadyForTransportRequest;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderReceivedEvent;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Dimension;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PackagingType;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.DeliveryToSellingPoint;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingVehicle;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.roles.SellingVehicleDriver;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrderItem;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrderParcel;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@Component
@Profile({"!prod & !test"})
public class MobilekioskExampleDataInitializer extends BaseDataInitializer implements IManualTestDataInitializer {

    public static final String TENANT_ID_STEINWALD = "96eaa4b2-dc80-4eed-8c1b-ea7eaaa7c5d5";
    public static final String SHOP_ID_DORFLADEN = "8cc9a372-fa9b-11e7-8c3f-9a214cf093ae";
    public static final String PERSON_ID_STEINWALD_1 = "8cc99b98-fa9b-11e7-8c3f-9a214cf093ae";
    public static final String PERSON_ID_STEINWALD_2 = "8cc99e40-fa9b-11e7-8c3f-9a214cf093ae";

    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IAddressService addressService;
    @Autowired
    private ISellingVehicleService sellingVehicleService;
    @Autowired
    private ISellingPointService sellingPointService;
    @Autowired
    private IShopService shopService;
    @Autowired
    private IPersonService personService;
    @Autowired
    private IRoleService roleService;
    @Autowired
    private IDeliveryToSellingPointService deliveryToSellingPointService;
    @Autowired
    private EventBusAccessor eventBusAccessor;

    @PostConstruct
    private void initialize(){
        eventBusAccessor.setOwner(this);
    }

    @SuppressWarnings("unchecked")
    private <E extends BaseEvent> E notifyAndWaitForReply(Class<E> eventToWaitFor, BaseEvent... eventsToNotify) {
        try {
            return (E) eventBusAccessor.notifyTriggerAndWaitForAnyReply(Arrays.asList(eventsToNotify), Collections.singletonList(eventToWaitFor));
        } catch (Exception e) {
            throw new DataInitializationException(e);
        }
    }

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("mobilekiosk-example")
                .requiredEntity(Tenant.class)
                .requiredEntity(Person.class)
                .requiredEntity(Shop.class)
                .processedEntity(SellingVehicle.class)
                .processedEntity(SellingPoint.class)
                .build();
    }

    @Override
    public String getScenarioId() {
        return "mobilekiosk-example-1";
    }

    @Override
    public Collection<String> getDependentTopics() {
        return unmodifiableList("person-example","shop-example");
    }

    private enum Case {
        ONE_DELIVERY, TWO_DELIVERIES, NO_DELIVERY, MULTIPLE_PARCEL_DELIVERY;

        public Case nextCase() {
            final int total = values().length;
            return values()[(ordinal() + 1) % total];
        }

        public String niceName() {
            return name().toLowerCase().replace("_", " ");
        }
    }

    @Override
    @SuppressWarnings("fallthrough")
    @SuppressFBWarnings(value = {"SF_SWITCH_FALLTHROUGH", "SF_SWITCH_NO_DEFAULT"}, justification = "Fallthrough is intended " +
            "for the sake of brevity in code; default case is present, so this is a false positive")
    protected void createInitialDataSimple(LogSummary logSummary) {
        logSummary.info(
                "Starting data creation for mobilekiosk-example. Note that it is required to have 'person-example' " +
                        "and 'shop-example' run first. If in the following, some person or shop ids cannot be found, " +
                        "this might be the reason.");
        final Tenant steinwald = tenantService.findTenantById(TENANT_ID_STEINWALD);
        final Shop dorfladen = shopService.findShopById(SHOP_ID_DORFLADEN);
        final Person receiver1 = personService.findPersonById(PERSON_ID_STEINWALD_1);
        final Person receiver2 = personService.findPersonById(PERSON_ID_STEINWALD_2);

        createSellingVehicles(logSummary, steinwald);

        final List<SellingPoint> sellingPoints = createSellingPoints(logSummary, steinwald);

        //creating different amount of deliveries when iterating over the selling points
        Case currentCase = Case.ONE_DELIVERY;

        for (SellingPoint sellingPoint : sellingPoints) {
            logSummary.info("Creating " + currentCase.niceName() + " for selling point " + sellingPoint);

            //remove existing deliveries assigned to the selling point
            deliveryToSellingPointService.findBySellingPoint(sellingPoint)
                    .forEach(deliveryToSellingPointService::remove);

            switch (currentCase) {
                case TWO_DELIVERIES:
                    createDeliveryAndTransportForSellingPoint(logSummary, steinwald, dorfladen, receiver2, sellingPoint);
                case ONE_DELIVERY:
                    createDeliveryAndTransportForSellingPoint(logSummary, steinwald, dorfladen, receiver1, sellingPoint);
                    break;
                case MULTIPLE_PARCEL_DELIVERY:
                    createMultipleParcelDeliveryAndTransportForSellingPoint(logSummary, steinwald, dorfladen, receiver1,
                            sellingPoint, 2);
                    break;
                case NO_DELIVERY:
                default:
                    //no action
            }
            currentCase = currentCase.nextCase();
        }
    }

    private void createSellingVehicles(LogSummary logSummary, Tenant tenant) {
        Map<String, String> driverPersonByVehicleId = loadEntitiesFromDataJson(tenant, SellingVehicle.class)
                .get("driver")
                .stream()
                .collect(Collectors.toMap(SellingVehicle::getId, SellingVehicle::getName));
        final List<SellingVehicle> vehicles = loadEntitiesFromDataJsonDefaultSection(tenant, SellingVehicle.class);
        for (SellingVehicle vehicle : vehicles) {
            vehicle.setTenant(tenant);
            vehicle = sellingVehicleService.store(vehicle);
            final Person driver = personService.findPersonById(
                    checkOrLookupId(driverPersonByVehicleId.get(vehicle.getId()))
            );
            roleService.assignRoleToPerson(driver, SellingVehicleDriver.class, vehicle);
            logSummary.info("Created " + vehicle + " with driver " + driver);
        }
    }

    private List<SellingPoint> createSellingPoints(LogSummary logSummary, Tenant tenant) {
        final List<SellingPoint> sellingPoints = loadEntitiesFromDataJsonDefaultSection(tenant, SellingPoint.class);
        final List<SellingPoint> createdSellingPoints = new ArrayList<>();
        for (SellingPoint sellingPoint : sellingPoints) {
            sellingPoint.setTenant(tenant);
            sellingPoint.setLocation(addressService.findOrCreateAddress(
                    IAddressService.AddressDefinition.fromAddress(sellingPoint.getLocation()),
                    IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                    IAddressService.GPSResolutionStrategy.LOOKEDUP_PROVIDED_NONE,
                    false));
            sellingPoint.setVehicle(sellingVehicleService.findById(sellingPoint.getVehicle().getId()));
            final TimeSpan plannedStayOriginal = new TimeSpan(
                    hourMinuteToTimestampOfToday(sellingPoint.getPlannedStay().getStartTime()),
                    hourMinuteToTimestampOfToday(sellingPoint.getPlannedStay().getEndTime())
            );
            sellingPoint.setPlannedStay(plannedStayOriginal);
            createdSellingPoints.add(sellingPointService.store(sellingPoint));
            logSummary.info("Created " + sellingPoint);

            if (!sellingPoint.getId().endsWith("0")) {
                throw new DataInitializationException("Selling point IDs must end with 0");
            }
            final String idPrefix = sellingPoint.getId().substring(0, sellingPoint.getId().length()-1);

            //create points the last two days and the next three days
            Stream.of(-2, -1, 1, 2, 3)
                    .forEach(i -> {
                        //store point again for tomorrow with an id derived from the today's selling point id in order to overwrite
                        //selling point when executing again
                        String newId = idPrefix;
                        if (i > 0) {
                            newId += i;
                        } else if (i == -1) {
                            newId += 'a';
                        } else if (i == -2) {
                            newId += 'b';
                        } else {
                            throw new IllegalArgumentException();
                        }
                        sellingPoint.setId(newId);
                        sellingPoint.setPlannedStay(new TimeSpan(
                                sameTimeButPlusDays(plannedStayOriginal.getStartTime(), i),
                                sameTimeButPlusDays(plannedStayOriginal.getEndTime(), i)
                        ));
                        createdSellingPoints.add(sellingPointService.store(sellingPoint));
                        logSummary.info("Created " + sellingPoint);
                    });
        }
        createdSellingPoints.sort(Comparator.comparingLong(sp -> sp.getPlannedStay().getStartTime()));
        return createdSellingPoints;
    }

    private void createDeliveryAndTransportForSellingPoint(LogSummary logSummary, Tenant tenant, Shop shop,
            Person receiver, SellingPoint sellingPoint) {
        //create purchase order
        final PurchaseOrderReceivedEvent purchaseOrderReceivedEvent = PurchaseOrderReceivedEvent.builder()
                .tenant(tenant)
                .credits(8)
                .pickupAddress(shop.getAddresses().iterator().next().getAddress())
                .deliveryAddress(sellingPoint.getLocation())
                .desiredDeliveryTime(sellingPoint.getPlannedStay())
                .orderedItems(Arrays.asList(
                        PurchaseOrderItem.builder()
                                .amount(1)
                                .itemName("Mehl")
                                .unit("Packung")
                                .build(),
                        PurchaseOrderItem.builder()
                                .amount(5)
                                .itemName("Kartoffeln")
                                .unit("kg")
                                .build()))
                .purchaseOrderNotes("Platz für Notizen.")
                .receiver(receiver)
                .sender(shop)
                .shopOrderId("order" + timeService.currentTimeMillisUTC())
                .transportNotes("Bitte vorsichtig behandeln")
                .build();

        final PurchaseOrder purchaseOrder = notifyAndWaitForReply(PurchaseOrderCreatedEvent.class, purchaseOrderReceivedEvent)
                .getPurchaseOrder();
        final Delivery delivery = purchaseOrder.getDelivery();
        logSummary.info("  Created " + purchaseOrder);

        waitForEventProcessingFinished(20);

        //connect delivery with selling point
        final DeliveryToSellingPoint deliveryToSellingPoint = deliveryToSellingPointService.store(DeliveryToSellingPoint.builder()
                .delivery(delivery)
                .sellingPoint(sellingPoint)
                .build());
        logSummary.info("    Created " + deliveryToSellingPoint);

        //mark purchase order ready for transport
        final PurchaseOrderReadyForTransportRequest readyForTransportRequest = new PurchaseOrderReadyForTransportRequest(
                purchaseOrder,
                null,
                new Dimension(10.0, 10.0, 5.0, 12.0),
                null,
                PackagingType.PARCEL,
                1);
        final Delivery deliveryReadyForTransport = notifyAndWaitForReply(DeliveryReadyForTransportConfirmation.class,
                readyForTransportRequest).getDelivery();
        logSummary.info("    Updated " + deliveryReadyForTransport);
    }

    private void createMultipleParcelDeliveryAndTransportForSellingPoint(LogSummary logSummary, Tenant tenant,
            Shop shop,
            Person receiver, SellingPoint sellingPoint, int numberAdditionalParcels) {
        //create purchase order
        final PurchaseOrderReceivedEvent purchaseOrderReceivedEvent = PurchaseOrderReceivedEvent.builder()
                .tenant(tenant)
                .credits(8)
                .pickupAddress(shop.getAddresses().iterator().next().getAddress())
                .deliveryAddress(sellingPoint.getLocation())
                .desiredDeliveryTime(sellingPoint.getPlannedStay())
                .orderedItems(Arrays.asList(
                        PurchaseOrderItem.builder()
                                .amount(1)
                                .unit("Packung")
                                .itemName("Mehl")
                                .build(),
                        PurchaseOrderItem.builder()
                                .amount(5)
                                .unit("kg")
                                .itemName("Kartoffeln")
                                .build()))
                .purchaseOrderNotes("Platz für Notizen.")
                .receiver(receiver)
                .sender(shop)
                .shopOrderId("order" + timeService.currentTimeMillisUTC())
                .transportNotes("Bitte vorsichtig behandeln")
                .build();

        final PurchaseOrder purchaseOrder = notifyAndWaitForReply(PurchaseOrderCreatedEvent.class, purchaseOrderReceivedEvent)
                .getPurchaseOrder();
        final Delivery delivery = purchaseOrder.getDelivery();
        logSummary.info("  Created " + purchaseOrder);

        waitForEventProcessingFinished(20);

        //connect delivery with selling point
        final DeliveryToSellingPoint deliveryToSellingPoint = deliveryToSellingPointService.store(DeliveryToSellingPoint.builder()
                .delivery(delivery)
                .sellingPoint(sellingPoint)
                .build());
        logSummary.info("    Created " + deliveryToSellingPoint);

        //mark purchase order ready for transport
        final PurchaseOrderReadyForTransportRequest readyForTransportRequest = new PurchaseOrderReadyForTransportRequest(
                purchaseOrder,
                null,
                new Dimension(10.0, 10.0, 5.0, 12.0),
                null,
                PackagingType.PARCEL,
                1);

        if(numberAdditionalParcels > 0) {
            List<PurchaseOrderParcel> additionalParcels = new ArrayList<>();

            for(int i = 0; i < numberAdditionalParcels; i++) {
                additionalParcels.add(PurchaseOrderParcel.builder()
                        .contentNotes("Inhalt "+i)
                        .packagingType(PackagingType.PARCEL)
                        .size(new Dimension((i+1)*10, (i+2)*10, (i+3)*10, (i+4)*10))
                        .build());
            }

            readyForTransportRequest.setAdditionalParcels(additionalParcels);
        }
        final Delivery deliveryReadyForTransport = notifyAndWaitForReply(DeliveryReadyForTransportConfirmation.class,
                readyForTransportRequest).getDelivery();
        logSummary.info("    Updated " + deliveryReadyForTransport);
    }

    private static final Pattern hourMinute = Pattern.compile("(\\d?\\d)(\\d\\d)");

    private long hourMinuteToTimestampOfToday(long start) {
        final Matcher m = hourMinute.matcher(String.valueOf(start));
        if (!m.matches()) {
            throw new IllegalArgumentException("Wrong time format in plannedStay, expected HHmm, but was " + start);
        }
        return timeService.toTimeMillis(timeService.nowLocal()
                .withHour(Integer.parseInt(m.group(1)))
                .withMinute(Integer.parseInt(m.group(2)))
                .withSecond(0)
                .withNano(0));
    }

    private long sameTimeButPlusDays(long timestamp, int days) {
        return timestamp + days * 24L * 60 * 60 * 1000;
    }

    private void waitForEventProcessingFinished(int waitIterations) {
        try {
            Thread.sleep(50L);
            for (int i = 0; i < waitIterations; i++) {
                if (eventBusAccessor.areEventsInProcessing()) {
                    Thread.sleep(100L);
                    log.info("waitForEventProcessing: iteration {}", i);
                }
                Thread.sleep(25L);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

}
