/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2018 Balthasar Weitzel, Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.services;

import java.util.Collection;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.mobilekiosk.exceptions.DeliveryToSellingPointNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.DeliveryToSellingPoint;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.repos.DeliveryToSellingPointRepository;

@Service
class DeliveryToSellingPointService extends BaseEntityService<DeliveryToSellingPoint> implements IDeliveryToSellingPointService {

    @Autowired
    private DeliveryToSellingPointRepository deliveryToSellingPointRepository;

    @Override
    public DeliveryToSellingPoint findByDelivery(Delivery delivery) {
        return deliveryToSellingPointRepository.findOneByDelivery(delivery)
                .orElseThrow(DeliveryToSellingPointNotFoundException::new);
    }

    @Override
    public Set<DeliveryToSellingPoint> findBySellingPoint(SellingPoint sellingPoint) {
        return deliveryToSellingPointRepository.findAllBySellingPoint(sellingPoint);
    }

    @Override
    public boolean existsForDelivery(Delivery delivery) {
        return deliveryToSellingPointRepository.findOneByDelivery(delivery).isPresent();
    }

    @Override
    public Set<DeliveryToSellingPoint> findAllBySellingPointIds(Collection<String> sellingPointIds){
        return deliveryToSellingPointRepository.findBySellingPointIdIn(sellingPointIds);
    }

    @Override
    public void remove(DeliveryToSellingPoint deliveryToSellingPoint) {
        deliveryToSellingPointRepository.delete(deliveryToSellingPoint);
    }

    @Override
    public DeliveryToSellingPoint findFirstByCustomReferenceNumber(String customReferenceNumber){
        return deliveryToSellingPointRepository.findFirstByDeliveryCustomReferenceNumber(customReferenceNumber)
                .orElseThrow(DeliveryToSellingPointNotFoundException::new);
    }

}
