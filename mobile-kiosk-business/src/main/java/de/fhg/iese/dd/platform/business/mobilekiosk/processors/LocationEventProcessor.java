/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.processors;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.LocationRecordsReceivedConfirmation;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.LocationRecordsReceivedEvent;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.IRawLocationRecordService;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.RawLocationRecord;

@EventProcessor
public class LocationEventProcessor extends BaseEventProcessor {

    @Autowired
    private IRawLocationRecordService rawLocationRecordService;

    @EventProcessing
    private LocationRecordsReceivedConfirmation handleLocationRecordsReceivedEvent(LocationRecordsReceivedEvent event) {
        List<RawLocationRecord> records = event.getLocationRecords()
                .stream()
                .map(rawLocationRecord -> {
                    try{
                        return rawLocationRecordService.store(rawLocationRecord);
                    } catch (Exception e){
                        log.error("Could not store raw location record :"+e.getMessage()+"; record: "+rawLocationRecord.toString(), e);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        return LocationRecordsReceivedConfirmation.builder()
                .locationRecords(records)
                .build();
    }

}
