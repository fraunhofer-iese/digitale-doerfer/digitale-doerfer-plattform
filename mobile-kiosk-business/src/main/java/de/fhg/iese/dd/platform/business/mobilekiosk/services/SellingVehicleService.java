/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2018 Balthasar Weitzel, Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.services;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Strings;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.mobilekiosk.exceptions.SellingVehicleInvalidException;
import de.fhg.iese.dd.platform.business.mobilekiosk.exceptions.SellingVehicleNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingVehicle;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.repos.SellingVehicleRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

@Service
class SellingVehicleService extends BaseEntityService<SellingVehicle> implements ISellingVehicleService {

    @Autowired
    private SellingVehicleRepository sellingVehicleRepository;

    @Override
    public SellingVehicle findAndUpdateOrCreate(String id, String name, Tenant tenant) {
        if (StringUtils.isEmpty(id)) {
            throw new SellingVehicleInvalidException(id);
        }
        SellingVehicle sellingVehicle = sellingVehicleRepository.findById(id).orElseGet(
                () -> {
                    //in the case of creation, a name and tenant must be provided
                    if (StringUtils.isEmpty(name) || tenant == null) {
                        throw new SellingVehicleInvalidException(id);
                    }
                    return SellingVehicle.builder()
                            .id(id)
                            .build();
                });
        if (!StringUtils.isEmpty(name)) {
            sellingVehicle.setName(name);
        }
        if (tenant != null) {
            sellingVehicle.setTenant(tenant);
        }
        sellingVehicle = store(sellingVehicle);
        return sellingVehicle;
    }

    @Override
    public SellingVehicle findById(String id) {
        if (Strings.isNullOrEmpty(id)) throw new SellingVehicleInvalidException(id);
        return sellingVehicleRepository.findById(id)
                .orElseThrow(() -> new SellingVehicleNotFoundException(id));
    }

}
