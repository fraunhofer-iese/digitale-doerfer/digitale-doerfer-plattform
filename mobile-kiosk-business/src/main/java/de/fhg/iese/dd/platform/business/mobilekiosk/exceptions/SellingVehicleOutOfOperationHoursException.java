/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class SellingVehicleOutOfOperationHoursException extends BadRequestException {

    private static final long serialVersionUID = -8168628212797609245L;

    public SellingVehicleOutOfOperationHoursException(String vehicleId) {
        super("The selling vehicle '"+vehicleId+"' is currently not in its operation hours.");
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.VEHICLE_NOT_IN_OPERATING_HOURS;
    }

}
