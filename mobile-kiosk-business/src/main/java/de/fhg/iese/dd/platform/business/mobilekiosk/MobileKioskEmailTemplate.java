/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk;

import de.fhg.iese.dd.platform.business.shared.template.TemplateLocation;

public class MobileKioskEmailTemplate extends TemplateLocation {

    public static final MobileKioskEmailTemplate CUSTOMER_DELIVERY_AGREEMENT =
            new MobileKioskEmailTemplate("customer_delivery-agreed.ftl");

    public static final MobileKioskEmailTemplate CUSTOMER_NEW_APPOINTMENT_BY_ADMIN =
            new MobileKioskEmailTemplate("customer_appointment-reselection-of-admin.ftl");
    public static final MobileKioskEmailTemplate VENDOR_NEW_APPOINTMENT_BY_ADMIN =
            new MobileKioskEmailTemplate("vendor_appointment-reselection-of-admin.ftl");

    public static final MobileKioskEmailTemplate CUSTOMER_NEED_TO_RESELECT_SELLINGPOINT =
            new MobileKioskEmailTemplate("customer_reselect-delivery-location-and-time.ftl");
    public static final MobileKioskEmailTemplate VENDOR_NEW_APPOINTMENT_BY_CUSTOMER =
            new MobileKioskEmailTemplate("vendor_appointment-reselection-of-customer.ftl");
    
    private MobileKioskEmailTemplate(String name) {
        super(name);
    }

}
