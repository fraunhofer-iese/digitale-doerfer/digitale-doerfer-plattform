/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.mobilekiosk.exceptions.RawLocationRecordNotFoundException;
import de.fhg.iese.dd.platform.business.mobilekiosk.exceptions.SellingVehicleOutOfOperationHoursException;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.config.MobileKioskConfig;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.RawLocationRecord;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.repos.RawLocationRecordRepository;

@Service
class RawLocationRecordService extends BaseEntityService<RawLocationRecord> implements IRawLocationRecordService {

    @Autowired
    private RawLocationRecordRepository rawLocationRecordRepository;

    @Autowired
    private ISellingPointService sellingPointService;

    @Autowired
    private MobileKioskConfig mobileKioskConfig;

    @Override
    public RawLocationRecord findById(String id) {
        return rawLocationRecordRepository.findById(id).orElseThrow(()->new RawLocationRecordNotFoundException(id));
    }

    @Override
    public List<RawLocationRecord> findAllForVehicleIdAndTimestampBetween(String vehicleId, long start, long end) {
        return rawLocationRecordRepository.findAllByVehicleIdAndTimestampGreaterThanEqualAndTimestampLessThanEqualOrderByTimestampAsc(vehicleId, start, end);
    }

    @Override
    public RawLocationRecord findLatestByVehicleId(String vehicleId) {
        if(!sellingPointService.isAnySellingPointInProximityOfCurrentTime(mobileKioskConfig.getLatestLocationSellingPointProximityMillis(), vehicleId)){
            throw new SellingVehicleOutOfOperationHoursException(vehicleId);
        }
        return rawLocationRecordRepository
                .findFirstByVehicleIdOrderByTimestampDesc(vehicleId)
                .orElseThrow(()-> new RawLocationRecordNotFoundException("for vehicleId "+vehicleId));
    }

}
