/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.services;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.html2pdf.exceptions.Html2PdfException;
import de.fhg.iese.dd.platform.business.shared.html2pdf.services.IHtml2PdfService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.logistics.config.LogisticsConfig;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.DeliveryToSellingPoint;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
class ParcelLabelService extends BaseService implements IParcelLabelService {

    @Autowired
    private IFileStorage fileStorage;

    @Autowired
    private LogisticsConfig logisticsConfig;

    @Autowired
    private IHtml2PdfService html2PdfService;

    @Override
    public String generateParcelLabelPdfUrl(DeliveryToSellingPoint deliveryToSellingPoint) {
        final String html = generateParcelLabelHtml(deliveryToSellingPoint);
        final String fileName = "label_" + deliveryToSellingPoint.getDelivery().getId() + "_"
                + timeService.currentTimeMillisUTC() + ".pdf";
        try {
            byte[] pdfBytes = html2PdfService.convert(html, 100, 62);
            fileStorage.saveFile(pdfBytes, "application/pdf",
                    logisticsConfig.getParcelLabelInternalStoragePrefix() + fileName);

            return fileStorage.getExternalUrl(logisticsConfig.getParcelLabelInternalStoragePrefix() + fileName);
        } catch (Html2PdfException ex) {
            log.error("Could not convert label html to pdf", ex);
        } catch (FileStorageException ex) {
            log.error("Could not save label pdf file to file storage", ex);
        }
        return null;
    }

    @Override
    public String generateParcelLabelHtml(DeliveryToSellingPoint deliveryToSellingPoint) {

        String date =
                this.timeService.toLocalTime(deliveryToSellingPoint.getSellingPoint().getPlannedStay().getStartTime())
                        .format(DateTimeFormatter.ofPattern("E, dd. MMMM").withLocale(Locale.GERMANY));

        final Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("parcelNumber", deliveryToSellingPoint.getDelivery().getTrackingCode());
        templateModel.put("receiverFirstName",
                deliveryToSellingPoint.getDelivery().getReceiver().getPerson().getFirstName());
        templateModel.put("receiverLastName",
                deliveryToSellingPoint.getDelivery().getReceiver().getPerson().getLastName());
        templateModel.put("receiverStreet", deliveryToSellingPoint.getSellingPoint().getLocation().getStreet());
        templateModel.put("receiverCity", deliveryToSellingPoint.getSellingPoint().getLocation().getCity());
        templateModel.put("date", date);
        if (StringUtils.isNotEmpty(deliveryToSellingPoint.getDelivery().getTransportNotes())) {
            templateModel.put("transportNotes", deliveryToSellingPoint.getDelivery().getTransportNotes());
        }
        if (StringUtils.isNotEmpty(deliveryToSellingPoint.getDelivery().getContentNotes())) {
            templateModel.put("contentNotes", deliveryToSellingPoint.getDelivery().getContentNotes());
        }

        final Configuration freemarkerConfiguration = new Configuration(Configuration.VERSION_2_3_23);
        freemarkerConfiguration.setDefaultEncoding("UTF-8");
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");

        try {
            final Template template = freemarkerConfiguration.getTemplate("parcelLabel/parcelLabel.ftl");
            return FreeMarkerTemplateUtils.processTemplateIntoString(template, templateModel);
        } catch (IOException | TemplateException ex) {
            throw new IllegalStateException("Parcel label creation via freemarker is not configured properly", ex);
        }
    }

}
