/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.init;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.mobilekiosk.services.ISellingVehicleService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingVehicle;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.roles.SellingVehicleDriver;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;

@Component
public class MobileKioskDataInitializer extends BaseDataInitializer {

    @Autowired
    protected ITenantService tenantService;

    @Autowired
    protected IPersonService personService;

    @Autowired
    protected IRoleService roleService;

    @Autowired
    private ISellingVehicleService sellingVehicleService;

    @Autowired
    protected ApplicationConfig appConfig;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("mobilekiosk")
                .requiredEntity(Tenant.class)
                .requiredEntity(Person.class)
                .processedEntity(SellingVehicle.class)
                .build();
    }

    @Override
    protected void createInitialDataSimple(LogSummary logSummary) {
        logSummary.info("Starting data creation for mobilekiosk. Note that it is required to have 'person' " +
                "and 'shop' run first. If in the following, some person or shop ids cannot be found, " +
                "this might be the reason.");
        for (Tenant tenant : tenantService.findAllOrderedByNameAsc()) {
            createSellingVehicles(logSummary, tenant);
        }
    }

    private void createSellingVehicles(LogSummary logSummary, Tenant tenant) {

        Map<String, List<SellingVehicle>> entities = loadEntitiesFromDataJson(tenant, SellingVehicle.class);
        if(entities != null) {
            Map<String, String> driverPersonByVehicleId = entities
                    .get("driver")
                    .stream()
                    .collect(Collectors.toMap(SellingVehicle::getId, SellingVehicle::getName));
            final List<SellingVehicle> vehicles = loadEntitiesFromDataJsonDefaultSection(tenant,
                    SellingVehicle.class);
            for (SellingVehicle vehicle : vehicles) {
                vehicle.setTenant(tenant);
                vehicle = sellingVehicleService.store(vehicle);
                final Person driver = personService.findPersonById(
                        checkOrLookupId(driverPersonByVehicleId.get(vehicle.getId()))
                );
                roleService.assignRoleToPerson(driver, SellingVehicleDriver.class, vehicle);
                logSummary.info("Created " + vehicle + " with driver " + driver);
            }
        }
    }

}
