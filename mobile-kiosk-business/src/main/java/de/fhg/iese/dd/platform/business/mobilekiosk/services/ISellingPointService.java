/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.services;

import java.util.Collection;
import java.util.List;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.mobilekiosk.model.SellingPointWithTransportAssignments;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

public interface ISellingPointService extends IEntityService<SellingPoint> {

    List<SellingPoint> findAllActiveFutureOrCurrentSellingPointsOrderedByPlannedArrivalAsc(Tenant tenant);
    SellingPoint findById(String id);
    void updateSellingPoints(Tenant tenant, Collection<SellingPoint> sellingPoints);
    boolean isSellingPointAvailableForSelection(SellingPoint sellingPoint);
    List<SellingPoint> getSellingPointsAvailableForOrder(Tenant tenant);
    List<SellingPoint> getTodaysSellingPoints(Tenant tenant);
    List<SellingPointWithTransportAssignments> getSellingPointsWithTransportAssignments(Tenant tenant);
    List<SellingPointWithTransportAssignments> getSellingPointsWithTransportAssignments(Tenant tenant, int relativeDate, int days);
    SellingPointWithTransportAssignments getSellingPointWithTransportAssignmentsById(String id);

    boolean isAnySellingPointInProximityOfCurrentTime(long proximityMillis, String vehicleId);
}
