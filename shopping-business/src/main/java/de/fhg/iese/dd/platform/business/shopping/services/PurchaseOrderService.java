/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Steffen Hupp, Balthasar Weitzel, Axel Wickenkamp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.shopping.exceptions.PurchaseOrderNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import de.fhg.iese.dd.platform.datamanagement.shopping.repos.PurchaseOrderRepository;

@Service
class PurchaseOrderService extends BaseEntityService<PurchaseOrder> implements IPurchaseOrderService  {

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepo;

    @Override
    public PurchaseOrder findPurchaseOrderById(String purchaseOrderId) throws PurchaseOrderNotFoundException {
        if(purchaseOrderId == null)
            throw new PurchaseOrderNotFoundException("null");
        return purchaseOrderRepo.findById(purchaseOrderId)
                .orElseThrow(() -> new PurchaseOrderNotFoundException(purchaseOrderId));
    }

    @Override
    public PurchaseOrder findPurchaseOrderByIdOrReturnNull(String purchaseOrderId){
        return purchaseOrderRepo.findById(purchaseOrderId).orElse(null);
    }

    @Override
    public PurchaseOrder findByShopOrderId(String shopOrderId) {
        return purchaseOrderRepo.findOneByShopOrderId(shopOrderId);
    }

    @Override
    public PurchaseOrder findByDelivery(Delivery delivery) {
        return purchaseOrderRepo.findOneByDelivery(delivery);
    }

    @Override
    public List<PurchaseOrder> findAllByReceiver(Person person) {
        return purchaseOrderRepo.findAllByReceiverOrderByCreatedDesc(person);
    }

    @Override
    public List<PurchaseOrder> findAllBySender(Shop shop) {
        return purchaseOrderRepo.findAllBySenderOrderByCreatedDesc(shop);
    }

    @Override
    public int numberOfPurchaseOrdersForReceiver(Person receiver) {
        return Math.toIntExact(purchaseOrderRepo.countByReceiver(receiver));
    }

    @Override
    public PurchaseOrder getMostRecentPurchaseOrderForReceiver(Person receiver) {
        return purchaseOrderRepo.findFirstByReceiverOrderByCreatedDesc(receiver);
    }

    @Override
    public List<PurchaseOrder> getPurchaseOrdersForReceiver(Person receiver) {
        return purchaseOrderRepo.findAllByReceiverOrderByCreatedDesc(receiver);
    }

    @Override
    public int numberOfShopsForReceiver(Person receiver) {
        return Math.toIntExact(purchaseOrderRepo.countDistinctShopsForReceiver(receiver));
    }

    @Override
    public List<Shop> shopsForReceiverWithNumberOfOrdersGreaterThan(Person receiver, int numberOfOrders) {
       return purchaseOrderRepo.findShopsWithPurchaseOrdersByReceiverGreaterThanValueOrderByCreatedDesc(receiver, numberOfOrders);
    }

    @Override
    public int numberOfPurchaseOrdersAtShopForReceiver(Shop shop, Person receiver) {
        return Math.toIntExact(purchaseOrderRepo.countBySenderAndReceiver(shop, receiver));
    }

}
