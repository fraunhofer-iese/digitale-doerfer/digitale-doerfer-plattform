/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.services;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountCreditLimitExceededException;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;

public interface IShoppingScoreService extends IService {
    /**
     * Book the sales commission the shop owner has to pay for a new purchase
     * order
     *
     * @param purchaseOrder
     */
    void bookSalesCommission(PurchaseOrder purchaseOrder);

    /**
     * Book the motivation score a buyer gets for a new purchase order
     *
     * @param purchaseOrder
     */
    void bookPurchaseRewardBuyer(PurchaseOrder purchaseOrder);

    /**
     * Calculate the motivation score a buyer gets for this purchase order.
     *
     * @param purchaseOrder
     * @return
     */
    int calculateMotivationScore(PurchaseOrder purchaseOrder);

    /**
     * Rebook the sales commission the shop owner has to pay for a new purchase
     * order. Only applies to ones accidentally payed by receivers.
     *
     * @param purchaseOrder
     * @throws AccountCreditLimitExceededException
     */
    void rebookWrongSalesCommission(PurchaseOrder purchaseOrder) throws AccountCreditLimitExceededException;

    /**
     * Rebook the sales commissions the shop owner has to pay for a new purchase order. Only applies to ones
     * accidentally payed by receivers.
     */
    void rebookWrongSalesCommissions();
}
