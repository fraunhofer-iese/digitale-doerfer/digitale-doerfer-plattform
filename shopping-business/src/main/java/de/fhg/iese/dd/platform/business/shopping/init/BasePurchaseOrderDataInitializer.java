/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.init;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.logistics.init.ILogisticsDataInitService;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.business.shopping.services.IDemoShoppingService;
import de.fhg.iese.dd.platform.business.shopping.services.IPurchaseOrderService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrderItem;

public abstract class BasePurchaseOrderDataInitializer extends BaseDataInitializer {

    @Autowired
    private IPurchaseOrderService purchaseOrderService;
    @Autowired
    protected IDemoShoppingService demoShoppingService;
    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IDeliveryService deliveryService;
    @Autowired
    private ILogisticsDataInitService logisticsDataInitService;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .requiredEntity(Tenant.class)
                .requiredEntity(Person.class)
                .requiredEntity(Shop.class)
                .requiredEntity(Delivery.class)
                .processedEntity(PurchaseOrder.class)
                .build();
    }

    protected void newPurchaseOrder(LogSummary logSummary, String shopOrderId, String purchaseOrderId, String deliveryId) {

        Delivery delivery = deliveryService.findDeliveryById(deliveryId);

        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setTenant(tenantService.findTenantById(LogisticsConstants.TENANT_ID_DEMO));
        purchaseOrder.setSender(delivery.getSender().getShop());
        purchaseOrder.setReceiver(delivery.getReceiver().getPerson());
        purchaseOrder.setPickupAddress(logisticsDataInitService.newParcelAddress(delivery.getSender().getShop()));
        purchaseOrder.setDeliveryAddress(logisticsDataInitService.newParcelAddress(delivery.getReceiver().getPerson()));
        purchaseOrder.setDesiredDeliveryTime(delivery.getDesiredDeliveryTime());
        purchaseOrder.setShopOrderId(shopOrderId);
        purchaseOrder.setItems(Arrays.asList(
                PurchaseOrderItem.builder()
                        .amount(5)
                        .itemName("Kaiser-Brötchen")
                        .unit("Stück")
                        .build(),
                PurchaseOrderItem.builder()
                        .amount(1)
                        .itemName("Nussecke")
                        .unit("Stück")
                        .build()));
        purchaseOrder.setCredits(10);
        purchaseOrder.setContentNotes("Bitte beim Nachbarn klingeln!");
        purchaseOrder.setTransportNotes("Trocken halten");
        purchaseOrder.setId(purchaseOrderId);

        purchaseOrder.setDelivery(deliveryService.findDeliveryById(deliveryId));

        purchaseOrderService.store(purchaseOrder);
        logSummary.info("Created purchase order " + purchaseOrder);
    }

}
