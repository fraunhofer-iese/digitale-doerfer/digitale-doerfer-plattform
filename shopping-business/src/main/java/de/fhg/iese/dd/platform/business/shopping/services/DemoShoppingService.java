/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.datamanagement.shopping.repos.PurchaseOrderRepository;

@Service
class DemoShoppingService extends BaseService implements IDemoShoppingService {

    @Autowired
    private ITenantService tenantService;

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Transactional
    @Override
    public void deleteDataForDemoTenant(String tenantId) {
        tenantService.checkTenantByIdExists(tenantId);

        purchaseOrderRepository.deleteByCommunityId(tenantId);

        //there are some purchase orders that are done in the wrong tenant
        purchaseOrderRepository.deleteByDeliveryCommunityId(tenantId);
    }

}
