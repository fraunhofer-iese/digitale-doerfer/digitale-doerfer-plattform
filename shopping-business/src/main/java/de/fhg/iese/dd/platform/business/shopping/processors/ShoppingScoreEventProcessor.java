/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderCreatedEvent;
import de.fhg.iese.dd.platform.business.shopping.services.IShoppingScoreService;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_PREFERRED)
class ShoppingScoreEventProcessor extends BaseEventProcessor {

    @Autowired
    private IShoppingScoreService shoppingScoreService;

    @EventProcessing
    private void handlePurchaseOrderCreatedEvent(PurchaseOrderCreatedEvent event) {
        try {
            // Book sales commission shop -> dd-logistics
            shoppingScoreService.bookSalesCommission(event.getPurchaseOrder());

            // Book motivation scores dd-logistics -> receiver
            shoppingScoreService.bookPurchaseRewardBuyer(event.getPurchaseOrder());
        } catch (Exception e) {
            log.error("Failed to book logistics score in PurchaseOrderCreatedEvent", e);
        }
    }

}
