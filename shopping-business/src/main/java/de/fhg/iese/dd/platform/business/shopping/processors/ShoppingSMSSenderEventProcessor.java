/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2020 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.processors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.phone.services.ISMSSenderService;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderCreatedEvent;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shopping.config.ShoppingConfig;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
class ShoppingSMSSenderEventProcessor extends BaseEventProcessor {

    @Autowired
    private ISMSSenderService smsSenderService;

    @Autowired
    private IPersonService personService;

    @Autowired
    private ShoppingConfig shoppingConfig;

    @EventProcessing
    private void handlePurchaseOrderCreatedEvent(PurchaseOrderCreatedEvent event) {
        try {
            Shop shop = event.getPurchaseOrder().getSender();
            String shopOrderId = event.getPurchaseOrder().getShopOrderId();
            String webShopURL =
                    StringUtils.removeStart(StringUtils.removeStart(
                            StringUtils.removeEnd(shop.getWebShopURL(), "/"),
                            "http://"), "https://");
            String preText = "Eine neue Bestellung (#" + shopOrderId + ") von ";
            String postText = " ist eingegangen " + webShopURL;

            int remainingCharCount = 140 - preText.length() - postText.length();
            String varText = personService.getFirstAndLastName(remainingCharCount, event.getPurchaseOrder().getReceiver());
            String text = preText + varText + postText;
            String smsNumber = shop.getSmsNotificationNumber();
            if(StringUtils.isEmpty(smsNumber)){
                log.warn("No notification number defined for " + shop.getName() + " (" + shop.getId() + ")");
            }else{
                smsSenderService.sendSMS(shoppingConfig.getSmsNotificationSenderId(), smsNumber, text);
            }
        } catch (Exception e){
            //Failed SMS should never result in a failed service call
            log.error("Failed to send SMS", e);
        }
    }

}
