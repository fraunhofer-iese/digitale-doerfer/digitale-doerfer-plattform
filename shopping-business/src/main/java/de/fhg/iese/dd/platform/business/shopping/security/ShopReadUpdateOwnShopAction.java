/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.security;

import java.util.Collection;
import java.util.Collections;

import de.fhg.iese.dd.platform.business.shared.security.services.ActionRoleRelatedEntityRestricted;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.roles.ShopOwner;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;

public class ShopReadUpdateOwnShopAction extends ActionRoleRelatedEntityRestricted<Shop> {

    private static final Collection<Class<? extends BaseRole<?>>> ROLES_SPECIFIC_ENTITIES_ALLOWED =
            Collections.singletonList(ShopOwner.class);

    @Override
    public String getActionDescription() {
        return "Updates and reads details of own shop.";
    }

    protected String entitiesName() {
        return "shops";
    }

    @Override
    protected Collection<Class<? extends BaseRole<?>>> rolesAllEntitiesAllowed() {
        return Collections.emptySet();
    }

    @Override
    protected Collection<Class<? extends BaseRole<?>>> rolesSpecificEntitiesAllowed() {
        return ROLES_SPECIFIC_ENTITIES_ALLOWED;
    }

}
