/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.processors;

import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.shop.services.IShopService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.business.shopping.events.*;
import de.fhg.iese.dd.platform.business.shopping.exceptions.ShopNotDeletableException;
import de.fhg.iese.dd.platform.business.shopping.services.IPurchaseOrderService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHours;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.roles.ShopOwner;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@EventProcessor
public class ShopEventProcessor extends BaseEventProcessor {

    @Autowired
    private IShopService shopService;

    @Autowired
    private IAddressService addressService;

    @Autowired
    private IMediaItemService mediaItemService;

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IPurchaseOrderService purchaseOrderService;

    @EventProcessing
    private ShopCreateConfirmation handleShopCreateRequest(ShopCreateRequest shopCreateRequest) {

        URL profilePictureURL = shopCreateRequest.getProfilePictureURL();
        MediaItem profilePicture = null;
        if (profilePictureURL != null) {
            profilePicture = mediaItemService.createMediaItem(profilePictureURL,
                    FileOwnership.of(shopCreateRequest.getCreator(), shopCreateRequest.getAppVariant()));
        }

        Shop shop = Shop.builder()
                .community(shopCreateRequest.getTenant())
                .owner(shopCreateRequest.getCreator())
                .name(shopCreateRequest.getName())
                .email(shopCreateRequest.getEmail())
                .phone(shopCreateRequest.getPhone())
                .webShopURL(shopCreateRequest.getWebShopURL())
                .openingHours(shopCreateRequest.getOpeningHours())
                .addresses(createAddressListEntries(shopCreateRequest.getAddress()))
                .profilePicture(profilePicture)
                .build();

        Shop createdShop = shopService.store(shop);
        roleService.assignRoleToPerson(createdShop.getOwner(), ShopOwner.class, createdShop);

        return new ShopCreateConfirmation(createdShop, shopCreateRequest.getCreator());
    }

    @EventProcessing
    private ShopUpdateConfirmation handleShopUpdateRequest(ShopUpdateRequest shopUpdateRequest) {

        Shop shop = shopUpdateRequest.getShop();

        String name = shopUpdateRequest.getName();
        if (StringUtils.isNotEmpty(name)) {
            shop.setName(name);
        }

        String email = shopUpdateRequest.getEmail();
        if (StringUtils.isNotEmpty(email)) {
            shop.setEmail(email);
        }

        String phone = shopUpdateRequest.getPhone();
        if (StringUtils.isNotEmpty(phone)) {
            shop.setPhone(phone);
        }

        String webShopURL = shopUpdateRequest.getWebShopURL();
        if (StringUtils.isNotEmpty(webShopURL)) {
            shop.setWebShopURL(webShopURL);
        }

        OpeningHours openingHours = shopUpdateRequest.getOpeningHours();
        if (openingHours != null) {
            shop.setOpeningHours(openingHours);
        }

        Address address = shopUpdateRequest.getAddress();
        if (address != null) {
            shop.setAddresses(createAddressListEntries(address));
        }

        URL profilePictureURL = shopUpdateRequest.getProfilePictureURL();
        if (profilePictureURL != null) {
            MediaItem profilePicture = mediaItemService.createMediaItem(profilePictureURL,
                    FileOwnership.of(shopUpdateRequest.getShop().getOwner(), shopUpdateRequest.getAppVariant()));
            shop.setProfilePicture(profilePicture);
        }

        shop = shopService.store(shop);

        return new ShopUpdateConfirmation(shop);
    }

    @EventProcessing
    private ShopSetOwnerConfirmation handleShopSetOwnerRequest(ShopSetOwnerRequest shopSetOwnerRequest) {

        Shop shop = shopSetOwnerRequest.getShop();

        Person newOwner = shopSetOwnerRequest.getNewOwner();

        List<RoleAssignment> currentShopOwnerRoleAssignments =
                roleService.getCurrentRoleAssignmentsForEntity(shop.getOwner(), shop);
        currentShopOwnerRoleAssignments.forEach(roleService::removeRoleAssignment);
        shop.setOwner(newOwner);
        roleService.assignRoleToPerson(newOwner, ShopOwner.class, shop);
        shop = shopService.store(shop);

        return new ShopSetOwnerConfirmation(shop, shop.getOwner());
    }

    @EventProcessing
    private ShopDeleteConfirmation handleShopDeleteRequest(ShopDeleteRequest shopDeleteRequest) {

        Shop shop = shopDeleteRequest.getShop();
        String shopId = shop.getId();
        Tenant shopTenant = shop.getTenant();

        // Check if any purchases have been made at this shop
        List<PurchaseOrder> purchaseOrders = purchaseOrderService.findAllBySender(shop);

        // If there are purchase orders, the shop can't be deleted
        if (! CollectionUtils.isEmpty(purchaseOrders)) {
            throw new ShopNotDeletableException(shop.getId());
        }

        List<RoleAssignment> currentShopRoleAssignments = roleService.getAllRoleAssignmentsForEntity(shop);
        currentShopRoleAssignments.forEach(assignment -> roleService.removeRoleAssignment(assignment));
        shopService.deleteShop(shop);

        return new ShopDeleteConfirmation(shopTenant, shopId);
    }

    private Set<AddressListEntry> createAddressListEntries(Address address) {
        if (address == null) {
            return Collections.emptySet();
        }
        return Collections.singleton(addressService.storeAddressListEntry(
                AddressListEntry.builder()
                        .address(address)
                        .name(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME)
                        .build()));
    }

}
