/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.init;

import java.util.Collection;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.logistics.init.DeliveryDemoDataInitializer;
import de.fhg.iese.dd.platform.business.participants.person.init.PersonDataInitializer;
import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;

@Component
public class PurchaseOrderDemoDataInitializer extends BasePurchaseOrderDataInitializer implements IManualTestDataInitializer {

    public static final String PURCHASE_ORDER_DEMO_ID_01 = "0b42dc08-6723-4e89-b3ed-f065d6e3f3a7";
    public static final String PURCHASE_ORDER_DEMO_ID_02 = "6a0f7bc9-cdba-4b9e-afd3-900aef066a8d";
    public static final String PURCHASE_ORDER_DEMO_ID_03 = "4c9ce0e0-eddf-4ea8-82ed-57155a9794f0";
    public static final String PURCHASE_ORDER_DEMO_ID_04 = "084a93fd-26ce-4f4e-8f49-00f1da0d5a87";
    public static final String PURCHASE_ORDER_DEMO_ID_05 = "0e13175b-85ad-49dd-b3ca-fb20b02f266c";
    public static final String PURCHASE_ORDER_DEMO_ID_06 = "bdb4201f-6153-496d-8b11-8f25a3690c40";
    public static final String PURCHASE_ORDER_DEMO_ID_07 = "64bb6ba4-2662-4c7d-8f3e-f1536e264676";
    public static final String PURCHASE_ORDER_DEMO_ID_08 = "a914647a-979f-4771-8b43-06f62c24cfba";
    public static final String PURCHASE_ORDER_DEMO_ID_09 = "4d692ec0-d673-4614-9115-ca44d60ebd87";
    public static final String PURCHASE_ORDER_DEMO_ID_10 = "803f1bb2-658e-4fab-b4eb-72928687aa2c";
    public static final String PURCHASE_ORDER_DEMO_ID_11 = "70e9b5bd-2db4-4ec7-afaa-fa37f7c596d8";
    public static final String PURCHASE_ORDER_DEMO_ID_12 = "26c96a97-2463-4ec7-830f-c9b6800a9561";
    public static final String PURCHASE_ORDER_DEMO_ID_13 = "8218ecbb-d2c1-4d01-ac08-5441d77118c0";

    @Override
    public String getTopic() {
        return "shopping-demo";
    }

    @Override
    public String getScenarioId() {
        //contributes to several scenarios
        return null;
    }

    @Override
    public Collection<String> getDependentTopics() {
        return unmodifiableList("shop-demo", PersonDataInitializer.TOPIC_DEMO);
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {
        newPurchaseOrder(logSummary,
                "DEMO_01",
                PURCHASE_ORDER_DEMO_ID_01,
                DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_01);

        newPurchaseOrder(logSummary,
                "DEMO_02",
                PURCHASE_ORDER_DEMO_ID_02,
                DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_02);

        newPurchaseOrder(logSummary,
                "DEMO_03",
                PURCHASE_ORDER_DEMO_ID_03,
                DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_03);

        newPurchaseOrder(logSummary,
                "DEMO_04",
                PURCHASE_ORDER_DEMO_ID_04,
                DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_04);

        newPurchaseOrder(logSummary,
                "DEMO_05",
                PURCHASE_ORDER_DEMO_ID_05,
                DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_05);

        newPurchaseOrder(logSummary,
                "DEMO_06",
                PURCHASE_ORDER_DEMO_ID_06,
                DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_06);

        newPurchaseOrder(logSummary,
                "DEMO_07",
                PURCHASE_ORDER_DEMO_ID_07,
                DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_07);

        newPurchaseOrder(logSummary,
                "DEMO_08",
                PURCHASE_ORDER_DEMO_ID_08,
                DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_08);

        newPurchaseOrder(logSummary,
                "DEMO_09",
                PURCHASE_ORDER_DEMO_ID_09,
                DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_09);

        newPurchaseOrder(logSummary,
                "DEMO_10",
                PURCHASE_ORDER_DEMO_ID_10,
                DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_10);

        newPurchaseOrder(logSummary,
                "DEMO_11",
                PURCHASE_ORDER_DEMO_ID_11,
                DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_11);

        newPurchaseOrder(logSummary,
                "DEMO_12",
                PURCHASE_ORDER_DEMO_ID_12,
                DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_12);

        newPurchaseOrder(logSummary,
                "DEMO_13",
                PURCHASE_ORDER_DEMO_ID_13,
                DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_13);
    }

    @Override
    public void dropDataSimple(LogSummary logSummary) {
        demoShoppingService.deleteDataForDemoTenant(LogisticsConstants.TENANT_ID_DEMO);
        logSummary.info("dropped all shopping data for demo tenant");
    }

}
