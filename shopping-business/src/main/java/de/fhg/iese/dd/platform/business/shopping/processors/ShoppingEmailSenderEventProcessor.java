/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2020 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.processors;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCancelConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCreatedEvent;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.business.shopping.ShoppingEmailTemplate;
import de.fhg.iese.dd.platform.business.shopping.services.IPurchaseOrderService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import de.fhg.iese.dd.platform.datamanagement.shopping.config.ShoppingConfig;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
class ShoppingEmailSenderEventProcessor extends BaseEventProcessor {

    @Autowired
    private IEmailSenderService emailSenderService;

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IPurchaseOrderService purchaseOrderService;

    @Autowired
    private ShoppingConfig shoppingConfig;

    @EventProcessing
    private void handleTransportAssignmentCreatedEvent(TransportAssignmentCreatedEvent event) {

        try {
            TransportAssignment assignment = event.getTransportAssignment();

            Person carrier = assignment.getCarrier();

            Delivery delivery = assignment.getDelivery();

            Tenant tenant = delivery.getTenant();

            Person contactPerson = roleService.getDefaultContactPersonForTenant(tenant);

            PurchaseOrder purchaseOrder = purchaseOrderService.findByDelivery(delivery);

            if(purchaseOrder == null && delivery.getCustomReferenceNumber() != null){
                // additional parcel, try get purchase order via reference number
                purchaseOrder = purchaseOrderService
                        .findPurchaseOrderByIdOrReturnNull(delivery.getCustomReferenceNumber());
            }

            if (purchaseOrder != null) {
                // shop delivery
                Shop shop = purchaseOrder.getSender();
                String shopEmailAddress = shop.getEmail();
                String shopName = shop.getName();
                String orderId = purchaseOrder.getShopOrderId();
                String desiredDeliveryTime = TimeSpan
                        .toFriendlyHumanReadableString(assignment.getDesiredDeliveryTime());
                String desiredPickupTime = toFriendlyHumanReadableString(assignment.getDesiredPickupTime());

                Map<String, Object> templateModel = new HashMap<>();
                templateModel.put("shop", purchaseOrder.getSender());
                templateModel.put("purchaseOrder", purchaseOrder);
                templateModel.put("delivery", delivery);
                templateModel.put("carrier", carrier);
                templateModel.put("desiredPickupTime", desiredPickupTime);
                templateModel.put("desiredDeliveryTime", desiredDeliveryTime);
                templateModel.put("contactPerson", contactPerson);

                String fromEmailAddress = shoppingConfig.getSenderEmailAddressShop();
                String text = emailSenderService
                        .createTextWithTemplate(ShoppingEmailTemplate.TRANSPORT_ASSIGNMENT_CREATED, templateModel);

                String subject = "[" + orderId + "] Bestellung durch Lieferant übernommen";
                emailSenderService.sendEmail(fromEmailAddress, shopName, shopEmailAddress, subject, text,
                        Collections.emptyList(), true);
            } else {
                // private delivery
            }
        } catch (Exception e) {
            log.error("Failed to send Email for TransportAssignmentCreatedEvent " + event.getLogMessage(), e);
        }

    }

    @EventProcessing
    private void handleTransportAssignmentCancelConfirmation(TransportAssignmentCancelConfirmation event) {
        try {
            TransportAssignment assignment = event.getTransportAssignment();
            Person carrier = assignment.getCarrier();
            Delivery delivery = assignment.getDelivery();
            Tenant tenant = delivery.getTenant();
            Person contactPerson = roleService.getDefaultContactPersonForTenant(tenant);
            PurchaseOrder purchaseOrder = purchaseOrderService.findByDelivery(delivery);

            if(purchaseOrder == null && delivery.getCustomReferenceNumber() != null){
                // additional parcel, try get purchase order via reference number
                purchaseOrder = purchaseOrderService
                        .findPurchaseOrderByIdOrReturnNull(delivery.getCustomReferenceNumber());
            }

            if (purchaseOrder != null) {
                // shop delivery
                Shop shop = purchaseOrder.getSender();

                String shopEmailAddress = shop.getEmail();
                String shopName = shop.getName();
                String orderId = purchaseOrder.getShopOrderId();
                String desiredDeliveryTime = TimeSpan
                        .toFriendlyHumanReadableString(assignment.getDesiredDeliveryTime());

                Map<String, Object> templateModel = new HashMap<>();
                templateModel.put("shop", purchaseOrder.getSender());
                templateModel.put("purchaseOrder", purchaseOrder);
                templateModel.put("delivery", delivery);
                templateModel.put("carrier", carrier);
                templateModel.put("desiredDeliveryTime", desiredDeliveryTime);
                templateModel.put("contactPerson", contactPerson);

                String fromEmailAddress = shoppingConfig.getSenderEmailAddressShop();
                String text = emailSenderService
                        .createTextWithTemplate(ShoppingEmailTemplate.TRANSPORT_ASSIGNMENT_CANCELLED, templateModel);

                String subject = "[" + orderId + "] Änderung: Lieferant hat Lieferung für Bestellung abgesagt";
                emailSenderService.sendEmail(fromEmailAddress, shopName, shopEmailAddress, subject, text,
                        Collections.emptyList(), true);
            } else {
                // private delivery
            }
        } catch (Exception e) {
            log.error("Failed to send Email for TransportAssignmentCancelConfirmation " + event.getLogMessage(), e);
        }

    }

    private String toFriendlyHumanReadableString(TimeSpan timespan) {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy").withZone(ZoneOffset.UTC);
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm").withZone(ZoneOffset.UTC);

        Instant endTime = Instant.ofEpochMilli(timespan.getEndTime());
        return dateFormatter.format(endTime) + " um " + timeFormatter.format(endTime) + " Uhr";
    }

}
