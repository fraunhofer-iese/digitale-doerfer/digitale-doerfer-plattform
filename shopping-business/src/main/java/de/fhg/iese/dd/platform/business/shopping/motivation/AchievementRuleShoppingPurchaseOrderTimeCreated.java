/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.motivation;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.business.motivation.framework.BaseAchievementRule;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderCreatedEvent;
import de.fhg.iese.dd.platform.business.shopping.services.IPurchaseOrderService;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;

@AchievementRule
public class AchievementRuleShoppingPurchaseOrderTimeCreated extends BaseAchievementRule<PurchaseOrderCreatedEvent> {

    private static final String ACHIEVEMENT_ID =          "d332c880-d553-4822-846e-1624503653f3";
    private static final String ACHIEVEMENT_LEVEL_ID_01 = "84020246-d8d6-4cc6-9125-712dc64f2a16";

    @Autowired
    private IPurchaseOrderService purchaseOrderService;

    @Override
    public String getName() {
        return "shopping.score.purchaseOrder.created.4";
    }

    @Override
    public List<AchievementPersonPairing> checkAchievement(PurchaseOrderCreatedEvent event) {
        PurchaseOrder purchaseOrder = event.getPurchaseOrder();
        if (purchaseOrder == null) {
            return null;
        }
        Person receiver = purchaseOrder.getReceiver();
        if (receiver == null) {
            return null;
        }
        long timestamp = event.getCreated();

        List<PurchaseOrder> purchaseOrders = purchaseOrderService.getPurchaseOrdersForReceiver(receiver);

        int count = 0;

        for(PurchaseOrder order:purchaseOrders) {
            long orderTimestamp = order.getCreated();
            if(isPurchasedDuringNight(orderTimestamp)) {
                count++;
                if(count >= 5) {
                    break;
                }
            }
        }
        if(count >= 5) {
            return Collections.singletonList(
                    achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_01, receiver, timestamp));
        }
        return null;
    }

    @Override
    public Collection<AchievementPersonPairing> checkAchievement(Person person) {
        PurchaseOrder lastPurchaseOder = purchaseOrderService.getMostRecentPurchaseOrderForReceiver(person);
        long timestamp = lastPurchaseOder == null ? timeService.currentTimeMillisUTC() : lastPurchaseOder.getCreated();

        List<PurchaseOrder> purchaseOrders = purchaseOrderService.getPurchaseOrdersForReceiver(person);

        Collection<AchievementPersonPairing> achievedLevels = new ArrayList<>();

        int count = 0;

        for(PurchaseOrder order:purchaseOrders) {
            long orderTimestamp = order.getCreated();
            if(isPurchasedDuringNight(orderTimestamp)) {
                count++;
                if(count >= 5) {
                    break;
                }
            }
        }
        if(count >= 5) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_01, person, timestamp));
        }

        return achievedLevels;
    }

    private boolean isPurchasedDuringNight(long timestamp) {
        ZonedDateTime orderTime = timeService.toLocalTime(timestamp);
        ZonedDateTime endTime = orderTime.withHour(21).plus(Duration.ofHours(8));
        return orderTime.getHour() >= 21 && orderTime.isBefore(endTime);
    }

    @Override
    public Class<PurchaseOrderCreatedEvent> getRelevantEvent() {
        return PurchaseOrderCreatedEvent.class;
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Shop.Order.Night")
                .description("Gebe Bestellungen auf um Awards zu bekommen")
                .pushCategory(findPushCategory(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_MOTIVATION_ID))
                .category("BestellBar")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel01 = AchievementLevel.builder()
                .name("Late-Night-Shopper")
                .description("Wenn andere ans Schlafen denken wirst du erst richtig aktiv und machst dich auf die Jagd!")
                .challengeDescription("Tätige fünf Bestellungen nach 21:00 Uhr")
                .icon(createIconFromDefault("late_night_shopper.png"))
                .iconNotAchieved(createIconFromDefault("late_night_shopper_grey.png"))
                .achievement(achievement)
                .orderValue(1)
                .build();
        achievementLevel01.setId(ACHIEVEMENT_LEVEL_ID_01);
        achievementLevel01 = achievementService.save(achievementLevel01);

        //only needed since the set is not updated automatically by hibernate based on the newly added achievement levels
        achievement.setAchievementLevels(Collections.singleton(achievementLevel01));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_01);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
