/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.motivation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.business.motivation.framework.BaseAchievementRule;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderCreatedEvent;
import de.fhg.iese.dd.platform.business.shopping.services.IPurchaseOrderService;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;

@AchievementRule
public class AchievementRuleShoppingPurchaseOrderSameCreated extends BaseAchievementRule<PurchaseOrderCreatedEvent> {

    private static final String ACHIEVEMENT_ID =          "4cc94f33-705e-4921-818a-702b943ed1d2";
    private static final String ACHIEVEMENT_LEVEL_ID_01 = "fa212644-dc0a-4e73-9342-236d83b2c04a";

    @Autowired
    private IPurchaseOrderService purchaseOrderService;

    @Override
    public String getName() {
        return "shopping.score.purchaseOrder.created.3";
    }

    @Override
    public List<AchievementPersonPairing> checkAchievement(PurchaseOrderCreatedEvent event) {
        PurchaseOrder purchaseOrder = event.getPurchaseOrder();
        if (purchaseOrder == null) {
            return null;
        }
        Person receiver = purchaseOrder.getReceiver();
        if (receiver == null) {
            return null;
        }
        Shop shop = purchaseOrder.getSender();

        int numberOfOrdersAtShop = purchaseOrderService.numberOfPurchaseOrdersAtShopForReceiver(shop, receiver);
        long timestamp = event.getCreated();

        if(numberOfOrdersAtShop >= 5) {
            return Collections.singletonList(
                    achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_01, receiver, timestamp));
        }

        return null;
    }

    @Override
    public Collection<AchievementPersonPairing> checkAchievement(Person person) {
        PurchaseOrder lastPurchaseOder = purchaseOrderService.getMostRecentPurchaseOrderForReceiver(person);
        long timestamp = lastPurchaseOder == null ? timeService.currentTimeMillisUTC() : lastPurchaseOder.getCreated();

        Collection<AchievementPersonPairing> achievedLevels = new ArrayList<>();

        List<Shop> shops = purchaseOrderService.shopsForReceiverWithNumberOfOrdersGreaterThan(person, 4);

        if(!shops.isEmpty()) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_01, person, timestamp));
        }

        return achievedLevels;
    }

    @Override
    public Class<PurchaseOrderCreatedEvent> getRelevantEvent() {
        return PurchaseOrderCreatedEvent.class;
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Shop.Order.Same")
                .description("Bestelle mehrmals beim gleichen Geschäft um Awards zu bekommen")
                .pushCategory(findPushCategory(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_MOTIVATION_ID))
                .category("BestellBar")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel01 = AchievementLevel.builder()
                .name("Lieblingsladen")
                .description("Ohne was zu kaufen gehst du nie aus diesem Laden, oder?")
                .challengeDescription("Bestelle fünf Mal beim gleichen Geschäft.")
                .icon(createIconFromDefault("lieblingsladen.png"))
                .iconNotAchieved(createIconFromDefault("lieblingsladen_grey.png"))
                .achievement(achievement)
                .orderValue(1)
                .build();
        achievementLevel01.setId(ACHIEVEMENT_LEVEL_ID_01);
        achievementLevel01 = achievementService.save(achievementLevel01);

        //only needed since the set is not updated automatically by hibernate based on the newly added achievement levels
        achievement.setAchievementLevels(Collections.singleton(achievementLevel01));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_01);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
