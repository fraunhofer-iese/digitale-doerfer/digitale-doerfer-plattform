/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.motivation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.business.motivation.framework.BaseAchievementRule;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderCreatedEvent;
import de.fhg.iese.dd.platform.business.shopping.services.IPurchaseOrderService;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;

@AchievementRule
public class AchievementRuleShoppingPurchaseOrderCreated extends BaseAchievementRule<PurchaseOrderCreatedEvent> {

    private static final String ACHIEVEMENT_ID =          "4ec52812-e2ad-4e7c-9a0b-009127f7cc8c";
    private static final String ACHIEVEMENT_LEVEL_ID_01 = "245d547d-b8ba-4aed-90c5-be0af5e98e43";
    private static final String ACHIEVEMENT_LEVEL_ID_05 = "43790750-110f-4a0a-87b5-6505aefc5bcf";
    private static final String ACHIEVEMENT_LEVEL_ID_10 = "09c9b758-4079-4cfd-8d9a-de3e970db610";
    private static final String ACHIEVEMENT_LEVEL_ID_25 = "838a0d83-544c-4c6b-88ed-e9cd5b414f03";

    @Autowired
    private IPurchaseOrderService purchaseOrderService;

    @Override
    public String getName() {
        return "shopping.score.purchaseOrder.created.1";
    }

    @Override
    public List<AchievementPersonPairing> checkAchievement(PurchaseOrderCreatedEvent event) {
        PurchaseOrder purchaseOrder = event.getPurchaseOrder();
        if (purchaseOrder == null) {
            return null;
        }
        Person receiver = purchaseOrder.getReceiver();
        if (receiver == null) {
            return null;
        }

        int numberOfPurchaseOrders = purchaseOrderService.numberOfPurchaseOrdersForReceiver(receiver);
        long timestamp = event.getCreated();

        switch (numberOfPurchaseOrders) {
            case 1:
                return Collections.singletonList(
                        achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_01, receiver, timestamp));
            case 5:
                return Collections.singletonList(
                        achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_05, receiver, timestamp));
            case 10:
                return Collections.singletonList(
                        achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_10, receiver, timestamp));
            case 25:
                return Collections.singletonList(
                        achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_25, receiver, timestamp));
            default:
                return null;
        }
    }

    @Override
    public Collection<AchievementPersonPairing> checkAchievement(Person person) {
        PurchaseOrder lastPurchaseOder = purchaseOrderService.getMostRecentPurchaseOrderForReceiver(person);
        long timestamp = lastPurchaseOder == null ? timeService.currentTimeMillisUTC() : lastPurchaseOder.getCreated();

        int numberOfPurchaseOrders = purchaseOrderService.numberOfPurchaseOrdersForReceiver(person);

        Collection<AchievementPersonPairing> achievedLevels = new ArrayList<>();

        if (numberOfPurchaseOrders >= 1) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_01, person, timestamp));
        }
        if (numberOfPurchaseOrders >= 5) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_05, person, timestamp));
        }
        if (numberOfPurchaseOrders >= 10) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_10, person, timestamp));
        }
        if (numberOfPurchaseOrders >= 25) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_25, person, timestamp));
        }

        return achievedLevels;
    }

    @Override
    public Class<PurchaseOrderCreatedEvent> getRelevantEvent() {
        return PurchaseOrderCreatedEvent.class;
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Shop.Order")
                .description("Gebe Bestellung auf um Awards zu bekommen")
                .pushCategory(findPushCategory(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_MOTIVATION_ID))
                .category("BestellBar")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel01 = AchievementLevel.builder()
                .name("Einkaufstüte")
                .description("Erste Bestellung - der Jutebeutel unter den Badges.")
                .challengeDescription("Gib deine erste Bestellung ab.")
                .icon(createIconFromDefault("einkaufstuete.png"))
                .iconNotAchieved(createIconFromDefault("einkaufstuete_grey.png"))
                .achievement(achievement)
                .orderValue(1)
                .build();
        achievementLevel01.setId(ACHIEVEMENT_LEVEL_ID_01);
        achievementLevel01 = achievementService.save(achievementLevel01);

        AchievementLevel achievementLevel05 = AchievementLevel.builder()
                .name("Großeinkauf")
                .description("Da hat sich ja ganz schön was angesammelt - Hoffentlich hast du 'nen großen Kofferraum.")
                .challengeDescription("Gib fünf Bestellungen ab.")
                .icon(createIconFromDefault("grosseinkauf.png"))
                .iconNotAchieved(createIconFromDefault("grosseinkauf_grey.png"))
                .achievement(achievement)
                .orderValue(2)
                .build();
        achievementLevel05.setId(ACHIEVEMENT_LEVEL_ID_05);
        achievementLevel05 = achievementService.save(achievementLevel05);

        AchievementLevel achievementLevel10 = AchievementLevel.builder()
                .name("Hamster")
                .description("Sorgst du vor für schlechtere Zeiten?")
                .challengeDescription("Gib zehn Bestellungen ab.")
                .icon(createIconFromDefault("hamster.png"))
                .iconNotAchieved(createIconFromDefault("hamster_grey.png"))
                .achievement(achievement)
                .orderValue(3)
                .build();
        achievementLevel10.setId(ACHIEVEMENT_LEVEL_ID_10);
        achievementLevel10 = achievementService.save(achievementLevel10);

        AchievementLevel achievementLevel25 = AchievementLevel.builder()
                .name("Shopping Queen")
                .description("Die Kreditkarte glüht! Nicht dass gleich ein Rosa-Bus vorbeikommt und dich einsammelt!")
                .challengeDescription("Gib 25 Bestellungen ab.")
                .icon(createIconFromDefault("shopping_queen.png"))
                .iconNotAchieved(createIconFromDefault("shopping_queen_grey.png"))
                .achievement(achievement)
                .orderValue(4)
                .build();
        achievementLevel25.setId(ACHIEVEMENT_LEVEL_ID_25);
        achievementLevel25 = achievementService.save(achievementLevel25);

        //only needed since the set is not updated automatically by hibernate based on the newly added achievement levels
        achievement.setAchievementLevels(Stream.of(achievementLevel01, achievementLevel05, achievementLevel10, achievementLevel25).collect(Collectors.toSet()));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_01);
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_05);
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_10);
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_25);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
