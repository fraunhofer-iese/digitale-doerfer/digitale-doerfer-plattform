/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.motivation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.business.motivation.framework.BaseAchievementRule;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderCreatedEvent;
import de.fhg.iese.dd.platform.business.shopping.services.IPurchaseOrderService;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;

@AchievementRule
public class AchievementRuleShoppingPurchaseOrderDifferentCreated extends BaseAchievementRule<PurchaseOrderCreatedEvent> {

    private static final String ACHIEVEMENT_ID =          "41509548-df84-4355-9964-559669a9b8c8";
    private static final String ACHIEVEMENT_LEVEL_ID_01 = "6f738108-181a-40a8-8e40-3fef1116a1c1";

    @Autowired
    private IPurchaseOrderService purchaseOrderService;

    @Override
    public String getName() {
        return "shopping.score.purchaseOrder.created.2";
    }

    @Override
    public List<AchievementPersonPairing> checkAchievement(PurchaseOrderCreatedEvent event) {
        PurchaseOrder purchaseOrder = event.getPurchaseOrder();
        if (purchaseOrder == null) {
            return null;
        }
        Person receiver = purchaseOrder.getReceiver();
        if (receiver == null) {
            return null;
        }

        long timestamp = event.getCreated();

        int numberOfShopsForReceiver = purchaseOrderService.numberOfShopsForReceiver(receiver);

        if(numberOfShopsForReceiver >= 10) {
            return Collections.singletonList(
                    achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_01, receiver, timestamp));
        }

        return null;
    }

    @Override
    public Collection<AchievementPersonPairing> checkAchievement(Person person) {
        PurchaseOrder lastPurchaseOder = purchaseOrderService.getMostRecentPurchaseOrderForReceiver(person);
        long timestamp = lastPurchaseOder == null ? timeService.currentTimeMillisUTC() : lastPurchaseOder.getCreated();

        int numberOfShopsForReceiver = purchaseOrderService.numberOfShopsForReceiver(person);

        Collection<AchievementPersonPairing> achievedLevels = new ArrayList<>();

        if(numberOfShopsForReceiver >= 10) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_01, person, timestamp));
        }

        return achievedLevels;
    }

    @Override
    public Class<PurchaseOrderCreatedEvent> getRelevantEvent() {
        return PurchaseOrderCreatedEvent.class;
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Shop.Order.Different")
                .description("Bestelle bei verschiedenen Geschäften um Awards zu bekommen")
                .pushCategory(findPushCategory(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_MOTIVATION_ID))
                .category("BestellBar")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel01 = AchievementLevel.builder()
                .name("Konjunkturmotor")
                .description("Der Dorfladen-Index klettert dank dir heute um satte 100 Punkte!")
                .challengeDescription("Bestelle bei zehn verschiedenen Geschäften.")
                .icon(createIconFromDefault("konjunkturmotor.png"))
                .iconNotAchieved(createIconFromDefault("konjunkturmotor_grey.png"))
                .achievement(achievement)
                .orderValue(1)
                .build();
        achievementLevel01.setId(ACHIEVEMENT_LEVEL_ID_01);
        achievementLevel01 = achievementService.save(achievementLevel01);

        //only needed since the set is not updated automatically by hibernate based on the newly added achievement levels
        achievement.setAchievementLevels(Collections.singleton(achievementLevel01));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_01);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
