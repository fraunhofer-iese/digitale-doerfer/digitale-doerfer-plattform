/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.business.shopping.events.ShopCreateConfirmation;
import de.fhg.iese.dd.platform.business.shopping.events.ShopDeleteConfirmation;
import de.fhg.iese.dd.platform.business.shopping.events.ShopDeleteRequest;
import de.fhg.iese.dd.platform.business.shopping.events.ShopSetOwnerConfirmation;
import de.fhg.iese.dd.platform.business.shopping.events.ShopUpdateConfirmation;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
public class ShoppingTeamNotificationEventProcessor extends BaseEventProcessor {

    private static final String TEAM_NOTIFICATION_TOPIC = "shopping";

    @Autowired
    private ITeamNotificationService teamNotificationService;

    @EventProcessing
    private void handleShopCreateConfirmation(ShopCreateConfirmation shopCreateConfirmation) {
        Shop shop = shopCreateConfirmation.getShop();
        Person creator = shopCreateConfirmation.getCreator();

        teamNotificationService.sendTeamNotification(
                shop.getTenant(),
                TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "Shop {} `{}` ({}) created by {} `{}` in {}",
                shop.getName(),
                shop.getId(),
                shop.getWebShopURL(),
                creator.getFullName(),
                creator.getEmail(),
                shop.getTenant().getName());
    }

    @EventProcessing
    private void handleShopUpdateConfirmation(ShopUpdateConfirmation shopUpdateConfirmation) {
        Shop shop = shopUpdateConfirmation.getShop();

        teamNotificationService.sendTeamNotification(
                shop.getTenant(),
                TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "Shop {} `{}` ({}) updated in {}",
                shop.getName(),
                shop.getId(),
                shop.getWebShopURL(),
                shop.getTenant().getName());
    }

    @EventProcessing
    private void handleShopDeleteRequest(ShopDeleteRequest shopDeleteRequest) {
        Shop shop = shopDeleteRequest.getShop();

        teamNotificationService.sendTeamNotification(
                shop.getTenant(),
                TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "Shop {} `{}` ({}) is going to be deleted in {}",
                shop.getName(),
                shop.getId(),
                shop.getWebShopURL(),
                shop.getTenant().getName());
    }

    @EventProcessing
    private void handleShopDeleteRequest(ShopDeleteConfirmation shopDeleteConfirmation) {
        Tenant tenant = shopDeleteConfirmation.getTenant();
        String shopId = shopDeleteConfirmation.getShopId();

        teamNotificationService.sendTeamNotification(
                tenant,
                TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "Shop `{}` was deleted in {}",
                shopId,
                tenant.getName());
    }

    @EventProcessing
    private void handleShopCreateConfirmation(ShopSetOwnerConfirmation shopSetOwnerConfirmation) {
        Shop shop = shopSetOwnerConfirmation.getShop();
        Person owner = shopSetOwnerConfirmation.getOwner();

        teamNotificationService.sendTeamNotification(
                shop.getTenant(),
                TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "Shop {} `{}` ({}) got owner set to {} `{}` in {}",
                shop.getName(),
                shop.getId(),
                shop.getWebShopURL(),
                owner.getFullName(),
                owner.getEmail(),
                shop.getTenant().getName());
    }

}
