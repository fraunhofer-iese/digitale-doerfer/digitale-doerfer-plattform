/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountCreditLimitExceededException;
import de.fhg.iese.dd.platform.business.motivation.services.IAccountService;
import de.fhg.iese.dd.platform.business.motivation.services.IPersonAccountService;
import de.fhg.iese.dd.platform.business.motivation.services.ITenantAccountService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.business.shopping.exceptions.PurchaseOrderNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Account;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AccountEntry;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.TenantAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AccountEntryType;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AppAccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@Service
class ShoppingScoreService extends BaseService implements IShoppingScoreService {

    private static final String ACCOUNT_ENTRY_MARKER_SALES_COMMISSION  = "sales-commision";
    private static final String ACCOUNT_ENTRY_MARKER_PURCHASE_REWARD  = "purchase-reward";

    @Autowired
    private IAccountService accountService;

    @Autowired
    private ITenantAccountService tenantAccountService;

    @Autowired
    private IPersonAccountService personAccountService;

    @Autowired
    private ITenantService tenantService;

    @Autowired
    private IPurchaseOrderService purchaseOrderService;

    @Autowired
    private IPushCategoryService pushCategoryService;

    @Override
    public void bookSalesCommission(PurchaseOrder purchaseOrder) throws AccountCreditLimitExceededException {

        Account shopAccount = getShopAccount(purchaseOrder);
        Account logisticsAccount = getLogisticsAccount(purchaseOrder);

        if(!accountService.existsAccountEntryForSubject(shopAccount, purchaseOrder, ACCOUNT_ENTRY_MARKER_SALES_COMMISSION)){
            //the receiver has no booking for that sales commission of this purchase order

            int salesCommission = purchaseOrder.getCredits();
            String postingText = "Verkaufsgebühr für "+purchaseOrder.getShopOrderId();

            accountService.bookScore(shopAccount, logisticsAccount, postingText, salesCommission,
                purchaseOrder, ACCOUNT_ENTRY_MARKER_SALES_COMMISSION, getPushCategory());
        }else{
            log.debug("Sales Commision for Purchase Order {} was already booked", purchaseOrder.getId());
        }
    }

    @Override
    @SuppressFBWarnings(value="DLS_DEAD_LOCAL_STORE", justification="Always update local variable when saving to repository")
    public void rebookWrongSalesCommission(PurchaseOrder purchaseOrder) throws AccountCreditLimitExceededException {

        Account shopAccount = getShopAccount(purchaseOrder);
        Account logisticsAccount = getLogisticsAccount(purchaseOrder);
        Account receiverAccount = getReceiverAccount(purchaseOrder);

        Optional<AccountEntry> entryOpt = accountService.getAccountEntriesForAccountByType(receiverAccount, AccountEntryType.MINUS).stream()
            .filter(e -> e.getSubjectMarker().equals(ACCOUNT_ENTRY_MARKER_SALES_COMMISSION))
            .filter(e -> e.getSubjectId().equals(purchaseOrder.getId()))
            .findFirst();

        Optional<AccountEntry> logisticsEntryOpt = accountService.getAccountEntriesForAccountByType(logisticsAccount, AccountEntryType.PLUS).stream()
            .filter(e -> e.getSubjectMarker() != null && e.getSubjectMarker().equals(ACCOUNT_ENTRY_MARKER_SALES_COMMISSION))
            .filter(e -> e.getSubjectId() != null && e.getSubjectId().equals(purchaseOrder.getId()))
            .findFirst();

        if(entryOpt.isPresent() && logisticsEntryOpt.isPresent()){
            log.debug("Sales Commision for Purchase Order {} was booked from wrong account", purchaseOrder.getId());
            AccountEntry entry = entryOpt.get();
            AccountEntry logisticsEntry = logisticsEntryOpt.get();
            entry.setAccount(shopAccount);
            entry = accountService.save(entry);
            logisticsEntry.setPartner(shopAccount);
            logisticsEntry = accountService.save(logisticsEntry);
            shopAccount.addToBalance(entry.getAmount());
            shopAccount = accountService.save(shopAccount);
            receiverAccount.addToBalance(-entry.getAmount());
            receiverAccount = accountService.save(receiverAccount);
        }
    }

    @Override
    public void rebookWrongSalesCommissions() {
        List<TenantAccount> tenantLogisticsAccounts = new ArrayList<>();
        tenantService.findAllOrderedByNameAsc()
                .forEach(c ->
                        tenantLogisticsAccounts.add(
                                tenantAccountService.createTenantAccountIfNotExisting(AppAccountType.LOGISTICS, c)));

        for (TenantAccount tenantAccount : tenantLogisticsAccounts) {
            accountService.getAccountEntriesForAccount(tenantAccount).stream()
                    .filter(e -> e.getSubjectMarker() != null &&
                            e.getSubjectMarker().equals(ACCOUNT_ENTRY_MARKER_SALES_COMMISSION))
                    .filter(e -> e.getSubjectName() != null &&
                            StringUtils.equals(e.getSubjectName(), PurchaseOrder.class.getName()))
                    .map(ae -> {
                        try {
                            return purchaseOrderService.findPurchaseOrderById(ae.getSubjectId());
                        } catch (PurchaseOrderNotFoundException e) {
                            log.error(e.getMessage());
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .distinct()
                    .forEach(this::rebookWrongSalesCommission);
        }
    }

    @Override
    public void bookPurchaseRewardBuyer(PurchaseOrder purchaseOrder) {

        Account receiverAccount = getReceiverAccount(purchaseOrder);
        Account logisticsAccount = getLogisticsAccount(purchaseOrder);

        if(!accountService.existsAccountEntryForSubject(receiverAccount, purchaseOrder, ACCOUNT_ENTRY_MARKER_PURCHASE_REWARD)){
            //the receiver has no booking for that purchase reward of this purchase order

            int motivationScore = calculateMotivationScore(purchaseOrder);
            String postingText = "Einkauf bei "+purchaseOrder.getSender().getName();

            accountService.bookScore(logisticsAccount, receiverAccount, postingText, motivationScore,
                purchaseOrder, ACCOUNT_ENTRY_MARKER_PURCHASE_REWARD, getPushCategory());
        }else{
            log.debug("Purchase Reward for Purchase Order {} was already booked", purchaseOrder.getId());
        }
    }

    private Account getLogisticsAccount(PurchaseOrder purchaseOrder) {
        Shop shop = purchaseOrder.getSender();
        Tenant shopTenant = shop.getTenant();

        return tenantAccountService.findTenantAccount(AppAccountType.LOGISTICS, shopTenant);
    }

    private Account getShopAccount(PurchaseOrder purchaseOrder){
        Shop shop = purchaseOrder.getSender();

        return personAccountService.getAccountForShop(shop);
    }

    private Account getReceiverAccount(PurchaseOrder purchaseOrder) {
        Person receiver = purchaseOrder.getReceiver();

        return personAccountService.getAccountForOwner(receiver);
    }

    @Override
    public int calculateMotivationScore(PurchaseOrder purchaseOrder) {
        return purchaseOrder.getCredits();
    }

    private PushCategory getPushCategory(){
        return pushCategoryService.findPushCategoryById(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_MOTIVATION_ID);
    }

}
