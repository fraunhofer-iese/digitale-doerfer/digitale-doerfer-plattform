/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Steffen Hupp, Balthasar Weitzel, Axel Wickenkamp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryCreatedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryCreationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForTransportRequest;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.logistics.services.ILogisticsParticipantService;
import de.fhg.iese.dd.platform.business.logistics.services.IParcelAddressService;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderCreatedEvent;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderReadyForTransportConfirmation;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderReadyForTransportRequest;
import de.fhg.iese.dd.platform.business.shopping.events.PurchaseOrderReceivedEvent;
import de.fhg.iese.dd.platform.business.shopping.exceptions.PurchaseOrderAlreadyInTransportException;
import de.fhg.iese.dd.platform.business.shopping.services.IPurchaseOrderService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrderParcel;

@EventProcessor
class PurchaseOrderEventProcessor extends BaseEventProcessor {

    @Autowired
    private IDeliveryService deliveryService;

    @Autowired
    private IPurchaseOrderService purchaseOrderService;

    @Autowired
    private IParcelAddressService parcelAddressService;

    @Autowired
    private ILogisticsParticipantService logisticsParticipantService;

    @EventProcessing
    private DeliveryCreationRequest handlePurchaseOrderReceivedEvent(PurchaseOrderReceivedEvent orderReceivedEvent) {

        Address pickupAddress = orderReceivedEvent.getPickupAddress();
        ParcelAddress pickupParcelAddress = new ParcelAddress(pickupAddress,orderReceivedEvent.getSender());
        pickupParcelAddress = parcelAddressService.store(pickupParcelAddress);

        ParcelAddress deliveryParcelAddress;

        if(orderReceivedEvent.getDeliveryPoolingStation() == null){
            //delivery goes to receiver directly
            Address deliveryAddress = orderReceivedEvent.getDeliveryAddress();
            deliveryParcelAddress = new ParcelAddress(deliveryAddress,orderReceivedEvent.getReceiver());
        }else{
            //delivery goes to pooling station
            Address deliveryAddress = orderReceivedEvent.getDeliveryPoolingStation().getAddress();
            deliveryParcelAddress = new ParcelAddress(deliveryAddress, orderReceivedEvent.getDeliveryPoolingStation());
        }
        deliveryParcelAddress = parcelAddressService.store(deliveryParcelAddress);

        PurchaseOrder purchaseOrder = PurchaseOrder.builder()
            .shopOrderId(orderReceivedEvent.getShopOrderId())
                .community(orderReceivedEvent.getTenant())
            .sender(orderReceivedEvent.getSender())
            .pickupAddress(pickupParcelAddress)
            .receiver(orderReceivedEvent.getReceiver())
            .deliveryAddress(deliveryParcelAddress)
            .desiredDeliveryTime(orderReceivedEvent.getDesiredDeliveryTime())
            .items(orderReceivedEvent.getOrderedItems())
            .purchaseOrderNotes(orderReceivedEvent.getPurchaseOrderNotes())
            .transportNotes(orderReceivedEvent.getTransportNotes())
            .credits(orderReceivedEvent.getCredits())
            .build();

        purchaseOrder = purchaseOrderService.store(purchaseOrder);

        //send no PurchaseOrderCreatedEvent yet, as the delivery is not appended yet. See handleDeliveryCreatedEvent.
        return DeliveryCreationRequest.builder()
                .tenant(purchaseOrder.getTenant())
                .sender(logisticsParticipantService.findOrCreate(purchaseOrder.getSender()))
                .receiver(logisticsParticipantService.findOrCreate(purchaseOrder.getReceiver()))
                .pickupAddress(purchaseOrder.getPickupAddress())
                .deliveryAddress(purchaseOrder.getDeliveryAddress())
                .desiredDeliveryTime(purchaseOrder.getDesiredDeliveryTime())
                .transportNotes(purchaseOrder.getTransportNotes())
                .contentNotes(purchaseOrder.getContentNotes())
                .customReferenceNumber(purchaseOrder.getShopOrderId())
                .originId(purchaseOrder.getId())
                .originType(purchaseOrder.getClass()).build();
    }

    @EventProcessing
    private void handlePurchaseOrderReadyForTransportRequest(PurchaseOrderReadyForTransportRequest event, final EventProcessingContext context) {

        PurchaseOrder purchaseOrder = event.getPurchaseOrder();
        Delivery delivery = purchaseOrder.getDelivery();
        DeliveryStatus currentStatus = deliveryService.getCurrentStatusRecord(delivery).getStatus();

        if ( currentStatus != DeliveryStatus.CREATED ){
            throw new PurchaseOrderAlreadyInTransportException("Purchase Order [shopOrderId= "+purchaseOrder.getShopOrderId()+"] already in transport");
        }

        ParcelAddress pickupParcelAddress = null;
        if(event.getPickupAddress() != null){
            Address pickupAddress = event.getPickupAddress();
            pickupParcelAddress = new ParcelAddress(pickupAddress, purchaseOrder.getSender());
            pickupParcelAddress = parcelAddressService.store(pickupParcelAddress);
        }

        if(pickupParcelAddress != null){
            delivery.setPickupAddress(pickupParcelAddress);
        }
        delivery.setSize( event.getSize() );
        delivery.setPackagingType( event.getPackagingType() );
        delivery.setContentNotes( event.getContentNotes() );
        delivery.setParcelCount(event.getNumParcels());

        delivery = deliveryService.store(delivery);

        if(pickupParcelAddress != null){
            purchaseOrder.setPickupAddress(pickupParcelAddress);
        }
        purchaseOrder.setContentNotes( event.getContentNotes() );
        purchaseOrder = purchaseOrderService.store(purchaseOrder);

        if (event.getAdditionalParcels() != null) {
            processAdditionalParcels(event, delivery, context);
        }

        notify(new DeliveryReadyForTransportRequest(delivery), context);
        notify(new PurchaseOrderReadyForTransportConfirmation(purchaseOrder), context);
    }
    
    private void processAdditionalParcels(PurchaseOrderReadyForTransportRequest request, Delivery originalDelivery, final EventProcessingContext context) {
        
        for(PurchaseOrderParcel parcel : request.getAdditionalParcels()) {
            //create Delivery via event
            //In this event processor, we have to use synchronous events to ensure correct update of the delivery
            @SuppressWarnings("deprecation")
            DeliveryCreatedEvent deliveryCreatedEvent = (DeliveryCreatedEvent)
                    notifyAndWaitForAnyReply(DeliveryCreationRequest.builder()
                                    .tenant(request.getPurchaseOrder().getTenant())
                                    .sender(logisticsParticipantService.findOrCreate(request.getPurchaseOrder().getSender()))
                                    .receiver(logisticsParticipantService.findOrCreate(request.getPurchaseOrder().getReceiver()))
                                    .pickupAddress(originalDelivery.getPickupAddress())
                                    .deliveryAddress(originalDelivery.getDeliveryAddress())
                                    .desiredDeliveryTime(originalDelivery.getDesiredDeliveryTime())
                                    .transportNotes(originalDelivery.getTransportNotes())
                                    .contentNotes(parcel.getContentNotes())
                                    .customReferenceNumber(request.getPurchaseOrder().getShopOrderId())
                                    .originId(request.getPurchaseOrder().getId())
                                    .originType(request.getPurchaseOrder().getClass()).build(),
                            DeliveryCreatedEvent.class);

            Delivery deliveryForParcel = deliveryCreatedEvent.getDelivery();
            
            //do the "same" as in handlePurchaseOrderReadyForTransportRequest in PurchaseOrderEventProcessor                
            deliveryForParcel.setSize(parcel.getSize());
            deliveryForParcel.setPackagingType(parcel.getPackagingType());
            deliveryForParcel.setContentNotes(parcel.getContentNotes());

            ParcelAddress pickupParcelAddress = null;
            if(request.getPickupAddress() != null){
                Address pickupAddress = request.getPickupAddress();
                pickupParcelAddress = new ParcelAddress(pickupAddress, request.getPurchaseOrder().getSender());
                pickupParcelAddress = parcelAddressService.store(pickupParcelAddress);
            }

            if(pickupParcelAddress != null){
                deliveryForParcel.setPickupAddress(pickupParcelAddress);
            }
            
            deliveryForParcel = deliveryService.store(deliveryForParcel);

            notify(new DeliveryReadyForTransportRequest(deliveryForParcel), context);
        }
    }

    @EventProcessing
    private PurchaseOrderCreatedEvent handleDeliveryCreatedEvent(DeliveryCreatedEvent event) {

        if(PurchaseOrder.class.isAssignableFrom(event.getOriginType())){
            PurchaseOrder purchaseOrder = purchaseOrderService.findPurchaseOrderById(event.getOriginId());
            if (purchaseOrder.getDelivery()!=null){
                return null; //delivery created for additional parcel -> do not overwrite
            }
            purchaseOrder.setDelivery(event.getDelivery());
            purchaseOrder = purchaseOrderService.store(purchaseOrder);

            return new PurchaseOrderCreatedEvent( purchaseOrder );
        }
        return null;
    }

}
