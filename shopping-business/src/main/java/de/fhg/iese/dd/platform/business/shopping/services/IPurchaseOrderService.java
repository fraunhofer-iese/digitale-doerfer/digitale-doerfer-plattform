/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Steffen Hupp, Balthasar Weitzel, Axel Wickenkamp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.services;

import java.util.List;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.shopping.exceptions.PurchaseOrderNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;

public interface IPurchaseOrderService extends IEntityService<PurchaseOrder> {

    PurchaseOrder findPurchaseOrderById(String purchaseOrderId) throws PurchaseOrderNotFoundException;

    PurchaseOrder findPurchaseOrderByIdOrReturnNull(String purchaseOrderId);

    PurchaseOrder findByShopOrderId(String shopOrderId);

    PurchaseOrder findByDelivery(Delivery delivery);

    List<PurchaseOrder> findAllByReceiver(Person person);

    List<PurchaseOrder> findAllBySender(Shop shop);

    int numberOfPurchaseOrdersForReceiver(Person receiver);

    PurchaseOrder getMostRecentPurchaseOrderForReceiver(Person receiver);

    List<PurchaseOrder> getPurchaseOrdersForReceiver(Person receiver);

    int numberOfShopsForReceiver(Person receiver);

    int numberOfPurchaseOrdersAtShopForReceiver(Shop shop, Person receiver);

    List<Shop> shopsForReceiverWithNumberOfOrdersGreaterThan(Person receiver, int numberOfOrders);

}
