/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.dataprivacy;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shopping.services.IPurchaseOrderService;
import de.fhg.iese.dd.platform.business.shopping.services.IShoppingDataDeletionService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrderItem;

@Component
public class ShoppingDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(Person.class, App.class, Delivery.class);

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(PurchaseOrder.class);

    @Autowired
    private IPurchaseOrderService purchaseOrderService;

    @Autowired
    private IShoppingDataDeletionService shoppingDataDeletionService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public void collectUserData(Person person, IDataPrivacyReport dataPrivacyReport) {

        List<PurchaseOrder> ownPurchaseOrders = purchaseOrderService.findAllByReceiver(person);

        if (CollectionUtils.isEmpty(ownPurchaseOrders)) {
            return;
        }

        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("BestellBar", "Bestellungen aus BestellBar");

        //we want to have the oldest on top
        Collections.reverse(ownPurchaseOrders);

        for (PurchaseOrder purchaseOrder : ownPurchaseOrders) {
            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection("Bestellung",
                            "Bestellt am " + timeService.toLocalTimeHumanReadable(purchaseOrder.getCreated()) +
                                    " bei " +
                                    purchaseOrder.getSender().getName());

            subSection.newContent("Lieferadresse",
                    purchaseOrder.getDeliveryAddress().getAddress().toHumanReadableString(true));

            subSection.newContent("Inhalt", purchaseOrder.getItems().stream()
                    .map(pi -> pi.getAmount() + " " + pi.getUnit() + " " + pi.getItemName())
                    .collect(Collectors.joining(",\n", "", "")));

            subSection.newContent("Notizen an Händler", purchaseOrder.getPurchaseOrderNotes());
            subSection.newContent("Notizen an Lieferanten", purchaseOrder.getTransportNotes());

            if(purchaseOrder.getDesiredDeliveryTime() != null) {
                subSection.newContent("Gewünschter Lieferzeitraum",
                        timeService.toLocalTimeHumanReadable(purchaseOrder.getDesiredDeliveryTime().getStartTime()) +
                                " - " +
                                timeService.toLocalTimeHumanReadable(
                                        purchaseOrder.getDesiredDeliveryTime().getEndTime()));
            }
        }
    }

    @Override
    public void deleteUserData(Person person, IPersonDeletionReport personDeletionReport) {

        List<PurchaseOrder> ownPurchaseOrders = purchaseOrderService.findAllByReceiver(person);
        for (PurchaseOrder purchaseOrder : ownPurchaseOrders) {
            final List<PurchaseOrderItem> items = purchaseOrder.getItems();
            DeleteOperation deleteOperation = shoppingDataDeletionService.deletePurchaseOrder(purchaseOrder);
            personDeletionReport.addEntries(items, deleteOperation);
            personDeletionReport.addEntry(purchaseOrder, deleteOperation);
        }
    }

}
