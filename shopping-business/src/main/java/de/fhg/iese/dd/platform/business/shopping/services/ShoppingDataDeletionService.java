/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import de.fhg.iese.dd.platform.datamanagement.shopping.repos.PurchaseOrderRepository;

@Service
class ShoppingDataDeletionService implements IShoppingDataDeletionService {

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Override
    public DeleteOperation deletePurchaseOrder(PurchaseOrder purchaseOrder) {
        //PurchaseOrder.items has Cascade All activated, so deleting the PO is enough
        purchaseOrderRepository.delete(purchaseOrder);
        return DeleteOperation.DELETED;
    }

}
