/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Alberto Lara
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping.init;

import java.util.Collection;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.logistics.init.DeliveryManualTestDataInitializer;
import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;

@Component
public class PurchaseOrderManualTestDataInitializer extends BasePurchaseOrderDataInitializer implements IManualTestDataInitializer {

    public static final String PURCHASE_ORDER_TEST_DEMO_ID_01 = "0737f1c9-c7b9-482c-a7bd-9e2dd668f9bf";
    public static final String PURCHASE_ORDER_TEST_DEMO_ID_02 = "8bbcc198-705e-463e-b120-a4562c7d73fd";
    public static final String PURCHASE_ORDER_TEST_DEMO_ID_03 = "ba9143d1-e584-4bec-95fe-a6ccdc9e3f9b";
    public static final String PURCHASE_ORDER_TEST_DEMO_ID_04 = "3bc142fa-7e7e-484e-90f3-077425ddbf90";

    @Override
    public String getTopic() {
        return "shopping-manual-test";
    }

    @Override
    public String getScenarioId() {
        //contributes to several scenarios
        return null;
    }

    @Override
    public Collection<String> getDependentTopics() {
        return unmodifiableList("person-manual-test","shop-manual-test");
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {
        newPurchaseOrder(logSummary,
                "TEST_DEMO_01",
                PURCHASE_ORDER_TEST_DEMO_ID_01,
                DeliveryManualTestDataInitializer.DELIVERY_TEST_DEMO_ID_01);

        newPurchaseOrder(logSummary,
                "TEST_DEMO_02",
                PURCHASE_ORDER_TEST_DEMO_ID_02,
                DeliveryManualTestDataInitializer.DELIVERY_TEST_DEMO_ID_02);

        newPurchaseOrder(logSummary,
                "TEST_DEMO_03",
                PURCHASE_ORDER_TEST_DEMO_ID_03,
                DeliveryManualTestDataInitializer.DELIVERY_TEST_DEMO_ID_03);

        newPurchaseOrder(logSummary,
                "TEST_DEMO_04",
                PURCHASE_ORDER_TEST_DEMO_ID_04,
                DeliveryManualTestDataInitializer.DELIVERY_TEST_DEMO_ID_04);
    }

    @Override
    public void dropDataSimple(LogSummary logSummary) {
        demoShoppingService.deleteDataForDemoTenant(LogisticsConstants.TENANT_ID_DEMO);
        logSummary.info("dropped all shopping data for demo tenant");
    }

}
