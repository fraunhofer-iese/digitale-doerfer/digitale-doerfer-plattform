<#include "/mailHeader.ftl">
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 125%;">

                            <h1 class="null" style="text-align: center;"><span style="font-family:lato,helvetica neue,helvetica,arial,sans-serif"><span style="color:#696969"><span style="font-size:12px">Neues aus der BestellBar</span></span><br>
<strong><span style="color:#DE3B5D">Lieferung wurde übernommen!</span></strong></span></h1>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->

				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 27px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                            <h2 class="null" style="text-align: left;">Hallo ${shop.name}!</h2>
                            <p style="text-align: left;">
                                <p><strong>${carrier.fullName}</strong> übernimmt die Lieferung der folgenden Bestellung:</p>

                                <p><strong>Bestell-Nr.:</strong><br />
                                ${purchaseOrder.shopOrderId}</p>

                                <p><strong>Empfänger:</strong><br />
                                ${delivery.deliveryAddress.address.name}<br />
                                ${delivery.deliveryAddress.address.street}<br />
                                ${delivery.deliveryAddress.address.zip} ${delivery.deliveryAddress.address.city}</p>

                                <p><strong>Lieferzeitraum:</strong><br />
                                ${desiredDeliveryTime}.<br />
                                ${carrier.fullName} wird das Paket voraussichtlich bis spätestens ${desiredPickupTime} bei Ihnen abholen.</p>

                                <p><strong>Paket-Code:</strong><br />
                                ${delivery.trackingCode}</p>
                                
                                <p>Wichtig: Bitte lassen Sie ${carrier.fullName} den <strong>Erhalt des Paketes</strong> durch Scannen des QR-Codes auf dem Paket <strong>mit der LieferBar-App bestätigen</strong>.</p>
                            
                                <p>Mit freundlichen Grüßen,<br />
                                Das Digitale Dörfer Logistiksystem</p>
                            </p>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->

				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table>
<#include "/mailFooter.ftl">