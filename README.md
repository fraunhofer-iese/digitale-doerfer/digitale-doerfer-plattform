# Welcome to the Digitale Dörfer Plattform

This is the Digitale Dörfer platform project, serving as basis for all projects in the Digitale Dörfer context.

Created by Fraunhofer IESE 2015 - 2024

## Structure

### Domains

The platform currently consists of the following domains:

* shared
  * _base infrastructure_
* communication
  * _chat functionality_
* motivation
  * _achievements and motivation "currency"_
* grapevine
  * _DorfFunk_
* contentintegration
  * _Data ingestion for DorfFunk_
* logistics
  * _LieferBar_
* shopping
  * _BestellBar to LieferBar integration_
* mobile-kiosk
  * _Special version of BestellBar and LieferBar_

### Internal structure and layering

Every domain is structured in this way:

* api
  * clientevent
    * _Events from and to the clients_
  * clientmodel
    * _Data Transfer Objects_
  * exceptions
      * _Exceptions used towards the clients_
  * controllers
    * _Realization of REST endpoints_
  * processors
    * _Event processors directly sending push messages to clients_
* business
  * admintasks
    * _Admin tasks encapsulating typical rarely performed scripted tasks_
  * datadeletion
    * _Handling of reference data deletion_
  * dataprivacy
    * _Handling of GDPR requests (deletion and data report)_
  * events
    * _Business events, mainly triggered by client events, processed by event processors_
  * exceptions
    * _Business exceptions_
  * init
    * _Initialization of reference data_
  * processors
    * _Event processors_
  * security
    * _Authorization rules_
  * services
    * _Business services_
  * statistics
    * _Statistics providers_
  * worker
    * _Worker tasks, usually triggered periodically_
* data
  * config
    * _Configuration access_
  * feature
    * _Feature configuration_
  * model
    * _Data entities_
  * repos
      * _Repositories for access to database_
  * roles
    * _Roles for authorization_

## Packaging & Deployment

The platform is currently packaged in one application `rest-application.war`. This file is ready to be used in an Apache
Tomcat, especially in an AWS ElasticBeanstalk environment.

## License

[Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0)

## Used Libraries

The platform uses various libraries, they are only linked and not included in the source code.
Refer to the pom.xml for an overview of the used library versions.

### Special Notice about Libraries with LGPL license

Libraries published under LGPL 2.1 are used by the platform via linking mechanisms. This is allowed according to the
license, see [GNU Lesser General Public License, Version 2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html),
section 6 and 6.b since they are explicitly mentioned, a proper shared library mechanism is used and we link to a copy
of the license.
The relevant libraries are:

* [openhtmltopdf-core](https://github.com/danfickle/openhtmltopdf/openhtmltopdf-core)
* [openhtmltopdf-pdfbox](https://github.com/danfickle/openhtmltopdf/openhtmltopdf-pdfbox)
* [spotbugs-annotations](https://spotbugs.github.io/)

See the list below for details about them.

### Overview of all used libraries

This is a list of the used libraries, their maven identifier, the description as provided by the libraries authors and
their license(s).

* [commons-compress](https://commons.apache.org/proper/commons-compress/)
    * `org.apache.commons:commons-compress`
    * Apache Commons Compress defines an API for working with compression and archive formats. These include bzip2,
      gzip, pack200, LZMA, XZ, Snappy, traditional Unix Compress, DEFLATE, DEFLATE64, LZ4, Brotli, Zstandard and ar,
      cpio, jar, tar, zip, dump, 7z, arj.
    * Published under Apache-2.0
* [commons-io](https://commons.apache.org/proper/commons-io/)
    * `commons-io:commons-io`
    * The Apache Commons IO library contains utility classes, stream implementations, file filters, file comparators,
      endian transformation classes, and much more.
    * Published under Apache-2.0
* [commons-lang3](https://commons.apache.org/proper/commons-lang/)
    * `org.apache.commons:commons-lang3`
    * Apache Commons Lang, a package of Java utility classes for the classes that are in java.lang's hierarchy, or are
      considered to be so standard as to justify existence in java.lang.
    * Published under Apache-2.0
* [freemarker](https://freemarker.apache.org/)
    * `org.freemarker:freemarker`
    * FreeMarker is a "template engine"; a generic tool to generate text output based on templates.
    * Published under Apache-2.0
* [log4j-api](https://logging.apache.org/log4j/2.x/log4j/log4j-api/)
    * `org.apache.logging.log4j:log4j-api`
    * The Apache Log4j API
    * Published under Apache-2.0
* [log4j-core](https://logging.apache.org/log4j/2.x/log4j/log4j-core/)
    * `org.apache.logging.log4j:log4j-core`
    * The Apache Log4j Implementation
    * Published under Apache-2.0
* [log4j-slf4j-impl](https://logging.apache.org/log4j/2.x/log4j-slf4j-impl/)
    * `org.apache.logging.log4j:log4j-slf4j-impl`
    * The Apache Log4j SLF4J API binding to Log4j 2 Core
    * Published under Apache-2.0
* [log4j-web](https://logging.apache.org/log4j/2.x/log4j/log4j-web/)
    * `org.apache.logging.log4j:log4j-web`
    * The Apache Log4j support for web servlet containers
    * Published under Apache-2.0
* [tika-core](https://tika.apache.org/)
    * `org.apache.tika:tika-core`
    * This is the core Apache Tika™ toolkit library from which all other modules inherit functionality. It also includes
      the core facades for the Tika API.
    * Published under Apache-2.0
* [archunit-junit5](https://github.com/TNG/ArchUnit)
    * `com.tngtech.archunit:archunit-junit5`
    * A Java architecture test library, to specify and assert architecture rules in plain Java - Module '
      archunit-junit5'
    * Published under Apache-2.0
* [auth0](https://github.com/auth0/auth0-java)
    * `com.auth0:auth0`
    * Java client library for the Auth0 platform.
    * Published under MIT License
* [auth0-spring-security-api](https://github.com/auth0/auth0-spring-security-api)
    * `com.auth0:auth0-spring-security-api`
    * Auth0 Java Spring integration for API
    * Published under MIT License
* [auth](https://aws.amazon.com/sdkforjava)
    * `software.amazon.awssdk:auth`
    * The AWS SDK for Java - Auth module holds the classes that are used for authentication with services
    * Published under Apache-2.0
* [regions](https://aws.amazon.com/sdkforjava/core/regions)
    * `software.amazon.awssdk:regions`
    * The AWS SDK for Java - Core is an umbrella module that contains child modules which are considered as core of the
      library.
    * Published under Apache-2.0
* [cloudwatch](https://aws.amazon.com/sdkforjava)
    * `software.amazon.awssdk:cloudwatch`
    * The AWS Java SDK for Amazon CloudWatch module holds the client classes that are used for communicating with Amazon
      CloudWatch Service
    * Published under Apache-2.0
* [cloudwatchlogs](https://aws.amazon.com/sdkforjava)
    * `software.amazon.awssdk:cloudwatchlogs`
    * The AWS Java SDK for Amazon CloudWatch Logs module holds the client classes that are used for communicating with
      Amazon CloudWatch Logs Service
    * Published under Apache-2.0
* [pinpoint](https://aws.amazon.com/sdkforjava)
    * `software.amazon.awssdk:pinpoint`
    * The AWS Java SDK for Amazon Pinpoint module holds the client classes that are used for communicating with Amazon
      Pinpoint Service
    * Published under Apache-2.0
* [s3](https://aws.amazon.com/sdkforjava)
    * `software.amazon.awssdk:s3`
    * The AWS Java SDK for Amazon S3 module holds the client classes that are used for communicating with Amazon Simple
      Storage Service
    * Published under Apache-2.0
* [metadata-extractor](https://drewnoakes.com/code/exif/)
    * `com.drewnoakes:metadata-extractor`
    * Java library for extracting EXIF, IPTC, XMP, ICC and other metadata from image and video files.
    * Published under Apache-2.0
* [guava](https://github.com/google/guava)
    * `com.google.guava:guava`
    * Guava is a suite of core and expanded libraries that include utility classes, Google's collections, I/O classes,
      and much more.
    * Published under Apache-2.0
* [h2](https://h2database.com)
    * `com.h2database:h2`
    * H2 Database Engine
    * Published under EPL 1.0
    * Published under MPL 2.0
* [imgscalr-lib](http://www.thebuzzmedia.com/software/imgscalr-java-image-scaling-library/)
    * `org.imgscalr:imgscalr-lib`
    * imgscalr is an simple and efficient best-practices image-scaling and manipulation library implemented in pure
      Java.
    * Published under Apache-2.0
* [jackson-dataformat-smile](http://github.com/FasterXML/jackson-dataformats-binary)
    * `com.fasterxml.jackson.dataformat:jackson-dataformat-smile`
    * Support for reading and writing Smile ("binary JSON") encoded data using Jackson abstractions (streaming API, data
      binding, tree model)
    * Published under Apache-2.0
* [jackson-datatype-jdk8](https://github.com/FasterXML/jackson-modules-java8/jackson-datatype-jdk8)
    * `com.fasterxml.jackson.datatype:jackson-datatype-jdk8`
    * Add-on module for Jackson (http://jackson.codehaus.org) to support JDK 8 data types.
    * Published under Apache-2.0
* [jackson-module-kotlin](https://github.com/FasterXML/jackson-module-kotlin)
    * `com.fasterxml.jackson.module:jackson-module-kotlin`
    * Add-on module for Jackson (https://github.com/FasterXML/jackson/) to support Kotlin language, specifically
      introspection of method/constructor parameter names, without having to add explicit property name annotation.
    * Published under Apache-2.0
* [google-maps-services](https://github.com/googlemaps/google-maps-services-java)
    * `com.google.maps:google-maps-services`
    * Use the Google Maps Platform Web Services in Java! https://developers.google.com/maps/documentation/webservices/
    * Published under Apache-2.0
* [java-jwt](https://github.com/auth0/java-jwt)
    * `com.auth0:java-jwt`
    * Java implementation of JSON Web Token (JWT)
    * Published under MIT License
* [org.eclipse.jgit](https://www.eclipse.org/jgit//org.eclipse.jgit)
    * `org.eclipse.jgit:org.eclipse.jgit`
    * Repository access and algorithms
    * Published under BSD-3-Clause
* [json-path](https://github.com/jayway/JsonPath)
    * `com.jayway.jsonpath:json-path`
    * A library to query and verify JSON
    * Published under Apache-2.0
* [jsoup](https://jsoup.org/)
    * `org.jsoup:jsoup`
    * jsoup is a Java library that simplifies working with real-world HTML and XML. It offers an easy-to-use API for URL
      fetching, data parsing, extraction, and manipulation using DOM API methods, CSS, and xpath selectors. jsoup
      implements the WHATWG HTML5 specification, and parses HTML to the same DOM as modern browsers.
    * Published under MIT License
* [jul-to-slf4j](http://www.slf4j.org)
    * `org.slf4j:jul-to-slf4j`
    * JUL to SLF4J bridge
    * Published under MIT License
* [liquibase-core](http://www.liquibase.com)
    * `org.liquibase:liquibase-core`
    * Liquibase is a tool for managing and executing database changes.
    * Published under Apache-2.0
* [micrometer-registry-cloudwatch2](https://github.com/micrometer-metrics/micrometer)
    * `io.micrometer:micrometer-registry-cloudwatch2`
    * Application monitoring instrumentation facade
    * Published under Apache-2.0
* [mockito-core](https://github.com/mockito/mockito)
    * `org.mockito:mockito-core`
    * Mockito mock objects library core API and implementation
    * Published under MIT License
* [mockwebserver](https://square.github.io/okhttp/)
    * `com.squareup.okhttp3:mockwebserver`
    * Square’s meticulous HTTP client for Java and Kotlin.
    * Published under Apache-2.0
* [mysql-connector-j](http://dev.mysql.com/doc/connector-j/en/)
    * `com.mysql:mysql-connector-j`
    * JDBC Type 4 driver for MySQL.
    * Published under The GNU General Public License, v2 with Universal FOSS Exception, v1.0
* [reactor-core](https://github.com/reactor/reactor-core)
    * `io.projectreactor:reactor-core`
    * Non-Blocking Reactive Foundation for the JVM
    * Published under Apache-2.0
* [okhttp](https://square.github.io/okhttp/)
    * `com.squareup.okhttp3:okhttp`
    * Square’s meticulous HTTP client for Java and Kotlin.
    * Published under Apache-2.0
* [opencsv](http://opencsv.sf.net)
    * `com.opencsv:opencsv`
    * A simple library for reading and writing CSV in Java
    * Published under Apache-2.0
* [openhtmltopdf-core](https://github.com/danfickle/openhtmltopdf/openhtmltopdf-core)
    * `com.openhtmltopdf:openhtmltopdf-core`
    * Open HTML to PDF is a CSS 2.1 renderer written in Java. This artifact contains the core rendering and layout code.
    * Published under LGPL-2.1
* [openhtmltopdf-pdfbox](https://github.com/danfickle/openhtmltopdf/openhtmltopdf-pdfbox)
    * `com.openhtmltopdf:openhtmltopdf-pdfbox`
    * Openhtmltopdf is a CSS 2.1 renderer written in Java. This artifact supports PDF output with Apache PDF-BOX 2.
    * Published under LGPL-2.1
* [jts-io-common](https://www.locationtech.org/projects/technology.jts/jts-modules/jts-io/jts-io-common)
    * `org.locationtech.jts.io:jts-io-common`
    * The JTS Topology Suite is an API for 2D linear geometry predicates and functions.
    * Published under EDL 1.0
    * Published under Eclipse Public License, Version 2.0
* [jts-core](https://www.locationtech.org/projects/technology.jts/jts-modules/jts-core)
    * `org.locationtech.jts:jts-core`
    * The JTS Topology Suite is an API for 2D linear geometry predicates and functions.
    * Published under EDL 1.0
    * Published under Eclipse Public License, Version 2.0
* [lombok](https://projectlombok.org)
    * `org.projectlombok:lombok`
    * Spice up your java: Automatic Resource Management, automatic generation of getters, setters, equals, hashCode and
      toString, and more!
    * Published under MIT License
* [reactor-netty](https://github.com/reactor/reactor-netty)
    * `io.projectreactor.netty:reactor-netty`
    * Reactor Netty with all modules
    * Published under Apache-2.0
* [reflections](http://github.com/ronmamo/reflections)
    * `org.reflections:reflections`
    * Reflections - Java runtime metadata analysis
    * Published under Apache-2.0
    * Published under WTFPL
* [sardine](https://github.com/lookfirst/sardine)
    * `com.github.lookfirst:sardine`
    * An easy to use WebDAV client for Java
    * Published under Apache-2.0
* [spatial4j](https://projects.eclipse.org/projects/locationtech.spatial4j)
    * `org.locationtech.spatial4j:spatial4j`
    * Spatial4j is a general purpose spatial / geospatial ASL licensed open-source Java library. It's core capabilities
      are 3-fold: to provide common geospatially-aware shapes, to provide distance calculations and other math, and to
      read shape formats like WKT and GeoJSON.
    * Published under Apache-2.0
* [spotbugs-annotations](https://spotbugs.github.io/)
    * `com.github.spotbugs:spotbugs-annotations`
    * Annotations the SpotBugs tool supports
    * Published under LGPL-2.1
* [spring-data-elasticsearch](https://github.com/spring-projects/spring-data-elasticsearch)
    * `org.springframework.data:spring-data-elasticsearch`
    * Spring Data Implementation for Elasticsearch
    * Published under Apache-2.0
* [spring-webflux](https://github.com/spring-projects/spring-framework)
    * `org.springframework:spring-webflux`
    * Spring WebFlux
    * Published under Apache-2.0
* [spring-boot-autoconfigure](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-autoconfigure`
    * Spring Boot AutoConfigure
    * Published under Apache-2.0
* [spring-boot-configuration-processor](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-configuration-processor`
    * Spring Boot Configuration Annotation Processor
    * Published under Apache-2.0
* [spring-boot-starter](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-starter`
    * Core starter, including auto-configuration support, logging and YAML
    * Published under Apache-2.0
* [spring-boot-starter-actuator](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-starter-actuator`
    * Starter for using Spring Boot's Actuator which provides production ready features to help you monitor and manage
      your application
    * Published under Apache-2.0
* [spring-boot-starter-data-jpa](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-starter-data-jpa`
    * Starter for using Spring Data JPA with Hibernate
    * Published under Apache-2.0
* [spring-boot-starter-jdbc](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-starter-jdbc`
    * Starter for using JDBC with the HikariCP connection pool
    * Published under Apache-2.0
* [spring-boot-starter-log4j2](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-starter-log4j2`
    * Starter for using Log4j2 for logging. An alternative to spring-boot-starter-logging
    * Published under Apache-2.0
* [spring-boot-starter-mail](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-starter-mail`
    * Starter for using Java Mail and Spring Framework's email sending support
    * Published under Apache-2.0
* [spring-boot-starter-security](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-starter-security`
    * Starter for using Spring Security
    * Published under Apache-2.0
* [spring-boot-starter-test](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-starter-test`
    * Starter for testing Spring Boot applications with libraries including JUnit Jupiter, Hamcrest and Mockito
    * Published under Apache-2.0
* [spring-boot-starter-thymeleaf](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-starter-thymeleaf`
    * Starter for building MVC web applications using Thymeleaf views
    * Published under Apache-2.0
* [spring-boot-starter-tomcat](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-starter-tomcat`
    * Starter for using Tomcat as the embedded servlet container. Default servlet container starter used by
      spring-boot-starter-web
    * Published under Apache-2.0
* [spring-boot-starter-validation](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-starter-validation`
    * Starter for using Java Bean Validation with Hibernate Validator
    * Published under Apache-2.0
* [spring-boot-starter-web](https://spring.io/projects/spring-boot)
    * `org.springframework.boot:spring-boot-starter-web`
    * Starter for building web, including RESTful, applications using Spring MVC. Uses Tomcat as the default embedded
      container
    * Published under Apache-2.0
* [spring-security-test](https://spring.io/projects/spring-security)
    * `org.springframework.security:spring-security-test`
    * Spring Security
    * Published under Apache-2.0
* [springfox-swagger-ui](https://github.com/springfox/springfox)
    * `io.springfox:springfox-swagger-ui`
    * JSON API documentation for spring based applications
    * Published under Apache-2.0
* [springfox-swagger2](https://github.com/springfox/springfox)
    * `io.springfox:springfox-swagger2`
    * JSON API documentation for spring based applications
    * Published under Apache-2.0
* [elasticsearch](https://java.testcontainers.org)
    * `org.testcontainers:elasticsearch`
    * Isolated container management for Java code testing
    * Published under MIT License
* [imageio-jpeg](https://github.com/haraldk/TwelveMonkeys/tree/master/imageio/imageio-jpeg)
    * `com.twelvemonkeys.imageio:imageio-jpeg`
    * ImageIO plugin for Joint Photographer Expert Group images (JPEG/JFIF).
    * Published under BSD-3-Clause
* [imageio-tiff](https://github.com/haraldk/TwelveMonkeys/tree/master/imageio/imageio-tiff)
    * `com.twelvemonkeys.imageio:imageio-tiff`
    * ImageIO plugin for Aldus/Adobe Tagged Image File Format (TIFF).
    * Published under BSD-3-Clause
* [imageio-webp](https://github.com/haraldk/TwelveMonkeys/tree/master/imageio/imageio-webp)
    * `com.twelvemonkeys.imageio:imageio-webp`
    * ImageIO plugin for Google WebP File Format (WebP).
    * Published under BSD-3-Clause
* [twilio](https://www.twilio.com)
    * `com.twilio.sdk:twilio`
    * Twilio Java Helper Library
    * Published under MIT License
* [core](https://github.com/zxing/zxing/core)
    * `com.google.zxing:core`
    * Core barcode encoding/decoding library
    * Published under Apache-2.0
* [javase](https://github.com/zxing/zxing/javase)
    * `com.google.zxing:javase`
    * Java SE-specific extensions to core ZXing library
    * Published under Apache-2.0
