/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Mher Ter-Tovmasyan, Steffen Hupp, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.services;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.exceptions.EventProcessingException;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.grapevine.events.BaseExternalPostChangeByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.BaseExternalPostCreateByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.PostChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningChangeByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningCreateByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.newsitem.NewsItemChangeByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.newsitem.NewsItemCreateByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.config.ContentIntegrationConfig;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.CrawlingConfig;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.repos.CrawlingConfigRepository;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IParallelismService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.HappeningPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.NewsItemPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ExternalPostRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileDownloadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileDownloadService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
class CrawlingService extends BaseService implements ICrawlingService {

    private static final Marker LOG_MARKER_CONTENT_MAGNET = MarkerManager.getMarker("CONTENT_MAGNET");

    @Autowired
    private ContentIntegrationConfig contentIntegrationConfig;
    @Autowired
    private IWebFeedScraperService webFeedScraperService;
    @Autowired
    private IFeatureService featureService;
    @Autowired
    private IAppService appService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private IFileDownloadService fileDownloadService;
    @Autowired
    private IParallelismService parallelismService;
    @Autowired
    private ITimeService timeService;
    @Autowired
    private ExternalPostRepository externalPostRepository;
    @Autowired
    private CrawlingConfigRepository crawlingConfigRepository;

    @Override
    public void createPostsFromExternalContent(LogSummary logSummary, boolean publishImmediately) {

        logSummary.setLogMarker(LOG_MARKER_CONTENT_MAGNET);
        logSummary.setLogger(log);
        List<CrawlingConfig> crawlingConfigs = crawlingConfigRepository.findAll();
        crawlingConfigs.sort(Comparator.comparing(c -> BaseEntity.getIdOf(c.getNewsSource())));
        //ensures that we use the cached geo areas when we sent them as events
        crawlingConfigs.forEach(
                crawlingConfig -> crawlingConfig.setGeoAreas(geoAreaService.forceCached(crawlingConfig.getGeoAreas())));

        Instant nextCrawlingConfigProcessingTime = timeService.nowLocal().toInstant();

        for (CrawlingConfig crawlingConfig : crawlingConfigs) {
            NewsSource newsSource = crawlingConfig.getNewsSource();
            AppVariant appVariant = newsSource.getAppVariant();
            if (newsSource.isDeleted()) {
                logSummary.info("Crawling config '{}' references deleted news source '{}'", crawlingConfig.getId(),
                        BaseEntity.getIdOf(newsSource));
            }
            if (appVariant == null) {
                logSummary.info("Crawling config '{}' references news source '{}' without app variant",
                        crawlingConfig.getId(),
                        BaseEntity.getIdOf(newsSource));
            }
            PostType postType = crawlingConfig.getExternalPostType();
            logSummary.info("Processing crawling config '{}' for news source '{}' for {}", crawlingConfig.getId(),
                    BaseEntity.getIdOf(newsSource), postType);
            logSummary.indent();

            if (postType != PostType.NewsItem && postType != PostType.Happening) {
                logSummary.error("Invalid post type {} at crawling config '{}' for news source '{}'", postType,
                        crawlingConfig.getId(), BaseEntity.getIdOf(newsSource));
                continue;
            }
            // we want a bit of delay between the crawling runs, so that not all posts are created at once
            parallelismService.scheduleOneTimeTask(
                    () -> processCrawlingConfig(crawlingConfig, publishImmediately, logSummary),
                    nextCrawlingConfigProcessingTime);
            nextCrawlingConfigProcessingTime = nextCrawlingConfigProcessingTime
                    .plus(contentIntegrationConfig.getDelayBetweenCrawling());
        }
    }

    private void processCrawlingConfig(CrawlingConfig crawlingConfig, boolean publishImmediately, LogSummary logSummary) {

        NewsSource newsSource = crawlingConfig.getNewsSource();
        AppVariant appVariant = newsSource.getAppVariant();
        PostType postType = crawlingConfig.getExternalPostType();
        int maxTextLength = getMaxTextLength(postType, appVariant);
        try {
            List<WebFeedItemScrapingResult> contentItemsFromWebFeed = webFeedScraperService.scrapeWebFeed(
                    crawlingConfig.getSourceUrl(),
                    crawlingConfig.getAccessType(),
                    crawlingConfig.getWebFeedParameters(),
                    logSummary);
            List<BaseEvent> createOrUpdateRequests = new ArrayList<>(contentItemsFromWebFeed.size());
            List<ScheduledFuture<BaseEvent>> replyFutures = new ArrayList<>(contentItemsFromWebFeed.size());
            long delayBetweenPostRequests = 0;
            for (int i = 0; i < contentItemsFromWebFeed.size(); i++) {
                WebFeedItemScrapingResult feedItem = contentItemsFromWebFeed.get(i);

                switch (postType) {
                    case NewsItem:
                        if (expectedEntriesAreBlank(i + 1, feedItem, logSummary, "postURL", "text")) {
                            continue;
                        }
                        break;
                    case Happening:
                        if (expectedEntriesAreBlank(i + 1, feedItem, logSummary, "postURL", "text", "startTime")) {
                            continue;
                        }
                        break;
                    default:
                        log.warn("Illegal post type {} at feed item {}", postType, i);
                        continue;
                }
                //limit the text to the configured limit without simply rejecting it
                String text = StringUtils.truncate(feedItem.getStringScrapingResultValue("text", ", ", true),
                        maxTextLength);
                String postURLString = feedItem.getStringScrapingResultValue("postURL");
                String authorString = feedItem.getStringScrapingResultValue("author", ", ", false);
                String categoriesString = feedItem.getStringScrapingResultValue("categories", ", ", false);
                List<String> imageURLStrings = feedItem.getListStringScrapingResultValue("imageURIs");

                @SuppressWarnings("UnnecessaryLocalVariable")
                String externalId = postURLString;
                ExternalPost existingPost = externalPostRepository
                        .findByNewsSourceAndExternalId(newsSource, externalId)
                        .orElse(null);

                BaseEvent request = null;
                switch (postType) {
                    case NewsItem:
                        if (existingPost == null) {
                            request = NewsItemCreateByNewsSourceRequest.builder()
                                    .appVariant(appVariant)
                                    .geoAreas(crawlingConfig.getGeoAreas())
                                    .newsSource(newsSource)
                                    .organization(crawlingConfig.getOrganization())
                                    .externalId(externalId)
                                    .postURL(postURLString)
                                    .authorName(authorString)
                                    .text(text)
                                    .categories(categoriesString)
                                    .imageURLs(getImageURLs(imageURLStrings, logSummary))
                                    .publishImmediately(publishImmediately)
                                    .commentsDisallowed(crawlingConfig.isCommentsDisallowed())
                                    .logMarker(LOG_MARKER_CONTENT_MAGNET)
                                    .build();
                        } else {
                            if (!(existingPost instanceof NewsItem)) {
                                logSummary.warn(
                                        "Existing item is not of type NewsItem at crawling config '{}' for news source '{}' for crawled item {} with external id '{}'",
                                        crawlingConfig.getId(),
                                        BaseEntity.getIdOf(newsSource),
                                        i,
                                        externalId);
                                continue;
                            }
                            NewsItemChangeByNewsSourceRequest changeRequest =
                                    NewsItemChangeByNewsSourceRequest.builder()
                                            .post((NewsItem) existingPost)
                                            .appVariant(appVariant)
                                            .geoAreas(crawlingConfig.getGeoAreas())
                                            .organization(crawlingConfig.getOrganization())
                                            .postURL(postURLString)
                                            .authorName(authorString)
                                            .text(text)
                                            .categories(categoriesString)
                                            .imageURLs(getImageURLs(imageURLStrings, logSummary))
                                            .publishImmediately(publishImmediately)
                                            .commentsDisallowed(crawlingConfig.isCommentsDisallowed())
                                            .logMarker(LOG_MARKER_CONTENT_MAGNET)
                                            .build();
                            if (externalPostNeedsUpdate(existingPost, changeRequest)) {
                                request = changeRequest;
                            }
                        }
                        break;
                    case Happening:
                        Long startTime = feedItem.getLongScrapingResultValue("startTime", -1L);
                        Long endTime = feedItem.getLongScrapingResultValue("endTime", -1L);
                        String allDayEventString = feedItem.getStringScrapingResultValue("allDayEvent");
                        String organizerString = feedItem.getStringScrapingResultValue("organizer", ", ", false);

                        boolean allDayEvent = Boolean.parseBoolean(allDayEventString) ||
                                (startTime > 0 && endTime == -1);

                        if (existingPost == null) {
                            request = HappeningCreateByNewsSourceRequest.builder()
                                    .startTime(startTime)
                                    .endTime(endTime)
                                    .allDayEvent(allDayEvent)
                                    .organizer(organizerString)
                                    .appVariant(appVariant)
                                    .geoAreas(crawlingConfig.getGeoAreas())
                                    .newsSource(newsSource)
                                    .organization(crawlingConfig.getOrganization())
                                    .externalId(externalId)
                                    .postURL(postURLString)
                                    .authorName(authorString)
                                    .text(text)
                                    .categories(categoriesString)
                                    .imageURLs(getImageURLs(imageURLStrings, logSummary))
                                    .publishImmediately(publishImmediately)
                                    .commentsDisallowed(crawlingConfig.isCommentsDisallowed())
                                    .logMarker(LOG_MARKER_CONTENT_MAGNET)
                                    .build();

                        } else {
                            if (!(existingPost instanceof Happening)) {
                                logSummary.warn(
                                        "Existing item is not of type Happening at crawling config '{}' for news source '{}' for crawled item {} with external id '{}'",
                                        crawlingConfig.getId(),
                                        BaseEntity.getIdOf(newsSource),
                                        i,
                                        existingPost);
                                continue;
                            }
                            HappeningChangeByNewsSourceRequest changeRequest =
                                    HappeningChangeByNewsSourceRequest.builder()
                                            .post((Happening) existingPost)
                                            .startTime(startTime)
                                            .endTime(endTime)
                                            .allDayEvent(allDayEvent)
                                            .organizer(organizerString)
                                            .appVariant(appVariant)
                                            .geoAreas(crawlingConfig.getGeoAreas())
                                            .organization(crawlingConfig.getOrganization())
                                            .postURL(postURLString)
                                            .authorName(authorString)
                                            .text(text)
                                            .categories(categoriesString)
                                            .imageURLs(getImageURLs(imageURLStrings, logSummary))
                                            .publishImmediately(publishImmediately)
                                            .commentsDisallowed(crawlingConfig.isCommentsDisallowed())
                                            .logMarker(LOG_MARKER_CONTENT_MAGNET)
                                            .build();

                            if (happeningNeedsUpdate((Happening) existingPost, changeRequest)) {
                                request = changeRequest;
                            }
                        }
                        break;
                    default:
                        //should not be reached here
                        log.warn("Illegal post type {} at feed item {}", postType, i);
                }
                if (request != null) {
                    createOrUpdateRequests.add(request);
                    ScheduledFuture<BaseEvent> replyFuture = notifyTriggerAndWaitForAnyReplyDelayed(
                            request, delayBetweenPostRequests, TimeUnit.MILLISECONDS,
                            PostCreateConfirmation.class, PostChangeConfirmation.class);
                    replyFutures.add(replyFuture);
                    delayBetweenPostRequests += contentIntegrationConfig.getDelayBetweenPostCreation().toMillis();
                }
            }

            //we want to wait for all results, which shouldn't take longer than 90 seconds
            IParallelismService.ComputationOutcomes<BaseEvent> confirmationsOrExceptions = parallelismService
                    .getFutureComputationOutcome(replyFutures, Duration.ofSeconds(90));

            List<Throwable> exceptions = confirmationsOrExceptions.exceptions();
            if (!CollectionUtils.isEmpty(exceptions)) {
                logSummary.info("Failed to create or update {} posts", exceptions.size());
                List<IParallelismService.ComputationOutcome<BaseEvent>> computationOutcomes = confirmationsOrExceptions.computationOutcomes();
                for (int i = 0; i < computationOutcomes.size(); i++) {
                    // we want to have the index of the exception, so we know what request caused it
                    Throwable exception = computationOutcomes.get(i).exception();
                    if (exception instanceof EventProcessingException eventProcessingException) {
                        exception = eventProcessingException.getCause();
                    }
                    if (exception != null) {
                        BaseEvent causingRequest = createOrUpdateRequests.get(i);
                        if (causingRequest instanceof BaseExternalPostChangeByNewsSourceRequest<?> changeRequest) {
                            logSummary.info("Failed to update post {} at feed item {}: {}",
                                    changeRequest.getPost().getId(), i, exception.toString());
                        }
                        if (causingRequest instanceof BaseExternalPostCreateByNewsSourceRequest createRequest) {
                            logSummary.info("Failed to create post '{}' at feed item {}: {}",
                                    createRequest.getPostURL(), i, exception.toString());
                        }
                    }
                }
            }
            logSummary.outdent();
            logSummary.info(
                    "Finished crawling config '{}' for news source '{}' for {}: {} new, {} updated, {} unchanged, {} exceptions",
                    crawlingConfig.getId(), BaseEntity.getIdOf(newsSource), postType,
                    createOrUpdateRequests.stream()
                            .filter(r -> r instanceof BaseExternalPostCreateByNewsSourceRequest)
                            .count(),
                    createOrUpdateRequests.stream()
                            .filter(r -> r instanceof BaseExternalPostChangeByNewsSourceRequest)
                            .count(),
                    contentItemsFromWebFeed.size() - createOrUpdateRequests.size(),
                    exceptions.size());
        } catch (Exception ex) {
            logSummary.outdent();
            logSummary.error("Failed: {}", ex, ex.toString());
        }
    }

    private int getMaxTextLength(PostType postType, AppVariant appVariant) {

        return switch (postType) {
            case NewsItem -> {
                //get the news item feature for the app variant, if not configured, get it for DorfFunk
                final NewsItemPostTypeFeature newsItemFeature =
                        featureService.getFeatureOptional(NewsItemPostTypeFeature.class,
                                        FeatureTarget.of(appVariant))
                                .orElseGet(() -> featureService.getFeature(NewsItemPostTypeFeature.class,
                                        FeatureTarget.of(appService.findById(DorfFunkConstants.APP_ID))));
                yield newsItemFeature.getMaxNumberCharsPerPost();
            }
            case Happening -> {
                //get the happening feature for the app variant, if not configured, get it for DorfFunk
                final HappeningPostTypeFeature happeningFeature =
                        featureService.getFeatureOptional(HappeningPostTypeFeature.class,
                                        FeatureTarget.of(appVariant))
                                .orElseGet(() -> featureService.getFeature(HappeningPostTypeFeature.class,
                                        FeatureTarget.of(appService.findById(DorfFunkConstants.APP_ID))));
                yield happeningFeature.getMaxNumberCharsPerPost();
            }
            default -> throw new RuntimeException("Illegal post type");
        };
    }

    private boolean externalPostNeedsUpdate(ExternalPost externalPost,
            BaseExternalPostChangeByNewsSourceRequest<?> changeRequest) {
        if (externalPost == null) {
            return true;
        }
        //we need to normalize null, and blank Strings to empty strings
        String existingAuthorName = StringUtils.defaultIfBlank(externalPost.getAuthorName(), "");
        String newAuthorName = StringUtils.defaultIfBlank(changeRequest.getAuthorName(), "");
        if (!StringUtils.equals(existingAuthorName, newAuthorName)) {
            return true;
        }
        if (!StringUtils.equals(externalPost.getText(), changeRequest.getText())) {
            return true;
        }
        if (!StringUtils.equals(externalPost.getCategories(), changeRequest.getCategories())) {
            return true;
        }
        int existingImageSize = externalPost.getImages() == null ? 0 : externalPost.getImages().size();
        int newImageSize =
                changeRequest.getImageURLs() == null
                        ? 0
                        : changeRequest.getImageURLs().size();
        if (existingImageSize != newImageSize) {
            return true;
        }
        return false;
    }

    private boolean happeningNeedsUpdate(Happening happening, HappeningChangeByNewsSourceRequest changeRequest) {
        if (externalPostNeedsUpdate(happening, changeRequest)) {
            return true;
        }
        if (happening.getStartTime() != changeRequest.getStartTime()) {
            return true;
        }
        if (happening.getEndTime() != changeRequest.getEndTime()) {
            return true;
        }
        if (happening.isAllDayEvent() != changeRequest.isAllDayEvent()) {
            return true;
        }
        if (!StringUtils.equals(happening.getOrganizer(), changeRequest.getOrganizer())) {
            return true;
        }
        return false;
    }

    /**
     * @param imageURLStrings Image URLs as strings
     *
     * @return Valid URLs from image URL strings
     */
    private List<URL> getImageURLs(List<String> imageURLStrings, LogSummary logSummary) {

        if (CollectionUtils.isEmpty(imageURLStrings)) {
            return Collections.emptyList();
        }
        return imageURLStrings.stream()
                .map(StringUtils::strip)
                .map(rawURL -> {
                    try {
                        return fileDownloadService.createUrlAndCheck(rawURL);
                    } catch (FileDownloadException e) {
                        logSummary.info("Image URL '{}' invalid, skipping image: {}", e, rawURL, e.toString());
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private boolean expectedEntriesAreBlank(int index, WebFeedItemScrapingResult itemScrapingResult, LogSummary logSummary, String... expectedEntries) {

        List<String> blankEntries = Arrays.stream(expectedEntries)
                .filter(itemScrapingResult::hasNoScrapingResultValue)
                .collect(Collectors.toList());

        if (blankEntries.isEmpty()) {
            return false;
        } else {
            logSummary.info("Parsed feed item #{} contains blank entries at {}: '{}'", index, blankEntries,
                    itemScrapingResult);
            return true;
        }
    }

}

