/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.services;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public abstract class ScrapingResultValue<V> {

    protected V value;

    boolean isNotEmpty() {

        return !isEmpty();
    }

    public abstract boolean isEmpty();

    protected abstract ScrapingResultValue<?> doCombine(ScrapingResultValue<?> other);

    /**
     * Combines two result values into one, if possible.
     *
     * @param other the other value that is combined after this value
     * @return the combined value
     */
    public ScrapingResultValue<?> combine(ScrapingResultValue<?> other) {

        if (other == null) {
            return this;
        }
        if (other.isEmpty()) {
            return this;
        }
        if (this.isEmpty()) {
            return other;
        }
        ScrapingResultValue<?> result = doCombine(other);
        if (result != null) {
            return result;
        } else {
            throw new RuntimeException("Can not combine value " + other + " into a " + this + " value");
        }
    }

    @Override
    public String toString() {

        return this.getClass().getSimpleName() + "(" + value + ")";
    }

}
