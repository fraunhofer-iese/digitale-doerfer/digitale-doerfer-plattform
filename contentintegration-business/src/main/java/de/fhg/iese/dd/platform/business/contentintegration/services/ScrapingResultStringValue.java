/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.services;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ScrapingResultStringValue extends ScrapingResultValue<String> {

    public ScrapingResultStringValue(String value) {

        super(StringUtils.defaultIfBlank(value, null));
    }

    @Override
    public boolean isEmpty() {

        return value == null;
    }

    @Override
    public ScrapingResultValue<?> doCombine(ScrapingResultValue<?> other) {

        if (other instanceof ScrapingResultStringValue otherScrapingResultStringValue) {
            //other is a non-empty strings
            return ScrapingResultListStringValue.of(List.of(this.value, otherScrapingResultStringValue.value));
        }
        if (other instanceof ScrapingResultListStringValue otherScrapingResultListStringValue) {
            //other is a non-empty string list
            ArrayList<String> values = new ArrayList<>();
            values.add(this.value);
            values.addAll(otherScrapingResultListStringValue.value);
            //a non-empty string and a list of strings
            return ScrapingResultListStringValue.of(values);
        }
        return null;
    }

}
