/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Mher Ter-Tovmasyan, Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.services;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.CrawlingAccessType;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.FormattedRegex;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.WebFeedParameter;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.WebFeedValueSource;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Profile({"!test"})
@Service
class WebFeedScraperService extends BaseService implements IWebFeedScraperService {

    @Override
    public List<WebFeedItemScrapingResult> scrapeWebFeed(String webFeedSourceUrl, CrawlingAccessType accessType,
                                                         Map<String, List<WebFeedParameter>> scrapingConfiguration, LogSummary logSummary) {

        logSummary.setLogger(log);
        Elements feedElements = getWebFeedElementsFromSource(webFeedSourceUrl, accessType, logSummary);
        logSummary.info("Found {} feed elements from '{}'", feedElements.size(), webFeedSourceUrl);

        return feedElements.stream()
                .map(element -> scrapeElement(element, accessType, scrapingConfiguration, logSummary))
                .collect(Collectors.toList());
    }

    /**
     * @param webFeedSourceUrl URL of the RSS or Atom feed
     * @param accessType       Type of the feed (RSS or Atom)
     *
     * @return Found elements of the feed
     */
    private Elements getWebFeedElementsFromSource(String webFeedSourceUrl, CrawlingAccessType accessType,
            LogSummary logSummary) {
        try {
            final Document feed = getDocument(webFeedSourceUrl);
            String tagName;
            if (accessType == CrawlingAccessType.RSS) {
                tagName = "item";
            } else {
                tagName = "entry";
            }
            return feed.getElementsByTag(tagName);
        } catch (Exception e) {
            logSummary.warn("Failed to get feed elements from '{}' : {} (full URL '{}')",
                    StringUtils.abbreviate(webFeedSourceUrl, 50), e.toString(), webFeedSourceUrl);
            return new Elements();
        }
    }

    protected Document getDocument(String webFeedSourceUrl) throws IOException {

        return Jsoup.connect(webFeedSourceUrl)
                .parser(Parser.xmlParser())
                .get();
    }

    /**
     * @param element       Element of the web feed (RSS item or Atom entry)
     * @param accessType    Type of the web feed (RSS or Atom)
     * @param scrapingConfiguration Map of the output and the web feed parameter which define the sources of the needed values
     *                      in the web feed
     *
     * @return result of scraping of this element
     */
    private WebFeedItemScrapingResult scrapeElement(Element element, CrawlingAccessType accessType,
                                                    Map<String, List<WebFeedParameter>> scrapingConfiguration, LogSummary logSummary) {

        WebFeedItemScrapingResult scrapingResult = new WebFeedItemScrapingResult();
        try {
            for (Map.Entry<String, List<WebFeedParameter>> entry : scrapingConfiguration.entrySet()) {
                String resultAttributeName = entry.getKey();
                List<WebFeedParameter> feedParameters = entry.getValue();
                for (WebFeedParameter feedParameter : feedParameters) {
                    if(StringUtils.isNotBlank(feedParameter.getFixedValue())){
                        scrapingResult.putScrapingResultValue(resultAttributeName,
                                new ScrapingResultStringValue(feedParameter.getFixedValue()));
                        //skip parameter parsing if a fixed value is set
                        continue;
                    }

                    String query;
                    if (accessType == CrawlingAccessType.RSS) {
                        query = "item > " + feedParameter.getTag();
                    } else {
                        query = "entry > " + feedParameter.getTag();
                    }
                    Elements elements = element.select(query);
                    for (WebFeedValueSource webFeedValueSource : feedParameter.getWebFeedValueSources()) {
                        for (Element el : elements) {
                            ScrapingResultValue<?> scrapingResultValue = scrapeValue(el, webFeedValueSource,
                                    logSummary);
                            scrapingResult.putScrapingResultValue(resultAttributeName, scrapingResultValue);
                        }
                    }
                }
            }

        } catch (Exception e) {
            logSummary.warn("Failed to get mapped values from element: {}", e.toString());
        }
        return scrapingResult;
    }

    /**
     * @param element     Element of an element of the feed (e.g. <description>...</description>)
     * @param valueSource Config which describes where the needed value is located
     * @param logSummary  log summary
     *
     * @return Value of an element
     */
    private ScrapingResultValue<?> scrapeValue(Element element, WebFeedValueSource valueSource, LogSummary logSummary) {
        try {
            // Additional Images: Since in most cases the description/content of a feed contains additional images.
            // In case of child == img and attr == src we search for image urls
            if (StringUtils.equals(valueSource.getChild(), "img") &&
                    StringUtils.equals(valueSource.getAttribute(), "src")) {
                return ScrapingResultListStringValue.of(scrapeImageURLsFromHtml(element.text(), logSummary));
            }
            String value;
            boolean typePresent = StringUtils.isNotBlank(valueSource.getType());
            boolean childPresent = StringUtils.isNotBlank(valueSource.getChild());
            boolean attributePresent = StringUtils.isNotBlank(valueSource.getAttribute());
            if (!typePresent && !childPresent && attributePresent) {
                // Only attribute is provided: needed value is the text of the attribute of the given element
                // (e.g. <category term="..." />)
                value = element.attr(valueSource.getAttribute());
            } else {
                if (typePresent && !childPresent && attributePresent) {
                    if (element.attr("type").contains(valueSource.getType())) {
                        // Only attribute and type are provided: needed value is the text of the attribute containing the given type of the given element
                        // (e.g. <enclosure url="....." length="0" type="image/jpeg"/>  / <link rel="enclosure" type="image/jpeg" length="0" href="..."/>)
                        value = element.attr(valueSource.getAttribute());
                    } else {
                        value = "";
                    }
                } else {
                    if (!typePresent && childPresent && !attributePresent) {
                        // Only child is provided: needed value is the text of the given child of the given element
                        // (e.g. <author> <name>...</name> <email>...</email> </author>)
                        value = element.getElementsByTag(valueSource.getChild()).text();
                    } else {
                        // no matching case (e.g. no type, child or attribute present)
                        // (e.g. <category>...</category>)
                        value = removeHtmlTags(element.text(), logSummary);
                    }
                }
            }

            //if regexes are defined to extract the value, try extracting it
            if (!CollectionUtils.isEmpty(valueSource.getRegexes())){
                value = extractValueByRegexes(value, valueSource.getRegexes(), logSummary);
            }

            //if a format is present the value needs to be formatted first
            if (StringUtils.isNotBlank(valueSource.getFormat())) {
                if (StringUtils.equals(valueSource.getType(), "date")) {
                    // Date: in case the given data type is a date, it needs to be converted to a timestamp
                    return new ScrapingResultLongValue(parseDate(value, valueSource.getFormat(), logSummary));
                } else {
                    return new ScrapingResultStringValue(String.format(valueSource.getFormat(), value));
                }
            } else {
                return new ScrapingResultStringValue(value);
            }
        } catch (Exception e) {
            logSummary.warn("Failed to get values from element: {}", e.toString());
            return null;
        }
    }

    /**
     * Extracts the value out of the input by the first match to a regex and passing the extracted groups to a MessageFormatter with the corresponding messageFormat.
     * Returns the input string if no match was found.
     *
     * @param input the input string
     * @param regexes the list of FormattedRegex, consisting of regex and messageFormat pairs, to match against.
     * @param logSummary the LogSummary to log into
     * @return the message-formatted extracted groups of the first match, the input string otherwise.
     */
    private String extractValueByRegexes(String input, List<FormattedRegex> regexes, LogSummary logSummary) {
        try {
            for (FormattedRegex formattedRegex : regexes) {
                Pattern pattern = Pattern.compile(formattedRegex.getRegex());
                Matcher matcher = pattern.matcher(input);
                if (matcher.find() && matcher.groupCount() > 0){
                    return MessageFormat.format(formattedRegex.getMessageFormat(), getMatchedGroups(matcher).toArray());
                }
            }
        } catch (Exception e){
            logSummary.warn("Exception while trying to extract values by regexes: {}", e.toString());
        }

        return input;
    }

    /**
     * Collects all groups of the current match of the matcher into a list
     * @param matcher Matcher with an active match
     * @return the list of groups the matcher currently matches
     */
    private List<String> getMatchedGroups(Matcher matcher){
        List<String> groups = new ArrayList<>();
        for (int i = 1; i <= matcher.groupCount(); i++){
            groups.add(matcher.group(i));
        }
        return groups;
    }

    /**
     * Parses the html input
     *
     * @param htmlString HTML string
     *
     * @return string which contains no html tags
     */
    private String removeHtmlTags(String htmlString, LogSummary logSummary) {

        if (StringUtils.isBlank(htmlString)) {
            return "";
        }
        try {
            final Document doc = Jsoup.parse(htmlString);
            return doc.text();
        } catch (Exception e) {
            logSummary.warn("Failed to parse html: {}", e.toString());
            return "";
        }
    }

    /**
     * Parses date (as a string) to UTC timestamp
     *
     * @param dateString date as a string
     * @param dateFormatPattern format of the date
     * @param logSummary log Summary
     *
     * @return timestamp from date string
     */
    private Long parseDate(String dateString, String dateFormatPattern, LogSummary logSummary) {

        if (StringUtils.isBlank(dateString)) {
            return null;
        } else {
            try {
                DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                        .parseCaseInsensitive()
                        .appendPattern(dateFormatPattern)
                        .toFormatter(Locale.ENGLISH);

                // try to create a ZonedDateTime, if it fails, try LocalDateTime
                TemporalAccessor parsed = formatter.parseBest(dateString, ZonedDateTime::from, LocalDateTime::from);

                if (parsed instanceof ZonedDateTime zonedDateTime) {
                    //the date string contained the time zone or offset
                    return zonedDateTime.toInstant().toEpochMilli();
                }
                if (parsed instanceof LocalDateTime localDateTime) {
                    //the date string was without zone information, so we assume it is Europe/Berlin
                    return localDateTime.atZone(ZoneId.of("Europe/Berlin")).toInstant().toEpochMilli();
                }
                throw new RuntimeException("Parsed date was of unknown type " + parsed);
            } catch (Exception e) {
                logSummary.info("Can't convert '{}' using pattern '{}': {}", dateString, dateFormatPattern,
                        e.toString());
                return null;
            }
        }
    }

    /**
     * Looks for <code>&lt;img&gt;</code> tags in the html and creates a list of the image sources
     *
     * @param htmlString HTML string
     *
     * @return list of image sources
     */
    private List<String> scrapeImageURLsFromHtml(String htmlString, LogSummary logSummary) {

        if (StringUtils.isBlank(htmlString)) {
            return Collections.emptyList();
        }
        try {
            final Document doc = Jsoup.parse(htmlString);
            return doc.getElementsByTag("img").stream()
                    .map(element -> element.absUrl("src"))
                    .filter(StringUtils::isNotBlank)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            logSummary.info("Failed to get image URIs from html: {}", e.toString());
            return Collections.emptyList();
        }
    }

}
