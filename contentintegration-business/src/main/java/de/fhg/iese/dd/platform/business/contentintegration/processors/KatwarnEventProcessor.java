/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.processors;

import de.fhg.iese.dd.platform.business.contentintegration.events.KatwarnAlertReceivedEvent;
import de.fhg.iese.dd.platform.business.contentintegration.services.IKatwarnService;
import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.PostChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.newsitem.NewsItemChangeByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.newsitem.NewsItemCreateByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.NewsSourceNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.services.INewsSourceService;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.feature.KatwarnFeature;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.KatwarnAlert;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static de.fhg.iese.dd.platform.business.contentintegration.services.IKatwarnService.KATWARN_SENDER_ID_DWD;
import static de.fhg.iese.dd.platform.business.contentintegration.services.IKatwarnService.LOG_MARKER_KATWARN;

@EventProcessor
public class KatwarnEventProcessor extends BaseEventProcessor {

    @Autowired
    private IFeatureService featureService;
    @Autowired
    private INewsSourceService newsSourceService;
    @Autowired
    private IPostService postService;
    @Autowired
    private IKatwarnService katwarnService;

    @EventProcessing
    private BaseEvent handleKatwarnAlertReceivedEvent(KatwarnAlertReceivedEvent event) {

        AppVariant katwarnAppVariant = event.getKatwarnAppVariant();
        if (!featureService.isFeatureEnabled(KatwarnFeature.class, FeatureTarget.of(katwarnAppVariant))) {
            log.debug(LOG_MARKER_KATWARN, "No Katwarn feature defined or disabled, skipping processing");
            return null;
        }

        KatwarnAlert katwarnAlert = event.getKatwarnAlert();
        if (katwarnAlert.getStatus() != KatwarnAlert.KatwarnStatus.ACTUAL) {
            log.debug(LOG_MARKER_KATWARN, "Katwarn status was {}, ignoring", katwarnAlert.getStatus());
            return null;
        }

        NewsSource newsSource;
        try {
            newsSource = newsSourceService.findNewsSourceByAppVariant(katwarnAppVariant);
        } catch (NewsSourceNotFoundException e) {
            log.error(LOG_MARKER_KATWARN, "No news source defined for Katwarn app variant {}, skipping processing",
                    katwarnAppVariant);
            return null;
        }
        List<Organization> organizations = newsSourceService.findAllAvailableOrganizationsByNewsSource(
                newsSource, EnumSet.of(OrganizationTag.CIVIL_PROTECTION));
        Organization organization;
        if (CollectionUtils.isEmpty(organizations)) {
            organization = null;
        } else {
            if (organizations.size() > 1) {
                log.error(LOG_MARKER_KATWARN,
                        "Multiple organizations defined for Katwarn news source {}, skipping processing", newsSource);
                return null;
            } else {
                organization = organizations.get(0);
            }
        }

        KatwarnAlert.KatwarnLanguageBlock deLanguageBlock = katwarnAlert.getLanguages().get("de");
        if (deLanguageBlock == null) {
            log.error(LOG_MARKER_KATWARN, "No de language block found, skipping alert {}", katwarnAlert.getId());
            return null;
        }
        IKatwarnService.IncidentCategory incidentCategory = katwarnService.calculateIncidentCategory(katwarnAlert);
        Map<String, Object> customAttributes = calculateAttributes(katwarnAlert, deLanguageBlock, incidentCategory);
        String eventCodeText;
        if (StringUtils.isNotBlank(deLanguageBlock.getEventType())) {
            eventCodeText = deLanguageBlock.getEventType();
        } else {
            String eventCode = katwarnAlert.getEventCode();
            eventCodeText = katwarnService.getEventCodeText(eventCode);
            if (StringUtils.isBlank(eventCodeText)) {
                log.error(LOG_MARKER_KATWARN, "Unknown Katwarn event code '{}' at alert {}", eventCode,
                        katwarnAlert.getId());
            }
        }
        customAttributes.put(IKatwarnService.KATWARN_ATTRIBUTE_EVENT_CODE_TEXT, eventCodeText);

        Set<GeoArea> affectedGeoAreas = katwarnService.getAffectedGeoAreas(katwarnAlert, katwarnAppVariant);
        Optional<ExternalPost> existingPostOpt = postService
                .findExternalPostByNewsSourceAndExternalIgnoringDeleted(newsSource, katwarnAlert.getIncident());
        NewsItem existingPost = (NewsItem) existingPostOpt
                .filter(post -> post instanceof NewsItem)
                .orElse(null);
        String alertUrl = StringUtils.defaultString(deLanguageBlock.getWeb());
        String categories = eventCodeText + ": " + incidentCategory.category();
        String alertText = calculateAlertText(katwarnAlert, deLanguageBlock);
        if (existingPost == null) {
            if (affectedGeoAreas.isEmpty()) {
                log.debug(LOG_MARKER_KATWARN, "No geo area matched Katwarn alert geometry, skipping processing");
                return null;
            }
            return NewsItemCreateByNewsSourceRequest.builder()
                    .geoAreas(affectedGeoAreas)
                    .newsSource(newsSource)
                    .appVariant(katwarnAppVariant)
                    .organization(organization)
                    .externalId(katwarnAlert.getIncident())
                    .postURL(alertUrl)
                    .text(alertText)
                    .customAttributes(customAttributes)
                    .categories(categories)
                    .desiredUnpublishTime(calculateDesiredUnpublishTime(katwarnAlert))
                    .pushMessage(calculatePushMessage(katwarnAlert, incidentCategory, deLanguageBlock))
                    .pushMessageLoud(katwarnAlert.isNotifiable())
                    .pushCategoryId(DorfFunkConstants.PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED)
                    .publishImmediately(true)
                    .build();
        } else {
            if (affectedGeoAreas.isEmpty()) {
                //it has been an update or cancellation without geometry, so we leave the existing geo areas
                affectedGeoAreas = existingPost.getGeoAreas();
            }
            return NewsItemChangeByNewsSourceRequest.builder()
                    .post(existingPost)
                    .appVariant(katwarnAppVariant)
                    .geoAreas(affectedGeoAreas)
                    .organization(organization)
                    .postURL(alertUrl)
                    .text(alertText)
                    .customAttributes(customAttributes)
                    .categories(categories)
                    .desiredUnpublishTime(calculateDesiredUnpublishTime(katwarnAlert))
                    .pushMessage(calculatePushMessage(katwarnAlert, incidentCategory, deLanguageBlock))
                    .pushMessageLoud(katwarnAlert.isNotifiable())
                    .pushCategoryId(DorfFunkConstants.PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED)
                    .publishImmediately(true)
                    .updateLastActivity(true)
                    .build();
        }
    }

    @EventProcessing
    private void handlePostCreated(PostCreateConfirmation<?> postCreateConfirmation) {

        Post post = postCreateConfirmation.getPost();
        if (post instanceof NewsItem newsItem) {
            Map<String, Object> customAttributes = newsItem.getCustomAttributes();
            if (!CollectionUtils.isEmpty(customAttributes) && customAttributes.containsKey(
                    IKatwarnService.KATWARN_ATTRIBUTE_INCIDENT_ID)) {
                Object incidentId = customAttributes.get(IKatwarnService.KATWARN_ATTRIBUTE_INCIDENT_ID);
                log.debug(LOG_MARKER_KATWARN, "New Katwarn post '{}' created for incident {}", post.getId(),
                        incidentId);
            }
        }
    }

    @EventProcessing
    private void handlePostChanged(PostChangeConfirmation<?> postChangeConfirmation) {

        Post post = postChangeConfirmation.getPost();
        if (post instanceof NewsItem newsItem) {
            Map<String, Object> customAttributes = newsItem.getCustomAttributes();
            if (!CollectionUtils.isEmpty(customAttributes) && customAttributes.containsKey(
                    IKatwarnService.KATWARN_ATTRIBUTE_INCIDENT_ID)) {
                Object incidentId = customAttributes.get(IKatwarnService.KATWARN_ATTRIBUTE_INCIDENT_ID);
                log.debug(LOG_MARKER_KATWARN, "Katwarn post '{}' updated for incident {}", post.getId(), incidentId);
            }
        }
    }

    private Long calculateDesiredUnpublishTime(KatwarnAlert katwarnAlert) {

        if (katwarnAlert.getExpires() != null) {
            return katwarnAlert.getExpires();
        } else {
            if (katwarnAlert.getContentType() == KatwarnAlert.KatwarnContentType.CLEAR) {
                //the warning is cleared, so it is removed in some minutes
                return timeService.currentTimeMillisUTC() + TimeUnit.MINUTES.toMillis(30);
            } else {
                // no expiration time set
                return null;
            }
        }
    }

    @NonNull
    private static String calculatePushMessage(KatwarnAlert katwarnAlert, IKatwarnService.IncidentCategory incidentCategory, KatwarnAlert.KatwarnLanguageBlock deLanguageBlock) {

        StringBuilder pushMessage = new StringBuilder();
        if (katwarnAlert.getMsgType() == KatwarnAlert.KatwarnMessageType.UPDATE) {
            pushMessage.append("Aktualisierung: ");
        }
        String headline = StringUtils.abbreviate(deLanguageBlock.getHeadline(), "...", 150);
        pushMessage.append(StringUtils.upperCase(incidentCategory.category()))
                .append(" | ")
                .append(headline);
        return pushMessage.toString();
    }

    @NonNull
    private Map<String, Object> calculateAttributes(KatwarnAlert katwarnAlert, KatwarnAlert.KatwarnLanguageBlock deLanguageBlock, IKatwarnService.IncidentCategory incidentCategory) {

        Map<String, Object> attributes = new HashMap<>();
        attributes.put(IKatwarnService.KATWARN_ATTRIBUTE_INCIDENT_ID, katwarnAlert.getIncident());
        attributes.put(IKatwarnService.KATWARN_ATTRIBUTE_SEVERITY, katwarnAlert.getSeverity().name());
        attributes.put(IKatwarnService.KATWARN_ATTRIBUTE_CONTENT_TYPE, katwarnAlert.getContentType().name());
        attributes.put(IKatwarnService.KATWARN_ATTRIBUTE_EVENT_CODE, katwarnAlert.getEventCode());
        attributes.put(IKatwarnService.KATWARN_ATTRIBUTE_EFFECTIVE, katwarnAlert.getEffective());
        attributes.put(IKatwarnService.KATWARN_ATTRIBUTE_EXPIRES, katwarnAlert.getExpires());
        attributes.put(IKatwarnService.KATWARN_ATTRIBUTE_INCIDENT_CATEGORY, incidentCategory.category());
        attributes.put(IKatwarnService.KATWARN_ATTRIBUTE_INCIDENT_CATEGORY_COLOR, incidentCategory.color());
        attributes.put(IKatwarnService.KATWARN_ATTRIBUTE_ISSUER, deLanguageBlock.getIssuer());
        return attributes;
    }

    @NonNull
    private String calculateAlertText(KatwarnAlert katwarnAlert, KatwarnAlert.KatwarnLanguageBlock deLanguageBlock) {

        StringBuilder alertText = new StringBuilder()
                .append(deLanguageBlock.getHeadline())
                .append("\n");
        if (StringUtils.isNotBlank(deLanguageBlock.getDescription())) {
            alertText.append("\nBESCHREIBUNG:\n")
                    .append(deLanguageBlock.getDescription())
                    .append("\n");
        }
        List<String> instructions = deLanguageBlock.getInstruction();
        if (!CollectionUtils.isEmpty(instructions)) {
            String instructionList;
            boolean multipleInstructions;
            if (KATWARN_SENDER_ID_DWD.equals(katwarnAlert.getSenderId())) {
                multipleInstructions = true;
                //the dwd warnings have "Handlungsempfehlungen" inside the instructions
                StringJoiner joiner = new StringJoiner("\n- ", "- ", "\n");
                for (String instruction : instructions) {
                    if (StringUtils.contains(instruction, "\nHandlungsempfehlungen: \n")) {
                        //this should be the default case, only one instruction with the "Handlungsempfehlungen" inside
                        String[] split = StringUtils.splitByWholeSeparator(instruction, "\nHandlungsempfehlungen: \n");
                        if (split.length >= 1) {
                            String trailingText = split[0];
                            joiner.add(StringUtils.strip(trailingText));
                        }
                        if (split.length >= 2) {
                            //the instructions are separated by ";" and randomly capitalized
                            String coreInstructions = split[1];
                            Arrays.stream(StringUtils.splitByWholeSeparator(coreInstructions, "; "))
                                    .map(StringUtils::capitalize)
                                    .map(StringUtils::strip)
                                    .forEach(joiner::add);
                        }
                    } else {
                        //this should not happen, we just treat it as above: separated by ";" and randomly capitalized
                        Arrays.stream(StringUtils.splitByWholeSeparator(instruction, "; "))
                                .map(StringUtils::capitalize)
                                .map(StringUtils::strip)
                                .forEach(joiner::add);
                    }
                }
                instructionList = joiner.toString();
            } else {
                multipleInstructions = instructions.size() > 1;
                instructionList = instructions.stream()
                        .collect(Collectors.joining("\n- ", "- ", "\n"));
            }
            if (multipleInstructions) {
                alertText.append("\nHANDLUNGSEMPFEHLUNGEN:\n");
            } else {
                alertText.append("\nHANDLUNGSEMPFEHLUNG:\n");
            }
            alertText.append(instructionList);
        }
        KatwarnAlert.KatwarnLanguageBlock.KatwarnLocality locality = deLanguageBlock.getLocality();
        if (locality != null && !CollectionUtils.isEmpty(locality.getNames())) {
            alertText.append("\nBETROFFENE REGIONEN:\n");
            if (StringUtils.isNotBlank(locality.getPrefix())) {
                alertText.append(locality.getPrefix()).append(" ");
            }
            alertText.append(String.join(", ", locality.getNames()));
            if (StringUtils.isNotBlank(locality.getSuffix())) {
                alertText.append(" ").append(locality.getSuffix());
            }
            alertText.append("\n");
        }
        String contact = deLanguageBlock.getContact();
        if (StringUtils.isNotBlank(contact)) {
            alertText.append("\nKONTAKT:\n")
                    .append(contact)
                    .append("\n");
        }
        return alertText.toString();
    }

}
