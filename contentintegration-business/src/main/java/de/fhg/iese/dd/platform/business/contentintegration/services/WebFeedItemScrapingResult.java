/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Mher Ter-Tovmasyan, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.services;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;

import java.util.*;
import java.util.stream.Collectors;

@NoArgsConstructor
@ToString
public class WebFeedItemScrapingResult {

    private final Map<String, ScrapingResultValue<?>> attributeNameToScrapingResultValue = new HashMap<>();

    /**
     * Stores the value as result for this attribute. If a value already exists it gets combined with the existing one.
     * </p>
     * Empty values are not stored.
     *
     * @param attributeName       name of the result attribute
     * @param scrapingResultValue value that should be put as result
     */
    public void putScrapingResultValue(String attributeName, ScrapingResultValue<?> scrapingResultValue) {

        if (scrapingResultValue != null && scrapingResultValue.isNotEmpty()) {
            ScrapingResultValue<?> existingValue = attributeNameToScrapingResultValue.get(attributeName);
            if (existingValue != null) {
                //there is already a value available, we combine it with the new one
                ScrapingResultValue<?> combinedValue = existingValue.combine(scrapingResultValue);
                attributeNameToScrapingResultValue.put(attributeName, combinedValue);
            } else {
                attributeNameToScrapingResultValue.put(attributeName, scrapingResultValue);
            }
        }
    }

    /**
     * Gets the stored value or null if none exists or is empty.
     *
     * @param attributeName name of the result attribute
     * @return the stored value or null
     */
    public ScrapingResultValue<?> getScrapingResultValue(String attributeName) {

        ScrapingResultValue<?> resultValue = attributeNameToScrapingResultValue.get(attributeName);
        if (resultValue == null || resultValue.isEmpty()) {
            return null;
        } else {
            return resultValue;
        }
    }

    /**
     * @param attributeName name of the result attribute
     * @return true if there is a non-empty value for this attribute
     */
    public boolean hasScrapingResultValue(String attributeName) {

        ScrapingResultValue<?> resultValue = getScrapingResultValue(attributeName);
        return resultValue != null;
    }

    /**
     * Inversion of {@link #hasScrapingResultValue(String)}
     */
    public boolean hasNoScrapingResultValue(String attributeName) {

        return !hasScrapingResultValue(attributeName);
    }

    /**
     * Gets the result value for this attribute as Long or the default value if no value was set, or is of wrong type.
     *
     * @param attributeName name of the result attribute
     * @param defaultValue  default value if no result value was defined, can be null
     * @return the stored result value or the default value
     */
    public Long getLongScrapingResultValue(String attributeName, Long defaultValue) {

        ScrapingResultValue<?> resultValue = getScrapingResultValue(attributeName);
        if (resultValue instanceof ScrapingResultLongValue scrapingResultLongValue) {
            Long value = scrapingResultLongValue.getValue();
            if (value == null) {
                return defaultValue;
            } else {
                return value;
            }
        }
        return defaultValue;
    }

    /**
     * Gets the result value for this attribute as list of strings or null if no value was set, or is of wrong type.
     * If the result is only a string it is put in a list.
     *
     * @param attributeName name of the result attribute
     * @return list of result values or null
     */
    public List<String> getListStringScrapingResultValue(String attributeName) {

        ScrapingResultValue<?> resultValue = getScrapingResultValue(attributeName);
        if (resultValue instanceof ScrapingResultStringValue scrapingResultStringValue) {
            return List.of(scrapingResultStringValue.getValue());
        }
        if (resultValue instanceof ScrapingResultListStringValue scrapingResultListStringValue) {
            return scrapingResultListStringValue.getValue();
        }
        return null;
    }

    /**
     * Gets the result value for this attribute as string or null if no value was set, or is of wrong type.
     * If the result is a list of strings null is returned.
     *
     * @param attributeName name of the result attribute
     * @return result value or null
     */
    public String getStringScrapingResultValue(String attributeName) {

        ScrapingResultValue<?> resultValue = getScrapingResultValue(attributeName);
        if (resultValue instanceof ScrapingResultStringValue scrapingResultStringValue) {
            return scrapingResultStringValue.getValue();
        }
        return null;
    }

    /**
     * Gets the result value for this attribute as string or null if no value was set, or is of wrong type.
     * If the result is a list of strings they are joined using the separator.
     *
     * @param attributeName name of the result attribute
     * @param separator     separator used to join the strings
     * @param removeDuplicates flag if duplicate entries should be removed
     * @return result value or null
     */
    public String getStringScrapingResultValue(String attributeName, String separator, Boolean removeDuplicates) {

        ScrapingResultValue<?> resultValue = getScrapingResultValue(attributeName);
        if (resultValue instanceof ScrapingResultStringValue scrapingResultStringValue) {
            return scrapingResultStringValue.getValue();
        }
        if (resultValue instanceof ScrapingResultListStringValue scrapingResultListStringValue) {
            List<String> rawValues = scrapingResultListStringValue.getValue();
            if (removeDuplicates) {
                Set<String> values = removeDuplicates(rawValues);
                return String.join(separator, values);
            }
            return String.join(separator, rawValues);
        }
        return null;
    }

    private static @NonNull Set<String> removeDuplicates(List<String> rawValues) {

        Set<String> valuesToRemove = new HashSet<>();
        for (String entry : rawValues) {
            for (String otherEntry : rawValues) {
                if (entry.equals(otherEntry)) {
                    continue;
                }
                if (StringUtils.startsWith(entry, otherEntry)) {
                    valuesToRemove.add(entry.length() < otherEntry.length()
                            ? entry
                            : otherEntry);
                }
            }
        }
        Set<String> values = new LinkedHashSet<>(rawValues);
        values.removeAll(valuesToRemove);
        return values;
    }

    /**
     * Constructs a map of attribute names and string representations of the values. Only suitable for debugging.
     *
     * @return the map of attribute names and their values
     */
    public Map<String, String> toDebugMap() {

        return attributeNameToScrapingResultValue.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().toString()));
    }

}
