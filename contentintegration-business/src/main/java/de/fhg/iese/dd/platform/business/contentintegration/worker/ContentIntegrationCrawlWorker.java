/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Mher Ter-Tovmasyan, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.worker;

import de.fhg.iese.dd.platform.business.contentintegration.services.ICrawlingService;
import de.fhg.iese.dd.platform.business.framework.environment.model.IWorkerTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class ContentIntegrationCrawlWorker implements IWorkerTask {

    @Autowired
    private ITimeService timeService;
    @Autowired
    private ICrawlingService crawlingService;

    @Override
    public Trigger getTaskTrigger() {

        return new CronTrigger("13 13 7-21 * * *", timeService.getLocalTimeZone());//At xx:13:13 from 7-21
    }

    @Override
    public void run() {

        long start = System.currentTimeMillis();
        log.info("crawlers for content integration triggered");
        LogSummary logSummary = new LogSummary();
        crawlingService.createPostsFromExternalContent(logSummary, true);
        log.info("crawling content finished in {} ms", System.currentTimeMillis() - start);
    }

}
