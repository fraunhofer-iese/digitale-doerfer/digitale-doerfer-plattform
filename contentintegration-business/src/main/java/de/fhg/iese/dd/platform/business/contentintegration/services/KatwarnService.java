/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.services;

import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerAtOnceCache;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.geo.services.ISpatialService;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.KatwarnAlert;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.KatwarnEventDetails;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.repos.KatwarnEventDetailsRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.locationtech.jts.geom.Geometry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
class KatwarnService extends BaseService implements IKatwarnService {

    @Autowired
    private KatwarnEventDetailsRepository katwarnEventDetailsRepository;
    @Autowired
    private ISpatialService spatialService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private IAppService appService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private CacheCheckerAtOnceCache cacheChecker;

    private Map<String, KatwarnEventDetails> eventDetailsByEventCode;

    @PostConstruct
    private void initialize() {
        //this cache is replaced with a new map after it got cleared
        eventDetailsByEventCode = Collections.emptyMap();
    }

    @Override
    public CacheConfiguration getCacheConfiguration() {

        return CacheConfiguration.builder()
                .cachedEntity(KatwarnEventDetails.class)
                .build();
    }

    @Override
    @NonNull
    public IncidentCategory calculateIncidentCategory(@NotNull KatwarnAlert katwarnAlert) {

        KatwarnAlert.KatwarnContentType contentType = katwarnAlert.getContentType();
        KatwarnAlert.KatwarnSeverity severity = katwarnAlert.getSeverity();
        switch (contentType) {
            case CLEAR -> {
                return new IncidentCategory("Entwarnung!", "#4AABA4");
            }
            case INFO -> {
                return new IncidentCategory("Information", "#0098D4");
            }
            case WARNING -> {
                switch (severity) {
                    case MINOR -> {
                        return new IncidentCategory("Information", "#0098D4");
                    }
                    case MODERATE -> {
                        return new IncidentCategory("Warnung!", "#E9862F");
                    }
                    case SEVERE -> {
                        return new IncidentCategory("Erhöhte Gefahr!", "#D94247");
                    }
                    case EXTREME -> {
                        return new IncidentCategory("Extreme Gefahr!!", "#7F499A");
                    }
                    default -> log.error(LOG_MARKER_KATWARN, "Unknown Katwarn severity '{}' at alert {}", severity,
                            katwarnAlert.getId());
                }
            }
            default -> log.error(LOG_MARKER_KATWARN, "Unknown Katwarn content type '{}' at alert {}", contentType,
                    katwarnAlert.getId());
        }
        //should never happen, but we do not want to lose the alert
        return new IncidentCategory("Information", "blue");
    }

    @Override
    @Nullable
    public String getEventCodeText(String eventCode) {

        try {
            //the text of the according event is looked up in the resource bundle
            return messageSource.getMessage("katwarn.eventCode." + eventCode, null, Locale.GERMAN);
        } catch (NoSuchMessageException e) {
            return null;
        }
    }

    @Override
    @NonNull
    public Set<GeoArea> getAffectedGeoAreas(KatwarnAlert katwarnAlert, AppVariant katwarnAppVariant) {

        String geoJson = katwarnAlert.getGeometry();
        if (StringUtils.isEmpty(geoJson)) {
            return Collections.emptySet();
        }
        Geometry alertGeometry = spatialService.createGeometryFromGeoJson(geoJson);
        //the katwarn app variant is configured to have those geo ares available that should be warned in DorfFunk
        Set<GeoArea> availableGeoAreas = appService.getAvailableGeoAreasStrict(katwarnAppVariant);
        return geoAreaService.findIntersectedLeafGeoAreas(alertGeometry, availableGeoAreas);
    }

    @Override
    @Nullable
    public KatwarnEventDetails findEventDetails(String eventCode) {

        checkAndRefreshCache();
        KatwarnEventDetails eventDetails = eventDetailsByEventCode.get(eventCode);
        if (eventDetails == null) {
            log.warn(LOG_MARKER_KATWARN, "Could not find Katwarn event details for '{}'", eventCode);
        }
        return eventDetails;
    }

    @Override
    public void invalidateCache() {

        cacheChecker.resetCacheChecks();
    }

    private void checkAndRefreshCache() {
        // if the cache is stale or no fetch ever happened
        // get all katwarn event details fresh from the database
        cacheChecker.refreshCacheIfStale(() -> {
            long start = System.currentTimeMillis();

            eventDetailsByEventCode = query(() -> katwarnEventDetailsRepository.findAll().stream()
                    .map(proxy -> Hibernate.unproxy(proxy, KatwarnEventDetails.class))
                    .collect(Collectors.toMap(KatwarnEventDetails::getEventCode, Function.identity())));
            log.info("Refreshed Katwarn event details cache in {} ms", System.currentTimeMillis() - start);
        });
    }

}
