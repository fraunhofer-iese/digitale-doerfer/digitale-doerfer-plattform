/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2022 Steffen Hupp, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.contentintegration.exceptions.CrawlingConfigNotFoundException;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.CrawlingConfig;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.repos.CrawlingConfigRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;

@Service
class CrawlingConfigService extends BaseEntityService<CrawlingConfig> implements ICrawlingConfigService {

    @Autowired
    private CrawlingConfigRepository crawlingConfigRepository;

    @Override
    public CrawlingConfig findCrawlingConfigById(String configId) throws CrawlingConfigNotFoundException {
        return crawlingConfigRepository.findById(configId)
                .orElseThrow(() -> CrawlingConfigNotFoundException.forId(configId));
    }

    @Override
    public Page<CrawlingConfig> findAllCrawlingConfigs(Pageable pageable) {
        return crawlingConfigRepository.findAll(pageable);
    }

    @Override
    public List<CrawlingConfig> findAllCrawlingConfigsByNewsSource(NewsSource newsSource) {
        return crawlingConfigRepository.findAllByNewsSourceOrderById(newsSource);
    }

}
