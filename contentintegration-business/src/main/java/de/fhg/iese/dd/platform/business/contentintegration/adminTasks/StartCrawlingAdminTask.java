/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Mher Ter-Tovmasyan, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.adminTasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.contentintegration.services.ICrawlingService;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;

@Component
public class StartCrawlingAdminTask extends BaseAdminTask {

    @Autowired
    private ICrawlingService crawlingService;

    @Override
    public String getName() {
        return "StartCrawling";
    }

    @Override
    public String getDescription() {
        return "Starts crawling from external sources, creates items and publishes them";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) throws Exception {
        final long start = System.currentTimeMillis();
        logSummary.info("start: crawl and create news items from external content");
        crawlingService.createPostsFromExternalContent(logSummary, true);
        logSummary.info("end: crawling and creating content finished in {} ms",
                System.currentTimeMillis() - start);
    }

}
