/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.init;

import de.fhg.iese.dd.platform.business.contentintegration.services.IKatwarnService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.KatwarnConstants;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.KatwarnEventDetails;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.repos.KatwarnEventDetailsRepository;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class KatwarnDataInitializer extends BaseDataInitializer {

    @Autowired
    private KatwarnEventDetailsRepository katwarnEventDetailsRepository;
    @Autowired
    private AppRepository appRepository;
    @Autowired
    private IKatwarnService katwarnService;

    @Override
    public DataInitializerConfiguration getConfiguration() {

        return DataInitializerConfiguration.builder()
                .topic("katwarn")
                .requiredEntity(App.class)
                .processedEntity(KatwarnEventDetails.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {

        App katwarnApp = appRepository.findById(KatwarnConstants.APP_ID)
                .orElseThrow(() -> new DataInitializationException("Could not find katwarn app with id '{}'",
                        KatwarnConstants.APP_ID));

        List<KatwarnEventDetails> eventDetails = loadAllEntities(KatwarnEventDetails.class);
        logSummary.info("Found {} katwarn event details", eventDetails.size());
        DuplicateIdChecker<KatwarnEventDetails> duplicateIdChecker =
                new DuplicateIdChecker<>(KatwarnEventDetails::getEventCode);
        for (KatwarnEventDetails eventDetail : eventDetails) {
            //the entities in the data init do not provide ids, we generate them based on the unique event code
            eventDetail.withConstantId();
            duplicateIdChecker.checkId(eventDetail);
            adjustCreated(eventDetail);
            String eventCode = eventDetail.getEventCode();
            //this checks if we know the event code in the properties
            String eventCodeText = katwarnService.getEventCodeText(eventCode);
            if (StringUtils.isBlank(eventCodeText)) {
                logSummary.warn("Unknown katwarn event code {}", eventCode);
            }
            createImage(KatwarnEventDetails::getIcon, KatwarnEventDetails::setIcon, eventDetail,
                    FileOwnership.of(katwarnApp), logSummary, true);
        }
        Set<String> idsToKeep = eventDetails.stream()
                .map(KatwarnEventDetails::getId)
                .collect(Collectors.toSet());
        katwarnEventDetailsRepository.saveAll(eventDetails);
        long deletedOldEventDetails = katwarnEventDetailsRepository.deleteAllByIdNotIn(idsToKeep);
        if (deletedOldEventDetails > 0) {
            logSummary.info("Deleted {} old event details", deletedOldEventDetails);
        }
        logSummary.info("Saved {} katwarn event details", eventDetails.size());
        katwarnEventDetailsRepository.flush();
    }

}
