/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.services;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ScrapingResultListStringValue extends ScrapingResultValue<List<String>> {

    public ScrapingResultListStringValue(List<String> value) {

        super(Collections.unmodifiableList(normalizeStringList(value)));
    }

    @Override
    public boolean isEmpty() {

        return CollectionUtils.isEmpty(value);
    }

    @Override
    protected ScrapingResultValue<?> doCombine(ScrapingResultValue<?> other) {

        if (other instanceof ScrapingResultStringValue) {
            ArrayList<String> values = new ArrayList<>(this.value);
            values.add(((ScrapingResultStringValue) other).value);
            return ScrapingResultListStringValue.of(values);
        }
        if (other instanceof ScrapingResultListStringValue) {
            ArrayList<String> values = new ArrayList<>(this.value);
            values.addAll(((ScrapingResultListStringValue) other).value);
            return ScrapingResultListStringValue.of(values);
        }
        return null;
    }

    /**
     * @param values values
     * @return single string result value or list string result value, depending on the size of the list
     */
    public static ScrapingResultValue<?> of(List<String> values) {

        List<String> normalizedValues = normalizeStringList(values);
        if (CollectionUtils.isEmpty(normalizedValues)) {
            return new ScrapingResultStringValue(null);
        }
        if (normalizedValues.size() == 1) {
            return new ScrapingResultStringValue(normalizedValues.get(0));
        }
        return new ScrapingResultListStringValue(normalizedValues);
    }

    private static List<String> normalizeStringList(List<String> rawStrings) {

        if (CollectionUtils.isEmpty(rawStrings)) {
            return Collections.emptyList();
        } else {
            return rawStrings.stream()
                    .map(s -> StringUtils.defaultIfBlank(s, null))
                    .collect(Collectors.toList());
        }
    }

}
