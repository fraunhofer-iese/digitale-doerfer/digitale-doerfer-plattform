/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotFoundException;

public class CrawlingConfigNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 3235216609637034087L;

    private CrawlingConfigNotFoundException(String message) {
        super(message);
    }

    private CrawlingConfigNotFoundException(String message, Object... arguments) {
        super(message, arguments);
    }

    public static CrawlingConfigNotFoundException forId(String id) {
        return new CrawlingConfigNotFoundException("Could not find crawlingConfig with id '" + id + "'.");
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.CRAWLING_CONFIG_NOT_FOUND;
    }

}
