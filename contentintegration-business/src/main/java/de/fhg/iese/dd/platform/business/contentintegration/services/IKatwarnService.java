/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.services;

import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.KatwarnAlert;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.KatwarnEventDetails;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Set;

public interface IKatwarnService extends ICachingComponent {

    record IncidentCategory(String category, String color) {

    }

    Marker LOG_MARKER_KATWARN = MarkerManager.getMarker("KATWARN");

    String KATWARN_ATTRIBUTE_INCIDENT_ID = "katwarn.incidentId";
    String KATWARN_ATTRIBUTE_SEVERITY = "katwarn.severity";
    String KATWARN_ATTRIBUTE_CONTENT_TYPE = "katwarn.contentType";
    String KATWARN_ATTRIBUTE_EVENT_CODE = "katwarn.eventCode";
    String KATWARN_ATTRIBUTE_EFFECTIVE = "katwarn.effective";
    String KATWARN_ATTRIBUTE_EXPIRES = "katwarn.expires";
    String KATWARN_ATTRIBUTE_INCIDENT_CATEGORY = "katwarn.incidentCategory";
    String KATWARN_ATTRIBUTE_INCIDENT_CATEGORY_COLOR = "katwarn.incidentCategoryColor";
    String KATWARN_ATTRIBUTE_ISSUER = "katwarn.issuer";
    String KATWARN_ATTRIBUTE_EVENT_CODE_TEXT = "katwarn.eventCodeText";
    String KATWARN_SENDER_ID_DWD = "dwd";

    @Nullable
    String getEventCodeText(String eventCode);

    @NonNull
    Set<GeoArea> getAffectedGeoAreas(KatwarnAlert katwarnAlert, AppVariant katwarnAppVariant);

    @NonNull
    IncidentCategory calculateIncidentCategory(KatwarnAlert katwarnAlert);

    @Nullable
    KatwarnEventDetails findEventDetails(String eventCode);

}
