/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.init;

import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.CrawlingConfig;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.repos.CrawlingConfigRepository;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.NewsSourceRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.OrganizationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.repos.GeoAreaRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class CrawlingConfigDataInitializer extends BaseDataInitializer {

    @Autowired
    private CrawlingConfigRepository crawlingConfigRepository;
    @Autowired
    protected NewsSourceRepository newsSourceRepository;
    @Autowired
    protected OrganizationRepository organizationRepository;
    @Autowired
    protected GeoAreaRepository geoAreaRepository;

    @Override
    public DataInitializerConfiguration getConfiguration() {

        return DataInitializerConfiguration.builder()
                .topic("crawling-config")
                .requiredEntity(App.class)
                .requiredEntity(NewsSource.class)
                .requiredEntity(Organization.class)
                .processedEntity(CrawlingConfig.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {

        List<String> apiManagedCrawlingConfigIds = crawlingConfigRepository.getIdsByApiManagedTrue();
        logSummary.info("Found {} API-managed CrawlingConfigs", apiManagedCrawlingConfigIds.size());

        List<CrawlingConfig> createdCrawlingConfigs = new ArrayList<>();

        DuplicateIdChecker<CrawlingConfig> crawlingConfigDuplicateIdChecker = new DuplicateIdChecker<>();

        List<CrawlingConfig> crawlingConfigs = loadAllEntities(CrawlingConfig.class);

        EnumSet<PostType> allowedPostTypes = EnumSet.of(PostType.NewsItem, PostType.Happening);
        for (CrawlingConfig crawlingConfig : crawlingConfigs) {
            checkId(crawlingConfig);
            crawlingConfigDuplicateIdChecker.checkId(crawlingConfig);
            if (apiManagedCrawlingConfigIds.contains(crawlingConfig.getId())) {
                logSummary.warn("CrawlingConfig with id '{}' is managed by the API and cannot be changed " +
                        "by data init, skipping it. Adjust data init to not include it.", crawlingConfig.getId());
                continue;
            }
            adjustCreated(crawlingConfig);
            //get the geo areas to ensure that they exist
            Set<String> geoAreaIds = crawlingConfig.getGeoAreas().stream()
                    .map(GeoArea::getId)
                    .collect(Collectors.toSet());
            Set<GeoArea> geoAreas = geoAreaRepository.findAllByIdIn(geoAreaIds);
            crawlingConfig.setGeoAreas(geoAreas);
            PostType postType = crawlingConfig.getExternalPostType();
            if (postType == null || !allowedPostTypes.contains(postType)) {
                throw new DataInitializationException("Invalid post type {}", postType);
            }
            createdCrawlingConfigs.add(crawlingConfig);
            NewsSource newsSource = checkAndAddNewsSource(crawlingConfig);
            Organization organization = null;
            String organizationId = BaseEntity.getIdOf(crawlingConfig.getOrganization());
            if (StringUtils.isNotEmpty(organizationId)) {
                Optional<Organization> organizationOptional = organizationRepository.findByIdAndDeletedFalse(
                        organizationId);
                organization = organizationOptional.orElseThrow(() -> new DataInitializationException(
                        "Could not find organization '{}' referenced in crawling config '{}'", organizationId,
                        crawlingConfig.getId()));
                crawlingConfig.setOrganization(organization);
            }

            logSummary.info("created CrawlingConfig '{}' for NewsSource '{}' and Organization '{}'",
                    crawlingConfig.getId(),
                    newsSource.getSiteName(), organization == null
                            ? "-"
                            : organization.getName());
        }

        logSummary.info("stored {} CrawlingConfigs", createdCrawlingConfigs.size());

        crawlingConfigRepository.saveAll(createdCrawlingConfigs);
        crawlingConfigRepository.flush();
    }

    private NewsSource checkAndAddNewsSource(CrawlingConfig crawlingConfig) {

        String newsSourceId = BaseEntity.getIdOf(crawlingConfig.getNewsSource());
        if (StringUtils.isEmpty(newsSourceId)) {
            throw new DataInitializationException("No NewsSource referenced by crawling config '{}'",
                    crawlingConfig.getId());
        }
        NewsSource newsSource = newsSourceRepository.findById(newsSourceId)
                .orElseThrow(() -> new DataInitializationException(
                    "NewsSource '{}' referenced at crawling config '{}' could not be found",
                        newsSourceId, crawlingConfig.getId()));

        if (newsSource.isDeleted()) {
            throw new DataInitializationException(
                    "NewsSource '{}' referenced at crawling config '{}' is deleted",
                    newsSourceId, crawlingConfig.getId());
        }
        crawlingConfig.setNewsSource(newsSource);
        return newsSource;
    }

}
