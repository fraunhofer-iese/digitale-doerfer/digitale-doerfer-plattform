/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.SqlResultSetMapping;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.repos.results.DeliveryAndSellingPointAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
// The following SqlResultSetMapping definitions are needed for MobileKioskAdminMySQLRepository.
@SqlResultSetMapping(
        name = "DeliveryAndSellingPointAdminUIRowMapping",
        classes = @ConstructorResult(
                targetClass = DeliveryAndSellingPointAdminUIRow.class,
                columns = {
                        @ColumnResult(name = "ordered", type = Long.class),
                        @ColumnResult(name = "currentStatus"),
                        @ColumnResult(name = "lastStatusUpdate", type = Long.class),
                        @ColumnResult(name = "shopName"),
                        @ColumnResult(name = "pickupAddress"),
                        @ColumnResult(name = "receiver"),
                        @ColumnResult(name = "carrier"),
                        @ColumnResult(name = "deliveryAddress"),
                        @ColumnResult(name = "desiredDeliveryTimeStart", type = Long.class),
                        @ColumnResult(name = "desiredDeliveryTimeEnd", type = Long.class),
                        @ColumnResult(name = "purchaseOrderItems"),
                        @ColumnResult(name = "trackingCode"),
                        @ColumnResult(name = "size"),
                        @ColumnResult(name = "weight", type = Double.class),
                        @ColumnResult(name = "orderNotes", type = String.class),
                        @ColumnResult(name = "transportNotes", type = String.class),
                        @ColumnResult(name = "contentNotes", type = String.class),
                        @ColumnResult(name = "packed", type = Long.class),
                        @ColumnResult(name = "pickedUp", type = Long.class),
                        @ColumnResult(name = "delivered", type = Long.class),
                        @ColumnResult(name = "received", type = Long.class),
                        @ColumnResult(name = "statusHistory"),
                        @ColumnResult(name = "deliveryId"),
                        @ColumnResult(name = "purchaseOrderId"),
                        @ColumnResult(name = "shopOrderId"),
                        @ColumnResult(name = "senderShopId"),
                        @ColumnResult(name = "community"),
                        @ColumnResult(name = "receiverId"),
                        @ColumnResult(name = "sellingPointId"),
                        @ColumnResult(name = "sellingPointStatus")}))
public class SellingPoint extends BaseEntity {

    @ManyToOne
    private SellingVehicle vehicle;
    @ManyToOne
    private Tenant community;
    @ManyToOne
    private Address location;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "startTime", column = @Column(name = "plannedArrival")),
            @AttributeOverride(name = "endTime", column = @Column(name = "plannedDeparture"))
    })
    private TimeSpan plannedStay;
    
    @Enumerated(EnumType.STRING)
    @Column
    private SellingPointStatus status;

    @Override
    public String toString() {
        return "SellingPoint[" +
                "id = " + getId() +
                ", vehicleId=" + (vehicle != null ? vehicle.getId() : "null") +
                ", community=" + community +
                ", location=" + location +
                ", plannedStay=" + TimeSpan.toFriendlyHumanReadableString(plannedStay) +
                ", status=" + status +
                ']';
    }

    /**
     * Use {@link #getTenant()} instead
     */
    @SuppressWarnings("unused")
    @Deprecated
    private Tenant getCommunity() {
        return community;
    }

    /**
     * Use {@link #setTenant(Tenant)} instead
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void setCommunity(Tenant community) {
        this.community = community;
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @param tenant
     */
    public void setTenant(Tenant tenant) {
        this.community = tenant;
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @return the tenant of the person
     */
    public Tenant getTenant() {
        return community;
    }

}
