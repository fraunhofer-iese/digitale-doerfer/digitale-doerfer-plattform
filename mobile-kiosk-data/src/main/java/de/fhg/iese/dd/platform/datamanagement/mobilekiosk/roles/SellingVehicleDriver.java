/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2018 Balthasar Weitzel, Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.mobilekiosk.roles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingVehicle;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.repos.SellingVehicleRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@Component
@SuppressFBWarnings(value="SE_BAD_FIELD", justification="If this role gets serialized it needs new repositories anyway.")
public class SellingVehicleDriver extends BaseRole<SellingVehicle> {

    protected SellingVehicleDriver() {
        super(SellingVehicle.class);
    }

    @Autowired
    private SellingVehicleRepository sellingVehicleRepository;

    @Override
    public String getDisplayName() {
        return "Verkaufsfahrzeug-Fahrer";
    }

    @Override
    public String getDescription() {
        return "Fährt ein Verkaufsfahrzeug in der Gemeinde";
    }

    @Override
    public SellingVehicle getRelatedEntityById(String relatedEntityId) {
        return sellingVehicleRepository.findById(relatedEntityId).orElse(null);
    }

    @Override
    public boolean existsRelatedEntity(String relatedEntityId) {
        return sellingVehicleRepository.existsById(relatedEntityId);
    }

    @Override
    public Tenant getTenantOfRelatedEntity(SellingVehicle relatedEntity) {
        return relatedEntity.getTenant();
    }

}
