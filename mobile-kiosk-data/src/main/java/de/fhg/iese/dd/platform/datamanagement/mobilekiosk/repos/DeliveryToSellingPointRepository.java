/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2018 Balthasar Weitzel, Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.mobilekiosk.repos;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.DeliveryToSellingPoint;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;

public interface DeliveryToSellingPointRepository extends JpaRepository<DeliveryToSellingPoint, String> {

    Optional<DeliveryToSellingPoint> findOneByDelivery(Delivery delivery);

    Set<DeliveryToSellingPoint> findAllBySellingPoint(SellingPoint sellingPoint);
    
    Set<DeliveryToSellingPoint> findBySellingPointIdIn(Collection<String> sellingPointIds);
    
    Optional<DeliveryToSellingPoint> findFirstByDeliveryCustomReferenceNumber(String customReferenceNumber);

}
