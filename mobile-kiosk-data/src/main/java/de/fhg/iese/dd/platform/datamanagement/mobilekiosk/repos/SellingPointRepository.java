/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.mobilekiosk.repos;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPointStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

public interface SellingPointRepository extends JpaRepository<SellingPoint, String> {

    List<SellingPoint> findAllByCommunityAndPlannedStayEndTimeGreaterThanAndStatusOrderByPlannedStayStartTimeAsc(
            Tenant tenant, long timestamp, SellingPointStatus status);

    List<SellingPoint> findAllByCommunityAndPlannedStayStartTimeGreaterThanAndStatusOrderByPlannedStayStartTimeAsc(
            Tenant tenant, long timestamp, SellingPointStatus status);

    Set<SellingPoint> findAllByCommunityAndPlannedStayEndTimeLessThanAndStatus(Tenant tenant, long timestamp,
            SellingPointStatus status);

    Set<SellingPoint> findAllByCommunityAndPlannedStayEndTimeGreaterThan(Tenant tenant, long end);

    List<SellingPoint> findAllByCommunityAndPlannedStayStartTimeGreaterThanAndPlannedStayStartTimeLessThanOrderByPlannedStayStartTimeAsc(
            Tenant tenant, long start, long end);

    List<SellingPoint> findAllByCommunityAndPlannedStayStartTimeGreaterThanOrderByPlannedStayStartTimeAsc(Tenant tenant,
            long start);

    //PlannedStayStart is between start and end
    boolean existsByPlannedStayStartTimeGreaterThanEqualAndPlannedStayStartTimeLessThanEqualAndVehicleIdEquals(
            long start, long end, String vehicleId);

    //PlannedStayEnd is between start and end
    boolean existsByPlannedStayEndTimeGreaterThanEqualAndPlannedStayEndTimeLessThanEqualAndVehicleIdEquals(
            long start, long end, String vehicleId);

    //start to end contains PlannedStayStart to PlannedStayEnd
    boolean existsByPlannedStayStartTimeLessThanEqualAndPlannedStayEndTimeGreaterThanEqualAndVehicleIdEquals(
            long start, long end, String vehicleId);

}
