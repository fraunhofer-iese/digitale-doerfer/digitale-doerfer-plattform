/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.mobilekiosk;

import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory.PushCategoryId;

public final class MobileKioskConstants {

    public static final String DORFKURIER_APP_ID = "d15c4bf9-fef0-4121-b032-883d7ee39aed";

    public static final PushCategoryId DORFKURIER_PUSH_CATEGORY_GENERAL = new PushCategoryId("a882ccd9-685f-427d-af69-a3a7afcad0c1");

    public static final PushCategoryId DORFKURIER_PUSH_CATEGORY_ID_MOTIVATION = new PushCategoryId("21d1ab30-595a-4b47-be51-804f09bb376c");

    public static final PushCategoryId DORFKURIER_PUSH_CATEGORY_ID_OWN_PERSON_CHANGES = new PushCategoryId("4e772427-b51a-487f-8541-a8b0b36935fd");

}
