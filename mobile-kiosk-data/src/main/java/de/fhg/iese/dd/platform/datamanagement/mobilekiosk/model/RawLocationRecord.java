/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.enums.LocationSourceType;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class RawLocationRecord extends BaseEntity {

    @Column
    private long timestamp;

    @ManyToOne
    private SellingVehicle vehicle;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="latitude",column=@Column(name="locationLat", nullable = false)),
            @AttributeOverride(name="longitude",column=@Column(name="locationLong", nullable = false)),
    })
    private GPSLocation location;

    @Column
    private Double accuracy;

    @Column
    private LocationSourceType sourceType;

    @Column
    private String sourceId;

}
