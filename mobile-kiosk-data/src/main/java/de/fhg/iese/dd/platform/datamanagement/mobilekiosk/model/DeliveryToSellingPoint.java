/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2018 Balthasar Weitzel, Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * This class connects a delivery with a selling point. Since selling points are a concept special to mobile kiosk,
 * we do not want to have this connection directly in {@link Delivery}, but indirectly via this class.<br/>
 * Note that one delivery is connected to one DeliveryToSellingPoint (one-to-one), and one selling point can have multiple
 * deliveries and, thus, multiple DeliveryToSellingPoints (many-to-one). All in all, this class can be as an attachment
 * to Delivery (one-to-one), which manages the many-to-one connection to selling point.
 */
@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class DeliveryToSellingPoint extends BaseEntity {

    @OneToOne
    private Delivery delivery;
    @ManyToOne
    private SellingPoint sellingPoint;

    @Override
    public String toString() {
        return "DeliveryToSellingPoint[" +
                "id=" + getId() +
                ", deliveryId=" + (delivery != null ? delivery.getId() : null) +
                ", sellingPointId=" + (sellingPoint != null ? sellingPoint.getId() : null) +
                ']';
    }

}
