/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class SellingVehicle extends BaseEntity implements NamedEntity {

    private String name;

    @ManyToOne
    private Tenant community;

    @Override
    public String toString() {
        return "SellingVehicle[" +
                "id=" + getId() +
                ", name=" + name +
                ", community=" + community +
                ']';
    }

    /**
     * Use {@link #getTenant()} instead
     */
    @SuppressWarnings("unused")
    @Deprecated
    private Tenant getCommunity() {
        return community;
    }

    /**
     * Use {@link #setTenant(Tenant)} instead
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void setCommunity(Tenant community) {
        this.community = community;
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @param tenant
     */
    public void setTenant(Tenant tenant) {
        this.community = tenant;
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @return the tenant of the person
     */
    public Tenant getTenant() {
        return community;
    }

}
