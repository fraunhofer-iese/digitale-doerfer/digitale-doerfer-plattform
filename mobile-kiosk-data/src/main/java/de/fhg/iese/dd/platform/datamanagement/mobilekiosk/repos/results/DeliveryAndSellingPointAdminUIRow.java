/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.mobilekiosk.repos.results;

import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.DeliveryAdminUIRow;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DeliveryAndSellingPointAdminUIRow extends DeliveryAdminUIRow {

    private String sellingPointId;
    private String sellingPointStatus;

    public DeliveryAndSellingPointAdminUIRow(Long ordered, String currentStatus, Long lastStatusUpdate,
            String shopName, String pickupAddress, String receiver, String carrier, String deliveryAddress,
            Long desiredDeliveryTimeStart, Long desiredDeliveryTimeEnd, String purchaseOrderItems,
            String trackingCode, String size, Double weight, String orderNotes, String transportNotes,
            String contentNotes, Long packed, Long pickedUp, Long delivered, Long received, String statusHistory,
            String deliveryId, String purchaseOrderId, String shopOrderId, String senderShopId, String community,
            String receiverId, String sellingPointId, String sellingPointStatus) {
        super(ordered, currentStatus, lastStatusUpdate, shopName, pickupAddress, receiver, carrier, deliveryAddress,
                desiredDeliveryTimeStart, desiredDeliveryTimeEnd, purchaseOrderItems, trackingCode, size, weight,
                orderNotes, transportNotes, contentNotes, packed, pickedUp, delivered, received, statusHistory,
                deliveryId,
                purchaseOrderId, shopOrderId, senderShopId, community, receiverId);
        this.sellingPointId = sellingPointId;
        this.sellingPointStatus = sellingPointStatus;
    }

}
