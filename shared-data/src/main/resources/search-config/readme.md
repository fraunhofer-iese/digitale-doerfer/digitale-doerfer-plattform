# CONTEXT

Open/Elasticsearch can use files on the local file machine of the search server to configure analyzers or filters.

These files are copied from https://github.com/uschindler/german-decompounder
They aim to provide a reasonable German "de-compounding"
They are not automatically deployed, you need to deploy them manually to open/elasticsearch!
See https://docs.aws.amazon.com/opensearch-service/latest/developerguide/custom-packages.html for hints

# IMPORTANT

Currently, the AWS open/elasticsearch does not support non-dictionary files, so we can not use the xml file that would
provide better de-compounding. Nevertheless, we should use it when we run it on our own machines.