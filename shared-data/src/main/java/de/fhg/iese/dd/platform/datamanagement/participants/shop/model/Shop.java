/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.participants.shop.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@NamedEntityGraphs({
        @NamedEntityGraph(name = "Shop.reduced",
                includeAllAttributes = false),
        @NamedEntityGraph(name = "Shop.full",
                includeAllAttributes = true,
                attributeNodes = {
                        @NamedAttributeNode(value = "addresses", subgraph = "AddressListEntry.full"),
                }),
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class Shop extends BaseEntity implements NamedEntity {

    @Column
    private String name;

    /**
     * Associated MediaItem.
     * </p>
     * There is no need to delete the files associated with the MediaItem manually before deleting this entity.
     *
     * @see {@link MediaItem} for more details
     */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private MediaItem profilePicture;

    @Column
    private String webShopURL;

    @Column
    private String phone;

    @Column(name = "e_mail") //needed since the property previously was named eMail
    private String email;

    @Column
    private String smsNotificationNumber;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private OpeningHours openingHours;

    @ManyToOne
    private Tenant community;

    @OneToMany(fetch = FetchType.EAGER)
    private Set<AddressListEntry> addresses;

    @ManyToOne
    private Person owner;

    @Override
    public String toString() {
        return "Shop [" +
                "id='" + id + "', " +
                "name='" + name + "'" +
                ']';
    }

    /**
     * Use {@link #getTenant()} instead
     */
    @SuppressWarnings("unused")
    @Deprecated
    private Tenant getCommunity() {
        return community;
    }

    /**
     * Use {@link #setTenant(Tenant)} instead
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void setCommunity(Tenant community) {
        this.community = community;
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @param tenant
     */
    public void setTenant(Tenant tenant) {
        this.community = tenant;
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @return the tenant of the person
     */
    public Tenant getTenant() {
        return community;
    }

}
