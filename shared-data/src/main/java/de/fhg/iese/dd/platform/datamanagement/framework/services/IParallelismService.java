/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.services;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public interface IParallelismService {

    /**
     * An {@link Executor} that can be used for tasks that block on I/O (http calls, database requests, ...).
     */
    ExecutorService getBlockableExecutor();

    void submitToBlockableExecutorAndWaitForCompletion(Collection<Runnable> runnables, Duration timeout);

    <T> List<T> submitToBlockableExecutorAndWaitForResults(Collection<Callable<T>> callables, Duration timeout);

    <T> ComputationOutcomes<T> submitToBlockableExecutorAndWaitForComputationOutcome(Collection<Callable<T>> callables,
                                                                                     Duration timeout);

    <T> ComputationOutcomes<T> getFutureComputationOutcome(List<? extends Future<T>> futures, Duration timeout);

    ScheduledFuture<?> scheduleOneTimeTask(Runnable task, Instant startTime);

    record ComputationOutcomes<T>(List<ComputationOutcome<T>> computationOutcomes) {

        public List<Throwable> exceptions() {

            return computationOutcomes.stream()
                    .map(ComputationOutcome::exception)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }

        public List<T> results() {

            return computationOutcomes.stream()
                    .map(ComputationOutcome::result)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }

    }

    record ComputationOutcome<T>(T result, Throwable exception) {

    }

}
