/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.repos.listeners;

import org.hibernate.event.spi.PostDeleteEventListener;
import org.hibernate.persister.entity.EntityPersister;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;

public interface IEntityDeleteListener extends PostDeleteEventListener {

    long serialVersionUID = 1234165632240752097L;

    Class<? extends BaseEntity> relevantEntityClass();

    @Override
    @SuppressWarnings({"deprecation", "RedundantSuppression"})//deprecated due to typo ^^
    default boolean requiresPostCommitHanding(EntityPersister persister) {
        return requiresPostCommitHandling(persister);
    }

}
