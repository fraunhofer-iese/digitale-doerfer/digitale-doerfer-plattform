/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2022 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.security.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.security.exceptions.RoleNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;

@Converter
@SuppressWarnings("SpringJavaAutowiredMembersInspection") //Spring applies dependency injection to converters
public class RoleConverter implements AttributeConverter<Class<? extends BaseRole<? extends NamedEntity>>, String> {

    @SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection"})
    // spring can handle converter as beans
    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public String convertToDatabaseColumn(Class<? extends BaseRole<? extends NamedEntity>> role) {
        return applicationContext.getBean(role).getKey();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Class<? extends BaseRole<? extends NamedEntity>> convertToEntityAttribute(String roleKey) {
        return (Class<? extends BaseRole<? extends NamedEntity>>) applicationContext
                .getBeansOfType(BaseRole.class, false, true)
                .values()
                .stream()
                .filter(role -> role.getKey().equals(roleKey))
                .findAny()
                .map(Object::getClass)
                .orElseThrow(() ->
                        new RoleNotFoundException(
                                "Could not find role '{}' in application that was stored in database. "
                                        +
                                        "Either the database entry got changed manually or the key of the role changed in the meantime.",
                                roleKey));
    }

}
