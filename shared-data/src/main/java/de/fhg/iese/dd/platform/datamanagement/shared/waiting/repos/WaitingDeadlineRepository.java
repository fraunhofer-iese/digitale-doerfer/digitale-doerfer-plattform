/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.waiting.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.shared.waiting.model.WaitingDeadline;

@Transactional(readOnly = true)
public interface WaitingDeadlineRepository extends JpaRepository<WaitingDeadline, String> {

   String QUERY_findAllExpiredDeadlines =
            "SELECT waiting_deadline.* " +
            "FROM waiting_deadline " +
            "WHERE waiting_deadline.deadline < (?1) " +
            "ORDER BY waiting_deadline.deadline ASC" ;

    @Query(value=QUERY_findAllExpiredDeadlines, nativeQuery = true)
    List<WaitingDeadline> findAllExpiredDeadlinesOrderByDeadlineAsc(long now);
}
