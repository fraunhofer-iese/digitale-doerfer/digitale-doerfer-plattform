/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.exceptions;

import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;

/**
 * Wrapper for exceptions occurring at a concrete {@link IFileStorage}.
 *
 *
 */
public class FileStorageException extends InternalServerErrorException {

    private static final long serialVersionUID = 5903173961837945169L;

    public FileStorageException(String message, Object... arguments) {
        super(message, arguments);
    }

    public FileStorageException(String message, Exception e) {
        super(message, e);
    }

    public FileStorageException(String message) {
        super(message);
    }

    @Override
    public ClientExceptionType getClientExceptionType(){
        return ClientExceptionType.FILE_STORAGE_FAILED;
    }

}
