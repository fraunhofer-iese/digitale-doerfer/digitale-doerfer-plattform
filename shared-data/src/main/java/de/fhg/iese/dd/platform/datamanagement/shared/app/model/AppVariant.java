/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.app.model;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.DeletableEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.encryption.EncryptionConverter;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.Set;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Table(indexes = {
        @Index(columnList = "appVariantIdentifier", unique = true),
        @Index(columnList = "created"),
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class AppVariant extends BaseEntity implements NamedEntity, DeletableEntity {

    @Column
    private String appVariantIdentifier;

    @Column
    private String callBackUrl;

    @Column
    @Convert(converter = EncryptionConverter.class)
    private String apiKey1;

    @Column
    private Long apiKey1Created;

    @Column
    @Convert(converter = EncryptionConverter.class)
    private String apiKey2;

    @Column
    private Long apiKey2Created;

    @Column
    @Convert(converter = EncryptionConverter.class)
    private String externalApiKey;

    @Column
    @Convert(converter = EncryptionConverter.class)
    private String externalBasicAuth;

    @Column
    private Long externalApiKeyCreated;

    @Column
    private String name;

    @ManyToOne
    private App app;

    /**
     * id for this entity in the external push service, e.g. app ID in Pinpoint
     */
    @Column(length = 64)
    private String pushAppId;

    @Column
    private String updateUrlAndroid;

    @Column(name = "update_url_ios")
    private String updateUrlIOS;

    @Column
    private boolean supportsIos;

    @Column
    private boolean supportsAndroid;

    @Column
    private boolean supportsWeb;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<OauthClient> oauthClients;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private MediaItem logo;

    @Column(nullable = false)
    @Builder.Default
    private boolean deleted = false;

    @Column
    private Long deletionTime;

    @Override
    public String toString() {
        return "AppVariant ["
                + "id=" + id + ", "
                + "appVariantIdentifier=" + appVariantIdentifier + ", "
                + "name=" + name
                + (app != null ? (", app=" + app.getAppIdentifier()) : "")
                + "]";
    }

    public boolean hasExternalSystem() {
        return StringUtils.isNotBlank(callBackUrl);
    }

    public String getExternalBasicAuthUsername() {
        if (StringUtils.isNotBlank(externalBasicAuth)) {
            return StringUtils.substringBefore(externalBasicAuth, ":");
        }
        return "";
    }

    public String getExternalBasicAuthPassword() {
        if (StringUtils.isNotBlank(externalBasicAuth)) {
            return StringUtils.substringAfter(externalBasicAuth, ":");
        }
        return "";
    }

    public AppVariant withConstantId() {
        super.generateConstantId(getAppVariantIdentifier());
        return this;
    }

    public AppVariant withConstantId(BaseEntity seedEntity) {
        super.generateConstantId(seedEntity);
        return this;
    }

}
