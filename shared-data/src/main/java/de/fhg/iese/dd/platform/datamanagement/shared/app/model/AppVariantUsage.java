/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2019 Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Table(
        uniqueConstraints = {@UniqueConstraint(columnNames = {"app_variant_id", "person_id"})}
)
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class AppVariantUsage extends BaseEntity {

    @ManyToOne
    private AppVariant appVariant;

    @ManyToOne
    private Person person;

    @Column
    @Builder.Default
    private long lastUsage = System.currentTimeMillis();

    public AppVariantUsage withConstantId() {
        super.generateConstantId(appVariant, person);
        return this;
    }

    @Override
    public String toString() {
        return "AppVariantUsage ["
                + (id != null ? "id=" + id + ", " : "")
                + (appVariant != null ? "appVariant=" + appVariant.getAppVariantIdentifier() + ", " : "")
                + (person != null ? "person=" + person.getId() : "") + "]";
    }

}
