/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.services;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;

@Service
@Profile({"!test"})
class TimeService implements ITimeService {

    private static final DateTimeFormatter FORMATTER_HUMAN_READABLE =
            DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
    private static final DateTimeFormatter FORMATTER_PRECISE_HUMAN_READABLE =
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");

    private final ZoneId timeZone;

    @Autowired
    TimeService(ApplicationConfig applicationConfig) {
        timeZone = ZoneId.of(applicationConfig.getLocalTimeZone());
    }

    @Override
    public ZoneId getLocalTimeZone() {
        return timeZone;
    }

    @Override
    public long currentTimeMillisUTC() {
        return System.currentTimeMillis();
    }

    @Override
    public ZonedDateTime nowLocal() {
        return ZonedDateTime.now(Clock.system(timeZone));
    }

    @Override
    public ZonedDateTime nowUTC() {
        return ZonedDateTime.now(Clock.system(ZoneOffset.UTC));
    }

    @Override
    public ZonedDateTime toLocalTime(long timestampUTC) {
        return Instant.ofEpochMilli(timestampUTC).atZone(timeZone);
    }

    @Override
    public long toTimeMillis(final ZonedDateTime dateTime) {
        return dateTime.toInstant().toEpochMilli();
    }

    @Override
    public <T extends BaseEntity> T setCreatedToNow(T entity) {
        entity.setCreated(currentTimeMillisUTC());
        return entity;
    }

    @Override
    public String toLocalTimeHumanReadable(final Long timestampUTC) {
        if ((timestampUTC == null) || (timestampUTC == 0)) {
            return "";
        }
        return toLocalTime(timestampUTC).format(FORMATTER_HUMAN_READABLE);
    }

    @Override
    public String toLocalTimePreciseHumanReadable(Long timestampUTC) {
        if ((timestampUTC == null) || (timestampUTC == 0)) {
            return "";
        }
        return toLocalTime(timestampUTC).format(FORMATTER_PRECISE_HUMAN_READABLE);
    }

    @Override
    public ZonedDateTime getSameDayStart(ZonedDateTime now) {

        return now.truncatedTo(ChronoUnit.DAYS);
    }

    @Override
    public ZonedDateTime getSameWeekStart(ZonedDateTime now) {
        int daysToMonday = now.getDayOfWeek().getValue() - 1;

        return now.truncatedTo(ChronoUnit.DAYS).minusDays(daysToMonday);
    }

    @Override
    public ZonedDateTime getSameMonthStart(ZonedDateTime now) {
        int daysToFirstOfMonth = now.getDayOfMonth() - 1;

        return now.truncatedTo(ChronoUnit.DAYS).minusDays(daysToFirstOfMonth);
    }

    @Override
    public ZonedDateTime getSameYearStart(ZonedDateTime now) {
        int daysToFirstOfYear = now.getDayOfYear() - 1;

        return now.truncatedTo(ChronoUnit.DAYS).minusDays(daysToFirstOfYear);
    }

    @Override
    public ZonedDateTime getPreviousWeekStart(ZonedDateTime now) {

        return getSameWeekStart(now).minusWeeks(1);
    }

    @Override
    public ZonedDateTime getPreviousWeekEnd(ZonedDateTime now) {

        return getSameWeekStart(now).minus(1, ChronoUnit.MILLIS);
    }

    @Override
    public ZonedDateTime getPreviousDayStart(ZonedDateTime now) {

        return getSameDayStart(now).minusDays(1);
    }

    @Override
    public ZonedDateTime getPreviousDayEnd(ZonedDateTime now) {

        return getSameDayStart(now).minus(1, ChronoUnit.MILLIS);
    }

    @Override
    public ZonedDateTime getPreviousMonthStart(ZonedDateTime now) {

        return getSameMonthStart(now).minusMonths(1);
    }

    @Override
    public ZonedDateTime getPreviousMonthEnd(ZonedDateTime now) {

        return getSameMonthStart(now).minus(1, ChronoUnit.MILLIS);
    }

}
