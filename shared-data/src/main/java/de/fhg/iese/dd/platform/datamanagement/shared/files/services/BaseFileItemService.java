/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.BaseFileItemConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemCannotBeDeletedException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.MediaItemUsageNotAllowedException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.TemporaryFileItemDuplicateUsageException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.TemporaryFileItemNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.BaseFileItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryBaseFileItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.BaseFileItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.TemporaryBaseFileItemRepository;
import lombok.extern.log4j.Log4j2;

/**
 * Abstract service for the abstract {@link BaseFileItem} that abstracts from {@link MediaItem} and
 * {@link DocumentItem}.
 *
 * @param <F> the concrete subtype of {@link BaseFileItem} that is wrapped in the temporary item
 * @param <T> the concrete subtype of {@link TemporaryBaseFileItem} that gets managed
 */
@Log4j2
abstract class BaseFileItemService<F extends BaseFileItem, T extends TemporaryBaseFileItem<F>> implements IBaseFileItemService<F, T> {

    @Autowired
    protected ITimeService timeService;
    @Autowired
    protected IFileStorage fileStorage;

    protected abstract BaseFileItemConfig getConfig();

    protected abstract BaseFileItemRepository<F> getItemRepository();

    protected abstract TemporaryBaseFileItemRepository<F, T> getTemporaryItemRepository();

    private Set<String> allowedFileExtensions;

    protected Set<String> getAllowedFileExtensions() {

        if (allowedFileExtensions == null) {
            allowedFileExtensions = new HashSet<>();
            for (String allowedMediaType : getConfig().getAllowedMediaTypes()) {
                try {
                    final MimeType mimeType = fileStorage.getTikaConfig().getMimeRepository().forName(allowedMediaType);
                    final List<String> extensions = mimeType.getExtensions();
                    if (!CollectionUtils.isEmpty(extensions)) {
                        extensions.stream()
                                .map(ex -> StringUtils.removeStart(ex, "."))
                                .forEach(allowedFileExtensions::add);
                    }
                } catch (MimeTypeException e) {
                    log.error("Invalid allowed mime type {} in config: {}", allowedMediaType, e);
                }
            }
        }
        return allowedFileExtensions;
    }

    @Override
    public F findItemById(String id) {
        return getItemRepository().findById(id)
                .orElseThrow(() -> new FileItemNotFoundException(id));
    }

    @Override
    public T findTemporaryItemById(Person owner, String id) {
        return getTemporaryItemRepository().findByOwnerAndId(owner, id)
                .orElseThrow(() -> new TemporaryFileItemNotFoundException(id));
    }

    @Override
    public List<T> findTemporaryItemsById(Person owner, List<String> ids)
            throws TemporaryFileItemNotFoundException, TemporaryFileItemDuplicateUsageException {
        //if there are no ids there is no need to look something up
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }

        List<String> duplicateIds = BaseEntity.findDuplicateIds(ids);
        if (!duplicateIds.isEmpty()) {
            throw new TemporaryFileItemDuplicateUsageException(duplicateIds);
        }
        //get the entities in random order
        List<T> foundItems = getTemporaryItemRepository().findAllByOwnerAndIdIn(owner, ids);
        //since the ids are unique this check is enough to find out if we got all
        if (foundItems.size() != ids.size()) {

            Set<String> foundIds = foundItems.stream()
                    .map(BaseEntity::getId)
                    .collect(Collectors.toSet());

            List<String> notFoundIds = ids.stream()
                    .filter(i -> !foundIds.contains(i))
                    .collect(Collectors.toList());

            throw new TemporaryFileItemNotFoundException(notFoundIds);
        }
        //sort the returned entities according to the order of the ids we got
        foundItems.sort(Comparator.comparingInt(o -> ids.indexOf(o.getId())));
        return foundItems;
    }

    @Override
    public Set<T> findTemporaryItemsByIdUnordered(Person owner, List<String> ids)
            throws TemporaryFileItemNotFoundException, TemporaryFileItemDuplicateUsageException {

        return new HashSet<>(findTemporaryItemsById(owner, ids));
    }

    @Override
    public List<T> findAllTemporaryItemsByOwner(Person owner) {
        return getTemporaryItemRepository().findAllByOwnerOrderByCreatedDesc(owner);
    }

    @Override
    public Page<F> findAllItemsOwnedByAppVariant(AppVariant appVariant, boolean userOwned,
            Pageable pageable) {
        if (userOwned) {
            return getItemRepository().findAllByAppVariantOrderByCreatedDesc(appVariant, pageable);
        } else {
            return getItemRepository().findAllByAppVariantAndOwnerIsNullOrderByCreatedDesc(appVariant, pageable);
        }
    }

    @Override
    public List<F> findAllItemsOwnedByPerson(Person owner) {
        return getItemRepository().findAllByOwnerOrderByCreatedDesc(owner);
    }

    @Override
    @Transactional
    public List<F> findItemsByIdOwnedByAppVariant(AppVariant appVariant, List<String> ids)
            throws FileItemNotFoundException, MediaItemUsageNotAllowedException {
        // if there are no ids there is no need to look something up
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }
        // check if there exist a matching media item for each id
        List<F> mediaItems = getItemRepository().findAllByIdIn(ids);
        if (mediaItems.size() != ids.size()) {

            Set<String> foundIds = mediaItems.stream()
                    .map(BaseEntity::getId)
                    .collect(Collectors.toSet());

            List<String> notFoundIds = ids.stream()
                    .filter(i -> !foundIds.contains(i))
                    .collect(Collectors.toList());

            throw new FileItemNotFoundException(notFoundIds);
        }
        // check if all media items are owned by app variant
        List<F> ownedMediaItems = mediaItems.stream()
                .filter(mi -> mi.getAppVariant() != null &&
                        mi.getAppVariant().equals(appVariant))
                .collect(Collectors.toList());
        if (ownedMediaItems.size() != mediaItems.size()) {

            Set<String> foundIds = ownedMediaItems.stream()
                    .map(BaseEntity::getId)
                    .collect(Collectors.toSet());

            List<String> notFoundIds = mediaItems.stream()
                    .map(BaseEntity::getId)
                    .filter(i -> !foundIds.contains(i))
                    .collect(Collectors.toList());

            throw new MediaItemUsageNotAllowedException(appVariant, notFoundIds);
        }
        return ownedMediaItems;
    }

    @Override
    public void deleteItem(F item) {
        getItemRepository().delete(item);
    }

    @Override
    public void deleteAllItems(Iterable<F> items) throws FileItemCannotBeDeletedException {

        List<String> notDeletedItemIds = new ArrayList<>();
        for (F item : items) {
            try {
                getItemRepository().delete(item);
            } catch (Exception ex) {
                notDeletedItemIds.add(item.getId());
            }
        }
        // check if there are media items that could not be deleted (references)
        if (!CollectionUtils.isEmpty(notDeletedItemIds)) {
            throw new FileItemCannotBeDeletedException(notDeletedItemIds);
        }
    }

    @Override
    public void deleteAllItemsByOwner(Person owner) {
        getItemRepository().deleteAllByOwner(owner);
    }

    @Override
    public void deleteAllTemporaryItemsByOwner(Person owner) {
        getTemporaryItemRepository().deleteAllByOwner(owner);
    }

    @Override
    public void deleteTemporaryItemById(Person owner, String id) {
        long deletedItems = getTemporaryItemRepository().deleteByOwnerAndId(owner, id);
        if (deletedItems < 1) {
            throw new TemporaryFileItemNotFoundException(id);
        }
    }

    @Override
    public void deleteExpiredTemporaryItems() {
        long currentTime = timeService.currentTimeMillisUTC();
        long deletedItems = getTemporaryItemRepository().deleteAllByExpirationTimeBefore(currentTime);
        getTemporaryItemRepository().flush();
        log.info("Deleted {} TemporaryMediaItems", deletedItems);
    }

    @Override
    public F useItem(T temporaryItem, FileOwnership fileOwnership) {
        F F = temporaryItem.getFileItem();
        F.setFileOwnership(fileOwnership);
        F = getItemRepository().saveAndFlush(F);
        //un-attach the file item, so that we can safely delete this entity without deleting the media item
        temporaryItem.setFileItem(null);
        getTemporaryItemRepository().save(temporaryItem);
        //now we can just delete this entity
        getTemporaryItemRepository().delete(temporaryItem);
        getTemporaryItemRepository().flush();
        return F;
    }

    @Override
    public List<F> useAllItems(List<T> temporaryItems, FileOwnership fileOwnership)
            throws TemporaryFileItemDuplicateUsageException {

        if (CollectionUtils.isEmpty(temporaryItems)) {
            return Collections.emptyList();
        }

        List<String> duplicateIds = BaseEntity.findDuplicateEntities(temporaryItems);
        if (!duplicateIds.isEmpty()) {
            throw new TemporaryFileItemDuplicateUsageException(duplicateIds);
        }

        List<F> items = new ArrayList<>(temporaryItems.size());
        useItems(temporaryItems, fileOwnership, items);
        return items;
    }

    @Override
    public Set<F> useAllItemsUnordered(Set<T> temporaryItems, FileOwnership fileOwnership) {

        if (CollectionUtils.isEmpty(temporaryItems)) {
            return Collections.emptySet();
        }

        Set<F> items = new HashSet<>(temporaryItems.size());
        useItems(temporaryItems, fileOwnership, items);
        return items;
    }

    @Override
    public Runnable getOrphanItemDeletion(Collection<F> existingItems, Collection<F> newItems) {
        if (CollectionUtils.isEmpty(existingItems)) {
            return () -> {
            };
        }
        final List<F> orphanItems;
        if (CollectionUtils.isEmpty(newItems)) {
            //we create a copy here so that we do not mess up anything with the special hibernate lists
            orphanItems = new ArrayList<>(existingItems);
        } else {
            orphanItems = existingItems.stream()
                    .filter(i -> !newItems.contains(i))
                    .collect(Collectors.toList());
        }
        return () -> deleteAllItems(orphanItems);
    }

    private void useItems(Collection<T> temporaryItems, FileOwnership fileOwnership, Collection<F> items) {
        for (T temporaryItem : temporaryItems) {
            F F = temporaryItem.getFileItem();
            F.setFileOwnership(fileOwnership);
            items.add(temporaryItem.getFileItem());
            //un-attach the media item, so that we can safely delete this entity without deleting the media item
            temporaryItem.setFileItem(null);
        }
        getItemRepository().saveAll(items);
        getItemRepository().flush();
        getTemporaryItemRepository().saveAll(temporaryItems);
        //now we can just delete these entities
        getTemporaryItemRepository().deleteAll(temporaryItems);
        getTemporaryItemRepository().flush();
    }

}
