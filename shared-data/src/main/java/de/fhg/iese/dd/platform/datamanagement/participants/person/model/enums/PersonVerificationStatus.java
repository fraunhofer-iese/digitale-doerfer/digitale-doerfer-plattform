/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums;

import de.fhg.iese.dd.platform.datamanagement.framework.enums.StorableEnum;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Verification status of a person. A person can have different verification statuses. An unverified person has no
 * status, there is no separate status for that.
 *
 * @see Person#getVerificationStatuses()
 */
@AllArgsConstructor
@Getter
public enum PersonVerificationStatus implements StorableEnum {

    /**
     * {@link Person#getEmail()} has been verified
     */
    EMAIL_VERIFIED(1 << 1),

    /**
     * {@link Person#getPhoneNumber()} has been verified
     */
    PHONE_NUMBER_VERIFIED(1 << 2),

    /**
     * {@link Person#getFirstName()}, {@link Person#getLastName()} has been verified
     */
    NAME_VERIFIED(1 << 3),

    /**
     * {@link Person#getHomeArea()}} has been verified
     */
    HOME_AREA_VERIFIED(1 << 4),

    /**
     * {@link Person#getAddresses()}} has been verified
     */
    ADDRESS_VERIFIED(1 << 5);

    private final int bitMaskValue;

}
