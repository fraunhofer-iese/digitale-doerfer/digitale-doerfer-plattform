/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2020 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.participants.tenant.model;

import java.util.Collections;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * A tenant contains all persons that should be treated with the same configuration.
 */
@Entity(name = "Community") //this name is used in queries
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class Tenant extends BaseEntity implements NamedEntity {

    @Column
    private String name;

    /**
     * A tag helps to group several tenants together that share the same configuration. It is especially relevant for
     * the data init.
     */
    @Column
    private String tag;

    /**
     * Associated MediaItem.
     * </p>
     * There is no need to delete the files associated with the MediaItem manually before deleting this entity.
     *
     * @see MediaItem for more details
     */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private MediaItem profilePicture;

    @ManyToOne
    private GeoArea rootArea;

    @Override
    public String toString() {
        if (tag == null) {
            return "Tenant [id=" + id + ", name=" + name + "]";
        } else {
            return "Tenant [id=" + id + ", tag=" + tag + ", name=" + name + "]";
        }
    }
    
    private static final Set<Tenant> emptySetToFixHQLBugOnEmptySet =
            Collections.singleton(new Tenant());

    /**
     * HQL can not deal with IN conditions and empty sets, it constructs an invalid query. To fix this a fake entity is
     * used that never matches a real entity.
     */
    public static Set<Tenant> toInSafeEntitySet(Set<Tenant> tenants) {
        if (CollectionUtils.isEmpty(tenants)) {
            return emptySetToFixHQLBugOnEmptySet;
        } else {
            return tenants;
        }
    }

}
