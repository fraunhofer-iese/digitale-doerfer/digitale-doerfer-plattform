/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services.init;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import lombok.extern.log4j.Log4j2;

@Log4j2
abstract class BaseFileProvider implements IFileProvider {

    private final Map<String, Boolean> existingFiles = new HashMap<>();

    protected abstract Path getDataInitRootFolder();

    protected abstract void ensureInitialized();

    @Override
    public String checkoutInitData(LogSummary logSummary) throws DataInitializationException {
        ensureInitialized();
        existingFiles.clear();
        return "";
    }

    @Override
    public Collection<String> findDataInitFiles(Pattern pattern) {
        ensureInitialized();
        final Path dataInitRootFolder = getDataInitRootFolder();
        try {
            try (Stream<Path> pathStream = Files.find(dataInitRootFolder, 100, (p, f) -> {
                if (!f.isRegularFile()) {
                    return false;
                }
                final String relativeName = dataInitRootFolder.relativize(p).toString();
                return pattern.matcher(relativeName).matches();
            }, FileVisitOption.FOLLOW_LINKS)) {
                return pathStream
                        .map(dataInitRootFolder::relativize)
                        .map(Path::toString)
                        .collect(Collectors.toList());
            }

        } catch (IOException e) {
            log.error("Could not find files at '" + dataInitRootFolder + "'", e);
        }
        return Collections.emptyList();
    }

    @Override
    public InputStream loadInitDataFile(String location) {
        ensureInitialized();
        if (!existingFiles.getOrDefault(location, true)) {
            return null;
        }

        //resolve the relative location to an absolute path
        //remove any starting / to be sure that we do not interpret the location as absolute path
        Path fileLocation = getDataInitRootFolder().resolve(StringUtils.removeStart(location, File.separator));
        if (Files.exists(fileLocation)) {
            existingFiles.put(location, true);
            try {
                return Files.newInputStream(fileLocation, StandardOpenOption.READ);
            } catch (IOException e) {
                log.error("Could not open init file at '" + location + "'", e);
            }
        }
        existingFiles.put(location, false);
        return null;
    }

    @Override
    public void cleanup(LogSummary logSummary) {
        ensureInitialized();
        existingFiles.clear();
    }

}
