/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2022 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.regex.Pattern;
import java.util.zip.ZipException;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MimeType;
import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.FileStorageConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileTypeNotDetectableException;
import lombok.extern.log4j.Log4j2;

@Log4j2
public abstract class BaseFileStorage implements IFileStorage {

    protected final Pattern FILE_NAME_SANITIZATION_PATTERN = Pattern.compile("[^a-zA-Z0-9-_\\.]");
    protected static final String PATH_SEPARATOR = "/";

    protected final FileStorageConfig fileStorageConfig;

    private TikaConfig tikaConfig;

    public BaseFileStorage(FileStorageConfig fileStorageConfig) {
        this.fileStorageConfig = fileStorageConfig;
    }

    @Override
    public MediaTypeAndFileExtension determineMediaTypeAndFileExtension(byte[] content, @Nullable String fileName)
            throws FileTypeNotDetectableException {
        Metadata metadata = new Metadata();
        if (StringUtils.isNotEmpty(fileName)) {
            metadata.set(TikaCoreProperties.RESOURCE_NAME_KEY, fileName);
        }
        try (InputStream stream = TikaInputStream.get(content, metadata)) {
            MediaType detectedMediaType = getTikaConfig().getDetector().detect(stream, metadata);
            MimeType detectedMimeType = getTikaConfig().getMimeRepository().forName(detectedMediaType.toString());
            //tika returns the extension with a dot, we need it without
            String detectedExtension = StringUtils.removeStart(detectedMimeType.getExtension(), ".");
            return MediaTypeAndFileExtension.builder()
                    .fileExtension(detectedExtension)
                    .mediaType(detectedMimeType.toString())
                    .build();
        } catch (Exception e) {
            throw new FileTypeNotDetectableException(
                    "The media type / file extension of the file could not be detected.", e);
        }
    }

    @Override
    public String appendFileName(String folderName, String fileName) {
        return StringUtils.appendIfMissing(folderName, PATH_SEPARATOR) +
                StringUtils.removeStart(fileName, PATH_SEPARATOR);
    }

    @Override
    public List<String> splitFileName(String fileName) {
        return Arrays.asList(StringUtils.split(fileName, PATH_SEPARATOR));
    }

    @Override
    public String sanitizeFileName(String rawFileName) {
        final String strippedFilename = StringUtils.stripAccents(rawFileName);
        return FILE_NAME_SANITIZATION_PATTERN.matcher(strippedFilename).replaceAll("_");
    }

    @Override
    public String getExternalBaseUrl() {
        return fileStorageConfig.getExternalBaseUrl();
    }

    @Override
    public String getExternalUrl(String internalFileName) {
        return getExternalBaseUrl() + internalFileName;
    }

    @Override
    public String getInternalFileName(String externalUrl) {
        return StringUtils.removeStart(externalUrl, getExternalBaseUrl());
    }

    @Override
    public void deleteOldFiles(final String folder, final Duration maxAge,
            final BiFunction<String, Duration, Boolean> onDelete) {
        for (final String fileName : findAll(folder)) {
            final Duration fileAge = getAge(fileName);
            if (fileAge.compareTo(maxAge) > 0) {
                if ((onDelete == null) || onDelete.apply(fileName, fileAge)) {
                    deleteFile(fileName);
                }
            }
        }
    }

    @Override
    public TikaConfig getTikaConfig() {
        if (tikaConfig == null) {
            tikaConfig = TikaConfig.getDefaultConfig();
        }
        return tikaConfig;
    }

    @Override
    public String createZipFile(String internalZipFileName, List<String> internalFileNames, List<String> pathsToFlatten)
            throws FileStorageException {

        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ZipArchiveOutputStream archive = new ZipArchiveOutputStream(bos)) {
            for (String internalFileName : internalFileNames) {
                try {
                    InputStream fis = getFile(internalFileName);
                    ZipArchiveEntry entry =
                            new ZipArchiveEntry(getZipInternalFileName(internalFileName, pathsToFlatten));
                    archive.putArchiveEntry(entry);
                    IOUtils.copy(fis, archive);
                    fis.close();
                    archive.closeArchiveEntry();
                } catch (ZipException e) {
                    log.error("Error adding entry '" + internalFileName + "' to zip", e);
                }
            }
            archive.finish();
            byte[] zipContent = bos.toByteArray();
            saveFile(zipContent, MediaType.APPLICATION_ZIP.toString(), internalZipFileName);
            return getExternalUrl(internalZipFileName);

        } catch (IOException e) {
            throw new FileStorageException("Failed to create zip file", e);
        }
    }

    private String getZipInternalFileName(final String fileName, final List<String> pathsToFlatten) {

        if (StringUtils.isBlank(fileName) || CollectionUtils.isEmpty(pathsToFlatten)) {
            return fileName;
        }

        String zipFileName = fileName;
        for (String pathToFlatten : pathsToFlatten) {
            zipFileName = StringUtils.remove(zipFileName, pathToFlatten);
        }
        return zipFileName;
    }

}
