/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2022 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.repos;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;

@NoRepositoryBean
@Transactional(readOnly = true)
public interface MediaItemRepository extends BaseFileItemRepository<MediaItem> {

    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD, attributePaths = {"owner", "appVariant", "app"})
    @Query("select m from MediaItem m where m.id = :id")
    MediaItem findByIdFull(String id);

    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD, attributePaths = {"owner", "appVariant", "app"})
    @Query("select m from MediaItem m")
    List<MediaItem> findAllFull();

    Page<MediaItem> findAllBySizeVersionIsLessThanOrderByCreatedDesc(int sizeVersion, Pageable request);

    //implemented in the database specific repositories
    List<Object[]> findTableAndColumnReferencingMediaItems(String schema);

    Page<MediaItem> findAllByIdNotInOrderByCreatedDesc(Set<String> mediaItemIdsToNotFind, Pageable request);

    Page<MediaItem> findAllByIdNotInAndUrlsJsonIsNullOrderByCreatedDesc(Set<String> mediaItemIdsToNotFind,
            Pageable request);

}
