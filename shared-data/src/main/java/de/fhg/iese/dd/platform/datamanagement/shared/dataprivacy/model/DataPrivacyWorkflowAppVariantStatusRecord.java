/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowAppVariantStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(indexes = {
        @Index(columnList = "status"),
        @Index(columnList = "created")
})
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class DataPrivacyWorkflowAppVariantStatusRecord extends BaseEntity {

    @ManyToOne(optional = false)
    private DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DataPrivacyWorkflowAppVariantStatus status;

    @Override
    public String toString() {
        return "DataPrivacyWorkflowAppVariantStatusRecord [" +
                "id='" + id + "', " +
                "dataPrivacyWorkflowAppVariant='" + (dataPrivacyWorkflowAppVariant != null ?
                dataPrivacyWorkflowAppVariant.getAppVariant().getAppVariantIdentifier() : null) + "', " +
                "status='" + status +
                ']';
    }

}
