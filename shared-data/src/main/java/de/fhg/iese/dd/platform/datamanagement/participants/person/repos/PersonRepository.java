/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2022 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Johannes Schneider, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.participants.person.repos;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.results.PersonCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;

@Transactional(readOnly = true)
public interface PersonRepository extends JpaRepository<Person, String> {

    /**
     * Person with profilePicture, homeArea and tenant fetched
     */
    @EntityGraph(value = "Person.normal", type = EntityGraphType.FETCH)
    Optional<Person> findById(String personId);
    
    /**
     * Finds a person by oauthId. If there are multiple with the same id only one is returned.
     *
     * @param oauthId
     *
     * @return
     */
    @EntityGraph(value = "Person.normal", type = EntityGraphType.FETCH)
    Optional<Person> findByOauthId(String oauthId);

    /**
     * Finds a person by Email.
     *
     * @param email
     *
     * @return
     */
    @EntityGraph(value = "Person.normal", type = EntityGraphType.LOAD)
    Optional<Person> findByEmail(String email);

    @Query("select count(p) > 0 " +
            "from Person p " +
            "where p.email = :email or p.pendingNewEmail = :email")
    boolean existsByEmailOrPendingNewEmail(String email);

    boolean existsById(String id);

    boolean existsByOauthId(String oauthId);

    boolean existsByOauthIdAndDeletedTrue(String oauthId);

    boolean existsByOauthIdAndDeletedFalse(String oauthId);

    Page<Person> findAllByDeletedFalse(Pageable page);

    @Query("select p.addresses from Person p where p = :person")
    Set<AddressListEntry> findAllAddressListEntriesByPerson(Person person);
    
    long countAllByCreatedBetween(long start, long end);

    long countAllByCreatedBetweenAndCommunity(long start, long end, Tenant tenant);

    long countAllByCreatedBetweenAndCommunityTag(long start, long end, String tag);

    long countAllByDeletionTimeBetweenAndDeletedTrue(long start, long end);

    long countAllByDeletionTimeBetweenAndDeletedTrueAndCommunity(long start, long end, Tenant tenant);

    long countAllByDeletionTimeBetweenAndDeletedTrueAndCommunityTag(long start, long end, String tag);

    long countAllByLastLoggedInGreaterThan(long start);

    long countAllByLastLoggedInGreaterThanAndCommunity(long start, Tenant tenant);

    long countAllByLastLoggedInGreaterThanAndCommunityTag(long start, String tag);

    @Query("select new de.fhg.iese.dd.platform.datamanagement.participants.person.repos.results.PersonCountByGeoAreaId(" +
            "p.homeArea.id, " +
            "sum( case when p.deleted = false then 1 else 0 end), " +
            "sum( case when p.deleted = true  then 1 else 0 end), " +
            "sum( case when p.created >= :created then 1 else 0 end), " +
            "sum( case when (p.deleted = true and p.deletionTime >= :created) then 1 else 0 end), " +
            "sum( case when p.lastLoggedIn >= :lastLoggedIn then 1 else 0 end)) " +
            "from Person p group by p.homeArea.id")
    List<PersonCountByGeoAreaId> countByHomeGeoAreaId(long created, long lastLoggedIn);

    long countByHomeArea(GeoArea homeArea);

    long countByHomeAreaAndDeletedFalse(GeoArea homeArea);

    long countByCommunity(Tenant tenant);

    @Transactional
    @Modifying
    @Query("update Person p set p.homeArea = :newHomeArea where p.homeArea = :oldHomeArea")
    int updateHomeArea(GeoArea oldHomeArea, GeoArea newHomeArea);

    @Transactional
    @Modifying
    @Query("update Person p set p.community = :newTenant where p.community = :oldTenant")
    int updateTenant(Tenant oldTenant, Tenant newTenant);

    @Transactional
    @Modifying
    @Query("update Person p set p.homeArea = :newHomeArea, p.community = :newTenant where p.homeArea = :oldHomeArea")
    int updateHomeAreaAndTenant(GeoArea oldHomeArea, GeoArea newHomeArea, Tenant newTenant);

    @Query("select p from Person p " +
            "where p.lastLoggedIn < :lastLoggedIn " +
            "and bitand(p.status, :forbiddenStatusBitMask) = 0 " +
            "and bitand(p.verificationStatus, :requiredVerificationStatusBitMask) != 0 " +
            "and p.deleted = false " +
            "order by p.lastLoggedIn asc")
    Page<Person> findByLastLoggedInBeforeWithoutStatusWithVerificationStatus(int forbiddenStatusBitMask,
            int requiredVerificationStatusBitMask, long lastLoggedIn, Pageable page);

    @Query("select p from Person p " +
            "where p.lastLoggedIn < :lastLoggedIn " +
            "and bitand(p.status, :requiredStatusBitMask) != 0 " +
            "and bitand(p.verificationStatus, :forbiddenVerificationStatusBitMask) = 0 " +
            "and p.deleted = false " +
            "order by p.lastLoggedIn asc")
    Page<Person> findByLastLoggedInBeforeWithStatusWithoutVerificationStatus(
            int requiredStatusBitMask, int forbiddenVerificationStatusBitMask, long lastLoggedIn, Pageable page);

    @Query("select p from Person p " +
            "where p.lastLoggedIn < :lastLoggedIn " +
            "and bitand(p.status, :forbiddenStatusBitMask) = 0 " +
            "and bitand(p.verificationStatus, :forbiddenVerificationStatusBitMask) = 0 " +
            "and p.deleted = false " +
            "order by p.lastLoggedIn asc")
    Page<Person> findByLastLoggedInBeforeWithoutStatusWithoutVerificationStatus(
            int forbiddenStatusBitMask, int forbiddenVerificationStatusBitMask, long lastLoggedIn, Pageable page);

    @Query("select p from Person p " +
            "where p.lastSentTimePendingDeletionWarning is not null " +
            "and p.lastSentTimePendingDeletionWarning < :pendingDeletionWarning " +
            "and bitand(p.status, :requiredStatusBitMask) != 0 " +
            "and p.deleted = false " +
            "order by p.lastSentTimePendingDeletionWarning asc")
    Page<Person> findAllByStatusAndPendingDeletionWarningBeforeAndNotDeletedOrderByPendingDeletionWarningAsc(
            int requiredStatusBitMask, long pendingDeletionWarning, Pageable page);

    //the left join is important because the homeArea can be unset and spring by default uses cross join when
    //querying "where ... or p.homeArea.tenant.id in :tenantIds"
    @Query("select distinct p from Person p left join p.homeArea ha " +
            "where (p.community.id in :tenantIds or ha.tenant.id in :tenantIds) " +
            "and p.deleted = false ")
    Page<Person> findByTenantOrHomeAreaTenantInAndDeletedFalse(Set<String> tenantIds, Pageable page);

    //fields with index are listed first (maybe there is some short circuit evaluation that leads to faster results...)
    @Query("select p from Person p where lower(p.id) like lower(:idLike) " +
            "    or lower(p.oauthId) like lower(:oauthIdLike) " +
            "    or lower(p.email) like lower(:emailLike) " +
            "    or lower(p.firstName) like lower(:firstNameLike) " +
            "    or lower(p.lastName) like lower(:lastNameLike)")
    Page<Person> findByIdLikeOrFirstNameLikeOrLastNameLikeOrOauthIdLikeOrEmailLikeCaseInsensitive(String idLike,
            String firstNameLike, String lastNameLike, String oauthIdLike, String emailLike, Pageable page);

    //combining findByHomeCommunityOrHomeAreaTenantIn and findByIdLikeOrFirstNameLikeOrLastNameLikeOrOauthIdLikeOrEmailLikeCaseInsensitive
    @Query("select distinct p from Person p left join p.homeArea ha " +
            "where (p.community.id in :tenantIds or ha.tenant.id in :tenantIds) " +
            "and (lower(p.id) like lower(:idLike) " +
            "    or lower(p.oauthId) like lower(:oauthIdLike) " +
            "    or lower(p.email) like lower(:emailLike) " +
            "    or lower(p.firstName) like lower(:firstNameLike) " +
            "    or lower(p.lastName) like lower(:lastNameLike))")
    Page<Person> findByHomeCommunityOrHomeAreaTenantInAndIdLikeOrFirstNameLikeOrLastNameLikeOrOauthIdLikeOrEmailLikeCaseInsensitive(
            Set<String> tenantIds, String idLike, String firstNameLike, String lastNameLike, String oauthIdLike,
            String emailLike, Pageable page);

    //fields with index are listed first (maybe there is some short circuit evaluation that leads to faster results...)
    @Query("select p from Person p " +
            "where (lower(p.firstName) like lower(:firstNameLike) " +
            "       and lower(p.lastName) like lower(:lastNameLike)) " +
            "    or lower(p.firstName) like lower(:concatenatedNameLike) " +
            "    or lower(p.lastName) like lower(:concatenatedNameLike)")
    Page<Person> findByFirstNameLikeAndLastNameLikeCaseInsensitive(String firstNameLike, String lastNameLike,
            String concatenatedNameLike, Pageable page);

    //combining findByHomeCommunityOrHomeAreaTenantIn and findByFirstNameLikeAndLastNameLikeCaseInsensitive
    @Query("select distinct p from Person p left join p.homeArea ha " +
            "where (p.community.id in :tenantIds or ha.tenant.id in :tenantIds) " +
            "    and ((lower(p.firstName) like lower(:firstNameLike) " +
            "       and lower(p.lastName) like lower(:lastNameLike)) " +
            "    or lower(p.firstName) like lower(:concatenatedNameLike) " +
            "    or lower(p.lastName) like lower(:concatenatedNameLike))")
    Page<Person> findByHomeCommunityOrHomeAreaTenantInAndFirstNameLikeAndLastNameLikeCaseInsensitive(
            Set<String> tenantIds, String firstNameLike, String lastNameLike, String concatenatedNameLike,
            Pageable page);

    @Transactional
    @Modifying
    @Query("update Person set oauthId=:newOauthId where oauthId=:oldOauthId")
    void updateOauthId(String oldOauthId, String newOauthId);

}
