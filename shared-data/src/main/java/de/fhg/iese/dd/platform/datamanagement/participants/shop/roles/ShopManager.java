/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.participants.shop.roles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.repos.TenantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@Component
@SuppressFBWarnings(value = "SE_BAD_FIELD",
        justification = "If this role gets serialized it needs new repositories anyway.")
public class ShopManager extends BaseRole<Tenant> {

    @Autowired
    private TenantRepository tenantRepository;

    private ShopManager() {
        super(Tenant.class);
    }

    @Override
    public String getDisplayName() {
        return "Shop-Manager";
    }

    @Override
    public String getDescription() {
        return "Kann über die Shopping-Komponente einen neuen Shop für einen zugewiesenen Tenant erstellen.";
    }

    @Override
    public Tenant getRelatedEntityById(String relatedEntityId) {
        return tenantRepository.findById(relatedEntityId).orElse(null);
    }

    @Override
    public boolean existsRelatedEntity(String relatedEntityId) {
        return tenantRepository.existsById(relatedEntityId);
    }

    @Override
    public Tenant getTenantOfRelatedEntity(Tenant relatedEntity) {
        return relatedEntity;
    }

}
