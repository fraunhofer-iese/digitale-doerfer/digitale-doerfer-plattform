/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Stefan Schweitzer, Johannes Schneider, Jannis von Albedyll, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.participants.personblocking.repos;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.model.PersonBlocking;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;

@SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
public interface PersonBlockingRepository extends JpaRepository<PersonBlocking, String> {

    boolean existsByAppAndBlockingPersonAndBlockedPerson(App app, Person blockingPerson, Person blockedPerson);

    Optional<PersonBlocking> findByAppAndBlockingPersonAndBlockedPerson(App app, Person blockingPerson, Person blockedPerson);

    @Query("select pb.blockedPerson " +
            "from PersonBlocking as pb " +
            "where pb.app = :app " +
            "and pb.blockingPerson = :blockingPerson " +
            "order by pb.blockedPerson.firstName, pb.blockedPerson.lastName")
    Collection<Person> findBlockedPersonsByAppAndBlockingPerson(App app, Person blockingPerson);

    @Query("select pb.blockedPerson " +
            "from PersonBlocking pb " +
            "where pb.app = :app " +
            "and pb.blockingPerson = :blockingPerson " +
            "and pb.blockedPerson.deleted = false " +
            "order by pb.blockedPerson.firstName, pb.blockedPerson.lastName")
    Collection<Person> findBlockedPersonByAppAndBlockingPersonAndBlockedPerson_DeletedFalse(App app,
            Person blockingPerson);

    int countByAppAndBlockingPersonAndBlockedPerson_DeletedFalse(App app, Person blockingPerson);

    @Query("select distinct pb.app " +
            "from PersonBlocking as pb " +
            "where pb.blockingPerson = :blockingPerson " +
            "order by pb.app.appIdentifier")
    Collection<App> findAppsByBlockingPerson(@Param("blockingPerson") Person blockingPerson);

    @Query("select pb.blockedPerson.id " +
            "from PersonBlocking pb " +
            "where pb.app = :app " +
            "and pb.blockingPerson = :blockingPerson " +
            "and pb.blockedPerson.deleted = false")
    Set<String> findBlockedPersonIdsByAppAndBlockingPersonAndBlockedPerson_DeletedFalse(App app, Person blockingPerson);

    List<PersonBlocking> findAllByBlockingPersonOrderByCreatedDesc(Person blockingPerson);
}
