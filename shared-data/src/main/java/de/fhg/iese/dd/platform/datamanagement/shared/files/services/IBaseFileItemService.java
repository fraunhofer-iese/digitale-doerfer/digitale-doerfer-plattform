/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemCannotBeDeletedException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.MediaItemUsageNotAllowedException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.TemporaryFileItemDuplicateUsageException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.TemporaryFileItemNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.BaseFileItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryBaseFileItem;

/**
 * Abstract service interfaces for the abstract {@link BaseFileItem} that abstracts from {@link MediaItem} and
 * {@link DocumentItem}.
 *
 * @param <F> the concrete subtype of {@link BaseFileItem} that is wrapped in the temporary item
 * @param <T> the concrete subtype of {@link TemporaryBaseFileItem} that gets managed
 */
public interface IBaseFileItemService<F extends BaseFileItem, T extends TemporaryBaseFileItem<F>> {

    F findItemById(String id);

    /**
     * Finds a temporary item that is owned by the user by id. Both id and owner have to match, as a mechanism to
     * prevent using temporary items of other users.
     * <p>
     * To use that temporary item call {@link #useItem(TemporaryBaseFileItem, FileOwnership)} , which makes the
     * associated item ready to be used.
     * </p>
     *
     * @param owner owner of the temporary item
     * @param id    id of the temporary item, not the id of the associated item
     *
     * @return the temporary item of that user with the given id
     *
     * @throws TemporaryFileItemNotFoundException if the given person has no temporary media item with the given id
     */
    T findTemporaryItemById(Person owner, String id);

    /**
     * Finds all temporary items that are owned by the user by id. Both id and owner have to match, as a mechanism to
     * prevent using temporary items of other users.
     * <p>
     * To use these temporary items call {@link #useAllItems(List, FileOwnership)}, which makes the associated items
     * ready to be used.
     * </p>
     *
     * @param owner owner of the temporary items
     * @param ids   ids of the temporary items, not the ids of the associated items
     *
     * @return list of temporary items of that user
     *
     * @throws TemporaryFileItemNotFoundException       if at least one of the ids could not be found
     * @throws TemporaryFileItemDuplicateUsageException if the same temporary media item occurs twice in the list
     */
    List<T> findTemporaryItemsById(Person owner, List<String> ids) throws TemporaryFileItemNotFoundException;

    Set<T> findTemporaryItemsByIdUnordered(Person owner, List<String> ids)
            throws TemporaryFileItemNotFoundException, TemporaryFileItemDuplicateUsageException;

    /**
     * Finds all temporary items a user owns.
     *
     * @param owner owner of the temporary items
     *
     * @return list of all temporary items the user owns
     */
    List<T> findAllTemporaryItemsByOwner(Person owner);

    /**
     * Finds all items that are owned by the app variant. If `userOwned` is true, also media items having an additional
     * person as owner are returned.
     *
     * @param appVariant owner of the media items
     * @param userOwned  find items that are owned by the user and the app variant
     * @param pageable   the page request
     *
     * @return paged list of all items owned by that app variant
     */
    Page<F> findAllItemsOwnedByAppVariant(AppVariant appVariant, boolean userOwned,
            Pageable pageable);

    /**
     * Finds all items that are owned by the person
     *
     * @param owner owner of the items
     *
     * @return list of all items the person owns
     */
    List<F> findAllItemsOwnedByPerson(Person owner);

    /**
     * Finds all items that are owned by the app variant by id. Both id and app variant have to match, as a mechanism to
     * prevent using items of other app variants.
     *
     * @param appVariant owner of the items
     * @param ids        ids of the items
     *
     * @return list of items of that app variant
     *
     * @throws FileItemNotFoundException         if at least one of the ids could not be found
     * @throws MediaItemUsageNotAllowedException if at least one of the ids belong to a media item the app variant is
     *                                           not owner
     */
    List<F> findItemsByIdOwnedByAppVariant(AppVariant appVariant, List<String> ids)
            throws FileItemNotFoundException, MediaItemUsageNotAllowedException;

    F cloneItem(F item, FileOwnership newFileOwnership);

    void deleteReferencedFiles(F item);

    /**
     * Deletes the item from the repository. This deletion automatically triggers a post delete listener that deletes
     * all associated files, so no other action needs to be done.
     *
     * @param item the item to be deleted
     *
     * @see MediaItemPostDeleteListener
     * @see DocumentItemPostDeleteListener
     */
    void deleteItem(F item);

    void deleteAllItems(Iterable<F> items) throws FileItemCannotBeDeletedException;

    void deleteAllItemsByOwner(Person owner);

    void deleteAllTemporaryItemsByOwner(Person owner);

    /**
     * Deletes a temporary item that is owned by the user and has the given id. Both id and owner have to match, as a
     * mechanism to prevent using temporary items of other users.
     * <p>
     * The referenced item and the files are automatically deleted by a post delete listener, so no other action needs
     * to be done.
     *
     * @param owner owner of the temporary item
     * @param id    id of the temporary item, not the id of the associated item
     *
     * @throws TemporaryFileItemNotFoundException if the id does not match with a temporary media item of that user
     */
    void deleteTemporaryItemById(Person owner, String id);

    /**
     * Deletes all temporary items that have {@link TemporaryBaseFileItem#getExpirationTime()} > now.
     */
    void deleteExpiredTemporaryItems();

    /**
     * "Frees" the item from the temporary item so that it can be used.
     * <p>
     * The temporary item is deleted.<br> Take care to use the item afterwards, so this method should only be called
     * after all other preconditions have been checked before. Otherwise the item is orphaned.
     *
     * @param temporaryItem temporary item to be used
     * @param fileOwnership the owner of a item. The owner can either be a person, an app or an app variant.
     *
     * @return the ready to use item
     */
    F useItem(T temporaryItem, FileOwnership fileOwnership);

    /**
     * Applies {@link #useItem(TemporaryBaseFileItem, FileOwnership)} to a list of items.
     *
     * @param temporaryItems temporary  items to be used
     * @param fileOwnership  the owner of an item. The owner can either be a person, an app or an app variant.
     *
     * @return the ready to use items
     *
     * @throws TemporaryFileItemDuplicateUsageException if the same temporary item occurs twice in the list
     */
    List<F> useAllItems(List<T> temporaryItems, FileOwnership fileOwnership)
            throws TemporaryFileItemDuplicateUsageException;

    /**
     * Applies {@link #useItem(TemporaryBaseFileItem, FileOwnership)} to a set of items.
     *
     * @param temporaryItems temporary  items to be used
     * @param fileOwnership  the owner of an item. The owner can either be a person, an app or an app variant.
     *
     * @return the ready to use items
     **/
    Set<F> useAllItemsUnordered(Set<T> temporaryItems, FileOwnership fileOwnership);

    /**
     * Creates a runnable for deleting orphan file items of an entity after it got saved.
     * <p>
     * This is to solve issues with ordered OneToMany relationships, as mentioned in <a
     * href="https://stackoverflow.com/questions/4022509/constraint-violation-in-hibernate-unidirectional-onetomany-mapping-with-jointabl">this
     * discussion</a>
     *
     * @param existingItems the existing file items of the entity
     * @param newItems      the new file items of the entity
     *
     * @return a runnable that should be run when the entity is saved and the orphan file are no longer referenced
     */
    Runnable getOrphanItemDeletion(@Nullable Collection<F> existingItems,
            @Nullable Collection<F> newItems);

}
