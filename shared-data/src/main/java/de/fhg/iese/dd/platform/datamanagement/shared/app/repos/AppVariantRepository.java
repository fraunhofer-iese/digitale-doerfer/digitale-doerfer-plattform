/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.app.repos;

import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Transactional(readOnly = true)
public interface AppVariantRepository extends JpaRepository<AppVariant, String> {

    @Query("select av from AppVariant av where av.id = :id")
    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD, attributePaths = {"oauthClients"})
    Optional<AppVariant> findByIdFull(@Param("id") String id);

    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD, attributePaths = {"oauthClients"})
    Optional<AppVariant> findByAppVariantIdentifier(String appVariantIdentifier);

    @Query("select av from AppVariant av where av.deleted = false ")
    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD, attributePaths = {"oauthClients"})
    Set<AppVariant> findAllFullNotDeleted();

    @Query("select av from AppVariant av")
    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD, attributePaths = {"oauthClients"})
    Set<AppVariant> findAllFull();
    
    List<AppVariant> findAllByOrderByAppVariantIdentifier();

    Page<AppVariant> findAllByNameOrAppVariantIdentifierLike(String search1, String search2, Pageable pageable);

    @Query("select av from AppVariant av " +
            "where not exists " +
            "   (select 1 from AppVariantVersion avv where avv.appVariant = av)" +
            "order by av.created desc")
    List<AppVariant> findAllWithoutAppVariantVersionOrderByCreatedDesc();

    @Query("select av from AppVariant av " +
            "where (av.apiKey1 = :apiKey or av.apiKey2 = :apiKey) and av <> :appVariant " +
            "order by av.created desc")
    List<AppVariant> findAllOtherAppVariantsByApiKeyOrderByCreatedDesc(String apiKey, AppVariant appVariant);

    @Query("select av from AppVariant av " +
            "where (av.apiKey1Created > :minCreated and av.apiKey1Created <= :maxCreated) " +
            "or    (av.apiKey2Created > :minCreated and av.apiKey2Created <= :maxCreated) " +
            "or    (av.externalApiKeyCreated > :minCreated and av.externalApiKeyCreated <= :maxCreated)")
    List<AppVariant> findAllByApiKeyCreatedBetween(long minCreated, long maxCreated);

    List<AppVariant> findAllByAppOrderByAppVariantIdentifier(App app);

}
