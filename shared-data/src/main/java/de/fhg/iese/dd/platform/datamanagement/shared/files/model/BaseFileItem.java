/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.model;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * Abstract parent for {@link MediaItem} and {@link DocumentItem}, since both have a lot in common.
 */
@Entity
@Table(name = "media_item")
//all subclasses are stored in a single table and different types are distinguished via the discriminator field
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//this tells hibernate to use the field file_type to store the discriminator
//we use an int here because there will only be two types, and they are easily distinguishable in the db
@DiscriminatorColumn(name = "file_type", discriminatorType = DiscriminatorType.INTEGER, columnDefinition = "TINYINT")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public abstract class BaseFileItem extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    protected Person owner;

    @ManyToOne(fetch = FetchType.LAZY)
    protected AppVariant appVariant;

    @ManyToOne(fetch = FetchType.LAZY)
    protected App app;

    public void setFileOwnership(FileOwnership fileOwnership) {
        owner = fileOwnership.getOwner() != null ? fileOwnership.getOwner() : owner;
        appVariant = fileOwnership.getAppVariant() != null ? fileOwnership.getAppVariant() : appVariant;
        app = fileOwnership.getApp() != null ? fileOwnership.getApp() : app;
    }

}
