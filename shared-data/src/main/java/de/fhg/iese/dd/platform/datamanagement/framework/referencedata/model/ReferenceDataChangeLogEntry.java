/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.referencedata.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(indexes = {
        @Index(columnList = "startTime", unique = true)
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ReferenceDataChangeLogEntry extends BaseEntity {

    /**
     * Start time of the change.
     */
    @Column(nullable = false)
    private long startTime;
    /**
     * End time of the change if it is finished. For an ongoing change this is null.
     */
    @Column
    private Long endTime;
    /**
     * True if the change was successful, only for documentation. For an ongoing change this is null.
     */
    @Column
    private Boolean successful;
    /**
     * Identifier of the instance where the change was started, only for documentation.
     */
    @Column(nullable = false)
    private String instanceIdentifier;
    /**
     * Id of the person who triggered the change, only for documentation.
     */
    @Column(length = 36, nullable = false)
    private String initiatorId;
    /**
     * Name of the person who triggered the change, only for documentation.
     */
    @Column(nullable = false)
    private String initiatorName;
    /**
     * Command that was executed to do the change, only for documentation.
     */
    @Column(nullable = false)
    private String command;
    /**
     * Description of the change, only for documentation.
     */
    @Column(length = BaseEntity.DESCRIPTION_LENGTH)
    private String versionInfo;

}
