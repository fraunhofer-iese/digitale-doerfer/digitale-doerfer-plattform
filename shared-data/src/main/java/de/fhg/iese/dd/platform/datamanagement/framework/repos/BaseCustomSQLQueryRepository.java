/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Tahmid Ekram
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.repos;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseCustomSQLQueryRepository {

    @Autowired
    private EntityManager entityManager;

    protected Query getQueryFromResource(String filename, String resultSetMapping) {

        final String query;
        final String fileLocation = "sql-queries/" + filename + ".sql";
        try (InputStream file = this.getClass().getClassLoader().getResourceAsStream(fileLocation)) {

            if (file == null) {
                throw new IllegalStateException("Could not find file '" + fileLocation + "'." +
                        " Make sure the file is present in the 'sql-queries' directory of the module containing " +
                        this.getClass());
            }
            query = IOUtils.toString(file, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to open file '" + fileLocation + "'." +
                    " Make sure the file is present in the 'sql-queries' directory of the module containing " +
                    this.getClass(), e);
        }
        return entityManager.createNativeQuery(query, resultSetMapping);
    }

}
