/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.model;

import java.util.Comparator;

import com.fasterxml.jackson.annotation.JsonValue;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@EqualsAndHashCode
@ToString
public class MediaItemSize implements Comparable<MediaItemSize> {

    private static final String ORIGINAL_SIZE_NAME = "original";
    private static final String ORIGINAL_SIZE_FILE_SUFFIX = null;
    private static final int ORIGINAL_SIZE_MAX_SIZE = Integer.MAX_VALUE;
    private static final Comparator<MediaItemSize> NATURAL_ORDER_COMPARATOR =
            Comparator.comparingInt(MediaItemSize::getMaxSize);

    public static final MediaItemSize ORIGINAL_SIZE = MediaItemSize.builder()
            .name(MediaItemSize.ORIGINAL_SIZE_NAME)
            .fileSuffix(MediaItemSize.ORIGINAL_SIZE_FILE_SUFFIX)
            .squared(false)
            .maxSize(MediaItemSize.ORIGINAL_SIZE_MAX_SIZE)
            .build();

    private final String name;
    private final String fileSuffix;
    private final boolean squared;
    private final int maxSize;

    @JsonValue
    public String getName() {
        return name;
    }

    @Override
    public int compareTo(MediaItemSize other) {
        return NATURAL_ORDER_COMPARATOR.compare(this, other);
    }

    public boolean isImageBigEnough(int imageWidth, int imageHeight){
        int longestSideLength = Math.max(imageWidth, imageHeight);
        if(longestSideLength > getMaxSize()){
            return true;
        }else{
            return MediaItemSize.ORIGINAL_SIZE.equals(this);
        }
    }

}
