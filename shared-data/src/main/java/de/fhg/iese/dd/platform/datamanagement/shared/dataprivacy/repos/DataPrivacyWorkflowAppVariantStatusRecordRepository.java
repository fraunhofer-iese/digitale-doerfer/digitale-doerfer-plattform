/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflow;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariantStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowAppVariantStatus;

public interface DataPrivacyWorkflowAppVariantStatusRecordRepository extends JpaRepository<DataPrivacyWorkflowAppVariantStatusRecord, String> {

    Optional<DataPrivacyWorkflowAppVariantStatusRecord> findFirstByDataPrivacyWorkflowAppVariantOrderByCreatedDesc(
            DataPrivacyWorkflowAppVariant workflowAppVariant);

    List<DataPrivacyWorkflowAppVariantStatusRecord> findAllByDataPrivacyWorkflowAppVariantOrderByCreatedDesc(
            DataPrivacyWorkflowAppVariant workflowAppVariant);

    @Query("select count(d.id) " +
            "from DataPrivacyWorkflowAppVariantStatusRecord d " +
            "where d.dataPrivacyWorkflowAppVariant = :workflowAppVariant " +
            "and d.status in :status")
    int countStatusRecordsHavingStatus(DataPrivacyWorkflowAppVariant workflowAppVariant,
            Set<DataPrivacyWorkflowAppVariantStatus> status);

    @Transactional
    @Modifying
    int deleteAllByDataPrivacyWorkflowAppVariant_DataPrivacyWorkflow(
            DataPrivacyWorkflow dataPrivacyWorkflow);

}
