/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Stefan Schweitzer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.app.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(
                columnNames = { "app_variant_usage_id", "selected_area_id" })
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class AppVariantUsageAreaSelection extends BaseEntity {

    @ManyToOne(optional = false)
    private AppVariantUsage appVariantUsage;

    @ManyToOne(optional = false)
    private GeoArea selectedArea;

    public AppVariantUsageAreaSelection withConstantId() {
        super.generateConstantId(appVariantUsage, selectedArea);
        return this;
    }

}
