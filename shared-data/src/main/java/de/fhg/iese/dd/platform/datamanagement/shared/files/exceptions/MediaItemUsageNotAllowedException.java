/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions;

import java.util.List;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.AuthorizationException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

public class MediaItemUsageNotAllowedException extends AuthorizationException {

    private static final long serialVersionUID = 3514950229414144655L;

    public MediaItemUsageNotAllowedException(String message) {
        super(message);
    }

    public MediaItemUsageNotAllowedException(String message, Object... arguments) {
        super(message, arguments);
    }

    public MediaItemUsageNotAllowedException(AppVariant appVariant, List<String> ids) {
        super("App variant '{}' is not owner of media item with ids '{}'.", appVariant, ids);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.MEDIA_ITEM_USAGE_NOT_ALLOWED;
    }

}
