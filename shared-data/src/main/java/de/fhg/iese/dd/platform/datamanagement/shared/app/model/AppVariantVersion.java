/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.StringUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.app.exceptions.VersionParseException;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.enums.AppPlatform;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class AppVariantVersion extends BaseEntity {

    @ManyToOne
    private AppVariant appVariant;

    @Column
    private String versionIdentifier;

    @Enumerated(EnumType.STRING)
    @Column
    private AppPlatform platform;

    @Column
    private boolean supported;

    /**
     * Checks if the major and minor version are the same, according to the
     * notation {@code major.minor.bugfix}.
     *
     * @param versionIdentifierToTest
     * @return {@code true} if major and minor can be extracted and match,
     *         {@code false} if the versionIdToTest do not match or could not be
     *         parsed.
     * @throws VersionParseException
     *             if the versionId of this AppVariantVersion is invalid. Never thrown
     *             if the versionIdToTest is invalid.
     */
    public boolean matchesMajorMinor(String versionIdentifierToTest) throws VersionParseException {
        if(StringUtils.isEmpty(versionIdentifierToTest)){
            return false;
        }
        String[] majorMinor = StringUtils.split(versionIdentifier, '.');
        if(majorMinor == null || majorMinor.length < 2){
            throw new VersionParseException("The versionIdentifier of this AppVariantVersion could not be parsed, adjust the data initialization: {}", this.toString());
        }
        String major = majorMinor[0];
        String minor = majorMinor[1];

        String[] majorMinorToTest = StringUtils.split(versionIdentifierToTest, '.');
        if(majorMinorToTest == null || majorMinorToTest.length < 2){
            return false;
        }

        String majorToTest = majorMinorToTest[0];
        String minorToTest = majorMinorToTest[1];

        return(StringUtils.equals(major, majorToTest) && StringUtils.equals(minor, minorToTest));
    }

    /**
     * Sets the ID to a constant ID, based on appVariant, versionIdentifier and platform
     *
     * @return
     */
    public AppVariantVersion withConstantId() {
        super.generateConstantId(appVariant.getId(), versionIdentifier, platform.name());
        return this;
    }

    @Override
    public String toString() {
        return "AppVariantVersion ["
                + (id != null ? "id=" + id + ", " : "")
                + (appVariant != null ? "appVariant=" + appVariant + ", " : "")
                + (versionIdentifier != null ? "versionIdentifier=" + versionIdentifier + ", " : "")
                + (platform != null ? "platform=" + platform + ", " : "")
                + "supported=" + supported + "]";
    }

}
