/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Jannis von Albedyll, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.security.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthRegistration;

@Transactional(readOnly = true)
public interface OauthRegistrationRepository extends JpaRepository<OauthRegistration, String> {

    boolean existsByOauthId(String oauthId);

    OauthRegistration findByOauthId(String oauthId);

    @Query("select oa " +
            "from OauthRegistration oa " +
            "where oa.errorMessage is null " +
            "and oa.created < :before " +
            "and not exists (select 1 from Person p where p.oauthId = oa.oauthId )" +
            "order by oa.created asc")
    List<OauthRegistration> findAllUnusedAndNoError(long before);

    @Transactional(readOnly = false)
    void deleteByOauthId(String oauthId);

}
