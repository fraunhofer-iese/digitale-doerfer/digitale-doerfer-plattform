/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Stefan Schweitzer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.app.repos;

import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsageAreaSelection;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.results.PersonCountByAppVariantUsageAreaSelection;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.results.PersonCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface AppVariantUsageAreaSelectionRepository extends JpaRepository<AppVariantUsageAreaSelection, String> {

    @Query("select distinct avuas.selectedArea.id " +
            "from AppVariantUsageAreaSelection avuas " +
            "where avuas.appVariantUsage = :appVariantUsage ")
    Set<String> findAllSelectedGeoAreaIdsByAppVariantUsage(AppVariantUsage appVariantUsage);

    @Query("select distinct avuas.selectedArea.id " +
            "from AppVariantUsageAreaSelection avuas " +
            "left join AppVariantUsage av " +
            "on avuas.appVariantUsage = av " +
            "where av.appVariant = :appVariant " +
            "and av.person = :person")
    Set<String> findAllSelectedGeoAreaIdsByAppVariant(AppVariant appVariant, Person person);

    @Query("select distinct avuas.selectedArea.tenant " +
            "from AppVariantUsageAreaSelection avuas " +
            "where avuas.appVariantUsage = :appVariantUsage ")
    Set<Tenant> findAllTenantsOfSelectedGeoAreasByAppVariantUsage(AppVariantUsage appVariantUsage);

    @Transactional
    @Modifying
    @Query("delete from AppVariantUsageAreaSelection gA where gA.appVariantUsage = :appVariantUsage and gA.selectedArea not in :selectedAreas")
    void deleteByAppVariantUsageAndSelectedAreaIsNotIn(AppVariantUsage appVariantUsage, Set<GeoArea> selectedAreas);

    long countByAppVariantUsage_AppVariantAndSelectedArea(AppVariant appVariant, GeoArea selectedArea);

    @Query("select avuasA " +
            "from AppVariantUsageAreaSelection avuasA " +
            "where avuasA.selectedArea = :geoAreaA " +
            "and not exists " +
            "   (select  1 " +
            "   from AppVariantUsageAreaSelection avuasB " +
            "   where avuasB.appVariantUsage = avuasA.appVariantUsage " +
            "   and avuasB.selectedArea = :geoAreaB)")
    Set<AppVariantUsageAreaSelection> findSelectionsOfGeoAreaAAndNotGeoAreaB(GeoArea geoAreaA, GeoArea geoAreaB);

    @Transactional
    @Modifying
    void deleteByAppVariantUsage(AppVariantUsage appVariantUsage);

    @Query("select distinct p.id " +
            "from   Person p, " +
            "       PushCategory pc " +
            "where  exists (select avuas.id " +
            "               from   AppVariantUsageAreaSelection avuas " +
            "               where  avuas.selectedArea in :geoAreas " +
            "                  and avuas.appVariantUsage.person = p " +
            "                  and avuas.appVariantUsage.appVariant = :appVariant " +
            "                  and avuas.appVariantUsage.lastUsage >= :latestLastUsage) " +
            "   and ( ( pc = :pushCategory " +
            "           and pc.defaultLoudPushEnabled = true " +
            "           and not exists (select pcus.id " +
            "                           from   PushCategoryUserSetting pcus " +
            "                           where  pcus.pushCategory = :pushCategory " +
            "                              and pcus.person = p " +
            "                              and pcus.loudPushEnabled = false " +
            "                              and pcus.appVariant = :appVariant) ) " +
            "          or ( pc = :pushCategory " +
            "               and pc.defaultLoudPushEnabled = false " +
            "               and exists (select pcus.id " +
            "                           from   PushCategoryUserSetting pcus " +
            "                           where  pcus.pushCategory = :pushCategory " +
            "                              and pcus.person = p " +
            "                              and pcus.loudPushEnabled = true " +
            "                              and pcus.appVariant = :appVariant) ) ) " +
            "   and not exists (select pb.id " +
            "                   from   PersonBlocking pb " +
            "                   where  pb.blockingPerson = p " +
            "                      and pb.blockedPerson = :author) " +
            "   and ( :author is null or p <> :author ) ")
    Collection<String> getLoudPersonIdsForLoudPushToGeoArea(AppVariant appVariant, long latestLastUsage,
                                                            Set<GeoArea> geoAreas,
            PushCategory pushCategory, Person author);

    @Query("select distinct p.id " +
            "from   Person p, " +
            "       PushCategory pc " +
            "where  exists (select avuas.id " +
            "               from   AppVariantUsageAreaSelection avuas " +
            "               where  avuas.selectedArea in :geoAreas " +
            "                  and avuas.appVariantUsage.person = p " +
            "                  and avuas.appVariantUsage.appVariant = :appVariant " +
            "                  and avuas.appVariantUsage.lastUsage >= :latestLastUsage) " +
            "   and ( ( ( pc = :pushCategory " +
            "             and pc.defaultLoudPushEnabled = false " +
            "             and not exists (select pcus.id " +
            "                             from   PushCategoryUserSetting pcus " +
            "                             where  pcus.pushCategory = :pushCategory " +
            "                                and pcus.person = p " +
            "                                and pcus.loudPushEnabled = true " +
            "                                and pcus.appVariant = :appVariant) ) " +
            "            or ( pc = :pushCategory " +
            "                 and pc.defaultLoudPushEnabled = true " +
            "                 and exists (select pcus.id " +
            "                             from   PushCategoryUserSetting pcus " +
            "                             where  pcus.pushCategory = :pushCategory " +
            "                                and pcus.person = p " +
            "                                and pcus.loudPushEnabled = false " +
            "                                and pcus.appVariant = :appVariant) ) ) " +
            "          or exists (select pb.id " +
            "                     from   PersonBlocking pb " +
            "                     where  pb.blockingPerson = p " +
            "                        and pb.blockedPerson = :author) " +
            "          or p = :author ) ")
    Collection<String> getSilentPersonIdsForLoudPushToGeoArea(AppVariant appVariant, long latestLastUsage,
                                                              Set<GeoArea> geoAreas,
            PushCategory pushCategory, Person author);

    @Query("select distinct p.id " +
            "from   Person p, " +
            "       PushCategory pc " +
            "where  p.homeArea in :geoAreas " +
            "   and exists (select avuas.id " +
            "               from   AppVariantUsageAreaSelection avuas " +
            "               where  avuas.selectedArea in :geoAreas " +
            "                  and avuas.appVariantUsage.person = p " +
            "                  and avuas.appVariantUsage.appVariant = :appVariant " +
            "                  and avuas.appVariantUsage.lastUsage >= :latestLastUsage) " +
            "   and ( ( pc = :pushCategory " +
            "           and pc.defaultLoudPushEnabled = true " +
            "           and not exists (select pcus.id " +
            "                           from   PushCategoryUserSetting pcus " +
            "                           where  pcus.pushCategory = :pushCategory " +
            "                              and pcus.person = p " +
            "                              and pcus.loudPushEnabled = false " +
            "                              and pcus.appVariant = :appVariant) ) " +
            "          or ( pc = :pushCategory " +
            "               and pc.defaultLoudPushEnabled = false " +
            "               and exists (select pcus.id " +
            "                           from   PushCategoryUserSetting pcus " +
            "                           where  pcus.pushCategory = :pushCategory " +
            "                              and pcus.person = p " +
            "                              and pcus.loudPushEnabled = true " +
            "                              and pcus.appVariant = :appVariant) ) ) " +
            "   and not exists (select pb.id " +
            "                   from   PersonBlocking pb " +
            "                   where  pb.blockingPerson = p " +
            "                      and pb.blockedPerson = :author) " +
            "   and ( :author is null or p <> :author ) ")
    Collection<String> getLoudPersonIdsForLoudPushToSameHomeArea(AppVariant appVariant,
                                                                 long latestLastUsage, Set<GeoArea> geoAreas, PushCategory pushCategory, Person author);

    @Query("select distinct p.id " +
            "from   Person p, " +
            "       PushCategory pc " +
            "where  p.homeArea in :geoAreas " +
            "   and exists (select avuas.id " +
            "               from   AppVariantUsageAreaSelection avuas " +
            "               where  avuas.selectedArea in :geoAreas " +
            "                  and avuas.appVariantUsage.person = p " +
            "                  and avuas.appVariantUsage.appVariant = :appVariant " +
            "                  and avuas.appVariantUsage.lastUsage >= :latestLastUsage) " +
            "   and ( ( ( pc = :pushCategory " +
            "             and pc.defaultLoudPushEnabled = false " +
            "             and not exists (select pcus.id " +
            "                             from   PushCategoryUserSetting pcus " +
            "                             where  pcus.pushCategory = :pushCategory " +
            "                                and pcus.person = p " +
            "                                and pcus.loudPushEnabled = true " +
            "                                and pcus.appVariant = :appVariant) ) " +
            "            or ( pc = :pushCategory " +
            "                 and pc.defaultLoudPushEnabled = true " +
            "                 and exists (select pcus.id " +
            "                             from   PushCategoryUserSetting pcus " +
            "                             where  pcus.pushCategory = :pushCategory " +
            "                                and pcus.person = p " +
            "                                and pcus.loudPushEnabled = false " +
            "                                and pcus.appVariant = :appVariant) ) ) " +
            "          or exists (select pb.id " +
            "                     from   PersonBlocking pb " +
            "                     where  pb.blockingPerson = p " +
            "                        and pb.blockedPerson = :author) " +
            "          or p = :author ) ")
    Collection<String> getSilentPersonIdsForLoudPushToSameHomeArea(AppVariant appVariant,
                                                                   long latestLastUsage, Set<GeoArea> geoAreas, PushCategory pushCategory, Person author);

    @Query("select distinct avuas.appVariantUsage.person.id " +
            "from AppVariantUsageAreaSelection avuas " +
            "where avuas.selectedArea in :geoAreas " +
            "and avuas.appVariantUsage.appVariant = :appVariant " +
            "and avuas.appVariantUsage.person.homeArea in :geoAreas ")
    Collection<String> getSilentPersonIdsForSilentPushToSameHomeArea(AppVariant appVariant, Set<GeoArea> geoAreas);

    @Query("select distinct avuas.appVariantUsage.person.id " +
            "from AppVariantUsageAreaSelection avuas " +
            "where avuas.selectedArea in :geoAreas " +
            "and avuas.appVariantUsage.appVariant = :appVariant ")
    Collection<String> getSilentPersonIdsForSilentPushToGeoArea(AppVariant appVariant, Set<GeoArea> geoAreas);

    @Query("select new de.fhg.iese.dd.platform.datamanagement.shared.app.repos.results.PersonCountByAppVariantUsageAreaSelection(" +
            "avuas.selectedArea, avuas.appVariantUsage.appVariant, count(avuas)) " +
            "from AppVariantUsageAreaSelection avuas " +
            "group by avuas.selectedArea, avuas.appVariantUsage.appVariant ")
    List<PersonCountByAppVariantUsageAreaSelection> countByAppVariantUsageAreaSelection();

    @Query("select new de.fhg.iese.dd.platform.datamanagement.shared.app.repos.results.PersonCountByGeoAreaId( " +
            "avuas.selectedArea.id, count(distinct avuas.appVariantUsage.person)) " +
            "from AppVariantUsageAreaSelection avuas " +
            "where avuas.appVariantUsage.appVariant.app = :app " +
            "group by avuas.selectedArea.id ")
    List<PersonCountByGeoAreaId> countByAppUsageAreaSelection(App app);

    @Query("select parentSelection " +
            "from AppVariantUsageAreaSelection parentSelection " +
            "where " +
            "exists (" +
            //there are child geo areas under the selected area
            "          select g " +
            "          from GeoArea g " +
            "          where g.parentArea = parentSelection.selectedArea " +
            ") " +
            "and " +
            "not exists ( " +
            //and none of the child areas was selected
            "          select childSelection " +
            "          from AppVariantUsageAreaSelection childSelection " +
            "          where childSelection.appVariantUsage = parentSelection.appVariantUsage " +
            "          and childSelection.selectedArea.parentArea = parentSelection.selectedArea " +
            ") ")
    Collection<AppVariantUsageAreaSelection> findAllInvalidSelectionsWithoutChildren();

    // we need the distinct to avoid having the same usage multiple times
    @Query("select distinct childSelection.appVariantUsage " +
            "from AppVariantUsageAreaSelection childSelection " +
            // we want to get all selections where a parent geo area exists
            "where childSelection.selectedArea.parentArea is not null " +
            "and not exists ( " +
            //and none of the parent areas was selected
            "          select 1 " +
            "          from AppVariantUsageAreaSelection parentSelection " +
            "          where parentSelection.appVariantUsage = childSelection.appVariantUsage " +
            "          and parentSelection.selectedArea = childSelection.selectedArea.parentArea " +
            ") ")
    Page<AppVariantUsage> findAllInvalidUsagesWithoutParentSelections(Pageable request);

    @Query("select sel.selectedArea " +
            "from AppVariantUsageAreaSelection sel " +
            "join sel.appVariantUsage avu " +
            "where avu.appVariant = :appVariant " +
            "and avu.person = :person " +
            "order by sel.selectedArea.created desc")
    List<GeoArea> findAllSelectedGeoAreas(AppVariant appVariant, Person person);

    @Transactional
    @Modifying
    long deleteAllByAppVariantUsageAppVariant(AppVariant appVariant);

    @Transactional
    @Modifying
    int deleteBySelectedArea(GeoArea geoArea);

}
