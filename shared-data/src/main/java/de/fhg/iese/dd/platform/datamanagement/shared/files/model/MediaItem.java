/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Steffen Hupp, Balthasar Weitzel, Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.model;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonReader;
import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonWriter;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.Map;
import java.util.SortedMap;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;

import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.MediaItemPostDeleteListener;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * MediaItems represent images.
 * <p/>
 * There is no need to delete the files associated with the MediaItem manually
 * before deleting it. They will be automatically deleted when this entity (or a
 * container of it with <code>cascade = CascadeType.ALL</code>) is removed. The
 * {@link MediaItemPostDeleteListener} is used for this purpose.
 */
@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@DiscriminatorValue("0")
public class MediaItem extends BaseFileItem {

    private static final ObjectWriter ORDERED_MAP_ENTRY_JSON_WRITER = defaultJsonWriter()
            .with(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);

    private static final TypeReference<SortedMap<String, String>> JSON_TYPEREF_NAME_TO_URL =
            new TypeReference<>() {
            };

    private static final ObjectReader STRING_MAP_READER = defaultJsonReader().forType(JSON_TYPEREF_NAME_TO_URL);

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String urlsJson;

    @Column
    private int sizeVersion;

    @Nullable
    @Embedded
    private RelativeImageCropDefinition relativeImageCropDefinition;

    @JsonIgnore //json annotations are only required for data init json, not for the API
    public Map<String, String> getUrls() {
        try {
            if (StringUtils.isNotEmpty(urlsJson)) {
                return STRING_MAP_READER.readValue(urlsJson);
            } else {
                return Collections.emptySortedMap();
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @JsonIgnore //json annotations are only required for data init json, not for the API
    public void setUrls(SortedMap<MediaItemSize, String> urls) {
        if (!CollectionUtils.isEmpty(urls)) {
            try {
                urlsJson = ORDERED_MAP_ENTRY_JSON_WRITER.writeValueAsString(urls);
            } catch (JsonProcessingException e) {
                throw new UncheckedIOException(e);
            }
        }
    }

    @Override
    public String toString() {
        return "MediaItem ["
                + (id != null ? "id=" + id + ", " : "")
                + (urlsJson != null ? "urlsJson=" + urlsJson : "")
                + "]";
    }

}
