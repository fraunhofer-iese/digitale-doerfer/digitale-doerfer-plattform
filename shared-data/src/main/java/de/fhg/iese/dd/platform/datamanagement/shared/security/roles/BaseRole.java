/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.security.roles;

import java.util.Objects;

import com.google.common.base.CaseFormat;

import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;

/**
 * All roles must extends this class
 *
 * @param <E> related entity for role scope, e.g. Tenant for VG admins. If there is no such entity (i.e., {@link
 *            RoleAssignment#getRelatedEntityId() } is always {@code null}), use {@link RelatedEntityIsNull}.
 */
public abstract class BaseRole<E extends NamedEntity> {

    private final Class<E> relatedEntityClass;

    protected BaseRole(Class<E> relatedEntityClass) {
        Objects.requireNonNull(relatedEntityClass);
        this.relatedEntityClass = relatedEntityClass;
    }

    /**
     * the class of the related entity
     *
     * @return never {@code null}
     */
    public Class<E> getRelatedEntityClass() {
        return relatedEntityClass;
    }

    public boolean hasNullEntity() {
        return relatedEntityClass.equals(RelatedEntityIsNull.class);
    }

    public String getKey() {
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, getClass().getSimpleName());
    }

    public abstract String getDisplayName();

    public abstract String getDescription();

    public abstract E getRelatedEntityById(String relatedEntityId);

    public abstract boolean existsRelatedEntity(String relatedEntityId);

    public abstract Tenant getTenantOfRelatedEntity(E relatedEntity);

    /**
     * @return class of this role that can be used in generified methods
     */
    public Class<? extends BaseRole<E>> getRoleClass() {
        @SuppressWarnings("unchecked")
        Class<? extends BaseRole<E>> aClass = (Class<? extends BaseRole<E>>) this.getClass();
        return aClass;
    }

}
