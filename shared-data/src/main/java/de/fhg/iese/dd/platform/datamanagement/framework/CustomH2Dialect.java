/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework;

import org.hibernate.dialect.H2Dialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.IntegerType;

/**
 * Custom dialect based on the {@link H2Dialect} that adds bitwise operations. These operations are offered by all SQL
 * databases, but not part of JPQL / HQL.
 * <p>
 * It is referenced in the application profile as spring.jpa.database-platform
 */
@SuppressWarnings("unused")
public class CustomH2Dialect extends H2Dialect {

    public CustomH2Dialect() {
        super();
        // adds the bitwise and as function "bitand"
        registerFunction("bitand",
                new SQLFunctionTemplate(IntegerType.INSTANCE, "BITAND(CAST(?1 AS INTEGER), CAST(?2 AS INTEGER))"));
    }

}
