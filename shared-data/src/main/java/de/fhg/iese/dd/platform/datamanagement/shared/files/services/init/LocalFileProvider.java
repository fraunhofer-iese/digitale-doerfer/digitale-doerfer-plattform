/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services.init;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import lombok.Getter;

class LocalFileProvider extends BaseFileProvider implements IFileProvider {

    private final ITimeService timeService;
    @Getter
    private final Path dataInitRootFolder;

    public LocalFileProvider(Path dataInitRootFolder, ITimeService timeService) {
        this.dataInitRootFolder = dataInitRootFolder;
        this.timeService = timeService;
    }

    @Override
    protected void ensureInitialized() {
    }

    @Override
    public String checkoutInitData(LogSummary logSummary) throws DataInitializationException {
        super.checkoutInitData(logSummary);
        logSummary.info("Using local data init files at {}", dataInitRootFolder);
        return getLocalVersionMessage();
    }

    private String getLocalVersionMessage() {
        try (Stream<Path> pathStream = Files.walk(dataInitRootFolder)) {
            final String lastModifiedFilesMessage = pathStream
                    .filter(Files::isRegularFile)
                    //we exclude all ".*" directories and files, since the are config files
                    .filter(path -> !path.toString().contains(File.separator + "."))
                    .sorted(Comparator.comparingLong(
                            (Path path) -> {
                                try {
                                    return Files.getLastModifiedTime(path).toMillis();
                                } catch (IOException e) {
                                    return 0L;
                                }
                            }).reversed())
                    .limit(3)
                    .map(path -> {
                        try {
                            return timeService.toLocalTimeHumanReadable(
                                    Files.getLastModifiedTime(path).toMillis()) +
                                    "  " +
                                    dataInitRootFolder.relativize(path);
                        } catch (IOException e) {
                            return "Failed to find out latest file change";
                        }
                    })
                    .collect(Collectors.joining("\n"));
            return "Last 3 modified files in the data init folder:\n" + lastModifiedFilesMessage;
        } catch (IOException e) {
            return "Failed to find out latest file change";
        }
    }

}
