/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import java.util.List;
import java.util.Set;

import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemDuplicateUsageException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.TemporaryFileItemDuplicateUsageException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItemPlaceHolder;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryBaseFileItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;

public interface IDocumentItemService extends IBaseFileItemService<DocumentItem, TemporaryDocumentItem> {

    void deleteReferencedFiles(DocumentItem documentItem);

    TemporaryDocumentItem createTemporaryDocumentItem(byte[] documentData, String fileName, String title,
            String description,
            FileOwnership fileOwnership);

    DocumentItem createDocumentItem(byte[] documentData, String fileName, String title, String description,
            FileOwnership fileOwnership);

    /**
     * Uses either the temporary document items or the existing document items. For temporary document items
     * {@link #useItem(TemporaryBaseFileItem, FileOwnership)} is called.
     *
     * @param documentItemPlaceHolders containing either an existing or a temporary document item
     * @param fileOwnership            the owner of a document item. The owner can either be a person, an app or an app
     *                                 variant.
     *
     * @return list of document items consisting of existing and used temporary document items
     *
     * @throws FileItemDuplicateUsageException          if the same document item is used at least twice
     * @throws TemporaryFileItemDuplicateUsageException if the same temporary document item is used at least twice
     */
    Set<DocumentItem> useTemporaryAndExistingDocumentItems(List<DocumentItemPlaceHolder> documentItemPlaceHolders,
            FileOwnership fileOwnership)
            throws FileItemDuplicateUsageException, TemporaryFileItemDuplicateUsageException;

}
