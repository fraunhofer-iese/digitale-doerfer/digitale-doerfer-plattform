/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.encryption.EncryptionConverter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(indexes = {
        @Index(columnList = "created"),
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class TeamNotificationConnection extends BaseEntity implements NamedEntity {

    /**
     * Name of the team chat server
     */
    @Column
    private String name;

    /**
     * Type of the team chat server
     */
    @Column
    private String teamNotificationConnectionType;

    /**
     * Configuration to access the team chat server, stored encrypted
     */
    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    @Convert(converter = EncryptionConverter.class)
    private String connectionConfig;

    @Override
    public String toString() {
        return "TeamNotificationConnection [" +
                "id= '" + id + "', " +
                "name= '" + name + "', " +
                "teamNotificationConnectionType= " + teamNotificationConnectionType +
                ']';
    }

}
