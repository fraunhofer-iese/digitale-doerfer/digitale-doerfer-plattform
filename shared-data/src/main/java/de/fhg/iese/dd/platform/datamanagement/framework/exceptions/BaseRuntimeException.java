/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.exceptions;

import org.apache.logging.log4j.message.ParameterizedMessage;

public class BaseRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 4727327597256606326L;

    private String detail;

    public BaseRuntimeException() {
        super();
    }

    public BaseRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseRuntimeException(String message) {
        super(message);
    }

    /**
     * Creates a new exception with a message that is formatted similar to log4j
     * logging. Sets the detail to the first argument.
     * <p/>
     * <code>("Component {} failed", "myComponent")</code> will result in
     * <code>"Component myComponent failed")</code>
     *
     * @param message the message to explain the cause of the exception, containing {} placeholders
     * @param arguments the o
     */
    public BaseRuntimeException(String message, Object... arguments) {
        super(new ParameterizedMessage(message, arguments).getFormattedMessage());
        if(arguments.length > 0){
            setDetail(String.valueOf(arguments[0]));
        }
    }

    public BaseRuntimeException(Throwable cause) {
        super(cause);
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public BaseRuntimeException withDetail(String detail){
        this.detail = detail;
        return this;
    }

    public ClientExceptionType getClientExceptionType(){
        return ClientExceptionType.UNSPECIFIED_ERROR;
    }

}
