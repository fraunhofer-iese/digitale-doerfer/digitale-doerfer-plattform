/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services.init;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.config.DataInitConfig;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Service
@Profile("local")
@Log4j2
class LocalOrGitDataInitializerFileService extends BaseDataInitializerFileService implements IDataInitializerFileService {

    private static final String SYSTEM_PROPERTY_DATA_INIT_FOLDER = "dd.datainit.local";
    @Getter
    private final IFileProvider fileProvider;

    @Autowired
    public LocalOrGitDataInitializerFileService(DataInitConfig dataInitConfig, ITimeService timeService) {

        if (StringUtils.isEmpty(getSystemPropertyDataInitFolder())) {
            log.info("No local data init files defined (use {} as environment variable), " +
                    "switching to git data init", SYSTEM_PROPERTY_DATA_INIT_FOLDER);
            fileProvider = new GitFileProvider(dataInitConfig);
        } else {
            final Path dataInitRootFolder = Paths.get(getSystemPropertyDataInitFolder());
            fileProvider = new LocalFileProvider(dataInitRootFolder, timeService);
            log.info("Using local data init files at {}", dataInitRootFolder);
        }
    }

    private String getSystemPropertyDataInitFolder(){
        String env = System.getenv(SYSTEM_PROPERTY_DATA_INIT_FOLDER);
        if(StringUtils.isNotEmpty(env)){
            return env;
        }
        String prop = System.getProperty(SYSTEM_PROPERTY_DATA_INIT_FOLDER);
        if(StringUtils.isNotEmpty(prop)){
            return prop;
        }
        return null;
    }

}
