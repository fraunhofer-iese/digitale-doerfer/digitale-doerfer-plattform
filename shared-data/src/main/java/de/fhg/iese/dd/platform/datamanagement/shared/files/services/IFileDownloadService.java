/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileDownloadException;
import org.springframework.util.unit.DataSize;

import java.net.URL;
import java.util.List;

/**
 * Required service for downloading files.
 * <p/>
 * Implemented in business layer.
 */
public interface IFileDownloadService {

    /**
     * Downloads a file from an external system
     *
     * @param url               encoded or unencoded url of the file to download
     * @param minSize           minimum file size
     * @param maxSize           maximum file size
     * @param allowedMediaTypes media types that are allowed, checked with HEAD first
     * @return the bytes of the file
     * @throws FileDownloadException if the file could not be downloaded
     */
    byte[] downloadFromExternalSystem(URL url, DataSize minSize, DataSize maxSize, List<String> allowedMediaTypes)
            throws FileDownloadException;

    URL createUrlAndCheck(String rawURL) throws FileDownloadException;

}
