/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Stefan Schweitzer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.participants.geoarea.repos;

import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Transactional(readOnly = true)
public interface GeoAreaRepository extends JpaRepository<GeoArea, String> {

    @Query("select g from GeoArea g where g not in :geoAreasToExclude order by g.created asc")
    List<GeoArea> findAllNotIn(Set<GeoArea> geoAreasToExclude);

    Set<GeoArea> findAllByIdIn(Collection<String> geoAreaIds);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"logo"})
    @Query("select g from GeoArea g order by g.id")
    Page<GeoArea> findAllFull(Pageable page);

}
