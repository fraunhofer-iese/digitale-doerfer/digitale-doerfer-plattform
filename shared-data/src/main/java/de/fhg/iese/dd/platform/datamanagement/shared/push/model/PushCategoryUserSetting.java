/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.push.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Table(
        uniqueConstraints = {@UniqueConstraint(columnNames = {"person_id", "push_category_id", "app_variant_id"})},
        indexes = {
                @Index(columnList = "person_id, push_category_id, app_variant_id, loudPushEnabled")}
)
public class PushCategoryUserSetting extends BaseEntity {

    @ManyToOne(optional=false)
    private Person person;

    @ManyToOne(optional=false)
    private PushCategory pushCategory;

    @ManyToOne(optional=false)
    private AppVariant appVariant;

    @Column(nullable=false)
    private boolean loudPushEnabled;

    @Override
    public String toString() {
        return "PushCategoryUserSetting ["
                + "id=" + id + ", "
                + "person=" + person + ", "
                + "pushCategory=" + pushCategory + ", "
                + (appVariant != null ? "appVariant=" + appVariant.getId() + ", " : "")
                + "loudPushEnabled=" + loudPushEnabled + "]";
    }
    
}
