/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.repos;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;

@Transactional(readOnly = true)
public interface TemporaryDocumentItemRepository extends TemporaryBaseFileItemRepository<DocumentItem, TemporaryDocumentItem> {

    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD,
            attributePaths = {"owner", "fileItem", "fileItem.appVariant", "fileItem.owner", "fileItem.app"})
    @Query("select t from TemporaryDocumentItem t where t.id = :id")
    TemporaryDocumentItem findByIdFull(String id);

}
