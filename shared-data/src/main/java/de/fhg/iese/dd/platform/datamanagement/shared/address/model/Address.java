/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2019 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.address.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.DeletableEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * Addresses are immutable, they can <strong>never</strong> be updated, since we
 * do not know where they are referenced.
 * <p/>
 * e.g. If a delivery uses the same address of a person it would be updated if
 * the person changes the address.
 * <p/>
 * An address has an optional GPSLocation (if the GPSLocation is resolved once,
 * it can be persisted for later lookups).
 * <p/>
 * An address can have empty Strings, if it contains a GPSLocation
 * <p>
 */
@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@NamedEntityGraphs({
        @NamedEntityGraph(name = "Address.reduced",
                includeAllAttributes = true
        ),
        @NamedEntityGraph(name = "Address.full",
                includeAllAttributes = true
        )
})
@Table(indexes = {
        //these indexes need to use prefixes to not get too long
        //unfortunately such a definition can not be done with the annotation, so it is only visible in liquibase
        @Index(columnList = "name, street, zip, city"),
        @Index(columnList = "name, street, zip, city, locationLat, locationLng"),
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class Address extends BaseEntity implements DeletableEntity {

    @Column
    private String name;

    @Column
    private String street;

    @Column
    private String zip;

    @Column
    private String city;

    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name="latitude",column=@Column(name="locationLat")),
        @AttributeOverride(name="longitude",column=@Column(name="locationLng")),
    })
    private GPSLocation gpsLocation;

    @Column(nullable = false)
    @Builder.Default
    private boolean deleted = false;

    @Column(nullable = false)
    @Builder.Default
    private boolean verified = false;

    @Column
    private Long deletionTime;
    
    @JsonIgnore
    public String toHumanReadableString(boolean oneLine) {
        if (deleted) {
            return "gelöschte Addresse";
        }
        if (oneLine) {
            return name + ", " + street + ", " + zip + " " + city;
        } else {
            return name + "\n" + street + "\n" + zip + " " + city;
        }
    }

    @Override
    public String toString() {
        return "Address" + (deleted ? " (deleted)" : "") + " [id=" + id + ", name=" + name + ", street=" + street
                + ", zip=" + zip + ", city=" + city + ", gpsLocation=" + gpsLocation
                + ", verified=" + verified + "]";
    }

}

