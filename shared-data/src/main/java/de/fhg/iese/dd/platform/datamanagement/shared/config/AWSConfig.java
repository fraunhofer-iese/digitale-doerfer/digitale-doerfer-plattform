/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.config;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.Setter;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.client.config.ClientOverrideConfiguration;
import software.amazon.awssdk.regions.Region;

@Validated
@Configuration
@ConfigurationProperties(prefix="dd-platform.aws")
@Getter
@Setter
public class AWSConfig {

    @NotBlank
    private String accessKey;

    @NotBlank
    private String secretKey;

    private String region;

    private S3 s3;

    private Pinpoint pinpoint;

    private Elasticsearch elasticsearch;

    private CloudWatchConfig cloudWatch;

    private List<AWSTag> tags;

    public StaticCredentialsProvider getAWSCredentials() {
        return StaticCredentialsProvider.create(AwsBasicCredentials.create(getAccessKey(), getSecretKey()));
    }

    public Region getAWSRegion() {
        if (StringUtils.isEmpty(region)) {
            return Region.EU_CENTRAL_1;
        } else {
            return Region.of(region);
        }
    }

    public Map<String, String> getAWSTags() {
        return tags.stream().collect(Collectors.toMap(AWSTag::getName, AWSTag::getValue));
    }

    public ClientOverrideConfiguration getDefaultServiceConfiguration() {
        return ClientOverrideConfiguration.builder()
                .apiCallTimeout(Duration.ofSeconds(60))
                .apiCallAttemptTimeout(Duration.ofSeconds(20))
                .build();
    }

    @Getter
    @Setter
    public static class S3 {

        @NotNull
        private String staticContentBucket;
        @NotNull
        private String defaultDataBucket;
        @NotNull
        private String trashBucket;
    }

    @Getter
    @Setter
    public static class Pinpoint {

        private String appPrefix;

    }

    @Getter
    @Setter
    public static class Elasticsearch {

        private String userAgentKey;

    }

    @Getter
    @Setter
    public static class CloudWatchConfig {

        @Getter
        @Setter
        public static class CloudWatchTeamNotificationConfig {

            private boolean enabled;
            private String logGroup;

        }

        @NotNull
        private String metricNameSpace;
        private CloudWatchTeamNotificationConfig teamNotification;

    }

    @Getter
    @Setter
    public static class AWSTag {

        private String name;
        private String value;

    }

}
