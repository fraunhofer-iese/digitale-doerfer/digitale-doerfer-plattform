/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.repos;

import java.util.List;

import org.springframework.context.annotation.Conditional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.framework.repos.conditions.H2Condition;

@Conditional(value = {H2Condition.class})
@Transactional(readOnly = true)
public interface MediaItemH2Repository extends MediaItemRepository {

    @Override
    @Query(nativeQuery = true, value =
            "select kcu_source.table_name, kcu_source.column_name  " +
                    "from INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu_target,  " +
                    "INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc, " +
                    "INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu_source  " +
                    "where lower(kcu_target.table_catalog) = :schema " +
                    "and lower(kcu_target.table_name) = 'media_item' " +
                    "and lower(kcu_target.column_name) = 'id' " +
                    "and rc.unique_constraint_name = kcu_target.constraint_name " +
                    "and kcu_source.constraint_name = rc.constraint_name")
    List<Object[]> findTableAndColumnReferencingMediaItems(String schema);

}
