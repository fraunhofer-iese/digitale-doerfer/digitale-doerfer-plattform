/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.repos.listeners;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.hibernate.event.spi.PostDeleteEvent;
import org.hibernate.event.spi.PostDeleteEventListener;
import org.hibernate.event.spi.PreDeleteEvent;
import org.hibernate.event.spi.PreDeleteEventListener;
import org.hibernate.persister.entity.EntityPersister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.MediaItemPostDeleteListener;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.log4j.Log4j2;

/**
 * Listener on hibernate for PreDelete and PostDelete events. Whenever an entity
 * is deleted, also in case of a cascading delete, this listener is called. It
 * distributes the callback to specific listeners for entities, e.g. if a
 * {@link MediaItem} is deleted the {@link MediaItemPostDeleteListener} is
 * called.
 * </p>
 * To add your own listener:
 * <li>Derive your listener from {@link PreDeleteEventListener} or
 * {@link PostDeleteEventListener}, depending when you want to react
 *
 * <li>Implement the required methods, be aware that only the <code>on*</code>
 * are called
 *
 * <li>You can be sure that the entity in the event is of the right type, no
 * need to do a check here, just cast it
 *
 * <li>Add @Component to it, so that it will be found by spring
 *
 * <li>Add an @Autowired field in the {@link GenericDeleteEventListener} to get
 * your listener
 *
 * <li>Add it to the right map in
 * {@link GenericDeleteEventListener#registerListeners()}
 *
 * <li>Done :-)
 *
 *
 */
@Component
@Log4j2
public class GenericDeleteEventListener implements PreDeleteEventListener, PostDeleteEventListener {

    private static final long serialVersionUID = 987610886518922979L;

    //private Map<Class<?>, PreDeleteEventListener> preDeleteListeners;
    private Map<Class<?>, PostDeleteEventListener> postDeleteListenersByEntityClass;

    @SuppressFBWarnings(
            value = "SE_BAD_FIELD",
            justification = "If this listener gets serialized it needs a new executor service anyway.")
    private ExecutorService executorService = null;

    private ExecutorService getExecutor() {
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor(new CustomizableThreadFactory("delete-"));
        }
        return executorService;
    }

    @Autowired
    private List<IEntityDeleteListener> deleteListeners;

    /**
     * Registers the listeners for the according entities.
     * <p/>
     * Called after all
     * injects have been done.
     */
    @PostConstruct
    public void registerListeners() {
        if (!CollectionUtils.isEmpty(deleteListeners)) {
            postDeleteListenersByEntityClass = deleteListeners.stream()
                    .collect(Collectors.toMap(IEntityDeleteListener::relevantEntityClass, l -> l));
        } else {
            postDeleteListenersByEntityClass = Collections.emptyMap();
        }
    }

    @Override
    public boolean onPreDelete(PreDeleteEvent event) {
        /*
        Currently commented out until we need some pre-delete actions
        Class<?> entityClass = event.getEntity().getClass();
        PreDeleteEventListener listener = preDeleteListeners.get(entityClass);
        if (listener != null) {
            getExecutor().execute(()-> {
                try {
                    listener.onPreDelete(event);
                } catch (Exception e) {
                    log.error("Failed to execute PreDelete listener "+listener.getClass().getSimpleName(), e);
                }
            });
        }
        */
        return false;
    }

    @Override
    public void onPostDelete(PostDeleteEvent event) {
        Class<?> entityClass = event.getEntity().getClass();
        PostDeleteEventListener listener = postDeleteListenersByEntityClass.get(entityClass);
        if (listener != null) {
            getExecutor().submit(() -> {
                try {
                    listener.onPostDelete(event);
                } catch (Exception e) {
                    log.error("Failed to execute PostDelete listener " + listener.getClass().getSimpleName(), e);
                }
            });
        }
    }

    /**
     * Not used so far.
     *
     * @see org.hibernate.event.spi.PostDeleteEventListener#requiresPostCommitHandling(org.hibernate.persister.entity.EntityPersister)
     */
    @Override
    @SuppressWarnings({"deprecation", "RedundantSuppression"})//deprecated due to typo ^^
    public boolean requiresPostCommitHanding(EntityPersister persister) {
        return false;
    }

    @PreDestroy
    private void shutDownExecutor() {
        if (executorService != null) {
            executorService.shutdown();
            try {
                executorService.awaitTermination(20, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            executorService.shutdownNow();
        }
    }

}
