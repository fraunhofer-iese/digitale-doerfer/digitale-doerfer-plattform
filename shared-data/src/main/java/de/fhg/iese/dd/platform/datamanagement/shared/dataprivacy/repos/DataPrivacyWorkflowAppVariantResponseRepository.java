/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollection;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollectionAppVariantResponse;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflow;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariantResponse;

public interface DataPrivacyWorkflowAppVariantResponseRepository extends JpaRepository<DataPrivacyWorkflowAppVariantResponse, String> {

    @Query("select d " +
            "from DataPrivacyDataCollectionAppVariantResponse d " +
            "where d.dataPrivacyWorkflowAppVariant.dataPrivacyWorkflow = :dataCollection " +
            "and d.fileName is not null " +
            "order by d.created desc")
    List<DataPrivacyDataCollectionAppVariantResponse> findAllByDataCollectionAppVariantAndFileNameIsNotNull(
            DataPrivacyDataCollection dataCollection);

    @Query("select count(d.id) " +
            "from DataPrivacyDataCollectionAppVariantResponse d " +
            "where d.dataPrivacyWorkflowAppVariant = :collectionAppVariant " +
            "and d.fileName is not null ")
    int countAllByDataPrivacyDataCollectionAppVariant(DataPrivacyWorkflowAppVariant collectionAppVariant);

    @Transactional
    @Modifying
    int deleteAllByDataPrivacyWorkflowAppVariant_DataPrivacyWorkflow(DataPrivacyWorkflow dataPrivacyWorkflow);

}
