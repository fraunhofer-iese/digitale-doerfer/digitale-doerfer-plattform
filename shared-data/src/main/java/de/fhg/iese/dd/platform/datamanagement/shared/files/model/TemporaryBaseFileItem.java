/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * Abstract parent for {@link TemporaryMediaItem} and {@link TemporaryDocumentItem}, since both have a lot in common.
 *
 * @param <F> the concrete subtype of {@link BaseFileItem} that is wrapped in the temporary item
 */
@Entity
@Table(name = "temporary_media_item")
//all subclasses are stored in a single table and different types are distinguished via the discriminator field
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//this tells hibernate to use the field file_type to store the discriminator
@DiscriminatorColumn(name = "file_type", discriminatorType = DiscriminatorType.INTEGER, columnDefinition = "TINYINT")
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public abstract class TemporaryBaseFileItem<F extends BaseFileItem> extends BaseEntity {

    @ManyToOne(optional = false)
    private Person owner;

    /**
     * Associated temporary BaseFileItem, no orphan removal, so that we can un-attach the file item and then delete this
     * entity when we want to use the file item.
     * </p>
     * There is no need to delete the files associated with the file item manually before deleting this entity.
     *
     * @see BaseFileItem for more details
     */
    @ManyToOne(cascade = CascadeType.ALL, optional = true, targetEntity = BaseFileItem.class)
    private F fileItem;

    @Column
    private long expirationTime;

    @Override
    public String toString() {
        return "TemporaryBaseFileItem [" +
                "id='" + id + "', " +
                "owner='" + BaseEntity.getIdOf(owner) + "', " +
                "fileItem='" + fileItem + "', " +
                "expirationTime='" + expirationTime + "', " +
                ']';
    }

}
