/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums;

import org.apache.logging.log4j.Level;

import de.fhg.iese.dd.platform.datamanagement.framework.enums.StorableEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TeamNotificationPriority implements StorableEnum {

    /* Technical */

    /**
     * An error on technical level that concerns the whole system.
     */
    ERROR_TECHNICAL_SYSTEM(Level.ERROR, Scope.TECHNICAL, Impact.SYSTEM),

    /**
     * An error on technical level that concerns a single user.
     */
    ERROR_TECHNICAL_SINGLE_USER(Level.ERROR, Scope.TECHNICAL, Impact.SINGLE_USER),

    /**
     * An event on technical level that has high impact on the whole system.
     */
    WARN_TECHNICAL_SYSTEM(Level.WARN, Scope.TECHNICAL, Impact.SYSTEM),

    /**
     * An event on technical level that concerns the whole system.
     */
    INFO_TECHNICAL_SYSTEM(Level.INFO, Scope.TECHNICAL, Impact.SYSTEM),

    /**
     * An event on technical level that concerns multiple users, e.g. deletion of multiple accounts
     */
    INFO_TECHNICAL_MULTIPLE_USERS(Level.INFO, Scope.TECHNICAL, Impact.MULTIPLE_USERS),

    /**
     * An event on technical level that concerns a single user, e.g. account related events
     */
    INFO_TECHNICAL_SINGLE_USER(Level.INFO, Scope.TECHNICAL, Impact.SINGLE_USER),

    /**
     * A general debugging event on technical level that concerns a single user
     */
    DEBUG_TECHNICAL_SINGLE_USER(Level.DEBUG, Scope.TECHNICAL, Impact.SINGLE_USER),

    /* Application */

    /**
     * An error on application level that concerns the whole application.
     */
    ERROR_APPLICATION_SYSTEM(Level.ERROR, Scope.APPLICATION, Impact.SYSTEM),

    /**
     * An error on application level that concerns the whole application.
     */
    ERROR_APPLICATION_SINGLE_USER(Level.ERROR, Scope.APPLICATION, Impact.SINGLE_USER),

    /**
     * A warn event on application level that concerns the whole application.
     */
    WARN_APPLICATION_SYSTEM(Level.WARN, Scope.APPLICATION, Impact.SYSTEM),

    /**
     * A warn event on application level that concerns multiple users, e.g. flagged content.
     */
    WARN_APPLICATION_MULTIPLE_USERS(Level.WARN, Scope.APPLICATION, Impact.MULTIPLE_USERS),

    /**
     * An event on application level that concerns the whole system (in this case the whole application).
     */
    INFO_APPLICATION_SYSTEM(Level.INFO, Scope.APPLICATION, Impact.SYSTEM),

    /**
     * An info event on application level that concerns multiple users, e.g. content creation that is visible for many
     * users.
     */
    INFO_APPLICATION_MULTIPLE_USERS(Level.INFO, Scope.APPLICATION, Impact.MULTIPLE_USERS),

    /**
     * An info event on application level that concerns a single users, e.g. blocking of other users.
     */
    INFO_APPLICATION_SINGLE_USER(Level.INFO, Scope.APPLICATION, Impact.SINGLE_USER),

    /**
     * A general debugging event on application level that concerns a single user.
     */
    DEBUG_APPLICATION_SINGLE_USER(Level.DEBUG, Scope.APPLICATION, Impact.SINGLE_USER);

    TeamNotificationPriority(Level logLevel,
            Scope scope, Impact impact) {
        this.logLevel = logLevel;
        this.scope = scope;
        this.impact = impact;
        // The bitMaskValue changes on re-ordering the values
        // This is okay, since all stored TeamNotificationChannelMappings get fully replaced on data init
        // Changing them requires an immediate data init after deploying the new version.
        this.bitMaskValue = 1 << ordinal();
    }

    private final Level logLevel;
    private final Scope scope;
    private final Impact impact;
    private final int bitMaskValue;

    public enum Scope {

        TECHNICAL,
        APPLICATION

    }

    public enum Impact {

        SYSTEM,
        MULTIPLE_USERS,
        SINGLE_USER;

        public boolean hasMoreImpactThan(final Impact impact) {
            return this.ordinal() <= impact.ordinal();
        }
    }

}
