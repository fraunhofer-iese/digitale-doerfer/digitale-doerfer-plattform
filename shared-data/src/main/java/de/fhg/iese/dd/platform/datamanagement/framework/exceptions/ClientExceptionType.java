/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.exceptions;

import org.springframework.http.HttpStatus;

public enum ClientExceptionType {
    //!!!DO NOT REMOVE/MODIFY ENTRIES!!!
    //Only add new entries!

    //Internal Server Errors (500)
    UNSPECIFIED_SERVER_ERROR("An internal server error occurred", 500),
    SEND_SMS_FAILED("Sending of the SMS failed", 500),
    SEND_EMAIL_FAILED("Sending of the email failed", 500),
    PUSH_FAILED("Sending of a push notification failed", 500),
    PUSH_MESSAGE_INVALID("The push message was invalid", 500),
    DATA_INITIALIZATION_FAILED("Some data could not be initialized", 500),
    TEAM_NOTIFICATION_CONNECTION_CONFIG_INVALID("The connection config in the team notification connection is invalid",
            500),
    FILE_STORAGE_FAILED("There was a problem with a file on the storage", 500),
    INVALID_FEATURE_CONFIGURATION("The feature configuration is invalid", 500),
    REFLECTION_FAILED("The reflection operation failed", 500),

    //Not Found Errors (404)
    UNSPECIFIED_NOT_FOUND("Whatever you searched for, it does not exist", 404),

    TENANT_NOT_FOUND("The requested tenant does not exist", 404),
    GEO_AREA_NOT_FOUND("The requested geo area does not exist", 404),
    PERSON_NOT_FOUND("The requested person does not exist", 404),
    OAUTH_ACCOUNT_NOT_FOUND("The requested oauth account does not exist", 404),
    SHOP_NOT_FOUND("The requested shop does not exist", 404),
    APP_NOT_FOUND("The requested app does not exist", 404),
    APP_VARIANT_NOT_FOUND("The requested app variant does not exist", 404),
    APP_VARIANT_TENANT_CONTRACT_NOT_FOUND("The requested app variant tenant contract does not exist", 404),
    OAUTH_CLIENT_NOT_FOUND("The requested oauth client does not exist", 404),
    ROLE_NOT_FOUND("The requested role does not exist", 404),
    RELATED_ENTITY_NOT_FOUND("The requested related entity does not exist", 404),
    FILE_ITEM_NOT_FOUND("The requested media item does not exist", 404),
    TEMPORARY_FILE_ITEM_NOT_FOUND("The requested temporary file item does not exist", 404),
    STATISTICS_REPORT_NOT_FOUND("The requested statistics report does not exist", 404),
    TEAM_FILE_NOT_FOUND("The requested file does not exist in the team file storage", 404),
    LEGAL_TEXT_NOT_FOUND("The requested legal text could not be found", 404),
    NEWS_SOURCE_NOT_FOUND("The requested newsSource could not be found", 404),
    CRAWLING_CONFIG_NOT_FOUND("The requested crawlingConfig could not be found", 404),
    DEFAULT_MEDIA_ITEM_NOT_FOUND("The requested default media item could not be found", 404),
    CONTACT_PERSON_NOT_FOUND("The requested contact person could not be found", 404),
    FEATURE_NOT_FOUND("The requested feature could not be found", 404),

    PERSON_WITH_EMAIL_NOT_FOUND("No person exists with the given email", 404),
    PERSON_WITH_OAUTH_ID_NOT_FOUND("No person exists with the given OAuth ID", 404),

    DELIVERY_NOT_FOUND("The requested delivery does not exist", 404),
    INTELLIGENT_CONTAINER_NOT_FOUND("The requested intelligent container does not exist", 404),
    POOLING_STATION_BOX_NOT_FOUND("The requested box of the pooling station does not exist", 404),
    POOLING_STATION_NOT_FOUND("The requested pooling station does not exist", 404),
    PURCHASE_ORDER_NOT_FOUND("The requested purchase order does not exist", 404),
    RECEIVER_PICKUP_ASSIGNMENT_NOT_FOUND("The requested receiver pickup assignment does not exist", 404),
    TRANSPORT_ALTERNATIVE_BUNDLE_NOT_FOUND("The requested transport alternative bundle does not exist", 404),
    TRANSPORT_ALTERNATIVE_NOT_FOUND("The requested transport alternative does not exist", 404),
    TRANSPORT_ASSIGNMENT_NOT_FOUND("The requested transport assignment does not exist", 404),
    TRANSPORT_ASSIGNMENT_OR_ALTERNATIVE_NOT_FOUND("The requested transport assignment or alternative does not exist",
            404),
    TRANSPORT_CONFIRMATION_NOT_FOUND("The requested transport confirmation does not exist", 404),

    PUSH_ENDPOINT_NOT_FOUND("The requested push endpoint does not exist", 404),
    PUSH_TOPIC_NOT_FOUND("The requested push topic does not exist", 404),
    PUSH_CATEGORY_NOT_FOUND("The requested push category does not exist", 404),

    NAMED_COUNTER_NOT_FOUND("The named counter does not exist", 404),

    CHAT_NOT_FOUND("The requested chat does not exist", 404),

    ACCOUNT_NOT_FOUND("The requested account does not exist", 404),
    ACCOUNT_ENTRY_NOT_FOUND("The requested account entry does not exist", 404),
    KEY_NOT_FOUND("The requested key-value pair could not be found.", 404),

    SELLING_POINT_NOT_FOUND("The requested selling point does not exist", 404),
    SELLING_VEHICLE_NOT_FOUND("The requested selling vehicle does not exist", 404),
    RAW_LOCATION_RECORD_NOT_FOUND("The raw location record does not exist", 404),
    USER_GENERATED_CONTENT_FLAG_NOT_FOUND("The requested user generated content flag does not exists", 404),

    ENDPOINT_NOT_FOUND("The requested page endpoint does not exist", HttpStatus.NOT_FOUND),

    DATA_PRIVACY_WORKFLOW_NOT_FOUND("The requested data privacy workflow does not exist",
            HttpStatus.NOT_FOUND),

    //Grapevine
    POST_NOT_FOUND("The requested post does not exist", 404),
    COMMENT_NOT_FOUND("The requested comment does not exist", 404),
    POST_NOT_LIKED("The requested post has not been liked before", 404),
    COMMENT_NOT_LIKED("The requested comment has not been liked before", 404),
    TRADING_CATEGORY_NOT_FOUND("The requested trading category does not exist", 404),
    SUGGESTION_CATEGORY_NOT_FOUND("The requested suggestion category does not exist", 404),
    GROUP_NOT_FOUND("The requested group does not exist", 404),
    CONVENIENCE_NOT_FOUND("The requested convenience does not exist", HttpStatus.NOT_FOUND),
    ORGANIZATION_NOT_FOUND("The requested organization does not exist", HttpStatus.NOT_FOUND),

    ACHIEVEMENT_LEVEL_NOT_FOUND("The requested achievement level does not exist", 404),

    //Bad Request Errors (400)
    UNSPECIFIED_BAD_REQUEST("The sent request is not valid", 400),
    CLIENT_ABORTED_CONNECTION("The client aborted the connection", 400),
    EVENT_ATTRIBUTE_INVALID("An attribute of the provided event is not valid", 400),
    QUERY_PARAMETER_INVALID("The provided query parameter is not valid", 400),
    PAGE_PARAMETER_INVALID("The page parameters are invalid", 400),
    SEARCH_PARAMETER_TOO_SHORT("The search parameter is too short", 400),
    SEARCH_PARAMETER_TOO_LONG("The search parameter is too long", 400),
    CONCURRENT_REQUEST_FAILED("The request was done concurrently with another of the same type and failed", 400),
    INVALID_ADMIN_TASK_PARAMETERS("The parameters are invalid for this admin task", 400),
    FEATURE_NOT_ENABLED("The feature is not enabled for this user", 400),
    RELATED_ENTITY_MUST_NOT_BE_NULL("The given role expects a non-null entity", 400),

    FILE_ITEM_PROCESSING_FAILED("The processing of a file item failed", HttpStatus.PRECONDITION_FAILED),
    FILE_ITEM_UPLOAD_FAILED("The upload of a file item failed", 400),
    FILE_DOWNLOAD_FAILED("The download of a file failed", HttpStatus.EXPECTATION_FAILED),

    INVALID_IMAGE_CROP_DEFINITION("The image crop definition is invalid", HttpStatus.BAD_REQUEST),

    FILE_TYPE_NOT_DETECTABLE("The media type / mime type / file extension of the file could not be detected",
            HttpStatus.BAD_REQUEST),
    FILE_ITEM_DUPLICATE_USAGE("At least one file item is used at least twice", 400),
    TEMPORARY_FILE_ITEM_LIMIT_EXCEEDED("The maximum number of temporary file items is exceeded", 400),
    TEMPORARY_FILE_ITEM_DUPLICATE_USAGE("At least one temporary file item is used at least twice", 400),

    URI_SCHEME_INVALID("The provided URI scheme is not valid", HttpStatus.BAD_REQUEST),

    PERSON_INFORMATION_INVALID("The provided person information is not valid", 400),
    PERSON_PARAMETERS_INVALID("The provided person parameters are not valid", 400),
    ACCOUNT_TYPE_INVALID("The provided account type is not valid", 400),
    GEO_AREA_IS_NO_LEAF("The selected geo is not a leaf area", 400),

    BLOCKED_PERSONS_LIMIT_EXCEEDED("The limit for maximum blocked persons has exceeded", 400),

    APP_VARIANT_USAGE_INVALID("The requested app variant usage is invalid", 400),
    PLATFORM_NOT_SUPPORTED("The provided platform is not supported", 400),
    PUSH_CATEGORY_NOT_CONFIGURABLE("The requested push category is not configurable", 400),

    ADDRESS_INVALID("The provided address is not valid", 400),

    PURCHASE_ORDER_INVALID("The provided purchase order is not valid", 400),
    WRONG_TRACKING_CODE("The given tracking code is wrong", 400),
    CHAT_MESSAGE_INVALID("The provided chat message is not valid", 400),
    CHAT_NOT_UNIQUE_BY_SUBJECT(
            "More than one chat was found for the combination of subjects, but only one is supported", 400),

    CATEGORY_NAME_INVALID("The provided category name is invalid", 400),
    KEY_NAME_INVALID("The provided key name is invalid", 400),

    SCORE_EXCHANGE_AMOUNT_INVALID("The score amount to be exchanged is invalid", 400),

    GROUP_CANNOT_BE_DELETED("Group cannot be deleted", HttpStatus.BAD_REQUEST),
    FILE_ITEM_CANNOT_BE_DELETED("Media item cannot be deleted", HttpStatus.BAD_REQUEST),

    USER_GENERATED_CONTENT_FLAG_ALREADY_EXISTS("User generated content flag already exists", 400),
    USER_GENERATED_CONTENT_FLAG_ENTITY_CAN_NOT_BE_DELETED("User generated content flag entity can not be deleted",
            HttpStatus.BAD_REQUEST),
    USER_GENERATED_CONTENT_FLAG_STATUS_CHANGE_INVALID("User generated content flag entity status change is invalid",
            HttpStatus.BAD_REQUEST),

    POST_INVALID("The provided post is not valid", 400),
    POST_TYPE_CANNOT_BE_CHANGED("The type of an existing post can not be changed", 400),
    INVALID_SORTING_CRITERION("The provided sorting criterion is not valid", 400),
    INVALID_FILTER_CRITERIA("The provided filter criteria are not valid", 400),
    POST_COULD_NOT_BE_CREATED("The post could not be created because the creator has no home area set", 400),
    POST_HAS_NO_CREATOR("The post has no creator, but the action needs a creator", 400),

    POST_ALREADY_LIKED("The provided post is already liked by this user", 400),
    COMMENT_ALREADY_LIKED("The provided comment is already liked by this user", 400),
    POST_ALREADY_UNLIKED("The provided post is already unliked by this user", 400),
    COMMENT_ALREADY_UNLIKED("The provided comment is already unliked by this user", 400),
    ORGANIZATION_NOT_AVAILABLE("The organization is not available", HttpStatus.BAD_REQUEST),

    VEHICLE_NOT_IN_OPERATING_HOURS("The referenced vehicle is currently not in its operating hours", 400),

    DEFAULT_TRADING_CATEGORY_MUST_NOT_BE_DELETED("The default trading category must not be deleted", 400),
    DEFAULT_SUGGESTION_CATEGORY_MUST_NOT_BE_DELETED("The default suggestion category must not be deleted", 400),
    SUGGESTION_WORKER_ALREADY_WORKING_ON_SUGGESTION("The worker is already working on the suggestion", 400),
    SUGGESTION_WORKER_NOT_WORKING_ON_SUGGESTION("The worker is not on the suggestion", 400),
    SUGGESTION_STATUS_CHANGE_INVALID("The suggestion status change is invalid", 400),
    SUGGESTION_CANNOT_BE_DELETED("Suggestions cannot be deleted", 400),

    EMAIL_ADDRESS_INVALID("The provided email address is invalid", 400),
    PHONE_NUMBER_INVALID("The provided phone number is invalid", HttpStatus.BAD_REQUEST),

    SHOP_NOT_DELETABLE("The shop can not be deleted.", 400),

    OAUTH_ID_REGISTRATION_ALREADY_EXISTS("An OAuth registration with this OAuth ID is already existing.", 400),

    RESEND_VERIFICATION_EMAIL_NOT_POSSIBLE("Time period for resending verification mail is not elapsed",
            HttpStatus.BAD_REQUEST),

    GROUP_GEO_AREAS_INVALID("The included, excluded and main geo area are contradicting", HttpStatus.BAD_REQUEST),
    APP_VARIANT_ALREADY_EXISTS("The app variant does already exist", HttpStatus.BAD_REQUEST),
    NEWS_SOURCE_ALREADY_EXISTS("The news source does already exist", HttpStatus.BAD_REQUEST),

    GROUP_MEMBERSHIP_ADMIN_REMOVAL_NOT_POSSIBLE("The only group membership admin can not be removed",
            HttpStatus.BAD_REQUEST),

    DATA_PRIVACY_WORKFLOW_RESPONSE_REJECTED("The data privacy workflow response was rejected",
            HttpStatus.BAD_REQUEST),

    //Authorization Errors (403)
    UNSPECIFIED_AUTHORIZATION_ERROR("An unspecified authorization error occurred", 403),

    ROLE_MANAGEMENT_NOT_ALLOWED("The user is not allowed to manage this role", 403),

    NOT_AUTHORIZED("The user is not authorized to do this action or view the data", HttpStatus.FORBIDDEN),

    PERSON_VERIFICATION_STATUS_INSUFFICIENT(
            "The users verification status is not sufficient to do this action or view the data", HttpStatus.FORBIDDEN),

    EMAIL_ALREADY_REGISTERED("The provided email is already registered", 403),

    OAUTH_EMAIL_ALREADY_REGISTERED("The provided email is already registered in OAuth", 403),
    PASSWORD_WRONG("The provided password was wrong", HttpStatus.FORBIDDEN),
    EMAIL_CHANGE_NOT_POSSIBLE("The provided email could not be changed", 403),
    PASSWORD_CHANGE_NOT_POSSIBLE("The password could not be changed", 403),
    PERSON_ALREADY_EXISTS("The person does already exist", 403),

    WRONG_CARRIER_CANCELLED("The wrong carrier tried to cancel the transport assignment", 403),
    WRONG_CARRIER_DELIVERED("The wrong carrier delivered the parcel", 403),
    WRONG_LOCATION_DELIVERED("The parcel has been delivered to the wrong location", 403),
    WRONG_CARRIER_PICKUP("The wrong carrier picked up the parcel", 403),
    WRONG_RECEIVER_RECEIVED("The wrong receiver received the parcel", 403),
    CHAT_SENDER_NOT_CARRIER("The sender of a chat message is not the carrier", 403),
    TRANSPORT_NOT_ASSIGNED_TO_CURRENT_PERSON("The transport is not assigned to the current person", 403),

    ACCESSED_ANOTHERS_RESOURCES("The requested resources belong to someone else", 403),
    PERSON_NOT_CHAT_PARTICIPANT("The person trying to access the chat is not a participant of the chat", 403),

    POST_ACTION_NOT_ALLOWED("The action on the post can not be done by this user", 403),
    COMMENT_ACTION_NOT_ALLOWED("The action on the comment can not be done by this user", 403),
    GROUP_CONTENT_NOT_ACCESSIBLE("The user is not allowed to view the content of the group", 403),
    COMMENT_CREATION_NOT_ALLOWED("Creation of comments is not allowed for this post", HttpStatus.FORBIDDEN),

    MEDIA_ITEM_USAGE_NOT_ALLOWED("The usage of the media item is only allowed by its owner", 403),

    //Authentication Error (401)
    NOT_AUTHENTICATED("Mandatory authentication not provided", HttpStatus.UNAUTHORIZED),
    WRONG_CHECK("Wrong check", HttpStatus.UNAUTHORIZED),

    //External Errors (417)
    POOLING_STATION_FULL("No more space in the pooling station", 417),
    POOLING_STATION_BOX_CANNOT_BE_OPENED(
            "Door of box cannot be opened, either due to a communication or hardware failure", 417),
    UNSPECIFIED_EXTERNAL_ERROR("An external server error occurred", 417),
    EXTERNAL_SYSTEM_CALL_FAILED("A call to an external system failed", HttpStatus.EXPECTATION_FAILED),
    SEARCH_NOT_AVAILABLE("The external search engine is not available", HttpStatus.EXPECTATION_FAILED),

    //Status Conflict Errors (409)
    DELIVERY_NOT_IN_POOLING_STATION("The delivery is not in a pooling station", 409),
    DELIVERY_ALREADY_IN_POOLING_STATION("The delivery is already in a pooling station", 409),
    DELIVERY_STILL_IN_POOLING_STATION("The delivery is still in a pooling station", 409),
    DELIVERY_STATUS_INVALID("The status of the delivery is not valid", 409),
    TRANSPORT_ASSIGNMENT_STATUS_INVALID("The status of the transport assignment is not valid", 409),
    DELIVERY_STATUS_ALREADY_REACHED("The delivery reached already the status", 409),
    UNSPECIFIED_STATUS_CONFLICT("An unspecified status conflict occurred", 409),
    POOLING_STATION_BOX_NOT_CLOSED("The box of the pooling station is not closed", 409),

    DELIVERY_PICKED_UP_AT_WRONG_LOCATION("Receiver tried to pick up delivery at wrong location", 409),

    ACCOUNT_ENTRY_ALREADY_BOOKED("The account entry is already booked", 409),

    ACCOUNT_SHORTAGE("The account exceeds the credit limit", 409),

    RESERVATION_NOT_EXISTING("There is no reservation account entry existing", 409),

    RESERVATIONS_NOT_MATCHING("The two reservations do not match and can not be booked", 409),

    GROUP_MEMBERSHIP_ALREADY_PENDING("The group membership status is already PENDING", 409),

    NOT_MEMBER_OF_GROUP("The user is not a member of the group", 409),

    PERSON_HAS_NO_PENDING_NEW_EMAIL(
            "The person is currently not changing the email address and thus has no pending new email",
            HttpStatus.CONFLICT),

    DATA_PRIVACY_WORKFLOW_ALREADY_FINISHED("The data privacy workflow is already finished",
            HttpStatus.CONFLICT),

    //Unprocessable Entity (422)
    USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE(
            "The requested user generated content flag has a different type, use a different endpoint for getting the details",
            HttpStatus.UNPROCESSABLE_ENTITY),

    //Internal Server Error (500)
    UNSPECIFIED_ERROR("An unspecified error occurred", HttpStatus.INTERNAL_SERVER_ERROR),

    VERSION_PARSING_FAILED("The version could not be parsed", HttpStatus.INTERNAL_SERVER_ERROR),

    ENCRYPTION_FAILED("The encryption failed, most likely due to misconfiguration", HttpStatus.INTERNAL_SERVER_ERROR),

    DATA_DEPENDENCY_UNSATISFIABLE("The data dependency can not be satisfied", HttpStatus.INTERNAL_SERVER_ERROR),

    REFERENCE_DATA_DELETION_FAILED("The request to delete the reference data failed", HttpStatus.INTERNAL_SERVER_ERROR),

    DATA_PRIVACY_PROCESSING_FAILED("The data privacy operation failed", HttpStatus.INTERNAL_SERVER_ERROR),

    STATISTICS_PROCESSING_FAILED("The statistics collection operation failed", HttpStatus.INTERNAL_SERVER_ERROR),

    USER_GENERATED_CONTENT_FLAG_ENTITY_DELETION_FAILED("User generated content flag entity failed to be deleted",
            HttpStatus.INTERNAL_SERVER_ERROR),

    USER_GENERATED_CONTENT_FLAG_INVALID("User generated content flag entity is internally invalid",
            HttpStatus.INTERNAL_SERVER_ERROR),

    //Not Implemented (501)
    UNEXPECTED_EVENT_OCCURRED("An unexpected event occurred", HttpStatus.NOT_IMPLEMENTED),

    //Service Unavailable (503)
    REQUEST_PROCESSING_TIMEOUT("A timeout occurred while processing the request", HttpStatus.SERVICE_UNAVAILABLE),
    ;

    private final String explanation;
    private final HttpStatus httpStatus;

    /**
     * Only for legacy status, use {@link #ClientExceptionType(String, HttpStatus)} instead
     */
    ClientExceptionType(String explanation, int httpStatusCode) {
        this.explanation = explanation;
        this.httpStatus = HttpStatus.valueOf(httpStatusCode);
    }

    ClientExceptionType(String explanation, HttpStatus httpStatus) {
        this.explanation = explanation;
        this.httpStatus = httpStatus;
    }

    public String getExplanation() {
        return explanation;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

}

