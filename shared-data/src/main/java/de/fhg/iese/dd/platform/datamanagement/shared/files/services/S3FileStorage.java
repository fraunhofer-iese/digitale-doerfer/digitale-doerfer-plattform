/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.shared.config.AWSConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.FileStorageConfig;
import lombok.extern.log4j.Log4j2;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.CopyObjectRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectResponse;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.MetadataDirective;
import software.amazon.awssdk.services.s3.model.NoSuchKeyException;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.S3Object;
import software.amazon.awssdk.services.s3.model.ServerSideEncryption;

/**
 * S3 file storage based on {@link IFileStorage}
 */
@Component
@Profile("aws | S3FileStorageTest")
@Log4j2
public class S3FileStorage extends BaseFileStorage {

    private final S3Client s3client;
    private final String defaultDataBucket;
    private final String staticContentBucket;
    private final String trashBucket;

    @Autowired
    public S3FileStorage(FileStorageConfig fileStorageConfig, AWSConfig awsConfig) {
        super(fileStorageConfig);
        this.staticContentBucket = awsConfig.getS3().getStaticContentBucket();
        this.defaultDataBucket = awsConfig.getS3().getDefaultDataBucket();
        this.trashBucket = awsConfig.getS3().getTrashBucket();

        s3client = createS3Client(awsConfig);

        log.info("Using S3FileStorage as file storage");
        log.info("defaultStorageLocation: '{}'", defaultDataBucket);
        log.info("staticStorageLocation: '{}'", staticContentBucket);
        log.info("trashStorageLocation: '{}'", trashBucket);
        log.info("externalBaseUrl: '{}'", getExternalBaseUrl());
    }

    private S3Client createS3Client(AWSConfig awsConfig) {

        return S3Client.builder()
                .region(awsConfig.getAWSRegion())
                .credentialsProvider(awsConfig.getAWSCredentials())
                .overrideConfiguration(awsConfig.getDefaultServiceConfiguration())
                .build();
    }

    @Override
    public void saveFile(byte[] content, String mimeType, String internalFileName) throws FileStorageException {
        checkFileName(internalFileName);
        try {

            s3client.putObject(PutObjectRequest.builder()
                    .bucket(staticContentBucket)
                    .key(internalFileName)
                    .contentLength((long) content.length)
                    .contentType(mimeType)
                    .serverSideEncryption(ServerSideEncryption.AES256)
                    .build(), RequestBody.fromBytes(content));
        } catch (SdkException e) {
            throw new FileStorageException(
                    "File '" + internalFileName + "' could not be uploaded to S3: " + e, e);
        }
    }

    @Override
    public void copyFile(String internalFileNameSource, String internalFileNameTarget) throws FileStorageException {
        checkFileName(internalFileNameSource);
        checkFileName(internalFileNameTarget);
        try {
            if (!copyFile(staticContentBucket, internalFileNameSource, staticContentBucket, internalFileNameTarget)) {
                throw new FileStorageException(
                        "Copy of file '" + internalFileNameSource + "' to '" + internalFileNameTarget +
                                "' failed due to internal AWS failure");
            }
        } catch (SdkException e) {
            throw new FileStorageException(
                    "Copy of file '" + internalFileNameSource + "' to '" + internalFileNameSource +
                            "' failed: " + e, e);
        }
    }

    @Override
    public InputStream getFile(String internalFileName) throws FileStorageException {
        checkFileName(internalFileName);
        try {
            return getFile(staticContentBucket, internalFileName);
        } catch (SdkException | IOException e) {
            throw new FileStorageException("Getting file '" + internalFileName + "' failed: " + e, e);
        }
    }

    @Override
    public boolean fileExistsDefault(String internalFileName) {
        return fileExistsInternal(defaultDataBucket, internalFileName);
    }

    private boolean fileExistsInternal(String bucket, String internalFileName) {
        try {
            s3client.headObject(HeadObjectRequest.builder()
                    .bucket(bucket)
                    .key(internalFileName)
                    .build());
            return true;
        } catch (NoSuchKeyException e) {
            return false;
        }
    }

    @Override
    public void copyFileFromDefault(String defaultInternalFileName, String internalFileName)
            throws FileStorageException {
        checkFileName(internalFileName);
        checkFileName(defaultInternalFileName);
        try {
            if (!copyFile(defaultDataBucket, defaultInternalFileName, staticContentBucket, internalFileName)) {
                throw new FileStorageException(
                        "Copy of default file '" + defaultInternalFileName + "' to '" + internalFileName +
                                "' failed due to internal AWS failure");
            }
        } catch (SdkException e) {
            throw new FileStorageException(
                    "Copy of default file '" + defaultInternalFileName + "' to '" + internalFileName + "' failed: " +
                            e, e);
        }
    }

    @Override
    public InputStream getFileFromDefault(String defaultInternalFileName) throws FileStorageException {
        checkFileName(defaultInternalFileName);
        try {
            return getFile(defaultDataBucket, defaultInternalFileName);
        } catch (SdkException | IOException e) {
            throw new FileStorageException("Getting file '" + defaultInternalFileName + "' from default failed: " + e,
                    e);
        }
    }

    private InputStream getFile(String bucketName, String fileName) throws SdkException, IOException {

        return s3client.getObjectAsBytes(GetObjectRequest.builder()
                        .bucket(bucketName)
                        .key(fileName)
                        .build())
                .asInputStream();
    }

    @Override
    public boolean fileExists(String internalFileName) {
        return fileExistsInternal(staticContentBucket, internalFileName);
    }

    @Override
    public List<String> findAll(String folderName) {
        try {

            ListObjectsV2Request request = ListObjectsV2Request.builder()
                    .bucket(staticContentBucket)
                    .prefix(folderName)
                    .maxKeys(5000)
                    .build();
            ListObjectsV2Response response;
            List<String> files = new ArrayList<>();
            do {
                response = s3client.listObjectsV2(request);

                files.addAll(response.contents().stream()
                        .map(S3Object::key)
                        .collect(Collectors.toList()));

                // If there are more than maxKeys keys in the bucket, get a continuation token and list the next objects
                String token = response.nextContinuationToken();
                request = request.toBuilder()
                        .continuationToken(token)
                        .build();
            } while (response.isTruncated() && files.size() < 100000);

            return files;
        } catch (SdkException e) {
            throw new FileStorageException("Error while retrieving all files: " + e.getMessage(), e);
        }
    }

    @Override
    public void moveToTrash(String internalFileName) {
        checkFileName(internalFileName);
        try {
            if (!copyFile(staticContentBucket, internalFileName, trashBucket, internalFileName)) {
                throw new FileStorageException("Move of '{}' to trash failed due to internal AWS failure",
                        internalFileName);
            }
            s3client.deleteObject(DeleteObjectRequest.builder()
                    .bucket(staticContentBucket)
                    .key(internalFileName)
                    .build());
        } catch (SdkException e) {
            throw new FileStorageException("Could not move file '" + internalFileName + "' to trash: " + e, e);
        }
    }

    private boolean copyFile(String sourceBucket, String sourceFileName, String targetBucket, String targetFileName) {

        CopyObjectRequest copyObjectRequest = CopyObjectRequest.builder()
                .sourceBucket(sourceBucket)
                .sourceKey(sourceFileName)
                .destinationBucket(targetBucket)
                .destinationKey(targetFileName)
                .metadataDirective(MetadataDirective.COPY)
                //the existing metadata is kept, except for the encryption
                .serverSideEncryption(ServerSideEncryption.AES256)
                .build();

        return s3client.copyObject(copyObjectRequest) != null;
    }

    @Override
    public void deleteFile(String internalFileName) throws FileStorageException {
        checkFileName(internalFileName);
        try {
            s3client.deleteObject(DeleteObjectRequest.builder()
                    .bucket(staticContentBucket)
                    .key(internalFileName)
                    .build());
        } catch (SdkException e) {
            throw new FileStorageException("Failed to delete file '" + internalFileName + "' from S3: " + e, e);
        }
    }

    private void checkFileName(String fileName) throws FileStorageException {
        if (StringUtils.isEmpty(fileName)) {
            throw new FileStorageException("File name can not be empty");
        }
    }

    @Override
    public Duration getAge(final String internalFileName) {
        try {
            HeadObjectResponse response = s3client.headObject(HeadObjectRequest.builder()
                    .bucket(staticContentBucket)
                    .key(internalFileName)
                    .build());

            return Duration.between(response.lastModified(), Instant.now());
        } catch (SdkException e) {
            throw new FileStorageException("Failed to get age of file " + internalFileName, e);
        }
    }

}
