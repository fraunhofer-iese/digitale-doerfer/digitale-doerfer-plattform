/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions;

import java.util.List;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class FileItemCannotBeDeletedException extends BadRequestException {

    private static final long serialVersionUID = 5407643923549493644L;

    public FileItemCannotBeDeletedException(String message) {
        super(message);
    }

    public FileItemCannotBeDeletedException(String message, Object... arguments) {
        super(message, arguments);
    }

    public FileItemCannotBeDeletedException(List<String> ids) {
        super("Could not delete file item with ids '{}'.", ids);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.FILE_ITEM_CANNOT_BE_DELETED;
    }

}
