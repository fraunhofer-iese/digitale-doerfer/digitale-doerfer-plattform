/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.model;

import org.springframework.lang.Nullable;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class FileOwnership {

    public static final FileOwnership UNKNOWN = new FileOwnership();

    @Nullable
    private Person owner;
    @Nullable
    private AppVariant appVariant;
    @Nullable
    private App app;

    FileOwnership(
            @Nullable Person owner,
            @Nullable AppVariant appVariant,
            @Nullable App app
    ) {
        this.owner = owner;
        this.appVariant = appVariant;

        if (app == null && appVariant != null) {
            this.app = appVariant.getApp();
        } else {
            this.app = app;
        }
    }

    public static FileOwnership of(Person owner, AppVariant appVariant) {
        return new FileOwnership(owner, appVariant, null);
    }

    public static FileOwnership of(Person owner, App app) {
        return new FileOwnership(owner, null, app);
    }

    // only immediately since DorfFunk does not add an app variant when uploading temporary media items and we do not yet have a platform app
    public static FileOwnership of(Person owner) {
        return new FileOwnership(owner, null, null);
    }

    public static FileOwnership of(AppVariant appVariant) {
        return new FileOwnership(null, appVariant, null);
    }

    public static FileOwnership of(App app) {
        return new FileOwnership(null, null, app);
    }

}
