/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.encryption.services;

import java.security.NoSuchAlgorithmException;

import de.fhg.iese.dd.platform.datamanagement.shared.encryption.exceptions.EncryptionException;

public interface IEncryptionService {

    /**
     * Generate a new key for the given algorithm with default or given length.
     *
     * @param algorithm the algorithm to create the key for
     * @param keySize   if null the default size as defined by the algorithm is taken
     *
     * @return a generated key for the algorithm and length
     *
     * @throws NoSuchAlgorithmException if there is no algorithm implementation available
     */
    String generateKey(String algorithm, Integer keySize) throws NoSuchAlgorithmException;

    /**
     * Encrypt a message using the current configured algorithm and key.
     * <p>
     * Empty strings are returned as null.
     *
     * @param message the message to encrypt
     *
     * @return the encrypted message that can NOT be used in URLs
     *
     * @throws EncryptionException if something went wrong during encryption
     */
    String encrypt(String message) throws EncryptionException;

    /**
     * Encrypt a message using the current configured external algorithm and key. The encrypted message is fully URL
     * safe.
     * <p>
     * Empty strings are returned as null.
     *
     * @param message the message to encrypt
     *
     * @return the encrypted message that can be used in URLs
     *
     * @throws EncryptionException if something went wrong during encryption
     */
    String encryptForExternalUse(String message) throws EncryptionException;

    /**
     * Decrypt a message using the current or the previously configured algorithms and keys. If no key identifier can be
     * found it returns the plain encryptedMessage. This is required to deal with unencrypted legacy values.
     *
     * @param encryptedMessage the encrypted message to decrypt.
     *
     * @return the unencrypted message
     *
     * @throws EncryptionException if something went wrong during decryption
     */
    String decrypt(String encryptedMessage) throws EncryptionException;

    /**
     * Decrypt a message using the allowed configured external algorithms and keys. This is more strict than {@link
     * #decrypt(String)} and does not allow unencrypted strings.
     * <p>
     * Only messages encrypted from {@link #encryptForExternalUse(String)} can be decrypted.
     *
     * @param encryptedMessage the encrypted message to decrypt.
     *
     * @return the unencrypted message
     *
     * @throws EncryptionException if something went wrong during decryption
     */
    String decryptFromExternalUse(String encryptedMessage) throws EncryptionException;

    /**
     * Encrypts an arbitrary object by serializing it to binary JSON and then encrypting the resulting string via {@link
     * #encryptForExternalUse(String)}. The resulting encrypted String is NOT intended to be interpreted, it can only be
     * used at {@link #decryptObjectFromExternalUse(String, Class)}.
     * <p>
     * The encrypted string is fully URL safe.
     *
     * @param o the object to encrypt
     *
     * @return the encrypted string representation of the object that can be used in URLs
     *
     * @throws EncryptionException if the object cannot be serialized or encrypted
     */
    String encryptObjectForExternalUse(Object o) throws EncryptionException;

    /**
     * Decrypts an object that has been encrypted via {@link #encryptObjectForExternalUse(Object)}
     *
     * @param encryptedObject the encrypted string representation of the object
     * @param returnType      the expected type of the object
     * @param <C>             the expected type of the object
     *
     * @return the decrypted object
     *
     * @throws EncryptionException if the decryption failed
     */
    <C> C decryptObjectFromExternalUse(String encryptedObject, Class<C> returnType) throws EncryptionException;

}
