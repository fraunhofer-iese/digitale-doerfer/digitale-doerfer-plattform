/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework;

import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.elasticsearch.ElasticsearchProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.data.elasticsearch.core.RefreshPolicy;
import org.springframework.lang.NonNull;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.shared.config.AWSConfig;
import lombok.extern.log4j.Log4j2;

@Configuration
@Log4j2
public class SearchConfiguration extends AbstractElasticsearchConfiguration {

    @Autowired
    private ElasticsearchProperties config;

    @Autowired
    protected AWSConfig awsConfig;

    @NonNull
    @Override
    public RestHighLevelClient elasticsearchClient() {

        if (CollectionUtils.isEmpty(config.getUris()) || config.getUris().size() != 1) {
            throw new RuntimeException("No or too many elasticsearch uris defined");
        }
        final String uriString = config.getUris().get(0);
        //this is a magic string used to prevent the elasticsearch client being activated when it is not required
        if ("deactivated".equals(uriString)) {
            return RestClients.create(ClientConfiguration.builder()
                    .connectedToLocalhost()
                    .withSocketTimeout(1)
                    .build())
                    .rest();
        }

        final URL url;
        try {
            url = new URL(uriString);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Invalid URL", e);
        }
        final InetSocketAddress address =
                new InetSocketAddress(url.getHost(), url.getPort() == -1 ? url.getDefaultPort() : url.getPort());

        final ClientConfiguration.MaybeSecureClientConfigurationBuilder preBuilder = ClientConfiguration.builder()
                .connectedTo(address);
        if (StringUtils.contains(uriString, "https")) {
            preBuilder.usingSsl();
        }
        final ClientConfiguration.TerminalClientConfigurationBuilder clientConfigurationBuilder = preBuilder
                .withBasicAuth(config.getUsername(), config.getPassword())
                // when updating: withClientConfigurer(RestClients.RestClientConfigurationCallback.from(this::configureAWS))
                .withHttpClientConfigurer(this::configureAWS)
                .withSocketTimeout(Duration.ofSeconds(30))
                .withConnectTimeout(Duration.ofSeconds(5));

        final ClientConfiguration clientConfiguration = clientConfigurationBuilder.build();
        log.info("Creating rest elasticsearch client pointing to {}", clientConfiguration.getEndpoints());
        return RestClients.create(clientConfiguration).rest();
    }

    @Override
    protected RefreshPolicy refreshPolicy() {
        //this causes the requests to wait until the actual update of the index is done
        return RefreshPolicy.WAIT_UNTIL;
    }

    private HttpAsyncClientBuilder configureAWS(HttpAsyncClientBuilder clientBuilder) {

        if (awsConfig.getElasticsearch() != null &&
                StringUtils.isNotBlank(awsConfig.getElasticsearch().getUserAgentKey())) {
            // The user agent header is used as an api key for an additional layer of security.
            // It is configured in the access policy in elasticsearch at AWS
            clientBuilder.setUserAgent("DD-Search " + awsConfig.getElasticsearch().getUserAgentKey());
        }
        return clientBuilder;
    }

}
