/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.waiting.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Table(indexes = {
        @Index(columnList = "deadline"),
})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WaitingDeadline extends BaseEntity {

    @Column
    private String waitingEventName;

    @Column
    private long deadline;

    @Column
    private long previousDeadline;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String eventAttributes;

    public WaitingDeadline(String waitingEventName, long deadline) {
        super();
        this.waitingEventName = waitingEventName;
        this.deadline = deadline;
    }

    @Override
    public String toString() {
        return "WaitingDeadline [id=" +
                id +
                ", waitingEventName=" +
                waitingEventName +
                ", deadline=" +
                deadline +
                ", eventAttributes=" +
                eventAttributes +
                "]";
    }

}
