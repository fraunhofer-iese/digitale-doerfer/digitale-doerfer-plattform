/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.services;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BaseRuntimeException;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.jvm.ExecutorServiceMetrics;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Service
@Log4j2
@Profile({"!test"})
class ParallelismService implements IParallelismService {

    private static final String METRIC_NAME_INTERNAL_BLOCKABLE_POOL = "internal-blockable-pool";
    private ExecutorService blockableExecutor;
    @Autowired
    private TaskScheduler taskScheduler;
    @Autowired
    private MeterRegistry meterRegistry;

    @PostConstruct
    private void initialize() {
        final int availableCores = Runtime.getRuntime().availableProcessors();
        //this is just a guess about a useful number of threads
        int minThreads = availableCores * 4;
        int maxThreads = availableCores * 16;
        int queueSize = maxThreads * 2;
        //expected workload: multiple tasks for every incoming request
        final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(minThreads, maxThreads,
                120L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(queueSize),
                new CustomizableThreadFactory("internal-"));
        //if the queue is full we want the caller to run the tasks, so that the request is served by the request thread
        threadPoolExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        this.blockableExecutor =
                ExecutorServiceMetrics.monitor(meterRegistry, threadPoolExecutor, METRIC_NAME_INTERNAL_BLOCKABLE_POOL,
                        METRIC_NAME_INTERNAL_BLOCKABLE_POOL);
        log.debug("Found {} cores: Using [{}, {}] threads and queue size {} in parallel request processing pool",
                availableCores, minThreads, maxThreads, queueSize);
    }

    @Override
    public ExecutorService getBlockableExecutor() {
        return blockableExecutor;
    }

    @Override
    public void submitToBlockableExecutorAndWaitForCompletion(Collection<Runnable> runnables, Duration timeout) {

        if (CollectionUtils.isEmpty(runnables)) {
            return;
        }
        final List<? extends Future<?>> futures = runnables.stream()
                .map(blockableExecutor::submit)
                .collect(Collectors.toList());
        for (Future<?> future : futures) {
            try {
                future.get(timeout.toMillis(), TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } catch (ExecutionException | TimeoutException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public <T> List<T> submitToBlockableExecutorAndWaitForResults(Collection<Callable<T>> callables,
            Duration timeout) {

        if (CollectionUtils.isEmpty(callables)) {
            return Collections.emptyList();
        }
        final List<Future<T>> futures = callables.stream()
                .map(blockableExecutor::submit)
                .toList();
        List<T> results = new ArrayList<>(futures.size());
        for (Future<T> future : futures) {
            try {
                results.add(future.get(timeout.toMillis(), TimeUnit.MILLISECONDS));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } catch (ExecutionException | TimeoutException e) {
                Throwable cause = e.getCause();
                if (cause instanceof BaseRuntimeException) {
                    throw (BaseRuntimeException) cause;
                } else {
                    throw new RuntimeException(e);
                }
            }
        }
        return results;
    }

    @Override
    public <T> ComputationOutcomes<T> submitToBlockableExecutorAndWaitForComputationOutcome(Collection<Callable<T>> callables,
                                                                                            Duration timeout) {

        if (CollectionUtils.isEmpty(callables)) {
            return new ComputationOutcomes<>(Collections.emptyList());
        }
        final List<Future<T>> futures = callables.stream()
                .map(blockableExecutor::submit)
                .toList();
        return getFutureComputationOutcome(futures, timeout);
    }

    @Override
    public <T> ComputationOutcomes<T> getFutureComputationOutcome(List<? extends Future<T>> futures, Duration timeout) {

        List<ComputationOutcome<T>> computationOutcomes = new ArrayList<>(futures.size());
        for (Future<T> future : futures) {
            try {
                computationOutcomes.add(
                        new ComputationOutcome<>(future.get(timeout.toMillis(), TimeUnit.MILLISECONDS), null));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } catch (ExecutionException | TimeoutException e) {
                Throwable cause = e.getCause();
                computationOutcomes.add(new ComputationOutcome<>(null, cause));
            }
        }
        return new ComputationOutcomes<>(computationOutcomes);
    }

    @Override
    public ScheduledFuture<?> scheduleOneTimeTask(Runnable task, Instant startTime) {

        return taskScheduler.schedule(() -> blockableExecutor.submit(task), startTime);
    }

    @PreDestroy
    private void shutDownExecutor() {
        if (blockableExecutor != null) {
            blockableExecutor.shutdown();
            try {
                blockableExecutor.awaitTermination(20, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            blockableExecutor.shutdownNow();
            blockableExecutor = null;
        }
    }

}
