/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.participants.shop.model;

import java.time.DayOfWeek;

import javax.persistence.Column;
import javax.persistence.Entity;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OpeningHoursEntry extends BaseEntity {

    @Column
    private DayOfWeek weekday;

    @Column
    private long fromTime;

    @Column
    private long toTime;

    public OpeningHoursEntry(DayOfWeek weekday) {
        super();
        this.weekday = weekday;
        this.fromTime = 0;
        this.toTime = 0;
    }

    public OpeningHoursEntry withFromHours(int fromHour){
        this.fromTime += ((long)fromHour) * 60 * 60 * 1000;
        return this;
    }

    public OpeningHoursEntry withToHours(int toHour){
        this.toTime += ((long)toHour) * 60 * 60 * 1000;
        return this;
    }

    public OpeningHoursEntry withFromMinutes(int fromMinutes){
        this.fromTime += ((long)fromMinutes) * 60 * 1000;
        return this;
    }

    public OpeningHoursEntry withToMinutes(int toMinutes){
        this.toTime += ((long)toMinutes) * 60 * 1000;
        return this;
    }

    /**
     * Sets the ID to a constant ID, based on parent.getId and weekday, from and to
     *
     * @param parent
     * @return
     */
    public OpeningHoursEntry withConstantId(BaseEntity parent){
        generateConstantId(parent.getId(), weekday.name(), Long.toString(fromTime), Long.toString(toTime));
        return this;
    }

    @Override
    public String toString() {
        return "OpeningHoursEntry [weekday=" +
                weekday +
                ", fromTime=" +
                fromTime +
                ", toTime=" +
                toTime +
                "]";
    }

}
