/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework;

import com.google.common.base.CaseFormat;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.boot.model.naming.*;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * Naming strategy for hibernate mapping, using different naming strategies.
 */
@Component
@Log4j2
public class DBSchemaNamingStrategy extends SpringImplicitNamingStrategy {

    private static final long serialVersionUID = -5918135276965587389L;

    @Override
    protected String transformEntityName(EntityNaming entityNaming) {

        NamingStrategyName strategy = NamingStrategyName.MODULENAME_CLASSNAME;
        try {
            Class<?> aClass = Class.forName(entityNaming.getClassName());
            NamingStrategy namingStrategy = aClass.getAnnotation(NamingStrategy.class);
            if (namingStrategy != null) {
                strategy = namingStrategy.strategy();
            }
        } catch (ClassNotFoundException e) {
            log.error("Could not find class {} of entity ({})", entityNaming.getClassName(),
                    entityNaming.getEntityName());
        }
        switch (strategy) {
            case CLASSNAME:
                return super.transformEntityName(entityNaming);
            case MODULENAME_CLASSNAME:
                return getModuleName(entityNaming.getClassName()) + "_" + super.transformEntityName(entityNaming);
            default:
                throw new IllegalStateException("Unexpected naming strategy: " + strategy);
        }
    }

    protected String getModuleName(String className){
        //find out module name by using "de.fhg.iese.dd.platform.data._shared_.app.model.EntityA
        String[] packageNames = StringUtils.split(className, '.');
        if(packageNames.length < 7){
            return "unknown";
        }
        return packageNames[6];
    }

    @Override
    public Identifier determineForeignKeyName(ImplicitForeignKeyNameSource source) {
        return generateIdentifier("FK", source);
    }

    @Override
    public Identifier determineIndexName(ImplicitIndexNameSource source) {
        return generateIdentifier("IX", source);
    }

    @Override
    public Identifier determineUniqueKeyName(ImplicitUniqueKeyNameSource source) {
        return generateIdentifier("UK", source);
    }

    private Identifier generateIdentifier(String prefix, ImplicitConstraintNameSource source) {

        int maxIdentifierLength = 64;
        int hashCodeLength = 10;
        int prefixLength = StringUtils.length(prefix);

        int tableNameLengthMax = maxReadableLength(source.getTableName().getText()) + 1;
        int tableNameLengthMin = Math.min(20, tableNameLengthMax);
        int tableNameLength = tableNameLengthMax;

        String hashCode;

        String referencedTableNameOriginal = "";
        int referencedTableNameLengthMax = 0;
        int referencedTableNameLengthMin = 0;
        int referencedTableNameLength = 0;

        if (source instanceof ImplicitForeignKeyNameSource sourceFK) {
            referencedTableNameOriginal = sourceFK.getReferencedTableName().getText();

            referencedTableNameLengthMax = maxReadableLength(referencedTableNameOriginal) + 1;
            referencedTableNameLengthMin = Math.min(15, referencedTableNameLengthMax);
            referencedTableNameLength = referencedTableNameLengthMax;

            hashCode = StringUtils.truncate(NamingHelper.INSTANCE.generateHashedFkName(
                "",
                source.getTableName(),
                sourceFK.getReferencedTableName(),
                source.getColumnNames()
            ), hashCodeLength);

        }else{
            hashCode = StringUtils.truncate(NamingHelper.INSTANCE.generateHashedConstraintName(
                "",
                source.getTableName(),
                source.getColumnNames()
            ), hashCodeLength);
        }

        //get the maximum length of all columns without abbreviating, only transforming them to camel case
        int columnNameLengthMax = source.getColumnNames().stream()
                //+1 because of the "_" that will be added later on for each column
                .mapToInt(i -> maxReadableLength(i.getText()) + 1)
                .sum();
        int columnNameLength = columnNameLengthMax;

        if((prefixLength + tableNameLength + columnNameLength + referencedTableNameLength + hashCodeLength) > maxIdentifierLength){
            //we can not just add all the items together, one needs to be shortened, start with the table name
            tableNameLength = tableNameLengthMin;
            if((prefixLength + tableNameLength + columnNameLength + referencedTableNameLength + hashCodeLength) > maxIdentifierLength){
                //we need to shorten again, referenced table
                referencedTableNameLength = referencedTableNameLengthMin;
                if((prefixLength + tableNameLength + columnNameLength + referencedTableNameLength + hashCodeLength) > maxIdentifierLength){
                    //we need to shorten again, column names take the rest or 0
                    columnNameLength = Math.max(0,
                        maxIdentifierLength - (prefixLength + tableNameLength +  referencedTableNameLength + hashCodeLength));
                }
            }
        }

        String tableName = abbreviateReadable(source.getTableName().getText(), tableNameLength - 1) + "_";
        String referencedTableName = "";
        if(! StringUtils.isEmpty(referencedTableNameOriginal)){
            referencedTableName = abbreviateReadable(referencedTableNameOriginal, referencedTableNameLength - 1) + "_";
        }

        String columnNames;
        if(columnNameLength == columnNameLengthMax){
            //no need to shorten the columns
            columnNames = source.getColumnNames().stream()
                    .sorted(Comparator.comparing(Identifier::getText))
                    //again we need to take core of the "_" at the end, which is added
                    .map(i -> abbreviateReadable(i.getText(), i.getText().length()))
                    .collect(Collectors.joining("_", "", "_"));

        }else {
            int numColumns = source.getColumnNames().size();
            int columnNameLengthSingle = Math.max(0, Math.floorDiv(columnNameLength, numColumns));
            columnNames = source.getColumnNames().stream()
                    .sorted(Comparator.comparing(Identifier::getText))
                    //again we need to take core of the "_" at the end, which is added
                    .map(i -> abbreviateReadable(i.getText(), columnNameLengthSingle - 1))
                    .collect(Collectors.joining("_", "", "_"));
        }

        String identifier = prefix + tableName + columnNames + referencedTableName + hashCode;

        return toIdentifier( identifier, source.getBuildingContext());
    }

    /**
     * Abbreviate an identifier in camel case and try to use as many
     * sub-identifiers as possible in the given length.
     *
     * @param identifier
     *            the identifier to abbreviate in camel case
     * @param length
     *            max length of the resulting abbreviated identifier
     * @return
     */
    private String abbreviateReadable(String identifier, int length) {

        identifier = StringUtils.removeEnd(identifier, "_id");

        String simpleAbbreviation = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, identifier);
        if (length >= simpleAbbreviation.length() ){
            return simpleAbbreviation;
        }

        String[] subIdentifiers = StringUtils.split(identifier, "_");
        int numSubIdentifiers = subIdentifiers.length;

        int avgLengthSubIdentifier = Math.floorDiv(length, numSubIdentifiers);

        //the division rest is unused
        int unusedChars = length % numSubIdentifiers;

        //find out how many chars are unused due to too short sub-identifiers
        unusedChars += Arrays.stream(subIdentifiers)
                .mapToInt(i -> Math.max(0, avgLengthSubIdentifier - i.length()))
                .sum();

        StringBuilder result = new StringBuilder();

        for (String singleIdentifier : subIdentifiers) {
            if (unusedChars > 0 && singleIdentifier.length() > avgLengthSubIdentifier) {
                //we have unused characters and the identifier is long enough to extend it
                //add the unused characters at the beginning, since this is most likely the most important identifier
                int extensionChars = Math.min(unusedChars, singleIdentifier.length() - avgLengthSubIdentifier);
                unusedChars -= extensionChars;
                result.append(StringUtils.truncate(StringUtils.capitalize(singleIdentifier),
                        avgLengthSubIdentifier + extensionChars));
            } else {
                result.append(StringUtils.truncate(StringUtils.capitalize(singleIdentifier), avgLengthSubIdentifier));
            }
        }

        return result.toString();
    }

    private int maxReadableLength(String identifier){
        return StringUtils.remove(StringUtils.removeEnd(identifier, "_id"), '_').length();
    }

}
