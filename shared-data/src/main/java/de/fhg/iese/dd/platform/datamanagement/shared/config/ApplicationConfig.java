/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.config;

import java.time.ZoneId;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.MediaConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.security.config.OauthConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.security.config.OauthManagementConfig;
import lombok.Getter;
import lombok.Setter;

@Validated
@Configuration
@ConfigurationProperties(prefix = "dd-platform")
@Getter
@Setter
public class ApplicationConfig {

    @NestedConfigurationProperty
    private AWSConfig aws;
    @NestedConfigurationProperty
    private OauthConfig oauth;
    @NestedConfigurationProperty
    private OauthManagementConfig oauthManagement;
    @NestedConfigurationProperty
    private DefaultSuperAdminUserConfig defaultSuperAdminUser;
    @NestedConfigurationProperty
    private MediaConfig media;
    @NestedConfigurationProperty
    private DataInitConfig dataInitConfig;
    @NestedConfigurationProperty
    private DataPrivacyConfig dataPrivacyConfig;
    @NotNull
    private VersionInfo versionInfo;
    @NotNull
    private EMail email;
    @NotNull
    private TwilioSMS twilioSms;
    @NotNull
    private Google google;
    @NotNull
    private DStation dStation; //TODO move to logistics
    @NotNull
    private String localTimeZone;
    @NotNull
    private int geoAreaLookupRadiusInKm;
    /**
     * Machine readable identifier of the environment. It can use $nodeId$ as variable to ensure the identifier is
     * unique for shared environments, like "local"
     */
    @NotBlank
    private String environmentIdentifier;
    @NotBlank
    private String environmentFullName;
    private String environmentDescription;
    private String environmentInfoColor;

    private TestPersons testPersons;

    public ZoneId getLocalZoneId() {
        return ZoneId.of(getLocalTimeZone());
    }

    @Getter
    @Setter
    public static class VersionInfo {

        private String pomVersion;
        private String majorVersion;
        private String minorVersion;

        /**
         * @return Unique identifier of the platform version, consisting of major + minor version
         */
        public String getPlatformVersion() {
            return majorVersion + ", " + minorVersion;
        }

    }

    @Getter
    @Setter
    public static class EMail {

        private boolean enabled;
        private String address;
        //if true, the environment full name is added to the mails
        private boolean includeEnvironmentInfo = false;
        //if true, the mails are stored and a link is posted in the team chat
        private boolean emailStorageEnabled = false;
        @NotBlank
        private String emailStoragePath;
        private String emailImageBaseURL;
        @Min(value = 0, message = "Only positive number of days possible")
        private int maxRetentionEmailStorageInDays;

    }

    @Getter
    @Setter
    public static class TwilioSMS {

        private boolean enabled;
        private String accountSid;
        private String authToken;
        private String senderId;

    }

    @Getter
    @Setter
    public static class Google {

        private String apiKey;

    }

    @Getter
    @Setter
    public static class DStation {

        @NotBlank
        private String apiKey;

    }

    @Getter
    @Setter
    public static class TestPersons {

        private String demoPersonDefaultPassword;
        private String demoPersonExtendedDefaultPassword;
        private String shopDefaultPassword;

    }

}
