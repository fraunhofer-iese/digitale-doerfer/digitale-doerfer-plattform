/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.services;

import java.util.Set;

import org.springframework.orm.jpa.vendor.Database;

public interface IEnvironmentService {

    /**
     * A machine readable identifier of the current environment.
     * <p/>
     * Configured via application config
     *
     * @return always a String, never null
     */
    String getEnvironmentIdentifier();

    /**
     * A human readable name of the current environment.
     * <p/>
     * Configured via application config
     *
     * @return always a String, never null
     */
    String getEnvironmentFullName();

    /**
     * A human readable description of the current environment.
     * <p/>
     * Configured via application config
     *
     * @return always a String, never null
     */
    String getEnvironmentDescription();

    /**
     * A valid HTML color for easier recognition of the environment.
     * <p/>
     * Configured via application config
     *
     * @return always a color, never null
     */
    String getEnvironmentInfoColor();

    /**
     * All current active profiles.
     *
     * @param excludeUnderscoreProfiles if true all profiles starting with <code>_</code> are excluded.
     *
     * @return always a set, never null
     */
    Set<String> getActiveProfiles(boolean excludeUnderscoreProfiles);

    /**
     * A human readable representation of the current active profiles.
     *
     * @param excludeUnderscoreProfiles if true all profiles starting with <code>_</code> are excluded.
     *
     * @return always a String, never null
     */
    String getActiveProfileInfo(boolean excludeUnderscoreProfiles);

    /**
     * Identifier of this computing node. Might not be globally unique.
     * <p/>
     * On AWS this will be the instance id.
     *
     * @return always a String, never null
     */
    String getNodeIdentifier();

    /**
     * Unique identifier of this running instance. It combines the {@link #getNodeIdentifier()} with the precise startup
     * time of the application.
     * <p/>
     * If multiple instances get started on the same node at exactly the same time this identifier is not unique
     * anymore. Since this is very unlikely no measures are taken so far to prevent it.
     *
     * @return always a String, never null
     */
    String getInstanceIdentifier();

    /**
     * The currently active database vendor.
     * <p/>
     * This can differ from the current active (simulated) database, since H2 simulates MySQL
     */
    Database getActiveDatabaseVendor();

}
