/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * A {@link MediaItem} temporarily uploaded by a user. When a new entity is created by the user, this temporary image is
 * just referenced by id. If it is not used after a while it gets automatically removed.
 */
@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@DiscriminatorValue("0")
public class TemporaryMediaItem extends TemporaryBaseFileItem<MediaItem> {

    @SuppressWarnings("JpaAttributeTypeInspection")
    public MediaItem getMediaItem() {
        return getFileItem();
    }

    public void setMediaItem(MediaItem mediaItem) {
        setFileItem(mediaItem);
    }

}
