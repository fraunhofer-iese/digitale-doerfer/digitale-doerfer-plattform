/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2022 Axel Wickenkamp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.FileTime;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.FileStorageConfig;
import lombok.extern.log4j.Log4j2;

/**
 * Local file storage based on {@link IFileStorage}
 */
@Component
@Profile({"local"})
@Log4j2
public class LocalFileStorage extends BaseFileStorage {

    static final String SYSTEM_PROPERTY_LOCAL_FILE_STORAGE_FOLDER = "dd.filestorage.local";

    private final Path defaultStorageLocation;
    private final Path staticStorageLocation;
    private final Path trashStorageLocation;

    @Autowired
    public LocalFileStorage(FileStorageConfig config) {
        super(config);
        final Path localPlatformDir = Paths.get(getLocalPlatformDir()).resolve("dd-platform");
        defaultStorageLocation = localPlatformDir.resolve("dd-default-data");
        staticStorageLocation = localPlatformDir.resolve("dd-static-data");
        trashStorageLocation = localPlatformDir.resolve("dd-trash-data");

        log.info("Using LocalFileStorage as file storage");
        log.info("defaultStorageLocation: '{}'", defaultStorageLocation);
        log.info("staticStorageLocation: '{}'", staticStorageLocation);
        log.info("trashStorageLocation: '{}'", trashStorageLocation);
        log.info("externalBaseUrl: '{}'", getExternalBaseUrl());
    }

    private static String getLocalPlatformDir(){
        String env = System.getenv(SYSTEM_PROPERTY_LOCAL_FILE_STORAGE_FOLDER);
        if(StringUtils.isNotEmpty(env)){
            return env;
        }
        String prop = System.getProperty(SYSTEM_PROPERTY_LOCAL_FILE_STORAGE_FOLDER);
        if (StringUtils.isNotEmpty(prop)) {
            return prop;
        }
        String userHome = System.getProperty("user.home");
        if (StringUtils.isNotEmpty(userHome)) {
            return userHome;
        }
        return System.getProperty("java.io.tmpdir");
    }

    @Override
    public void saveFile(byte[] content, String mimeType, String internalFileName) throws FileStorageException {
        Path fileLocation = staticStorageLocation.resolve(internalFileName);

        Path parent = fileLocation.getParent();
        if (parent == null) {
            throw new InvalidPathException(internalFileName, "Could not determine parent");
        }
        try {
            Files.createDirectories(parent);
        } catch (IOException e) {
            throw new FileStorageException("Could not create parent directory", e);
        }

        try (OutputStream outputStream = Files.newOutputStream(fileLocation)) {
            outputStream.write(content);
        } catch (IOException e) {
            throw new FileStorageException("Could not save file locally", e);
        }
    }

    @Override
    public void copyFile(String internalFileNameSource, String internalFileNameTarget) throws FileStorageException {
        Path source = staticStorageLocation.resolve(internalFileNameSource);
        Path target = staticStorageLocation.resolve(internalFileNameTarget);
        Path parent = target.getParent();
        try {
            if (parent == null) {
                throw new InvalidPathException(internalFileNameTarget, "Could not determine parent");
            }
            Files.createDirectories(parent);
            Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException | InvalidPathException e) {
            throw new FileStorageException(
                    "Copy of file '" + internalFileNameSource + "' to '" + internalFileNameTarget + "' failed: " +
                            e.getMessage(), e);
        }
    }

    @Override
    public InputStream getFile(String internalFileName) throws FileStorageException {
        Path fileLocation = staticStorageLocation.resolve(internalFileName);

        try {
            return Files.newInputStream(fileLocation, StandardOpenOption.READ);
        } catch (IOException e) {
            throw new FileStorageException("Getting file " + internalFileName + " failed", e);
        }
    }

    @Override
    public boolean fileExistsDefault(String defaultInternalFileName) {
        try {
            return Files.exists(defaultStorageLocation.resolve(defaultInternalFileName));
        } catch (InvalidPathException e) {
            log.error("Invalid path while checking file \"" + defaultInternalFileName + "\": " + e.getMessage());
            return false;
        }
    }

    @Override
    public void copyFileFromDefault(String defaultInternalFileName, String internalFileName)
            throws FileStorageException {

        Path source = defaultStorageLocation.resolve(defaultInternalFileName);
        Path target = staticStorageLocation.resolve(internalFileName);
        Path parent = target.getParent();
        try {
            if (parent == null) {
                throw new InvalidPathException(internalFileName, "Could not determine parent");
            }
            Files.createDirectories(parent);
            Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException | InvalidPathException e) {
            throw new FileStorageException("Copy of default file "+defaultInternalFileName+" to "+internalFileName+" failed: "+e.getMessage(), e);
        }
    }

    @Override
    public InputStream getFileFromDefault(String defaultInternalFileName) throws FileStorageException {
        Path fileLocation = defaultStorageLocation.resolve(defaultInternalFileName);

        try {
            return Files.newInputStream(fileLocation, StandardOpenOption.READ);
        } catch (IOException e) {
            throw new FileStorageException("Getting default file "+defaultInternalFileName+" failed",e);
        }
    }

    @Override
    public boolean fileExists(String internalFileName) {
        try {
            return Files.exists(staticStorageLocation.resolve(internalFileName));
        } catch (InvalidPathException e) {
            log.error("Invalid path while checking file \""+internalFileName+"\": "+e.getMessage());
            return false;
        }
    }

    @Override
    public List<String> findAll(String folderName) {
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(staticStorageLocation.resolve(folderName))) {
            List<String> fileNames = new ArrayList<>();
            for (Path path : directoryStream) {
                //we only want the short relative names
                fileNames.add(staticStorageLocation.relativize(path).toString());
            }
            return fileNames;
        } catch (IOException e) {
            throw new FileStorageException("Error while retrieving all files", e);
        }
    }

    @Override
    public void moveToTrash(String internalFileName) {
        Path fileLocation = staticStorageLocation.resolve(internalFileName);
        Path trashLocation = trashStorageLocation.resolve(internalFileName);
        Path parent = trashLocation.getParent();
        try {
            if(parent == null){
                throw new InvalidPathException(internalFileName, "Could not determine parent");
            }
            Files.createDirectories(parent);
            Files.move(fileLocation, trashLocation, StandardCopyOption.REPLACE_EXISTING);
            log.info("Moved file {} to trash {} ", internalFileName, trashLocation);
        } catch (IOException e) {
            throw new FileStorageException("Could not move file to trash", e);
        }
    }

    @Override
    public void deleteFile(String internalFileName) {
        Path fileLocation = staticStorageLocation.resolve(internalFileName);
        try {
            Files.deleteIfExists(fileLocation);
        } catch (IOException e) {
            throw new FileStorageException("Error while deleting file " + internalFileName, e);
        }
    }

    @Override
    public String getExternalUrl(String internalFileName) {
        return getExternalBaseUrl() + staticStorageLocation.resolve(internalFileName).toAbsolutePath();
    }

    @Override
    public String getInternalFileName(String externalUrl) {
        //we remove the parts we add in getExternalUrl: the base url and the absolute path
        return staticStorageLocation.relativize(Paths.get(
                StringUtils.removeStart(externalUrl, getExternalBaseUrl()))).toString();
    }

    @Override
    public Duration getAge(final String internalFileName) {
        try {
            final FileTime lastModifiedTime = Files.getLastModifiedTime(Paths.get(internalFileName));
            return Duration.between(lastModifiedTime.toInstant(), Instant.now());
        } catch (IOException e) {
            log.error("Failed to get last modified time of {}", internalFileName);
            return Duration.ZERO;
        }
    }

}
