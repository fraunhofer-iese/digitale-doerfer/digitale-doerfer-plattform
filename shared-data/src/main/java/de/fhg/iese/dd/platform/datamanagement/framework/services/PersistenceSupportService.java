/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.services;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.interceptor.TransactionAttribute;
import org.springframework.transaction.support.TransactionTemplate;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
class PersistenceSupportService implements IPersistenceSupportService {

    private static final String REPOSITORY_NAME_MYSQL = "MySQLRepository";

    private static final String REPOSITORY_NAME_DEFAULT = "Repository";

    private final Map<Class<? extends BaseEntity>, Optional<JpaRepository<? extends BaseEntity, String>>>
            repositoryByClass = new ConcurrentHashMap<>();

    @Autowired
    private ApplicationContext context;
    @Autowired
    protected PlatformTransactionManager transactionManager;

    @Override
    public BaseEntity findById(String fullQualifiedClassName, String entityId) {

        Objects.requireNonNull(fullQualifiedClassName);
        Objects.requireNonNull(entityId);

        final Class<?> entityClassRaw;
        try {
            entityClassRaw = Class.forName(fullQualifiedClassName);
        } catch (ClassNotFoundException e) {
            log.error("Could not load class '{}'", fullQualifiedClassName);
            return null;
        }
        if (!BaseEntity.class.isAssignableFrom(entityClassRaw)) {
            return null;
        } else {
            @SuppressWarnings("unchecked")
            Class<? extends BaseEntity> entityClass = (Class<? extends BaseEntity>) entityClassRaw;
            return findById(entityClass, entityId);
        }
    }

    @Override
    public <T extends BaseEntity> T findById(Class<? extends T> entityClass, String entityId) {

        final Optional<JpaRepository<? extends BaseEntity, String>> repository = getRepository(entityClass);
        if (repository.isEmpty()) {
            log.error("Could not find repository for entities of type {}", entityClass.getName());
            return null;
        }
        Optional<? extends BaseEntity> entityOptional = repository.get().findById(entityId);
        if (entityOptional.isEmpty()) {
            log.debug("No entity of type {} with id {} found", entityClass.getName(), entityId);
            return null;
        } else {
            BaseEntity foundEntity = entityOptional.get();
            if (entityClass.isAssignableFrom(foundEntity.getClass())) {
                @SuppressWarnings("unchecked")
                T castedEntity = (T) foundEntity;
                return castedEntity;
            } else {
                return null;
            }
        }
    }

    @Override
    public <T extends BaseEntity> T refreshProxy(T entity) {
        if (entity instanceof HibernateProxy) {
            @SuppressWarnings("unchecked")
            Class<? extends T> entityClass = (Class<? extends T>) entity.getClass();
            return findById(entityClass, entity.getId());
        } else {
            return entity;
        }
    }

    @Override
    public <T> T saveAndIgnoreIntegrityViolation(
            Supplier<T> saveSupplier,
            Supplier<T> onExceptionSupplier) {

        TransactionSupport.logTransactionInfo("saveAndRetryOnDataIntegrityViolation", log);

        if (!TransactionSupport.isNoTransactionOrIsolationLevelReadUncommited()) {
            //this exception is required because of the separate transaction we use to save the entity
            throw new IllegalStateException(
                    "Method only permitted with transaction isolation level ISOLATION_READ_UNCOMMITTED");
        }

        try {
            //this needs to run within a separate transaction, to avoid causing the current transaction to fail
            //even if the transaction is not rolled back, hibernate will again try to persist the entity with the next query
            //this can only be avoided by having the save as a separate transaction that is rolled back on a failure
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_UNCOMMITTED);
            transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
            return transactionTemplate.execute(
                    (ts) -> saveSupplier.get()
            );
        } catch (DataIntegrityViolationException | ConcurrencyFailureException e) {
            //the constraint on db level prevented to save the entity, most likely due to concurrent requests
            //try to get the one that was created by the concurrent request
            log.warn("Concurrent request detected, trying to find the entity that was created in parallel.");
            return onExceptionSupplier.get();
        }
    }

    @Override
    public <T extends BaseEntity> T saveAndRetryOnDataIntegrityViolation(
            Supplier<T> saveSupplier,
            Supplier<T> onExceptionSupplier) {

        TransactionSupport.logTransactionInfo("saveAndRetryOnDataIntegrityViolation", log);

        if (!TransactionSupport.isNoTransactionOrIsolationLevelReadUncommited()) {
            //this exception is required because of the separate transaction we use to save the entity
            throw new IllegalStateException(
                    "Method only permitted with transaction isolation level ISOLATION_READ_UNCOMMITTED");
        }

        try {
            //this needs to run within a separate transaction, to avoid causing the current transaction to fail
            //even if the transaction is not rolled back, hibernate will again try to persist the entity with the next query
            //this can only be avoided by having the save as a separate transaction that is rolled back on a failure
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_UNCOMMITTED);
            transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
            return transactionTemplate.execute(
                    (ts) -> saveSupplier.get()
            );
        } catch (DataIntegrityViolationException | ConcurrencyFailureException e) {
            //the constraint on db level prevented to save the entity, most likely due to concurrent requests
            //try to get the one that was created by the concurrent request
            log.warn("Concurrent request detected, trying to find the entity that was created in parallel.");
            T concurrentCreatedEntity = onExceptionSupplier.get();
            if (concurrentCreatedEntity != null) {
                log.debug("Entity that was created in parallel could be found, returning it.");
                return concurrentCreatedEntity;
            } else {
                log.error(
                        "Entity that was created could not be found, trying to save it again. Most likely the constraint is wrong.");
                //here we just try to save it again and let the exception be thrown, since it can not be fixed
                return saveSupplier.get();
            }
        }
    }

    @Override
    public <T extends BaseEntity> T executeAndRetryOnDataIntegrityViolation(Supplier<T> saveSupplier) {

        TransactionSupport.logTransactionInfo("executeAndRetryOnDataIntegrityViolation", log);

        if (!TransactionSupport.isNoTransactionOrIsolationLevelReadUncommited()) {
            //this exception is required because of the separate transaction we use to save the entity
            throw new IllegalStateException(
                    "Method only permitted with transaction isolation level ISOLATION_READ_UNCOMMITTED");
        }

        try {
            //this needs to run within a separate transaction, to avoid causing the current transaction to fail
            //even if the transaction is not rolled back, hibernate will again try to persist the entity with the next query
            //this can only be avoided by having the save as a separate transaction that is rolled back on a failure
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_UNCOMMITTED);
            transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
            return transactionTemplate.execute(
                    (ts) -> saveSupplier.get()
            );
        } catch (DataIntegrityViolationException | ConcurrencyFailureException e) {
            //the constraint on db level prevented to save the entity, most likely due to concurrent requests
            //try to get the one that was created by the concurrent request
            log.warn("Concurrent request detected, trying to execute the action again.");
            //here we just try to execute the action again and let the exception be thrown, since it can not be fixed
            return saveSupplier.get();
        }
    }

    @Override
    public <T> T query(Supplier<T> query) {
        TransactionTemplate transaction = new TransactionTemplate(transactionManager);
        transaction.setIsolationLevel(TransactionDefinition.ISOLATION_READ_UNCOMMITTED);
        transaction.setReadOnly(true);
        return transaction.execute(status -> query.get());
    }

    @Override
    public <T> T save(Supplier<T> query) {
        TransactionTemplate transaction = new TransactionTemplate(transactionManager);
        transaction.setIsolationLevel(TransactionDefinition.ISOLATION_READ_UNCOMMITTED);
        transaction.setReadOnly(false);
        return transaction.execute(status -> query.get());
    }

    @Override
    public <P> Page<P> sliceToFakePage(Slice<P> slice) {

        int totalCount;
        if (slice.hasNext()) {
            //at least one more entity can be expected on the next slice
            totalCount = ((slice.getNumber() + 1) * slice.getSize()) + 1;
        } else {
            //items in previous slices + the items in this slice
            totalCount = (slice.getNumber() * slice.getSize()) + slice.getNumberOfElements();
        }
        return new PageImpl<>(slice.getContent(), slice.getPageable(), totalCount);
    }

    /**
     * Internal helper for accessing details of a transaction
     */
    protected static class TransactionSupport extends TransactionAspectSupport {

        private static TransactionAttribute getTransactionAttribute() {
            TransactionInfo transactionInfo = TransactionAspectSupport.currentTransactionInfo();
            if (transactionInfo == null) {
                return null;
            } else {
                return transactionInfo.getTransactionAttribute();
            }
        }

        private static boolean isNoTransactionOrIsolationLevelReadUncommited() {
            TransactionAttribute transactionAttribute = getTransactionAttribute();
            if (transactionAttribute == null) {
                return true;
            } else {
                return transactionAttribute.getIsolationLevel() ==
                        TransactionDefinition.ISOLATION_READ_UNCOMMITTED;
            }
        }

        public static void logTransactionInfo(String prefix, Logger log) {
            if (!log.isTraceEnabled()) {
                return;
            }
            TransactionAttribute transactionAttribute = getTransactionAttribute();
            if (transactionAttribute == null) {
                log.trace("No transaction");
            } else {
                log.trace("{}: transaction '{}', isolation level {}, read only {}",
                        prefix,
                        transactionAttribute.getName(),
                        isolationLevelToName(transactionAttribute.getIsolationLevel()),
                        transactionAttribute.isReadOnly());
            }
        }

        private static String isolationLevelToName(int isolationLevel) {
            for (Isolation isolation : Isolation.values()) {
                if (isolation.value() == isolationLevel) {
                    return isolation.name();
                }
            }
            return "unknown";
        }

    }

    private Optional<JpaRepository<? extends BaseEntity, String>> getRepository(
            Class<? extends BaseEntity> entityClass) {

        //if there is no repository cached, look it up from the context
        return repositoryByClass.computeIfAbsent(entityClass, this::lookupRepositoryForEntityIncludingSuperClasses);
    }

    private Optional<JpaRepository<? extends BaseEntity, String>> lookupRepositoryForEntityIncludingSuperClasses(
            Class<? extends BaseEntity> entityClass) {

        //we look up the inheritance hierarchy until we are at the BaseEntity
        for (Class<?> currentClass = entityClass;
             BaseEntity.class.isAssignableFrom(currentClass);
             currentClass = currentClass.getSuperclass()) {
            final JpaRepository<? extends BaseEntity, String> repository = lookupRepositoryForEntity(currentClass);
            if (repository != null) {
                return Optional.of(repository);
            }
        }
        return Optional.empty();
    }

    @SuppressWarnings("unchecked")
    private JpaRepository<? extends BaseEntity, String> lookupRepositoryForEntity(Class<?> entityClass) {

        //the repositories are registered by name, but with a small first letter, e.g. personRepository
        Object repositoryBean = null;

        String repositoryName = StringUtils.uncapitalize(entityClass.getSimpleName()) + REPOSITORY_NAME_DEFAULT;
        if (context.containsBean(repositoryName)) {
            repositoryBean = context.getBean(repositoryName);
        }

        if (repositoryBean == null) {
            repositoryName = StringUtils.uncapitalize(entityClass.getSimpleName()) + REPOSITORY_NAME_MYSQL;
            if (context.containsBean(repositoryName)) {
                repositoryBean = context.getBean(repositoryName);
            }
        }

        if (repositoryBean instanceof JpaRepository<?, ?>) {
            return (JpaRepository<? extends BaseEntity, String>) repositoryBean;
        }
        return null;
    }

}
