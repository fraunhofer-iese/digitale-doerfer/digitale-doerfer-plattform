/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services.init;

import java.io.InputStream;
import java.util.Collection;
import java.util.regex.Pattern;

import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;

/**
 * Internal interface to abstract from concrete implementations of accessing the data init files.
 */
interface IFileProvider {

    /**
     * Checks out all current data init files from git to local disk.
     * <p/>
     * If no repository exists, a new one is created. Otherwise the local repository is forced updated, all local
     * changes are discarded.
     *
     * @param logSummary a human readable summary
     *
     * @return human readable message about the version of the init data, if available
     *
     * @throws DataInitializationException if the git checkout failed
     */
    String checkoutInitData(LogSummary logSummary) throws DataInitializationException;

    /**
     * Loads the data init file from the given relative location. If the file is not available {@code null} is
     * returned.
     *
     * @param location the relative location of the data init file, e.g. {@code init-shared/legal/LegalText.json}.
     *                 Absolute path will be treated relative.
     *
     * @return stream to the content of the file or {@code null} if the file does not exist.
     */
    InputStream loadInitDataFile(String location);

    Collection<String> findDataInitFiles(Pattern pattern);

    /**
     * Signals that the data init is done and any resources can be cleaned up.
     */
    void cleanup(LogSummary logSummary);

}
