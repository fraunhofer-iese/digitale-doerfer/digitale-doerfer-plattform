/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.enums;

import java.util.EnumSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Accessor for a set of enum values of a {@link StorableEnum} that is stored as a bit mask.
 *
 * @param <E> the actual enum that needs to be a {@link StorableEnum}
 */
public class StorableEnumSet<E extends Enum<E> & StorableEnum> {

    private final Consumer<Integer> enumSetBitValueSetter;

    private final Supplier<Integer> enumSetBitValueGetter;

    private final Class<E> enumClass;

    /**
     * Creates a new accessor for the enum values stored as bit mask.
     *
     * @param enumSetBitValueSetter setter of the enum set bit value
     * @param enumSetBitValueGetter getter of the enum set bit value
     * @param enumClass             the actual enum
     */
    public StorableEnumSet(Consumer<Integer> enumSetBitValueSetter, Supplier<Integer> enumSetBitValueGetter,
            Class<E> enumClass) {
        this.enumSetBitValueSetter = enumSetBitValueSetter;
        this.enumSetBitValueGetter = enumSetBitValueGetter;
        this.enumClass = enumClass;
    }

    /**
     * Gets all enum values
     */
    public Set<E> getValues() {

        int enumSetBitValue = enumSetBitValueGetter.get();

        EnumSet<E> es = EnumSet.noneOf(enumClass);
        for (E value : enumClass.getEnumConstants()) {
            if (isEnumValueSet(value, enumSetBitValue)) {
                es.add(value);
            }
        }
        return es;
    }

    /**
     * Sets the enum values
     */
    public void setValues(Iterable<E> values) {

        int newEnumSetBitValue = 0;

        for (E value : values) {
            newEnumSetBitValue |= value.getBitMaskValue();
        }
        enumSetBitValueSetter.accept(newEnumSetBitValue);
    }

    /**
     * Checks if the given enum value is in the set of enum values
     */
    public boolean hasValue(E valueToCheck) {
        return isEnumValueSet(valueToCheck, enumSetBitValueGetter.get());
    }

    /**
     * Adds the given enum value to the set of enum values. Does not fail if the value is already in the set.
     */
    public void addValue(E valueToAdd) {
        int enumSetBitValue = enumSetBitValueGetter.get();
        enumSetBitValue |= valueToAdd.getBitMaskValue();
        enumSetBitValueSetter.accept(enumSetBitValue);
    }

    /**
     * Removes the given enum value from the set of enum values. Does not fail if the value is not in the set.
     */
    public void removeValue(E valueToRemove) {
        int enumSetBitValue = enumSetBitValueGetter.get();
        enumSetBitValue &= ~valueToRemove.getBitMaskValue();
        enumSetBitValueSetter.accept(enumSetBitValue);
    }

    public void clear() {
        enumSetBitValueSetter.accept(0);
    }

    private boolean isEnumValueSet(E valueToCheck, int enumSetBitValue) {

        return (enumSetBitValue & valueToCheck.getBitMaskValue()) != 0;
    }

    @Override
    public String toString() {

        return getValues().toString();
    }

    /**
     * Creates a bitmask for these enum values. Typical use cases, e being the entity, bitmask being the result of this method:
     * <ul>
     *     <li>All of: <code>not e.enumValue & bitmask == 0</code></li>
     *     <li>Any of: <code>e.enumValue & bitmask != 0</code></li>
     *     <li>None of: <code>e.enumValue & bitmask == 0</code></li>
     *     <li>Exactly same values: <code>e.enumValue == bitmask</code></li>
     * </ul>
     *
     * @param enumValues values that are relevant for the bitmask
     * @return the bitmask of these values
     */
    public static int toBitMaskValue(Iterable<? extends StorableEnum> enumValues) {

        int bitMaskValue = 0;
        for (StorableEnum enumValue : enumValues) {
            bitMaskValue |= enumValue.getBitMaskValue();
        }
        return bitMaskValue;
    }

}
