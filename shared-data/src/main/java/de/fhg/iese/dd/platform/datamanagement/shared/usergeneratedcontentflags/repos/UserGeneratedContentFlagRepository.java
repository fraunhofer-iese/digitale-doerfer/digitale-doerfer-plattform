/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Dominik Schnier, Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.results.UserGeneratedContentFlagExtendedCountByGeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.results.UserGeneratedContentFlagSimpleCountByGeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.results.UserGeneratedContentFlagSummary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserGeneratedContentFlagRepository extends JpaRepository<UserGeneratedContentFlag, String> {

    boolean existsByFlagCreatorAndEntityIdAndEntityType(Person flagCreator, String entityId, String entityType);

    Optional<UserGeneratedContentFlag> findByFlagCreatorAndEntityIdAndEntityType(Person flagCreator, String entityId,
            String entityType);

    Set<UserGeneratedContentFlag> findByFlagCreator(Person flagCreator);

    @Query("select entityId from UserGeneratedContentFlag where flagCreator = ?1")
    Set<String> findEntityIdsByFlagCreator(Person flagCreator);

    @Query("select distinct ugcf " +
            "from UserGeneratedContentFlag ugcf " +
            "where ugcf.tenant.id in :tenantIds")
    Page<UserGeneratedContentFlag> findAllByTenantIn(Set<String> tenantIds, Pageable page);

    Page<UserGeneratedContentFlag> findAllByStatusIn(Set<UserGeneratedContentFlagStatus> status, Pageable page);

    @Query("select distinct ugcf " +
            "from UserGeneratedContentFlag ugcf " +
            "where ugcf.tenant.id in :tenantIds " +
            "and ugcf.status in :status")
    Page<UserGeneratedContentFlag> findAllByTenantInAndStatusIn(Set<String> tenantIds,
            Set<UserGeneratedContentFlagStatus> status, Pageable page);

    @Query("select new de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.results.UserGeneratedContentFlagSimpleCountByGeoArea(" +
            "ugcf.entityAuthor.homeArea.id, " +
            "count(ugcf)) " +
            "from UserGeneratedContentFlag ugcf " +
            "where ugcf.flagCreator is null " +
            "and ugcf.created >= :lastCreated " +
            "group by ugcf.entityAuthor.homeArea.id ")
    List<UserGeneratedContentFlagSimpleCountByGeoArea> countFlagCreatorNullTotalByGeoAreaId(long lastCreated);

    @Query("select new de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.results.UserGeneratedContentFlagSimpleCountByGeoArea(" +
            "ugcf.entityAuthor.homeArea.id, " +
            "count(ugcf)) " +
            "from UserGeneratedContentFlag ugcf " +
            "where ugcf.created >= :lastCreated " +
            "group by ugcf.entityAuthor.homeArea.id ")
    List<UserGeneratedContentFlagSimpleCountByGeoArea> countTotalByGeoAreaId(long lastCreated);

    @Query("select new de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.results.UserGeneratedContentFlagSimpleCountByGeoArea(" +
            "ugcf.entityAuthor.homeArea.id, " +
            "sum( case when ugcf.entityType = :expectedEntityType then 1 else 0 end)) " +
            "from UserGeneratedContentFlag ugcf " +
            "where ugcf.created >= :lastCreated " +
            "group by ugcf.entityAuthor.homeArea.id ")
    List<UserGeneratedContentFlagSimpleCountByGeoArea> countByEntityTypeByGeoAreaId(String expectedEntityType,
            long lastCreated);

    @Query("select new de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.results.UserGeneratedContentFlagExtendedCountByGeoArea(" +
            "ugcf.entityAuthor.homeArea.id, " +
            "sum( case when ugcf.entityType in ( :expectedEntityTypes ) then 1 else 0 end), " +
            "sum( case when ugcf.entityType = :expectedEntityType then 1 else 0 end)) " +
            "from UserGeneratedContentFlag ugcf " +
            "where ugcf.created >= :lastCreated " +
            "group by ugcf.entityAuthor.homeArea.id ")
    List<UserGeneratedContentFlagExtendedCountByGeoArea> countByEntityTypesByGeoAreaId(Set<String> expectedEntityTypes,
            String expectedEntityType, long lastCreated);

    @Query("select new de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.results.UserGeneratedContentFlagSummary(" +
            "ugcf.entityId, " +
            "ugcf.entityType, " +
            "sum( case when ugcf.status = 'OPEN' then 1 else 0 end), " +
            "sum( case when ugcf.status = 'IN_PROGRESS' then 1 else 0 end), " +
            "sum( case when ugcf.status = 'ACCEPTED' then 1 else 0 end), " +
            "sum( case when ugcf.status = 'REJECTED' then 1 else 0 end)) " +
            "from UserGeneratedContentFlag ugcf " +
            "where ugcf.entityType in :expectedEntityTypes " +
            "and ugcf.created >= :start " +
            "and ugcf.created <= :end " +
            "group by ugcf.entityId " +
            "order by max(ugcf.created) ")
    Page<UserGeneratedContentFlagSummary> countByStatusFilteredByEntityType(Set<String> expectedEntityTypes, long start, long end, Pageable page);

}
