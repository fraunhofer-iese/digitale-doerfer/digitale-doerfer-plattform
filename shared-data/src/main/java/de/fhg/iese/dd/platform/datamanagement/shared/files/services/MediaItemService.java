/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2024 Steffen Hupp, Balthasar Weitzel, Adeline Silva Schäfer, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IParallelismService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IPersistenceSupportService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.BaseFileItemConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.MediaConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.*;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.*;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.unit.DataSize;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.Duration;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItemSize.ORIGINAL_SIZE;

@Profile({"!test", "MediaItemServiceTest"})
@Service
@Log4j2
class MediaItemService extends BaseFileItemService<MediaItem, TemporaryMediaItem> implements IMediaItemService {

    private static final DataSize MIN_IMAGE_FILE_SIZE = DataSize.ofBytes(8); //8 byte image is very very tiny

    /**
     * the maximum tolerated error for x + width or y + height being > 1
     */
    protected static final double MAX_CROP_SUM_ERROR = 0.0001;

    @Autowired
    private MediaItemRepository mediaItemRepository;
    @Autowired
    private TemporaryMediaItemRepository temporaryMediaItemRepository;
    @Autowired
    private DefaultMediaItemRepository defaultMediaItemRepository;
    @Autowired
    private ITimeService timeService;
    @Autowired
    private IParallelismService parallelismService;
    @Autowired
    private IPersistenceSupportService persistenceSupportService;
    @Autowired
    private IFileDownloadService fileDownloadService;
    @Autowired
    private MediaConfig config;

    private final Set<String> allowedFileExtensions = new HashSet<>();

    private int currentVersion;
    private Map<Integer, List<MediaItemSize>> versionToImageSize;

    @PostConstruct
    protected void initialize() {
        allowedFileExtensions.addAll(Arrays.asList(ImageIO.getReaderFileSuffixes()));
        versionToImageSize = new HashMap<>();
        currentVersion = -1;
        for (Map.Entry<Integer, List<MediaConfig.ImageSizeConfig>> imageSizeVersion : config.getImageSizeVersions().entrySet()) {
            int version = imageSizeVersion.getKey();
            List<MediaItemSize> sizes = new ArrayList<>(imageSizeVersion.getValue().size());

            for (MediaConfig.ImageSizeConfig imageSizeConfig : imageSizeVersion.getValue()) {
                if (StringUtils.equals(ORIGINAL_SIZE.getName(), imageSizeConfig.getName())) {
                    log.error("Invalid image size name '{}', ignoring", ORIGINAL_SIZE.getName());
                } else {
                    sizes.add(MediaItemSize.builder()
                            .name(imageSizeConfig.getName())
                            .fileSuffix(StringUtils.isEmpty(imageSizeConfig.getFileSuffix()) ?
                                    imageSizeConfig.getName() : imageSizeConfig.getFileSuffix())
                            .squared(imageSizeConfig.isSquared())
                            .maxSize(imageSizeConfig.getMaxSize())
                            .build());
                }
            }
            sizes.add(ORIGINAL_SIZE);
            log.debug("Added image size version '{}' with sizes {}", version, sizes);
            versionToImageSize.put(version, Collections.unmodifiableList(sizes));
            currentVersion = Math.max(currentVersion, imageSizeVersion.getKey());
        }
        log.info("Current image size version '{}'", currentVersion);
        ImageUtil.configure();
    }

    @Override
    protected BaseFileItemConfig getConfig() {
        return config;
    }

    @Override
    protected TemporaryBaseFileItemRepository<MediaItem, TemporaryMediaItem> getTemporaryItemRepository() {
        return temporaryMediaItemRepository;
    }

    @Override
    protected BaseFileItemRepository<MediaItem> getItemRepository() {
        return mediaItemRepository;
    }

    @Override
    public int getCurrentSizeVersion() {
        return currentVersion;
    }

    @Override
    public List<MediaItemSize> getDefinedSizesCurrentSizeVersion() {
        return versionToImageSize.get(getCurrentSizeVersion());
    }

    @Override
    public List<MediaItemSize> getDefinedSizes(int sizeVersion) {
        return versionToImageSize.get(sizeVersion);
    }

    @Override
    public MediaItem createMediaItemFromDefault(String defaultMediaIdentifier, FileOwnership fileOwnership)
            throws FileItemProcessingException, DefaultMediaItemNotFoundException {

        DefaultMediaItem existingDefaultMediaItem = defaultMediaItemRepository.findByIdentifier(defaultMediaIdentifier);
        if (existingDefaultMediaItem != null) {
            MediaItem mediaItem = cloneItem(existingDefaultMediaItem.getMediaItem(), fileOwnership);
            log.debug("Reusing MediaItem '{}' from default '{}'", mediaItem.getId(), defaultMediaIdentifier);
            return mediaItem;
        }

        MediaItem newMediaItem = MediaItem.builder()
                .sizeVersion(getCurrentSizeVersion())
                .build();
        newMediaItem.setFileOwnership(fileOwnership);

        IFileStorage.MediaTypeAndFileExtension mediaTypeAndFileExtension =
                guessMediaTypeAndFileExtension(defaultMediaIdentifier);
        String mediaType = mediaTypeAndFileExtension.getMediaType();
        String fileExtension = mediaTypeAndFileExtension.getFileExtension();
        String defaultInternalOriginalFileName = getDefaultInternalFileNameFromDefaultMediaIdentifier(
                defaultMediaIdentifier, ORIGINAL_SIZE, fileExtension);
        if (!fileStorage.fileExistsDefault(defaultInternalOriginalFileName)) {
            throw new DefaultMediaItemNotFoundException(defaultMediaIdentifier);
        }

        BufferedImage originalImage = null;
        SortedMap<MediaItemSize, String> urls = new TreeMap<>();
        try {
            //copy the existing images in all sizes or create them, in case they do not exist
            for (MediaItemSize size : getDefinedSizesCurrentSizeVersion()) {
                String defaultInternalFileName = getDefaultInternalFileNameFromDefaultMediaIdentifier(
                        defaultMediaIdentifier, size, fileExtension);
                String internalFileName = getInternalFileName(newMediaItem, size, fileExtension);
                if (fileStorage.fileExistsDefault(defaultInternalFileName)) {
                    fileStorage.copyFileFromDefault(defaultInternalFileName, internalFileName);
                    if (log.isTraceEnabled()) {
                        log.trace("Copied default file '{}' for MediaItem '{}'", defaultInternalFileName,
                                newMediaItem.getId());
                    }
                    urls.put(size, getExternalURL(newMediaItem, size, fileExtension));
                } else {
                    if (originalImage == null) {
                        try (final InputStream originalImageStream = fileStorage
                                .getFileFromDefault(defaultInternalOriginalFileName)) {
                            originalImage = ImageUtil.createImage(originalImageStream, fileExtension);
                        }
                    }
                    if (size.isImageBigEnough(originalImage.getWidth(), originalImage.getHeight())) {
                        byte[] resizedImage = ImageUtil.createResizedImage(originalImage, size, fileExtension);
                        fileStorage.saveFile(resizedImage, mediaType, internalFileName);
                        urls.put(size, getExternalURL(newMediaItem, size, fileExtension));
                    }
                }
            }
            newMediaItem.setUrls(urls);
        } catch (Exception e) {
            throw new FileItemProcessingException(
                    "Failed to create media item '" + defaultMediaIdentifier + "' from default: " + e, e);
        } finally {
            if (originalImage != null) {
                //clean up the original image
                originalImage.flush();
            }
        }
        newMediaItem = mediaItemRepository.saveAndFlush(newMediaItem);
        //we want a copy, so that it's not deleted with other referencing entities
        MediaItem copyForDefaultMediaItem = cloneItem(newMediaItem, FileOwnership.UNKNOWN);
        DefaultMediaItem newDefaultMediaItem = DefaultMediaItem.builder()
                .identifier(defaultMediaIdentifier)
                .mediaItem(copyForDefaultMediaItem)
                .build();

        persistenceSupportService.saveAndIgnoreIntegrityViolation(
                () -> defaultMediaItemRepository.saveAndFlush(newDefaultMediaItem),
                //if there was an exception saving it there is no need to retry, it might be created in parallel or next time
                () -> null);

        log.debug("Created MediaItem '{}' from default '{}'", newMediaItem.getId(), defaultMediaIdentifier);
        return newMediaItem;
    }

    @Override
    public MediaItem createMediaItem(URL url, FileOwnership fileOwnership) throws FileDownloadException {

        byte[] fileBytes = downloadImage(url);
        try {
            return createMediaItem(fileBytes, fileOwnership);
        } catch (Exception e) {
            throw new FileDownloadException("Failed to process file downloaded from '" + url.toString() + "': " + e, e);
        }
    }

    @Override
    public MediaItem createMediaItem(CroppedImageReference croppedImageReference, FileOwnership fileOwnership)
            throws FileDownloadException {

        final URL url = croppedImageReference.getUrl();
        byte[] fileBytes = downloadImage(url);
        try {
            return createMediaItem(fileBytes, fileOwnership, croppedImageReference.getImageCropDefinition());
        } catch (InvalidImageCropDefinitionException e) {
            throw e;
        } catch (Exception e) {
            throw new FileDownloadException(
                    "Failed to process cropped file downloaded from '" + url.toString() + "': " + e, e);
        }
    }

    @Override
    public MediaItem createMediaItem(byte[] imageData, FileOwnership fileOwnership) throws FileItemProcessingException {
        return createMediaItem(imageData, fileOwnership, null);
    }

    private MediaItem createMediaItem(byte[] imageData, FileOwnership fileOwnership, @Nullable ImageCropDefinition cropDefinition)
            throws FileItemProcessingException, InvalidImageCropDefinitionException {
        IFileStorage.MediaTypeAndFileExtension mediaTypeAndFileExtension;
        try {
            mediaTypeAndFileExtension = fileStorage.determineMediaTypeAndFileExtension(imageData, null);
        } catch (FileTypeNotDetectableException e) {
            throw new FileItemProcessingException(e);
        }
        //we need to check the media type for images uploaded by the clients
        checkMediaType(mediaTypeAndFileExtension.getMediaType());
        checkFileExtension(mediaTypeAndFileExtension.getFileExtension());

        MediaItem mediaItem = MediaItem.builder()
                .sizeVersion(getCurrentSizeVersion())
                .build();
        mediaItem.setFileOwnership(fileOwnership);
        BufferedImage originalImage = null;
        try {
            originalImage = ImageUtil.createImage(imageData, mediaTypeAndFileExtension.getFileExtension());
            SortedMap<MediaItemSize, String> sizeToUrl =
                    createImageFiles(mediaTypeAndFileExtension, mediaItem, originalImage,
                            getDefinedSizesCurrentSizeVersion());
            mediaItem.setUrls(sizeToUrl);

            handleCropDefinition(cropDefinition, originalImage.getWidth(), originalImage.getHeight(), mediaItem);
        } catch (InvalidImageCropDefinitionException e) {
            throw e;
        } catch (Exception e) {
            throw new FileItemProcessingException(e);
        } finally {
            if (originalImage != null) {
                //clean up the original image
                originalImage.flush();
            }
        }

        mediaItem = mediaItemRepository.saveAndFlush(mediaItem);

        log.debug("Created MediaItem '{}'", mediaItem.getId());
        return mediaItem;
    }

    private static void handleCropDefinition(@Nullable ImageCropDefinition cropDefinition, int originalImageWidth, int originalImageHeight, MediaItem mediaItem)
            throws InvalidImageCropDefinitionException {

        if (cropDefinition instanceof AbsoluteImageCropDefinition absoluteImageCropDefinition) {
            int cropOffsetX = absoluteImageCropDefinition.getAbsoluteOffsetX();
            int cropOffsetY = absoluteImageCropDefinition.getAbsoluteOffsetY();
            int cropHeight = absoluteImageCropDefinition.getAbsoluteHeight();
            int cropWidth = absoluteImageCropDefinition.getAbsoluteWidth();
            if (cropOffsetX > originalImageWidth) {
                throw new InvalidImageCropDefinitionException(
                        "The x offset ({}) of the cropped image should not be larger than the original width ({})",
                        cropOffsetX, originalImageWidth);
            }
            if (cropOffsetY > originalImageHeight) {
                throw new InvalidImageCropDefinitionException(
                        "The y offset ({}) of the cropped image should not be larger than the original height ({})",
                        cropOffsetY, originalImageHeight);
            }
            if ((cropWidth + cropOffsetX) > originalImageWidth) {
                throw new InvalidImageCropDefinitionException(
                        "The width ({}) + x offset ({}) of the cropped image should not be larger than the original width ({})",
                        cropWidth, cropOffsetX, originalImageWidth);
            }
            if ((cropHeight + cropOffsetY) > originalImageHeight) {
                throw new InvalidImageCropDefinitionException(
                        "The height ({}) + y offset ({}) of the cropped image should not be larger than the original height ({})",
                        cropHeight, cropOffsetY, originalImageHeight);
            }
            mediaItem.setRelativeImageCropDefinition(RelativeImageCropDefinition.builder()
                    .relativeOffsetX((double) cropOffsetX / (double) originalImageWidth)
                    .relativeOffsetY((double) cropOffsetY / (double) originalImageHeight)
                    .relativeWidth((double) cropWidth / (double) originalImageWidth)
                    .relativeHeight((double) cropHeight / (double) originalImageHeight)
                    .build());
        }
        if (cropDefinition instanceof RelativeImageCropDefinition relativeImageCropDefinition) {
            double cropOffsetX = relativeImageCropDefinition.getRelativeOffsetX();
            double cropOffsetY = relativeImageCropDefinition.getRelativeOffsetY();
            double cropHeight = relativeImageCropDefinition.getRelativeHeight();
            double cropWidth = relativeImageCropDefinition.getRelativeWidth();
            if (cropOffsetX < 0 || cropOffsetY < 0 || cropOffsetX > 1 || cropOffsetY > 1) {
                throw new InvalidImageCropDefinitionException(
                        "The relative x offset ({}) and y offset ({}) of the cropped image should be between 0 and 1",
                        cropOffsetX, cropOffsetY);
            }
            if (cropWidth < 0 || cropHeight < 0 || cropWidth > 1 || cropHeight > 1) {
                throw new InvalidImageCropDefinitionException(
                        "The relative width ({}) and height ({}) of the cropped image should be between 0 and 1",
                        cropWidth, cropHeight);
            }
            if ((cropWidth + cropOffsetX) > 1) {
                //we remove the tolerated error from the width and check if it is valid now
                cropWidth -= MAX_CROP_SUM_ERROR;
                relativeImageCropDefinition.setRelativeWidth(cropWidth);
                if ((cropWidth + cropOffsetX) > 1) {
                    throw new InvalidImageCropDefinitionException(
                            "The relative width ({}) + relative x offset ({}) of the cropped image should not be larger than 1",
                            cropWidth + MAX_CROP_SUM_ERROR, cropOffsetX);
                }
            }
            if ((cropHeight + cropOffsetY) > 1) {
                //we remove the tolerated error from the width and check if it is valid now
                cropHeight -= MAX_CROP_SUM_ERROR;
                relativeImageCropDefinition.setRelativeHeight(cropHeight);
                if ((cropHeight + cropOffsetY) > 1) {
                    throw new InvalidImageCropDefinitionException(
                            "The relative height ({}) + relative y offset ({}) of the cropped image should not be larger than 1",
                            cropHeight + MAX_CROP_SUM_ERROR, cropOffsetY);
                }
            }
            mediaItem.setRelativeImageCropDefinition(relativeImageCropDefinition);
        }
    }

    @Override
    public MediaItem updateMediaItemToCurrentSizeVersion(MediaItem mediaItem, LogSummary logSummary, boolean dryRun) {
        if (mediaItem.getSizeVersion() >= getCurrentSizeVersion()) {
            logSummary.warn("{}: Media item already uses size version {}, no need to update",
                    mediaItem.getId(), mediaItem.getSizeVersion());
            return mediaItem;
        }
        List<MediaItemSize> oldSizeDefinitions = getDefinedSizes(mediaItem.getSizeVersion());
        if (CollectionUtils.isEmpty(oldSizeDefinitions)) {
            logSummary.error("{}: Media item uses unknown size version {}, can not update",
                    mediaItem.getId(), mediaItem.getSizeVersion());
            return mediaItem;
        }
        logSummary.info("{}: Updating from {} to {}",
                mediaItem.getId(), mediaItem.getSizeVersion(), getCurrentSizeVersion());

        //map the old urls to the old size definitions
        SortedMap<MediaItemSize, String> oldSizeUrls = mapSizeNamesToSizes(mediaItem.getUrls(), oldSizeDefinitions);

        //get the original file name that we need for creating the missing sizes
        String internalOriginalFileName = fileStorage.getInternalFileName(oldSizeUrls.get(ORIGINAL_SIZE));
        IFileStorage.MediaTypeAndFileExtension mediaTypeAndFileExtension =
                guessMediaTypeAndFileExtension(internalOriginalFileName);
        String fileExtension = mediaTypeAndFileExtension.getFileExtension();

        //map the old urls with the old sizes to the new ones if they match
        List<MediaItemSize> currentSizes = getDefinedSizesCurrentSizeVersion();
        SortedMap<MediaItemSize, String> newSizeUrls = new TreeMap<>();
        List<MediaItemSize> missingSizes = new ArrayList<>(currentSizes.size());
        //check what sizes can be reused, deleted or need to be done new
        for (MediaItemSize currentSize : currentSizes) {
            //If we get the url, we can reuse it, since MediaItemSize.equals checks for all relevant attributes
            // (name, squared, maxSize)
            //we remove it, so that we later on just have the list of unused size urls
            String url = oldSizeUrls.remove(currentSize);
            if (StringUtils.isNotEmpty(url)) {
                newSizeUrls.put(currentSize, url);
            } else {
                missingSizes.add(currentSize);
            }
        }

        //these are all urls that were not used in the current size list, we can delete them
        Set<MediaItemSize> unusedSizes = oldSizeUrls.keySet();
        Collection<String> unusedSizeUrls = oldSizeUrls.values();
        logSummary.info("{}: Deleting {} files for changed sizes {}",
                mediaItem.getId(), unusedSizes.size(), logMessageSizes(unusedSizes));
        if (!dryRun) {
            try {
                final List<Runnable> deletionTasks = unusedSizeUrls.stream()
                        .map(fileStorage::getInternalFileName)
                        .filter(StringUtils::isNotEmpty)
                        .map(internalFileName -> (Runnable) (() -> fileStorage.deleteFile(internalFileName)))
                        .collect(Collectors.toList());
                parallelismService.submitToBlockableExecutorAndWaitForCompletion(deletionTasks, Duration.ofMinutes(1));
            } catch (Exception e) {
                logSummary.error("{}: Could not delete old image files: {}",
                        e, mediaItem.getId(), unusedSizeUrls);
            }
        }
        //check if we need to create missing sizes
        if (!CollectionUtils.isEmpty(missingSizes)) {
            BufferedImage originalImage = null;
            try {
                //get the original size file
                try (final InputStream originalImageStream = fileStorage.getFile(internalOriginalFileName)) {
                    originalImage = ImageUtil.createImage(originalImageStream, fileExtension);
                }
                if (!dryRun) {
                    //create all missing sizes
                    SortedMap<MediaItemSize, String> newImageFiles =
                            createImageFiles(mediaTypeAndFileExtension, mediaItem, originalImage, missingSizes);
                    logSummary.info("{}: Created {} new files for changed sizes {}",
                            mediaItem.getId(), newImageFiles.size(), logMessageSizes(newImageFiles.keySet()));
                    newSizeUrls.putAll(newImageFiles);
                }
            } catch (IOException | FileStorageException | MimeTypeException e) {
                logSummary.error(
                        "{}: Could not create new files for changed sizes, skipping conversion, potentially leaving dead files on file store: {}",
                        e, mediaItem.getId(), logMessageSizes(newSizeUrls.keySet()), e.getMessage());
                return mediaItem;
            } finally {
                if (originalImage != null) {
                    //clean up the original image
                    originalImage.flush();
                }
            }
        }
        //update
        if (!dryRun) {
            mediaItem.setUrls(newSizeUrls);
            mediaItem.setSizeVersion(getCurrentSizeVersion());
            mediaItem = mediaItemRepository.saveAndFlush(mediaItem);
        }
        return mediaItem;
    }

    private SortedMap<MediaItemSize, String> mapSizeNamesToSizes(Map<String, String> sizeNameToUrl,
            List<MediaItemSize> currentMediaItemSizes) {
        Map<String, MediaItemSize> sizeNameToSize = currentMediaItemSizes.stream()
                .collect(Collectors.toMap(MediaItemSize::getName, Function.identity()));

        SortedMap<MediaItemSize, String> result = new TreeMap<>();
        for (Map.Entry<String, String> sizeNameUrl : sizeNameToUrl.entrySet()) {
            MediaItemSize size = sizeNameToSize.get(sizeNameUrl.getKey());
            if (size == null) {
                throw new IllegalStateException(String.format(
                        "Could not find size with name '%s' in current size version", sizeNameUrl.getKey()));
            }
            result.put(size, sizeNameUrl.getValue());
        }
        return result;
    }

    private SortedMap<MediaItemSize, String> createImageFiles(
            IFileStorage.MediaTypeAndFileExtension mediaTypeAndFileExtension, MediaItem mediaItem,
            BufferedImage originalImage, List<MediaItemSize> sizes) throws IOException, MimeTypeException {

        IFileStorage.MediaTypeAndFileExtension targetType;
        //some formats need to be converted, since we don't have a writer or don't want to deliver it to the clients
        if (config.getConversions().containsKey(mediaTypeAndFileExtension.getMediaType())) {
            String targetMediaType = config.getConversions().get(mediaTypeAndFileExtension.getMediaType());
            MimeType targetMimeType = fileStorage.getTikaConfig().getMimeRepository().forName(targetMediaType);
            targetType = IFileStorage.MediaTypeAndFileExtension.builder()
                    .fileExtension(StringUtils.removeStart(targetMimeType.getExtension(), "."))
                    .mediaType(targetMediaType)
                    .build();
        } else {
            targetType = mediaTypeAndFileExtension;
        }

        SortedMap<MediaItemSize, String> sizeToUrl = new TreeMap<>();
        for (MediaItemSize size : sizes) {
            if (size.isImageBigEnough(originalImage.getWidth(), originalImage.getHeight())) {
                String fileExtension = targetType.getFileExtension();
                String mediaType = targetType.getMediaType();
                byte[] resizedImage = ImageUtil.createResizedImage(originalImage, size, fileExtension);
                String internalFileName = getInternalFileName(mediaItem, size, fileExtension);
                fileStorage.saveFile(resizedImage, mediaType, internalFileName);
                sizeToUrl.put(size, fileStorage.getExternalUrl(internalFileName));
            }
        }
        return sizeToUrl;
    }

    private String logMessageSizes(Collection<MediaItemSize> sizes) {
        return sizes.stream()
                .sorted()
                .map(MediaItemSize::getName)
                .collect(Collectors.joining(", ", "[", "]"));
    }

    @Override
    public TemporaryMediaItem createTemporaryMediaItem(byte[] imageData, FileOwnership fileOwnership)
            throws FileItemProcessingException {

        return internalCreateTemporaryMediaItem(() -> createMediaItem(imageData, fileOwnership), fileOwnership);
    }

    @Override
    public TemporaryMediaItem createTemporaryMediaItem(URL url, FileOwnership fileOwnership) throws FileItemProcessingException {

        return internalCreateTemporaryMediaItem(() -> createMediaItem(url, fileOwnership), fileOwnership);
    }

    private TemporaryMediaItem internalCreateTemporaryMediaItem(Supplier<MediaItem> mediaItemSupplier, FileOwnership fileOwnership) {

        if (fileOwnership.getOwner() == null) {
            //this is actually an implementation error, so we throw a hard exception
            throw new RuntimeException("No owner provided in file ownership");
        }
        // check if the user has too many temporary images
        int countTempItems = temporaryMediaItemRepository.countByOwner(fileOwnership.getOwner());
        int maxTempItems = config.getMaxNumberTemporaryItems();
        if (countTempItems + 1 > maxTempItems) {
            throw new TemporaryFileItemLimitExceededException(maxTempItems, countTempItems);
        }
        // create the media item
        MediaItem mediaItem = mediaItemSupplier.get();
        // calculate the expiration time based on config
        long expirationTime = timeService.currentTimeMillisUTC() + config.getTemporaryItemExpiration().toMillis();

        TemporaryMediaItem tempItem = TemporaryMediaItem.builder()
                .owner(fileOwnership.getOwner())
                .fileItem(mediaItem)
                .expirationTime(expirationTime)
                .build();

        return temporaryMediaItemRepository.saveAndFlush(tempItem);
    }

    @Override
    public MediaItem cloneItem(MediaItem mediaItem, FileOwnership newFileOwnership) {

        final MediaItem newMediaItem = MediaItem.builder()
                .sizeVersion(mediaItem.getSizeVersion())
                .app(newFileOwnership.getApp())
                .appVariant(newFileOwnership.getAppVariant())
                .owner(newFileOwnership.getOwner())
                .created(timeService.currentTimeMillisUTC())
                .build();

        //this is a kind of hack and requires the id to be used as part of the file name
        newMediaItem.setUrlsJson(StringUtils.replace(mediaItem.getUrlsJson(), mediaItem.getId(), newMediaItem.getId()));
        List<Runnable> copyTasks = mediaItem.getUrls().values().stream()
                .map(fileStorage::getInternalFileName)
                .map(internalFileNameSource -> (Runnable) (() -> {
                    String internalFileNameTarget =
                            //this is the same hack
                            StringUtils.replace(internalFileNameSource, mediaItem.getId(), newMediaItem.getId());
                    try {
                        fileStorage.copyFile(internalFileNameSource, internalFileNameTarget);
                        if (log.isTraceEnabled()) {
                            log.trace("Copied media file '{}' to '{}'", internalFileNameSource, internalFileNameTarget);
                        }
                    } catch (FileStorageException e) {
                        log.warn("Could not copy media file '{}' to '{}'", internalFileNameSource,
                                internalFileNameTarget, e);
                    }
                }))
                .collect(Collectors.toList());
        parallelismService.submitToBlockableExecutorAndWaitForCompletion(copyTasks, Duration.ofMinutes(1));
        return mediaItemRepository.saveAndFlush(newMediaItem);
    }

    @Override
    public void deleteReferencedFiles(MediaItem mediaItem) {

        final List<Runnable> deletionTasks = mediaItem.getUrls().values().stream()
                .map(fileStorage::getInternalFileName)
                .filter(StringUtils::isNotEmpty)
                .map(internalFilename -> (Runnable) (() -> {
                    try {
                        fileStorage.deleteFile(internalFilename);
                        log.debug("Deleted media file '{}'", internalFilename);
                    } catch (FileStorageException e) {
                        log.warn("Could not delete media file '" + internalFilename + "'", e);
                    }
                }))
                .collect(Collectors.toList());
        parallelismService.submitToBlockableExecutorAndWaitForCompletion(deletionTasks, Duration.ofMinutes(1));
    }

    @Override
    public List<MediaItem> useTemporaryAndExistingMediaItems(List<MediaItemPlaceHolder> mediaItemPlaceHolders,
            FileOwnership fileOwnership) {

        if (CollectionUtils.isEmpty(mediaItemPlaceHolders)) {
            return Collections.emptyList();
        }
        List<String> duplicateMediaItemIds = BaseEntity.findDuplicateEntities(mediaItemPlaceHolders.stream()
                .map(MediaItemPlaceHolder::getMediaItem)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
        if (!duplicateMediaItemIds.isEmpty()) {
            throw new FileItemDuplicateUsageException(duplicateMediaItemIds);
        }
        List<String> duplicateTemporaryMediaItemIds = BaseEntity.findDuplicateEntities(mediaItemPlaceHolders.stream()
                .map(MediaItemPlaceHolder::getTemporaryMediaItem)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
        if (!duplicateTemporaryMediaItemIds.isEmpty()) {
            throw new TemporaryFileItemDuplicateUsageException(duplicateTemporaryMediaItemIds);
        }

        List<MediaItem> newImages = new ArrayList<>(mediaItemPlaceHolders.size());
        for (MediaItemPlaceHolder placeHolder : mediaItemPlaceHolders) {
            if (placeHolder != null) {
                if (placeHolder.getMediaItem() != null) {
                    newImages.add(placeHolder.getMediaItem());
                } else if (placeHolder.getTemporaryMediaItem() != null) {
                    newImages.add(useItem(placeHolder.getTemporaryMediaItem(), fileOwnership));
                }
            }
        }
        return newImages;
    }

    //can be manually tested in MediaItemServiceDownloadTest (that's also the reason why the method is not private)
    byte[] downloadImage(URL url) throws FileDownloadException {

        return fileDownloadService.downloadFromExternalSystem(url, MIN_IMAGE_FILE_SIZE, config.getMaxDownloadFileSize(),
                config.getAllowedMediaTypes());
    }

    private void checkMediaType(String mediaType) {
        List<String> allowedMediaTypes = config.getAllowedMediaTypes();
        if (!allowedMediaTypes.contains(mediaType)) {
            throw new FileItemUploadException(mediaType, allowedMediaTypes);
        }
    }

    private void checkFileExtension(String fileExtension) {
        if (!allowedFileExtensions.contains(fileExtension)) {
            throw new FileItemUploadException("FileExtension '" + fileExtension +
                    "' can not be understood, because there are no registered writers ( " +
                    allowedFileExtensions + ").");
        }
    }

    private IFileStorage.MediaTypeAndFileExtension guessMediaTypeAndFileExtension(String fileName) {
        String fileExtension = FilenameUtils.getExtension(fileName);
        String mediaType = "image/" + fileExtension;
        return IFileStorage.MediaTypeAndFileExtension.builder()
                .fileExtension(fileExtension)
                .mediaType(mediaType)
                .build();
    }

    private String getDefaultInternalFileNameFromDefaultMediaIdentifier(String defaultMediaIdentifier,
            MediaItemSize size, String fileExtension) {
        if (StringUtils.isNotEmpty(size.getFileSuffix())) {
            return StringUtils.removeEnd(defaultMediaIdentifier, "." + fileExtension) + "_" + size.getFileSuffix() +
                    "." + fileExtension;
        } else {
            return defaultMediaIdentifier;
        }
    }

    private String getInternalFileName(MediaItem mediaItem, MediaItemSize size, String fileExtension) {
        //when changing this pattern also change the copy method
        if (StringUtils.isNotEmpty(size.getFileSuffix())) {
            return fileStorage.appendFileName(config.getInternalStoragePrefix(),
                    mediaItem.getId() + "_" + size.getFileSuffix() + "." + fileExtension);
        } else {
            return fileStorage.appendFileName(config.getInternalStoragePrefix(),
                    mediaItem.getId() + "." + fileExtension);
        }
    }

    private String getExternalURL(MediaItem mediaItem, MediaItemSize size, String fileExtension) {
        return fileStorage.getExternalUrl(getInternalFileName(mediaItem, size, fileExtension));
    }

}
