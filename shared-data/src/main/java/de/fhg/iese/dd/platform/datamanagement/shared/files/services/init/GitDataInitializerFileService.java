/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.datamanagement.shared.config.DataInitConfig;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Service
@Profile("!test & !local")
@Log4j2
class GitDataInitializerFileService extends BaseDataInitializerFileService implements IDataInitializerFileService {

    @Getter
    private final IFileProvider fileProvider;

    @Autowired
    public GitDataInitializerFileService(DataInitConfig dataInitConfig) {
        this.fileProvider = new GitFileProvider(dataInitConfig);
    }

}
