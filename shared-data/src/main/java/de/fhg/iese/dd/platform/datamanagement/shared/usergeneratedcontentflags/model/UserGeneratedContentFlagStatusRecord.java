/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class UserGeneratedContentFlagStatusRecord extends BaseEntity {

    @ManyToOne
    private UserGeneratedContentFlag userGeneratedContentFlag;

    /**
     * The person that triggered the status change, might be null in case of automatic changes
     */
    @ManyToOne
    @Nullable
    private Person initiator;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private UserGeneratedContentFlagStatus status;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String comment;

    @Override
    public String toString() {
        return "UserGeneratedContentFlagStatusRecord [" +
                "id=" + id +
                ", initiator=" + initiator +
                ", status=" + status +
                ", created=" + created +
                ", comment=" + StringUtils.abbreviate(comment, 50) +
                ']';
    }

}
