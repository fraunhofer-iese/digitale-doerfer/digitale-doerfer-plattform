/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2024 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@MappedSuperclass
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public abstract class BaseEntity implements IdentifiableEntity {

    /**
     * Default length for description fields. In MySQL 2+1022 = 1024 bytes are needed (at maximum) to store this entry
     */
    protected static final int DESCRIPTION_LENGTH = 1022;

    /**
     * Forces mapping to SQL data type with maximal text length, i.e. LONGTEXT in MySQL and NVARCHAR(max) in SQL server
     */
    protected static final int MAXIMAL_TEXT_LENGTH = 100_000;

    /**
     * HQL can not deal with IN conditions and empty sets, it constructs an invalid query. To fix this a fake id is used
     * that never matches an entity id.
     */
    private static final Set<String> emptyIdSetToFixHQLBugOnEmptySet = Collections.singleton("never-ever-a-id");

    /**
     * The id of the entity in UUID format.
     */
    @Id
    @Column(length = 36, nullable = false)
    @Builder.Default
    protected String id = UUID.randomUUID().toString();

    @Column
    @Builder.Default
    protected long created = System.currentTimeMillis();

    /**
     * Generate a constant id based on the given seedIds using a UUID of type 3.
     * <p>
     * The same seedIds will result in the same id
     *
     * @param seedIds all ids that should be used as a seed for the id, can also contain nulls
     */
    protected void generateConstantId(String... seedIds) {
        this.id = UUID.nameUUIDFromBytes(Arrays.stream(seedIds)
                .filter(Objects::nonNull)
                .collect(Collectors.joining())
                .getBytes(StandardCharsets.US_ASCII)).toString();
    }

    /**
     * Generate a constant id based on the given seedIds of the given entities
     * using a UUID of type 3.
     * <p>
     * The same seedIds will result in the same id
     *
     * @param seedEntities
     *            all entities which ids should be used as a seed for the id, can also
     *            contain nulls
     */
    protected void generateConstantId(BaseEntity... seedEntities){
        this.id = UUID.nameUUIDFromBytes(Arrays.stream(seedEntities)
            .filter(Objects::nonNull)
            .map(BaseEntity::getId)
            .filter(Objects::nonNull)
            .collect(Collectors.joining())
            .getBytes(StandardCharsets.US_ASCII)).toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BaseEntity other = (BaseEntity) obj;
        if (id == null) {
            return other.id == null;
        } else{
            return id.equals(other.id);
        }
    }

    @Override
    public String toString() {
        return "BaseEntity [id=" + id + ", created=" + created + "]";
    }

    /**
     * Null-safe variant of {@link #getId()}
     *
     * @return id of the entity, or null if the entity is null
     */
    public static <T extends BaseEntity> String getIdOf(final T entity) {
        return (entity == null) ? null : entity.getId();
    }

    public static <T extends BaseEntity> List<String> getIdsOf(final Collection<T> entities) {
        if (!CollectionUtils.isEmpty(entities)) {
            return entities.stream()
                    .map(BaseEntity::getIdOf)
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    public static List<String> findDuplicateEntities(Collection<? extends BaseEntity> entities) {
        return entities.stream()
                .collect(Collectors.groupingBy(BaseEntity::getId, Collectors.counting()))
                .entrySet().stream()
                .filter(e -> e.getValue() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public static List<String> findDuplicateIds(Collection<String> ids) {
        return ids.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .filter(e -> e.getValue() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    /**
     * HQL can not deal with IN conditions and empty sets, it constructs an invalid query. To fix this a fake id is used
     * that never matches a real entity id.
     */
    public static Set<String> toInSafeIdSet(Set<String> set) {
        if (CollectionUtils.isEmpty(set)) {
            return emptyIdSetToFixHQLBugOnEmptySet;
        } else {
            return set;
        }
    }

}
