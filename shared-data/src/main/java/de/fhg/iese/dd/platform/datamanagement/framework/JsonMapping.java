/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.type.TypeFactory;

import lombok.Getter;

public class JsonMapping {

    private JsonMapping() {
        //static utility class
    }

    @Getter(lazy = true)
    private static final ObjectMapper OBJECT_MAPPER = initObjectMapper();

    private static ObjectMapper initObjectMapper() {
        final ObjectMapper objectMapper = new ObjectMapper();
        //forces Jackson to use no args constructor when deserializing. In this constructor, default values can be applied.
        objectMapper.setVisibility(
                objectMapper.getVisibilityChecker().withCreatorVisibility(JsonAutoDetect.Visibility.PUBLIC_ONLY));
        return objectMapper;
    }

    public static ObjectWriter defaultJsonWriter() {
        return getOBJECT_MAPPER().writer();
    }

    public static ObjectReader defaultJsonReader() {
        return getOBJECT_MAPPER().reader();
    }

    public static TypeFactory jsonTypeFactory() {
        return getOBJECT_MAPPER().getTypeFactory();
    }

}
