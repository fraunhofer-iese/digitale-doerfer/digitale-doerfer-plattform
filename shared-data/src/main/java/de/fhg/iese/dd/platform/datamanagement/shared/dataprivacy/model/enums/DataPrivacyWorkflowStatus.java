/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums;

/**
 * Current status of a data privacy workflow
 */
public enum DataPrivacyWorkflowStatus {

    /**
     * The data privacy workflow just started, no data was collected so far.
     */
    OPEN,
    /**
     * The external systems have been called at least once (the calls might have failed).
     */
    WAITING_FOR_EXTERNALS,
    /**
     * All external systems have finished or finally failed.
     */
    EXTERNALS_FINISHED,
    /**
     * The internal part of the workflow finished.
     */
    INTERNAL_WORKFLOW_FINISHED,
    /**
     * The workflow is finished and the result is sent to the recipient (depending on the trigger).
     */
    FINISHED,
    /**
     * The workflow failed (either data privacy report creation or deletion fails)
     */
    FAILED

}
