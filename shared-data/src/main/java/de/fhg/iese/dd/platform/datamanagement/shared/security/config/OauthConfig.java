/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.security.config;

import java.time.Duration;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.time.DurationMin;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = "dd-platform.oauth")
@Getter
@Setter
@Validated
public class OauthConfig {

    @NotNull
    private SignatureMethod method;
    @NotNull
    private String issuer;
    private String issuer2;
    @NotNull
    private String audience;
    private String secretBase64;
    private String apiKeyForPersonLinking;
    private String apiKeyForRegistrationToken;
    private String apiKeyForPendingAccounts;
    private String swaggerClientId;
    @NotNull
    @DurationMin(seconds = 1L)
    private Duration durationUntilDeleteUnused;

    public enum SignatureMethod {
        HS256, RS256
    }

    public String[] getIssuers() {
        if (StringUtils.isEmpty(issuer2)) {
            return new String[]{issuer};
        }
        return new String[]{issuer, issuer2};
    }

}
