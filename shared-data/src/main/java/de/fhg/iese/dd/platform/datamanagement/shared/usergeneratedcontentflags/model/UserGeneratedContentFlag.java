/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(indexes={
        @Index(columnList="entityId,entityType,flag_creator_id", unique = true)
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class UserGeneratedContentFlag extends BaseEntity {

    @Column(length = 36, nullable = false)
    private String entityId;

    /**
     * The full qualified java class name
     */
    @Column(nullable = false)
    private String entityType;

    @ManyToOne
    @Nullable
    private Person entityAuthor;

    @ManyToOne
    @Nullable
    private Person flagCreator;

    @ManyToOne
    @Nullable
    private Tenant tenant;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private UserGeneratedContentFlagStatus status;

    @Column(nullable = false)
    private long lastStatusUpdate;

    @Transient
    private List<UserGeneratedContentFlagStatusRecord> userGeneratedContentFlagStatusRecords;

    public UserGeneratedContentFlag withCreated(long created) {
        this.created = created;
        return this;
    }

    @Override
    public String toString() {
        return "UserGeneratedContentFlag [" +
                "id='" + id + "', " +
                "entityId='" + entityId + "', " +
                "entityType='" + entityType + "', " +
                "entityAuthorId='" + getIdOf(entityAuthor) + "', " +
                "flagCreatorId='" + getIdOf(flagCreator) + "', " +
                "tenant='" + tenant + "', " +
                "status='" + status + "', " +
                ']';
    }

}
