/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.repos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.BaseFileItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryBaseFileItem;

/**
 * Abstract repository for the abstract {@link BaseFileItem} that abstracts from {@link MediaItem} and {@link
 * DocumentItem}.
 *
 * @param <F> the concrete subtype of {@link BaseFileItem} that is wrapped in the temporary item
 * @param <T> the concrete subtype of {@link TemporaryBaseFileItem} that gets managed
 */
@NoRepositoryBean
public interface TemporaryBaseFileItemRepository<F extends BaseFileItem, T extends TemporaryBaseFileItem<F>> extends JpaRepository<T, String> {

    List<T> findAllByOwnerOrderByCreatedDesc(Person owner);

    /**
     * We use both owner and id here to make it explicit that the temporary items are "owned" by a person and can not
     * just be referenced by id.
     *
     * @param owner
     * @param id
     *
     * @return
     */
    Optional<T> findByOwnerAndId(Person owner, String id);

    /**
     * We use both owner and id here to make it explicit that the temporary items are "owned" by a person and can not
     * just be referenced by id.
     *
     * @param owner
     * @param ids
     *
     * @return
     */
    List<T> findAllByOwnerAndIdIn(Person owner, List<String> ids);

    /**
     * We use both owner and id here to make it explicit that the temporary items are "owned" by a person and can not
     * just be referenced by id.
     *
     * @param owner
     * @param id
     *
     * @return
     */
    @Transactional(readOnly = false)
    long deleteByOwnerAndId(Person owner, String id);

    int countByOwner(Person owner);

    /**
     * Deletes all media Items that are older than the given timestamp (that means expiration time < current time)
     *
     * @param timestamp
     *
     * @return Number of deleted entities
     */
    @Transactional(readOnly = false)
    long deleteAllByExpirationTimeBefore(long timestamp);

    @Transactional(readOnly = false)
    void deleteAllByOwner(Person owner);

}
