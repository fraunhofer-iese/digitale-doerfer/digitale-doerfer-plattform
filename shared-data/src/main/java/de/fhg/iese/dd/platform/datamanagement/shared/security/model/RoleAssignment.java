/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.security.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * If persons have special roles they get a role assignment. Standard users do not have that.
 */
@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@NamedEntityGraphs({
        @NamedEntityGraph(name = "RoleAssignment.reduced",
                includeAllAttributes = false),
        @NamedEntityGraph(name = "RoleAssignment.full",
                includeAllAttributes = true),
})
@Table(indexes = {
        @Index(columnList = "person_id, role"),
        @Index(columnList = "person_id, role, relatedEntityId"),
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class RoleAssignment extends BaseEntity {

    public RoleAssignment(Person person, Class<? extends BaseRole<? extends NamedEntity>> role,
            String relatedEntityId) {
        this.person = person;
        this.role = role;
        this.relatedEntityId = relatedEntityId;
    }

    @ManyToOne
    private Person person;

    @Column(nullable = false)
    @Convert(converter = RoleConverter.class)
    private Class<? extends BaseRole<? extends NamedEntity>> role;

    /**
     * Any id of an entity the person is responsible for with the specific role. Examples: Community, Shop,
     * PoolingStation. It is {@code null} when {@code role == Role.SUPER_ADMIN} because this is a global role.
     */
    @Column
    private String relatedEntityId;

    /**
     * Read-only attribute that is set by IRoleService
     */
    @JsonIgnore
    @Transient
    private NamedEntity relatedEntity;

    /**
     * Read-only attribute that is set by IRoleService
     */
    @JsonIgnore
    @Transient
    private Tenant relatedEntityTenant;

    @Override
    public String toString() {
        return String.format("%s [id=%s, role=%s, personId=%s, relatedEntityId=%s]",
                getClass().getSimpleName(),
                getId(),
                role != null ? role.getSimpleName() : null,
                person != null ? person.getId() : null,
                relatedEntityId);
    }

}
