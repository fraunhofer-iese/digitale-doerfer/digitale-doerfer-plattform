/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import de.fhg.iese.dd.platform.datamanagement.shared.files.services.DocumentItemPostDeleteListener;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * DocumentItems represent documents in the form of files.
 * <p/>
 * There is no need to delete the files associated with the DocumentItem manually before deleting it. They will be
 * automatically deleted when this entity (or a container of it with <code>cascade = CascadeType.ALL</code>) is removed.
 * The {@link DocumentItemPostDeleteListener} is used for this purpose.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@DiscriminatorValue("1")
public class DocumentItem extends BaseFileItem {

    @Column(length = 511)
    private String url;

    @Column
    private String title;

    @Column
    private String mediaType;

    @Column(length = DESCRIPTION_LENGTH)
    private String description;

    @Override
    public String toString() {
        return "DocumentItem ["
                + (id != null ? "id=" + id + ", " : "")
                + (title != null ? "title=" + title + ", " : "")
                + (url != null ? "url=" + url : "")
                + "]";
    }

}
