/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.model;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.experimental.SuperBuilder;

/**
 * Wrapper for either {@link DocumentItem} or {@link TemporaryDocumentItem} when both are used in an ordered list.
 */
@SuperBuilder
public class DocumentItemPlaceHolder extends BaseFileItemPlaceHolder<DocumentItem> {

    public DocumentItem getDocumentItem() {
        return fileItem;
    }

    public TemporaryDocumentItem getTemporaryDocumentItem() {
        return (TemporaryDocumentItem) temporaryFileItem;
    }

    @Override
    public String toString() {
        return "DocumentItemPlaceHolder{" +
                "fileItem=" + BaseEntity.getIdOf(fileItem) +
                ", temporaryFileItem=" + BaseEntity.getIdOf(temporaryFileItem) +
                '}';
    }

}
