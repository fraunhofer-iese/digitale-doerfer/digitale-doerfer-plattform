/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.security.model;

import java.util.List;
import java.util.Set;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@ToString
public class OauthAccount {

    /**
     * Different authentication methods a person can use to login, as provided by the OAuth server
     */
    public enum AuthenticationMethod {

        USERNAME_PASSWORD, FACEBOOK, GOOGLE, APPLE

    }

    public enum BlockReason {

        TOO_MANY_LOGIN_ATTEMPTS, MANUALLY_BLOCKED
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @Builder
    @ToString
    @EqualsAndHashCode
    public static class OauthAccountDevice {

        private String clientIdentifier;
        private String deviceName;

    }

    private String oauthId;
    private String name;
    private Set<AuthenticationMethod> authenticationMethods;
    private List<OauthAccountDevice> devices;
    private Integer loginCount;
    private boolean emailVerified;
    private boolean blocked;
    private BlockReason blockReason;

}
