/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.app.repos;

import java.util.Collection;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantTenantContract;

public interface AppVariantGeoAreaMappingRepository extends JpaRepository<AppVariantGeoAreaMapping, String> {

    @Query("select m from AppVariantGeoAreaMapping m where m.contract.appVariant = :appVariant")
    Set<AppVariantGeoAreaMapping> findAllByAppVariant(AppVariant appVariant);

    Set<AppVariantGeoAreaMapping> findAllByGeoArea(GeoArea geoArea);

    Set<AppVariantGeoAreaMapping> findAllByContract(AppVariantTenantContract contract);

    @Transactional
    @Modifying
    @Query("delete from AppVariantGeoAreaMapping m where " +
            "exists(select c from AppVariantTenantContract c " +
            "where c.appVariant = :appVariant " +
            "and m.contract = c)")
    int deleteByAppVariant(AppVariant appVariant);

    @Transactional
    @Modifying
    @Query("delete from AppVariantGeoAreaMapping gA where gA.contract in :contracts and gA.id not in :mappingIdsToKeep")
    int deleteByContractInAndIdNotIn(Collection<AppVariantTenantContract> contracts, Set<String> mappingIdsToKeep);

}
