/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2021 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowTrigger;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowType;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
//this tells hibernate what value to put in the discriminator field, defined in the super class
@DiscriminatorValue("DATA_COLLECTION")
public class DataPrivacyDataCollection extends DataPrivacyWorkflow {

    //this can't be not nullable since it would require all other sub-entities of DataPrivacyWorkflow to have this value present, too
    @Column
    private String internalFolderPath;

    @Builder
    public DataPrivacyDataCollection(
            DataPrivacyWorkflowTrigger workflowTrigger,
            Person person,
            DataPrivacyWorkflowStatus status,
            long lastStatusChanged,
            long lastProcessed,
            String internalFolderPath) {
        super(DataPrivacyWorkflowType.DATA_COLLECTION, workflowTrigger, person, status, lastStatusChanged,
                lastProcessed);
        this.internalFolderPath = internalFolderPath;
    }

}
