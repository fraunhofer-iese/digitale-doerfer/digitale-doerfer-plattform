/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.misc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;

/**
 * This entity is not used in code, it is only required for the liquibase hibernate diff to generate the according
 * table. This table is programmatically accessed at LeaderService.
 */
@SuppressWarnings("unused")
@IgnoreArchitectureViolation(value = IgnoreArchitectureViolation.ArchitectureRule.ENTITY_MODEL,
        reason = "Only used as a placeholder for hibernate")
@Entity(name = "LeaderElection")
class LeaderElectionRecordEntity {

    @Id
    @Column(nullable = false)
    private byte id;
    @Column(nullable = false)
    private String leaderId;
    @Column(nullable = false)
    private long lastSeenAlive;

}
