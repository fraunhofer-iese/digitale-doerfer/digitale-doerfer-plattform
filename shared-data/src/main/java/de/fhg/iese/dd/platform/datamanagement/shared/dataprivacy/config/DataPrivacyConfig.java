/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Stefan Schweitzer, Dominik Schnier, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.time.DurationMin;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

@Validated
@Configuration
@ConfigurationProperties(prefix="dd-platform.data-privacy")
@Getter
@Setter
public class DataPrivacyConfig {

    @NotNull
    @Min(value=0, message="Only positive number of days possible")
    private int maxRetentionDataPrivacyReportsInDays;

    @NotNull
    @NestedConfigurationProperty
    private UserInactivityDeletionPreparationConfig userInactivityDeletionPreparation;

    @NotNull
    @NestedConfigurationProperty
    private UserInactivityDeletionConfig userInactivityDeletion;

    @NotNull
    private List<Pattern> userInactivityExcludes;

    @NotNull
    @NestedConfigurationProperty
    private ExternalSystemDataPrivacyWorkflowConfig externalSystemDataPrivacyWorkflow;

    @NotNull
    private Duration waitTimeBetweenDataPrivacyHandlers;

    @Validated
    @Configuration
    @Getter
    @Setter
    public static class ExternalSystemDataPrivacyWorkflowConfig {

        @NotNull
        private boolean enabled;

        @NotNull
        @DurationMin(seconds = 30)
        private Duration waitTimeAfterFailedCall;

        @NotNull
        @DurationMin(minutes = 1)
        private Duration timeout;

        @NotNull
        @NestedConfigurationProperty
        private ExternalSystemDataCollectionConfig dataCollection;

    }

    @Validated
    @Configuration
    @Getter
    @Setter
    public static class ExternalSystemDataCollectionConfig {

        /**
         * Allowed file extensions of the response files from external systems.
         */
        @NotEmpty
        private Set<String> allowedFileExtensions;

        /**
         * Allowed media types of the response files from external systems. Supports wildcards, e.g. "image/*"
         */
        @NotEmpty
        private Set<String> allowedMediaTypes;

    }

    @Validated
    @Configuration
    @Getter
    @Setter
    public static class UserInactivityDeletionPreparationConfig {

        @NotNull
        private boolean enabled;

        @NotNull
        @Min(value = 0)
        private int batchSize;

        @NotNull
        @Min(value = 0, message = "Only positive number of days possible")
        private int inactiveForDays;

        private String resetLastLoggedInBaseUrl;

    }

    @Validated
    @Configuration
    @Getter
    @Setter
    public static class UserInactivityDeletionConfig {

        @NotNull
        private boolean enabled;

        @NotNull
        @Min(value = 0)
        private int batchSize;

        @NotNull
        @Min(value = 0, message = "Only positive number of days possible")
        private int pendingDeletionAndWarnedForDays;

        @NotNull
        @Min(value = 0, message = "Only positive number of days possible")
        private int pendingDeletionUnverifiedEmailForDays;

    }

    @NotBlank
    private String senderEmailAddressAccount;

    @NotBlank
    private String userDeletionCheck;

}
