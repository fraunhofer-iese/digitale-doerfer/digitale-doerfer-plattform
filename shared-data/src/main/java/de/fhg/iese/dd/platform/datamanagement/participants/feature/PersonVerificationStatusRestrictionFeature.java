/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.participants.feature;

import com.fasterxml.jackson.annotation.JsonSetter;
import de.fhg.iese.dd.platform.datamanagement.framework.enums.StorableEnumSet;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.BaseFeature;
import lombok.Getter;

import java.util.Collections;
import java.util.Set;

/**
 * Feature that can be used to restrict an endpoint by {@link Person#getVerificationStatuses()}. Used in combination
 * with BaseController#checkVerificationStatusRestriction.
 */
public abstract class PersonVerificationStatusRestrictionFeature extends BaseFeature {

    @Getter
    private Set<PersonVerificationStatus> allowedStatusesAnyOf;
    @Getter
    private Set<PersonVerificationStatus> allowedStatusesAllOf;

    private int allowedStatusesAnyOfBitMask = 0;
    private int allowedStatusesAllOfBitMask = 0;

    @JsonSetter
    private void setAllowedStatusesAnyOf(Set<PersonVerificationStatus> allowedStatusesAnyOf) {
        // the feature should not be changed at runtime
        this.allowedStatusesAnyOf = Collections.unmodifiableSet(allowedStatusesAnyOf);
        allowedStatusesAnyOfBitMask = StorableEnumSet.toBitMaskValue(allowedStatusesAnyOf);
    }

    @JsonSetter
    private void setAllowedStatusesAllOf(Set<PersonVerificationStatus> allowedStatusesAllOf) {
        // the feature should not be changed at runtime
        this.allowedStatusesAllOf = Collections.unmodifiableSet(allowedStatusesAllOf);
        allowedStatusesAllOfBitMask = StorableEnumSet.toBitMaskValue(allowedStatusesAllOf);
    }

    public boolean isAllowed(Person person) {
        if (!isEnabled()) {
            return true;
        }
        //this is more or less a misconfiguration
        if (!isAnyOfCheckActive() && !isAllOfCheckActive()) {
            return true;
        }
        //first check if the any of is satisfied
        if (isAnyOfCheckActive()) {
            if (isAllowedAnyOf(person)) {
                return true;
            }
        }
        //then check if the all of is satisfied
        if (isAllOfCheckActive()) {
            //noinspection RedundantIfStatement
            if (isAllowedAllOf(person)) {
                return true;
            }
        }
        //if none of the above is satisfied, return false
        return false;
    }

    private boolean isAllOfCheckActive() {
        return allowedStatusesAllOfBitMask != 0;
    }

    private boolean isAnyOfCheckActive() {
        return allowedStatusesAnyOfBitMask != 0;
    }

    private boolean isAllowedAllOf(Person person) {
        //all of: not(person.status) & required.status matches never
        return (~person.getVerificationStatus() & allowedStatusesAllOfBitMask) == 0;
    }

    private boolean isAllowedAnyOf(Person person) {
        //any of: person.status & required.status matches at least once
        return (person.getVerificationStatus() & allowedStatusesAnyOfBitMask) != 0;
    }

}
