/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.repos;

import java.util.List;

import org.springframework.context.annotation.Conditional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.framework.repos.conditions.MySQLNoH2Condition;

@Conditional(value = {MySQLNoH2Condition.class})
@Transactional(readOnly = true)
public interface MediaItemMySQLRepository extends MediaItemRepository {

    @Override
    @Query(nativeQuery = true, value =
            "SELECT "
                    + "TABLE_NAME, COLUMN_NAME "
                    + "FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE "
                    + "WHERE REFERENCED_TABLE_SCHEMA = :schema "
                    + "AND REFERENCED_TABLE_NAME = 'media_item' ")
    List<Object[]> findTableAndColumnReferencingMediaItems(String schema);

}
