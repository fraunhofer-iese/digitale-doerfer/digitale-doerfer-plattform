/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Stefan Schweitzer, Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.enums.GeoAreaType;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.BoundingBox;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Entity
@Table(indexes = {
        @Index(columnList = "created"),
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class GeoArea extends BaseEntity implements NamedEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private GeoAreaType type;

    @Column(nullable = false)
    private String name;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private MediaItem logo;

    @ManyToOne
    private Tenant tenant;

    @ManyToOne
    private GeoArea parentArea;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String zip;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "longitude", column = @Column(name = "CENTER_LONGITUDE")),
            @AttributeOverride(name = "latitude", column = @Column(name = "CENTER_LATITUDE"))})
    private GPSLocation center;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String boundaryJson;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String customAttributesJson;

    /**
     * True if the geo area is cached and has the transient fields {@link #similarityCodes} {@link #leaf} and {@link
     * #depth} set.
     */
    @Builder.Default
    private transient boolean cached = false;

    /**
     * Bounding box of the boundary. Only set if {@link #cached} is true.
     */
    @Transient
    private BoundingBox boundingBox;

    /**
     * Used for searching geo areas, do not use it for anything else. Only set if {@link #cached} is true.
     */
    @Transient
    private transient String[] similarityCodes;

    /**
     * True if the geo area is a leaf. Only set if {@link #cached} is true.
     */
    @Transient
    private transient boolean leaf;

    /**
     * The global depth of the geo area. Only set if {@link #cached} is true.
     */
    @Transient
    private transient int depth;

    /**
     * The number of direct children of the geo area. Only set if {@link #cached} is true.
     */
    @Transient
    private transient int countDirectChildren;

    /**
     * The number of all children of the geo area, including children of children. Only set if {@link #cached} is true.
     */
    @Transient
    private transient int countChildren;

    /**
     * Used for searching geo areas. Only set if {@link #cached} is true.
     */
    @Transient
    private transient List<String> zips;

    @Override
    public String toString() {
        return "GeoArea [id='" + id + "', name='" + name + "']";
    }

    private static final Set<GeoArea> emptySetToFixHQLBugOnEmptySet = Collections.singleton(new GeoArea());

    /**
     * HQL can not deal with IN conditions and empty sets, it constructs an invalid query. To fix this a fake entity is
     * used that never matches a real entity.
     */
    public static Set<GeoArea> toInSafeEntitySet(Set<GeoArea> geoAreas) {
        if (CollectionUtils.isEmpty(geoAreas)) {
            return emptySetToFixHQLBugOnEmptySet;
        } else {
            return geoAreas;
        }
    }

    public static Collection<GeoArea> toInSafeEntityCollection(Collection<GeoArea> geoAreas) {
        if (CollectionUtils.isEmpty(geoAreas)) {
            return emptySetToFixHQLBugOnEmptySet;
        } else {
            return geoAreas;
        }
    }

}
