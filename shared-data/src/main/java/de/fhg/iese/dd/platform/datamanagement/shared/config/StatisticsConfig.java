/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.config;

import java.time.Duration;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.Setter;

@Validated
@Configuration
@ConfigurationProperties(prefix = "dd-platform.statistics")
@Getter
@Setter
public class StatisticsConfig {

    @NotEmpty
    private String teamFileStorageFolder;
    @NotEmpty
    private String teamFileStorageFolderWeekly;
    @NotEmpty
    private String teamFileStorageFolderDaily;
    @NotEmpty
    private String teamFileStorageFolderMonthly;
    @NotEmpty
    private String teamFileStorageFolderLongterm;
    @NotEmpty
    private String teamFileStorageFolderTemporary;
    @NotEmpty
    private String teamFileStorageFolderCustom;
    @NotNull
    private Duration delayBetweenReportGeneration;

}
