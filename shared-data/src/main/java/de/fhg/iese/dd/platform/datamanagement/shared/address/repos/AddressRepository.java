/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.address.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

/**
 * Addresses are immutable, they can <strong>never</strong> be updated, since we
 * do not know where they are referenced.
 * <p/>
 * e.g. If a delivery uses the same address of a person it would be updated if
 * the person changes the address.
 * <p>
 * <p/>
 * Central repository for all entities of type Address.
 * <p>
 * Basic methods for finding a single record, all records, paginated records, create/update, and delete are
 * automatically provided through JpaRepository and its super-classes, while specific methods are explicitly declared
 * in this class.
 * <p>
 * A concrete implementation of this interface is automatically injected by Spring @ runtime.
 */
@Transactional(readOnly = true)
public interface AddressRepository extends JpaRepository<Address, String> {

    /**
     * Looking up addresses that already exist in order to reuse them.
     * <p>
     * Take the oldest addresses first if there are duplicates.
     *
     */
    List<Address> findByNameAndStreetAndZipAndCityOrderByCreatedAsc(String name, String street, String zip,
            String city);

    /**
     * Looking up addresses that already exist in order to reuse them.
     * <p>
     * Take the oldest addresses first if there are duplicates.
     *
     */
    List<Address> findByNameAndStreetAndZipAndCityAndGpsLocationLatitudeAndGpsLocationLongitudeOrderByCreatedAsc(
            String name, String street, String zip, String city, double latitude, double longitude);

    /**
     * Looking up <strong>verified</strong> addresses that already exist in order to reuse them.
     * <p>
     * Take the oldest addresses first if there are duplicates.
     *
     */
    List<Address> findByNameAndStreetAndZipAndCityAndVerifiedTrueOrderByCreatedAsc(String name, String street,
            String zip, String city);

    List<Address> findByNameAndStreetAndZipAndCityAndGpsLocationOrderByVerifiedDescCreatedAsc(String name,
            String street, String zip, String city,
            GPSLocation gpsLocation);

}
