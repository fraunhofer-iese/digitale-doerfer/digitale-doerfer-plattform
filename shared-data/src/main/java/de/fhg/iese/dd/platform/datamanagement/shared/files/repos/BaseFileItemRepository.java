/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.repos;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.BaseFileItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;

/**
 * Abstract repository for the abstract {@link BaseFileItem} that abstracts from {@link MediaItem} and {@link
 * DocumentItem}.
 *
 * @param <F> the concrete subtype
 */
@NoRepositoryBean
public interface BaseFileItemRepository<F extends BaseFileItem> extends JpaRepository<F, String> {

    @Transactional
    void deleteAllByOwner(Person owner);

    Page<F> findAllByAppVariantOrderByCreatedDesc(AppVariant appVariant, Pageable request);

    Page<F> findAllByAppVariantAndOwnerIsNullOrderByCreatedDesc(AppVariant appVariant, Pageable request);

    List<F> findAllByOwnerOrderByCreatedDesc(Person owner);

    List<F> findAllByIdIn(List<String> ids);

}
