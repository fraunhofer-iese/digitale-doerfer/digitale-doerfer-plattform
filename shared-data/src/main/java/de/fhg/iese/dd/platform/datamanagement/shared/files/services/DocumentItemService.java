/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import java.security.SecureRandom;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.BaseFileItemConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.DocumentConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemDuplicateUsageException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemUploadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.TemporaryFileItemDuplicateUsageException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.TemporaryFileItemLimitExceededException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItemPlaceHolder;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.BaseFileItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.DocumentItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.TemporaryBaseFileItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.TemporaryDocumentItemRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
class DocumentItemService extends BaseFileItemService<DocumentItem, TemporaryDocumentItem> implements IDocumentItemService {

    private static final int MAX_LENGTH_FILENAME = 150;
    private static final char[] SECURE_FILENAME_LETTERS =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();

    @Autowired
    private DocumentConfig config;
    @Autowired
    private DocumentItemRepository documentItemRepository;
    @Autowired
    private TemporaryDocumentItemRepository temporaryDocumentItemRepository;
    @Autowired
    private IFileStorage fileStorage;

    @Getter(lazy = true, value = AccessLevel.PRIVATE)
    private final SecureRandom secureRandom = new SecureRandom();

    @Override
    protected BaseFileItemConfig getConfig() {
        return config;
    }

    @Override
    protected TemporaryBaseFileItemRepository<DocumentItem, TemporaryDocumentItem> getTemporaryItemRepository() {
        return temporaryDocumentItemRepository;
    }

    @Override
    protected BaseFileItemRepository<DocumentItem> getItemRepository() {
        return documentItemRepository;
    }

    @Override
    public TemporaryDocumentItem createTemporaryDocumentItem(byte[] documentData, String fileName, String title,
            String description,
            FileOwnership fileOwnership) {

        if (fileOwnership.getOwner() == null) {
            //this is actually an implementation error, so we throw a hard exception
            throw new RuntimeException("No owner provided in file ownership");
        }
        // check if the user has too many temporary images
        int countTempItems = getTemporaryItemRepository().countByOwner(fileOwnership.getOwner());
        int maxTempItems = config.getMaxNumberTemporaryItems();
        if (countTempItems + 1 > maxTempItems) {
            throw new TemporaryFileItemLimitExceededException(maxTempItems, countTempItems);
        }
        // create the document item
        DocumentItem documentItem = createDocumentItem(documentData, fileName, title, description, fileOwnership);
        // calculate the expiration time based on config
        long expirationTime = timeService.currentTimeMillisUTC() + config.getTemporaryItemExpiration().toMillis();

        TemporaryDocumentItem tempItem = TemporaryDocumentItem.builder()
                .owner(fileOwnership.getOwner())
                .fileItem(documentItem)
                .expirationTime(expirationTime)
                .build();

        return getTemporaryItemRepository().saveAndFlush(tempItem);
    }

    @Override
    public DocumentItem createDocumentItem(byte[] documentData, String fileName, String title, String description,
            FileOwnership fileOwnership) {

        final String sanitizedFilename = fileStorage.sanitizeFileName(fileName);
        //check media type
        final IFileStorage.MediaTypeAndFileExtension mediaTypeAndFileExtension =
                checkFileContent(sanitizedFilename, documentData);
        //build file name
        String truncatedFilename =
                StringUtils.truncate(FilenameUtils.removeExtension(sanitizedFilename), MAX_LENGTH_FILENAME) + "." +
                        mediaTypeAndFileExtension.getFileExtension();

        String internalFilename = generateInternalFileName(truncatedFilename);
        //store file
        fileStorage.saveFile(documentData, mediaTypeAndFileExtension.getMediaType(), internalFilename);

        //create document item
        final String externalUrl = fileStorage.getExternalUrl(internalFilename);

        final DocumentItem documentItem = DocumentItem.builder()
                //if there is no title we take the truncated filename
                .title(StringUtils.isBlank(title) ? truncatedFilename : title)
                .description(description)
                .mediaType(mediaTypeAndFileExtension.getMediaType())
                .app(fileOwnership.getApp())
                .appVariant(fileOwnership.getAppVariant())
                .owner(fileOwnership.getOwner())
                .url(externalUrl)
                .created(timeService.currentTimeMillisUTC())
                .build();

        return getItemRepository().saveAndFlush(documentItem);
    }

    @Override
    public Set<DocumentItem> useTemporaryAndExistingDocumentItems(
            List<DocumentItemPlaceHolder> documentItemPlaceHolders, FileOwnership fileOwnership)
            throws FileItemDuplicateUsageException, TemporaryFileItemDuplicateUsageException {
        if (CollectionUtils.isEmpty(documentItemPlaceHolders)) {
            return Collections.emptySet();
        }
        List<String> duplicateDocumentItemIds = BaseEntity.findDuplicateEntities(documentItemPlaceHolders.stream()
                .map(DocumentItemPlaceHolder::getDocumentItem)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
        if (!duplicateDocumentItemIds.isEmpty()) {
            throw new FileItemDuplicateUsageException(duplicateDocumentItemIds);
        }
        List<String> duplicateTemporaryDocumentItemIds =
                BaseEntity.findDuplicateEntities(documentItemPlaceHolders.stream()
                        .map(DocumentItemPlaceHolder::getTemporaryDocumentItem)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList()));
        if (!duplicateTemporaryDocumentItemIds.isEmpty()) {
            throw new TemporaryFileItemDuplicateUsageException(duplicateTemporaryDocumentItemIds);
        }

        Set<DocumentItem> newDocuments = new HashSet<>(documentItemPlaceHolders.size());
        for (DocumentItemPlaceHolder placeHolder : documentItemPlaceHolders) {
            if (placeHolder != null) {
                if (placeHolder.getDocumentItem() != null) {
                    newDocuments.add(placeHolder.getDocumentItem());
                } else if (placeHolder.getTemporaryDocumentItem() != null) {
                    newDocuments.add(useItem(placeHolder.getTemporaryDocumentItem(), fileOwnership));
                }
            }
        }
        return newDocuments;
    }

    private String generateInternalFileName(String readableFileName) {
        //when changing this pattern also change the copy method
        return fileStorage.appendFileName(config.getInternalStoragePrefix(),
                fileStorage.appendFileName(generateRandomFileNamePart(), readableFileName));
    }

    private String generateRandomFileNamePart() {
        final int length = 42 + getSecureRandom().nextInt(12);
        final StringBuilder result = new StringBuilder();
        for (int i = 0; i < length; i++) {
            result.append(SECURE_FILENAME_LETTERS[getSecureRandom().nextInt(SECURE_FILENAME_LETTERS.length)]);
        }
        return result.toString();
    }

    private IFileStorage.MediaTypeAndFileExtension checkFileContent(String fileName, byte[] fileContent)
            throws FileItemUploadException {

        if (StringUtils.isEmpty(fileName)) {
            throw new FileItemUploadException("File has no name.");
        }
        String rawExtension = StringUtils.lowerCase(FilenameUtils.getExtension(fileName));
        if (!getAllowedFileExtensions().contains(rawExtension)) {
            throw new FileItemUploadException(
                    "{} has file extension '{}' which is not an allowed extension.",
                    fileName,
                    rawExtension);
        }
        try {
            IFileStorage.MediaTypeAndFileExtension mediaTypeAndFileExtension =
                    fileStorage.determineMediaTypeAndFileExtension(fileContent, fileName);
            String mediaType = mediaTypeAndFileExtension.getMediaType();
            boolean isAllowedMediaType = false;
            for (String allowedMediaType : config.getAllowedMediaTypes()) {
                //it's possible to specify patterns like image/*
                if (StringUtils.endsWith(allowedMediaType, "*")) {
                    String mimeTypePrefix = StringUtils.removeEnd(allowedMediaType, "*");
                    if (StringUtils.startsWith(mediaType, mimeTypePrefix)) {
                        isAllowedMediaType = true;
                        break;
                    }
                } else {
                    if (StringUtils.equals(allowedMediaType, mediaType)) {
                        isAllowedMediaType = true;
                        break;
                    }
                }
            }
            if (!isAllowedMediaType) {
                throw new FileItemUploadException(
                        "{} has media type '{}' which is not an allowed media type.",
                        fileName,
                        mediaType);
            }
            return mediaTypeAndFileExtension;
        } catch (Exception e) {
            throw new FileItemUploadException(fileName + " could not be read: " + e, e);
        }
    }

    @Override
    public DocumentItem cloneItem(DocumentItem documentItem, FileOwnership newFileOwnership) {

        final DocumentItem newDocumentItem = DocumentItem.builder()
                .title(documentItem.getTitle())
                .description(documentItem.getDescription())
                .mediaType(documentItem.getMediaType())
                .app(newFileOwnership.getApp())
                .appVariant(newFileOwnership.getAppVariant())
                .owner(newFileOwnership.getOwner())
                .created(timeService.currentTimeMillisUTC())
                .build();

        String internalFileNameSource = fileStorage.getInternalFileName(documentItem.getUrl());
        List<String> fileNamePartsSource = fileStorage.splitFileName(internalFileNameSource);
        if (fileNamePartsSource.isEmpty()) {
            throw new RuntimeException("Unexpected number of file parts");
        }
        String lastFileNamePart = fileNamePartsSource.get(fileNamePartsSource.size() - 1);
        String internalFileNameTarget = generateInternalFileName(lastFileNamePart);
        try {
            fileStorage.copyFile(internalFileNameSource, internalFileNameTarget);
            log.info("Copied document file '{}' to '{}'", internalFileNameSource, internalFileNameTarget);
        } catch (FileStorageException e) {
            log.warn("Could not copy document file '{}' to '{}'", internalFileNameSource,
                    internalFileNameTarget, e);
        }
        newDocumentItem.setUrl(fileStorage.getExternalUrl(internalFileNameTarget));
        return documentItemRepository.saveAndFlush(newDocumentItem);
    }

    @Override
    public void deleteReferencedFiles(DocumentItem documentItem) {

        final String externalUrl = documentItem.getUrl();
        if (StringUtils.isEmpty(externalUrl)) {
            return;
        }
        final String internalFileName = fileStorage.getInternalFileName(externalUrl);
        if (StringUtils.isEmpty(internalFileName)) {
            return;
        }
        try {
            fileStorage.deleteFile(internalFileName);
            log.info("Deleted document file '{}'", internalFileName);
        } catch (FileStorageException e) {
            log.warn("Could not delete document file '" + internalFileName + "'", e);
        }
    }

}
