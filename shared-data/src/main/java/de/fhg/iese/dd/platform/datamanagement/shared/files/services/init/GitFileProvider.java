/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services.init;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.TimeZone;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.util.FS;
import org.eclipse.jgit.util.FileUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import de.fhg.iese.dd.platform.datamanagement.shared.config.DataInitConfig;
import lombok.Getter;

class GitFileProvider extends BaseFileProvider implements IFileProvider {

    private boolean initialized = false;
    @Getter
    protected Path dataInitRootFolder;
    private Path gitUserHome;

    private final DataInitConfig dataInitConfig;

    public GitFileProvider(DataInitConfig dataInitConfig) {
        this.dataInitConfig = dataInitConfig;
    }

    @Override
    protected void ensureInitialized() {
        if (!initialized) {
            try {
                Path tempDir = Files.createTempDirectory("dd-platform-");
                dataInitRootFolder = tempDir.resolve("dd-init-data");
                gitUserHome = tempDir.resolve("dd-init-data-settings");
            } catch (IOException e) {
                throw new DataInitializationException(e);
            }
            initialized = true;
        }
    }

    @Override
    public String checkoutInitData(LogSummary logSummary) throws DataInitializationException {
        super.checkoutInitData(logSummary);
        String branchName = dataInitConfig.getBranchName();
        String repositoryUrl = dataInitConfig.getRepositoryUrl();
        logSummary.info("Checking out branch '{}' from '{}' to '{}'",
                branchName,
                repositoryUrl,
                dataInitRootFolder);
        CredentialsProvider
                credentials =
                new UsernamePasswordCredentialsProvider(dataInitConfig.getUserName(), dataInitConfig.getPassword());
        Git git = null;
        String versionMessage;
        try {
            //let jgit write the settings files in a writeable place
            FS.DETECTED.setUserHome(gitUserHome.toFile());
            //we always remove the repository first, this should have been done already before in the cleanup
            deleteCheckoutLocation();
            git = Git.cloneRepository()
                    .setURI(repositoryUrl)
                    .setCredentialsProvider(credentials)
                    .setBranchesToClone(Collections.singletonList("refs/heads/" + branchName))
                    .setBranch(branchName)
                    .setDirectory(dataInitRootFolder.toFile())
                    .call();

            versionMessage = logGitCheckout(logSummary, git);
        } catch (GitAPIException | IOException e) {
            logSummary.error("Failed to checkout init data on branch '{}' from '{}' to '{}'", e,
                    branchName,
                    repositoryUrl,
                    dataInitRootFolder);
            throw new DataInitializationException(e);
        } finally {
            if (git != null) {
                //we do not need the repository anymore after checkout
                git.close();
            }
        }
        return versionMessage;
    }

    private String logGitCheckout(LogSummary logSummary, Git git) {
        try {
            ObjectId objectId = git.getRepository().resolve(Constants.HEAD);
            try (RevWalk walk = new RevWalk(git.getRepository())) {
                RevCommit commit = walk.parseCommit(objectId);

                PersonIdent authorIdent = commit.getAuthorIdent();
                String commitTime;
                TimeZone timeZone = authorIdent.getTimeZone();
                if (timeZone == null) {
                    commitTime =
                            Instant.ofEpochSecond(commit.getCommitTime()).atZone(ZoneId.systemDefault()).format(
                                    DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                } else {
                    commitTime = Instant.ofEpochSecond(commit.getCommitTime()).atZone(timeZone.toZoneId()).format(
                            DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                }
                String authorName = authorIdent.getName();
                String versionMessage = String.format("%s at %s by %s:%n%s",
                        commit.getId().getName(),
                        commitTime,
                        authorName,
                        commit.getFullMessage());
                logSummary.info("Checked out branch '{}' from '{}', last commit:\n{}",
                        dataInitConfig.getBranchName(),
                        dataInitConfig.getRepositoryUrl(),
                        versionMessage);
                return versionMessage;
            }
        } catch (IOException e) {
            logSummary.error("Failed to find out latest commit", e);
            return "Failed to find out latest commit";
        }
    }

    @Override
    public void cleanup(LogSummary logSummary) {
        super.cleanup(logSummary);
        try {
            //this is to ensure that no sensitive data is left unencrypted on the machine itself
            deleteCheckoutLocation();
        } catch (IOException e) {
            logSummary.error("Could not delete init data at '{}'", e, dataInitRootFolder);
        }
        logSummary.info("Deleted checked out init data at '{}' for security reasons", dataInitRootFolder);
    }

    private void deleteCheckoutLocation() throws IOException {
        FileUtils.delete(dataInitRootFolder.toFile(), FileUtils.RECURSIVE | FileUtils.RETRY | FileUtils.SKIP_MISSING);
    }

}
