/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Johannes Schneider, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums;

import de.fhg.iese.dd.platform.datamanagement.framework.enums.StorableEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PersonStatus implements StorableEnum {

    /**
     * Person has been warned about pending deletion due to inactivity
     */
    PENDING_DELETION(1 << 1),
    /**
     * Login of person is blocked
     */
    LOGIN_BLOCKED(1 << 2),
    /**
     * Content created by this person can only be seen by other persons in the parallel world
     */
    PARALLEL_WORLD_INHABITANT(1 << 3);

    private final int bitMaskValue;

}
