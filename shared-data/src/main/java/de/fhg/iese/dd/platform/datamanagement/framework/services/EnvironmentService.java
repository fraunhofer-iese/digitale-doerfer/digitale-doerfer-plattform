/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.services;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import lombok.extern.log4j.Log4j2;
import software.amazon.awssdk.regions.internal.util.EC2MetadataUtils;

@Service
@Profile({"!test"})
@Log4j2
class EnvironmentService implements IEnvironmentService {

    @Autowired
    private Environment environment;
    @Autowired
    private ITimeService timeService;
    @Autowired
    private ApplicationConfig config;

    private String startupTime;

    private Set<String> activeProfiles = null;
    private Set<String> activeProfilesFiltered = null;

    private String activeProfileInfo = null;
    private String activeProfileInfoFiltered = null;

    private String environmentIdentifier = null;

    private String nodeIdentifier = null;
    private String instanceIdentifier = null;

    private Database database = null;

    @PostConstruct
    private void initialize() {
        startupTime = timeService.toLocalTimePreciseHumanReadable(timeService.currentTimeMillisUTC());
        if (environment instanceof ConfigurableEnvironment) {
            //adds the node id as property, so that it can be used in other properties, like metrics tags
            ((ConfigurableEnvironment) environment).getPropertySources()
                    .addFirst(new MapPropertySource(EnvironmentService.class.getName(),
                            Collections.singletonMap("node-id", getNodeIdentifier())));
        }
    }

    @Override
    public String getEnvironmentIdentifier() {
        if (environmentIdentifier == null) {
            //if the environment identifier is queried too early the property might not be available
            environmentIdentifier =
                    StringUtils.replace(config.getEnvironmentIdentifier(), "${node-id}", getNodeIdentifier());
        }
        return environmentIdentifier;
    }

    @Override
    public String getEnvironmentFullName() {
        return config.getEnvironmentFullName();
    }

    @Override
    public String getEnvironmentDescription() {
        String description = config.getEnvironmentDescription();
        if (StringUtils.isEmpty(description)) {
            return "Environment '" + getEnvironmentFullName() + "'";
        }
        return description;
    }

    @Override
    public String getEnvironmentInfoColor() {
        String infoColor = config.getEnvironmentInfoColor();
        if (StringUtils.isEmpty(infoColor)) {
            return "#ff66ff"; //ugly pink
        }
        return infoColor;
    }

    @Override
    public Set<String> getActiveProfiles(boolean excludeUnderscoreProfiles) {
        if (excludeUnderscoreProfiles) {
            if (activeProfilesFiltered == null) {
                activeProfilesFiltered = Arrays.stream(environment.getActiveProfiles())
                        .filter(p -> !StringUtils.startsWith(p, "_"))
                        .collect(Collectors.toUnmodifiableSet());
            }
            return activeProfilesFiltered;
        } else {
            if (activeProfiles == null) {
                activeProfiles = Arrays.stream(environment.getActiveProfiles())
                        .collect(Collectors.toUnmodifiableSet());
            }
            return activeProfiles;
        }
    }

    @Override
    public String getActiveProfileInfo(boolean excludeUnderscoreProfiles) {
        if (excludeUnderscoreProfiles) {
            if (activeProfileInfoFiltered == null) {
                activeProfileInfoFiltered = getActiveProfiles(true).stream()
                        .sorted()
                        .collect(Collectors.joining(", ", "[", "]"));
            }
            return activeProfileInfoFiltered;
        } else {
            if (activeProfileInfo == null) {
                activeProfileInfo = getActiveProfiles(false).stream()
                        .sorted()
                        .collect(Collectors.joining(", ", "[", "]"));
            }
            return activeProfileInfo;
        }
    }

    @Override
    public String getNodeIdentifier() {
        if (nodeIdentifier == null) {
            nodeIdentifier = internalGetNodeIdentifier();
        }
        return nodeIdentifier;
    }

    @Override
    public String getInstanceIdentifier() {
        if (instanceIdentifier == null) {
            instanceIdentifier = getNodeIdentifier() + "@" + startupTime;
        }
        return instanceIdentifier;
    }

    @Override
    public Database getActiveDatabaseVendor() {
        if (database == null) {
            String datasourceDriver = environment.getProperty("spring.datasource.driver-class-name");
            if (StringUtils.isEmpty(datasourceDriver)) {
                database = Database.DEFAULT;
            } else {
                switch (datasourceDriver) {
                    case "com.mysql.cj.jdbc.Driver":
                        database = Database.MYSQL;
                        break;
                    case "com.microsoft.sqlserver.jdbc.SQLServerDriver":
                        database = Database.SQL_SERVER;
                        break;
                    case "org.h2.Driver":
                        database = Database.H2;
                        break;
                    default:
                        database = Database.DEFAULT;
                }
            }
        }
        return database;
    }

    private String internalGetNodeIdentifier(){
        if (getActiveProfiles(false).contains("aws")) {
            //this pollutes the log with stack traces when running it locally, so we only want to run it on aws
            String instanceId = EC2MetadataUtils.getInstanceId();
            if (!StringUtils.isEmpty(instanceId)) {
                return instanceId;
            }
        }
        try {
            String hostName = InetAddress.getLocalHost().getHostName();
            if (!StringUtils.isEmpty(hostName)) {
                return hostName;
            }
        } catch (UnknownHostException e) {
            // this might happen if the node has no network
        }
        String computerName = System.getenv("COMPUTERNAME");
        if (!StringUtils.isEmpty(computerName)) {
            return computerName;
        }
        String hostName = System.getenv("HOSTNAME");
        if (!StringUtils.isEmpty(computerName)) {
            return hostName;
        }
        //in case we can not determine a name, we add a unique id
        return "unknown-node (" + startupTime + ")";
    }

}
