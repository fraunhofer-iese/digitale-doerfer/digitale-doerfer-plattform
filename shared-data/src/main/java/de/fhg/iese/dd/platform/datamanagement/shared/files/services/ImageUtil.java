/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2024 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.ExifDirectoryBase;
import com.drew.metadata.exif.ExifIFD0Directory;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemProcessingException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemUploadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItemSize;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Log4j2
class ImageUtil {

    public static void configure(){
        ImageIO.setUseCache(true);
    }

    /**
     * Creates a image as byte[] suitable for storage as a file with the ending specified in the fileType.
     *
     * @param imageData raw image
     * @param fileType  type of the returned image, e.g. "jpg" or "png"
     *
     * @return Raw image as byte[]
     */
    public static BufferedImage createImage(byte[] imageData, String fileType) throws FileItemProcessingException {
        return createImage(new ByteArrayInputStream(imageData), fileType);
    }

    /**
     * Creates a image as byte[] suitable for storage as a file with the ending specified in the fileType.
     *
     * @param imageData raw image
     * @param fileType  type of the returned image, e.g. "jpg" or "png"
     *
     * @return Raw image as byte[]
     */
    public static BufferedImage createImage(InputStream imageData, String fileType) throws FileItemProcessingException {
        if (StringUtils.isEmpty(fileType)) {
            throw new FileItemUploadException("Could not create image, empty fileType");
        }
        try {
            BufferedImage original = ImageIO.read(imageData);
            int exifOrientation = getExifOrientation(imageData);
            if (exifOrientation != 1) {
                switch (exifOrientation) {
                    case 2 -> {// Mirror horizontal
                        BufferedImage flipped = Scalr.rotate(original, Scalr.Rotation.FLIP_HORZ);
                        original.flush();
                        return flipped;
                    }
                    case 3 -> {// Rotate 180
                        BufferedImage rotated180 = Scalr.rotate(original, Scalr.Rotation.CW_180);
                        original.flush();
                        return rotated180;
                    }
                    case 4 -> {// Mirror vertical
                        BufferedImage flipped = Scalr.rotate(original, Scalr.Rotation.FLIP_HORZ);
                        original.flush();
                        return flipped;
                    }
                    case 5 -> {// Mirror horizontal and rotate 270 CW
                        BufferedImage flipped = Scalr.rotate(original, Scalr.Rotation.FLIP_HORZ);
                        BufferedImage rotated270 = Scalr.rotate(flipped, Scalr.Rotation.CW_270);
                        flipped.flush();
                        original.flush();
                        return rotated270;
                    }
                    case 6 -> {// Rotate 90 CW
                        BufferedImage rotated90 = Scalr.rotate(original, Scalr.Rotation.CW_90);
                        original.flush();
                        return rotated90;
                    }
                    case 7 -> {// Mirror horizontal and rotate 90 CW
                        BufferedImage flipped = Scalr.rotate(original, Scalr.Rotation.FLIP_HORZ);
                        BufferedImage rotated90 = Scalr.rotate(flipped, Scalr.Rotation.CW_90);
                        flipped.flush();
                        original.flush();
                        return rotated90;
                    }
                    case 8 -> {// Rotate 270 CW
                        BufferedImage rotated270 = Scalr.rotate(original, Scalr.Rotation.CW_270);
                        original.flush();
                        return rotated270;
                    }
                }
            }
            return original;
        } catch (Throwable e) {
            throw new FileItemProcessingException("Could not create image: " + e, e);
        }
    }

    /**
     * These are the possible values (see <a href="https://exiftool.org/TagNames/EXIF.html">documentation</a>)
     * <ul>
     *      <li> 1 = Horizontal (normal)                      </li>
     *      <li> 2 = Mirror horizontal                        </li>
     *      <li> 3 = Rotate 180                               </li>
     *      <li> 4 = Mirror vertical                          </li>
     *      <li> 5 = Mirror horizontal and rotate 270 CW      </li>
     *      <li> 6 = Rotate 90 CW                             </li>
     *      <li> 7 = Mirror horizontal and rotate 90 CW       </li>
     *      <li> 8 = Rotate 270 CW                            </li>
     * </ul>
     *
     * @param imageData the image to get the data from
     * @return the value of the orientation tag of the exif data
     */
    private static int getExifOrientation(InputStream imageData) {

        //if the metadata could not be found or anything fails we assume the image is in standard orientation
        int orientation = 1;
        try {
            //this is important to reposition the stream at the start, might fail
            imageData.reset();
            Metadata metadata = ImageMetadataReader.readMetadata(imageData);
            ExifIFD0Directory exifMetadataDirectory = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
            if (exifMetadataDirectory != null && exifMetadataDirectory.containsTag(ExifDirectoryBase.TAG_ORIENTATION)) {
                try {
                    orientation = exifMetadataDirectory.getInt(ExifDirectoryBase.TAG_ORIENTATION);
                } catch (MetadataException e) {
                    //ignored
                }
            }
        } catch (Throwable e) {
            log.warn("Failed to get exif orientation", e);
        }
        return orientation;
    }

    /**
     * Creates a resized image as byte[] suitable for storage as a file with the ending specified in the fileType.
     *
     * @param image
     *         original Image
     * @param size
     *         size of the returned image
     * @param fileType
     *         type of the returned image, e.g. "jpg" or "png"
     *
     * @return Raw image as byte[]
     */
    public static byte[] createResizedImage(BufferedImage image, MediaItemSize size, String fileType)
            throws FileItemProcessingException {
        if (StringUtils.isEmpty(fileType)) {
            throw new FileItemUploadException("Could not resize image, empty fileType");
        }
        if (image == null) {
            throw new FileItemUploadException("Could not resize image, original image is null");
        }
        int longestSideLength = Math.max(image.getWidth(), image.getHeight());
        BufferedImage resizedImage = null;
        BufferedImage squaredImage = null;
        try {
            if(size.isSquared()){
                squaredImage = squarify(image);
                if (longestSideLength > size.getMaxSize()) {
                    resizedImage = Scalr.resize(squaredImage, Scalr.Method.BALANCED, Scalr.Mode.AUTOMATIC, size.getMaxSize());
                    return getImageBytes(resizedImage, fileType);
                } else {
                    return getImageBytes(squaredImage, fileType);
                }
            } else {
                if (longestSideLength > size.getMaxSize()) {
                    resizedImage = Scalr.resize(image, Scalr.Method.BALANCED, Scalr.Mode.AUTOMATIC, size.getMaxSize());
                    return getImageBytes(resizedImage, fileType);
                }
            }
            return getImageBytes(image, fileType);
        } catch (Throwable e) {
            throw new FileItemProcessingException("Could not resize image: " + e, e);
        } finally {
            if (squaredImage != null) {
                squaredImage.flush();
            }
            if (resizedImage != null) {
                resizedImage.flush();
            }
        }
    }

    /**
     * Creates a square out of a given image.
     *
     * @param image
     * @return
     */
    private static BufferedImage squarify(BufferedImage image) {
        int shortSideLength = Math.min(image.getWidth(), image.getHeight());
        int startX = (image.getWidth() - shortSideLength) / 2;
        int startY = (image.getHeight() - shortSideLength) / 2;

        return image.getSubimage(startX, startY, shortSideLength, shortSideLength);
    }

    /**
     * Converts a BufferedImage to byte[] suitable for storage as a file with
     * the ending specified in the fileType.
     *
     * @param image
     * @param fileType type of the returned image, e.g. ".jpg" or ".png"
     * @return
     * @throws IOException
     */
    public static byte[] getImageBytes(BufferedImage image, String fileType) throws IOException {
        final ByteArrayOutputStream imageOutStream = new ByteArrayOutputStream();
        try (ImageOutputStream stream = ImageIO.createImageOutputStream(imageOutStream)) {
            ImageIO.write(image, StringUtils.remove(fileType, "."), stream);
        }
        return imageOutStream.toByteArray();
    }

}
