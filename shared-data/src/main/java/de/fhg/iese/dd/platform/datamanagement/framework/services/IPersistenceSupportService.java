/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.services;

import java.util.function.Supplier;

import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Slice;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;

public interface IPersistenceSupportService {

    BaseEntity findById(String fullQualifiedClassName, String entityId);

    <T extends BaseEntity> T findById(Class<? extends T> entityClass, String entityId);

    <T extends BaseEntity> T refreshProxy(T entity);

    /**
     * If called within a transaction, it needs to have the isolation level ISOLATION_READ_UNCOMMITTED!
     * <p>
     * Try a save operation.
     * <p>
     * If it fails due to a {@link DataIntegrityViolationException} or {@link ConcurrencyFailureException} it returns
     * the value provided by the second supplier.
     * <p>
     * See current usages to understand how to use it.
     *
     * @param saveSupplier        typically () -> repository.saveAndFlush(entity)
     * @param onExceptionSupplier typically () -> repository.findByBlaBla("bla","bla")
     * @param <T>                 type of the entity to save
     *
     * @return the saved or retrieved entity
     */
    <T> T saveAndIgnoreIntegrityViolation(
            Supplier<T> saveSupplier,
            Supplier<T> onExceptionSupplier);

    /**
     * If called within a transaction, it needs to have the isolation level ISOLATION_READ_UNCOMMITTED!
     * <p>
     * Tries a save operation.
     * <p>
     * If it fails due to a {@link DataIntegrityViolationException} or {@link ConcurrencyFailureException} it tries to
     * get a value using the second supplier.
     * <p>
     * If the value from the second supplier is null, do the save operation again.
     * <p>
     * See current usages to understand how to use it.
     *
     * @param saveSupplier        typically () -> repository.saveAndFlush(entity)
     * @param onExceptionSupplier typically () -> repository.findByBlaBla("bla","bla")
     * @param <T>                 type of the entity to save
     *
     * @return the saved or retrieved entity
     */
    <T extends BaseEntity> T saveAndRetryOnDataIntegrityViolation(
            Supplier<T> saveSupplier,
            Supplier<T> onExceptionSupplier);

    /**
     * If called within a transaction, it needs to have the isolation level ISOLATION_READ_UNCOMMITTED!
     * <p>
     * Tries an operation on the database that might run into concurrency issues.
     * <p>
     * If it fails due to a {@link DataIntegrityViolationException} or {@link ConcurrencyFailureException} it tries to
     * execute it again.
     * <p>
     * See current usages to understand how to use it.
     *
     * @param saveSupplier typically () -> someOperation()
     * @param <T>          type of the entity returned by the operation
     *
     * @return the result of the operation
     */
    <T extends BaseEntity> T executeAndRetryOnDataIntegrityViolation(Supplier<T> saveSupplier);

    <T> T query(Supplier<T> query);

    <T> T save(Supplier<T> query);

    /**
     * Converts a slice to a fake page by using a fake total count. <br/> If there are more slices according to
     * {@link Slice#hasNext()} the total count assumes there is one more entity on the next slice. <br/> If this is the
     * last slice according to {@link Slice#hasNext()} the total count is the number of entities in the previous slices
     * and the current number of entities in this one.
     *
     * @param slice the slice to convert
     * @param <P>   type of entity in the page and slice
     *
     * @return a fake page
     */
    <P> Page<P> sliceToFakePage(Slice<P> slice);

}
