/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import org.hibernate.event.spi.PostDeleteEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.repos.listeners.IEntityDeleteListener;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@Component
public class MediaItemPostDeleteListener implements IEntityDeleteListener {

    private static final long serialVersionUID = -6629445820830849795L;

    @Autowired
    @SuppressFBWarnings(
            value = "SE_BAD_FIELD",
            justification = "If this listener gets serialized the service needs to be injected again anyway.")
    private IMediaItemService mediaItemService;

    @Override
    public Class<? extends BaseEntity> relevantEntityClass() {
        return MediaItem.class;
    }

    @Override
    public void onPostDelete(PostDeleteEvent event) {
        if (event.getEntity() != null) {
            MediaItem mediaItem = (MediaItem) event.getEntity();
            mediaItemService.deleteReferencedFiles(mediaItem);
        }
    }

}
