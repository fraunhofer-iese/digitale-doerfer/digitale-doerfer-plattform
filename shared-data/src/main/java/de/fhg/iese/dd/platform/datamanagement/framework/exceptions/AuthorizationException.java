/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.exceptions;

public class AuthorizationException extends BaseRuntimeException {

    private static final long serialVersionUID = 2770005702370107842L;

    public AuthorizationException() {
        super();
    }

    public AuthorizationException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthorizationException(Throwable cause) {
        super(cause);
    }

    public AuthorizationException(String message) {
        super(message);
    }

    public AuthorizationException(String message, Object... arguments) {
        super(message, arguments);
    }

    @Override
    public ClientExceptionType getClientExceptionType(){
        return ClientExceptionType.UNSPECIFIED_AUTHORIZATION_ERROR;
    }

}
