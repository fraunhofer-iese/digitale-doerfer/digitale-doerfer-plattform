/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.message.ParameterizedMessage;

import javax.annotation.Nullable;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

import static java.time.temporal.ChronoField.*;

/**
 * The LogSummary is intended to create a human readable summary of log messages
 * while writing to the standard log.
 */
public class LogSummary {

    private static final DateTimeFormatter TIMESTAMP_SUMMARY = new DateTimeFormatterBuilder()
            .appendLiteral('[')
            .appendValue(HOUR_OF_DAY, 2)
            .appendLiteral(':')
            .appendValue(MINUTE_OF_HOUR, 2)
            .appendLiteral(':')
            .appendValue(SECOND_OF_MINUTE, 2)
            .appendLiteral('.')
            .appendValue(MILLI_OF_SECOND, 3)
            .appendLiteral(']')
            .toFormatter();

    private static final int MAX_INDENT = 7;
    private static final String INDENT = "   ";

    private final StringBuilder summary;
    private final StringBuilder warnSummary;
    private final StringBuilder errorSummary;
    private Logger logger;
    private Marker logMarker;
    private volatile int indentLevel;
    private final String[] indents;
    private final boolean printStacktrace;
    @Getter
    private int warningsCount = 0;
    @Getter
    private int errorCount = 0;

    public LogSummary(@Nullable Logger logger, boolean printStacktrace) {
        this.logger = logger;
        this.printStacktrace = printStacktrace;
        summary = new StringBuilder();
        warnSummary = new StringBuilder();
        errorSummary = new StringBuilder();
        indentLevel = 0;
        indents = new String[MAX_INDENT + 1];
        for (int i = 0; i <= MAX_INDENT; i++) {
            indents[i] = StringUtils.repeat(INDENT, i);
        }
    }

    public LogSummary() {
        this(null, false);
    }

    public LogSummary(Logger logger) {
        this(logger, false);
    }

    public LogSummary(Logger logger, Marker logMarker) {
        this(logger, false);
        this.logMarker = logMarker;
    }

    public synchronized void setLogger(Logger logger) {
        this.logger = logger;
    }

    public synchronized void setLogMarker(Marker logMarker) {

        this.logMarker = logMarker;
    }

    public boolean hasErrors() {
        return errorCount > 0;
    }

    public boolean hasWarnings() {
        return warningsCount > 0;
    }

    public boolean hasErrorsOrWarnings() {
        return hasErrors() || hasWarnings();
    }

    public synchronized int indent() {
        if (indentLevel < MAX_INDENT) {
            return indentLevel++;
        } else {
            return MAX_INDENT;
        }
    }

    public synchronized int outdent() {
        if (indentLevel >= 1) {
            return indentLevel--;
        } else {
            return 0;
        }
    }

    public synchronized void resetIndentation() {
        indentLevel = 0;
    }

    public void info(String message){
        writeToSummary(message, Level.INFO);
    }

    public void info(String message, Object argument1){
        writeToSummary(new ParameterizedMessage(message, argument1).getFormattedMessage(), Level.INFO);
    }

    public void info(String message, Object argument1, Object argument2){
        writeToSummary(new ParameterizedMessage(message, argument1, argument2).getFormattedMessage(), Level.INFO);
    }

    public void info(String message, Object... arguments){
        writeToSummary(new ParameterizedMessage(message, arguments).getFormattedMessage(), Level.INFO);
    }

    public void warn(String message){
        writeToSummary(message, Level.WARN);
    }

    public void warn(String message, Object argument1){
        writeToSummary(new ParameterizedMessage(message, argument1).getFormattedMessage(), Level.WARN);
    }

    public void warn(String message, Object argument1, Object argument2){
        writeToSummary(new ParameterizedMessage(message, argument1, argument2).getFormattedMessage(), Level.WARN);
    }

    public void warn(String message, Object... arguments) {
        writeToSummary(new ParameterizedMessage(message, arguments).getFormattedMessage(), Level.WARN);
    }

    public String error(String message) {
        writeToSummary(message, Level.ERROR);
        return message;
    }

    public String error(String message, Object... arguments) {
        String formattedMessage = new ParameterizedMessage(message, arguments).getFormattedMessage();
        writeToSummary(formattedMessage, Level.ERROR);
        return formattedMessage;
    }

    public String error(String message, Exception exception, Object... arguments) {
        String formattedMessage = new ParameterizedMessage(message, arguments).getFormattedMessage();
        writeToSummary(formattedMessage, exception, Level.ERROR);
        return formattedMessage;
    }

    private void writeToSummary(String message, Level level) {
        writeToSummary(message, null, level);
    }

    private synchronized void writeToSummary(String message, Exception exception, Level level) {
        switch (level.getStandardLevel()) {
            case INFO:
                if (logger != null) {
                    logger.info(logMarker, message);
                }
                break;
            case WARN:
                summary.append("WARN");
                warnSummary.append(getLoggerPrefix())
                        .append(message)
                        .append("\n");
                if (logger != null) {
                    logger.warn(logMarker, message);
                }
                warningsCount++;
                break;
            case ERROR:
                summary.append("ERROR");
                errorSummary.append(getLoggerPrefix())
                        .append(message)
                        .append("\n");
                if (logger != null) {
                    if (exception != null) {
                        logger.error(logMarker, message, exception);
                    } else {
                        logger.error(logMarker, message);
                    }
                }
                errorCount++;
                break;
            default:
                if (logger != null) {
                    logger.debug(logMarker, message);
                }
                break;
        }
        summary.append(LocalDateTime.now().format(TIMESTAMP_SUMMARY))
                .append(getIndent())
                .append(" ")
                .append(message)
                .append("\n");
        if (printStacktrace && exception != null) {
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw, true);
            exception.printStackTrace(pw);
            summary.append(sw).append("\n");
        }
    }

    private String getLoggerPrefix() {
        if (logger == null) {
            return "";
        } else {
            return StringUtils.substringAfterLast(logger.getName(), ".") + ": ";
        }
    }

    private String getIndent() {
        return indents[indentLevel];
    }

    @Override
    public String toString() {
        return summary.toString();
    }

    public String getWarningLog() {
        return warnSummary.toString();
    }

    public String getErrorLog() {
        return errorSummary.toString();
    }

}
