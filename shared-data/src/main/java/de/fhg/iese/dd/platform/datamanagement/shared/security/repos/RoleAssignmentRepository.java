/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2018 Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.security.repos;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.RelatedEntityIsNull;

@Transactional(readOnly = true)
public interface RoleAssignmentRepository extends JpaRepository<RoleAssignment, String> {

    List<RoleAssignment> findByPersonOrderByCreatedDesc(Person person);
        
    @Query("select r.relatedEntityId " +
            "from RoleAssignment r " +
            "where r.person = :person " +
            "and r.role in :roles ")
    Set<String> findRelatedEntityIdByPersonAndRoleIn(Person person,
            Collection<Class<? extends BaseRole<?>>> roles);

    List<RoleAssignment> findByPersonAndRelatedEntityIdOrderByCreatedDesc(Person person, String relatedEntityId);

    List<RoleAssignment> findByRelatedEntityIdOrderByCreatedDesc(String relatedEntityId);

    Optional<RoleAssignment> findByPersonAndRoleAndRelatedEntityId(Person person,
            Class<? extends BaseRole<? extends NamedEntity>> role, String relatedEntityId);

    boolean existsByPersonAndRole(Person person, Class<? extends BaseRole<? extends NamedEntity>> role);

    boolean existsByPersonAndRoleIn(Person person, Collection<Class<? extends BaseRole<? extends NamedEntity>>> roles);

    <E extends NamedEntity> boolean existsByPersonAndRelatedEntityIdAndRoleIn(Person person,
            String relatedEntityId, Collection<Class<? extends BaseRole<E>>> roles);

    boolean existsByRole(Class<? extends BaseRole<? extends NamedEntity>> role);

    @Query("select distinct ra.person " +
            "from RoleAssignment ra " +
            "where ra.role = :role " +
            "order by ra.person.created desc")
    List<Person> findByRoleOrderByCreatedDesc(Class<? extends BaseRole<? extends NamedEntity>> role);

    @Query("select distinct ra.person " +
            "from RoleAssignment ra " +
            "where ra.role = :role " +
            "and ra.relatedEntityId = :entityId " +
            "order by ra.person.created desc")
    List<Person> findByRoleAndRelatedEntityIdOrderByCreatedDesc(
            Class<? extends BaseRole<? extends NamedEntity>> role,
            String entityId);

    @Query("select distinct p from Person p, RoleAssignment ra " +
            "where ra.role = :role and ra.person = p")
    Page<Person> findAllByAssignedRole(Class<? extends BaseRole<? extends NamedEntity>> role, Pageable page);

    //see also findByHomeCommunityOrHomeAreaTenantIn
    @Query("select distinct p from Person p, RoleAssignment ra " +
            "left join p.homeArea ha " +
            "where (p.community.id in :tenantIds or ha.tenant.id in :tenantIds) " +
            "    and ra.role = :role and ra.person = p")
    Page<Person> findByHomeCommunityOrHomeAreaTenantIn(Class<? extends BaseRole<? extends NamedEntity>> role,
            Set<String> tenantIds,
            Pageable page);

    @Query("select distinct ra.person " +
            "from RoleAssignment ra " +
            "where ra.role in :roles " +
            "and ra.relatedEntityId = :entityId " +
            "order by ra.person.created desc")
    <E extends NamedEntity> List<Person> findByRoleInAndRelatedEntityIdOrderByCreatedDesc(
            Collection<Class<? extends BaseRole<E>>> roles,
            String entityId);

    RoleAssignment findFirstByRoleAndRelatedEntityIdOrderByCreatedDesc(
            Class<? extends BaseRole<? extends NamedEntity>> role, String relatedEntityId);

    RoleAssignment findFirstByRoleOrderByCreatedDesc(Class<? extends BaseRole<RelatedEntityIsNull>> role);

    @Transactional
    @Modifying
    int deleteByRoleAndRelatedEntityId(Class<? extends BaseRole<? extends NamedEntity>> role, String relatedEntityId);

}
