/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.config;

import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.time.DurationMin;
import org.springframework.util.unit.DataSize;

import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import lombok.Getter;
import lombok.Setter;

/**
 * Abstract parent for handling {@link MediaItem}s and {@link DocumentItem}s
 */
@Getter
@Setter
public abstract class BaseFileItemConfig {

    private DataSize maxDownloadFileSize;
    /**
     * This has to be the same or less of spring.servlet.multipart.max-file-size
     */
    @NotNull
    private DataSize maxUploadFileSize;

    @NotNull
    private List<String> allowedMediaTypes;

    private Map<String, String> conversions = Collections.emptyMap();

    @NotNull
    private int maxNumberTemporaryItems;
    @NotNull
    @DurationMin(minutes = 1)
    private Duration temporaryItemExpiration;

    @NotNull
    private String internalStoragePrefix;

}
