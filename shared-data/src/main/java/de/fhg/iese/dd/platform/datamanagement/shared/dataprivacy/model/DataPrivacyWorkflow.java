/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2021 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.ReadOnlyProperty;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowTrigger;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(indexes = {
        @Index(columnList = "status"),
        @Index(columnList = "created")
})
//all subclasses are stored in a single table and different types are distinguished via the discriminator field
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//this tells hibernate to use the field workflowType to store the discriminator
@DiscriminatorColumn(name = "workflow_type", discriminatorType = DiscriminatorType.STRING)
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class DataPrivacyWorkflow extends BaseEntity {

    @Column(name = "workflow_type", insertable = false, updatable = false)
    @ReadOnlyProperty
    @Enumerated(EnumType.STRING)
    @Setter(AccessLevel.NONE)
    private DataPrivacyWorkflowType workflowType;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DataPrivacyWorkflowTrigger workflowTrigger;

    @ManyToOne(optional = false)
    private Person person;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DataPrivacyWorkflowStatus status;

    @Column(nullable = false)
    private long lastStatusChanged;

    @Column(nullable = false)
    private long lastProcessed;

    protected DataPrivacyWorkflow(DataPrivacyWorkflowType workflowType, DataPrivacyWorkflowTrigger workflowTrigger,
            Person person, DataPrivacyWorkflowStatus status, long lastStatusChanged, long lastProcessed) {
        super();
        this.workflowType = workflowType;
        this.workflowTrigger = workflowTrigger;
        this.person = person;
        this.status = status;
        this.lastStatusChanged = lastStatusChanged;
        this.lastProcessed = lastProcessed;
    }

}
