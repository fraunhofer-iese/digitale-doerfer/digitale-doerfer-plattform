/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
public class AppVariantGeoAreaMapping extends BaseEntity {

    @ManyToOne(optional = false)
    private GeoArea geoArea;

    @Column(nullable = false)
    private boolean excluded;

    @ManyToOne(optional = false)
    private AppVariantTenantContract contract;

    /**
     * Constant id based on geo area and contract. Be aware that the excluded flag is not used! Semantically it makes no
     * sense to include and exclude the same geo area and it is prevented by the data init.
     */
    public AppVariantGeoAreaMapping withConstantId() {
        super.generateConstantId(geoArea, contract);
        return this;
    }

}
