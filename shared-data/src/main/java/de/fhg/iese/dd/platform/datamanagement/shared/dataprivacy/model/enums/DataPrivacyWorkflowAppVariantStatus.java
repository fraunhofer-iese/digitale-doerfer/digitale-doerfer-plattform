/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2021 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums;

/**
 * Current status of called external system for a data privacy workflow
 */
public enum DataPrivacyWorkflowAppVariantStatus {

    /**
     * The data privacy workflow for this app variant did not start.
     */
    OPEN,
    /**
     * This status only makes sense when calling external systems.
     * <p>
     * The call to the external system to signal that the data  privacy workflow needs to start failed. We will retry
     * the call after waiting for some time.
     */
    CALL_FAILED,
    /**
     * This status only makes sense when calling external systems.
     * <p>
     * The call to the external system to signal that the data privacy workflow needs to start was successful. We now
     * wait for the external system to send responses.
     */
    CALL_SUCCESSFUL,
    /**
     * The data privacy workflow for this app variant is still in progress.
     * <p>
     * In case of calling external systems: It already responded, but wants to send more data.
     */
    IN_PROGRESS,
    /**
     * The data  privacy workflow for this app variant is finished and the overall workflow can continue or is already
     * finished, too.
     * <p>
     * In case of calling external systems: It responded and sent all data.
     */
    FINISHED,
    /**
     * The person could not be found, so the data  privacy workflow for this app variant is considered finished and the
     * overall workflow can continue or is already finished, too.
     * <p>
     * In case of calling external systems: It responded and signaled that it could not find the person.
     */
    PERSON_NOT_FOUND,
    /**
     * The data  privacy workflow for this app variant failed, so this needs to be logged for manual fix (or it already
     * has been noted).
     * <p>
     * In case of calling external systems: It responded with this status or it did not sent any response and the time
     * ran out.
     */
    FAILED

}
