/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Balthasar Weitzel, Adeline Silva Schäfer, Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.MediaConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.*;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.*;

import java.net.URL;
import java.util.List;

public interface IMediaItemService extends IBaseFileItemService<MediaItem, TemporaryMediaItem> {

    /**
     * Gets the current version of the media item size definitions.
     * <p>
     * This version will be used for {@link MediaItem#getSizeVersion()} for all newly created media items.
     * </p>
     *
     * @return the current size version
     */
    int getCurrentSizeVersion();

    /**
     * Gets the current defined media item sizes.
     * <p>
     * Shortcurt for {@code getDefinedSizes(getCurrentSizeVersion())}
     * </p>
     *
     * @return the list of currently defined sizes sorted by {@link MediaItemSize#getMaxSize()}
     *
     * @see #getDefinedSizes(int)
     */
    List<MediaItemSize> getDefinedSizesCurrentSizeVersion();

    /**
     * Gets the defined sizes for that size version. The sizes are defined in the configuration at
     * {@link MediaConfig#getImageSizeVersions()}. Each size defines the size of an image used in a {@link MediaItem}.
     *
     * @param sizeVersion the version of the size definitions
     *
     * @return the list of defined sizes for that version sorted by {@link MediaItemSize#getMaxSize()}
     */
    List<MediaItemSize> getDefinedSizes(int sizeVersion);

    /**
     * Creates and saves a {@link MediaItem}, copies the image in all available sizes identified by the given identifier
     * from the configured defaultDataBucket to the staticContentBucket (If S3 is used as FileStorage, this depends on
     * the profile the application is running). Both are configured in the application.yml.
     * <p/>
     * If not all sizes are given, they are created and a warning is logged.
     *
     * @param defaultMediaIdentifier the path to the image that should be used, relative to the defaultDataBucket
     *                               bucket.
     * @param fileOwnership          the owner of a media item. The owner can either be a person, an app or an app
     *                               variant.
     *
     * @return the new {@link MediaItem}, with the URLs pointing to copies of the default data.
     *
     * @throws DefaultMediaItemNotFoundException if the file could not be found via
     *                                           {@link IFileStorage#getFileFromDefault(String)}
     * @throws FileItemProcessingException       if the file could not be processed
     */
    MediaItem createMediaItemFromDefault(String defaultMediaIdentifier, FileOwnership fileOwnership)
            throws FileItemProcessingException, DefaultMediaItemNotFoundException;

    /**
     * Creates and saves a {@link MediaItem}, downloads the image from the given URL, creates all defined sizes from it
     * and  uploads them to the file storage.
     *
     * @param url           the URL of the image. This URL is used to download the image and to create all defined
     *                      sizes.
     * @param fileOwnership the owner of a media item. The owner can either be a person, an app or an app variant.
     *
     * @return the new {@link MediaItem}.
     *
     * @throws FileDownloadException if the file could not be downloaded or not processed after downloading
     */
    MediaItem createMediaItem(URL url, FileOwnership fileOwnership) throws FileDownloadException;

    /**
     * Creates and saves a {@link MediaItem}, downloads the image from the given URL, creates all defined sizes from it
     * and uploads them to the file storage as well as saved the information about the cropping.
     *
     * @param croppedImageReference a URI as well as information about the cropping of the image. The URI is used to
     *                              download the image and to create all defined sizes.
     * @param fileOwnership         the owner of a media item. The owner can either be a person, an app or an app
     *                              variant.
     *
     * @return the new {@link MediaItem}.
     *
     * @throws FileDownloadException if the file could not be downloaded or not processed after downloading
     * @throws InvalidImageCropDefinitionException if the image crop definition is invalid or can not be applied to the image
     */
    MediaItem createMediaItem(CroppedImageReference croppedImageReference, FileOwnership fileOwnership)
            throws FileDownloadException, InvalidImageCropDefinitionException;

    /**
     * Creates and saves a {@link MediaItem}, creates all defined resolutions from the given image and uploads it to the
     * file storage.
     *
     * @param imageData     the data of the image. This will be uploaded to the file storage and is also used to create
     *                      the thumbnail, which is also stored at the file storage.
     * @param fileOwnership the owner of a media item. The owner can either be a person, an app or an app variant.
     *
     * @return the new {@link MediaItem}, with URLs to the image and to the thumbnail the file storage.
     *
     * @throws FileItemProcessingException if the file could not processed
     */
    MediaItem createMediaItem(byte[] imageData, FileOwnership fileOwnership) throws FileItemProcessingException;

    /**
     * Updates the given media item to the newest media item sizes, as defined in
     * {@link #getDefinedSizesCurrentSizeVersion()}. When updating, as much image files as possible are preserved by
     * comparing the sizes of the given media item to the sizes defined in the current version. To do so,
     * {@link #getDefinedSizes(int)} is used with {@link MediaItem#getSizeVersion()} of the given media item.
     * <p>
     * The updated media item has all sizes available (if the size of the original image allows it) that are defined in
     * the current size version.
     * </p>
     *
     * @param mediaItem  media item with an older size version
     * @param logSummary summary for a human readable log
     * @param dryRun     if true, no changes are done, only logging takes place
     *
     * @return the updated media item
     */
    MediaItem updateMediaItemToCurrentSizeVersion(MediaItem mediaItem, LogSummary logSummary, boolean dryRun);

    /**
     * Creates a {@link TemporaryMediaItem} by using {@link #createMediaItem(byte[], FileOwnership)} internally.
     *
     * @param imageData     actual data of the image
     * @param fileOwnership the owner of the media item. The owner must be a person and additionally an app or an app
     *                      variant.
     *
     * @return the new {@link TemporaryMediaItem} that can be used by the owner.
     *
     * @throws FileItemProcessingException if the file could not be processed
     */
    TemporaryMediaItem createTemporaryMediaItem(byte[] imageData, FileOwnership fileOwnership)
            throws FileItemProcessingException;

    /**
     * Creates a {@link TemporaryMediaItem} by using {@link #createMediaItem(URL, FileOwnership)} internally.
     *
     * @param url           the URL of the image. This URL is used to download the image and to create all defined
     *                      sizes.
     * @param fileOwnership the owner of the media item. The owner must be a person and additionally an app or an app
     *                      variant.
     * @return the new {@link TemporaryMediaItem} that can be used by the owner.
     * @throws FileItemProcessingException if the file could not be processed
     */
    TemporaryMediaItem createTemporaryMediaItem(URL url, FileOwnership fileOwnership)
            throws FileItemProcessingException;

    /**
     * Uses either the temporary media items or the existing media items. For temporary media items
     * {@link #useItem(TemporaryBaseFileItem, FileOwnership)} is called.
     *
     * @param mediaItemPlaceHolders containing either an existing or a temporary media item
     * @param fileOwnership         the owner of a media item. The owner can either be a person, an app or an app
     *                              variant.
     *
     * @return list of media items consisting of existing and used temporary media items
     *
     * @throws FileItemDuplicateUsageException          if the same media item is used at least twice
     * @throws TemporaryFileItemDuplicateUsageException if the same temporary media item is used at least twice
     */
    List<MediaItem> useTemporaryAndExistingMediaItems(List<MediaItemPlaceHolder> mediaItemPlaceHolders,
            FileOwnership fileOwnership)
            throws FileItemDuplicateUsageException, TemporaryFileItemDuplicateUsageException;

}
