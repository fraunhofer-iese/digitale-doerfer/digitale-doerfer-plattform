/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.misc.model;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class TimeSpan {

    @Column
    private long startTime;

    @Column
    private long endTime;

    public String toHumanReadableString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy (HH:mm)").withZone(ZoneOffset.UTC);
        return formatter.format(Instant.ofEpochMilli(startTime)) + " - " + formatter.format(Instant.ofEpochMilli(
                endTime));
    }

    private boolean isSameDay() {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(new Date(startTime));
        cal2.setTime(new Date(endTime));
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    public static String toFriendlyHumanReadableString(TimeSpan timeSpan) {

        final Instant startTime = Instant.ofEpochMilli(timeSpan.getStartTime());
        final Instant endTime = Instant.ofEpochMilli(timeSpan.getEndTime());

        if (timeSpan.isSameDay()) {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy").withZone(ZoneOffset.UTC);
            DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm").withZone(ZoneOffset.UTC);
            return dateFormatter.format(startTime) + " zwischen " + timeFormatter.format(startTime) + " - " +
                    timeFormatter.format(endTime) + " Uhr";
        } else {
            DateTimeFormatter dateTimeFormatter =
                    DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm").withZone(ZoneOffset.UTC);
            return "zwischen " + dateTimeFormatter.format(startTime) + " Uhr und " + dateTimeFormatter.format(endTime) +
                    " Uhr";
        }
    }

}
