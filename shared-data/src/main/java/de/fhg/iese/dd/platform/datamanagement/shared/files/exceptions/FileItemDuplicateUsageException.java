/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions;

import java.util.List;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class FileItemDuplicateUsageException extends BadRequestException {

    private static final long serialVersionUID = -4333506266110914844L;

    public FileItemDuplicateUsageException(List<String> ids) {
        super("Duplicate usage of file items with ids '{}'.", ids);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.FILE_ITEM_DUPLICATE_USAGE;
    }

}
