/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared;

@SuppressWarnings({"unused", "RedundantSuppression"})//referenced from data init
public final class SharedConstants {

    public static final String PLATFORM_APP_ID = "c4502eec-4aa6-4eb6-9619-47c1105cb52c";

    public static final String PLATFORM_APP_IDENTIFIER = "dd-platform";

    public static final String PLATFORM_APP_VARIANT_ID = "f75fd534-287f-4775-91d8-0226f6eaedad";

    public static final String PLATFORM_APP_VARIANT_IDENTIFIER = "de.fhg.iese.dd.platform";

    public static final String ADMINISTRIERBAR_APP_ID = "034e5ce9-cf58-4b35-a496-4d45d6fed85c";

}
