/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import de.fhg.iese.dd.platform.datamanagement.framework.enums.StorableEnumSet;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(indexes = {
        @Index(columnList = "created"),
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class TeamNotificationChannelMapping extends BaseEntity {

    /**
     * Connection to a team chat server, where this channel is configured
     */
    @ManyToOne(optional = false)
    private TeamNotificationConnection teamNotificationConnection;

    /**
     * Notifications sent via this channel have to concern this tenant. If the tenant is null, the messages can concern
     * any tenant.
     */
    @ManyToOne
    private Tenant tenant;

    /**
     * Topic of the notifications that can be sent via this channel
     */
    @Column
    private String topic;

    /**
     * Filter that stores all allowed notification priorities for this channel mapping
     */
    @Column
    private int allowedPrioritiesMask;

    /**
     * Configuration of the actual channel, which is relative to the teamNotificationConnection
     */
    @Column
    private String channel;

    /**
     * Messages with these priorities should be sent to the channel.
     * <p>
     * Changes are directly reflected at {@link #allowedPrioritiesMask}, which is the value that is actually stored.
     */
    @Transient
    @Getter
    @Setter(value = AccessLevel.NONE)
    private final StorableEnumSet<TeamNotificationPriority> allowedPriorities =
            new StorableEnumSet<>(this::setAllowedPrioritiesMask, this::getAllowedPrioritiesMask,
                    TeamNotificationPriority.class);

    /**
     * Sets a constant Id, based on teamNotificationConnection, tenant, topic, channel
     *
     * @return the same entity
     */
    public TeamNotificationChannelMapping withConstantId() {
        generateConstantId(teamNotificationConnection.getId(), tenant == null ? null : tenant.getId(), topic, channel);
        return this;
    }

    @Override
    public String toString() {
        return "TeamNotificationChannelMapping [" +
                "teamNotificationConnection= '" +
                (teamNotificationConnection == null ? "null" : teamNotificationConnection.getName()) + "', " +
                "tenant= '" + (tenant == null ? "common" : tenant.toString()) + "', " +
                "topic= '" + topic + "', " +
                "allowedPriorities= '" + getAllowedPriorities().getValues() + "', " +
                "channel= '" + channel + "']";
    }

}
