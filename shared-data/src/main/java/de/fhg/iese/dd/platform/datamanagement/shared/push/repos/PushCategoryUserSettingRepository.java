/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.push.repos;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;

@Transactional(readOnly = true)
public interface PushCategoryUserSettingRepository extends JpaRepository<PushCategoryUserSetting, String> {

    PushCategoryUserSetting findByPushCategoryAndPersonAndAppVariant(PushCategory pushCategory, Person person,
            AppVariant appVariant);

    Set<PushCategoryUserSetting> findAllByPushCategoryInAndPersonAndAppVariant(Collection<PushCategory> pushCategories,
            Person person,
            AppVariant appVariant);

    List<PushCategoryUserSetting> findAllByPersonAndAppVariantOrderByCreatedDesc(Person person, AppVariant appVariant);

    @SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
    @Query("select new org.apache.commons.lang3.tuple.ImmutablePair(pcus.person.id, pcus.loudPushEnabled) " +
            "from PushCategoryUserSetting pcus " +
            "where pcus.pushCategory = :pushCategory " +
            "and pcus.person in :persons " +
            "and pcus.appVariant in :appVariants " +
            "order by pcus.created desc ")
    List<Pair<String, Boolean>> findLoudPushEnabledAndPersonIdByCategoryAndPersonAndAppVariant(
            PushCategory pushCategory,
            Collection<Person> persons,
            Collection<AppVariant> appVariants);

    List<PushCategoryUserSetting> findAllByPushCategoryOrderByCreatedDesc(PushCategory pushCategory);

    List<PushCategoryUserSetting> findByPersonOrderByCreatedDesc(Person person);

}
