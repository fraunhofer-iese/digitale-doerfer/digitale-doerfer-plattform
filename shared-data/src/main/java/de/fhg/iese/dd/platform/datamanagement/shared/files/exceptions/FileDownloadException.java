/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.unit.DataSize;

import java.net.URL;
import java.util.List;

public class FileDownloadException extends BadRequestException {

    private static final long serialVersionUID = -4056766269313878417L;

    @Override
    public ClientExceptionType getClientExceptionType() {

        return ClientExceptionType.FILE_DOWNLOAD_FAILED;
    }

    public FileDownloadException(String message) {

        super(message);
    }

    public FileDownloadException(String message, Object... arguments) {
        super(message, arguments);
    }

    public static FileDownloadException notDownloadableForTooSmall(URL url, long fileSize, DataSize minimumFileSize) {
        return new FileDownloadException(
                "The file could not be downloaded from '{}' " +
                        "as its file size {} Bytes is smaller than the allowed minimum file size of {} Bytes)",
                url, fileSize, minimumFileSize);
    }

    public static FileDownloadException notDownloadableForTooBig(URL url, long fileSize, DataSize maximumFileSize) {
        return new FileDownloadException(
                "The file could not be downloaded from '{}' " +
                        "as its file size {} Bytes exceeds the allowed maximum file size of {} Bytes)",
                url, fileSize, maximumFileSize);
    }

    public static FileDownloadException notDownloadableForUnsupportedProtocol(URL url) {
        return new FileDownloadException(
                "The file could not be downloaded from '{}' because the protocol '{}' is not supported.",
                url, url.getProtocol());
    }

    public static FileDownloadException notDownloadableForUnsupportedFileType(URL url, String mediaType,
            List<String> allowedMediaTypes) {
        return new FileDownloadException(
                "The file could not be downloaded from '{}' as the detected media type '{}' is not in the list of allowed media types [{}]",
                url, mediaType, StringUtils.join(allowedMediaTypes, ", "));
    }

    public static FileDownloadException notDownloadableForReason(URL url, String reason) {
        return new FileDownloadException(
                "The file could not be downloaded from '" + url + "' due to " + reason);
    }

    public static FileDownloadException notDownloadableForException(URL url, Exception e) {
        return new FileDownloadException(
                "The file could not be downloaded from '" + url + "': " + e.toString(), e);
    }

    public static FileDownloadException notDownloadableForInvalidUrl(String url, Exception e) {

        return new FileDownloadException(
                "The file could not be downloaded from '" + url + "': " + e.toString(), e);
    }

}
