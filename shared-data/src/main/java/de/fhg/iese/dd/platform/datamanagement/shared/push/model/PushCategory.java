/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.push.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Table(indexes = {
        @Index(columnList = "created"),
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class PushCategory extends BaseEntity implements NamedEntity {

    @ManyToOne
    private App app;

    @Column
    private String name;

    @Column(length = BaseEntity.DESCRIPTION_LENGTH)
    private String description;

    /**
     * Machine readable subject that helps to find push categories for a
     * specific subject across multiple apps without the need to explicitly
     * identify it by id.
     * <p/>
     * An example are the "motivation" push categories for achievements and
     * Digitaler. They are present in all apps and need to be found for pushing
     * "general" achievements to all of them.
     */
    @Column(nullable=true)
    private String subject;

    @Column(nullable=false)
    private boolean defaultLoudPushEnabled;

    @Default
    @Column(nullable=false)
    private boolean configurable = true;

    @Default
    @Column(nullable = false)
    private boolean visible = true;

    @ManyToOne(optional = true)
    private PushCategory parentCategory;

    @Column(nullable = false)
    private int orderValue;

    public PushCategory withOrderValue(int orderValue) {
        this.orderValue = orderValue;
        return this;
    }

    public PushCategory withId(PushCategoryId pushCategoryId) {
        id = pushCategoryId.id;
        return this;
    }

    public PushCategory createShallowCopy() {
        PushCategory copy = PushCategory.builder()
                .app(this.app)
                .name(this.name)
                .description(this.description)
                .subject(this.subject)
                .defaultLoudPushEnabled(this.defaultLoudPushEnabled)
                .configurable(this.configurable)
                .visible(this.visible)
                .parentCategory(this.parentCategory)
                .orderValue(this.orderValue)
                .build();
        copy.setId(this.id);
        return copy;
    }

    @Override
    public String toString() {
        return "PushCategory ["
                + "id=" + id + ", "
                + (app != null ? "app=" + app.getAppIdentifier() + ", " : "")
                + "name=" + name + ", "
                + (parentCategory != null ? "parentCategory=" + parentCategory.getId() + ", " : "")
                + "orderValue=" + orderValue + "]";
    }

    @Getter
    @AllArgsConstructor
    @EqualsAndHashCode
    public static class PushCategoryId {

        private final String id;

        public static PushCategoryId id(String id) {
            return new PushCategoryId(id);
        }

        public boolean hasSameId(PushCategory pushCategory) {
            return StringUtils.equals(pushCategory.getId(), getId());
        }

        @Override
        public String toString() {
            return "PushCategoryId [" +
                    "id='" + id + "'" +
                    ']';
        }

    }

}
