/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2022 Axel Wickenkamp, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import java.io.InputStream;
import java.time.Duration;
import java.util.List;
import java.util.function.BiFunction;

import org.apache.tika.config.TikaConfig;
import org.springframework.lang.Nullable;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileTypeNotDetectableException;
import lombok.Builder;
import lombok.Data;

public interface IFileStorage {

    @Data
    @Builder
    class MediaTypeAndFileExtension {

        private String mediaType;
        private String fileExtension;

    }

    /**
     * Tries to determine the media type and the file extension (without leading dot) of a given file snippet. The file
     * name can be provided to decide between types that have been identified based on the content, e.g. CSV if the
     * content was identified as text.
     * <p>
     * If it can not be determined <code>("application/octet-stream", "bin")</code> is returned.
     *
     * @param content  the content of the file to analyze
     * @param fileName the name of the file, can be empty or null
     *
     * @return media/mime type and file extension(without leading dot)
     *
     * @throws FileTypeNotDetectableException if the content can not be read or an internal exception occurs while doing
     *                                        the detection.
     */
    MediaTypeAndFileExtension determineMediaTypeAndFileExtension(byte[] content, @Nullable String fileName)
            throws FileTypeNotDetectableException;

    void copyFileFromDefault(String defaultInternalFileName, String internalFileName) throws FileStorageException;

    boolean fileExistsDefault(String defaultInternalFileName);

    void saveFile(byte[] content, String mimeType, String internalFileName) throws FileStorageException;

    void copyFile(String internalFileNameSource, String internalFileNameTarget) throws FileStorageException;

    void deleteFile(String internalFileName) throws FileStorageException;

    String getExternalBaseUrl();

    String getExternalUrl(String internalFileName);

    List<String> splitFileName(String fileName);

    String sanitizeFileName(String rawFileName);

    /**
     * @param folderName the name of the folder, e.g <code>images/folder</code> or <code>images/folder/</code>
     * @param fileName   the name of the file, e.g <code>image.jpg</code> or <code>/image.jpg</code>
     *
     * @return the correct concatenated file name  e.g <code>images/folder/image.jpg</code>
     */
    String appendFileName(String folderName, String fileName);

    /**
     * Extracts the initial part of the file name and returns its relative path. For the input
     * <code>http://digitale-dorfer/static/images/testImage.jpg</code> it returns
     * <code>images/testImage.jpg</code>
     *
     * @param externalUrl
     *
     * @return
     */
    String getInternalFileName(String externalUrl);

    InputStream getFileFromDefault(String defaultInternalFileName) throws FileStorageException;

    InputStream getFile(String internalFileName) throws FileStorageException;

    boolean fileExists(String internalFileName);

    /**
     * Searches all files in the storage in the given folder
     *
     * @param folder
     *
     * @return A list of all internal file names of the files in the storage in
     *         that folder
     * @throws FileStorageException
     */
    List<String> findAll(String folder) throws FileStorageException;

    /**
     * Moves the file to the trash
     *
     * @param internalFileName The internal name of the file. {@link #getInternalFileName(String)}
     * @throws FileStorageException
     */
    void moveToTrash(String internalFileName) throws FileStorageException;

    /**
     * Creates a ZipFile containing the given files
     *
     * @param internalZipFileName the internal name of the zip file to create
     * @param internalFileNames   the internal name of the files to include in the zip file
     * @param pathsToFlatten      the paths to be cut from the file name in the zip file
     *
     * @return The external file location of the zip file
     *
     * @throws FileStorageException if the files could not be read or the zipping fails
     */
    String createZipFile(String internalZipFileName, List<String> internalFileNames, List<String> pathsToFlatten)
            throws FileStorageException;

    /**
     * Get the age of given file (timespan since last modified)
     *
     * @param internalFileName
     *
     * @return Age of file as duration
     *
     * @throws FileStorageException
     */
    Duration getAge(String internalFileName) throws FileStorageException;

    /**
     * Deletes old files or directories with a maximum age (non recursive)
     *
     * @param folder   folder to lookup for files/directories
     * @param maxAge   maximum age for a file
     * @param onDelete (optional) Callback before a file is deleted. If the callback returns false the actual file will
     *                 be skipped
     *
     * @throws FileStorageException if the age could not be determined or the deletion failed
     */
    void deleteOldFiles(final String folder, final Duration maxAge,
            final BiFunction<String, Duration, Boolean> onDelete) throws FileStorageException;

    TikaConfig getTikaConfig();

}
