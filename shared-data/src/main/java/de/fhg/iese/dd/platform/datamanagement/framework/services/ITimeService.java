/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.services;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;

public interface ITimeService {

    long currentTimeMillisUTC();

    ZoneId getLocalTimeZone();

    ZonedDateTime nowLocal();

    ZonedDateTime nowUTC();

    ZonedDateTime toLocalTime(long timestampUTC);

    long toTimeMillis(ZonedDateTime dateTime);

    String toLocalTimeHumanReadable(Long timestampUTC);

    String toLocalTimePreciseHumanReadable(Long timestampUTC);

    /**
     * Set the created time stamp of the entity to {@link #currentTimeMillisUTC()} and return the entity.
     * <p/>
     * This should be used after <strong>all</strong> creation of entities.
     *
     * @param <T>
     * @param entity
     *
     * @return
     */
    <T extends BaseEntity> T setCreatedToNow(T entity);

    ZonedDateTime getSameDayStart(ZonedDateTime now);

    ZonedDateTime getSameWeekStart(ZonedDateTime now);

    ZonedDateTime getSameMonthStart(ZonedDateTime now);

    ZonedDateTime getSameYearStart(ZonedDateTime now);

    ZonedDateTime getPreviousDayStart(ZonedDateTime now);

    ZonedDateTime getPreviousDayEnd(ZonedDateTime now);

    ZonedDateTime getPreviousWeekStart(ZonedDateTime now);

    ZonedDateTime getPreviousWeekEnd(ZonedDateTime now);

    ZonedDateTime getPreviousMonthStart(ZonedDateTime now);

    ZonedDateTime getPreviousMonthEnd(ZonedDateTime now);

}
