/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.feature.model;

import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(indexes = {
        @Index(columnList = "created"),
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class FeatureConfig extends BaseEntity {

    @Column(nullable = false, length = BaseEntity.DESCRIPTION_LENGTH)
    private String featureClass;

    @Column(nullable = false)
    private boolean enabled;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String configValues;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<GeoArea> geoAreasIncluded;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<GeoArea> geoAreasExcluded;

    @Column(nullable = false)
    private boolean geoAreaChildrenIncluded;

    @ManyToOne
    private Tenant tenant;

    @ManyToOne
    private App app;

    @ManyToOne
    private AppVariant appVariant;

    public FeatureConfig withConstantId() {
        final String includedIds;
        if (!CollectionUtils.isEmpty(geoAreasIncluded)) {
            includedIds = geoAreasIncluded.stream()
                    .map(g -> "i_" + g.getId())
                    .collect(Collectors.joining());
        } else {
            includedIds = null;
        }
        final String excludedIds;
        if (!CollectionUtils.isEmpty(geoAreasExcluded)) {
            excludedIds = geoAreasExcluded.stream()
                    .map(g -> "i_" + g.getId())
                    .collect(Collectors.joining());
        } else {
            excludedIds = null;
        }
        generateConstantId(featureClass, includedIds, excludedIds, Boolean.toString(geoAreaChildrenIncluded),
                getIdOf(tenant), getIdOf(app), getIdOf(appVariant));
        return this;
    }

    @Override
    public String toString() {
        return "FeatureConfig [" +
                "id='" + id + "', " +
                "featureClass='" + featureClass + "', " +
                (!CollectionUtils.isEmpty(geoAreasIncluded) ? "geoAreasIncluded=" + geoAreasIncluded + ", " : "") +
                (!CollectionUtils.isEmpty(geoAreasExcluded) ? "geoAreasExcluded=" + geoAreasExcluded + ", " : "") +
                "geoAreaChildrenIncluded=" + geoAreaChildrenIncluded + ", " +
                "tenant='" + (tenant != null ? tenant.getId() : "null") + "'," +
                "app='" + (app != null ? app.getAppIdentifier() : "null") + "'," +
                "appVariant='" + (appVariant != null ? appVariant.getAppVariantIdentifier() : "null") + "'" +
                ']';
    }

}
