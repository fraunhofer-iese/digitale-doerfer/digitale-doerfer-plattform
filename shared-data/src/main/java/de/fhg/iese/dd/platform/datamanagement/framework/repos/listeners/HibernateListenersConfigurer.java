/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.repos.listeners;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Configures hibernate to use our {@link GenericDeleteEventListener} when
 * deleting entities.
 *
 * Can be extended to support other generic listeners.
 * <p/>
 * <b> Do not add your entity specific listeners here, the generic listeners are
 * intended for that.</b>
 *
 */
@Component
public class HibernateListenersConfigurer {

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private GenericDeleteEventListener listener;

    @PostConstruct
    public void registerListeners() {
        final SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        final EventListenerRegistry registry =
                sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        registry.getEventListenerGroup(EventType.PRE_DELETE).appendListener(listener);
        registry.getEventListenerGroup(EventType.POST_DELETE).appendListener(listener);
    }

}
