/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.legal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.enums.LegalTextType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Table(indexes = {
        @Index(columnList = "legalTextIdentifier", unique = true),
})
@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class LegalText extends BaseEntity implements NamedEntity {

    @ManyToOne
    private AppVariant appVariant;

    @Column(nullable = false)
    private String legalTextIdentifier;

    @Column(nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(nullable=false)
    private LegalTextType legalTextType;

    @Column(nullable=true)
    private int orderValue;

    @Column(nullable=false)
    private boolean required;

    @Column(nullable=false)
    private long dateActive;

    @Column(nullable=true)
    private long dateInactive;

    @Column(nullable=false, length = BaseEntity.DESCRIPTION_LENGTH)
    private String urlText;

    /**
     * Set a constant id by using the unique legalTextIdentifier
     *
     * @return same entity with a constant id
     */
    public LegalText withConstantId() {
        super.generateConstantId(legalTextIdentifier);
        return this;
    }

}
