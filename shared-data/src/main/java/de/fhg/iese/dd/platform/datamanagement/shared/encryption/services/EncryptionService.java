/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.encryption.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.smile.SmileFactory;
import de.fhg.iese.dd.platform.datamanagement.shared.encryption.config.EncryptionConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.encryption.config.EncryptionConfig.EncryptionKeyConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.encryption.exceptions.EncryptionException;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Service
@Log4j2
class EncryptionService implements IEncryptionService {

    private static final int KEY_IDENTIFIER_LENGTH = 3;
    private static final int KEY_IDENTIFIER_LENGTH_EXTERNAL = 1;

    private final Map<String, EncryptionKey> encryptionKeys;
    private final EncryptionKey currentKey;

    private final Map<String, EncryptionKey> encryptionKeysExternal;
    private final EncryptionKey currentKeyExternal;

    private final ObjectMapper smileObjectMapper = createSmileObjectMapper();

    @Getter
    private static final class EncryptionKey {

        public EncryptionKey(String keyIdentifier, SecretKey key) {
            this.keyIdentifier = keyIdentifier;
            this.key = key;
        }

        private final String keyIdentifier;
        private final SecretKey key;
        @Setter
        private Cipher encryptCipher;
        @Setter
        private Cipher decryptCipher;

    }

    @SuppressFBWarnings(value = "CT_CONSTRUCTOR_THROW",
            justification = "If there are exceptions the application startup needs to stop, since the encryption is crucial")
    EncryptionService(EncryptionConfig encryptionConfig) {

        encryptionKeys = new HashMap<>();
        encryptionKeysExternal = new HashMap<>();

        for (EncryptionKeyConfig encryptionKeyConfig : encryptionConfig.getKeys()) {
            String keyIdentifier = encryptionKeyConfig.getId();
            String algorithm = encryptionKeyConfig.getAlgorithm();
            String encodedKey = encryptionKeyConfig.getKey();
            SecretKey key = toSecretKey(encodedKey, algorithm);
            if (StringUtils.length(keyIdentifier) != KEY_IDENTIFIER_LENGTH) {
                throw new IllegalStateException("Key identifier '" + keyIdentifier + "' has invalid length");
            }
            encryptionKeys.put(keyIdentifier, new EncryptionKey(keyIdentifier, key));
            log.info("Added {} encryption key with identifier {}", algorithm, keyIdentifier);
        }

        for (EncryptionKeyConfig encryptionKeyConfig : encryptionConfig.getExternalKeys()) {
            String keyIdentifier = encryptionKeyConfig.getId();
            String algorithm = encryptionKeyConfig.getAlgorithm();
            String encodedKey = encryptionKeyConfig.getKey();
            SecretKey key = toSecretKey(encodedKey, algorithm);
            if (StringUtils.length(keyIdentifier) != KEY_IDENTIFIER_LENGTH_EXTERNAL) {
                throw new IllegalStateException("Key identifier external '" + keyIdentifier + "' has invalid length");
            }
            encryptionKeysExternal.put(keyIdentifier, new EncryptionKey(keyIdentifier, key));
            log.info("Added {} external encryption key with identifier {}", algorithm, keyIdentifier);
        }

        String currentKeyIdentifier = encryptionConfig.getCurrentKeyId();
        currentKey = encryptionKeys.get(currentKeyIdentifier);
        log.info("Using current encryption key with identifier {}", currentKeyIdentifier);
        if (currentKey == null) {
            log.error("Current encryption key '{}' not existing, configuration error!",
                    currentKeyIdentifier);
            throw new IllegalStateException(
                    "Failed to get the current key using identifier '" + currentKeyIdentifier + "'");
        }

        String currentKeyIdentifierExternal = encryptionConfig.getCurrentExternalKeyId();
        currentKeyExternal = encryptionKeysExternal.get(currentKeyIdentifierExternal);
        log.info("Using current external encryption key with identifier {}", currentKeyIdentifierExternal);
        if (currentKeyExternal == null) {
            log.error("Current external encryption key '{}' not existing, configuration error!",
                    currentKeyIdentifierExternal);
            throw new IllegalStateException(
                    "Failed to get the current external key using identifier '" + currentKeyIdentifierExternal + "'");
        }
    }

    private SecretKey toSecretKey(String encodedKey, String algorithm) {
        // decode the base64 encoded key string
        byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
        // rebuild key using SecretKeySpec
        return new SecretKeySpec(decodedKey, 0, decodedKey.length, algorithm);
    }

    private ObjectMapper createSmileObjectMapper() {

        return new ObjectMapper(new SmileFactory())
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Override
    public String generateKey(String algorithm, Integer keySize) throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(algorithm);
        if (keySize != null) {
            keyGenerator.init(keySize);
        }
        // create new key
        SecretKey secretKey = keyGenerator.generateKey();
        // get base64 encoded version of the key
        return Base64.getEncoder().encodeToString(secretKey.getEncoded());
    }

    @Override
    public String encrypt(String message) throws EncryptionException {
        if (StringUtils.isEmpty(message)) {
            //if there is no message, do not encrypt it
            return null;
        }
        EncryptionKey encryptionKey = currentKey;
        try {
            //convert the message to bytes
            byte[] plainBytes = message.getBytes(StandardCharsets.UTF_8);
            //do the actual encryption
            byte[] encryptedBytes = getEncryptCipher(encryptionKey).doFinal(plainBytes);
            // identifier of the key as prefix + encrypted bytes as encoded base64 string
            return encryptionKey.getKeyIdentifier() + Base64.getEncoder().encodeToString(encryptedBytes);
        } catch (Exception e) {
            throw new EncryptionException(
                    "Failed to encrypt using key identifier '" + encryptionKey.getKeyIdentifier() + "'", e);
        }
    }

    @Override
    public String encryptForExternalUse(String message) throws EncryptionException {
        if (StringUtils.isEmpty(message)) {
            //if there is no message, do not encrypt it
            return null;
        }
        //convert the message to bytes and encrypt it
        return encryptBytesForExternalUse(message.getBytes(StandardCharsets.UTF_8));
    }

    private String encryptBytesForExternalUse(byte[] message) throws EncryptionException {
        if (ArrayUtils.isEmpty(message)) {
            //if there is no message, do not encrypt it
            return null;
        }
        EncryptionKey encryptionKey = currentKeyExternal;
        try {
            //do the actual encryption
            byte[] encryptedBytes = getEncryptCipher(encryptionKey).doFinal(message);
            //encode the encrypted bytes url safe
            String encodedEncrypted = Base64.getUrlEncoder().encodeToString(encryptedBytes);
            //replace the padding with url safe dots
            String urlSafeEncoded = encodedEncrypted.replace('=', '.');
            // identifier of the key as prefix + encrypted bytes as url safe encoded base64 string
            return encryptionKey.getKeyIdentifier() + urlSafeEncoded;
        } catch (Exception e) {
            throw new EncryptionException(
                    "Failed to encrypt using external key identifier '" + encryptionKey.getKeyIdentifier() + "'", e);
        }
    }

    @Override
    public String decrypt(String encryptedMessage) throws EncryptionException {
        if (StringUtils.isEmpty(encryptedMessage)) {
            //if there is no message, do not decrypt it
            return null;
        }
        String keyIdentifier = StringUtils.substring(encryptedMessage, 0, KEY_IDENTIFIER_LENGTH);
        EncryptionKey encryptionKey = encryptionKeys.get(keyIdentifier);
        if (encryptionKey == null) {
            // we assume that if we did not find any key it is an unencrypted (legacy) message
            // in the internal case this can happen with older messages in the db
            return encryptedMessage;
        }
        //cut off the chars required for identifying the key and the optional external identifier
        String encryptedRawMessage = StringUtils.substring(encryptedMessage,
                KEY_IDENTIFIER_LENGTH);
        try {
            //transform the base64 string into the actual encrypted bytes
            byte[] encryptedBytes = Base64.getDecoder().decode(encryptedRawMessage);
            //do the actual decryption
            byte[] plainBytes = getDecryptCipher(encryptionKey).doFinal(encryptedBytes);
            //convert the bytes to a string with the right encoding8
            return new String(plainBytes, StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new EncryptionException("Failed to decrypt using key identifier '" + keyIdentifier + "'", e);
        }
    }

    @Override
    public String decryptFromExternalUse(String encryptedMessage) throws EncryptionException {
        if (StringUtils.isEmpty(encryptedMessage)) {
            //if there is no message, do not decrypt it
            return null;
        }
        // create a string from the decrypted bytes
        return new String(decryptBytesFromExternalUse(encryptedMessage), StandardCharsets.UTF_8);
    }

    private byte[] decryptBytesFromExternalUse(String encryptedMessage) throws EncryptionException {
        if (StringUtils.isEmpty(encryptedMessage)) {
            //if there is no message, do not decrypt it
            return new byte[0];
        }
        String keyIdentifier = StringUtils.substring(encryptedMessage, 0, KEY_IDENTIFIER_LENGTH_EXTERNAL);
        EncryptionKey encryptionKey = encryptionKeysExternal.get(keyIdentifier);
        if (encryptionKey == null) {
            // in the external case we fail if the key is not available anymore, we never want to allow unencrypted messages
            throw new EncryptionException("Failed to decrypt, invalid message");
        }
        //cut off the chars required for identifying the key and the optional external identifier
        String encryptedRawMessage = StringUtils.substring(encryptedMessage,
                KEY_IDENTIFIER_LENGTH_EXTERNAL);
        try {
            //transform the base64 string into the actual encrypted bytes
            String paddedEncryptedMessage = encryptedRawMessage.replace('.', '=');
            //external encryption uses url safe base64 encoding
            byte[] encryptedBytes = Base64.getUrlDecoder().decode(paddedEncryptedMessage);
            //do the actual decryption
            return getDecryptCipher(encryptionKey).doFinal(encryptedBytes);
        } catch (Exception e) {
            //in the external case we do not want to expose so much information
            throw new EncryptionException("Failed to decrypt, invalid message", e);
        }
    }

    /**
     * Gets the cached decrypt cipher or creates a new one and caches it.
     */
    private Cipher getDecryptCipher(EncryptionKey encryptionKey)
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        Cipher cipher = encryptionKey.getDecryptCipher();
        if (cipher == null) {
            SecretKey secretKey = encryptionKey.getKey();
            //initialize the cipher with the correct key and the right mode
            cipher = Cipher.getInstance(secretKey.getAlgorithm());
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            encryptionKey.setDecryptCipher(cipher);
        }
        return cipher;
    }

    /**
     * Gets the cached encrypt cipher or creates a new one and caches it.
     */
    private Cipher getEncryptCipher(EncryptionKey encryptionKey)
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        Cipher cipher = encryptionKey.getEncryptCipher();
        if (cipher == null) {
            SecretKey secretKey = encryptionKey.getKey();
            //initialize the cipher with the correct key and the right mode
            cipher = Cipher.getInstance(secretKey.getAlgorithm());
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            encryptionKey.setEncryptCipher(cipher);
        }
        return cipher;
    }

    @Override
    public String encryptObjectForExternalUse(Object o) throws EncryptionException {
        try {
            // smile is a binary json format that helps to reduce the size of the result
            // see https://github.com/FasterXML/smile-format-specification
            return encryptBytesForExternalUse(smileObjectMapper.writeValueAsBytes(o));
        } catch (JsonProcessingException e) {
            throw new EncryptionException("Failed to encrypt object: " + e, e);
        }
    }

    @Override
    public <C> C decryptObjectFromExternalUse(String encryptedObject, Class<C> returnType) throws EncryptionException {
        try {
            // see encryptObjectForExternalUse for details
            return smileObjectMapper.readValue(decryptBytesFromExternalUse(encryptedObject), returnType);
        } catch (IOException e) {
            throw new EncryptionException("Failed to decrypt object: " + e, e);
        }
    }

}
