/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.legal.repos;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;

@Transactional(readOnly = true)
public interface LegalTextRepository extends JpaRepository<LegalText, String> {

    List<LegalText> findAllByLegalTextIdentifierInOrderByOrderValue(Collection<String> legalTextIdentifier);

    @Query(value = "select l from LegalText l " +
            "where (l.appVariant = :appVariant or l.appVariant is null)" +
            "and l.dateActive <= :currentTime " +
            "and l.dateInactive > :currentTime " +
            "order by l.orderValue")
    List<LegalText> findAllByAppVariantOrAppVariantNullAndCurrentlyActiveOrderByOrderValue(
            @Param("appVariant")AppVariant appVariant,
            @Param("currentTime")long currentTime);

    @Transactional(readOnly = false)
    long deleteAllByAppVariant(AppVariant appVariant);

}
