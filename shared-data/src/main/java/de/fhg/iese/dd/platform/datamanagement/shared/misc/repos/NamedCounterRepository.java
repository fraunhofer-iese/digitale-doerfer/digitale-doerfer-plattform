/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.misc.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.NamedCounter;

@Transactional
public interface NamedCounterRepository extends JpaRepository<NamedCounter,String> {

    NamedCounter findByName(String name);
    boolean existsByName(String name);

    @Modifying
    @Query(value = "UPDATE named_counter c SET c.counter_value = c.counter_value + 1 WHERE name = ?1",
            nativeQuery = true)
    int increaseCounterByName(String name);

    @Modifying
    @Query(value = "UPDATE named_counter c SET c.counter_value = c.counter_value - 1 WHERE name = ?1",
            nativeQuery = true)
    int decreaseCounterByName(String name);

}
