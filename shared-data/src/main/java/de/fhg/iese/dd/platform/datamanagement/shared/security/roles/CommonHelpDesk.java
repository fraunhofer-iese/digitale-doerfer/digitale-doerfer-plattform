/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.security.roles;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

@Component
public class CommonHelpDesk extends BaseRole<RelatedEntityIsNull> {

    private CommonHelpDesk() {
        super(RelatedEntityIsNull.class);
    }

    @Override
    public String getDisplayName() {
        return "Ansprechperson (Global)";
    }

    @Override
    public String getDescription() {
        return "Eine Ansprechperson für alle Kunden, die keine spezielle Ansprechperson innerhalb ihres Mandanten haben.";
    }

    @Override
    public Tenant getTenantOfRelatedEntity(RelatedEntityIsNull relatedEntity) {
        return null;
    }

    @Override
    public RelatedEntityIsNull getRelatedEntityById(String relatedEntityId) {
        return null;
    }

    @Override
    public boolean existsRelatedEntity(String relatedEntityId) {
        return relatedEntityId == null;
    }

}
