/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2022 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class FileItemUploadException extends BadRequestException {

    private static final long serialVersionUID = -6310719278984885129L;

    public FileItemUploadException(String message, Object... arguments) {
        super(message, arguments);
    }

    public FileItemUploadException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileItemUploadException(String message) {
        super(message);
    }

    public FileItemUploadException(Throwable cause) {
        super(cause);
    }

    public FileItemUploadException(String mimeType, List<String> allowedMediaTypes) {
        super("The file could not be uploaded as the detected media type '{}' is not in the list of allowed media types [{}]",
                mimeType, StringUtils.join(allowedMediaTypes, ", "));
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.FILE_ITEM_UPLOAD_FAILED;
    }

}
