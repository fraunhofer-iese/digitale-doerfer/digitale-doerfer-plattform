/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.app.repos;

import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantTenantContract;

public interface AppVariantTenantContractRepository extends JpaRepository<AppVariantTenantContract, String> {

    Set<AppVariantTenantContract> findAllByTenant(Tenant tenant);

    Set<AppVariantTenantContract> findAllByTenantAndAppVariant(Tenant tenant, AppVariant appVariant);

    @Query("select distinct c.tenant " +
            "from AppVariantTenantContract c " +
            "where c.appVariant = :appVariant ")
    Page<Tenant> findTenantsByAppVariant(AppVariant appVariant, Pageable page);

    @Query("select distinct c.tenant " +
            "from AppVariantTenantContract c " +
            "where (c.id in :contractIds or c.appVariant.id in :appVariantIds) ")
    Page<Tenant> findTenantsByContractOrAppVariant(Set<String> contractIds, Set<String> appVariantIds, Pageable page);

    @Query("select distinct c.tenant " +
            "from AppVariantTenantContract c " +
            "where (" +
            "    (c.id in :contractIds or c.appVariant.id in :appVariantIds) " +
            "    and" +
            "    c.tenant.name like :searchTerm" +
            ")")
    Page<Tenant> findTenantsByContractOrAppVariantAndSearchTerm(Set<String> contractIds, Set<String> appVariantIds,
            String searchTerm, Pageable page);

    @Transactional
    long deleteByAppVariant(AppVariant appVariant);

    @Query("select distinct c.appVariant " +
            "from AppVariantTenantContract c " +
            "where c.id in :contractIds or c.tenant.id in :tenantIds ")
    Page<AppVariant> findAppVariantsByTenantOrContract(Set<String> tenantIds, Set<String> contractIds, Pageable page);

    @Query("select distinct c.appVariant " +
            "from AppVariantTenantContract c " +
            "where (" +
            "    (c.id in :contractIds or c.tenant.id in :tenantIds) " +
            "    and" +
            "    (c.appVariant.name like :searchTerm or c.appVariant.appVariantIdentifier like :searchTerm)" +
            ")")
    Page<AppVariant> findAppVariantsByTenantOrContractAndSearchTerm(Set<String> tenantIds, Set<String> contractIds,
            String searchTerm, Pageable page);

    Page<AppVariantTenantContract> findAllByNotesLike(String searchTerm, Pageable page);

    @Query("select c " +
            "from AppVariantTenantContract c " +
            "where c.tenant.id in :tenantIds or c.appVariant.id in :appVariantIds ")
    Page<AppVariantTenantContract> findAllByTenantOrAppVariant(Set<String> tenantIds, Set<String> appVariantIds,
            Pageable page);

    @Query("select c " +
            "from AppVariantTenantContract c " +
            "where (" +
            "    (c.tenant.id in :tenantIds or c.appVariant.id in :appVariantIds) " +
            "    and" +
            "    c.notes like :searchTerm" +
            ")")
    Page<AppVariantTenantContract> findAllByTenantOrAppVariantAndSearchTerm(Set<String> tenantIds,
            Set<String> appVariantIds, String searchTerm, Pageable page);

}
