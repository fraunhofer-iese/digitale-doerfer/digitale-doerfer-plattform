/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.legal.repos;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalTextAcceptance;

@Transactional(readOnly = true)
public interface LegalTextAcceptanceRepository extends JpaRepository<LegalTextAcceptance, String> {

    List<LegalTextAcceptance> findAllByPersonAndLegalTextInOrderByTimestampDesc(Person person,
            Collection<LegalText> legalTexts);

    List<LegalTextAcceptance> findAllByPersonOrderByTimestampDesc(Person person);

    LegalTextAcceptance findByPersonAndLegalText(Person person, LegalText legalText);

    @Transactional(readOnly = false)
    long deleteAllByLegalTextAppVariant(AppVariant appVariant);

    @Transactional(readOnly = false)
    long deleteAllByPerson(Person person);

}
