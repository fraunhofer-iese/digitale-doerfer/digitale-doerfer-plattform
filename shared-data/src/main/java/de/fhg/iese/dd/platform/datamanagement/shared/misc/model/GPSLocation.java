/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.misc.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode //this is extremely important for the cache in DirectionsController to work
public class GPSLocation {

    @Column
    private double latitude;

    @Column
    private double longitude;

    @JsonIgnore
    public boolean isValid(){
        //if both lat & long are zero the GPS is most likely not initialized
        return (latitude != 0.0 && longitude != 0.0) &&
                //if lat or long is NaN or infinite it is invalid
                Double.isFinite(latitude) &&
                Double.isFinite(longitude);
    }

    @Override
    public String toString() {
        return "GPSLocation[" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ']';
    }

}
