/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2020 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.participants.person.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.enums.StorableEnumSet;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.DeletableEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.AccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * Entity class representing a user.
 */
@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@NamedEntityGraphs({
        @NamedEntityGraph(name = "Person.reduced"),
        @NamedEntityGraph(name = "Person.normal",
                attributeNodes = {
                        @NamedAttributeNode(value = "profilePicture"),
                        @NamedAttributeNode(value = "homeArea"),
                        @NamedAttributeNode(value = "community")
                }),
        @NamedEntityGraph(name = "Person.full",
                includeAllAttributes = true),
})
@Table(indexes = {
        @Index(columnList = "email", unique = true),
        @Index(columnList = "pendingNewEmail", unique = true),
        @Index(columnList = "oauthId", unique = true),
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class Person extends BaseEntity implements DeletableEntity {

    @ManyToOne
    private Tenant community;

    @OneToMany(fetch = FetchType.LAZY)
    private Set<AddressListEntry> addresses;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String nickName;

    /**
     * Associated MediaItem.
     * </p>
     * There is no need to delete the files associated with the MediaItem manually before deleting this entity.
     *
     * @see MediaItem for more details
     */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private MediaItem profilePicture;

    @Column
    private String phoneNumber;

    //unique with index, do not set unique = true to prevent hibernate creating additional constraint
    @Column(nullable = false)
    private String email;

    @Column
    private String pendingNewEmail;

    @Column
    private Integer numberOfEmailChanges;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @Builder.Default
    private AccountType accountType = AccountType.OTHER;

    //unique with index, do not set unique = true to prevent hibernate creating additional constraint
    @Column
    private String oauthId;

    @Column
    @Builder.Default
    private long lastLoggedIn = System.currentTimeMillis();

    @Column(nullable = false)
    @Builder.Default
    private int status = 0;

    @Column(nullable = false)
    @Builder.Default
    private boolean deleted = false;

    @ManyToOne
    private GeoArea homeArea;

    /**
     * Bitmask value of {@link #getVerificationStatuses()}. Do not modify this directly, use {@link
     * #getVerificationStatuses()} instead. Changes there are directly reflected on this value.
     */
    @Column(nullable = false)
    @Builder.Default
    private int verificationStatus = 0;

    /**
     * Null if no warning has been sent
     */
    @Column
    private Long lastSentTimePendingDeletionWarning;

    /**
     * Null if no verification email has been sent
     */
    @Column
    private Long lastSentTimeEmailVerification;

    /**
     * Null if the person is not deleted
     */
    @Column
    private Long deletionTime;

    /**
     * Statuses of the person. Changes are directly reflected at {@link #status}, which is the value that is actually
     * stored.
     */
    @Transient
    @Getter(lazy = true)
    @Setter(value = AccessLevel.NONE)
    private final StorableEnumSet<PersonStatus> statuses =
            new StorableEnumSet<>(this::setStatus, this::getStatus,
                    PersonStatus.class);

    /**
     * Verification statuses of the person. Changes are directly reflected at {@link #verificationStatus}, which is the
     * value that is actually stored.
     */
    @Transient
    @Getter(lazy = true)
    @Setter(value = AccessLevel.NONE)
    private final StorableEnumSet<PersonVerificationStatus> verificationStatuses =
            new StorableEnumSet<>(this::setVerificationStatus, this::getVerificationStatus,
                    PersonVerificationStatus.class);

    /**
     * @return "First Last"
     */
    @JsonIgnore
    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    /**
     * @return "First L."
     */
    @JsonIgnore
    public String getFirstNameFirstCharLastDot() {
        return this.firstName + " " + StringUtils.substring(this.lastName, 0, 1) + ".";
    }

    @Override
    public String toString() {
        return "Person [" +
                "id='" + id + "', " +
                "firstName='" + firstName + "', " +
                "lastName='" + lastName + "', " +
                "email='" + email + "'" +
                ']';
    }

    /**
     * Use {@link #getTenant()} instead
     */
    @SuppressWarnings("unused")
    @Deprecated
    private Tenant getCommunity() {
        return community;
    }

    /**
     * Use {@link #setTenant(Tenant)} instead
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void setCommunity(Tenant community) {
        this.community = community;
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @param tenant the tenant of the person
     */
    public void setTenant(Tenant tenant) {
        this.community = tenant;
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @return the tenant of the person
     */
    public Tenant getTenant() {
        return community;
    }

    @JsonIgnore
    public boolean isEmailVerified() {
        return this.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED);
    }

}
