/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services.init;

import java.io.InputStream;
import java.util.Collection;
import java.util.regex.Pattern;

import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import lombok.extern.log4j.Log4j2;

@Log4j2
abstract class BaseDataInitializerFileService implements IDataInitializerFileService {

    protected abstract IFileProvider getFileProvider();

    @Override
    public String checkoutInitData(LogSummary logSummary) throws DataInitializationException {
        return getFileProvider().checkoutInitData(logSummary);
    }

    @Override
    public Collection<String> findDataInitFiles(Pattern pattern) {
        return getFileProvider().findDataInitFiles(pattern);
    }

    @Override
    public InputStream loadInitDataFile(String location) {
        return getFileProvider().loadInitDataFile(location);
    }

    @Override
    public void cleanup(LogSummary logSummary) {
        getFileProvider().cleanup(logSummary);
    }

}
