/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.participants.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;

public class PersonTest {

    @Test
    public void getVerificationStatuses() {

        PersonVerificationStatus status1 = PersonVerificationStatus.EMAIL_VERIFIED;
        PersonVerificationStatus status2 = PersonVerificationStatus.PHONE_NUMBER_VERIFIED;

        int statusValue = status1.getBitMaskValue() | status2.getBitMaskValue();

        Person person = new Person();

        person.setVerificationStatus(statusValue);

        Set<PersonVerificationStatus> actualStatuses = person.getVerificationStatuses().getValues();
        assertThat(actualStatuses).containsExactlyInAnyOrder(status1, status2);

        person.setVerificationStatus(0);
    }

    @Test
    public void getVerificationStatuses_NoStatus() {

        Person person = new Person();

        person.setVerificationStatus(0);

        Set<PersonVerificationStatus> actualStatuses = person.getVerificationStatuses().getValues();
        assertEquals(0, actualStatuses.size());
    }

    @Test
    public void getVerificationStatuses_InvalidStatus() {

        Person person = new Person();

        //the values do not exist
        person.setVerificationStatus(0b0_1111_0000_0000);

        Set<PersonVerificationStatus> actualStatuses = person.getVerificationStatuses().getValues();
        assertEquals(0, actualStatuses.size());
    }

    @Test
    public void setVerificationStatuses() {

        Person person = new Person();

        PersonVerificationStatus status1 = PersonVerificationStatus.EMAIL_VERIFIED;
        PersonVerificationStatus status2 = PersonVerificationStatus.NAME_VERIFIED;

        Set<PersonVerificationStatus> expectedStatuses = Collections.unmodifiableSet(EnumSet.of(status1, status2));
        int expectedStatusValue = status1.getBitMaskValue() | status2.getBitMaskValue();

        person.getVerificationStatuses().setValues(expectedStatuses);

        assertEquals(expectedStatusValue, person.getVerificationStatus());

        Set<PersonVerificationStatus> actualStatuses = person.getVerificationStatuses().getValues();
        assertThat(actualStatuses).containsExactlyInAnyOrder(status1, status2);
    }

    @Test
    public void setVerificationStatuses_NoStatus() {

        Person person = new Person();

        person.getVerificationStatuses().setValues(Collections.emptySet());

        assertEquals(0, person.getVerificationStatus());

        Set<PersonVerificationStatus> actualStatuses = person.getVerificationStatuses().getValues();
        assertEquals(0, actualStatuses.size());
    }

    @Test
    public void addVerificationStatus() {

        Person person = new Person();

        PersonVerificationStatus status1 = PersonVerificationStatus.EMAIL_VERIFIED;
        PersonVerificationStatus status2 = PersonVerificationStatus.PHONE_NUMBER_VERIFIED;

        person.getVerificationStatuses().addValue(status1);
        assertEquals(status1.getBitMaskValue(), person.getVerificationStatus());
        //multiple adding should not change anything
        person.getVerificationStatuses().addValue(status1);
        assertEquals(status1.getBitMaskValue(), person.getVerificationStatus());

        person.getVerificationStatuses().addValue(status2);
        assertEquals(status1.getBitMaskValue() | status2.getBitMaskValue(), person.getVerificationStatus());
    }

    @Test
    public void removeVerificationStatus() {

        Person person = new Person();

        PersonVerificationStatus status1 = PersonVerificationStatus.EMAIL_VERIFIED;
        PersonVerificationStatus status2 = PersonVerificationStatus.PHONE_NUMBER_VERIFIED;

        person.getVerificationStatuses().removeValue(status1);
        assertEquals(0, person.getVerificationStatus());

        person.getVerificationStatuses().addValue(status1);
        person.getVerificationStatuses().removeValue(status1);
        assertEquals(0, person.getVerificationStatus());

        person.getVerificationStatuses().addValue(status1);
        person.getVerificationStatuses().addValue(status2);
        person.getVerificationStatuses().removeValue(status1);
        assertEquals(status2.getBitMaskValue(), person.getVerificationStatus());
    }

    @Test
    public void hasVerificationStatus() {

        Person person = new Person();

        PersonVerificationStatus status1 = PersonVerificationStatus.EMAIL_VERIFIED;
        PersonVerificationStatus status2 = PersonVerificationStatus.PHONE_NUMBER_VERIFIED;

        assertFalse(person.getVerificationStatuses().hasValue(status1));

        person.getVerificationStatuses().addValue(status1);
        assertTrue(person.getVerificationStatuses().hasValue(status1));
        person.getVerificationStatuses().removeValue(status1);
        assertFalse(person.getVerificationStatuses().hasValue(status1));

        person.getVerificationStatuses().addValue(status1);
        person.getVerificationStatuses().addValue(status2);
        assertTrue(person.getVerificationStatuses().hasValue(status2));
    }

}
