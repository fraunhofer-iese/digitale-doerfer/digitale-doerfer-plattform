/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.encryption.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.encryption.config.EncryptionConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.encryption.config.EncryptionConfig.EncryptionKeyConfig;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class EncryptionServiceTest {

    private static IEncryptionService encryptionService;
    private static IEncryptionService oldEncryptionService;

    @BeforeAll
    public static void initialize() {
        oldEncryptionService = createEncryptionService("001", "A");
        encryptionService = createEncryptionService("002", "B");
    }

    private static IEncryptionService createEncryptionService(String keyIdentifier, String keyIdentifierExternal) {

        EncryptionConfig encryptionConfig = new EncryptionConfig();
        encryptionConfig.setKeys(Arrays.asList(
                EncryptionKeyConfig.builder()
                        .id("001")
                        .algorithm("AES")
                        .key("ma2Pl++ruywvVmHzdnJ2/o6lyuDWBN/Fn1yAKIfNz7E=")
                        .build(),
                EncryptionKeyConfig.builder()
                        .id("002")
                        .algorithm("AES")
                        .key("6ZYNHmUYm6HvGal9tLZ6tq2gjfzMxq7FAjCp7Uw+gmY=")
                        .build()));
        encryptionConfig.setCurrentKeyId(keyIdentifier);

        encryptionConfig.setExternalKeys(Arrays.asList(
                EncryptionKeyConfig.builder()
                        .id("A")
                        .algorithm("AES")
                        .key("cDgbUib6jSiYs3BTx00dfg==")
                        .build(),
                EncryptionKeyConfig.builder()
                        .id("B")
                        .algorithm("AES")
                        .key("BMMBKE11vKm03WxYa0lMb0vdVBkvzA8AheimrYhSEz4=")
                        .build()));
        encryptionConfig.setCurrentExternalKeyId(keyIdentifierExternal);

        return new EncryptionService(encryptionConfig);
    }

    @Test
    public void generateKey() throws NoSuchAlgorithmException {

        String keyDefaultLength = encryptionService.generateKey("AES", null);

        assertThat(keyDefaultLength).hasSizeGreaterThan(10);
        log.info("Generated key default length: {}", keyDefaultLength);

        String keyCustomLength = encryptionService.generateKey("AES", 256);

        assertThat(keyCustomLength).hasSizeGreaterThan(20);
        log.info("Generated key custom length: {}", keyCustomLength);
    }

    @Test
    public void encryptAndDecrypt() throws UnsupportedEncodingException {
        String secretMessage = "This is my secret message 🔒";

        String encrypted = encryptionService.encrypt(secretMessage);
        log.info("Encrypted: {}", encrypted);
        String encryptedExternal = encryptionService.encryptForExternalUse(secretMessage);
        log.info("Encrypted External: {}", encryptedExternal);

        assertThat(encryptedExternal)
                .as("no url encoding should be necessary")
                .isEqualTo(URLEncoder.encode(encryptedExternal, StandardCharsets.US_ASCII));

        assertThat(encrypted).doesNotContain(secretMessage);
        assertThat(encryptedExternal).doesNotContain(secretMessage).isNotEqualTo(encrypted);

        assertThat(encryptionService.decrypt(encrypted)).isEqualTo(secretMessage);
        assertThat(encryptionService.decryptFromExternalUse(encryptedExternal)).isEqualTo(secretMessage);
    }

    @Test
    public void encryptAndDecrypt_DifferentVersions() {
        String secretMessage = "This is my secret message 🔒";

        String encryptedOld = oldEncryptionService.encrypt(secretMessage);
        String encryptedNew = encryptionService.encrypt(secretMessage);

        assertThat(encryptedOld).doesNotContain(secretMessage);
        assertThat(encryptedOld).isNotEqualTo(encryptedNew);

        //old key is also known to the new service
        assertThat(encryptionService.decrypt(encryptedOld)).isEqualTo(secretMessage);
        //key is also known to the old service
        assertThat(oldEncryptionService.decrypt(encryptedNew)).isEqualTo(secretMessage);
    }

    @Test
    public void encryptAndDecryptObject() throws UnsupportedEncodingException {

        Person person = Person.builder()
                .firstName("Bob")
                .lastName("Alice")
                .nickName("🔒")
                .email("dead@letter.secure")
                .build();

        String encrypted = encryptionService.encryptObjectForExternalUse(person);
        log.info("Encrypted object external {} chars: {}", encrypted.length(), encrypted);

        assertThat(encrypted)
                .doesNotContain(person.getFirstName())
                .doesNotContain(person.getLastName())
                .doesNotContain(person.getNickName())
                .doesNotContain(person.getEmail())
                .doesNotContain(person.getId());

        assertThat(encrypted)
                .as("no url encoding should be necessary")
                .isEqualTo(URLEncoder.encode(encrypted, StandardCharsets.US_ASCII));

        Person decryptedPerson = encryptionService.decryptObjectFromExternalUse(encrypted, Person.class);

        assertThat(decryptedPerson.getId()).isEqualTo(person.getId());
        assertThat(decryptedPerson.getFirstName()).isEqualTo(person.getFirstName());
        assertThat(decryptedPerson.getLastName()).isEqualTo(person.getLastName());
        assertThat(decryptedPerson.getNickName()).isEqualTo(person.getNickName());
        assertThat(decryptedPerson.getEmail()).isEqualTo(person.getEmail());
    }

    @Test
    public void encryptAndDecryptObject_DifferentVersions() {

        Person person = Person.builder()
                .firstName("Bob")
                .lastName("Alice")
                .email("dead@letter.secure")
                .build();

        String encryptedOld = oldEncryptionService.encryptObjectForExternalUse(person);
        String encryptedNew = encryptionService.encryptObjectForExternalUse(person);

        Person decryptedPersonNew = encryptionService.decryptObjectFromExternalUse(encryptedOld, Person.class);
        Person decryptedPersonOld = oldEncryptionService.decryptObjectFromExternalUse(encryptedNew, Person.class);

        assertThat(decryptedPersonNew.getId()).isEqualTo(person.getId());
        assertThat(decryptedPersonOld.getId()).isEqualTo(person.getId());
    }

    @Test
    @Disabled("Only used to manually test the performance")
    public void encryptAndDecrypt_Performance() {
        String secretMessage = StringUtils.repeat("➡️This is my secret message! 🤓🔒", 42);

        long start = System.currentTimeMillis();

        for (int i = 0; i < 100000; i++) {
            String encryptedOld = oldEncryptionService.encrypt(secretMessage);
            String encryptedNew = encryptionService.encrypt(secretMessage);
            encryptionService.decrypt(encryptedOld);
            oldEncryptionService.decrypt(encryptedNew);
        }

        long duration = System.currentTimeMillis() - start;
        log.info("Encrypt / Decrypt took {} ms", duration);

        start = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            String encryptedOldExt = oldEncryptionService.encryptForExternalUse(secretMessage);
            String encryptedNewExt = encryptionService.encryptForExternalUse(secretMessage);
            encryptionService.decryptFromExternalUse(encryptedOldExt);
            oldEncryptionService.decryptFromExternalUse(encryptedNewExt);
        }

        duration = System.currentTimeMillis() - start;
        log.info("Encrypt / Decrypt external took {} ms", duration);
    }

}
