/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2021 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.services;

import java.util.Set;
import java.util.UUID;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

//this name is required since we need to reference the IEnvironmentService in a SPEL expression, which does not support reference by type
@Service("environmentService")
@Profile({"test"})
public class TestEnvironmentService extends EnvironmentService {

    private final ThreadLocal<Set<String>> activeProfilesOverride = new ThreadLocal<>();

    private static final String UNIQUE_TEST_RUN_IDENTIFIER = UUID.randomUUID().toString();

    public void setActiveProfilesOverride(Set<String> activeProfiles) {
        this.activeProfilesOverride.set(activeProfiles);
    }

    @Override
    public String getNodeIdentifier() {
        return super.getNodeIdentifier() + "-" + UNIQUE_TEST_RUN_IDENTIFIER;
    }

    public void removeActiveProfilesOverride() {
        this.activeProfilesOverride.remove();
    }

    @Override
    public Set<String> getActiveProfiles(boolean excludeUnderscoreProfiles) {
        Set<String> activeProfilesOverride = this.activeProfilesOverride.get();
        return activeProfilesOverride != null ? activeProfilesOverride :
                super.getActiveProfiles(excludeUnderscoreProfiles);
    }

}
