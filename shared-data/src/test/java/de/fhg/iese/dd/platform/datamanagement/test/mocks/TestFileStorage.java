/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.test.mocks;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.tika.mime.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.FileStorageConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.BaseFileStorage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

@Component
@Profile("test & !S3FileStorageTest")
@Log4j2
public class TestFileStorage extends BaseFileStorage {

    private static final String UNKNOWN_HASH_CODE = ":-(";
    private static final int UNKNOWN_LENGTH = 1042;
    // set to true for log messages on all operations
    private static final boolean VERBOSE = false;

    private final Map<String, FileAttributes> storedFiles = new HashMap<>();
    private final Map<String, List<String>> zipFileContent = new HashMap<>();

    private final ITimeService timeService;

    @Data
    @AllArgsConstructor
    @ToString
    private static class FileAttributes {

        private String hash;
        private int length;
        private long lastModified;

    }

    @Autowired
    public TestFileStorage(FileStorageConfig fileStorageConfig, ITimeService timeService) {
        super(fileStorageConfig);
        this.timeService = timeService;
    }

    public void reset() {
        storedFiles.clear();
        zipFileContent.clear();
    }

    @Override
    public void copyFileFromDefault(String defaultInternalFileName, String internalFileName)
            throws FileStorageException {
        if (StringUtils.isEmpty(defaultInternalFileName)) {
            throw new FileStorageException("Empty defaultInternalFileName");
        }
        if (StringUtils.isEmpty(internalFileName)) {
            throw new FileStorageException("Empty internalFileName");
        }
        storedFiles.put(internalFileName, new FileAttributes(UNKNOWN_HASH_CODE, UNKNOWN_LENGTH,
                timeService.currentTimeMillisUTC()));
        if (VERBOSE) {
            log.debug("copyFileFromDefault({}, {})", defaultInternalFileName, internalFileName);
        }
    }

    @Override
    public void saveFile(byte[] content, String mimeType, String internalFileName) throws FileStorageException {
        if (content.length == 0) {
            throw new FileStorageException("content empty");
        }
        if (StringUtils.isEmpty(mimeType)) {
            throw new FileStorageException("Empty mimeType");
        }
        if (StringUtils.isEmpty(internalFileName)) {
            throw new FileStorageException("Empty internalFileName");
        }
        storedFiles.put(internalFileName, createFileAttributes(content));
        if (VERBOSE) {
            log.debug("saveFile({}, {}, {})", content.length, mimeType, internalFileName);
        }
    }

    @Override
    public void copyFile(String internalFileNameSource, String internalFileNameTarget) throws FileStorageException {
        FileAttributes fileAttributes = storedFiles.get(internalFileNameSource);
        if (VERBOSE) {
            log.debug("copyFile({}, {})", internalFileNameSource, internalFileNameTarget);
        }
        storedFiles.put(internalFileNameTarget, fileAttributes);
    }

    @NonNull
    public FileAttributes createFileAttributes(byte[] content) {
        return createFileAttributes(content, timeService.currentTimeMillisUTC());
    }

    @NonNull
    private FileAttributes createFileAttributes(byte[] content, long lastModified) {
        return new FileAttributes(toHashCode(content), content.length, lastModified);
    }

    @Override
    public InputStream getFile(String internalFileName) throws FileStorageException {
        if (storedFiles.containsKey(internalFileName)) {
            if (VERBOSE) {
                log.debug("getFile({})", internalFileName);
            }
            return Thread.currentThread().getContextClassLoader().getResourceAsStream("testImage.jpg");
        } else {
            throw new FileStorageException("File {} not found", internalFileName);
        }
    }

    @Override
    public void deleteFile(String internalFileName) throws FileStorageException {
        if (StringUtils.isEmpty(internalFileName)) {
            throw new FileStorageException("Empty internalFileName");
        }
        storedFiles.remove(internalFileName);
        if (VERBOSE) {
            log.debug("deleteFile({})", internalFileName);
        }
    }

    @Override
    public InputStream getFileFromDefault(String defaultInternalFileName) throws FileStorageException {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream("testImage.jpg");
    }

    @Override
    public boolean fileExists(String internalFileName) {
        return storedFiles.containsKey(internalFileName);
    }

    @Override
    public boolean fileExistsDefault(String defaultInternalFileName) {
        return true;
    }

    public void logAllFileAttributes() {
        storedFiles.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .forEach(e -> log.debug("{}: {}", e.getKey(), e.getValue().toString()));
    }

    public void assertFileExistsWithContent(String internalFileName, byte[] expectedContent) {

        assertThat(storedFiles.containsKey(internalFileName))
                .as("File %s does not exist", internalFileName)
                .isTrue();

        String expectedHashCode = toHashCode(expectedContent);
        FileAttributes actualFileAttributes = storedFiles.get(internalFileName);

        assertThat(expectedHashCode)
                .as("File %s does not match content", internalFileName)
                .isEqualTo(actualFileAttributes.getHash());
    }

    public int fileSize(String internalFileName) {
        if (!storedFiles.containsKey(internalFileName)) {
            throw new FileStorageException("File {} not found", internalFileName);
        }
        FileAttributes actualFileAttributes = storedFiles.get(internalFileName);
        return actualFileAttributes.getLength();
    }

    public void addTestFile(String internalFileName, byte[] content, long lastModified) {
        storedFiles.put(internalFileName, createFileAttributes(content, lastModified));
        log.debug("addTestFile({}, {}, {})", internalFileName, content.length, lastModified);
    }

    @Override
    public List<String> findAll(String folderName) throws FileStorageException {
        return storedFiles.keySet().stream()
                .filter(f -> f.startsWith(folderName))
                .collect(Collectors.toList());
    }

    @Override
    public void moveToTrash(String internalFileName) throws FileStorageException {
        if (StringUtils.isEmpty(internalFileName)) {
            throw new FileStorageException("Empty internalFileName");
        }
        storedFiles.remove(internalFileName);
        if (VERBOSE) {
            log.debug("moveToTrash({})", internalFileName);
        }
    }

    private String toHashCode(byte[] content) {

        return DigestUtils.md5DigestAsHex(content);
    }

    @Override
    public String createZipFile(String internalZipFileName, List<String> internalFileNames, List<String> pathsToFlatten)
            throws FileStorageException {

        List<String> zipInternalFileNames = internalFileNames.stream()
                .map(f -> getZipInternalFileName(f, pathsToFlatten))
                .collect(Collectors.toList());

        zipFileContent.put(internalZipFileName, new ArrayList<>(zipInternalFileNames));
        String zipFileNames = zipInternalFileNames.stream()
                .collect(Collectors.joining(", ", "[", "]"));
        //this is just to have a unique content signature for that zip file
        saveFile(("ZipContent: " + zipFileNames).getBytes(StandardCharsets.UTF_8), MediaType.APPLICATION_ZIP.toString(),
                internalZipFileName);
        log.debug("saving zip ({}) with content {}", internalZipFileName, zipFileNames);
        return getExternalUrl(internalZipFileName);
    }

    private String getZipInternalFileName(final String fileName, final List<String> pathsToFlatten){
        if(fileName == null || pathsToFlatten == null || pathsToFlatten.size() == 0) {
            return fileName;
        }

        String zipFileName = fileName;
        for (String pathToFlatten : pathsToFlatten) {
            zipFileName = zipFileName.replace(pathToFlatten, "");
        }

        return zipFileName;
    }

    public List<String> getZippedFiles(String internalZipFileName) {
        return zipFileContent.get(internalZipFileName);
    }

    public List<String> getAllZipFiles() {
        return zipFileContent.keySet().stream()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public Duration getAge(String internalFileName) {

        if (StringUtils.isEmpty(internalFileName)) {
            throw new FileStorageException("Empty internalFileName");
        }

        if (!storedFiles.containsKey(internalFileName)) {
            throw new FileStorageException("File {} not found", internalFileName);
        }

        Duration duration = Duration.ofMillis(
                timeService.currentTimeMillisUTC() - storedFiles.get(internalFileName).getLastModified());
        if (VERBOSE) {
            log.debug("getAge({}): {}", internalFileName, duration);
        }
        return duration;
    }

}
