/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.services;

import lombok.Getter;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

@Service
@Profile("test")
public class TestParallelismService extends ParallelismService implements IParallelismService {

    @Getter
    private final List<ScheduledTask> scheduledTasks = new ArrayList<>();

    @Override
    public ScheduledFuture<?> scheduleOneTimeTask(Runnable task, Instant startTime) {

        ScheduledFuture<?> scheduledFuture = super.scheduleOneTimeTask(task, startTime);
        scheduledTasks.add(new ScheduledTask(task, startTime, scheduledFuture));
        return scheduledFuture;
    }

    public void reset() {

        scheduledTasks.clear();
    }

    public record ScheduledTask(Runnable task, Instant startTime, ScheduledFuture<?> scheduledFuture) {

    }

}
