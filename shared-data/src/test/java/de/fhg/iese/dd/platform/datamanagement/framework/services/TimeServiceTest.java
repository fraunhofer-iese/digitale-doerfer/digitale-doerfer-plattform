/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;

public class TimeServiceTest {

    private TimeService createTimeService() {
        return createTimeService("Europe/Berlin");
    }

    private TimeService createTimeService(String expectedTimeZoneName) {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setLocalTimeZone(expectedTimeZoneName);
        return new TimeService(applicationConfig);
    }

    @Test
    public void getLocalTimeZone() {
        String expectedTimeZoneName = "Europe/Berlin";

        TimeService timeService = createTimeService(expectedTimeZoneName);
        assertEquals(expectedTimeZoneName, timeService.getLocalTimeZone().getId());
    }

    @Test
    public void getSameDayStart() {
        TimeService timeService = createTimeService();

        assertEquals(ZonedDateTime.parse("2019-10-03T00:00:00Z"),
                timeService.getSameDayStart(ZonedDateTime.parse("2019-10-03T20:28:13Z")));
        assertEquals(ZonedDateTime.parse("2018-12-31T00:00:00Z"),
                timeService.getSameDayStart(ZonedDateTime.parse("2018-12-31T10:15:30Z")));
    }

    @Test
    public void getSameWeekStart() {
        TimeService timeService = createTimeService();

        assertEquals(ZonedDateTime.parse("2019-09-30T00:00:00Z"),
                timeService.getSameWeekStart(ZonedDateTime.parse("2019-10-03T20:28:13Z")));
        assertEquals(ZonedDateTime.parse("2018-12-31T00:00:00Z"),
                timeService.getSameWeekStart(ZonedDateTime.parse("2019-01-04T10:15:30Z")));
        assertEquals(ZonedDateTime.parse("2019-10-07T00:00:00Z"),
                timeService.getSameWeekStart(ZonedDateTime.parse("2019-10-07T10:15:30Z")));
    }

    @Test
    public void getSameMonthStart() {
        TimeService timeService = createTimeService();

        assertEquals(ZonedDateTime.parse("2019-10-01T00:00:00Z"),
                timeService.getSameMonthStart(ZonedDateTime.parse("2019-10-03T20:28:13Z")));
        assertEquals(ZonedDateTime.parse("2019-01-01T00:00:00Z"),
                timeService.getSameMonthStart(ZonedDateTime.parse("2019-01-08T10:15:30Z")));
        assertEquals(ZonedDateTime.parse("2019-01-01T00:00:00Z"),
                timeService.getSameMonthStart(ZonedDateTime.parse("2019-01-01T10:15:30Z")));
    }

    @Test
    public void getSameYearStart() {
        TimeService timeService = createTimeService();

        assertEquals(ZonedDateTime.parse("2019-01-01T00:00:00Z"),
                timeService.getSameYearStart(ZonedDateTime.parse("2019-10-03T20:28:13Z")));
        assertEquals(ZonedDateTime.parse("2018-01-01T00:00:00Z"),
                timeService.getSameYearStart(ZonedDateTime.parse("2018-01-08T10:15:30Z")));
        assertEquals(ZonedDateTime.parse("2018-01-01T00:00:00Z"),
                timeService.getSameYearStart(ZonedDateTime.parse("2018-01-01T10:15:30Z")));
    }

    @Test
    public void getPreviousDayStart() {
        TimeService timeService = createTimeService();

        assertEquals(ZonedDateTime.parse("2019-10-02T00:00:00Z"),
                timeService.getPreviousDayStart(ZonedDateTime.parse("2019-10-03T20:28:13Z")));
        assertEquals(ZonedDateTime.parse("2018-12-31T00:00:00Z"),
                timeService.getPreviousDayStart(ZonedDateTime.parse("2019-01-01T10:15:30Z")));
    }

    @Test
    public void getPreviousDayEnd() {
        TimeService timeService = createTimeService();

        assertEquals(ZonedDateTime.parse("2019-10-02T23:59:59.999Z"),
                timeService.getPreviousDayEnd(ZonedDateTime.parse("2019-10-03T20:28:13Z")));
        assertEquals(ZonedDateTime.parse("2018-12-31T23:59:59.999Z"),
                timeService.getPreviousDayEnd(ZonedDateTime.parse("2019-01-01T10:15:30Z")));
    }

    @Test
    public void getPreviousWeekStart() {
        TimeService timeService = createTimeService();

        assertEquals(ZonedDateTime.parse("2019-09-23T00:00:00Z"),
                timeService.getPreviousWeekStart(ZonedDateTime.parse("2019-10-03T20:28:13Z")));
        assertEquals(ZonedDateTime.parse("2018-12-31T00:00:00Z"),
                timeService.getPreviousWeekStart(ZonedDateTime.parse("2019-01-08T10:15:30Z")));
    }

    @Test
    public void getPreviousWeekEnd() {
        TimeService timeService = createTimeService();

        assertEquals(ZonedDateTime.parse("2019-09-29T23:59:59.999Z"),
                timeService.getPreviousWeekEnd(ZonedDateTime.parse("2019-10-03T20:28:13Z")));
        assertEquals(ZonedDateTime.parse("2019-01-06T23:59:59.999Z"),
                timeService.getPreviousWeekEnd(ZonedDateTime.parse("2019-01-08T10:15:30Z")));
    }

    @Test
    public void getPreviousMonthStart() {
        TimeService timeService = createTimeService();

        assertEquals(ZonedDateTime.parse("2019-09-01T00:00:00Z"),
                timeService.getPreviousMonthStart(ZonedDateTime.parse("2019-10-03T20:28:13Z")));
        assertEquals(ZonedDateTime.parse("2018-12-01T00:00:00Z"),
                timeService.getPreviousMonthStart(ZonedDateTime.parse("2019-01-12T13:33:07Z")));
    }

    @Test
    public void getPreviousMonthEnd() {
        TimeService timeService = createTimeService();

        assertEquals(ZonedDateTime.parse("2019-09-30T23:59:59.999Z"),
                timeService.getPreviousMonthEnd(ZonedDateTime.parse("2019-10-03T20:28:13Z")));
        assertEquals(ZonedDateTime.parse("2018-12-31T23:59:59.999Z"),
                timeService.getPreviousMonthEnd(ZonedDateTime.parse("2019-01-12T13:33:07Z")));
    }

}
