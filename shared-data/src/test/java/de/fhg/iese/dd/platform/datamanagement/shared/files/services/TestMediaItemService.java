/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileDownloadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItemSize;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

@Profile("test & !MediaItemServiceTest")
@Service
public class TestMediaItemService extends MediaItemService {

    @Autowired
    private ITimeService timeService;

    //we use URI instead of URL to avoid DNS lookups for equals and hashCode
    private final Map<URI, MediaItem> testMediaItems = new HashMap<>();
    private final Map<URI, RuntimeException> testExceptions = new HashMap<>();
    private final Map<URI, byte[]> testDownloads = new HashMap<>();

    @Override
    protected void initialize() {

        super.initialize();
    }

    @SneakyThrows
    public void addTestImageToDownload(URL url, byte[] content) {

        testDownloads.put(url.toURI(), content);
    }

    @SneakyThrows
    @Override
    public MediaItem createMediaItem(URL url, FileOwnership fileOwnership) {

        MediaItem testMediaItem = testMediaItems.get(url.toURI());
        if (testMediaItem != null) {
            return testMediaItem;
        } else {
            return super.createMediaItem(url, fileOwnership);
        }
    }

    @SneakyThrows
    @Override
    byte[] downloadImage(URL url) {

        if (testExceptions.containsKey(url.toURI())) {
            throw testExceptions.get(url.toURI());
        }
        byte[] content = testDownloads.get(url.toURI());
        if (content != null) {
            return content;
        } else {
            throw new FileDownloadException(
                    "URL {} is not set up in tests, download from external sources not allowed during tests", url);
        }
    }

    @SneakyThrows
    public void addTestMediaItem(URL url, MediaItem mediaItem) {

        testMediaItems.put(url.toURI(), mediaItem);
    }

    public MediaItem setupDownloadMock(URL url) {

        MediaItem mediaItem = MediaItem.builder()
                .created(timeService.currentTimeMillisUTC())
                .sizeVersion(0)
                .build();
        mediaItem.setFileOwnership(FileOwnership.UNKNOWN);
        SortedMap<MediaItemSize, String> urls = new TreeMap<>();
        urls.put(MediaItemSize.ORIGINAL_SIZE, url.toString());
        mediaItem.setUrls(urls);
        mediaItem = getItemRepository().saveAndFlush(mediaItem);
        addTestMediaItem(url, mediaItem);
        return mediaItem;
    }

    @SneakyThrows
    public void addTestException(URL url, RuntimeException exception) {

        testExceptions.put(url.toURI(), exception);
    }

    public void reset() {

        testMediaItems.clear();
        testDownloads.clear();
        testExceptions.clear();
    }

}
