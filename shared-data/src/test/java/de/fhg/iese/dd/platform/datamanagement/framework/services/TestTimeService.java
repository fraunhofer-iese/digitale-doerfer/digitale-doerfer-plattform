/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2019 Balthasar Weitzel, Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.framework.services;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;

@Service
@Profile({"test"})
public class TestTimeService extends TimeService implements ITimeService {

    private long offsetMillis = 0;

    private ZonedDateTime constantDateTime = null;

    @Autowired
    public TestTimeService(ApplicationConfig applicationConfig) {
        super(applicationConfig);
    }

    @Override
    public long currentTimeMillisUTC() {
        if(constantDateTime != null){
            return toTimeMillis(constantDateTime);
        }
        return System.currentTimeMillis() + offsetMillis;
    }

    @Override
    public ZonedDateTime nowLocal() {
        if(constantDateTime != null){
            return constantDateTime;
        }
        if(offsetMillis != 0){
            return ZonedDateTime.now(Clock.system(getLocalTimeZone())).plus(offsetMillis, ChronoUnit.MILLIS);
        }else{
            return ZonedDateTime.now(Clock.system(getLocalTimeZone()));
        }
    }

    @Override
    public ZonedDateTime nowUTC() {
        if (constantDateTime != null) {
            return constantDateTime;
        }
        if (offsetMillis != 0) {
            return ZonedDateTime.now(Clock.system(ZoneOffset.UTC)).plus(offsetMillis, ChronoUnit.MILLIS);
        } else {
            return ZonedDateTime.now(Clock.system(ZoneOffset.UTC));
        }
    }

    public void setOffset(Duration offset) {
        offsetMillis = offset.toMillis();
    }

    public void setOffset(long amount, TemporalUnit unit) {
        setOffset(Duration.of(amount, unit));
    }

    public void setOffsetToSetNow(ZonedDateTime newNow) {
        ZonedDateTime realNow = ZonedDateTime.now(Clock.system(ZoneOffset.UTC));
        offsetMillis = ChronoUnit.MILLIS.between(realNow, newNow);
    }

    public void resetOffset() {
        offsetMillis = 0;
    }

    public void setConstantDateTime(ZonedDateTime constantDateTime){
        this.constantDateTime = constantDateTime;
    }

    public void setConstantDateTime(long nowEpochMillis){
        this.constantDateTime = Instant.ofEpochMilli(nowEpochMillis).atZone(ZoneOffset.UTC);
    }

    public void resetConstantDateTime(){
        this.constantDateTime = null;
    }

}
