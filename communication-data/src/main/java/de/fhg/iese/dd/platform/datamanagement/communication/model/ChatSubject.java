/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.communication.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Table(indexes = {
        @Index(columnList = "entityName"),
        @Index(columnList = "entityId"),
        @Index(columnList = "entityName,entityId"),
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ChatSubject extends BaseEntity {

    @Column
    private String entityName;

    @Column(length = 36)
    private String entityId;

    @ManyToOne
    private PushCategory pushCategory;

    @ManyToOne
    @JoinColumn(name="chat_id")
    private Chat chat;

    @Override
    public String toString() {
        return "ChatSubject [" +
                "id='" + id + "', " +
                "entityName='" + entityName + "', " +
                "entityId='" + entityId + "'" +
                ']';
    }

}
